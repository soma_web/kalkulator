<?php
class Mysql_Lekeres {

    private $mysqli_handler;

    public function __construct($hostname, $username, $password, $database) {
        $this->mysqli_handler = new mysqli($hostname, $username, $password, $database);

        if ($this->mysqli_handler->connect_error) {
            trigger_error('Error: Could not make a database link (' . $this->mysqli_handler->connect_errno . ') ' . $this->mysqli_handler->connect_error);
        }

        $this->mysqli_handler->query("SET NAMES 'utf8'");
        $this->mysqli_handler->query("SET CHARACTER SET utf8");
        $this->mysqli_handler->query("SET CHARACTER_SET_CONNECTION=utf8");
        $this->mysqli_handler->query("SET SQL_MODE = ''");
    }

    public function query($sql) {
        $result = $this->mysqli_handler->query($sql, MYSQLI_STORE_RESULT);
        if ($result !== FALSE) {
            if (is_object($result)) {
                $i = 0;

                $data = array();

                while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                    $data[$i] = $row;

                    $i++;
                }

                $result->close();

                $query = new stdClass();
                $query->row = isset($data[0]) ? $data[0] : array();
                $query->rows = $data;
                $query->num_rows = count($data);

                unset($data);




                return $query;


            }
            else {
                return true;
            }
        }
        else {
            trigger_error('Error: ' . $this->mysqli_handler->error . '<br />Error No: ' . $this->mysqli_handler->errno . '<br />' . $sql);
            exit();
        }
    }

    public function escape($value) {
        return $this->mysqli_handler->real_escape_string($value);
    }

    public function countAffected() {
        return $this->mysqli_handler->affected_rows;
    }

    public function getLastId() {
        return $this->mysqli_handler->insert_id;
    }

    public function __destruct() {
        $this->mysqli_handler->close();
    }







    public function getSetting($group='config', $store_id = 0) {
        $data = array();

        $query = $this->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `group` = '" . $this->escape($group) . "'");

        foreach ($query->rows as $result) {
            if (!$result['serialized']) {
                $data[$result['key']] = $result['value'];
            } else {
                $data[$result['key']] = unserialize($result['value']);
            }
        }

        return $data;
    }

    public function editSetting($group, $data, $store_id = 0) {
        $this->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `group` = '" . $this->escape($group) . "'");

        foreach ($data as $key => $value) {
            if (!is_array($value)) {
                $this->query("INSERT INTO " . DB_PREFIX . "setting SET
				    store_id = '" . (int)$store_id . "',
				    `group` = '" . $this->escape($group) . "',
				    `key` = '" . $this->escape($key) . "', `value` = '" . $this->escape($value) . "'");
            } else {
                $this->query("INSERT INTO " . DB_PREFIX . "setting SET
				    store_id = '" . (int)$store_id . "',
				    `group` = '" . $this->escape($group) . "',
				    `key` = '" . $this->escape($key) . "', `value` = '" . $this->escape(serialize($value)) . "', serialized = '1'");
            }
        }
    }

    public function deleteSetting($group, $store_id = 0) {
        $this->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `group` = '" . $this->escape($group) . "'");
    }

    public function getCustomerGroups() {
        $sql = "SELECT 	customer_group_id FROM ".DB_PREFIX."customer_group ";
        $query = $this->query($sql);
        if ($query->num_rows > 0)
            return $query->rows;
        else
            return array();
    }

    public function getCustomer($name) {
        $sql = "SELECT 	customer_id FROM ".DB_PREFIX."customer WHERE `integracios_kod` LIKE '".$name."'";
        $query = $this->query($sql);
        if ($query->num_rows > 0)
            return $query->row['customer_id'];
        else
            return false;
    }

}