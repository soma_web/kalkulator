<?php
class ModelOldDatabaseTabla {

    private $db;
    public $data = array();

    public function __construct($query) {
        $this->db = $query;
    }

    public function tablaBeolvas() {

        $query = $this->db->query('show tables');
        if ($query) {
            foreach ($query->rows as $key=>$table) {
                if (strstr($table['Tables_in_'.OLD_DB_DATABASE],DB_PREFIX)) {
                    $table_adatok = substr($table['Tables_in_'.OLD_DB_DATABASE],strlen(DB_PREFIX));
                    $query_adatok = $this->db->query('DESCRIBE '.OLD_DB_PREFIX.$table_adatok);
                    $this->data['table'][$table_adatok] = $query_adatok->rows;

                } else {
                    $table_adatok = $table['Tables_in_'.OLD_DB_DATABASE];
                    $query_adatok = $this->db->query('DESCRIBE '.$table_adatok);
                    $this->data['table'][$table_adatok] = $query_adatok->rows;
                }
            }
        }
    }


}
?>