<?php
class ModelFormTabla {

    private $db;
    public $data = array();
    public $csoport = array();
    public $action;
    public $csv_files = array();

    public function __construct($query) {
        $this->db = $query;
    }

    public function csvFiles() {

        $csv_s=scandir("csv");

        foreach($csv_s as $file_name){

            $file_info = pathinfo($file_name);

            if  ($file_info['extension'] == "csv") {
                $this->csv_files[] = $file_name;

            }
        }
    }

    public function csvBeolvas($file='tablak_default.csv') {

        $handle = fopen('CSV/'.$file, "r");

        if ($handle){
            while (($sor = fgetcsv($handle, NULL, ";"))) {
                $tabla = str_replace(DB_PREFIX,'',$sor[0]);
                $tabla = str_replace(OLD_DB_PREFIX,'',$tabla);
                $oszlop['tabla']= str_replace(DB_PREFIX,'',$tabla);
                $oszlop['urit'] = $sor[1];
                $this->data[] = $oszlop;
            }
        }
        fclose($handle);

        $this->data = rendezes($this->data,'tabla');
        $elozo = '';
        foreach($this->data as $tabla) {
            $darabolt = explode('_',$tabla['tabla']);
            $this->csoport[$darabolt[0]][] = $tabla;
        }
        $this->action = "index.php?route=php/letrehoz";
    }
}
?>