<script type="text/javascript" src="tpl/javascript/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="tpl/javascript/content.js"></script>
<script type="text/javascript" src="tpl/javascript/masonry.pkgd.min.js"></script>
<script>
    $(document).ready(function(){
        var container = document.querySelector('.table_container');
        var msnry = new Masonry( container, {
            // options
            itemSelector: '.tablak'
        });
    });

</script>


<link rel="stylesheet" type="text/css" href="tpl/stylesheet.css" />

<form id="tablak" action="index.php?route=php/letrehoz" method="post" enctype="multipart/form-data" id="form">
    <div class="csv_files">
        <?php foreach($tables->csv_files as $key=>$files) { ?>

            <?php $active = (isset($_GET['file']) && $_GET['file'] == $files) ? "class='active'" : '' ?>

            <?php if (!isset($_GET['file']) && $key==0) {?>
                <?php $active = "class='active'";?>
            <?php } ?>

            <a <?php echo $active;?> href="index.php?file=<?php echo $files?>"><?php echo $files?></a>

        <?php } ?>
        <div class="check_all">
            <span>Minden kijelölése</span>
            <input type="checkbox" name="check_all" >
        </div>
    </div>
    <div>
        <span>A kijelölt táblák fognak átkerülni az új adatbázisba</span>
    </div>
    <div class="table_container">
    <?php foreach($tables->csoport as $tablak) { ?>
        <div class="tablak">
            <table >
                <?php $allchecked = true; ?>
                <?php foreach($tablak as $tabla) { ?>
                    <tr>
                        <td>
                            <?php $checked = $tabla['urit'] ? "checked='checked'" : ''?>
                            <input type="checkbox" <?php echo $checked; ?> name="<?php echo $tabla['tabla']?>" value="1"><?php echo $tabla['tabla']?>
                        <td>
                    </tr>
                    <?php if(!$tabla['urit']){$allchecked = false;}; ?>
                <?php } ?>
            </table>
            <div class="tabla_select">
                <div>
                    <span>Összes</span>
                    <?php $allchecked_text = $allchecked ? "checked='checked'" : ''?>
                    <input type="checkbox" name="check_all_in_box" class="check_all_box" <?php echo $allchecked_text; ?>>
                </div>
            </div>
        </div>
    <?php } ?>
    </div>
    <div>
        <input  id='dburites' type="button" value="Adatbázis létrehozása" style="padding: 7px 17px;">
        <input  id='mentes' type="button" value="Beállítások mentése" style="padding: 7px 17px;">
    </div>
</form>

<script>

    $('#dburites').bind("click",function(){
        $('#tablak').submit();
    });

    $('#mentes').bind("click",function(){
        var elkuld = true;
        if($(".filename_input").length == 0){
        $(this).parent("div").append("<div class='filename_input'><table><tr><td><span>Fájl neve:</span></td><td><input type='text' name='csv_filename'></td></tr></table></div>");
        }
        else{
            var text = $("input[name='csv_filename']").val();
            if(text != ""){
                var atadando = {};
                var data = $("input[type='checkbox']:not(.check_all_box):not([name='check_all'])");
                for(var i = 0; i < data.length; i++){
                    if(data.eq(i).is(":checked")){
                        atadando[data[i].name] = 1;
                    }else{
                        atadando[data[i].name] = 0;
                    }
                }
                debugger;
                $.ajax({
                    url: 'index.php?route=php/lement',
                    type: 'post',
                    data: {formadat:atadando},
                    success: function(status) {

                    },
                    error: function(e) {

                    }
                });
            }
            else{

            }
        }
    });
</script>
