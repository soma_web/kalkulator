<?php
function TermekOlvas($filename){
    $letrehoz="CREATE TABLE if not exists sap_termekek(
        `azonosito` INT(9) AUTO_INCREMENT DEFAULT NULL,
        `cikkszam`                    text default null,
        `cikk_megnevezese`            text default null,
        `cikk_idegen_neve_1`          text default null,
        `cikk_idegen_neve_2`          text default null,
        `cikk_idegen_neve_3`          text default null,

        `cikkcsoport_kodja`           text default null,
        `cikkcsoport_neve`            text default null,
        `gyarto_kodja`                text default null,
        `gyarto_neve`                 text default null,

        `ertekesitesi_egyseg`         text default null,
        `ertekesitesi_mennyiseg_min`  text default null,
        `csomagolasi_egyseg`          text default null,
        `csomagolasi_mennyiseg`       text default null,
        `darabaru_merete`             text default null,

        `szallitasi_hossz`            text default null,
        `szallitasi_szelesseg`        text default null,
        `szallitasi_magassag`         text default null,
        `szallitasi_meret_megyseg`    text default null,

        `szallitasi_suly`             text default null,
        `szallitasi_suly_megyseg`     text default null,
        `megjegyzes`                  text default null,
        `megjegyzes_idegen_neve_1`    text default null,
        `megjegyzes_idegen_neve_2`    text default null,
        `megjegyzes_idegen_neve_3`    text default null,
        `letrehozas_datuma`           text default null,
        `raktarkeszlet`               text default null,
        `ara`                         text default null,
        `statusz`                     text default null,
        PRIMARY KEY (`azonosito`))   engine=MyISAM default charset=UTF8";

    mysql_query($letrehoz) or die("Nem sikerült a tabla letrehozasa");



    $handle = fopen($filename, "r"); //a fájl megnyitása olvasásra
    $data = fgets($handle);

    while ($data = fgets($handle)) {
        $termek= explode(";",$data);
        $felvisz="insert into sap_termekek set
                    `cikkszam`                     = '".$termek[0]."',
                    `cikk_megnevezese`             = '".$termek[1]."',
                    `cikk_idegen_neve_1`           = '".$termek[2]."',
                    `cikk_idegen_neve_2`           = '".$termek[3]."',
                    `cikk_idegen_neve_3`           = '".$termek[4]."',
                    `cikkcsoport_kodja`            = '".$termek[5]."',
                    `cikkcsoport_neve`             = '".$termek[6]."',
                    `gyarto_kodja`                 = '".$termek[7]."',
                    `gyarto_neve`                  = '".$termek[8]."',
                    `ertekesitesi_egyseg`          = '".$termek[9]."',
                    `ertekesitesi_mennyiseg_min`   = '".$termek[10]."',
                    `csomagolasi_egyseg`           = '".$termek[11]."',
                    `csomagolasi_mennyiseg`        = '".$termek[12]."',
                    `darabaru_merete`              = '".$termek[13]."',
                    `szallitasi_hossz`             = '".$termek[14]."',
                    `szallitasi_szelesseg`         = '".$termek[15]."',
                    `szallitasi_magassag`          = '".$termek[16]."',
                    `szallitasi_meret_megyseg`     = '".$termek[17]."',
                    `szallitasi_suly`              = '".$termek[18]."',
                    `szallitasi_suly_megyseg`      = '".$termek[19]."',
                    `megjegyzes`                   = '".$termek[20]."',
                    `megjegyzes_idegen_neve_1`     = '".$termek[21]."',
                    `megjegyzes_idegen_neve_2`     = '".$termek[22]."',
                    `megjegyzes_idegen_neve_3`     = '".$termek[23]."',
                    `letrehozas_datuma`            = '".$termek[24]."',
                    `raktarkeszlet`                = '".$termek[25]."',
                    `ara`                          = '".$termek[26]."',
                    `statusz`                      = '".substr($termek[27],0,1)."'";
        mysql_query($felvisz) or Hibalista("Termékek CSV import", "sap_termekek", "csv beolvasás", $filename, "importálás");

    }
    fclose($handle);





    echo "A ".$filename." állomany importálasa befejeződött<br>";
}
?>