BEGIN

DECLARE NyelvKod INT(1) default 2;
DECLARE product_azonosito INT(11) default 0;
DECLARE store_azonosito INT(11) default 0;
DECLARE leiras_azonosito INT(11) default 0;

call termek_felvitel(uj_TermekKod);

SELECT product_id into product_azonosito FROM trodimp_aruhaz.product WHERE cikkszam LIKE uj_TermekKod limit 1;

if (upper(uj_Nyelv)="ENG") THEN
SET NyelvKod:=1;
END IF;


select product_id into leiras_azonosito from trodimp_aruhaz.product_description where product_id=product_azonosito AND language_id=NyelvKod limit 1;

if leiras_azonosito = 0 THEN
insert into trodimp_aruhaz.product_description SET
language_id	    = NyelvKod,
`name`	        = concat(uj_TermekNev," ",uj_NevMasodik," ",uj_NevHarmadik),
`description`	  = uj_Megjegyzes,
product_id      = product_azonosito,
meta_description	= uj_TermekNev,
meta_keyword	    = uj_TermekNev;
else
UPDATE trodimp_aruhaz.product_description SET
`name`	      = concat(uj_TermekNev," ",uj_NevMasodik," ",uj_NevHarmadik),
`description`	  = uj_Megjegyzes
WHERE product_id = product_azonosito and language_id = NyelvKod;
end if;

END