BEGIN

declare done int default false;
declare kategoria_nev varchar(255);
declare kategoria_nyelv varchar(5);
declare NyelvKod int(2);
declare foszulo varchar(255);
declare kategoria_id int(11);
declare talalt int(11);
declare szulo int(11);

set kategoria_id:=0;
set talalt:= 0;
set szulo:= 0;
set NyelvKod:= 2;

select category_id into kategoria_id from trodimp_aruhaz.category where kategoria_kod LIKE uj_Kod limit 1;

SET @i=length(uj_Kod)-1;

ciklus: WHILE (@i>= 0 AND kategoria_id = 0) DO
select category_id into szulo from trodimp_aruhaz.category where kategoria_kod LIKE substring(uj_Kod,1,@i) limit 1;
if (szulo != 0) then
LEAVE ciklus;
end if;
SET @i=@i-1;
END WHILE ciklus;

if (kategoria_id = 0) then
insert into  trodimp_aruhaz.category set
kategoria_kod = uj_Kod,
parent_id     = szulo,
`status`	    = 1,
`top`	        = 0,
`column`	    = 0,
`sort_order`	= 0,
date_added    = NOW(),
date_modified = NOW();

select category_id into kategoria_id from trodimp_aruhaz.category where kategoria_kod LIKE uj_Kod limit 1;

set foszulo:= concat(uj_kod,"_");
update trodimp_aruhaz.category set parent_id = kategoria_id  where kategoria_kod LIKE foszulo;
end if;

if uj_Nyelv="ENG" then
SET NyelvKod:=1;
end if;

select category_id into talalt from trodimp_aruhaz.category_description where category_id=kategoria_id AND language_id=NyelvKod limit 1;

if (talalt = 0) then
insert into trodimp_aruhaz.category_description set  category_id	= kategoria_id,
language_id	       = NyelvKod,
`description`	     = "",
`name`	           = uj_nev,
meta_description	 = uj_nev,
meta_keyword	     = uj_nev;
END IF;

if talalt=0 then
insert into trodimp_aruhaz.category_to_store set category_id=kategoria_id,
store_id = 0;
end if;

END