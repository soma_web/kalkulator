BEGIN

declare product_azonosito int(11) default 0;
declare related_azonosito int(11) default 0;
declare product_related_azonosito int(11) default 0;


call termek_felvitel(uj_TermekKod);
call termek_felvitel(uj_HasonloTerm);

SELECT product_id into product_azonosito FROM trodimp_aruhaz.product WHERE cikkszam LIKE uj_TermekKod limit 1;
SELECT product_id into related_azonosito FROM trodimp_aruhaz.product WHERE cikkszam LIKE uj_HasonloTerm limit 1;

select product_id into  product_related_azonosito from trodimp_aruhaz.product_related where product_id=product_azonosito and related_id=related_azonosito limit 1;
if product_related_azonosito=0 then
insert into trodimp_aruhaz.product_related set product_id=product_azonosito,
related_id=related_azonosito;
end if;

END