BEGIN
declare done int default false;
declare cikkszam_azonosito varchar(15);

declare lepteto cursor for
SELECT TermekKod FROM web_termek_torzs WHERE TermekTipus LIKE uj_TermekTipus;


declare continue handler for not found set done = true;

open lepteto;
ciklus: loop
fetch lepteto into cikkszam_azonosito;
if done then
leave ciklus;
end if;


end loop ciklus;
close lepteto;


END