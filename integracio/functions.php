<?php

 function rendezes($tomb, $rendez0=false, $merre0="ASC", $rendez1=false, $merre1="ASC", $rendez2=false, $merre2="ASC", $rendez3=false, $merre3="ASC", $reNumbering=false){

    $sortingSettings = array();
    if ($rendez0) {
        $sortingSettings[] = array(

            'orderby' =>  $rendez0,
            'sortorder'     => $merre0
        );
    }
    if ($rendez1) {
        $sortingSettings[] = array(

            'orderby' =>  $rendez1,
            'sortorder'     => $merre1
        );
    }
    if ($rendez2) {
        $sortingSettings[] = array(
            'orderby' =>  $rendez2,
            'sortorder'     => $merre2
        );
    }
    if ($rendez3) {
        $sortingSettings[] = array(
            'orderby' =>  $rendez3,
            'sortorder'     => $merre1
        );
    }


    $rendezo = new ArrayOfArrays($tomb);
    $rendezo->multiSorting($sortingSettings,$reNumbering);
    return $rendezo->getArrayCopy();
}

class ArrayOfArrays extends ArrayObject {
    private $sortSettings;

    public function filterByValue($searchedKey, $value) {
        $iterator = $this->getIterator();

        $newArray = array();
        foreach($iterator as $key=>$object) {
            if($object[$searchedKey] == $value) {
                $newArray[$key] = $object;
            }
        }

        return $newArray;
    }

    /**
     * @param array $sortingSettings
     * @param bool $reNumbering
     */
    public function multiSorting($sortingSettings, $reNumbering=false) {

        $this->sortSettings = $sortingSettings;
        $this->uasort(array($this, 'multisort'));

        if($reNumbering) {
            $n = 0;
            $newArray = array();
            foreach($this->getArrayCopy() as $value) {
                $newArray[$n] = $value;
                $n++;
            }
            $this->exchangeArray($newArray);
        }
    }

    public function multisort($a, $b) {
        foreach($this->sortSettings as $settings) {
            $key = $settings['orderby'];
            $sortorder = $settings['sortorder'];
            if($a[$key] > $b[$key]) {
                if($sortorder == "ASC") {
                    return 1;
                } else {
                    return -1;
                }
            } elseif($a[$key] < $b[$key]) {
                if($sortorder == "ASC") {
                    return -1;
                } else {
                    return 1;
                }
            }
        }
        return 0;
    }


}