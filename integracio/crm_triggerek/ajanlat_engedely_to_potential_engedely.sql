begin
    DECLARE result int(10);

  IF (NEW.quotestage = "Engedélyeztetés" AND OLD.quotestage != "Engedélyeztetés") THEN

     UPDATE vtiger_potential set
      sales_stage = 'Engedélyeztetés'
      WHERE NEW.potentialid = potentialid;

  END IF;

END