BEGIN
DECLARE webshop INT(1) default 0;

IF NEW.discontinued = 1 THEN

	SELECT cf_2393 into webshop FROM vtiger_productcf
    	WHERE productid = NEW.productid;
	IF webshop = 1 THEN

 		insert into product_to_webshop set
        	hivo_tabla = "vtiger_products",
        	productid = NEW.productid,
        	hivo_esemeny = "modositas";
    END IF;
END IF;

END