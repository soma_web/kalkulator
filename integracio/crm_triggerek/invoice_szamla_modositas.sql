begin

  IF (NEW.invoicestatus = "Számla" AND OLD.invoicestatus != "Számla") OR (NEW.invoicestatus = "Előlegszámla" AND OLD.invoicestatus != "Előlegszámla") OR (NEW.invoicestatus = "Végszámla" AND OLD.invoicestatus != "Végszámla") THEN
    INSERT INTO vtiger_invoice_szamla set
      invoiceid = NEW.invoiceid,
      invoicestatus = NEW.invoicestatus;

  END IF;

END