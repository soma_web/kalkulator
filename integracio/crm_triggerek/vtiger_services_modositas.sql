BEGIN
DECLARE webshop INT(1) default 0;

  IF NEW.discontinued = 1 THEN
      insert into product_to_webshop set
            hivo_tabla = "vtiger_service",
            productid = NEW.serviceid,
            hivo_esemeny = "modositas";
  END IF;

END