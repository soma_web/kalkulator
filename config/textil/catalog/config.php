<?php
// HTTP

define('HTTP_SERVER', 'http://textil.nmsnet.hu/');
define('HTTP_IMAGE', 'http://textil.nmsnet.hu/image/');
define('HTTP_ADMIN', 'http://textil.nmsnet.hu/admin/');

// HTTPS
define('HTTPS_SERVER', 'http://textil.nmsnet.hu/');
define('HTTPS_IMAGE', 'http://textil.nmsnet.hu/image/');

// DIR
define('DIR_ARUHAZ', '/home/textilworld/public_html/');
define('DIR_APPLICATION', '/home/textilworld/public_html/catalog/');
define('DIR_SYSTEM', '/home/textilworld/public_html/system/');
define('DIR_DATABASE', '/home/textilworld/public_html/system/database/');
define('DIR_LANGUAGE', '/home/textilworld/public_html/catalog/language/');
define('DIR_TEMPLATE', 'catalog/view/theme/');
define('DIR_CONFIG', '/home/textilworld/public_html/system/config/');
define('DIR_IMAGE_NYOMTAT', 'image/');
define('DIR_IMAGE', '/home/textilworld/public_html/image/');
define('DIR_CACHE', '/home/textilworld/public_html/system/cache/');
define('DIR_DOWNLOAD', '/home/textilworld/public_html/download/');
define('DIR_LOGS', '/home/textilworld/public_html/system/logs/');
define('DIR_TEMPLATE_IMAGE', '/catalog/view/theme/');

// DB

define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'textilworld');
define('DB_PASSWORD', 'tex_119');
define('DB_DATABASE', 'textilwo_textil');
define('DB_PREFIX', 'hungaro_');

?>
