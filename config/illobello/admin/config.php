<?php
// HTTP
define('HTTP_SERVER', 'http://illobello.nmsnet.hu/admin/');
define('HTTP_CATALOG', 'http://illobello.nmsnet.hu/');
define('HTTPS_CATALOG', 'http://illobello.nmsnet.hu/');
//define('HTTP_IMAGE', 'http://pos.nmsnet.hu/image/');
define('HTTP_IMAGE', '../image/');

// HTTPS
define('HTTPS_SERVER', 'http://illobello.nmsnet.hu/admin/');
define('HTTPS_IMAGE', 'http://illobello.nmsnet.hu/image/');

// DIR
define('DIR_APPLICATION', '/home/illobello/public_html/admin/');
define('DIR_SYSTEM', '/home/illobello/public_html/system/');
define('DIR_DATABASE', '/home/illobello/public_html/system/database/');
define('DIR_LANGUAGE', '/home/illobello/public_html/admin/language/');
define('DIR_TEMPLATE', '/home/illobello/public_html/admin/view/template/');
define('DIR_CONFIG', '/home/illobello/public_html/system/config/');
define('DIR_IMAGE', '/home/illobello/public_html/image/');
define('DIR_CACHE', '/home/illobello/public_html/system/cache/');
define('DIR_DOWNLOAD', '/home/illobello/public_html/download/');
define('DIR_LOGS', '/home/illobello/public_html/system/logs/');
define('DIR_CATALOG', '/home/illobello/public_html/catalog/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'illobello');
define('DB_PASSWORD', 'ill_119');
define('DB_DATABASE', 'illobell_illobello');
define('DB_PREFIX', 'hungaro_');


define('VATERA_FRISSIT', 2);
define('VATERA_AZONOSITO_KOD', 230);  // a 230 helyett a vatera által megadott kód
?>