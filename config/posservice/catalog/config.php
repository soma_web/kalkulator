<?php
// HTTP

define('HTTP_SERVER', 'http://pos.nmsnet.hu/');
define('HTTP_IMAGE', 'http://pos.nmsnet.hu/image/');
define('HTTP_ADMIN', 'http://pos.nmsnet.hu/admin/');

// HTTPS
define('HTTPS_SERVER', 'http://pos.nmsnet.hu/');
define('HTTPS_IMAGE', 'http://pos.nmsnet.hu/image/');

// DIR
define('DIR_APPLICATION', '/home/posser/public_html/catalog/');
define('DIR_SYSTEM', '/home/posser/public_html/system/');
define('DIR_DATABASE', '/home/posser/public_html/system/database/');
define('DIR_LANGUAGE', '/home/posser/public_html/catalog/language/');
define('DIR_TEMPLATE', 'catalog/view/theme/');
define('DIR_CONFIG', '/home/posser/public_html/system/config/');
define('DIR_IMAGE_NYOMTAT', 'image/');
define('DIR_IMAGE', '/home/posser/public_html/image/');
define('DIR_CACHE', '/home/posser/public_html/system/cache/');
define('DIR_DOWNLOAD', '/home/posser/public_html/download/');
define('DIR_LOGS', '/home/posser/public_html/system/logs/');
define('DIR_TEMPLATE_IMAGE', '/catalog/view/theme/');

// DB

define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'posser');
define('DB_PASSWORD', 'pos_119');
define('DB_DATABASE', 'posser_pos');
define('DB_PREFIX', 'hungaro_');

?>
