<?php
class CartElhelyezkedes {
    private $data = array();

    public function __construct($registry) {
        $this->config = $registry->get('config');
        $this->customer = $registry->get('customer');
        $this->session = $registry->get('session');
        $this->db = $registry->get('db');
        $this->tax = $registry->get('tax');
        $this->weight = $registry->get('weight');

        if (!isset($this->session->data['cart_elhelyezkedes']) || !is_array($this->session->data['cart_elhelyezkedes'])) {
            $this->session->data['cart_elhelyezkedes'] = array();
        }
    }

    public function getProducts() {
        if (!$this->data) {

            $total    = 0;

            foreach ($this->session->data['cart_elhelyezkedes'] as $key => $kiemeles_sorok) {
                $quantity = 0;

                $product_id = $key;
                $product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p
                    LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
                    WHERE
                        p.product_id = '" . (int)$product_id . "'
                        AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

                if ($product_query->num_rows) {

                    foreach($kiemeles_sorok as $value) {
                        $quantity ++;
                        $total += $value['brutto'];
                    }

                    $this->data[$key] = array(
                        'key'             => $key,
                        'product_id'      => $product_query->row['product_id'],
                        'name'            => $product_query->row['name'],
                        'image'           => $product_query->row['image'],
                        'quantity'        => $quantity,
                        'total'           => $total,
                        'kiemelesek'      => $kiemeles_sorok
                    );
                } else {
                    $this->remove($key);
                }
            }
        }


        return $this->data;
    }


    public function remove($key) {

        $kulcs = explode(",",$key);

        if ( count($kulcs) == 2 ) {
            if (isset($this->session->data['cart_elhelyezkedes'][trim($kulcs[0])][trim($kulcs[1])]  )   ) {
                unset( $this->session->data['cart_elhelyezkedes'][trim($kulcs[0])][trim($kulcs[1])]);
                if (count($this->session->data['cart_elhelyezkedes'][trim($kulcs[0])]) == 0 ) {
                    unset( $this->session->data['cart_elhelyezkedes'][trim($kulcs[0])]);
                }
            }
        } elseif (  isset($this->session->data['cart_elhelyezkedes'][trim($kulcs[0])])  ) {
            unset( $this->session->data['cart_elhelyezkedes'][trim($kulcs[0])]);
        }

        if (count($this->session->data['cart_elhelyezkedes']) == 0 ) {
            //unset( $this->session->data['cart_elhelyezkedes']);
            unset( $this->session->data['elhelyezkedes']);

        }
        $this->data = array();
    }

    public function clear() {
        $this->session->data['cart_elhelyezkedes'] = array();
        $this->data = array();
    }



   /* public function getSubTotal() {
        $total = 0;

        foreach ($this->getProducts() as $product) {
            if ($product['utalvany'] != 1) {
                $total += $product['total'];
            }
        }

        return $total;
    }*/

   /* public function getTaxes() {
        $tax_data = array();

        foreach ($this->getProducts() as $product) {
            if ($product['tax_class_id']) {
                $tax_rates = $this->tax->getRates($product['total'], $product['tax_class_id']);

                foreach ($tax_rates as $tax_rate) {
                    $amount = 0;

                    if ($tax_rate['type'] == 'F') {
                        $amount = ($tax_rate['amount'] * $product['quantity']);
                    } elseif ($tax_rate['type'] == 'P') {
                        $amount = $tax_rate['amount'];
                    }

                    if (!isset($tax_data[$tax_rate['tax_rate_id']])) {
                        $tax_data[$tax_rate['tax_rate_id']] = $amount;
                    } else {
                        $tax_data[$tax_rate['tax_rate_id']] += $amount;
                    }
                }
            }
        }

        return $tax_data;
    }*/

   /* public function getTotal() {
        $total = 0;

        foreach ($this->getProducts() as $product) {
            $total += $this->tax->calculate($product['total'], $product['tax_class_id'], $this->config->get('config_tax'));
        }

        return $total;
    }*/

    public function countProducts() {
        $product_total = 0;

        $products = $this->getProducts();

        foreach ($products as $product) {
            $product_total += count($product['kiemelesek']);
        }

        return $product_total;
    }

    public function hasProducts() {
        return count($this->session->data['cart_elhelyezkedes']);
    }










}
?>