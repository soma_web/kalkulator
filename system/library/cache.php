<?php
class Cache { 
	private $expire = 3600; 

  	public function __construct($registry) {
        $this->config = $registry->get('config');

        $files = glob(DIR_CACHE . 'cache.*');
		
		if ($files) {
			foreach ($files as $file) {
				$time = substr(strrchr($file, '.'), 1);

      			if ($time < time()) {
					if (file_exists($file)) {
						unlink($file);
					}
      			}
    		}
		}
  	}

	public function get($key) {
        if ($this->config->get('megjelenit_altalanos_cache')) {

            $files = glob(DIR_CACHE . 'cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.*');
            if ($files) {
                $cache = file_get_contents($files[0]);

                return unserialize($cache);
            }
		}
	}

  	public function set($key, $value) {
        if ($this->config->get('megjelenit_altalanos_cache')) {

            $this->delete($key);
		
            $file = DIR_CACHE . 'cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.' . (time() + $this->expire);

            $handle = fopen($file, 'w');

            $siker = fwrite($handle, serialize($value));

            $siker = fclose($handle);
        }
  	}

  	public function delete($key) {
		$files = glob(DIR_CACHE . 'cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.*');
		
		if ($files) {
    		foreach ($files as $file) {
      			if (file_exists($file)) {
					unlink($file);
				}
    		}
		}
  	}
}
?>