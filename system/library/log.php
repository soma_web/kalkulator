<?php
class Log {
    private $filename;
    private $time_pre;
    private $microtime;

    public function __construct($filename,$microtime=0) {
        $this->filename = $filename;
        $this->microtime = $microtime;
    }

    public function write($message) {
        $file = DIR_LOGS . $this->filename;

        $handle = fopen($file, 'a+');

        fwrite($handle, date('Y-m-d G:i:s') . ' - ' . $message . "\n");

        fclose($handle);
    }

    public function write_crone($message) {
        $file = DIR_LOGS . 'cron.txt';

        $handle = fopen($file, 'a+');

        fwrite($handle, date('Y-m-d G:i:s') . ' - ' . $message . "\n");

        fclose($handle);
    }

    public function setMicroTime() {
        $this->time_pre = microtime(true);
    }

    public function GetMicroTime($message,$min_ido=0,$elso=false) {
        if ($this->microtime) {
            $time_post = microtime(true);
            $exec_time = $time_post - $this->time_pre;
            if ($exec_time >= $min_ido) {
                $exec_time = round($exec_time, 4);
                $this->write_microtime(' idö: ' . round($exec_time, 4) . '     ' . $message,$elso);
            }
        }
    }

    public function write_microtime($message,$elso=false) {
        if ($this->microtime) {
            $file = DIR_LOGS . 'microtime.txt';

            $handle = fopen($file, 'a+');

            if ($elso) {
                $siker = fwrite($handle, "\n\n\n");
            }
            $siker = fwrite($handle, date('Y-m-d G:i:s') . ' - ' . $message . "\n");

            $siker = fclose($handle);
        }
    }
}
?>