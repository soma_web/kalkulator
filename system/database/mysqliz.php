<?php
final class MySQLiz {
	private $mysqli_handler;
	private $all_time = 0;
	private $log = '';
	private $microtime = '';

	public function __construct($hostname, $username, $password, $database,$dir_system='system/') {
		$this->mysqli_handler = new mysqli($hostname, $username, $password, $database);
		
		if ($this->mysqli_handler->connect_error) {
      		trigger_error('Error: Could not make a database link (' . $this->mysqli_handler->connect_errno . ') ' . $this->mysqli_handler->connect_error);
		}
		
		$this->mysqli_handler->query("SET NAMES 'utf8'");
		$this->mysqli_handler->query("SET CHARACTER SET utf8");
		$this->mysqli_handler->query("SET CHARACTER_SET_CONNECTION=utf8");
		$this->mysqli_handler->query("SET SQL_MODE = ''");

        $sql = "SHOW TABLES LIKE  '".DB_PREFIX."setting'";
        $query=$this->query($sql);
        if ($query->row) {
            $sql = "SELECT * FROM " . DB_PREFIX . "setting WHERE `key` = 'megjelenit_altalanos'";
            $query = $this->query($sql);
            $megjelenit_altalanos = unserialize($query->row['value']);
            $this->microtime = isset($megjelenit_altalanos['sebesseg_nyomkovetes']) ? $megjelenit_altalanos['sebesseg_nyomkovetes'] : 0 ;
        } else {
            $this->microtime = 0;
        }

  }
		
  public function query($sql) {

      if ($this->microtime) {
          $siker = require_once(DIR_SYSTEM . 'library/log.php');
          if (empty($this->log)) {
              $this->log = new Log("microtime.txt", $this->microtime);
          }
          $this->log->setMicroTime();
          $time_pre = microtime(true);
      }

      $result = $this->mysqli_handler->query($sql, MYSQLI_STORE_RESULT);
		if ($result !== FALSE) {
			if (is_object($result)) {

                $i = 0;
    	
				$data = array();
		
				while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
					$data[$i] = $row;
    	
					$i++;
				}

				$result->close();
				
				$query = new stdClass();
				$query->row = isset($data[0]) ? $data[0] : array();
				$query->rows = $data;
				$query->num_rows = count($data);
				
				unset($data);
                if ($this->microtime) {
                    $this->log->GetMicroTime('sql lekérés: ' . $sql, 0.1);
                    $time_post = microtime(true);
                    $this->all_time += ($time_post - $time_pre);
                    //$this->log->write_microtime(' Eddig sql: ' . $this->all_time . $sql);
                }
                return $query;
    		 
			
			}
			else {
                if ($this->microtime) {
                    $this->log->GetMicroTime('sql lekérés: ' . $sql, 0.1);
                    $time_post = microtime(true);
                    $this->all_time += ($time_post - $time_pre);
                }
				return true;
			}
		}
		else {
            if ($this->microtime) {
                $this->log->GetMicroTime('sql lekérés: ' . $sql, 0.1);
                $time_post = microtime(true);
                $this->all_time += ($time_post - $time_pre);
            }

			trigger_error('Error: ' . $this->mysqli_handler->error . '<br />Error No: ' . $this->mysqli_handler->errno . '<br />' . $sql);
			exit();
		}
  }
	
	public function escape($value) {
		return $this->mysqli_handler->real_escape_string($value);
	}
	
	public function countAffected() {
		return $this->mysqli_handler->affected_rows;
	}

	public function getLastId() {
		return $this->mysqli_handler->insert_id;
	}	
	


    public function __destruct() {
        if (!empty($this->log) && $this->microtime) {
            $this->log->write_microtime('SQL lekérések összesen: ' . $this->all_time."\n\n\n\n");
        }

        $this->mysqli_handler->close();
    }
}
?>