<?php

// Version
define('VERSION', '1.5.2.1');

// Configuration
include_once('config.php');

// Install
if (!defined('DIR_APPLICATION')) {
	header('Location: install/index.php');
	exit;
}

// Startup
require_once(DIR_SYSTEM . 'startup.php');
//require_once(DIR_SYSTEM . 'databaseupdate.php');

// Application Classes
require_once(DIR_SYSTEM . 'library/product.php');
require_once(DIR_SYSTEM . 'library/customer.php');
require_once(DIR_SYSTEM . 'library/affiliate.php');
require_once(DIR_SYSTEM . 'library/currency.php');
require_once(DIR_SYSTEM . 'library/tax.php');
require_once(DIR_SYSTEM . 'library/weight.php');
require_once(DIR_SYSTEM . 'library/length.php');
require_once(DIR_SYSTEM . 'library/cart.php');
require_once(DIR_SYSTEM . 'library/cart_elhelyezkedes.php');


// Registry
$registry = new Registry();

// Loader
$loader = new Loader($registry);
$registry->set('load', $loader);

// Config
$config = new Config();
$registry->set('config', $config);

// Database
$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
$registry->set('db', $db);

if (file_exists("config_mindennap.php")) {
    include_once('config_mindennap.php');
    $db_mindennap = new DB(DB_DRIVER_MINDENNAP, DB_HOSTNAME_MINDENNAP, DB_USERNAME_MINDENNAP, DB_PASSWORD_MINDENNAP, DB_DATABASE_MINDENNAP);
    $registry->set('db_mindennap', $db_mindennap);
}
if (file_exists("config_crm.php")) {
    include_once('config_crm.php');
    $db_crm = new DB(DB_DRIVER_CRM, DB_HOSTNAME_CRM, DB_USERNAME_CRM, DB_PASSWORD_CRM, DB_DATABASE_CRM);
    $registry->set('db_crm', $db_crm);
}

if (file_exists("config_kalkulator.php")) {
    include_once('config_kalkulator.php');
    $db_kalkulator = new DB(DB_DRIVER_KALKULATOR, DB_HOSTNAME_KALKULATOR, DB_USERNAME_KALKULATOR, DB_PASSWORD_KALKULATOR, DB_DATABASE_KALKULATOR);
    $registry->set('db_kalkulator', $db_kalkulator);
}


// Store
if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
	$store_query = $db->query("SELECT * FROM " . DB_PREFIX . "store WHERE REPLACE(`ssl`, 'www.', '') = '" . $db->escape('https://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
} else {
	$sql = "SELECT * FROM " . DB_PREFIX . "store WHERE REPLACE(`url`, 'www.', '') = '" . $db->escape('http://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'";
	$store_query = $db->query($sql);
}

if ($store_query->num_rows) {
	$config->set('config_store_id', $store_query->row['store_id']);
} else {
	$config->set('config_store_id', 0);
}


// Session
$session = new Session();
$registry->set('session', $session);

//$databaseupdate = new DatabaseUpdate($registry);

// Settings

$no_module = array();
if (isset($_SESSION['no_modules']) && empty($_GET['csere'])) {
    $query = $_SESSION['no_modules'];
} else {
    $query = $db->query("SELECT * FROM " . DB_PREFIX . "no_module");
    $_SESSION['no_modules'] = $query;
}
foreach ($query->rows as $result) {
    $no_module[] = $result['module_name'];
}

$query = $db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '0' OR store_id = '" . (int)$config->get('config_store_id') . "' ORDER BY store_id ASC");

foreach ($query->rows as $setting) {

    if (!in_array($setting['group'], $no_module)){

    	if (!$setting['serialized']) {
    		$config->set($setting['key'], $setting['value']);
	    } else {
            //echo $setting['value'];
		    $config->set($setting['key'], unserialize($setting['value']));
	    }
	}
}

$config->set('base_config_language',$config->get('config_language'));

if (!$store_query->num_rows) {
	$config->set('config_url', HTTP_SERVER);
	$config->set('config_ssl', HTTPS_SERVER);
}

// Url


$url = new Url($config->get('config_url'), $config->get('config_use_ssl') ? $config->get('config_ssl') : $config->get('config_url'));
$registry->set('url', $url);

// Log
$sebesseg_nyomkovetes = isset($config->get('megjelenit_altalanos')['sebesseg_nyomkovetes']) ? $config->get('megjelenit_altalanos')['sebesseg_nyomkovetes'] : 0;
$log = new Log($config->get('config_error_filename'),$sebesseg_nyomkovetes);
$registry->set('log', $log);

function error_handler($errno, $errstr, $errfile, $errline) {
	global $log, $config;

	switch ($errno) {
		case E_NOTICE:
		case E_USER_NOTICE:
			$error = 'Notice';
			break;
		case E_WARNING:
		case E_USER_WARNING:
			$error = 'Warning';
			break;
		case E_ERROR:
		case E_USER_ERROR:
			$error = 'Fatal Error';
			break;
		default:
			$error = 'Unknown';
			break;
	}

	if ($config->get('config_error_display')) {
		echo '<b>' . $error . '</b>: ' . $errstr . ' in <b>' . $errfile . '</b> on line <b>' . $errline . '</b>';
	}

	if ($config->get('config_error_log')) {
		$log->write('PHP ' . $error . ':  ' . $errstr . ' in ' . $errfile . ' on line ' . $errline);
	}

	return true;
}



// Error Handler
set_error_handler('error_handler');

// Request
$request = new Request();
$registry->set('request', $request);

// Response
$response = new Response();
$response->addHeader('Content-Type: text/html; charset=utf-8');
$response->setCompression($config->get('config_compression'));
$registry->set('response', $response);

// Cache
$registry->set('cache', new Cache($registry));

/*$cache = new Cache();
$registry->set('cache', $cache);*/

// Session
/*$session = new Session();
$registry->set('session', $session);*/

// Language Detection
$languages = array();

$query = $db->query("SELECT * FROM " . DB_PREFIX . "language WHERE status = '1'");

foreach ($query->rows as $result) {
	$languages[$result['code']] = $result;
}

$detect = '';

if (isset($request->server['HTTP_ACCEPT_LANGUAGE']) && ($request->server['HTTP_ACCEPT_LANGUAGE'])) {
	$browser_languages = explode(',', $request->server['HTTP_ACCEPT_LANGUAGE']);

	foreach ($browser_languages as $browser_language) {
		foreach ($languages as $key => $value) {
			if ($value['status']) {
				$locale = explode(',', $value['locale']);

				if (in_array($browser_language, $locale)) {
					$detect = $key;
				}
			}
		}
	}
}

if (isset($session->data['language']) && array_key_exists($session->data['language'], $languages) && $languages[$session->data['language']]['status']) {
	$code = $session->data['language'];
} elseif (isset($request->cookie['language']) && array_key_exists($request->cookie['language'], $languages) && $languages[$request->cookie['language']]['status']) {
	$code = $request->cookie['language'];
} elseif ($detect) {
	$code = $detect;
} else {
	$code = $config->get('config_language');
}
$code = $config->get('config_language');

if (!isset($session->data['language']) || $session->data['language'] != $code) {
	$session->data['language'] = $code;
}


if (!isset($request->cookie['language']) || $request->cookie['language'] != $code) {
	setcookie('language', $code, time() + 60 * 60 * 24 * 30, '/', $request->server['HTTP_HOST']);
}

$config->set('config_language_id', $languages[$code]['language_id']);
$config->set('config_language', $languages[$code]['code']);

// Language
$language = new Language($languages[$code]['directory'],$config->get('config_template'));
$language->load($languages[$code]['filename']);
//$language->load($languages[$code]['filename'],$config->get('config_template'));
$registry->set('language', $language);

// Document
$registry->set('document', new Document());

// Customer
$registry->set('customer', new Customer($registry));

// Product
$registry->set('product', new Product($registry));


// Affiliate
$registry->set('affiliate', new Affiliate($registry));

if (isset($request->get['tracking']) && !isset($request->cookie['tracking'])) {
	setcookie('tracking', $request->get['tracking'], time() + 3600 * 24 * 1000, '/');
}

// Currency
$registry->set('currency', new Currency($registry));

// Tax
$registry->set('tax', new Tax($registry));

// Weight
$registry->set('weight', new Weight($registry));

// Length
$registry->set('length', new Length($registry));

// Cart
$registry->set('cart', new Cart($registry));

// Cart_elhelyezkedes
$registry->set('cart_elhelyezkedes', new CartElhelyezkedes($registry));

//  Encryption
$registry->set('encryption', new Encryption($config->get('config_encryption')));


$megjelenit_admin_altalanos       = $config->get('megjelenit_admin_altalanos');
if (isset($megjelenit_admin_altalanos['fogalom_magyarazat']) && $megjelenit_admin_altalanos['fogalom_magyarazat']) {

    $sql = "SELECT fmd.name, fm.fogalom_magyarazat_id, LENGTH(fmd.name) AS hossz FROM " . DB_PREFIX . "fogalom_magyarazat fm
			 	LEFT JOIN " . DB_PREFIX . "fogalom_magyarazat_description fmd ON (fm.fogalom_magyarazat_id = fmd.fogalom_magyarazat_id)
			 	WHERE fmd.language_id = '" . (int)$config->get('config_language_id') . "'
			 	AND fm.status=1
			 	AND fmd.description > ''
			 	AND fmd.name > ''
			 	ORDER BY hossz DESC";

    $fogalom_query = $db->query($sql);

    $registry->set('fogalmak',$fogalom_query->rows);
}

// Front Controller
$controller = new Front($registry);

// Maintenance Mode
$controller->addPreAction(new Action('common/maintenance'));

// SEO URL's
$controller->addPreAction(new Action('common/seo_url'));

// Router
if (isset($request->get['route'])) {
    $log->setMicroTime();
    $action = new Action(trim($request->get['route']));
    $log->GetMicroTime('Action - '.$request->get['route'],0,true);

} else {
	$action = new Action('common/home');
}

// Dispatch

$controller->dispatch($action, new Action('error/not_found'));
$template=$config->get('config_template');

//$log->write('Összesen eltelt idő: '.$exec_time);


// Output
$response->output();
?>