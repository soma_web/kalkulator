﻿<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/alap/admin/');
define('HTTP_CATALOG', 'http://localhost/alap/');
define('HTTPS_CATALOG', 'http://localhost/alap/');
define('HTTP_IMAGE', 'http://localhost/alap/image/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/alap/admin/');
define('HTTPS_IMAGE', 'http://localhost/alap/image/');
define('HTTP_ARUHAZ', 'http://localhost/alap/');

// DIR
define('DIR_APPLICATION', '/var/www/html/alap/admin/');
define('DIR_SYSTEM', '/var/www/html/alap/system/');
define('DIR_DATABASE', '/var/www/html/alap/system/database/');
define('DIR_LANGUAGE', '/var/www/html/alap/admin/language/');
define('DIR_TEMPLATE', '/var/www/html/alap/admin/view/template/');
define('DIR_CONFIG', '/var/www/html/alap/system/config/');
define('DIR_IMAGE', '/var/www/html/alap/image/');
define('DIR_CACHE', '/var/www/html/alap/system/cache/');
define('DIR_DOWNLOAD', '/var/www/html/alap/download/');
define('DIR_LOGS', '/var/www/html/alap/system/logs/');
define('DIR_CATALOG', '/var/www/html/alap/catalog/');
define('DIR_ARUHAZ', '/var/www/html/alap/');

define('VATERA_FRISSIT', 2);
define('VATERA_AZONOSITO_KOD', 230);  // a 230 helyett a vatera által megadott kód

// DB
define('DB_DRIVER', 'mysqliz');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'dell');

define('DB_PREFIX', '');
define('DB_DATABASE', 'trodimp');

//define('DB_PREFIX', 'hungaro_');
//define('DB_DATABASE', 'eusolar');


?>