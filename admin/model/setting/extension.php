<?php
class ModelSettingExtension extends Model {

    public function getInstalled($type) {
        $extension_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "extension WHERE `type` = '" . $this->db->escape($type) . "'");

        foreach ($query->rows as $result) {
            $extension_data[] = $result['code'];
        }

        return $extension_data;
    }

    public function install($type, $code) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "extension SET `type` = '" . $this->db->escape($type) . "', `code` = '" . $this->db->escape($code) . "'");
    }

    public function uninstall($type, $code) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "extension WHERE `type` = '" . $this->db->escape($type) . "' AND `code` = '" . $this->db->escape($code) . "'");
    }

    public function getNoModule() {
        $no_module = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "no_module");

        if ($query->num_rows > 0) {
            foreach ($query->rows as $result) {
                $no_module[] = $result['module_name'];
            }
        }

        return $no_module;
    }

    public function insertNoModule($no_module) {

        $this->db->query("DELETE FROM " . DB_PREFIX . "no_module");

        if (isset($no_module) && !empty($no_module)) {
            foreach ($no_module as $key=>$value) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "no_module SET module_name = '".$key."'");
            }
        }

    }
}
?>