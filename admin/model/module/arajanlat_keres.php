<?php
class ModelModuleArajanlatKeres extends Model {
	public function getSzurtProducts($data) {
        $sql = "SELECT  p.product_id, p.price, p.model, p.cikkszam, m.name as gyarto, pd.name  FROM " . DB_PREFIX . "product p
			    INNER JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
			    LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id)";

        $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
        $sql .= " AND pd.name > ''";


        if (isset($data['ar_tol']) && $data['ar_tol'] ){
            $sql .= ' AND p.price >'.$data['ar_tol'];
        }

        if (isset($data['ar_ig']) && $data['ar_ig'] ){
            $sql .= ' AND p.price <'.$data['ar_ig'];
        }

        if (isset($data['kivett']) && $data['kivett']) {
            $kivetel = implode(",",$data['kivett']);
            $sql .= " AND p.product_id NOT IN(".$kivetel.")";
        }

        $sql .= " GROUP BY p.product_id";
        $sql .= " ORDER BY pd.name";

        $query = $this->db->query($sql);

        return $query->num_rows ? $query->rows : false;

    }

    public function getProduct($product_id) {
        $sql = "SELECT  p.product_id, p.price, p.model, p.cikkszam, m.name as gyarto, pd.name  FROM " . DB_PREFIX . "product p
			    LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
			    LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id)";

        $sql .= " WHERE p.product_id='".(int)$product_id."'";
        $sql.= " AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
        $sql .= " GROUP BY p.product_id";

        $query = $this->db->query($sql);

        return $query->num_rows ? $query->row : false;

    }
		

}
?>