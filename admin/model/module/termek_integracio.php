<?php
class ModelModuleTermekIntegracio extends Model {

    public function addIntegracio($data) {
        $sql = "INSERT INTO " . DB_PREFIX . "termek_integracio SET
            `products`      = '".(!empty($data['levalogatott']) ? serialize($data['levalogatott']) : '')."',
            `connects`      = '".(!empty($data['connects']) ? serialize($data['connects']) : '')."',
            `name`	        = '".$this->db->escape($data['name'])."',
            `shipping`      = '".$this->db->escape($data['shipping'])."',
            `shipping_time_in_stock`    = '".$data['shipping_time_in_stock']."',
            `shipping_time_not_stock`   = '".$data['shipping_time_not_stock']."',
            `warranty`      = '".$data['warranty']."',
            `pieces`        = '".$data['pieces']."',
            `description`   = '".$data['description']."',
            `pickpack`      = '".$data['pickpack']."',
            `shipping_price`= '".$data['shipping_price']."',
            `status`        = '".$data['status']."'";
        $siker = $this->db->query($sql);
        $termek_integracio_id = $this->db->getLastId();
        if (!empty($data['connects'])) {
            $this->changeConnectsIntegrations($termek_integracio_id, $data['connects']);
        }
    }

    public function editIntegracio($termek_integracio_id,$data) {

        if (!empty($data['connects'])) {
            $this->changeConnectsIntegrations($termek_integracio_id, $data['connects']);
        }


        $sql = "UPDATE " . DB_PREFIX . "termek_integracio SET
                                    `products`      = '".(!empty($data['levalogatott']) ? serialize($data['levalogatott']) : '')."',
                                    `connects`      = '".(!empty($data['connects']) ? serialize($data['connects']) : '')."',
                                    `name`	        = '".$this->db->escape($data['name'])."',
                                    `shipping`      = '".$this->db->escape($data['shipping'])."',
                                    `shipping_time_in_stock`    = '".$data['shipping_time_in_stock']."',
                                    `shipping_time_not_stock`   = '".$data['shipping_time_not_stock']."',
                                    `warranty`      = '".$data['warranty']."',
                                    `pieces`        = '".$data['pieces']."',
                                    `description`   = '".$data['description']."',
                                    `pickpack`      = '".$data['pickpack']."',
                                    `shipping_price`= '".$data['shipping_price']."',
                                    `status`        = '".$data['status']."'
                    WHERE termek_integracio_id='".$termek_integracio_id."'";
        $siker = $this->db->query($sql);

    }

    public function changeConnectsIntegrations($termek_integracio_id,$connects) {
        $sql = "SELECT * FROM " . DB_PREFIX . "termek_integracio WHERE `connects` > '' AND termek_integracio_id != '".$termek_integracio_id."'";
        $query = $this->db->query($sql);
        if ($query->rows) {
            foreach($query->rows as $value) {
                $kapcsolatok = unserialize($value['connects']);
                $modosult = false;
                foreach($kapcsolatok as $key=>$kapcsolat) {
                    if (array_key_exists($key,$connects)) {
                        unset ($kapcsolatok[$key]);
                        $modosult = true;
                    }
                }
                if ($modosult) {
                    $sql = "UPDATE " . DB_PREFIX . "termek_integracio SET
                                    `connects` = '".(!empty($kapcsolatok) ? serialize($kapcsolatok) : '')."'
                                WHERE termek_integracio_id='".$value['termek_integracio_id']."'";
                    $siker = $this->db->query($sql);
                }
            }
        }

    }


    public function deleteIntegracio($termek_integracio_id) {
        $sql = "DELETE FROM " . DB_PREFIX . "termek_integracio WHERE termek_integracio_id='".$termek_integracio_id."'";
        $siker = $this->db->query($sql);

    }

    public function getTotalIntegrations($data) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "termek_integracio");
        return $query->row['total'];
    }

    public function getIntegrations($data) {
        $sql = "SELECT * FROM " . DB_PREFIX . "termek_integracio";
        $sort_data = array(
            'termek_integracio_id',
            'name',
            'status'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);
        foreach($query->rows as $key=>$value) {
            $query->rows[$key]['products'] = unserialize($value['products']);
            $query->rows[$key]['connects'] = unserialize($value['connects']);
        }
        return $query->rows;
    }

    public function getIntegration($termek_integracio_id) {
        $sql = "SELECT * FROM " . DB_PREFIX . "termek_integracio WHERE termek_integracio_id='".$termek_integracio_id."'";
        $query = $this->db->query($sql);
        $query->row['products'] = unserialize($query->row['products']);
        $query->row['connects'] = unserialize($query->row['connects']);
        if ($query->row['products']) {
            foreach($query->row['products'] as $key=>$product) {
                $sql = "SELECT `name` FROM " . DB_PREFIX . "product_description WHERE product_id='".$product."' AND language_id='".$this->config->get('config_language_id')."'";
                $query_name = $this->db->query($sql);
                $query->row['products'][$key] = array(
                    'product_id'    => $product,
                    'name'          => $query_name->row['name']
                );
            }
        }
        return $query->row;
    }




	public function getSzurtProducts($data) {
        $sql = "SELECT  p.product_id, p.price, p.model, p.cikkszam, m.name as gyarto, pd.name, p2c.category_id  FROM " . DB_PREFIX . "product p
			    INNER JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
			    LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id)
                LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";

        $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
        $sql .= " AND pd.name > ''";


        if ( !empty($data['ar_tol_ig'][0]) ){
            $sql .= ' AND p.price >='.$data['ar_tol_ig'][0];
        }
        if ( !empty($data['ar_tol_ig'][1]) ){
            $sql .= ' AND p.price <='.$data['ar_tol_ig'][1];
        }

        if (!empty($data['products']) ) {
            $kivetel = implode(",",$data['products']);
            $sql .= " AND p.product_id NOT IN(".$kivetel.")";
        }

        if (!empty($data['categoryes']) ) {
            $kivetel = implode(",",$data['categoryes']);
            $sql .= " AND p2c.category_id IN(".$kivetel.")";
        }

        if ($data['status'] == 1) {
            $sql .= " AND p.status = 1 ";
        } elseif (empty($data['status'])) {
            $sql .= " AND p.status = 0 ";
        }

        $sql .= " GROUP BY p.product_id";
        $sql .= " ORDER BY pd.name";

        $query = $this->db->query($sql);

        return $query->num_rows ? $query->rows : false;

    }


		

}
?>