<?php
class ModelSaleCustomerGroup extends Model {
	public function addCustomerGroup($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer_group SET name = '" . $this->db->escape($data['name']) . "', reach_value = '" . (float)($data['reach_value']) . "', reach_customer_group_id = '" . (int)($data['reach_customer_group_id']) . "', reach_order_status_id = '" . (int)($data['reach_order_status_id']) . "', admin_notify = '" . (isset($data['admin_notify']) ? (int)$data['admin_notify'] : 0) . "', customer_notify = '" . (isset($data['customer_notify']) ? (int)$data['customer_notify'] : 0) . "'");
	}
	
	public function editCustomerGroup($customer_group_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer_group SET name = '" . $this->db->escape($data['name']) . "', reach_value = '" . (float)($data['reach_value']) . "', reach_customer_group_id = '" . (int)($data['reach_customer_group_id']) . "', reach_order_status_id = '" . (int)($data['reach_order_status_id']) . "', admin_notify = '" . (isset($data['admin_notify']) ? (int)$data['admin_notify'] : 0) . "', customer_notify = '" . (isset($data['customer_notify']) ? (int)$data['customer_notify'] : 0) . "' WHERE customer_group_id = '" . (int)$customer_group_id . "'");
	}
	
	public function deleteCustomerGroup($customer_group_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_group WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE customer_group_id = '" . (int)$customer_group_id . "'");
	}
	
	public function getCustomerGroup($customer_group_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer_group WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		
		return $query->row;
	}
	
	public function getCustomerGroups($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "customer_group";
		
		$sql .= " ORDER BY name";	
			
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
			
		$query = $this->db->query($sql);
		
		return $query->rows;
	}

	public function getTotalCustomerGroups() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_group");
		
		return $query->row['total'];
	}

    public function getOrderStatus() {
        $query = $this->db->query("SELECT *  FROM " . DB_PREFIX . "order_status WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'
          ORDER BY name");

        return $query->rows;
    }
    public function getOrderStatusbyId($order_status_id) {
        $query = $this->db->query("SELECT name  FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row['name'];
    }
    public function checkColumn() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_group");
        if (!isset($query->row['reach_value'])) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer_group ADD `reach_value` DECIMAL(15,4) NOT NULL DEFAULT '0.0000'");
        }
        if (!isset($query->row['reach_customer_group_id'])) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer_group ADD `reach_customer_group_id` int(11) NOT NULL");
        }
        if (!isset($query->row['reach_order_status_id'])) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer_group ADD `reach_order_status_id` int(11) NOT NULL");
        }
        if (!isset($query->row['admin_notify'])) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer_group ADD `admin_notify` int(1) NOT NULL");
        }
        if (!isset($query->row['customer_notify'])) {
            $this->db->query("ALTER TABLE " . DB_PREFIX . "customer_group ADD `customer_notify` int(1) NOT NULL");
        }
    }
}
?>