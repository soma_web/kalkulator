<?php
class ModelSaleCustomer extends Model {
    public function addSap($customer_id,$megnevezes) {

        $data=$this->db->query("SELECT * FROM ".DB_PREFIX."customer WHERE customer_id='".(int)$customer_id."'");
        $cust_data=array();
        $cust_data=$data->row;
         $letrehoz="CREATE TABLE if not exists sapnak_vevok
        ( `vevokod`  text default null,
         `vevo_neve`  text default null,
         `vevo_idegen_neve`  text default null,
         `vevo_sap_csoportkod`  text default null,
         `fizetesi_mod`  text default null,
         `telefonszam`  text default null,
         `mail`  text default null,
         `adoszam`  text default null,
         `szall_iranyitoszam`  text default null,
         `szall_varos`  text default null,
         `szall_utca`  text default null,
         `szall_orszagkod`  text default null,
         `szaml_iranyitoszam`  text default null,
         `szaml_varos`  text default null,
         `szaml_utca`  text default null,
         `szaml_orszagkod`  text default null,
         `vevo_csoport`  text default null)   engine=MyISAM default charset=UTF8";
         $this->db->query($letrehoz);
        echo "";



        $vevocsoport=$this->getCustomerGroupName($cust_data['customer_group_id']);

        $cimek= $this->getAddresses($customer_id);
        foreach($cimek as $cim){
            if (!isset($szamlazasi)){
                $szamlazasi=array(
                    'szall_iranyitoszam'    =>$cim['postcode'],
                    'szall_varos'           =>$cim['city'],
                    'szall_utca'            =>$cim['address_1'],
                    'szall_orszagkod'       =>$cim['iso_code_2'],
                    'szall_cegnev'          =>$cim['company']
                );
            }
            else if(!isset($szallitasi)){
                $szallitasi=array(
                    'szaml_iranyitoszam'    =>$cim['postcode'],
                    'szaml_varos'           =>$cim['city'],
                    'szaml_utca'            =>$cim['address_1'],
                    'szaml_cegnev'          =>$cim['company']
                );
            }
        }
        if (!isset($szallitasi)){
            $szallitasi=array(
                'szall_iranyitoszam'    =>'',
                'szall_varos'           =>'',
                'szall_utca'            =>'',
                'szall_orszagkod'       =>'',
                'szall_cegnev'       =>''
            );
        }
        if(!isset($szamlazasi)){
            $szamlazasi=array(
                'szaml_iranyitoszam'    =>'',
                'szaml_varos'           =>'',
                'szaml_utca'            =>'',
                'szaml_orszagkod'       =>'',
                'szaml_cegnev'       =>''
            );
        }


           $this->db->query("INSERT INTO sapnak_vevok SET
                           `vevokod`               =    '".$cust_data['vevokod']."',
                           `vevo_neve`             =    '".$cust_data['firstname']." ".$cust_data['lastname']."',
                           `fizetesi_mod`          =    '".$megnevezes."',
                           `telefonszam`           =    '".$cust_data['telephone']."',
                           `mail`                  =    '".$cust_data['email']."',
                           `adoszam`               =    '".$cust_data['adoszam']."',
                           `szall_iranyitoszam`    =    '".$szallitasi['szall_iranyitoszam']."',
                           `szall_varos`           =    '".$szallitasi['szall_varos']."',
                           `szall_utca`            =    '".$szallitasi['szall_utca']."',
                           `szall_orszagkod`       =    '".$szallitasi['szall_orszagkod']."',
                           `szaml_iranyitoszam`    =    '".$szamlazasi['szaml_iranyitoszam']."',
                           `szaml_varos`           =    '".$szamlazasi['szaml_varos']."',
                           `szaml_utca`            =    '".$szamlazasi['szaml_utca']."',
                           `szaml_orszagkod`       =    '".$szamlazasi['szaml_orszagkod']."',
                           `vevo_csoport`          =    '".$vevocsoport."'");

    echo "";

    }

    public function addCustomer($data) {

        $nem     = isset($data['nem']) ? $this->db->escape($data['nem']) : "";
        $eletkor = isset($data['eletkor']) ? $this->db->escape($data['eletkor']) : "";
        $feltolto = isset($data['feltolto']) ? $this->db->escape($data['feltolto']) : "0";
        $iskolai_vegzettseg = isset($data['iskolai_vegzettseg']) ? $this->db->escape($data['iskolai_vegzettseg']) : "";
        $vallalkozasi_forma = isset($data['vallalkozasi_forma']) ? $this->db->escape($data['vallalkozasi_forma']) : "";
        $ugyvezeto_neve = isset($data['ugyvezeto_neve']) ? $this->db->escape($data['ugyvezeto_neve']) : "";
        $ugyvezeto_telefonszama = isset($data['ugyvezeto_telefonszama']) ? $this->db->escape($data['ugyvezeto_telefonszama']) : "";
        $szekhely = isset($data['szekhely']) ? $this->db->escape($data['szekhely']) : "";
        $partner_kod = isset($data['partner_kod']) ? $data['partner_kod'] : "";
        $weblap = isset($data['weblap']) ? $this->db->escape($data['weblap']) : "";

        $this->db->query("INSERT INTO " . DB_PREFIX . "customer SET
            firstname           = '" . $this->db->escape($data['firstname']) . "',
      	    lastname            = '" . $this->db->escape($data['lastname']) . "',
      	    email               = '" . $this->db->escape($data['email']) . "',
      	    telephone           = '" . $this->db->escape($data['telephone']) . "',
      	    fax                 = '" . $this->db->escape($data['fax']) . "',
      	    newsletter          = '" . (int)$data['newsletter'] . "',
      	    szazalek            = '" . $data['szazalek'] . "',
      	    nem                 = '" . $nem . "',
      	    eletkor             = '" . $eletkor . "',
      	    customer_group_id   = '" . (int)$data['customer_group_id'] . "',
      	    password            = '" . $this->db->escape(md5($data['password'])) . "',
      	    status              = '" . (int)$data['status'] . "',
      	    adoszam             = '" . (int)$data['adoszam'] . "',
      	    feltolto            = '" . (int)$feltolto . "',
      	    iskolai_vegzettseg  = '" . $iskolai_vegzettseg . "',
      	    vallalkozasi_forma  = '" . $vallalkozasi_forma . "',
      	    ugyvezeto_neve      = '" . $ugyvezeto_neve . "',
      	    ugyvezeto_telefonszama  = '" . $ugyvezeto_telefonszama. "',
      	    szekhely            = '" . $szekhely . "',
      	    partner_kod         = '" . $partner_kod . "',
      	    weblap              = '" . $weblap . "',
		    company             = '" . (isset($data['company']) ? $this->db->escape($data['company']) : '') . "',
        	date_added          = NOW()");
      	
      	$customer_id = $this->db->getLastId();

        $this->db->query("UPDATE " . DB_PREFIX . "customer SET
            vevokod = 'W" .$customer_id. "'
		    WHERE customer_id = '" . (int)$customer_id . "'");



        if (isset($data['address'])) {
      		foreach ($data['address'] as $address) {	
      			$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($address['firstname']) . "', lastname = '" . $this->db->escape($address['lastname']) . "', company = '" . $this->db->escape($address['company']) . "', address_1 = '" . $this->db->escape($address['address_1']) . "', address_2 = '" . $this->db->escape($address['address_2']) . "', city = '" . $this->db->escape($address['city']) . "', postcode = '" . $this->db->escape($address['postcode']) . "', country_id = '" . (int)$address['country_id'] . "', zone_id = '" . (int)$address['zone_id'] . "'");
				
				if (isset($address['default'])) {
					$address_id = $this->db->getLastId();
					
					$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . $address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
				}
			}
		}

        if (isset($data['product_vevo'])) {
            foreach ($data['product_vevo'] as $address) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "customer_extension SET
                    customer_extension_id = '',
                    customer_id = '" . (int)$customer_id . "',
                    type = 'payment',
                    code = '" . $this->db->escape($address['fizetesi_mod']) . "',
                    value = '" .$address['engedelyezve'] . "'
                    ");
            }

        }

        if(isset($data['newsletter']) && $data['newsletter'] == '1' && isset($data['newslettercategories']) && is_array($data['newslettercategories'])) {
            $this->load->model('sale/newslettercategories');
            $this->model_sale_newslettercategories->setCategoriesToCustomer($customer_id, $data['newslettercategories']);
        }
	}

	public function editCustomer($customer_id, $data) {
        $nem     = isset($data['nem']) ? $this->db->escape($data['nem']) : "";
        $eletkor = isset($data['eletkor']) ? $this->db->escape($data['eletkor']) : "";
        $feltolto = isset($data['feltolto']) ? $this->db->escape($data['feltolto']) : "0";
        $iskolai_vegzettseg = isset($data['iskolai_vegzettseg']) ? $this->db->escape($data['iskolai_vegzettseg']) : "";

        $vallalkozasi_forma = isset($data['vallalkozasi_forma']) ? $this->db->escape($data['vallalkozasi_forma']) : "";
        $ugyvezeto_neve = isset($data['ugyvezeto_neve']) ? $this->db->escape($data['ugyvezeto_neve']) : "";
        $ugyvezeto_telefonszama = isset($data['ugyvezeto_telefonszama']) ? $this->db->escape($data['ugyvezeto_telefonszama']) : "";
        $szekhely = isset($data['szekhely']) ? $this->db->escape($data['szekhely']) : "";
        $partner_kod = isset($data['partner_kod']) ? $data['partner_kod'] : "";
        $weblap = isset($data['weblap']) ? $this->db->escape($data['weblap']) : "";

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET
		    firstname           = '" . $this->db->escape($data['firstname']) . "',
		    lastname            = '" . $this->db->escape($data['lastname']) . "',
		    email               = '" . $this->db->escape($data['email']) . "',
		    telephone           = '" . $this->db->escape($data['telephone']) . "',
		    fax                 = '" . $this->db->escape($data['fax']) . "',
		    newsletter          = '" . (int)$data['newsletter'] . "',
		    szazalek            = '" . $data['szazalek'] . "',
		    nem                 = '" . $nem . "',
		    eletkor             = '" . $eletkor . "',
		    adoszam             = '" . $data['adoszam'] . "',
		    customer_group_id   = '" . (int)$data['customer_group_id'] . "',
		    status              = '" . (int)$data['status'] . "',
		    feltolto            = '" . (int)$feltolto . "',
		    iskolai_vegzettseg  = '" . $iskolai_vegzettseg . "',
		    vallalkozasi_forma  = '" . $vallalkozasi_forma . "',
		    ugyvezeto_neve      = '" . $ugyvezeto_neve . "',
		    ugyvezeto_telefonszama  = '" . $ugyvezeto_telefonszama . "',
		    partner_kod            = '" . $partner_kod . "',
      	    weblap              = '" . $weblap . "',
		    company             = '" . (isset($data['company']) ? $this->db->escape($data['company']) : '') . "',
            szekhely            = '" . $szekhely . "'
		        WHERE customer_id = '" . (int)$customer_id . "'");
	
      	if ($data['password']) {
        	$this->db->query("UPDATE " . DB_PREFIX . "customer SET password = '" . $this->db->escape(md5($data['password'])) . "' WHERE customer_id = '" . (int)$customer_id . "'");
      	}
      	
      	$this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");
      	
      	if (isset($data['address'])) {
      		foreach ($data['address'] as $address) {
                $vallalkozasi_forma = isset($address['vallalkozasi_forma']) ? $this->db->escape($address['vallalkozasi_forma']) : "";
                $ugyvezeto_neve = isset($address['ugyvezeto_neve']) ? $this->db->escape($address['ugyvezeto_neve']) : "";
                $ugyvezeto_telefonszama = isset($address['ugyvezeto_telefonszama']) ? $this->db->escape($address['ugyvezeto_telefonszama']) : "";
                $szekhely = isset($address['szekhely']) ? $this->db->escape($address['szekhely']) : "";
				if ($address['address_id']) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "address SET
					        address_id                  = '" . $this->db->escape($address['address_id']) . "',
					        customer_id                 = '" . (int)$customer_id . "',
					        firstname                   = '" . $this->db->escape($address['firstname']) . "',
					        lastname                    = '" . $this->db->escape($address['lastname']) . "',
					        company                     = '" . $this->db->escape($address['company']) . "',
					        vallalkozasi_forma          = '" . $vallalkozasi_forma . "',
		                    ugyvezeto_neve              = '" . $ugyvezeto_neve . "',
		                    ugyvezeto_telefonszama      = '" . $ugyvezeto_telefonszama . "',
		                    szekhely                    = '" . $szekhely . "',
					        adoszam                     = '" . $this->db->escape($address['adoszam']) . "',
					        address_1                   = '" . $this->db->escape($address['address_1']) . "',
					        address_2                   = '" . $this->db->escape($address['address_2']) . "',
					        city                        = '" . $this->db->escape($address['city']) . "',
					        postcode                    = '" . $this->db->escape($address['postcode']) . "',
					        country_id                  = '" . (int)$address['country_id'] . "',
					        zone_id                     = '" . (int)$address['zone_id'] . "'");
					
					if (isset($address['default'])) {
						$this->db->query("UPDATE " . DB_PREFIX . "customer SET
						        address_id = '" . (int)$address['address_id'] . "' WHERE customer_id = '" . (int)$customer_id . "'");
					}
				} else {
					$this->db->query("INSERT INTO " . DB_PREFIX . "address SET
					        customer_id                 = '" . (int)$customer_id . "',
					        firstname                   = '" . $this->db->escape($address['firstname']) . "',
					        lastname                    = '" . $this->db->escape($address['lastname']) . "',
					        company                     = '" . $this->db->escape($address['company']) . "',
					        vallalkozasi_forma          = '" . $vallalkozasi_forma . "',
		                    ugyvezeto_neve              = '" . $ugyvezeto_neve . "',
		                    ugyvezeto_telefonszama      = '" . $ugyvezeto_telefonszama . "',
		                    szekhely                    = '" . $szekhely . "',
					        adoszam                     = '" . $this->db->escape($address['adoszam']) . "',
					        address_1                   = '" . $this->db->escape($address['address_1']) . "',
					        address_2                   = '" . $this->db->escape($address['address_2']) . "',
					        city                        = '" . $this->db->escape($address['city']) . "',
					        postcode                    = '" . $this->db->escape($address['postcode']) . "',
					        country_id                  = '" . (int)$address['country_id'] . "',
					        zone_id                     = '" . (int)$address['zone_id'] . "'");
					
					if (isset($address['default'])) {
						$address_id = $this->db->getLastId();
						
						$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
					}
				}
			}
		}

        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_extension WHERE customer_id = '" . (int)$customer_id . "'");
        if (isset($data['product_vevo'])) {
            foreach ($data['product_vevo'] as $address) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "customer_extension SET
                    customer_extension_id = '',
                    customer_id = '" . (int)$customer_id . "',
                    type = 'payment',
                    code = '" . $this->db->escape($address['fizetesi_mod']) . "',
                    value = '" .$address['engedelyezve'] . "'
                    ");
            }

        }
        if($data['newsletter'] != '1') {
            $data['newslettercategories'] = array();
        }

        if(isset($data['newslettercategories']) && is_array($data['newslettercategories'])) {
            $this->load->model('sale/newslettercategories');
            $this->model_sale_newslettercategories->setCategoriesToCustomer($customer_id, $data['newslettercategories']);
        }
	}

	public function editToken($customer_id, $token) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET token = '" . $this->db->escape($token) . "' WHERE customer_id = '" . (int)$customer_id . "'");
	}
	
	public function deleteCustomer($customer_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . (int)$customer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_extension WHERE customer_id = '" . (int)$customer_id . "'");

        $this->load->model('sale/newslettercategories');
        $this->model_sale_newslettercategories->deleteAllCategoryFromCustomer($customer_id);
	}

    public function deleteAddress($address_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "'");

    }
	
	public function getCustomer($customer_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");
	
		return $query->row;
	}
	
	public function getCustomerByEmail($email) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE email = '" . $this->db->escape($email) . "'");
	
		return $query->row;
	}

    public function getCustomersByIDArray($idArray) {
        if(is_array($idArray) && count($idArray) > 0) {
            $sql = "SELECT *, CONCAT(c.firstname, ' ', c.lastname) AS name, cg.name AS customer_group FROM " . DB_PREFIX . "customer c
        		LEFT JOIN " . DB_PREFIX . "customer_group cg ON (c.customer_group_id = cg.customer_group_id) WHERE c.customer_id IN (".implode(',', $idArray).")";

            $query = $this->db->query($sql);

            return $query->rows;
        } else {
            return array();
        }
    }

	public function getCustomers($data = array()) {

        if (!empty($data['filter_feltolto'])) {
            $sql = "SELECT CONCAT(c.firstname, ' ', c.lastname) AS name, c.customer_id, a.company, cg.name AS customer_group FROM " . DB_PREFIX . "customer c
		            LEFT JOIN " . DB_PREFIX . "customer_group cg ON (c.customer_group_id = cg.customer_group_id)
                    LEFT JOIN " . DB_PREFIX ."address a ON (c.address_id = a.address_id)";

        } else {
            $sql = "SELECT *, CONCAT(c.firstname, ' ', c.lastname) AS name, cg.name AS customer_group FROM " . DB_PREFIX . "customer c
        		LEFT JOIN " . DB_PREFIX . "customer_group cg ON (c.customer_group_id = cg.customer_group_id)";
        }

            $implode = array();
		
		if (!empty($data['filter_name'])) {
			$implode[] = "LCASE(CONCAT(c.firstname, ' ', c.lastname)) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}
		
		if (!empty($data['filter_email'])) {
			$implode[] = "LCASE(c.email) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "%'";
		}

		if (isset($data['filter_newsletter']) && !is_null($data['filter_newsletter'])) {
			$implode[] = "c.newsletter = '" . (int)$data['filter_newsletter'] . "'";
		}	
				
		if (!empty($data['filter_customer_group_id'])) {
			$implode[] = "cg.customer_group_id = '" . (int)$data['filter_customer_group_id'] . "'";
		}	
			
		if (!empty($data['filter_ip'])) {
			$implode[] = "c.customer_id IN (SELECT customer_id FROM " . DB_PREFIX . "customer_ip WHERE ip = '" . $this->db->escape($data['filter_ip']) . "')";
		}	
				
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$implode[] = "c.status = '" . (int)$data['filter_status'] . "'";
		}	
		
		if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
			$implode[] = "c.approved = '" . (int)$data['filter_approved'] . "'";
		}

        if (isset($data['filter_atkuldve']) && !is_null($data['filter_atkuldve'])) {
            $implode[] = "c.atkuldve = '" . (int)$data['filter_atkuldve'] . "'";
        }

        if (!empty($data['filter_date_added'])) {
			$implode[] = "DATE(c.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

        if (!empty($data['filter_feltolto'])) {
            $implode[] = "c.feltolto = '" . (int)$data['filter_feltolto'] . "'";
        }
        if (isset($data['filter_partner_kod']) && !is_null($data['filter_partner_kod'])) {
            $implode[] = "c.partner_kod = '" . (int)$data['filter_partner_kod'] . "'";
        }
        if (isset($data['filter_company']) && !is_null($data['filter_company'])) {
            $implode[] = "LCASE(c.company) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_company'])) . "%'";
        }
        if (isset($data['filter_szazalek']) && !is_null($data['filter_szazalek'])) {
            $implode[] = "c.szazalek = '" . (int)$data['filter_szazalek'] . "'";
        }

		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}
		
		$sort_data = array(
			'name',
			'c.email',
			'customer_group',
			'c.status',
			'c.approved',
			'c.ip',
			'c.date_added',
			'c.atkuldve',
			'c.partner_kod',
			'c.company',
			'c.szazalek'
		);
			
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}
			
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}		
		
		$query = $this->db->query($sql);

		return $query->rows;	
	}
	
	public function editFizetesiMod($customer_extension_id,$ertek) {
        $this->db->query("UPDATE " . DB_PREFIX . "customer_extension SET value = '".(int)$ertek."' WHERE customer_extension_id = '" . (int)$customer_extension_id . "'");
echo "";
    }


	public function approve($customer_id) {
		$customer_info = $this->getCustomer($customer_id);

		if ($customer_info) {
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET approved = '1' WHERE customer_id = '" . (int)$customer_id . "'");

			$this->load->language('mail/customer');
			
			$this->load->model('setting/store');
						
			$store_info = $this->model_setting_store->getStore($customer_info['store_id']);
			
			if ($store_info) {
				$store_name = $store_info['name'];
				$store_url = $store_info['url'] . 'index.php?route=account/login';
			} else {
				$store_name = $this->config->get('config_name');
				$store_url = HTTP_CATALOG . 'index.php?route=account/login';
			}
	
			$message  = sprintf($this->language->get('text_approve_welcome'), $store_name) . "\n\n";
			$message .= $this->language->get('text_approve_login') . "\n";
			$message .= $store_url . "\n\n";
			$message .= $this->language->get('text_approve_services') . "\n\n";
			$message .= $this->language->get('text_approve_thanks') . "\n";
			$message .= $store_name;
	
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');							
			$mail->setTo($customer_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($store_name);
			//$mail->setSubject(html_entity_decode(sprintf($this->language->get('text_approve_subject'), $store_name), ENT_QUOTES, 'UTF-8'));

            $mail->setSubject(sprintf($this->language->get('text_approve_subject'), $store_name));

            $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();
		}		
	}
		
	public function getAddress($address_id) {
		$address_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "'");

		if ($address_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");
			
			if ($country_query->num_rows) {
				$country = $country_query->row['name'];
				$iso_code_2 = $country_query->row['iso_code_2'];
				$iso_code_3 = $country_query->row['iso_code_3'];
				$address_format = $country_query->row['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';	
				$address_format = '';
			}
			
			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");
			
			if ($zone_query->num_rows) {
				$zone = $zone_query->row['name'];
				$zone_code = $zone_query->row['code'];
			} else {
				$zone = '';
				$zone_code = '';
			}		
		
			return array(
				'address_id'                => $address_query->row['address_id'],
				'customer_id'               => $address_query->row['customer_id'],
				'firstname'                 => $address_query->row['firstname'],
				'lastname'                  => $address_query->row['lastname'],
				'company'                   => $address_query->row['company'],
                'vallalkozasi_forma'        => $address_query->row['vallalkozasi_forma'],
                'szekhely'                  => $address_query->row['szekhely'],
                'ugyvezeto_neve'            => $address_query->row['ugyvezeto_neve'],
                'ugyvezeto_telefonszama'    => $address_query->row['ugyvezeto_telefonszama'],
				'adoszam'                   => $address_query->row['adoszam'],
				'address_1'                 => $address_query->row['address_1'],
				'address_2'                 => $address_query->row['address_2'],
				'postcode'                  => $address_query->row['postcode'],
				'city'                      => $address_query->row['city'],
				'zone_id'                   => $address_query->row['zone_id'],
				'zone'                      => $zone,
				'zone_code'                 => $zone_code,
				'country_id'                => $address_query->row['country_id'],
				'country'                   => $country,
				'iso_code_2'                => $iso_code_2,
				'iso_code_3'                => $iso_code_3,
				'address_format'            => $address_format
			);
		}
	}
		
	public function getAddresses($customer_id) {
		$address_data = array();
		
		$query = $this->db->query("SELECT address_id FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");
	
		foreach ($query->rows as $result) {
			$address_info = $this->getAddress($result['address_id']);
		
			if ($address_info) {
				$address_data[] = $address_info;
			}
		}		
		
		return $address_data;
	}	
				
	public function getTotalCustomers($data = array()) {
      	$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer";
		
		$implode = array();
		
		if (!empty($data['filter_name'])) {
			$implode[] = "LCASE(CONCAT(firstname, ' ', lastname)) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}
		
		if (!empty($data['filter_email'])) {
			$implode[] = "LCASE(email) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "%'";
		}
		
		if (isset($data['filter_newsletter']) && !is_null($data['filter_newsletter'])) {
			$implode[] = "newsletter = '" . (int)$data['filter_newsletter'] . "'";
		}
				
		if (!empty($data['filter_customer_group_id'])) {
			$implode[] = "customer_group_id = '" . (int)$data['filter_customer_group_id'] . "'";
		}	
		
		if (!empty($data['filter_ip'])) {
			$implode[] = "customer_id IN (SELECT customer_id FROM " . DB_PREFIX . "customer_ip WHERE ip = '" . $this->db->escape($data['filter_ip']) . "')";
		}	
						
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$implode[] = "status = '" . (int)$data['filter_status'] . "'";
		}			
		
		if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
			$implode[] = "approved = '" . (int)$data['filter_approved'] . "'";
		}		
				
		if (!empty($data['filter_date_added'])) {
			$implode[] = "DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

        if (isset($data['filter_partner_kod']) && !is_null($data['filter_partner_kod'])) {
            $implode[] = "partner_kod = '" . (int)$data['filter_partner_kod'] . "'";
        }
        if (isset($data['filter_company']) && !is_null($data['filter_company'])) {
            $implode[] = "LCASE(company) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_company'])) . "%'";
        }
        if (isset($data['filter_szazalek']) && !is_null($data['filter_szazalek'])) {
            $implode[] = "szazalek = '" . (int)$data['filter_szazalek'] . "'";
        }
		
		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}
				
		$query = $this->db->query($sql);
				
		return $query->row['total'];
	}
		
	public function getTotalCustomersAwaitingApproval() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE status = '0' OR approved = '0'");

		return $query->row['total'];
	}
	
	public function getTotalAddressesByCustomerId($customer_id) {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");
		
		return $query->row['total'];
	}
	
	public function getTotalAddressesByCountryId($country_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE country_id = '" . (int)$country_id . "'");
		
		return $query->row['total'];
	}	
	
	public function getTotalAddressesByZoneId($zone_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE zone_id = '" . (int)$zone_id . "'");
		
		return $query->row['total'];
	}
	
	public function getTotalCustomersByCustomerGroupId($customer_group_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		
		return $query->row['total'];
	}
			
	public function addTransaction($customer_id, $description = '', $amount = '', $order_id = 0) {
		$customer_info = $this->getCustomer($customer_id);
		
		if ($customer_info) { 
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_transaction SET customer_id = '" . (int)$customer_id . "', order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($description) . "', amount = '" . (float)$amount . "', date_added = NOW()");

			$this->language->load('mail/customer');
			
			if ($customer_info['store_id']) {
				$this->load->model('setting/store');
		
				$store_info = $this->model_setting_store->getStore($customer_info['store_id']);
				
				if ($store_info) {
					$store_name = $store_info['store_name'];
				} else {
					$store_name = $this->config->get('config_name');
				}	
			} else {
				$store_name = $this->config->get('config_name');
			}	
						
			$message  = sprintf($this->language->get('text_transaction_received'), $this->currency->format($amount, $this->config->get('config_currency'))) . "\n\n";
			$message .= sprintf($this->language->get('text_transaction_total'), $this->currency->format($this->getTransactionTotal($customer_id), $this->config->get('config_currency')));
								
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');
			$mail->setTo($customer_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($store_name);
			//$mail->setSubject(html_entity_decode(sprintf($this->language->get('text_transaction_subject'), $this->config->get('config_name')), ENT_QUOTES, 'UTF-8'));

            $mail->setSubject(sprintf($this->language->get('text_transaction_subject'), $this->config->get('config_name')));

            $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();
		}
	}
	
	public function deleteTransaction($order_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");
	}
	
	public function getTransactions($customer_id, $start = 0, $limit = 10) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "' ORDER BY date_added DESC LIMIT " . (int)$start . "," . (int)$limit);
	
		return $query->rows;
	}

	public function getTotalTransactions($customer_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total  FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "'");
	
		return $query->row['total'];
	}
			
	public function getTransactionTotal($customer_id) {
		$query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "'");
	
		return $query->row['total'];
	}
	
	public function getTotalTransactionsByOrderId($order_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");
	
		return $query->row['total'];
	}	
				
	public function addReward($customer_id, $description = '', $points = '', $order_id = 0) {
		$customer_info = $this->getCustomer($customer_id);
			
		if ($customer_info) { 
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_reward SET customer_id = '" . (int)$customer_id . "', order_id = '" . (int)$order_id . "', points = '" . (int)$points . "', description = '" . $this->db->escape($description) . "', date_added = NOW()");

			$this->language->load('mail/customer');
			
			if ($order_id) {
				$this->load->model('sale/order');
		
				$order_info = $this->model_sale_order->getOrder($order_id);
				
				if ($order_info) {
					$store_name = $order_info['store_name'];
				} else {
					$store_name = $this->config->get('config_name');
				}	
			} else {
				$store_name = $this->config->get('config_name');
			}		
				
			$message  = sprintf($this->language->get('text_reward_received'), $points) . "\n\n";
			$message .= sprintf($this->language->get('text_reward_total'), $this->getRewardTotal($customer_id));
				
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');
			$mail->setTo($customer_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($store_name);
			//$mail->setSubject(html_entity_decode(sprintf($this->language->get('text_reward_subject'), $store_name), ENT_QUOTES, 'UTF-8'));

            $mail->setSubject(sprintf($this->language->get('text_reward_subject'), $store_name));

            $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();
		}
	}

	public function deleteReward($order_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int)$order_id . "'");
	}
	
	public function getRewards($customer_id, $start = 0, $limit = 10) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "' ORDER BY date_added DESC LIMIT " . (int)$start . "," . (int)$limit);
	
		return $query->rows;
	}
	
	public function getTotalRewards($customer_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");
	
		return $query->row['total'];
	}
			
	public function getRewardTotal($customer_id) {
		$query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");
	
		return $query->row['total'];
	}		
	
	public function getTotalCustomerRewardsByOrderId($order_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int)$order_id . "'");
	
		return $query->row['total'];
	}
	
	public function getIpsByCustomerId($customer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->rows;
	}	
	
	public function getTotalCustomersByIp($ip) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_ip WHERE ip = '" . $this->db->escape($ip) . "'");

		return $query->row['total'];
	}
	
	public function addBlacklist($ip) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "customer_ip_blacklist` SET `ip` = '" . $this->db->escape($ip) . "'");
	}
		
	public function deleteBlacklist($ip) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "customer_ip_blacklist` WHERE `ip` = '" . $this->db->escape($ip) . "'");
	}
			
	public function getTotalBlacklistsByIp($ip) {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "customer_ip_blacklist` WHERE `ip` = '" . $this->db->escape($ip) . "'");
				 
		return $query->row['total'];
	}
    public function getNewsletterSubscribers() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "subscribe");

        return $query->rows;
    }

    function getExtensionsCustomer($type) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_extension WHERE `customer_id` = '" . $type . "'");

        return $query->rows;
    }
    function getExtensions($type) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "extension WHERE `type` = '" . $type . "'");

        return $query->rows;
    }
    function getCustomerGroupName($customer_group_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_group WHERE `customer_group_id` = '" . $customer_group_id . "'");

        return $query->row['name'];
    }
}
?>