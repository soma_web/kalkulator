<?php
class ModelSalePreOrder extends Model {
    public function addPreOrder($data) {

        $sql = "INSERT INTO " . DB_PREFIX . "pre_order SET

		    `product_id`	    =   '".$data['product_id']."',
            `model`           	=   '".$data['model']."',
            `cikkszam`          = 	'".$data['cikkszam']."',
            `product_name`      =   '".$data['product_name']."',
            `gyarto`           	=   '".$data['gyarto']."',
            `megrendelo_name`   =   '".$data['megrendelo_name']."',
            `megrendelo_email`  =   '".$data['megrendelo_email']."',
            `megrendelo_telefon`=   '".$data['megrendelo_telefon']."',
            `megrendelo_uzenet` =   '".$data['megrendelo_uzenet']."',
            `sajat_megjegyzes`  =   '".$data['sajat_megjegyzes']."',

            `megrendelo_iranyitoszam`='".$data['megrendelo_iranyitoszam']."',
            `megrendelo_varos`      =   '".$data['megrendelo_varos']."',
            `megrendelo_utca`       =   '".$data['megrendelo_utca']."',
            `megrendelo_ceg`        =   '".$data['megrendelo_ceg']."',
            `megrendelo_adoszam`    =   '".$data['megrendelo_adoszam']."',

            `date_added`        =   curdate(),
            `date_modified`     =   curdate()";


        $this->db->query($sql);

        return true;
    }

    public function editPreOrder($pre_order_id, $data) {

        $sql = "UPDATE  " . DB_PREFIX . "pre_order SET


		    `product_id`	    =   '".$data['product_id']."',
            `model`           	=   '".$data['model']."',
            `cikkszam`          = 	'".$data['cikkszam']."',
            `product_name`      =   '".$data['product_name']."',
            `gyarto`           	=   '".$data['gyarto']."',
            `megrendelo_name`   =   '".$data['megrendelo_name']."',
            `megrendelo_email`  =   '".$data['megrendelo_email']."',
            `megrendelo_uzenet` =   '".$data['megrendelo_uzenet']."',
            `megrendelo_telefon` =   '".$data['megrendelo_telefon']."',

            `megrendelo_iranyitoszam`='".$data['megrendelo_iranyitoszam']."',
            `megrendelo_varos`      =   '".$data['megrendelo_varos']."',
            `megrendelo_utca`       =   '".$data['megrendelo_utca']."',
            `megrendelo_ceg`        =   '".$data['megrendelo_ceg']."',
            `megrendelo_adoszam`    =   '".$data['megrendelo_adoszam']."',

            `sajat_megjegyzes`  =   '".$data['sajat_megjegyzes']."',


            `date_modified`     =   curdate()
            WHERE pre_order_id='".$pre_order_id."'";


        $this->db->query($sql);

        return true;
    }

    public function deletePreOrder($pre_order_id) {


        $this->db->query("DELETE FROM `" . DB_PREFIX . "pre_order` WHERE pre_order_id = '" . (int)$pre_order_id . "'");
    }

    public function getPreOrder($pre_order_id) {

        $sql = "SELECT * FROM `" . DB_PREFIX . "pre_order` o WHERE o.pre_order_id = '" . (int)$pre_order_id . "'";

        $query = $this->db->query($sql);

        if ($query->num_rows) {

            return $query->row;
        } else {
            return false;
        }
    }

    public function getPreOrders($data = array()) {

        $sql = "SELECT * FROM `" . DB_PREFIX . "pre_order` o WHERE 1 ";


        $sql .= !empty($data['filter_pre_order_id'])    ? " AND o.pre_order_id = '" . (int)$data['filter_pre_order_id'] . "'" : '';
        $sql .= !empty($data['filter_product_name'])    ? " AND LCASE(o.product_name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_product_name'])) . "%'"    : '';
        $sql .= !empty($data['filter_model'])           ? " AND LCASE(o.model) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_model'])) . "%'"  : '';
        $sql .= !empty($data['filter_customer'])        ? " AND LCASE(o.megrendelo_name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_customer'])) . "%'" : '';
        $sql .= !empty($data['filter_email'])           ? " AND LCASE(o.megrendelo_email) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "%'" : '';
        $sql .= !empty($data['filter_date_added'])      ? " AND DATE(o.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')" : '';
        $sql .= !empty($data['filter_date_modified'])   ? " AND DATE(o.date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')" : '';



        $sort_data = array(
            'o.pre_order_id',
            'o.megrendelo_name',
            'o.megrendelo_email',
            'o.date_added',
            'o.date_modified'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY o.pre_order_id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }



    public function getTotalPreOrders($data = array()) {
        $sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "pre_order`o  WHERE 1 ";

        $sql .= !empty($data['filter_pre_order_id'])    ? " AND o.pre_order_id = '" . (int)$data['filter_pre_order_id'] . "'" : '';
        $sql .= !empty($data['filter_product_name'])    ? " AND LCASE(o.product_name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_product_name'])) . "%'"    : '';
        $sql .= !empty($data['filter_model'])           ? " AND LCASE(o.model) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_model'])) . "%'"  : '';
        $sql .= !empty($data['filter_customer'])        ? " AND LCASE(o.megrendelo_name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_customer'])) . "%'" : '';
        $sql .= !empty($data['filter_email'])           ? " AND LCASE(o.megrendelo_email) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "%'" : '';
        $sql .= !empty($data['filter_date_added'])      ? " AND DATE(o.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')" : '';
        $sql .= !empty($data['filter_date_modified'])   ? " AND DATE(o.date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')" : '';

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getPreOrderProducts($pre_order_id) {

        $sql = "SELECT * FROM `" . DB_PREFIX . "pre_order` o
            LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = o.product_id)
		    WHERE pre_order_id = '" . (int)$pre_order_id . "'";



        $query = $this->db->query($sql);


        return $query->rows;
    }

}
?>