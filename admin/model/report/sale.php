<?php
class ModelReportSale extends Model {
	public function getOrders($data = array()) {
		$sql = "SELECT  MIN(tmp.date_added) AS date_start,
		                MAX(tmp.date_added) AS date_end,
		                COUNT(tmp.order_id) AS `orders`,
		                SUM(tmp.products) AS products,
		                SUM(tmp.tax) AS tax,
		                SUM(tmp.total) AS total FROM (SELECT o.order_id, (SELECT SUM(op.quantity) FROM `" . DB_PREFIX . "order_product` op WHERE op.order_id = o.order_id GROUP BY op.order_id) AS products,
		                (SELECT SUM(ot.value) FROM `" . DB_PREFIX . "order_total` ot WHERE ot.order_id = o.order_id AND ot.code = 'tax' GROUP BY ot.order_id) AS tax, o.total, o.date_added
		                FROM `" . DB_PREFIX . "order` o";

        if ($data['filter_ingyenes'] != "") {
            $sql .= " INNER JOIN `" . DB_PREFIX . "order_product` o_p ON (o.order_id = o_p.order_id)";
        }

		if (!empty($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}
		
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

        if (!empty($data['filter_korosztaly'])) {
            $sql .= " AND o.eletkor = '" . (int)$data['filter_korosztaly'] . "'";
        }

        if (!empty($data['filter_nem'])) {
            $sql .= " AND o.nem = '" . (int)$data['filter_nem'] . "'";
        }

        if ($data['filter_ingyenes'] != "") {
            $sql .= " AND o_p.ingyenes = '" . (int)$data['filter_ingyenes'] . "'";
        }

        if (!empty($data['filter_iskolai_vegzettseg'])) {
            $sql .= " AND o.iskolai_vegzettseg = '" . $this->db->escape($data['filter_iskolai_vegzettseg']) . "'";
        }
		
		$sql .= " GROUP BY o.order_id) tmp";
		
		if (!empty($data['filter_group'])) {
			$group = $data['filter_group'];
		} else {
			$group = 'week';
		}
		
		switch($group) {
			case 'day';
				$sql .= " GROUP BY DAY(tmp.date_added)";
				break;
			default:
			case 'week':
				$sql .= " GROUP BY WEEK(tmp.date_added)";
				break;	
			case 'month':
				$sql .= " GROUP BY MONTH(tmp.date_added)";
				break;
			case 'year':
				$sql .= " GROUP BY YEAR(tmp.date_added)";
				break;									
		}
		
		$sql .= " ORDER BY tmp.date_added DESC";
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		
		$query = $this->db->query($sql);
		
		return $query->rows;
	}	
	
	public function getTotalOrders($data = array()) {
		if (!empty($data['filter_group'])) {
			$group = $data['filter_group'];
		} else {
			$group = 'week';
		}
		
		switch($group) {
			case 'day';
				$sql = "SELECT COUNT(DISTINCT DAY(date_added)) AS total FROM `" . DB_PREFIX . "order`";
				break;
			default:
			case 'week':
				$sql = "SELECT COUNT(DISTINCT WEEK(date_added)) AS total FROM `" . DB_PREFIX . "order`";
				break;	
			case 'month':
				$sql = "SELECT COUNT(DISTINCT MONTH(date_added)) AS total FROM `" . DB_PREFIX . "order`";
				break;
			case 'year':
				$sql = "SELECT COUNT(DISTINCT YEAR(date_added)) AS total FROM `" . DB_PREFIX . "order`";
				break;									
		}

        if ($data['filter_ingyenes'] != "") {
            $sql .= " LEFT JOIN `" . DB_PREFIX . "order_product` o_p ON (".DB_PREFIX."order.order_id = o_p.order_id)";
        }
		
		if (!empty($data['filter_order_status_id'])) {
			$sql .= " WHERE order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE order_status_id > '0'";
		}
				
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

        if (!empty($data['filter_korosztaly'])) {
            $sql .= " AND eletkor = '" . (int)$data['filter_korosztaly'] . "'";
        }

        if (!empty($data['filter_nem'])) {
            $sql .= " AND nem = '" . (int)$data['filter_nem'] . "'";
        }

        if (!empty($data['filter_iskolai_vegzettseg'])) {
            $sql .= " AND iskolai_vegzettseg = '" . $this->db->escape($data['filter_iskolai_vegzettseg']) . "'";
        }

        if ($data['filter_ingyenes'] != "") {
            $sql .= " AND o_p.ingyenes = '" . (int)$data['filter_ingyenes'] . "'";
        }

		$query = $this->db->query($sql);

		return $query->row['total'];	
	}
	
	public function getTaxes($data = array()) {
		$sql = "SELECT MIN(o.date_added) AS date_start, MAX(o.date_added) AS date_end, ot.title, SUM(ot.value) AS total, COUNT(o.order_id) AS `orders` FROM `" . DB_PREFIX . "order_total` ot LEFT JOIN `" . DB_PREFIX . "order` o ON (ot.order_id = o.order_id) WHERE ot.code = 'tax'"; 

		if (!empty($data['filter_order_status_id'])) {
			$sql .= " AND o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " AND o.order_status_id > '0'";
		}
		
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		
		if (!empty($data['filter_group'])) {
			$group = $data['filter_group'];
		} else {
			$group = 'week';
		}
		
		switch($group) {
			case 'day';
				$sql .= " GROUP BY ot.title, DAY(o.date_added)";
				break;
			default:
			case 'week':
				$sql .= " GROUP BY ot.title, WEEK(o.date_added)";
				break;	
			case 'month':
				$sql .= " GROUP BY ot.title, MONTH(o.date_added)";
				break;
			case 'year':
				$sql .= " GROUP BY ot.title, YEAR(o.date_added)";
				break;									
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		
		$query = $this->db->query($sql);
		
		return $query->rows;
	}	
	
	public function getTotalTaxes($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM (SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order_total` ot LEFT JOIN `" . DB_PREFIX . "order` o ON (ot.order_id = o.order_id) WHERE ot.code = 'tax'";
		
		if (!is_null($data['filter_order_status_id'])) {
			$sql .= " AND order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " AND order_status_id > '0'";
		}
				
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		
		if (!empty($data['filter_group'])) {
			$group = $data['filter_group'];
		} else {
			$group = 'week';
		}
		
		switch($group) {
			case 'day';
				$sql .= " GROUP BY DAY(o.date_added), ot.title";
				break;
			default:
			case 'week':
				$sql .= " GROUP BY WEEK(o.date_added), ot.title";
				break;	
			case 'month':
				$sql .= " GROUP BY MONTH(o.date_added), ot.title";
				break;
			case 'year':
				$sql .= " GROUP BY YEAR(o.date_added), ot.title";
				break;									
		}
		
		$sql .= ") tmp";
		
		$query = $this->db->query($sql);

		return $query->row['total'];	
	}	
	
	public function getShipping($data = array()) {
		$sql = "SELECT MIN(o.date_added) AS date_start, MAX(o.date_added) AS date_end, ot.title, SUM(ot.value) AS total, COUNT(o.order_id) AS `orders` FROM `" . DB_PREFIX . "order_total` ot LEFT JOIN `" . DB_PREFIX . "order` o ON (ot.order_id = o.order_id) WHERE ot.code = 'shipping'"; 

		if (!is_null($data['filter_order_status_id'])) {
			$sql .= " AND o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " AND o.order_status_id > '0'";
		}
		
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		
		if (!empty($data['filter_group'])) {
			$group = $data['filter_group'];
		} else {
			$group = 'week';
		}
		
		switch($group) {
			case 'day';
				$sql .= " GROUP BY ot.title, DAY(o.date_added)";
				break;
			default:
			case 'week':
				$sql .= " GROUP BY ot.title, WEEK(o.date_added)";
				break;	
			case 'month':
				$sql .= " GROUP BY ot.title, MONTH(o.date_added)";
				break;
			case 'year':
				$sql .= " GROUP BY ot.title, YEAR(o.date_added)";
				break;									
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		
		$query = $this->db->query($sql);
		
		return $query->rows;
	}	
	
	public function getTotalShipping($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM (SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order_total` ot LEFT JOIN `" . DB_PREFIX . "order` o ON (ot.order_id = o.order_id) WHERE ot.code = 'shipping'";
		
		if (!is_null($data['filter_order_status_id'])) {
			$sql .= " AND order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " AND order_status_id > '0'";
		}
				
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		
		if (!empty($data['filter_group'])) {
			$group = $data['filter_group'];
		} else {
			$group = 'week';
		}
		
		switch($group) {
			case 'day';
				$sql .= " GROUP BY DAY(o.date_added), ot.title";
				break;
			default:
			case 'week':
				$sql .= " GROUP BY WEEK(o.date_added), ot.title";
				break;	
			case 'month':
				$sql .= " GROUP BY MONTH(o.date_added), ot.title";
				break;
			case 'year':
				$sql .= " GROUP BY YEAR(o.date_added), ot.title";
				break;									
		}
		
		$sql .= ") tmp";
		
		$query = $this->db->query($sql);

		return $query->row['total'];	
	}

    public function getCategoryOrders($queryFilter) {
        $sql = "SELECT cade.category_id, cade.name, COUNT(prtoc.category_id) AS darab, pr.utalvany AS ingyenes FROM ".DB_PREFIX."order_product AS opr
                  INNER JOIN ".DB_PREFIX."product_to_category AS prtoc ON opr.product_id=prtoc.product_id
                  INNER JOIN ".DB_PREFIX."category AS ca ON prtoc.category_id=ca.category_id
                  INNER JOIN ".DB_PREFIX."category_description AS cade ON ca.category_id=cade.category_id
                  INNER JOIN ".DB_PREFIX."product AS pr ON opr.product_id=pr.product_id
                WHERE cade.language_id=".$this->config->get('config_language_id');
        if(array_key_exists('filter_categories', $queryFilter) && isset($queryFilter['filter_categories'])) {
            $sql .= " AND cade.name LIKE '%".$queryFilter['filter_categories']."%'";
        }

        $sql .= " GROUP BY ca.category_id,pr.utalvany";

        $query = $this->db->query($sql);
        $total = $query->num_rows;

        if(array_key_exists('start', $queryFilter) && array_key_exists('limit', $queryFilter) && is_numeric($queryFilter['start']) && is_numeric($queryFilter['limit'])) {
            $sql .= " LIMIT ".$queryFilter['start'].",".$queryFilter['limit'];
        }

        $query = $this->db->query($sql);

        $categories = array();
        $ingyenesek = array();
        $fizetosek = array();
        foreach($query->rows as $row) {
            $category_id = $row['category_id'];
            if(!array_key_exists($category_id, $categories)) {
                $categories[$category_id] = array(
                    'id'            => $category_id,
                    'name'          => $row['name']
                );
            }
            $ingyenes = $row['ingyenes'];
            if($ingyenes == 0 || $ingyenes == null) {
                if(array_key_exists($category_id, $fizetosek)) {
                    $fizetosek[$category_id] += (int)$row['darab'];
                } else {
                    $fizetosek[$category_id] = (int)$row['darab'];
                }
            } else {
                if(array_key_exists($category_id, $ingyenesek)) {
                    $ingyenesek[$category_id] += (int)$row['darab'];
                } else {
                    $ingyenesek[$category_id] = (int)$row['darab'];
                }
            }
        }
        return array(
            'categories'            => $categories,
            'ingyenesek'            => $ingyenesek,
            'fizetosek'             => $fizetosek,
            'total'                 => $total
        );
    }

    public function getProductsByCategory() {
        $sql = "SELECT prca.category_id, COUNT(pr.product_id) AS darab, pr.utalvany FROM ".DB_PREFIX."product AS pr
                  INNER JOIN ".DB_PREFIX."product_to_category prca ON pr.product_id=prca.product_id
                GROUP BY prca.category_id,pr.utalvany";
        $query = $this->db->query($sql);

        $ingyenes = array();
        $fizetos = array();
        foreach($query->rows as $row) {
            $category_id = $row['category_id'];
            $utalvany = $row['utalvany'];
            if($utalvany == 0 || $utalvany == null) {
                if(array_key_exists($category_id, $fizetos)) {
                    $fizetos[$category_id] += (int)$row['darab'];
                } else {
                    $fizetos[$category_id] = (int)$row['darab'];
                }
            } else {
                $ingyenes[$category_id] = (int)$row['darab'];
            }
        }

        return array(
            'fizetos'           => $fizetos,
            'ingyenes'          => $ingyenes
        );
    }

    public function getUploaders($queryFilter) {
        $sql = "SELECT c.customer_id, c.firstname, c.lastname, COUNT(product_id) AS feltoltott FROM ".DB_PREFIX."customer AS c
                    INNER JOIN ".DB_PREFIX."product_customer AS pc ON c.customer_id=pc.customer_id
                    WHERE feltolto=1 GROUP BY pc.customer_id ORDER BY feltoltott DESC";

        $query = $this->db->query($sql);
        $total = $query->num_rows;

        if(array_key_exists('start', $queryFilter) && array_key_exists('limit', $queryFilter)) {
            $sql .=  " LIMIT ".$queryFilter['start'].",".$queryFilter['limit'].";";
            $query = $this->db->query($sql);
        }

        $return = array();
        $return['total'] = $total;
        if(!is_array($query->rows)) {
            $return['uploaders'] = array();
            return $return;
        }

        $rows = array();
        foreach($query->rows as $row) {
            $rows[$row['customer_id']] = $row;
        }
        $return['uploaders'] = $rows;

        return $return;
    }

    public function getSelledProductsByUploaders() {
        $sql = "SELECT pc.customer_id, COUNT(pc.product_id) AS vasaroltak FROM ".DB_PREFIX."order AS o
                  INNER JOIN ".DB_PREFIX."order_product AS op ON o.order_id=op.order_id
                  INNER JOIN ".DB_PREFIX."product_customer AS pc ON op.product_id=pc.product_id
                GROUP BY pc.customer_id;";

        $query = $this->db->query($sql);

        if(!is_array($query->rows)) {
            return array();
        }

        $rows = array();
        foreach($query->rows as $row) {
            $rows[$row['customer_id']] = $row['vasaroltak'];
        }

        return $rows;
    }
}
?>