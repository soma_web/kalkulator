<?php
class ModelToolImage extends Model {
	public function resize($filename, $width, $height, $watermark=true) {
		if (!file_exists(DIR_IMAGE . $filename) || !is_file(DIR_IMAGE . $filename)) {
			return;
		}

        if ($this->config->get('megjelenit_vizjel') == 0){
            $watermark = false;
        }
		
		$info = pathinfo($filename);
		$extension = $info['extension'];
        if($extension == 'pdf') {
            $extension = "jpg";
        }
		
		$old_image = $filename;
		$new_image = 'cache/' . utf8_substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;
		
		if (!file_exists(DIR_IMAGE . $new_image) || (filemtime(DIR_IMAGE . $old_image) > filemtime(DIR_IMAGE . $new_image))) {
			$path = '';
			
			$directories = explode('/', dirname(str_replace('../', '', $new_image)));
			
			foreach ($directories as $directory) {
				$path = $path . '/' . $directory;
				
				if (!file_exists(DIR_IMAGE . $path)) {
					@mkdir(DIR_IMAGE . $path, 0777);
				}		
			}

			$image = new Image(DIR_IMAGE . $old_image);

            $image->resize($width, $height);
            $image->save(DIR_IMAGE . $new_image);
		}
	
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			return HTTPS_IMAGE . $new_image;
		} else {
			return HTTP_IMAGE . $new_image;
		}	
	}

    public function imageLinkMove($old_link,$new_link){

        $prepare_image=explode("/",$old_link);
        $count=count($prepare_image);
        $image=$prepare_image[$count-1];
        $link=$new_link."/".$image;

        $sql = "UPDATE " . DB_PREFIX . "product SET image = '".$link."' WHERE image LIKE '" . $old_link . "'";
        $query = $this->db->query($sql);
        $teszt=$query;

        $sql = "UPDATE " . DB_PREFIX . "product_image SET image = '".$link."' WHERE image LIKE '" . $old_link . "'";
        $query = $this->db->query($sql);
        $teszt=$query;


        $sql = "UPDATE " . DB_PREFIX . "banner_image SET image = '".$link."' WHERE image LIKE '" . $old_link . "'";
        $query = $this->db->query($sql);
        $teszt=$query;


        $sql = "UPDATE " . DB_PREFIX . "category SET image = '".$link."' WHERE image LIKE '" . $old_link . "'";
        $query = $this->db->query($sql);
        $teszt=$query;


        $sql = "UPDATE " . DB_PREFIX . "category SET image_icon = '".$link."' WHERE image_icon LIKE '" . $old_link . "'";
        $query = $this->db->query($sql);
        $teszt=$query;









    }
}
?>