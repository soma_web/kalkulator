<?php
class ModelCatalogElhelyezkedesAlcsoport extends Model {

    public function getAlcsoportElhelyezkedesek() {

        $vissza = array();
        $elhelyezkedesek = $this->config->get("elhelyezkedes_alcsoport");

        if ($elhelyezkedesek) {
            foreach ($elhelyezkedesek as $value) {
                if ($value['status'] == 1) {
                    $vissza[] = $value;
                }
            }
        }
        $vissza = $this->config->rendezes($vissza,"sort_order");
        return $vissza;
    }
}
?>