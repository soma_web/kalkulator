<?php 
class ModelCatalogOptionSzinGroup extends Model {
	public function addOptionSzinGroup($data) {


        $sql = "INSERT INTO " . DB_PREFIX . "option_szin_group SET
		            sort_order  = '" . (int)$data['sort_order'] . "',
		            szinkod     = '" . $data['szinkod'] . "'";

            $this->db->query($sql);
            $option_szin_group_id = $this->db->getLastId();

        foreach ($data['option_szin_group_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "option_szin_group_description SET
                        `option_szin_group_id`    = '" . (int)$option_szin_group_id . "',
                        `language_id`       = '" . $language_id . "',
                        `name`              = '" . $value['name'] . "'");
        }

    }


	public function editOptionSzinGroup($option_szin_group_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "option_szin_group SET
		    sort_order  = '" . (int)$data['sort_order'] . "',
		    szinkod     = '" . $data['szinkod'] . "'
		    WHERE option_szin_group_id = '" . (int)$option_szin_group_id . "'");


        $this->db->query("DELETE FROM " . DB_PREFIX . "option_szin_group_description WHERE option_szin_group_id = '" . (int)$option_szin_group_id . "'");

        foreach ($data['option_szin_group_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "option_szin_group_description SET
                        `option_szin_group_id`    = '" . (int)$option_szin_group_id . "',
                        `language_id`       = '" . $language_id . "',
                        `name`              = '" . $value['name'] . "'");
        }


	}
	
	public function deleteOptionSzinGroup($option_szin_group_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "option_szin_group WHERE option_szin_group_id = '" . (int)$option_szin_group_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "option_szin_group_description WHERE option_szin_group_id = '" . (int)$option_szin_group_id . "'");
	}
		
	public function getOptionSzinGroup($option_szin_group_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_szin_group WHERE option_szin_group_id = '" . (int)$option_szin_group_id . "'");
		
		return $query->row;
	}
		
	public function getOptionSzinGroups($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "option_szin_group oszg
		        LEFT JOIN " .DB_PREFIX. "option_szin_group_description oszgd ON (oszg.option_szin_group_id=oszgd.option_szin_group_id)
		        WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'";
			
		$sort_data = array(
			'name',
			'sort_order',
			'szinkod'
		);
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";
		}	
			
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
		
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getOptionSzinGroupDescriptions($option_szin_group_id) {
		$option_szin_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_szin_group_description WHERE option_szin_group_id = '" . (int)$option_szin_group_id . "'");
		
		foreach ($query->rows as $result) {
            $option_szin_data[$result['language_id']] = array('name' => $result['name']);
		}
		
		return $option_szin_data;
	}
	
	public function getTotalOptionSzinGroups() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "option_szin_group");
		
		return $query->row['total'];
	}	
}
?>