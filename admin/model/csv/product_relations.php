<?php
class ModelCsvProductRelations extends Model {

    private $ertek=array();
    private $id;
    private $nyelvek=array();
    private $store;

    public function getFields($table) {

        $this->load->model("setting/store");
        $stores = $this->model_setting_store->getStores();


        $product = array();

        $product[] = DB_PREFIX."product.product_id";
        $product[] = DB_PREFIX."product.model";
        $product[] = DB_PREFIX."product.cikkszam";
        $product[] = DB_PREFIX."product.cikkszam2";
        $product[] = DB_PREFIX."product_description.name";
        if ($stores) $product[] = DB_PREFIX."product_to_store.store_id";

        $product[] = DB_PREFIX."product_to_category.Kategoriak";  //product_id, category_id
        $product[] = DB_PREFIX."product_accessory.Tartozekok";   //product_id, accessory_id
        $product[] = DB_PREFIX."product_general_accessory.Altalanos tartozekok";   //product_id, general_accessory_id
        $product[] = DB_PREFIX."product_ajandek.Ajandekok";   //product_id, ajandek_id
        $product[] = DB_PREFIX."product_related.Kacsolodo termekek";   //product_id, related_id
        $product[] = DB_PREFIX."product_attribute.Jellemzok";   //product_id, attribute_id, language_id, text  !!!!  még csinálni a text-nek
        $product[] = DB_PREFIX."product_helyettesito.Helyettesito termekek";   //product_id, helyettesito_id
        $product[] = DB_PREFIX."product_image.Kepek";   //product_id, product_image_id, image, sort_order
        $product[] = DB_PREFIX."product_package.Csomag ajanlat";   //product_id, package_id, quantity

        //$product[] = DB_PREFIX."product_pdf.PDF";   //product_id, pdf_id, sort_order, path, pdf_title
        //$product[] = DB_PREFIX."product_video.Videok";   //product_id, video_id, video_url, sort_order, video_title
        //$product[] = DB_PREFIX."product_warehouse.Raktarak";   //product_id, warehouse_id, quantity

        return $product;
    }

    public function backup($tabla="product_relations") {
        date_default_timezone_set('Europe/Budapest');
        $filename = $tabla.'_'.date('Y-m-d_H:i:s', time()).'.csv';

        $this->load->model("setting/store");
        $stores = $this->model_setting_store->getStores();

        $this->load->model("localisation/language");
        $nyelvek = $this->model_localisation_language->getLanguages();

        $fejlec = array();
        $product_fields = array();
        foreach($this->request->post['backup'] as $mezo) {
            if ($mezo == DB_PREFIX."product.product_id") $product_fields[] = "p.product_id";
            if ($mezo == DB_PREFIX."product.model") $product_fields[] = "p.model";
            if ($mezo == DB_PREFIX."product.cikkszam") $product_fields[] = "p.cikkszam";
            if ($mezo == DB_PREFIX."product.cikkszam2") $product_fields[] = "p.cikkszam2";
            if ($mezo == DB_PREFIX."product_description.name") $product_fields[] = "pd.name";
            if ($mezo == DB_PREFIX."product_to_store.store_id") $product_fields[] = "p2s.store_id";
            $fejlec[] = substr(strstr($mezo,'.'),1);
            /*if ($mezo == DB_PREFIX."product_attribute.Jellemzok") {
                foreach($nyelvek as $nyelv) {
                    $fejlec[]   ='Jellemzok-'.$nyelv['name'];
                }
            }*/
        }
        $sql = "SELECT ";
        $sql .= implode(',',$product_fields);
        $sql .= " FROM ".DB_PREFIX."product p";
        if (in_array("pd.name",$product_fields)) {
            $sql .= " LEFT JOIN ".DB_PREFIX."product_description pd ON (pd.product_id=p.product_id AND pd.language_id=".$this->config->get('config_language_id').")";
        }
        if ($stores) {
            $sql .= " LEFT JOIN ".DB_PREFIX."product_to_store p2s ON (p2s.product_id=p.product_id) ";
        }
        $sql .= " WHERE 1 ";


        if (!empty($this->request->post['filter'])) {
            foreach($this->request->post['filter'] as $filter_key=>$value) {
                $mezo = explode('-',$filter_key);
                $present = substr(strstr($mezo[0],".",0),1);

                if ($mezo[1] == "tol" && $value){
                    if (in_array($present,$fejlec)) {
                        $sql .=" AND substring(lcase(".$present."),1,".strlen($value).") >= lcase('".$value."')";
                    }
                } elseif ($mezo[1] == "ig" && $value ){
                    if (in_array($present,$fejlec)) {
                        $sql .=" AND substring(lcase(".$present."),1,".strlen($value).") <= lcase('".$value."')";
                    }

                } elseif ($mezo[1] == "select" && $value != "no" ){
                    if (in_array($present,$fejlec)) {
                        $sql .=" AND ".$present." = ".$value;
                    }
                }
            }
        }

        $order = isset($this->request->post['sorrend']) ? substr(strstr($this->request->post['sorrend'],".",0),1) : 'product_id';
        $sql .= " ORDER BY ".$order;

        $sql .= " LIMIT ".(int)$this->request->post['limit_tol'].",";
        $sql .= $this->request->post['limit_db'] ? $this->request->post['limit_db'] : 1844674407370955161;
        $query = $this->db->query($sql);

        if ($query->num_rows > 0) {

            foreach($query->rows as $key=>$value) {
                foreach($this->request->post['backup'] as $mezo) {
                    if ($mezo == DB_PREFIX."product_to_category.Kategoriak"){
                        $sql = "SELECT * FROM ".DB_PREFIX."product_to_category WHERE product_id='".$value['product_id']."'";
                        $sorok = $this->db->query($sql);
                        $query->rows[$key]['category'] = $sorok;
                    }
                    if ($mezo == DB_PREFIX."product_accessory.Tartozekok"){
                        $sql = "SELECT * FROM ".DB_PREFIX."product_accessory WHERE product_id='".$value['product_id']."'";
                        $sorok = $this->db->query($sql);
                        $query->rows[$key]['accessory'] = $sorok;
                    }
                    if ($mezo == DB_PREFIX."product_general_accessory.Altalanos tartozekok"){
                        $sql = "SELECT * FROM ".DB_PREFIX."product_general_accessory WHERE product_id='".$value['product_id']."'";
                        $sorok = $this->db->query($sql);
                        $query->rows[$key]['general_accessory'] = $sorok;
                    }
                    if ($mezo == DB_PREFIX."product_ajandek.Ajandekok"){
                        $sql = "SELECT * FROM ".DB_PREFIX."product_ajandek WHERE product_id='".$value['product_id']."'";
                        $sorok = $this->db->query($sql);
                        $query->rows[$key]['ajandek'] = $sorok;
                    }
                    if ($mezo == DB_PREFIX."product_attribute.Jellemzok"){
                        $sql = "SELECT * FROM ".DB_PREFIX."product_attribute WHERE product_id='".$value['product_id']."'";
                        $sorok = $this->db->query($sql);
                        $query->rows[$key]['attribute'] = $sorok;
                    }
                    if ($mezo == DB_PREFIX."product_helyettesito.Helyettesito termekek"){
                        $sql = "SELECT * FROM ".DB_PREFIX."product_helyettesito WHERE product_id='".$value['product_id']."'";
                        $sorok = $this->db->query($sql);
                        $query->rows[$key]['helyettesito'] = $sorok;
                    }
                    if ($mezo == DB_PREFIX."product_image.Kepek"){
                        $sql = "SELECT * FROM ".DB_PREFIX."product_image WHERE product_id='".$value['product_id']."'";
                        $sorok = $this->db->query($sql);
                        $query->rows[$key]['image'] = $sorok;
                    }
                    if ($mezo == DB_PREFIX."product_package.Csomag ajanlat"){
                        $sql = "SELECT * FROM ".DB_PREFIX."product_package WHERE product_id='".$value['product_id']."'";
                        $sorok = $this->db->query($sql);
                        $query->rows[$key]['package'] = $sorok;
                    }
                    if ($mezo == DB_PREFIX."product_pdf.PDF"){
                        $sql = "SELECT * FROM ".DB_PREFIX."product_pdf WHERE product_id='".$value['product_id']."'";
                        $sorok = $this->db->query($sql);
                        $query->rows[$key]['pdf'] = $sorok;
                    }
                    if ($mezo == DB_PREFIX."product_related.Kacsolodo termekek"){
                        $sql = "SELECT * FROM ".DB_PREFIX."product_related WHERE product_id='".$value['product_id']."'";
                        $sorok = $this->db->query($sql);
                        $query->rows[$key]['related'] = $sorok;
                    }
                    if ($mezo == DB_PREFIX."product_video.Videok"){
                        $sql = "SELECT * FROM ".DB_PREFIX."product_video WHERE product_id='".$value['product_id']."'";
                        $sorok = $this->db->query($sql);
                        $query->rows[$key]['video'] = $sorok;
                    }
                    if ($mezo == DB_PREFIX."product_warehouse.Raktarak"){
                        $sql = "SELECT * FROM ".DB_PREFIX."product_warehouse WHERE product_id='".$value['product_id']."'";
                        $sorok = $this->db->query($sql);
                        $query->rows[$key]['warehouse'] = $sorok;
                    }

                }
            }

            $handle = fopen('../csv/'.$filename, "w");
            fputcsv ( $handle, $fejlec, ";", '"');

            foreach($query->rows as $fields) {
                $sor = array();
                foreach($fields as $key=>$value) {
                    if (!is_object($value)) {
                        $sor[] = $value;

                    } else {
                        if ($key == "category") {
                            $gyujto = array();
                            if ($value->num_rows > 0) {
                                foreach($value->rows as $ertek) {
                                    $gyujto[] =  $ertek['category_id'];
                                }
                            }
                            $sor[] = implode(',',$gyujto);
                        }

                        if ($key == "accessory") {
                            $gyujto = array();
                            if ($value->num_rows > 0) {
                                foreach($value->rows as $ertek) {
                                    $gyujto[] =  $ertek['accessory_id'];
                                }
                            }
                            $sor[] = implode(',',$gyujto);
                        }

                        if ($key == "general_accessory") {
                            $gyujto = array();
                            if ($value->num_rows > 0) {
                                foreach($value->rows as $ertek) {
                                    $gyujto[] =  $ertek['general_accessory_id'];
                                }
                            }
                            $sor[] = implode(',',$gyujto);
                        }

                        if ($key == "ajandek") {
                            $gyujto = array();
                            if ($value->num_rows > 0) {
                                foreach($value->rows as $ertek) {
                                    $gyujto[] =  $ertek['ajandek_id'];
                                }
                            }
                            $sor[] = implode(',',$gyujto);
                        }

                        if ($key == "attribute") {
                            $gyujto = array();
                            $szoveg = array();
                            if ($value->num_rows > 0) {
                                foreach($value->rows as $ertek) {
                                    $gyujto[$ertek['attribute_id']] =  $ertek['attribute_id'];
                                    //$szoveg[$ertek['language_id']] = $ertek['text'];
                                }
                            }
                            $sor[] = implode(',',$gyujto);
                            /*foreach($nyelvek as $language_id=>$nyelv) {
                                $sor[] = !empty($szoveg[$language_id]) ? $szoveg[$language_id] : '';
                            }*/
                        }

                        if ($key == "helyettesito") {
                            $gyujto = array();
                            if ($value->num_rows > 0) {
                                foreach($value->rows as $ertek) {
                                    $gyujto[] =  $ertek['helyettesito_id'];
                                }
                            }
                            $sor[] = implode(',',$gyujto);
                        }

                        if ($key == "image") {
                            $gyujto = array();
                            if ($value->num_rows > 0) {
                                foreach($value->rows as $ertek) {
                                    $gyujto[$ertek['image']] =  $ertek['image'];
                                }
                            }
                            $sor[] = implode(',',$gyujto);
                        }

                        if ($key == "package") {
                            $gyujto = array();
                            if ($value->num_rows > 0) {
                                foreach($value->rows as $ertek) {
                                    $gyujto[] =  $ertek['package_id'].'&&'.$ertek['quantity'];
                                }
                            }
                            $sor[] = implode(',',$gyujto);
                        }

                        if ($key == "pdf") {
                            $gyujto = array();
                            if ($value->num_rows > 0) {
                                foreach($value->rows as $ertek) {
                                    $gyujto[] =  str_ireplace(array("\r","\n","\t",'"','','','','\\"'),'', $ertek['path']);
                                    //$gyujto[] =  $ertek['path'];
                                }
                            }
                            $sor[] = implode(',',$gyujto);
                        }

                        if ($key == "video") {
                            $gyujto = array();
                            if ($value->num_rows > 0) {
                                foreach($value->rows as $ertek) {
                                    $gyujto[] = str_ireplace(array("\r","\n","\t",'"','','','','\\"'),'', $ertek['video_url']);
                                    //$gyujto[] =  $ertek['video_url'];
                                }
                            }
                            $sor[] = implode(',',$gyujto);
                        }

                        if ($key == "related") {
                            $gyujto = array();
                            if ($value->num_rows > 0) {
                                foreach($value->rows as $ertek) {
                                    $gyujto[] =  $ertek['related_id'];
                                }
                            }
                            $sor[] = implode(',',$gyujto);
                        }

                        if ($key == "warehouse") {
                            $gyujto = array();
                            if ($value->num_rows > 0) {
                                foreach($value->rows as $ertek) {
                                    $gyujto[] =  $ertek['warehouse_id'].'&&'.$ertek['quantity'];
                                }
                            }
                            $sor[] = implode(',',$gyujto);
                        }
                    }
                }
                fputcsv ( $handle, $sor, ";", '"');
            }
            fclose($handle);

        }
        return '../csv/'.$filename;
    }



    public function restore($csv) {

        require_once(DIR_SYSTEM."library/Encoding.php");
        $encode = new Encoding();
        $csv = $encode->toUTF8($csv);

        $this->load->model("localisation/language");
        $this->nyelvek = $this->model_localisation_language->getLanguages();

        $sorok = str_getcsv($csv, "\n",'"','\\');
        $fejlec = str_getcsv($sorok[0], ";",'"','\\');

        $this->tabla = strstr($fejlec[0],".",1);

        if (!$this->formatumEllenorzes($fejlec)) return false;


        $this->load->model("catalog/product");

        $i = 0;
        $sorok_szama = 0;

        foreach($sorok as $sor) {
            if ($i == 0) {
                $i++;
                continue;
            }
            if (!empty($sor)) {
                $this->ertek = str_getcsv($sor, ";",'"','\\');
                $this->letezesVizsgalat($fejlec);
                $i++;
            }
            $sorok_szama++;
        }

        return $sorok_szama;
    }

    public function letezesVizsgalat($fejlec) {

        $kulcs_megvan = false;
        if ($fejlec_kulcs = array_keys($fejlec,"product_id")) { if ($this->ertek[$fejlec_kulcs[0]]) {$fejlec_kulcs = $fejlec_kulcs[0]; $kulcs_megvan = true;}}
        if (!$kulcs_megvan){
            if ($fejlec_kulcs = array_keys($fejlec,"model")) { if ($this->ertek[$fejlec_kulcs[0]]) {$fejlec_kulcs = $fejlec_kulcs[0]; $kulcs_megvan = true;}}
        }
        if (!$kulcs_megvan){
            if ($fejlec_kulcs = array_keys($fejlec,"cikkszam")) { if ($this->ertek[$fejlec_kulcs[0]]) {$fejlec_kulcs = $fejlec_kulcs[0]; $kulcs_megvan = true;}}
        }
        if (!$kulcs_megvan){
            if ($fejlec_kulcs = array_keys($fejlec,"cikkszam2")) { if ($this->ertek[$fejlec_kulcs[0]]) {$fejlec_kulcs = $fejlec_kulcs[0]; $kulcs_megvan = true;}}
        }

        $store_id = 0;
        if ($store_id_key = array_keys($fejlec,"store_id")) {
            $store_id = $this->ertek[$store_id_key[0]];
        }


        if (!$this->ertek[$fejlec_kulcs]) {
            $sql = "INSERT INTO ".DB_PREFIX."product SET model=''";
            $num_rows = 0;
        } else {
            $sql = "SELECT product_id FROM ".DB_PREFIX."product WHERE ".$fejlec[$fejlec_kulcs]."='".$this->ertek[$fejlec_kulcs]."'";
            $query = $this->db->query($sql);
            $num_rows = $query->num_rows;
            if ($num_rows == 0) {
                $sql = "INSERT INTO ".DB_PREFIX."product SET ".$fejlec[$fejlec_kulcs]."='".$this->ertek[$fejlec_kulcs]."'";
            }
        }
        if ($num_rows == 0) {
            $query = $this->db->query($sql);
            $product_id = $this->db->getLastId($sql);

            $sql = "INSERT IGNORE INTO ".DB_PREFIX."product_to_store SET product_id=".$product_id.", store_id='".$store_id."'";
            $query = $this->db->query($sql);

        } else {
            $product_id = $query->row['product_id'];
        }

        if ($fejlec_kulcs = array_keys($fejlec,"Kategoriak")) {
            $gyujto = explode(',',$this->ertek[$fejlec_kulcs[0]]);
            $sql = "DELETE FROM ".DB_PREFIX."product_to_category WHERE product_id='".$product_id."'";
            $query = $this->db->query($sql);
            if ($gyujto[0]) {
                foreach($gyujto as $id) {
                    $sql = "INSERT IGNORE INTO ".DB_PREFIX."category SET category_id='".$id."'";
                    $query = $this->db->query($sql);

                    $sql = "DELETE FROM ".DB_PREFIX."category_to_store WHERE category_id='".$id."'";
                    $query = $this->db->query($sql);
                    $sql = "INSERT IGNORE INTO ".DB_PREFIX."category_to_store SET category_id=".$id.", store_id='".$store_id."'";
                    $query = $this->db->query($sql);


                    $sql = "INSERT INTO ".DB_PREFIX."product_to_category SET category_id='".$id."', product_id='".$product_id."'";
                    $query = $this->db->query($sql);
                }

            }
        }


        if ($fejlec_kulcs = array_keys($fejlec,"Tartozekok")) {
            $gyujto = explode(',',$this->ertek[$fejlec_kulcs[0]]);
            $sql = "DELETE FROM ".DB_PREFIX."product_accessory WHERE product_id='".$product_id."'";
            $query = $this->db->query($sql);
            if ($gyujto[0]) {
                foreach($gyujto as $id) {
                    $sql = "INSERT IGNORE INTO ".DB_PREFIX."product SET product_id='".$id."'";
                    $query = $this->db->query($sql);

                    $sql = "INSERT IGNORE INTO ".DB_PREFIX."product_to_store SET product_id=".$id.", store_id='".$store_id."'";
                    $query = $this->db->query($sql);

                    $sql = "INSERT INTO ".DB_PREFIX."product_accessory SET accessory_id='".$id."', product_id='".$product_id."'";
                    $query = $this->db->query($sql);
                }

            }
        }

        if ($fejlec_kulcs = array_keys($fejlec,"Altalanos tartozekok")) {
            $gyujto = explode(',',$this->ertek[$fejlec_kulcs[0]]);
            $sql = "DELETE FROM ".DB_PREFIX."product_general_accessory WHERE product_id='".$product_id."'";
            $query = $this->db->query($sql);
            if ($gyujto[0]) {
                foreach($gyujto as $id) {
                    $sql = "INSERT IGNORE INTO ".DB_PREFIX."product SET product_id='".$id."'";
                    $query = $this->db->query($sql);

                    $sql = "INSERT IGNORE INTO ".DB_PREFIX."product_to_store SET product_id=".$id.", store_id='".$store_id."'";
                    $query = $this->db->query($sql);

                    $sql = "INSERT INTO ".DB_PREFIX."product_accessory SET accessory_id='".$id."', product_id='".$product_id."'";
                    $query = $this->db->query($sql);
                }

            }
        }

        if ($fejlec_kulcs = array_keys($fejlec,"Ajandekok")) {
            $gyujto = explode(',',$this->ertek[$fejlec_kulcs[0]]);
            $sql = "DELETE FROM ".DB_PREFIX."product_ajandek WHERE product_id='".$product_id."'";
            $query = $this->db->query($sql);
            if ($gyujto[0]) {
                foreach($gyujto as $id) {
                    $sql = "INSERT IGNORE INTO ".DB_PREFIX."product SET product_id='".$id."'";
                    $query = $this->db->query($sql);

                    $sql = "INSERT IGNORE INTO ".DB_PREFIX."product_to_store SET product_id=".$id.", store_id='".$store_id."'";
                    $query = $this->db->query($sql);

                    $sql = "INSERT INTO ".DB_PREFIX."product_ajandek SET ajandek_id='".$id."', product_id='".$product_id."'";
                    $query = $this->db->query($sql);
                }

            }
        }

        if ($fejlec_kulcs = array_keys($fejlec,"Helyettesito termekek")) {
            $gyujto = explode(',',$this->ertek[$fejlec_kulcs[0]]);
            $sql = "DELETE FROM ".DB_PREFIX."product_helyettesito WHERE product_id='".$product_id."'";
            $query = $this->db->query($sql);
            if ($gyujto[0]) {
                foreach($gyujto as $id) {
                    $sql = "INSERT IGNORE INTO ".DB_PREFIX."product SET product_id='".$id."'";
                    $query = $this->db->query($sql);

                    $sql = "INSERT IGNORE INTO ".DB_PREFIX."product_to_store SET product_id=".$id.", store_id='".$store_id."'";
                    $query = $this->db->query($sql);

                    $sql = "INSERT INTO ".DB_PREFIX."product_helyettesito SET helyettesito_id='".$id."', product_id='".$product_id."'";
                    $query = $this->db->query($sql);
                }

            }
        }

        if ($fejlec_kulcs = array_keys($fejlec,"Kacsolodo termekek")) {
            $gyujto = explode(',',$this->ertek[$fejlec_kulcs[0]]);
            $sql = "DELETE FROM ".DB_PREFIX."product_related WHERE product_id='".$product_id."'";
            $query = $this->db->query($sql);
            if ($gyujto[0]) {
                foreach($gyujto as $id) {
                    $sql = "INSERT IGNORE INTO ".DB_PREFIX."product SET product_id='".$id."'";
                    $query = $this->db->query($sql);

                    $sql = "INSERT IGNORE INTO ".DB_PREFIX."product_to_store SET product_id=".$id.", store_id='".$store_id."'";
                    $query = $this->db->query($sql);

                    $sql = "INSERT INTO ".DB_PREFIX."product_related SET related_id='".$id."', product_id='".$product_id."'";
                    $query = $this->db->query($sql);
                }

            }
        }

        if ($fejlec_kulcs = array_keys($fejlec,"Csomag ajanlat")) {
            $gyujto = explode(',',$this->ertek[$fejlec_kulcs[0]]);

            if ($gyujto[0]) {
                $sql = "SELECT package_id FROM ".DB_PREFIX."product_package WHERE product_id='".$product_id."'";
                $query_meglevo = $this->db->query($sql);
                foreach($query_meglevo->rows as $old_product) {
                    $talalt = false;
                    foreach($gyujto as $new_product) {
                        if ($old_product['package_id'] == $new_product) {
                            $talalt = true;
                            break;
                        }
                    }
                    if (!$talalt) {
                        $sql = "DELETE FROM ".DB_PREFIX."product_package WHERE package_id='".$old_product['package_id']."'";
                        $query = $this->db->query($sql);
                    }
                }

                foreach($gyujto as $id) {
                    $lebont = explode('&&',$id);
                    $id = $lebont[0];
                    $quantity = isset($lebont[1]) ? $lebont[1] : 1;
                    $sql = "INSERT IGNORE INTO ".DB_PREFIX."product SET product_id='".$id."'";
                    $query = $this->db->query($sql);

                    $sql = "INSERT IGNORE INTO ".DB_PREFIX."product_to_store SET product_id=".$id.", store_id='".$store_id."'";
                    $query = $this->db->query($sql);

                    $sql = "INSERT IGNORE INTO ".DB_PREFIX."product_package SET package_id='".$id."', product_id='".$product_id."'";
                    $query = $this->db->query($sql);

                    $sql = "UPDATE ".DB_PREFIX."product_package SET quantity='".$quantity."' WHERE package_id='".$id."' AND product_id='".$product_id."'";
                    $query = $this->db->query($sql);
                }
            } else {
                $sql = "DELETE FROM ".DB_PREFIX."product_package WHERE product_id='".$product_id."'";
                $query = $this->db->query($sql);
            }
        }

        if ($fejlec_kulcs = array_keys($fejlec,"Jellemzok")) {
            $gyujto = explode(',',$this->ertek[$fejlec_kulcs[0]]);

            if ($gyujto[0]) {
                $sql = "SELECT attribute_id FROM ".DB_PREFIX."product_attribute WHERE product_id='".$product_id."' AND language_id='".$this->config->get('config_language_id')."'";
                $query_meglevo = $this->db->query($sql);

                foreach($gyujto as $id) {
                    $talalt = false;
                    if ($query_meglevo->num_rows > 0) {
                        foreach($query_meglevo->rows as $attribute) {
                            if ($attribute['attribute_id'] == $id) {
                                $talalt = true;
                                break;
                            }
                        }
                    }
                    if (!$talalt) {
                        $sql = "INSERT IGNORE INTO ".DB_PREFIX."attribute SET attribute_id='".$id."'";
                        $query = $this->db->query($sql);

                        $sql = "INSERT INTO ".DB_PREFIX."product_attribute SET
                                        attribute_id='".$id."',
                                        product_id='".$product_id."',
                                        language_id='".$this->config->get('config_language_id')."'";
                        $query = $this->db->query($sql);
                    }
                }
            } else {
                $sql = "DELETE FROM ".DB_PREFIX."product_attribute WHERE product_id='".$product_id."'";
                $query = $this->db->query($sql);
            }
        }

        if ($fejlec_kulcs = array_keys($fejlec,"Kepek")) {
            $gyujto = explode(',',$this->ertek[$fejlec_kulcs[0]]);

            if ($gyujto[0]) {
                $sql = "SELECT image,product_image_id FROM ".DB_PREFIX."product_image WHERE product_id='".$product_id."'";
                $query_meglevo = $this->db->query($sql);
                foreach($query_meglevo->rows as $old_image) {
                    $talalt = false;
                    foreach($gyujto as $new_image) {
                        if ($old_image['image'] == $new_image) {
                            $talalt = true;
                            break;
                        }
                    }
                    if (!$talalt) {
                        $sql = "DELETE FROM ".DB_PREFIX."product_image WHERE product_image_id='".$old_image['product_image_id']."'";
                        $query = $this->db->query($sql);
                    }
                }



                foreach($gyujto as $image) {
                    $sql = "SELECT product_image_id FROM ".DB_PREFIX."product_image WHERE image='".$image."'";
                    $query = $this->db->query($sql);
                    if ($query->num_rows == 0) {
                        $sql = "INSERT INTO ".DB_PREFIX."product_image SET
                                        image='".$image."',
                                        product_id='".$product_id."'";
                        $query = $this->db->query($sql);
                    }


                }
            } else {
                $sql = "DELETE FROM ".DB_PREFIX."product_image WHERE product_id='".$product_id."'";
                $query = $this->db->query($sql);
            }
        }




    }


    public function formatumEllenorzes($fejlec) {

        $kulcs_megvan = array_keys($fejlec,"product_id") ? true : false;

        $kulcs_megvan = $kulcs_megvan ? true : (array_keys($fejlec,"model") ? true : false);
        $kulcs_megvan = $kulcs_megvan ? true : (array_keys($fejlec,"cikkszam") ? true : false);
        $kulcs_megvan = $kulcs_megvan ? true : (array_keys($fejlec,"cikkszam2") ? true : false);

        if (!$kulcs_megvan) return false;

        foreach($fejlec as $value) {
            if (!$this->fejlecEllenorzes($value)) {
                return false;
            }
        }
        return true;
    }

    public function fejlecEllenorzes($oszlop_neve) {
        if ($oszlop_neve == 'product_id' ||
            $oszlop_neve == 'model' ||
            $oszlop_neve == 'cikkszam' ||
            $oszlop_neve == 'cikkszam2' ||
            $oszlop_neve == 'name' ||
            $oszlop_neve == 'store_id' ||
            $oszlop_neve == 'Kategoriak' ||
            $oszlop_neve == 'Tartozekok' ||
            $oszlop_neve == 'Altalanos tartozekok' ||
            $oszlop_neve == 'Ajandekok' ||
            $oszlop_neve == 'Jellemzok' ||
            $oszlop_neve == 'Helyettesito termekek' ||
            $oszlop_neve == 'Kepek' ||
            $oszlop_neve == 'Csomag ajanlat' ||
            $oszlop_neve == 'PDF' ||
            $oszlop_neve == 'Kacsolodo termekek' ||
            $oszlop_neve == 'Videok' ||
            $oszlop_neve == 'Raktarak') {
            return true;
        } else {
            return false;
        }

    }

}


