<?php
class ControllerCatalogElhelyezkedes extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('catalog/elhelyezkedes');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/elhelyezkedes');

        $this->data['heading_title']    = $this->language->get('heading_title');
        $this->data['button_insert']    = $this->language->get('button_insert');
        $this->data['button_delete']    = $this->language->get('button_delete');
        $this->data['column_elhelyezkedes']   = $this->language->get('column_elhelyezkedes');
        $this->data['column_sort_order']= $this->language->get('column_sort_order');
        $this->data['column_action']    = $this->language->get('column_action');
        $this->data['column_maximum']   = $this->language->get('column_maximum');
        $this->data['column_ara']       = $this->language->get('column_ara');
        $this->data['column_status']       = $this->language->get('column_status');
        $this->data['column_alcsoport_status']       = $this->language->get('column_alcsoport_status');
        $this->data['text_no_results']  = $this->language->get('text_no_results');
        $this->data['text_engedelyezett']  = $this->language->get('text_engedelyezett');
        $this->data['text_letiltott']  = $this->language->get('text_letiltott');
        $this->data['error_warning']    = '';
        $this->data['success'] = '';


        $this->getList();
    }

    public function insert() {
        $this->load->language('catalog/elhelyezkedes');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/elhelyezkedes');

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $elhelyezkedes = $this->config->get('elhelyezkedes');
            $last_id = 0;

            if ($elhelyezkedes) {
                if (is_array($elhelyezkedes) ) {
                    foreach ($elhelyezkedes as $value) {
                        if ($value['elhelyezkedes_id'] > $last_id) {
                            $last_id = $value['elhelyezkedes_id'];
                        }
                    }
                }
            }
            $last_id++;
            if ( !empty($_REQUEST['megnevezes']) ) {
                $kiir["elhelyezkedes"] = $this->config->get("elhelyezkedes");
                $kiir["elhelyezkedes"][] = array(
                    'megnevezes'        => $_REQUEST['megnevezes'],
                    'sort_order'        => $_REQUEST['sort_order'],
                    'elhelyezkedes_id'  => $last_id,
                    'maximum'           => $_REQUEST['maximum'],
                    'ara'               => $_REQUEST['ara'],
                    'alcsoport_status'  => $_REQUEST['alcsoport_status'],
                    'status'            => $_REQUEST['status'],
                    'tax_class_id'      => $_REQUEST['tax_class_id']
                );
            }

            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('elhelyezkedes', $kiir);
            $this->session->data['success'] = $this->language->get('text_success');
            $this->redirect($this->url->link('catalog/elhelyezkedes', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getForm();
    }

    public function update() {
        $this->load->language('catalog/elhelyezkedes');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/elhelyezkedes');

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $kiir["elhelyezkedes"] = $this->config->get("elhelyezkedes");
            foreach ($kiir["elhelyezkedes"] as $value) {
                if ($value['elhelyezkedes_id'] ==  $_REQUEST['elhelyezkedes_id']) {
                    $modositott["elhelyezkedes"][] = array(
                        'megnevezes'        => $_REQUEST['megnevezes'],
                        'sort_order'        => $_REQUEST['sort_order'],
                        'elhelyezkedes_id'  => $_REQUEST['elhelyezkedes_id'],
                        'maximum'           => $_REQUEST['maximum'],
                        'ara'               => $_REQUEST['ara'],
                        'alcsoport_status'  => $_REQUEST['alcsoport_status'],
                        'status'            => $_REQUEST['status'],
                        'tax_class_id'      => $_REQUEST['tax_class_id']

                    );
                } else {
                    $modositott["elhelyezkedes"][] = array(
                        'megnevezes'        => $value['megnevezes'],
                        'sort_order'        => $value['sort_order'],
                        'elhelyezkedes_id'  => $value['elhelyezkedes_id'],
                        'maximum'           => $value['maximum'],
                        'ara'               => $value['ara'],
                        'alcsoport_status'  => $value['alcsoport_status'],
                        'status'            => $value['status'],
                        'tax_class_id'      => $value['tax_class_id']
                    );
                }
            }

            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('elhelyezkedes', $modositott);
            $this->session->data['success'] = $this->language->get('text_success');


            $this->redirect($this->url->link('catalog/elhelyezkedes', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('catalog/elhelyezkedes');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/elhelyezkedes');

        if (isset($this->request->post['selected']) ) {

            $kiir["elhelyezkedes"] = $this->config->get("elhelyezkedes");

            foreach ($kiir["elhelyezkedes"] as $value) {

                if ( !in_array($value['elhelyezkedes_id'],$_REQUEST['selected']) ) {
                    $modositott["elhelyezkedes"][] = array(
                        'megnevezes'        => $value['megnevezes'],
                        'sort_order'        => $value['sort_order'],
                        'elhelyezkedes_id'  => $value['elhelyezkedes_id'],
                        'maximum'           => $value['maximum'],
                        'alcsoport_status'  => $value['alcsoport_status'],
                        'status'            => $value['status'],
                        'tax_class_id'      => $value['tax_class_id']
                    );
                }
            }

            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('elhelyezkedes', $modositott);
            $this->session->data['success'] = $this->language->get('text_success');


            $this->redirect($this->url->link('catalog/elhelyezkedes', 'token=' . $this->session->data['token'], 'SSL'));

        }

        $this->getList();
    }

    private function getList() {

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('catalog/elhelyezkedes', 'token=' . $this->session->data['token'] , 'SSL'),
            'separator' => ' :: '
        );

        $this->data['insert'] = $this->url->link('catalog/elhelyezkedes/insert', 'token=' . $this->session->data['token'] , 'SSL');
        $this->data['delete'] = $this->url->link('catalog/elhelyezkedes/delete', 'token=' . $this->session->data['token'] , 'SSL');



        $this->data['elhelyezkedess'] = array();
        $elhelyezkedes = $this->config->get("elhelyezkedes");

        if ($elhelyezkedes) {
            foreach ($elhelyezkedes as $value) {
                $action = array(
                    'text' => $this->language->get('text_edit'),
                    'href' => $this->url->link('catalog/elhelyezkedes/update', 'token=' . $this->session->data['token'].'&elhelyezkedes_id='.$value['elhelyezkedes_id'], 'SSL')
                );

                if ($value['status'] == 0) {
                    $statusza = $this->data['text_letiltott'];
                } else {
                    $statusza = $this->data['text_engedelyezett'];
                }

                if ($value['alcsoport_status'] == 0) {
                    $alcsoport_statusza = $this->data['text_letiltott'];
                } else {
                    $alcsoport_statusza = $this->data['text_engedelyezett'];
                }

                $this->data['elhelyezkedess'][] = array(
                    'megnevezes'     => $value['megnevezes'],
                    'sort_order'     => $value['sort_order'],
                    'elhelyezkedes_id' => $value['elhelyezkedes_id'],
                    'maximum'        => $value['maximum'],
                    'ara'            => $value['ara'],
                    'alcsoport_status' => $value['alcsoport_status'],
                    'status'         => $value['status'],
                    'tax_class_id'   => $value['tax_class_id'],
                    'alcsoport_statusza' => $alcsoport_statusza,
                    'statusza'       => $statusza,
                    'action'         => $action,
                    'selected'       => isset($this->request->post['selected']) && in_array($value['elhelyezkedes_id'], $this->request->post['selected']),

                );
            }
        }


        $sortingSettings = array(
            0       =>  array(
                'orderby' =>  'sort_order',
                'sortorder'     => 'ASC'
            )
        );

        $rendezo = new ArrayOfArrays($this->data['elhelyezkedess']);
        $rendezo->multiSorting($sortingSettings,true);
        $this->data['elhelyezkedess'] = $rendezo->getArrayCopy();


        $this->template = 'catalog/elhelyezkedes_list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    private function getForm() {
        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');
        $this->data['text_percent'] = $this->language->get('text_percent');
        $this->data['text_amount'] = $this->language->get('text_amount');
        $this->data['text_select']  = $this->language->get('text_select');

        $this->data['entry_name'] = $this->language->get('entry_name');
        $this->data['entry_description'] = $this->language->get('entry_description');
        $this->data['entry_code'] = $this->language->get('entry_code');
        $this->data['entry_discount'] = $this->language->get('entry_discount');
        $this->data['entry_logged'] = $this->language->get('entry_logged');
        $this->data['entry_shipping'] = $this->language->get('entry_shipping');
        $this->data['entry_type'] = $this->language->get('entry_type');
        $this->data['entry_total'] = $this->language->get('entry_total');
        $this->data['entry_category'] = $this->language->get('entry_category');
        $this->data['entry_product'] = $this->language->get('entry_product');
        $this->data['entry_date_start'] = $this->language->get('entry_date_start');
        $this->data['entry_date_end'] = $this->language->get('entry_date_end');
        $this->data['entry_uses_total'] = $this->language->get('entry_uses_total');
        $this->data['entry_uses_customer'] = $this->language->get('entry_uses_customer');
        $this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_alcsoport_status'] = $this->language->get('entry_alcsoport_status');
        $this->data['entry_sorrend'] = $this->language->get('entry_sorrend');
        $this->data['entry_ara'] = $this->language->get('entry_ara');
        $this->data['entry_tax_class'] = $this->language->get('entry_tax_class');
        $this->data['entry_darabszam']     = $this->language->get('entry_darabszam');

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');

        $this->data['tab_general'] = $this->language->get('tab_general');
        $this->data['tab_elhelyezkedes_history'] = $this->language->get('tab_elhelyezkedes_history');

        $this->data['token'] = $this->session->data['token'];

        $this->data['text_engedelyezett']  = $this->language->get('text_engedelyezett');
        $this->data['text_letiltott']  = $this->language->get('text_letiltott');

        $this->load->model('localisation/tax_class');
        $this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

        if (isset($this->request->get['elhelyezkedes_id'])) {
            $elhelyezkedes = $this->config->get("elhelyezkedes");
            foreach ($elhelyezkedes as $value) {
                if ($value['elhelyezkedes_id'] == $this->request->get['elhelyezkedes_id']) {
                    $this->data['elhelyezkedes_id'] = $value['elhelyezkedes_id'];
                    $this->data['megnevezes']       = $value['megnevezes'];
                    $this->data['sort_order']       = $value['sort_order'];
                    $this->data['maximum']          = $value['maximum'];
                    $this->data['ara']              = $value['ara'];
                    $this->data['alcsoport_status']           = $value['alcsoport_status'];
                    $this->data['status']           = $value['status'];
                    $this->data['tax_class_id']     = $value['tax_class_id'];


                }
            }

        } else {
            $this->data['elhelyezkedes_id'] = 0;
            $this->data['megnevezes'] = '';
            $this->data['sort_order'] = 0;
            $this->data['maximum']    = 0;
            $this->data['ara']       = 0;
            $this->data['alcsoport_statusza']    = 0;
            $this->data['status']    = 0;
            $this->data['tax_class_id']    = 0;

        }

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }



        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('catalog/elhelyezkedes', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        if (!isset($this->request->get['elhelyezkedes_id'])) {
            $this->data['action'] = $this->url->link('catalog/elhelyezkedes/insert', 'token=' . $this->session->data['token'], 'SSL');
        } else {
            $this->data['action'] = $this->url->link('catalog/elhelyezkedes/update', 'token=' . $this->session->data['token'] . '&elhelyezkedes_id=' . $this->request->get['elhelyezkedes_id'], 'SSL');
        }

        $this->data['cancel'] = $this->url->link('catalog/elhelyezkedes', 'token=' . $this->session->data['token'], 'SSL');


        $this->template = 'catalog/elhelyezkedes_form.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }


    public function alcsoportVizsgal() {

        $json = array();

        $this->load->model('catalog/elhelyezkedes');
        $kell_alcsoport = $this->model_catalog_elhelyezkedes->kellAlcsoport();

        if ($kell_alcsoport) {
            $json['kell_alcsoport'] = true;
        }


        $json['sor'] = $this->request->request['sor'];


        $this->response->setOutput(json_encode($json));
    }

    public function elhelyezkedesSzabadHely() {

        $json = array();

        $this->load->language('catalog/product');
        $this->load->model('catalog/elhelyezkedes');
        $eredmeny = $this->model_catalog_elhelyezkedes->getSzabadHelyek();

        if ($eredmeny !== true) {
            if ($eredmeny['elhelyezkedesek_kulonbseg'] < 1 ) {
                $json['error']['elhelyezkedes'] = $this->language->get('elhelyezkedesek_kulonbseg_error');
            }

            if ($eredmeny['elhelyezkedes_alcsoport_kulonbseg']  < 1 ) {
                $json['error']['elhelyezkedes_alcsoport'] = $this->language->get('elhelyezkedes_alcsoport_kulonbseg_error');
            }

            if ($eredmeny['kiemelesek_kulonbseg']  < 1 ) {
                $json['error']['kiemelesek'] = $this->language->get('kiemelesek_kulonbseg_error');
            }

            if (!isset($json['error'])) {
                $json['succsess'] = $this->language->get('elhelyezkedesek_succsess');
            }
        }
        $json['sor'] = $_REQUEST['sor'];

        $this->response->setOutput(json_encode($json));
    }

}
?>