<?php
class ControllerCatalogFogalomMagyarazat extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/fogalom_magyarazat');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/fogalom_magyarazat');

		$this->getList();
	}

	public function insert() {
		$this->load->language('catalog/fogalom_magyarazat');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/fogalom_magyarazat');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_fogalom_magyarazat->addFogalomMagyarazat($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');


			$url = '';

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/fogalom_magyarazat', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->load->language('catalog/fogalom_magyarazat');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/fogalom_magyarazat');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_fogalom_magyarazat->editFogalomMagyarazat($this->request->get['fogalom_magyarazat_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';


			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/fogalom_magyarazat', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/fogalom_magyarazat');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/fogalom_magyarazat');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $fogalom_magyarazat_id) {
				$this->model_catalog_fogalom_magyarazat->deleteFogalomMagyarazat($fogalom_magyarazat_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/fogalom_magyarazat', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	private function getList() {

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'fmd.name';
        }

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/fogalom_magyarazat', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/fogalom_magyarazat/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/fogalom_magyarazat/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_no_results'] = $this->language->get('text_no_results');

        $this->data['column_title'] = $this->language->get('column_title');
        $this->data['column_status'] = $this->language->get('column_status');
        $this->data['column_action'] = $this->language->get('column_action');

        $this->data['button_insert'] = $this->language->get('button_insert');
        $this->data['button_delete'] = $this->language->get('button_delete');

		$this->data['fogalom_magyarazats'] = array();

		$data = array(
            'sort'  => $sort,
            'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$fogalom_magyarazat_total = $this->model_catalog_fogalom_magyarazat->getTotalFogalomMagyarazat();

		$results = $this->model_catalog_fogalom_magyarazat->getFogalomMagyarazatok($data);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/fogalom_magyarazat/update', 'token=' . $this->session->data['token'] . '&fogalom_magyarazat_id=' . $result['fogalom_magyarazat_id'] . $url, 'SSL')
			);

			$this->data['fogalom_magyarazats'][] = array(
				'fogalom_magyarazat_id' => $result['fogalom_magyarazat_id'],
				'name'           => $result['name'],
				'selected'       => isset($this->request->post['selected']) && in_array($result['fogalom_magyarazat_id'], $this->request->post['selected']),
				'action'         => $action,
				'status'         => ($result['status'] ? $this->data['text_enabled'] : $this->data['text_disabled'])
			);
		}


		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

        $this->data['sort_status'] = $this->url->link('catalog/fogalom_magyarazat', 'token=' . $this->session->data['token'] . '&sort=fm.status' . $url, 'SSL');
        $this->data['sort_title'] = $this->url->link('catalog/fogalom_magyarazat', 'token=' . $this->session->data['token'] . '&sort=fmd.name' . $url, 'SSL');


		$url = '';

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $fogalom_magyarazat_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/fogalom_magyarazat', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();


        $this->data['sort'] = $sort;
        $this->data['order'] = $order;

		$this->template = 'catalog/fogalom_magyarazat_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	private function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_fejlecbe'] = $this->language->get('entry_fejlecbe');
		$this->data['entry_lablecbe'] = $this->language->get('entry_lablecbe');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');
		$this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['tab_design'] = $this->language->get('tab_design');

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['title'])) {
			$this->data['error_title'] = $this->error['title'];
		} else {
			$this->data['error_title'] = array();
		}

		if (isset($this->error['description'])) {
			$this->data['error_description'] = $this->error['description'];
		} else {
			$this->data['error_description'] = array();
		}

		$url = '';

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/fogalom_magyarazat', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['fogalom_magyarazat_id'])) {
			$this->data['action'] = $this->url->link('catalog/fogalom_magyarazat/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/fogalom_magyarazat/update', 'token=' . $this->session->data['token'] . '&fogalom_magyarazat_id=' . $this->request->get['fogalom_magyarazat_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/fogalom_magyarazat', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['fogalom_magyarazat_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$fogalom_magyarazat_info = $this->model_catalog_fogalom_magyarazat->getFogalomMagyarazat($this->request->get['fogalom_magyarazat_id']);
		}


		$this->load->model('localisation/language');

		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['fogalom_magyarazat_description'])) {
			$this->data['fogalom_magyarazat_description'] = $this->request->post['fogalom_magyarazat_description'];
		} elseif (isset($this->request->get['fogalom_magyarazat_id'])) {
			$this->data['fogalom_magyarazat_description'] = $this->model_catalog_fogalom_magyarazat->getFogalomMagyarazatDescriptions($this->request->get['fogalom_magyarazat_id']);
		} else {
			$this->data['fogalom_magyarazat_description'] = array();
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($fogalom_magyarazat_info)) {
			$this->data['status'] = $fogalom_magyarazat_info['status'];
		} else {
			$this->data['status'] = 1;
		}



		$this->template = 'catalog/fogalom_magyarazat_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	private function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/fogalom_magyarazat')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}


		/*foreach ($this->request->post['fogalom_magyarazat_description'] as $language_id => $value) {
			if ((utf8_strlen($value['title']) < 3) || (utf8_strlen($value['title']) > 64)) {
				$this->error['title'][$language_id] = $this->language->get('error_title');
			}
		
			if (utf8_strlen($value['description']) < 3) {
				$this->error['description'][$language_id] = $this->language->get('error_description');
			}
		}*/

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	private function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/fogalom_magyarazat')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>