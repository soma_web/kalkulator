<?php
class ControllerCatalogCategory extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('catalog/category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/category');

        $this->model_catalog_category->categoryPath();


        $this->getList();
    }

    public function insert() {
        $this->load->language('catalog/category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/category');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_category->addCategory($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->redirect($this->url->link('catalog/category', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getForm();
    }

    public function update() {
        $this->load->language('catalog/category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/category');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_category->editCategory($this->request->get['category_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->redirect($this->url->link('catalog/category', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('catalog/category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/category');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $category_id) {
                $this->model_catalog_category->deleteCategory($category_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $this->redirect($this->url->link('catalog/category', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getList();
    }

public function getList() {

    $this->data['megjelenit_piacter'] = false;

    if ($this->config->get('megjelenit_piacter') == 1){
        $this->data['megjelenit_piacter'] = true;

        if (isset($_POST["kepekrogzit"])){
            $kepneve= $_FILES['userfile']['name'];
            $para1=basename($_FILES['userfile']['name']);
            $patad = explode ( "." , $para1 );
            for ($i=0; isset($patad[$i]) && !empty($patad[$i]); $i++){}
            if ($i>1)
                $kiter=$patad[$i-1];
            else
                $kiter= "";

            if (!empty($kiter) && strtoupper($kiter)=="CSV"){
                if ($_FILES['userfile']['size'] == 0){
                    ?>
                    <script>
                        alert("A kép mérete nagyobb a megengedettnél!");
                    </script>
                    <?
                } else{
                        $felvesz="../piacter/";
                    if (!is_dir($felvesz))
                        mkdir($felvesz);

                    $str1 =$kepneve;
                    $maxfilesize = 1200000;  // A maximum file méret => 204800 = 200kb
                    $filedir = $felvesz; //Itt megadhatjuk a mappa el�r�s�t, ahova mentj�k a k�pet. 777 legyen a mappa attrib�tuma
                    $size = $_FILES['userfile']['size'];
                    $type = $_FILES['userfile']['type'];
                    if($_FILES['userfile']['size'] < $maxfilesize){
                        if (is_uploaded_file($_FILES['userfile']['tmp_name'])){
                            if (!file_exists($filedir . '/' . $str1)){
                                if (move_uploaded_file($_FILES['userfile']['tmp_name'],$filedir.'/'.$str1)){
                                    chmod($filedir.'/'.$str1, 0777);
                                    $melyik_igenyles=$felvesz;
                                } else{
                                    echo "Nem sikerült<br>";
                                    echo $filedir.'/'.$str1;
                                }
                            }
                        } else{
                            ?>
                            <script>
                                alert("Ismeretlen hiba miatt a rögzítés nem sikerült!");
                            </script>
                            <?
                        }
                    } else{
                        $melyik_file=$_FILES['userfile']['name'];
                        $melyik_igenyles=$felvesz;
                        $melyik_mitirki="Az állomány meghaladja a megengedett méretet!";
                    }
                }
            }
        }
    }




    $this->data['breadcrumbs'] = array();

    $this->data['breadcrumbs'][] = array(
        'text'      => $this->language->get('text_home'),
        'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
        'separator' => false
    );

    $this->data['breadcrumbs'][] = array(
        'text'      => $this->language->get('heading_title'),
        'href'      => $this->url->link('catalog/category', 'token=' . $this->session->data['token'], 'SSL'),
        'separator' => ' :: '
    );

    $this->data['insert'] = $this->url->link('catalog/category/insert', 'token=' . $this->session->data['token'], 'SSL');
    $this->data['delete'] = $this->url->link('catalog/category/delete', 'token=' . $this->session->data['token'], 'SSL');
    $this->data['csvinsert'] = $this->url->link('catalog/category', 'token=' . $this->session->data['token'], 'SSL');

    $this->data['categories'] = array();

    $results = $this->model_catalog_category->getCategories(0);

    foreach ($results as $result) {
        $action = array();

        $action[] = array(
            'text' => $this->language->get('text_edit'),
            'href' => $this->url->link('catalog/category/update', 'token=' . $this->session->data['token'] . '&category_id=' . $result['category_id'], 'SSL')
        );

        $this->data['categories'][] = array(
            'category_id' => $result['category_id'],
            'name'        => $result['name'],
            'sort_order'  => $result['sort_order'],
            'selected'    => isset($this->request->post['selected']) && in_array($result['category_id'], $this->request->post['selected']),
            'action'      => $action
        );
    }

    $this->data['heading_title'] = $this->language->get('heading_title');

    $this->data['text_no_results'] = $this->language->get('text_no_results');

    $this->data['column_name'] = $this->language->get('column_name');
    $this->data['column_sort_order'] = $this->language->get('column_sort_order');
    $this->data['column_action'] = $this->language->get('column_action');

    $this->data['button_insert'] = $this->language->get('button_insert');
    $this->data['button_delete'] = $this->language->get('button_delete');
    $this->data['entry_csvk_piacternek'] = $this->language->get('entry_csvk_piacternek');

    if (isset($this->error['warning'])) {
        $this->data['error_warning'] = $this->error['warning'];
    } else {
        $this->data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
        $this->data['success'] = $this->session->data['success'];

        unset($this->session->data['success']);
    } else {
        $this->data['success'] = '';
    }

    $this->template = 'catalog/category_list.tpl';
    $this->children = array(
        'common/header',
        'common/footer'
    );

    $this->response->setOutput($this->render());
}

    private function getForm() {
        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_none']            = $this->language->get('text_none');
        $this->data['text_default']         = $this->language->get('text_default');
        $this->data['text_image_manager']   = $this->language->get('text_image_manager');
        $this->data['text_browse']          = $this->language->get('text_browse');
        $this->data['text_clear']           = $this->language->get('text_clear');
        $this->data['text_enabled']         = $this->language->get('text_enabled');
        $this->data['text_disabled']        = $this->language->get('text_disabled');
        $this->data['text_percent']         = $this->language->get('text_percent');
        $this->data['text_amount']          = $this->language->get('text_amount');
        $this->data['text_piacter']         = $this->language->get('text_piacter');
        $this->data['text_piacter_select']  = $this->language->get('text_piacter_select');
        $this->data['entry_piacter']        = $this->language->get('entry_piacter');
        $this->data['entry_csvk_piacternek'] = $this->language->get('entry_csvk_piacternek');
        $this->data['entry_filter']         = $this->language->get('entry_filter');

        $this->data['entry_name']           = $this->language->get('entry_name');
        $this->data['entry_meta_keyword']   = $this->language->get('entry_meta_keyword');
        $this->data['entry_meta_description'] = $this->language->get('entry_meta_description');
        $this->data['entry_description']    = $this->language->get('entry_description');
        $this->data['entry_store']          = $this->language->get('entry_store');
        $this->data['entry_keyword']        = $this->language->get('entry_keyword');
        $this->data['entry_parent']         = $this->language->get('entry_parent');
        $this->data['entry_image']          = $this->language->get('entry_image');
        $this->data['entry_top']            = $this->language->get('entry_top');
        $this->data['entry_column']         = $this->language->get('entry_column');
        $this->data['entry_sort_order']     = $this->language->get('entry_sort_order');
        $this->data['entry_status']         = $this->language->get('entry_status');
        $this->data['entry_layout']         = $this->language->get('entry_layout');
        $this->data['piacter_rogzit']       = $this->language->get('piacter_rogzit');
        $this->data['piacter_torol']        = $this->language->get('piacter_torol');
        $this->data['entry_image_icon']     = $this->language->get('entry_image_icon');

        $this->data['button_save']          = $this->language->get('button_save');
        $this->data['button_cancel']        = $this->language->get('button_cancel');

        $this->data['tab_general']          = $this->language->get('tab_general');
        $this->data['tab_data']             = $this->language->get('tab_data');
        $this->data['tab_design']           = $this->language->get('tab_design');

        $this->data['megjelenit_piacter'] = false;
        if ($this->config->get('megjelenit_piacter') == 1){
            $this->data['megjelenit_piacter'] = true;
        }

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $this->data['error_name'] = $this->error['name'];
        } else {
            $this->data['error_name'] = array();
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('catalog/category', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        if (!isset($this->request->get['category_id'])) {
            $this->data['action'] = $this->url->link('catalog/category/insert', 'token=' . $this->session->data['token'], 'SSL');
        } else {
            $this->data['action'] = $this->url->link('catalog/category/update', 'token=' . $this->session->data['token'] . '&category_id=' . $this->request->get['category_id'], 'SSL');
        }

        $this->data['cancel'] = $this->url->link('catalog/category', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['token'] = $this->session->data['token'];

        if (isset($this->request->get['category_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $category_info = $this->model_catalog_category->getCategory($this->request->get['category_id']);
        }

        $this->load->model('localisation/language');

        $this->data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->request->post['category_description'])) {
            $this->data['category_description'] = $this->request->post['category_description'];
        } elseif (isset($this->request->get['category_id'])) {
            $this->data['category_description'] = $this->model_catalog_category->getCategoryDescriptions($this->request->get['category_id']);
        } else {
            $this->data['category_description'] = array();
        }

        $categories = $this->model_catalog_category->getCategories(0);

        // Remove own id from list
        if (!empty($category_info)) {
            foreach ($categories as $key => $category) {
                if ($category['category_id'] == $category_info['category_id']) {
                    unset($categories[$key]);
                }
            }
        }

        $this->data['categories'] = $categories;

        if (isset($this->request->post['parent_id'])) {
            $this->data['parent_id'] = $this->request->post['parent_id'];
        } elseif (!empty($category_info)) {
            $this->data['parent_id'] = $category_info['parent_id'];
        } else {
            $this->data['parent_id'] = 0;
        }


        $this->load->model('catalog/filter');

        if (isset($this->request->post['category_filter'])) {
            $filters = $this->request->post['category_filter'];
        } elseif (isset($this->request->get['category_id'])) {
            $filters = $this->model_catalog_category->getCategoryFilters($this->request->get['category_id']);
        } else {
            $filters = array();
        }

        $this->data['category_filters'] = array();

        foreach ($filters as $filter_id) {
            $filter_info = $this->model_catalog_filter->getFilter($filter_id);

            if ($filter_info) {
                $this->data['category_filters'][] = array(
                    'filter_id' => $filter_info['filter_id'],
                    'name'      => $filter_info['group'] . ' &gt; ' . $filter_info['name']
                );
            }
        }






        $this->load->model('setting/store');

        $this->data['stores'] = $this->model_setting_store->getStores();

        if (isset($this->request->post['category_store'])) {
            $this->data['category_store'] = $this->request->post['category_store'];
        } elseif (isset($this->request->get['category_id'])) {
            $this->data['category_store'] = $this->model_catalog_category->getCategoryStores($this->request->get['category_id']);
        } else {
            $this->data['category_store'] = array(0);
        }

        if (isset($this->request->post['keyword'])) {
            $this->data['keyword'] = $this->request->post['keyword'];
        } elseif (!empty($category_info)) {
            $this->data['keyword'] = $category_info['keyword'];
        } else {
            $this->data['keyword'] = '';
        }

        if (isset($this->request->post['image'])) {
            $this->data['image'] = $this->request->post['image'];
        } elseif (!empty($category_info)) {
            $this->data['image'] = $category_info['image'];
        } else {
            $this->data['image'] = '';
        }

        if (isset($this->request->post['image_icon'])) {
            $this->data['image_icon'] = $this->request->post['image_icon'];
        } elseif (!empty($category_info)) {
            $this->data['image_icon'] = $category_info['image_icon'];
        } else {
            $this->data['image_icon'] = '';
        }

        $this->load->model('tool/image');

        if (!empty($category_info) && $category_info['image'] && file_exists(DIR_IMAGE . $category_info['image'])) {
            $this->data['thumb'] = $this->model_tool_image->resize($category_info['image'], 100, 100);
        } else {
            $this->data['thumb'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
        }

        if (!empty($category_info) && $category_info['image_icon'] && file_exists(DIR_IMAGE . $category_info['image_icon'])) {
            $this->data['thumb_icon'] = $this->model_tool_image->resize($category_info['image_icon'], 100, 100);
        } else {
            $this->data['thumb_icon'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);

        if (isset($this->request->post['top'])) {
            $this->data['top'] = $this->request->post['top'];
        } elseif (!empty($category_info)) {
            $this->data['top'] = $category_info['top'];
        } else {
            $this->data['top'] = 0;
        }


        if (isset($this->request->post['piacter'])) {
            $this->data['piacter'] = $this->request->post['piacter'];
        } elseif (!empty($category_info)) {
            $this->data['piacter'] = $category_info['piacter'];
        } else {
            $this->data['piacter'] = 0;
        }

        if (isset($this->request->post['column'])) {
            $this->data['column'] = $this->request->post['column'];
        } elseif (!empty($category_info)) {
            $this->data['column'] = $category_info['column'];
        } else {
            $this->data['column'] = 1;
        }

        if (isset($this->request->post['sort_order'])) {
            $this->data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($category_info)) {
            $this->data['sort_order'] = $category_info['sort_order'];
        } else {
            $this->data['sort_order'] = 0;
        }

        if (isset($this->request->post['status'])) {
            $this->data['status'] = $this->request->post['status'];
        } elseif (!empty($category_info)) {
            $this->data['status'] = $category_info['status'];
        } else {
            $this->data['status'] = 1;
        }

        if (isset($this->request->post['category_layout'])) {
            $this->data['category_layout'] = $this->request->post['category_layout'];
        } elseif (isset($this->request->get['category_id'])) {
            $this->data['category_layout'] = $this->model_catalog_category->getCategoryLayouts($this->request->get['category_id']);
        } else {
            $this->data['category_layout'] = array();
        }

        $csv_files=scandir("../piacter/");

        $this->data['piacter_csv_listak'] = array();
        foreach($csv_files as $file_name){
            if ($file_name != "." && $file_name != ".."){
                $this->data['piacter_csv_listak'][] = array('lista_neve'      => substr($file_name,0,-4) );
            }
        }

        $this->load->model('design/layout');

        $this->data['layouts'] = $this->model_design_layout->getLayouts();

        $this->template = 'catalog/category_form.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    private function validateForm() {
        if (!$this->user->hasPermission('modify', 'catalog/category')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }


        if ($this->config->get("megjelenit_admin_nyelvi_ellenorzes") == 1){
            foreach ($this->request->post['category_description'] as $language_id => $value) {
                if ((utf8_strlen($value['name']) < 2) || (utf8_strlen($value['name']) > 255)) {
                    $this->error['name'][$language_id] = $this->language->get('error_name');
                }
            }
        }


        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    private function validateDelete() {
        if (!$this->user->hasPermission('modify', 'catalog/category')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    public function csvOlvas() {

        $mit_torol=$this->db->query("select `product_id` from product where quantity=0 and piacter=1");
        //$this->db->query("INSERT INTO product  set `model` = 777");

        foreach($mit_torol->rows as $torol_id){
            $torol_product=$torol_id['product_id'];
            $this->db->query("delete from  product_to_store     where product_id='".$torol_product."'");
            $this->db->query("delete from  product_special      where product_id='".$torol_product."'");
            $this->db->query("delete from  product_to_category  where product_id='".$torol_product."'");
            $this->db->query("delete from  product_description  where product_id='".$torol_product."'");
            $this->db->query("delete from  product_discount     where product_id='".$torol_product."'");
            $this->db->query("delete from  product              where product_id='".$torol_product."'");
        }

        $mit_torol=$this->db->query("select category_id from  category where piacter=1");
        foreach($mit_torol->rows as $torol_id){
            $torol_category=$torol_id['category_id'];

            $torolhet=$this->db->query("select * from product_to_category where category_id='".$torol_category."'");
            if ($torolhet->num_rows = 0){
                $this->db->query("delete from  category               where category_id='".$torol_category."'");
                $this->db->query("delete from  category_description   where category_id='".$torol_category."'");
                $this->db->query("delete from  category_to_store      where category_id='".$torol_category."'");
            }
        }

        $category_name=$_POST['file_name'];
        $category_id=$_POST['category_id'];
        $filename=$category_name.".csv";

        $query=$this->db->query("select * from category_description where language_id=2 and `name` LIKE '".$category_name."'");

        if ($query->num_rows == 0){
            $this->db->query("INSERT INTO category set `status` = 1,
                                                         piacter = 1,
                                                         parent_id=".$category_id);
            $query=$this->db->query("select `category_id` from category order by category_id desc limit 1");
            $category_id=$query->row['category_id'];
            $this->db->query("INSERT INTO category_description  set `name` = '".$category_name."', language_id = 2, category_id='".$category_id."'");
            $this->db->query("INSERT INTO category_description  set `name` = '".$category_name."', language_id = 1, category_id='".$category_id."'");
            $this->db->query("INSERT INTO category_to_store  set `category_id` = '".$category_id."', store_id = 0");
        } else{
            $category_id=$query->row['category_id'];
            $query_category=$this->db->query("select * from product_to_category where category_id='".$category_id."'");
            foreach($query_category->rows as $value){
                $torol_product=$value['product_id'];
                $this->db->query("delete from  product_to_store     where product_id='".$torol_product."'");
                $this->db->query("delete from  product_special      where product_id='".$torol_product."'");
                $this->db->query("delete from  product_to_category  where product_id='".$torol_product."'");
                $this->db->query("delete from  product_description  where product_id='".$torol_product."'");
                $this->db->query("delete from  product_discount     where product_id='".$torol_product."'");
                $this->db->query("delete from  product              where product_id='".$torol_product."'");
            }
        }



        $handle = fopen("../piacter/".$filename, "r");

        $data = fgets($handle);
        while ($data = fgets($handle)) {
            $termek= explode(";",iconv("ISO-8859-1","UTF-8" , $data));

            $sql="INSERT INTO product SET
                  `price`                  =  '".$termek[3]."',
                  `model`                  = '".$termek[1]."',
                  `quantity`               = 1,
                  tax_class_id             = 11,
                  `weight`                 = '".(isset($termek[6]) ? $termek[6] : $termek[5])."',
                  `weight_class_id`        = 1,
                  `status`                 = 1,
                  piacter                  = 1   ";

            $query = $this->db->query($sql);

            //$letrehoz="SELECT product_id from product order by product_id desc limit 1";

            //$utoso_id = mysql_query($letrehoz) or csvOlvasHiba();
            $product_id = $this->db->getLastId();

            /*$row = mysql_fetch_array($utoso_id);
            $product_id = $row['product_id'];*/
            $sql="INSERT INTO product_to_store set
                  `product_id`             =  '".$product_id."',
                  `store_id`               = 0 ";
            $query = $this->db->query($sql);

            $sql="INSERT INTO product_description set
                  `product_id`             =  '".$product_id."',
                  `language_id`            = 2,
                  `name`                   = '".$termek[0]."'  ";
            $query = $this->db->query($sql);

            $sql="INSERT INTO product_description set
                  `product_id`             =  '".$product_id."',
                  `language_id`            = 1,
                  `name`                   = '".$termek[0]."'  ";
            $query = $this->db->query($sql);

            $this->db->query("INSERT INTO product_to_category  set `category_id` = '".$category_id."', product_id = '".$product_id."'");

            $this->db->query("INSERT INTO product_special  set
                                            product_id = '".$product_id."',
                                            customer_group_id=12,
                                            price='".$termek[2]."'");

            $this->db->query("INSERT INTO product_special  set
                                            product_id = '".$product_id."',
                                            customer_group_id=10,
                                            price='".$termek[3]."'");

            $this->db->query("INSERT INTO product_special  set
                                            product_id = '".$product_id."',
                                            customer_group_id=8,
                                            price='".$termek[3]."'");

            $this->db->query("INSERT INTO product_special  set
                                            product_id = '".$product_id."',
                                            customer_group_id=11,
                                            price='".$termek[4]."'");

            if (isset($termek[6])) {
                $this->db->query("INSERT INTO product_special  set
                                                product_id = '".$product_id."',
                                                customer_group_id=18,
                                                price='".$termek[5]."'");
            }
        }
        fclose($handle);
        unlink("../piacter/".$filename);

        $csv_files=scandir("../piacter/");

        $this->data['piacter_csv_listak'] = array();
        foreach($csv_files as $file_name){
            if ($file_name != "." && $file_name != ".."){
                $this->data['piacter_csv_listak'][] = array('lista_neve'      => substr($file_name,0,-4) );
            }
        }

        array_map('unlink', glob("../system/cache/*.*"));

        $json['success']=$this->data['piacter_csv_listak'];
        $this->response->setOutput(json_encode($json));


    }

    public function csvTorol() {
        $category_name=$_POST['file_name'];
        $filename=$category_name.".csv";

        unlink("../piacter/".$filename);

        $csv_files=scandir("../piacter/");

        $this->data['piacter_csv_listak'] = array();
        foreach($csv_files as $file_name){
            if ($file_name != "." && $file_name != ".."){
                $this->data['piacter_csv_listak'][] = array('lista_neve'      => substr($file_name,0,-4) );
            }
        }

        array_map('unlink', glob("../system/cache/*.*"));

        $json['success']=$this->data['piacter_csv_listak'];
        $this->response->setOutput(json_encode($json));

    }

    public function csvOlvasHiba() {

    }

    public function autocomplete() {
        $json = array();

        if (isset($this->request->get['filter_name'])) {
            $this->load->model('catalog/category');

            $data = array(
                'filter_name' => $this->request->get['filter_name'],
                'start'       => 0,
                'limit'       => 20
            );

            $results = $this->model_catalog_category->getCategoriesAutoComp($data);

            foreach ($results as $result) {
                $json[] = array(
                    'category_id' => $result['category_id'],
                    'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->response->setOutput(json_encode($json));
    }



}
?>