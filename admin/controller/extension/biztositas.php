<?php
class ControllerExtensionBiztositas extends Controller {
	public function index() {
		$this->load->language('extension/biztositas');

		$this->document->setTitle($this->language->get('heading_title'));

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/biztositas', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

        $this->data['insert'] = $this->url->link('extension/biztositas/insert', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_confirm'] = $this->language->get('text_confirm');

		$this->data['column_from'] = $this->language->get('column_from');
		$this->data['column_to'] = $this->language->get('column_to');
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_modify'] = $this->language->get('button_modify');
		$this->data['button_delete'] = $this->language->get('button_delete');
        $this->data['column_value'] = $this->language->get('column_value');
        $this->data['column_modosit'] = $this->language->get('column_modosit');
        $this->data['column_torol'] = $this->language->get('column_torol');

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->session->data['error'])) {
			$this->data['error'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} else {
			$this->data['error'] = '';
		}
        //$this->load->model('design/layout');

		$this->load->model('setting/biztositas');
        echo "";

        $biztositasok = $this->model_setting_biztositas->getBiztositasok();
        $this->data['biztositasok'] = $biztositasok;

        if (!empty($biztositasok)) {
            foreach($biztositasok as $biztositas){
                $this->data['biztositasok'][$biztositas['biztositas_id']]['action'] = $this->url->link('extension/biztositas/update', 'token=' . $this->session->data['token'] . '&biztositas_id=' . $biztositas['biztositas_id'], 'SSL');
                $this->data['biztositasok'][$biztositas['biztositas_id']]['torol']  = $this->url->link('extension/biztositas/delete', 'token=' . $this->session->data['token'] . '&biztositas_id=' . $biztositas['biztositas_id'], 'SSL');
                /*$this->data['biztositasok'][$biztositas['biztositas_id']]['action'] =  $action[$biztositas['biztositas_id']];
                $this->data['biztositasok'][$biztositas['biztositas_id']]['torol'] =  $action[$biztositas['biztositas_id']];*/
            }
        }


			$this->template = 'extension/biztositas.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

    public function insert() {
        $this->load->language('extension/biztositas');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/biztositas');

        //if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
        if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {
            $this->model_setting_biztositas->addBiztositas($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->redirect($this->url->link('extension/biztositas', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getForm();
    }
    private function getForm($mitcsinal=0) {

        //$mitcsinal - felvitel: 0
        //$mitcsinal - módosít: 1

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');

        $this->data['column_from'] = $this->language->get('column_from');
        $this->data['column_to'] = $this->language->get('column_to');
        $this->data['column_value'] = $this->language->get('column_value');

        if ($mitcsinal==0)
            $this->data['action'] = $this->url->link('extension/biztositas/insert', 'token=' . $this->session->data['token'], 'SSL');
        else
            $this->data['action'] = $this->url->link('extension/biztositas/update', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['cancel'] = $this->url->link('extension/biztositas', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('extension/biztositas', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->load->model('design/layout');

        $this->data['layouts'] = $this->model_design_layout->getLayouts();

        $this->template = 'extension/biztositas_form.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        if (isset($this->session->data['error'])) {
            $this->data['error'] = $this->session->data['error'];

            unset($this->session->data['error']);
        } else {
            $this->data['error'] = '';
        }

        $this->response->setOutput($this->render());
    }

    public function update() {
        $this->load->language('extension/biztositas');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/biztositas');
        //$this->load->model('extension/product');

        if (isset($this->request->get['biztositas_id'])){
            $getid=$this->request->get['biztositas_id'];
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "biztositas where $getid=id");
            $row= $query->row;
            $this->data['modosit'][0] = $query->row['biztositas_tol'];
            $this->data['modosit'][1] = $query->row['biztositas_ig'];
            $this->data['modosit'][2] = $query->row['biztositas_ertek'];
            $this->data['modosit'][3] = $query->row['id'];

            $this->template = 'extension/biztositas_form.tpl';
            $this->children = array(
                'common/header',
                'common/footer'
            );
            $this->getForm(1);
        }

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $data['id']=$_POST['id'];
            $data['biztositas_tol']=$_POST['biztositas_tol'];
            $data['biztositas_ig']=$_POST['biztositas_ig'];
            $data['biztositas_ertek']=$_POST['biztositas_ertek'];
            $this->db->query("UPDATE " . DB_PREFIX . "biztositas SET `biztositas_tol` = '" . $data["biztositas_tol"] . "', `biztositas_ig` = '" . $data["biztositas_ig"] . "', `biztositas_ertek` = '" . $data["biztositas_ertek"] . "' WHERE id = '" . $data["id"] . "'");

            $this->session->data['success'] = $this->language->get('text_success');
            $this->redirect($this->url->link('extension/biztositas', 'token=' . $this->session->data['token'], 'SSL'));

        }
    }

    public function delete() {
        $this->load->language('extension/biztositas');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/biztositas');

        //if (isset($this->request->post['selected']) && $this->validateDelete()) {
        if (isset($_GET['biztositas_id'])) {
            $data["id"]=$_GET['biztositas_id'];
            $this->db->query("DELETE FROM " . DB_PREFIX . "biztositas WHERE  id = '" . $data["id"] . "'");



            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('extension/biztositas', 'token=' . $this->session->data['token'], 'SSL'));
        }

       // $this->getList();
    }


    private function validateForm() {
        if (!$this->user->hasPermission('modify', 'catalog/product')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        foreach ($this->request->post['product_description'] as $language_id => $value) {
            if ((utf8_strlen($value['name']) < 1) || (utf8_strlen($value['name']) > 255)) {
                $this->error['name'][$language_id] = $this->language->get('error_name');
            }
        }

        if ((utf8_strlen($this->request->post['model']) < 1) || (utf8_strlen($this->request->post['model']) > 64)) {
            $this->error['model'] = $this->language->get('error_model');
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }


}

?>