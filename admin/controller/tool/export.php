<?php 
class ControllerToolExport extends Controller { 
	private $error = array();
	
	public function index() {
		$this->load->language('tool/export');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('tool/export');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			if ((isset( $this->request->files['upload'] )) && (is_uploaded_file($this->request->files['upload']['tmp_name']))) {
				$file = $this->request->files['upload']['tmp_name'];
				if ($this->model_tool_export->upload($file)) {
					$this->session->data['success'] = $this->language->get('text_success');
					$this->redirect($this->url->link('tool/export', 'token=' . $this->session->data['token'], 'SSL'));
				} else {
					$this->error['warning'] = $this->language->get('error_upload');
				}
			} else {
                $this->error['warning'] = $this->language->get('error_not_upload');
            }
		} elseif (isset($this->session->data['warning'])) {
            if ($this->session->data['warning'] == "1") {
                $this->error['warning'] = $this->language->get('warning_download_limit_tol');
            } elseif ($this->session->data['warning'] == "2"){
                $this->error['warning'] = $this->language->get('warning_download_limit_ig_kisebb');
            }
            unset($this->session->data['warning']);
        }

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['entry_restore'] = $this->language->get('entry_restore');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['button_import'] = $this->language->get('button_import');
		$this->data['button_export'] = $this->language->get('button_export');
		$this->data['tab_general'] = $this->language->get('tab_general');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => FALSE
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('tool/export', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);




		$this->data['token'] = $this->session->data['token'];
		$this->data['action'] = $this->url->link('tool/export', 'token=' . $this->session->data['token'], 'SSL');

		//$this->data['export'] = $this->url->link('tool/export/selects', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['letolt'] = $this->url->link('tool/export/download', 'token=' . $this->session->data['token'], 'SSL');


		$this->template = 'tool/export.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);
		$this->response->setOutput($this->render());
	}


	public function download() {
		if ($this->validate()) {

			// set appropriate memory and timeout limits
			ini_set("memory_limit","250M");
			set_time_limit( 3200 );

            $letolt_tol= (int)$this->request->request['letolt_tol'];
            $letolt_ig = (int)$this->request->request['letolt_ig'];

			// send the categories, products and options as a spreadsheet file
			$this->load->model('tool/export');
			$vissza = $this->model_tool_export->download($letolt_tol,$letolt_ig);
            if (isset($vissza) && $vissza === false) {
                $this->redirect($this->url->link('tool/export', 'token=' . $this->session->data['token'], 'SSL'));
            }
		} else {

			// return a permission error page
			return $this->forward('error/permission');
		}

	}


	private function validate() {
		if (!$this->user->hasPermission('modify', 'tool/export')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
?>