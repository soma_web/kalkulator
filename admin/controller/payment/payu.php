<?php
class ControllerPaymentPayU extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('payment/payu');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('payu', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');

        $this->data['text_language_hu'] = $this->language->get('text_language_hu');
        $this->data['text_language_en'] = $this->language->get('text_language_en');

        $this->data['entry_merchant_huf'] = $this->language->get('entry_merchant_huf');
        $this->data['entry_secret_key_huf'] = $this->language->get('entry_secret_key_huf');

        $this->data['entry_merchant_eur'] = $this->language->get('entry_merchant_eur');
        $this->data['entry_secret_key_eur'] = $this->language->get('entry_secret_key_eur');

        $this->data['entry_language'] = $this->language->get('entry_language');
        $this->data['entry_testorder'] = $this->language->get('entry_testorder');
        $this->data['entry_debug'] = $this->language->get('entry_debug');
        $this->data['entry_order_timeout'] = $this->language->get('entry_order_timeout');
        $this->data['entry_timeout_url'] = $this->language->get('entry_timeout_url');

        $this->data['entry_order_status'] = $this->language->get('entry_order_status');
        $this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_sort_order'] = $this->language->get('entry_sort_order');

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->error['merchant_eur'])) {
            $this->data['error_merchant'] = $this->error['merchant_eur'];
        } else {
            $this->data['error_merchant'] = '';
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_payment'),
            'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('payment/payu', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['action'] = $this->url->link('payment/payu', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');

        if (isset($this->request->post['payu_merchant_eur'])) {
            $this->data['payu_merchant_eur'] = $this->request->post['payu_merchant_eur'];
        } else {
            $this->data['payu_merchant_eur'] = $this->config->get('payu_merchant_eur');
        }

        if (isset($this->request->post['payu_secret_key_eur'])) {
            $this->data['payu_secret_key_eur'] = $this->request->post['payu_secret_key_eur'];
        } else {
            $this->data['payu_secret_key_eur'] = $this->config->get('payu_secret_key_eur');
        }



        if (isset($this->request->post['payu_merchant_huf'])) {
            $this->data['payu_merchant_huf'] = $this->request->post['payu_merchant_huf'];
        } else {
            $this->data['payu_merchant_huf'] = $this->config->get('payu_merchant_huf');
        }

        if (isset($this->request->post['payu_secret_key_huf'])) {
            $this->data['payu_secret_key_huf'] = $this->request->post['payu_secret_key_huf'];
        } else {
            $this->data['payu_secret_key_huf'] = $this->config->get('payu_secret_key_huf');
        }


        
        if (isset($this->request->post['payu_order_timeout'])) {
            $this->data['payu_order_timeout'] = $this->request->post['payu_order_timeout'];
        } else {
            $this->data['payu_order_timeout'] = $this->config->get('payu_order_timeout');
        }


        if (isset($this->request->post['payu_timeout_url'])) {
            $this->data['payu_timeout_url'] = $this->request->post['payu_timeout_url'];
        } else {
            $this->data['payu_timeout_url'] = $this->config->get('payu_timeout_url');
        }

        if (isset($this->request->post['payu_order_status_id'])) {
            $this->data['payu_order_status_id'] = $this->request->post['payu_order_status_id'];
        } else {
            $this->data['payu_order_status_id'] = $this->config->get('payu_order_status_id');
        }

        if (isset($this->request->post['payu_testorder'])) {
            $this->data['payu_testorder'] = $this->request->post['payu_testorder'];
        } else {
            $this->data['payu_testorder'] = $this->config->get('payu_testorder');
        }

        if (isset($this->request->post['payu_debug'])) {
            $this->data['payu_debug'] = $this->request->post['payu_debug'];
        } else {
            $this->data['payu_debug'] = $this->config->get('payu_debug');
        }


        if (isset($this->request->post['payu_language'])) {
            $this->data['payu_language'] = $this->request->post['payu_language'];
        } else {
            $this->data['payu_language'] = $this->config->get('payu_language');
        }


        $this->load->model('localisation/order_status');

        $this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        if (isset($this->request->post['payu_status'])) {
            $this->data['payu_status'] = $this->request->post['payu_status'];
        } else {
            $this->data['payu_status'] = $this->config->get('payu_status');
        }

        if (isset($this->request->post['payu_sort_order'])) {
            $this->data['payu_sort_order'] = $this->request->post['payu_sort_order'];
        } else {
            $this->data['payu_sort_order'] = $this->config->get('payu_sort_order');
        }

        $this->template = 'payment/payu.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    private function validate() {
        if (!$this->user->hasPermission('modify', 'payment/payu')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->request->post['payu_merchant_eur']) {
            $this->error['merchant_eur'] = $this->language->get('error_merchant');
        }


        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
}

?>