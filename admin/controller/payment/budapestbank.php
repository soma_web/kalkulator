<?php
require_once(DIR_SYSTEM . 'library/merchant.php');

class ControllerPaymentBudapestbank extends Controller {
    private $error = array();

    public function index() {

        $this->checkDB();

        //$this->load->language('payment/pp_standard');
        $this->load->language('payment/budapestbank');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && !isset($_GET['action']) ) { //&& $this->validate()
            //$this->model_setting_setting->editSetting('pp_standard', $this->request->post);
            $this->model_setting_setting->editSetting('budapestbank', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_all_zones'] = $this->language->get('text_all_zones');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');
        $this->data['text_authorization'] = $this->language->get('text_authorization');
        $this->data['text_sale'] = $this->language->get('text_sale');

        //new
        $this->data['text_sms'] = $this->language->get('text_sms');
        $this->data['text_dms'] = $this->language->get('text_dms');

        $this->data['token'] = '&token=' . $this->session->data['token'];

            //MUVELETEK

                if($this->config->get('budapestbank_test')) {
                    //!!!TEST MODE!!!!
                    $ecomm_server_url     = 'https://secureshop-test.firstdata.lv:8443/ecomm/MerchantHandler';
                    $ecomm_client_url     = 'https://secureshop-test.firstdata.lv/ecomm/ClientHandler';
                    //!!!TEST MODE!!!!
                } else {
                    //!!! THIS IS NOT TEST MODE - WATCH !!! /*USE THIS WHEN YOU GO TO PRODUCTION SYSTEM, ALSO CHANGE KEYSTORE AND PASSWORD*/
                    $ecomm_server_url     = 'http://index.hu'; //'https://secureshop.firstdata.lv:8443/ecomm/MerchantHandler'
                    $ecomm_client_url     = 'http://index.hu'; //'https://secureshop.firstdata.lv/ecomm/ClientHandler'
                    //!!! THIS IS NOT TEST MODE - WATCH !!!
                }

            if(isset($_GET['action'])) {
                switch($_GET['action']) {

                    case 'confirm_dms':
                        $id = $_GET['id']; //for getting MySQL data

                        $sql = $this->db->query("SELECT * FROM transaction WHERE `id`='$id'");

                        if (!$sql) {
                            die('*** Invalid query: ' . mysql_error());
                        }

                        $row = $sql->row;
                        $amount = $row['amount'];

                            $this->data['confirmdata'] = '<form action="index.php?route=payment/budapestbank&action=makedmstrans&id='.$id.'&token=' . $this->session->data['token'].'" method="POST">
                            DMS transaction amount: <input type="text" name="amount" value="'.$amount.'" /><br>
                            <input type="submit" Value="Make DMS" />
                            </form>';

                        //mysql_close($link);

                        break;

                    //CASE MAKE DMS TRANS
                    case 'makedmstrans':
                        $id = $_GET['id']; //for getting MySQL data

                        $merchant = new Merchant($ecomm_server_url, DIR_SYSTEM . $this->config->get('budapestbank_cert_url'), $this->config->get('budapestbank_cert_pass'), 1);

                        $result = $this->db->query("SELECT * FROM transaction WHERE `id`='$id'");
                        if (!$result) {
                            die('*** Invalid query: ' . mysql_error());
                        }
                        $row = $result->row;
                        $auth_id = urlencode($row['trans_id']);
                        $amount = $_POST['amount'];
                        $currency = urlencode($row['currency']);
                        $ip = urlencode($row['client_ip_addr']);
                        $desc = urlencode($row['description']);
                        $language = urlencode($row['language']);

                        $resp = $merchant->makeDMSTrans($auth_id, $amount, $currency, $ip, $desc, $language);

                        if (substr($resp,8,2)=="OK") {
                            $trans_id = $row['trans_id'];

                            $result = $this->db->query("UPDATE transaction SET `dms_ok` = 'YES', makeDMS_amount = '$amount' WHERE `trans_id` = '$trans_id'");

                            if (!$result) {
                                die('*** Invalid query: ' . mysql_error());
                            }
                            $this->data['confirmdata'] = $resp;
                        }
                        else{
                            $this->data['confirmdata'] = $resp;
                            $resp = htmlentities($resp, ENT_QUOTES);
                            $sql = $this->db->query("INSERT INTO error VALUES ('', now(), 'makeDMSTrans', '$resp')");

                            if (!$sql) {
                                die('*** Invalid query2: ' . mysql_error());
                            }
                        }
                        //echo '<br><br> <a href="?action=select">Back</a>';
                        //mysql_close($link);
                        break;


                    //CASE CONFIRM_REVERSE
                    case 'confirm_reverse':
                        $id = $_GET['id']; //for getting MySQL data

                        $sql = $this->db->query("SELECT * FROM transaction WHERE `id`='$id'");

                        if (!$sql) {
                            die('*** Invalid query: ' . mysql_error());
                        }
                        $row = $sql->row;
                        $trans_id = urlencode($row['trans_id']);
                        $amount = $row['amount'];

                        $this->data['confirmdata'] = '<form action="index.php?route=payment/budapestbank&action=reverse&id='.$id.'&token=' . $this->session->data['token'].'" method="POST">
                            Reverse amount: <input type="text" name="amount" value="'.$amount.'" /><br>
                            <input type="submit" Value="Reverse" />
                            </form>';

                        //mysql_close($link);
                        break;

            //CASE REVERSE
            case 'reverse':

                $id = $_GET['id']; //for getting MySQL data

                $sql = $this->db->query("SELECT * FROM transaction WHERE `id`='$id'");

                if (!$sql) {
                    die('*** Invalid query: ' . mysql_error());
                }
                $row = $sql->row;
                $trans_id = urlencode($row['trans_id']);
                $amount = $_POST['amount'];

                if($amount == '0'){
                    die("Amount invalid: $amount <br><br><a href=\"javascript:history.go(-1)\"><< Back </a>");
                }
                $merchant = new Merchant($ecomm_server_url, DIR_SYSTEM . $this->config->get('budapestbank_cert_url'), $this->config->get('budapestbank_cert_pass'), 1);
                $resp = $merchant->reverse($trans_id, $amount);

                if (substr($resp,8,2) == "OK" OR substr($resp,8,8) == "REVERSED") {

                    if (strstr($resp, 'RESULT:')) {
                        $result = explode('RESULT: ', $resp);
                        $result = preg_split( '/\r\n|\r|\n/', $result[1] );
                        $result = $result[0];
                    }else{
                        $result = '';
                    }

                    if (strstr($resp, 'RESULT_CODE:')) {
                        $result_code = explode('RESULT_CODE: ', $resp);
                        $result_code = preg_split( '/\r\n|\r|\n/', $result_code[1] );
                        $result_code = $result_code[0];
                    }else{
                        $result_code = '';
                    }

                    $trans_id = $row['trans_id'];

                    $sql = $this->db->query("UPDATE transaction SET `reversal_amount` = '$amount', `result_code` = '$result_code', `result` = '$result', `response` = '$resp' WHERE `trans_id` = '$trans_id'");

                    if (!$sql) {
                        die('*** Invalid query: ' . mysql_error());
                    }else{
                        $this->data['confirmadata'] = $resp;
                    }
                }
                else{
                    $this->data['confirmdata'] = $resp;
                    $resp = htmlentities($resp, ENT_QUOTES);
                    $sql = $this->db->query("INSERT INTO error VALUES ('', now(), 'reverse', '$resp')");

                    if (!$sql) {
                        die('*** Invalid query222: ' . mysql_error());
                    }
                }
                //echo '<br><br><a href="?action=select">Back to Transaction list</a><br>';
                //mysql_close($link);
                break;


            //CASE UPDATE
            case 'update':
                $id = $_GET['id']; //for getting MySQL data

                $sql = $this->db->query("SELECT * FROM transaction WHERE `id`='$id'");

                if (!$sql) {
                    die('*** Invalid query: ' . mysql_error());
                }
                $row = $sql->row;
                $trans_id = $row['trans_id'];
                $client_ip_addr = $row['client_ip_addr'];

                $merchant = new Merchant($ecomm_server_url, DIR_SYSTEM . $this->config->get('budapestbank_cert_url'), $this->config->get('budapestbank_cert_pass'), 1);

                $resp = $merchant->getTransResult(urlencode($trans_id), $client_ip_addr);

                if (strstr($resp, 'RESULT:')) {
                    //$resp example RESULT: OK RESULT_CODE: 000 3DSECURE: NOTPARTICIPATED RRN: 915300393049 APPROVAL_CODE: 705368 CARD_NUMBER: 4***********9913

                    if (strstr($resp, 'RESULT:')) {
                        $result = explode('RESULT: ', $resp);
                        $result = preg_split( '/\r\n|\r|\n/', $result[1] );
                        $result = $result[0];
                    }else{
                        $result = '';
                    }

                    if (strstr($resp, 'RESULT_CODE:')) {
                        $result_code = explode('RESULT_CODE: ', $resp);
                        $result_code = preg_split( '/\r\n|\r|\n/', $result_code[1] );
                        $result_code = $result_code[0];
                    }else{
                        $result_code = '';
                    }

                    if (strstr($resp, '3DSECURE:')) {
                        $result_3dsecure = explode('3DSECURE: ', $resp);
                        $result_3dsecure = preg_split( '/\r\n|\r|\n/', $result_3dsecure[1] );
                        $result_3dsecure = $result_3dsecure[0];
                    }else{
                        $result_3dsecure = '';
                    }

                    if (strstr($resp, 'CARD_NUMBER:')) {
                        $card_number = explode('CARD_NUMBER: ', $resp);
                        $card_number = preg_split( '/\r\n|\r|\n/', $card_number[1] );
                        $card_number = $card_number[0];
                    }else{
                        $card_number = '';
                    }

                    $sql = $this->db->query("UPDATE transaction SET
                          `result` = '$result',
                          `result_code` = '$result_code',
                          `result_3dsecure` = '$result_3dsecure',
                          `card_number` = '$card_number',
                          `response` = '$resp'
                          WHERE `trans_id` = '$trans_id'");

                    $this->data['confirmdata'] = $resp;

                }else{

                    $this->data['confirmdata'] = $resp;
                    $resp = htmlentities($resp, ENT_QUOTES);
                    $sql = $this->db->query("INSERT INTO error VALUES ('', now(), 'update', '$resp')");

                    if (!$sql) {
                        die('*** Invalid query2: ' . mysql_error());
                    }

                }

                //echo '<br><br><a href="?action=select">Back to Transaction list</a><br>';

                break;

            }

            }
            //MUVELETEK

        //new

        $this->data['entry_email'] = $this->language->get('entry_email');
        $this->data['entry_test'] = $this->language->get('entry_test');
        $this->data['entry_transaction'] = $this->language->get('entry_transaction');
        $this->data['entry_debug'] = $this->language->get('entry_debug');
        $this->data['entry_total'] = $this->language->get('entry_total');
        $this->data['entry_canceled_reversal_status'] = $this->language->get('entry_canceled_reversal_status');
        $this->data['entry_completed_status'] = $this->language->get('entry_completed_status');
        $this->data['entry_denied_status'] = $this->language->get('entry_denied_status');
        $this->data['entry_expired_status'] = $this->language->get('entry_expired_status');
        $this->data['entry_failed_status'] = $this->language->get('entry_failed_status');
        $this->data['entry_pending_status'] = $this->language->get('entry_pending_status');
        $this->data['entry_processed_status'] = $this->language->get('entry_processed_status');
        $this->data['entry_refunded_status'] = $this->language->get('entry_refunded_status');
        $this->data['entry_reversed_status'] = $this->language->get('entry_reversed_status');
        $this->data['entry_voided_status'] = $this->language->get('entry_voided_status');
        $this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
        $this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_sort_order'] = $this->language->get('entry_sort_order');

        //NEW
        $this->data['entry_process_or_msg'] = $this->language->get('entry_process_or_msg');
        $this->data['entry_deviza'] = $this->language->get('entry_deviza');
        $this->data['entry_cert_url'] = $this->language->get('entry_cert_url');
        $this->data['entry_cert_pass'] = $this->language->get('entry_cert_pass');
        $this->data['entry_client_ip'] = $this->language->get('entry_client_ip');
        $this->data['entry_client_language'] = $this->language->get('entry_client_language');
        //NEW

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->error['email'])) {
            $this->data['error_email'] = $this->error['email'];
        } else {
            $this->data['error_email'] = '';
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_payment'),
            'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            //'href'      => $this->url->link('payment/pp_standard', 'token=' . $this->session->data['token'], 'SSL'),
            'href'      => $this->url->link('payment/budapestbank', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        //$this->data['action'] = $this->url->link('payment/pp_standard', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['action'] = $this->url->link('payment/budapestbank', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');

        if (isset($this->request->post['budapestbank_email'])) {
            $this->data['budapestbank_email'] = $this->request->post['budapestbank_email'];
        } else {
            $this->data['budapestbank_email'] = $this->config->get('budapestbank_email');
        }

        if (isset($this->request->post['budapestbank_test'])) {
            $this->data['budapestbank_test'] = $this->request->post['budapestbank_test'];
        } else {
            $this->data['budapestbank_test'] = $this->config->get('budapestbank_test');
        }

        if (isset($this->request->post['budapestbank_transaction'])) {
            $this->data['budapestbank_transaction'] = $this->request->post['budapestbank_transaction'];
        } else {
            $this->data['budapestbank_transaction'] = $this->config->get('budapestbank_transaction');
        }

        if (isset($this->request->post['budapestbank_debug'])) {
            $this->data['budapestbank_debug'] = $this->request->post['budapestbank_debug'];
        } else {
            $this->data['budapestbank_debug'] = $this->config->get('budapestbank_debug');
        }

        if (isset($this->request->post['budapestbank_total'])) {
            $this->data['budapestbank_total'] = $this->request->post['budapestbank_total'];
        } else {
            $this->data['budapestbank_total'] = $this->config->get('budapestbank_total');
        }

        if (isset($this->request->post['budapestbank_canceled_reversal_status_id'])) {
            $this->data['budapestbank_canceled_reversal_status_id'] = $this->request->post['budapestbank_canceled_reversal_status_id'];
        } else {
            $this->data['budapestbank_canceled_reversal_status_id'] = $this->config->get('budapestbank_canceled_reversal_status_id');
        }

        if (isset($this->request->post['budapestbank_completed_status_id'])) {
            $this->data['budapestbank_completed_status_id'] = $this->request->post['budapestbank_completed_status_id'];
        } else {
            $this->data['budapestbank_completed_status_id'] = $this->config->get('budapestbank_completed_status_id');
        }

        if (isset($this->request->post['budapestbank_denied_status_id'])) {
            $this->data['budapestbank_denied_status_id'] = $this->request->post['budapestbank_denied_status_id'];
        } else {
            $this->data['budapestbank_denied_status_id'] = $this->config->get('budapestbank_denied_status_id');
        }

        if (isset($this->request->post['budapestbank_expired_status_id'])) {
            $this->data['budapestbank_expired_status_id'] = $this->request->post['budapestbank_expired_status_id'];
        } else {
            $this->data['budapestbank_expired_status_id'] = $this->config->get('budapestbank_expired_status_id');
        }

        if (isset($this->request->post['budapestbank_failed_status_id'])) {
            $this->data['budapestbank_failed_status_id'] = $this->request->post['budapestbank_failed_status_id'];
        } else {
            $this->data['budapestbank_failed_status_id'] = $this->config->get('budapestbank_failed_status_id');
        }

        if (isset($this->request->post['budapestbank_pending_status_id'])) {
            $this->data['budapestbank_pending_status_id'] = $this->request->post['budapestbank_pending_status_id'];
        } else {
            $this->data['budapestbank_pending_status_id'] = $this->config->get('budapestbank_pending_status_id');
        }

        if (isset($this->request->post['budapestbank_processed_status_id'])) {
            $this->data['budapestbank_processed_status_id'] = $this->request->post['budapestbank_processed_status_id'];
        } else {
            $this->data['budapestbank_processed_status_id'] = $this->config->get('budapestbank_processed_status_id');
        }

        if (isset($this->request->post['budapestbank_refunded_status_id'])) {
            $this->data['budapestbank_refunded_status_id'] = $this->request->post['budapestbank_refunded_status_id'];
        } else {
            $this->data['budapestbank_refunded_status_id'] = $this->config->get('budapestbank_refunded_status_id');
        }

        if (isset($this->request->post['budapestbank_reversed_status_id'])) {
            $this->data['budapestbank_reversed_status_id'] = $this->request->post['budapestbank_reversed_status_id'];
        } else {
            $this->data['budapestbank_reversed_status_id'] = $this->config->get('budapestbank_reversed_status_id');
        }

        if (isset($this->request->post['budapestbank_voided_status_id'])) {
            $this->data['budapestbank_voided_status_id'] = $this->request->post['budapestbank_voided_status_id'];
        } else {
            $this->data['budapestbank_voided_status_id'] = $this->config->get('budapestbank_voided_status_id');
        }

        //NEW

        if (isset($this->request->post['budapestbank_deviza'])) {
            $this->data['budapestbank_deviza'] = $this->request->post['budapestbank_deviza'];
        } else {
            $this->data['budapestbank_deviza'] = $this->config->get('budapestbank_deviza');
        }

        if (isset($this->request->post['budapestbank_cert_url'])) {
            $this->data['budapestbank_cert_url'] = $this->request->post['budapestbank_cert_url'];
        } else {
            $this->data['budapestbank_cert_url'] = $this->config->get('budapestbank_cert_url');
        }

        if (isset($this->request->post['budapestbank_cert_pass'])) {
            $this->data['budapestbank_cert_pass'] = $this->request->post['budapestbank_cert_pass'];
        } else {
            $this->data['budapestbank_cert_pass'] = $this->config->get('budapestbank_cert_pass');
        }

        if (isset($this->request->post['budapestbank_client_ip'])) {
            $this->data['budapestbank_client_ip'] = $this->request->post['budapestbank_client_ip'];
        } else {
            $this->data['budapestbank_client_ip'] = $this->config->get('budapestbank_client_ip');
        }

        if (isset($this->request->post['budapestbank_client_language'])) {
            $this->data['budapestbank_client_language'] = $this->request->post['budapestbank_client_language'];
        } else {
            $this->data['budapestbank_client_language'] = $this->config->get('budapestbank_client_language');
        }


            $this->data['bptrans'] = $this->db->query('SELECT * FROM transaction ORDER by `id` ASC')->rows;
            $this->data['bperror'] = $this->db->query('SELECT * FROM error ORDER by `id` ASC')->rows;

        //NEW

        $this->load->model('localisation/order_status');

        $this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        if (isset($this->request->post['budapestbank_geo_zone_id'])) {
            $this->data['budapestbank_geo_zone_id'] = $this->request->post['budapestbank_geo_zone_id'];
        } else {
            $this->data['budapestbank_geo_zone_id'] = $this->config->get('budapestbank_geo_zone_id');
        }

        $this->load->model('localisation/geo_zone');

        $this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

        if (isset($this->request->post['budapestbank_status'])) {
            $this->data['budapestbank_status'] = $this->request->post['budapestbank_status'];
        } else {
            $this->data['budapestbank_status'] = $this->config->get('budapestbank_status');
        }

        if (isset($this->request->post['budapestbank_sort_order'])) {
            $this->data['budapestbank_sort_order'] = $this->request->post['budapestbank_sort_order'];
        } else {
            $this->data['budapestbank_sort_order'] = $this->config->get('budapestbank_sort_order');
        }

        //$this->template = 'payment/pp_standard.tpl';
        $this->template = 'payment/budapestbank.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    //private function test() { }

    private function validate() {
        //if (!$this->user->hasPermission('modify', 'payment/pp_standard')) {
        if (!$this->user->hasPermission('modify', 'payment/budapestbank')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->request->post['budapestbank_email']) {
            $this->error['email'] = $this->language->get('error_email');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    function checkDB() {

        $this->db->query("CREATE TABLE /*!32312 IF NOT EXISTS*/ transaction  (
          id INT(10) NOT NULL AUTO_INCREMENT,
          trans_id VARCHAR(50),
          amount INT(10),
          currency INT(10),
          client_ip_addr VARCHAR(50),
          description TEXT,
          language VARCHAR(50),
          dms_ok VARCHAR(50),
          result VARCHAR(50),
          result_code VARCHAR(50),
          result_3dsecure VARCHAR(50),
          card_number VARCHAR(50),
          t_date VARCHAR(20),
          response TEXT,
          reversal_amount INT(10),
          makeDMS_amount INT(10),
          PRIMARY KEY (id)
          )
          ");

        $this->db->query("CREATE TABLE /*!32312 IF NOT EXISTS*/ batch  (
          id INT(10) NOT NULL AUTO_INCREMENT,
          result TEXT,
          result_code VARCHAR(3),
          count_reversal VARCHAR(10),
          count_transaction VARCHAR(10),
          amount_reversal VARCHAR(16),
          amount_transaction VARCHAR(16),
          close_date VARCHAR(20),
          response TEXT,
          PRIMARY KEY (id)
          )
          ");

        $this->db->query("CREATE TABLE /*!32312 IF NOT EXISTS*/ error  (
          id INT(10) NOT NULL AUTO_INCREMENT,
          error_time VARCHAR (20),
          action VARCHAR (20),
          response TEXT,
          PRIMARY KEY (id)
          )
          ");
    }
}
?>