<?php
class ControllerDesignMegjelenitoFrontend extends Controller {
    private $error = array();

    public function index() {

        $this->load->language('design/megjelenito');

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {

            foreach($this->request->post as $key=>$value) {
                $beallit[$key] = $value;
            }

            $this->document->setTitle($this->language->get('heading_title'));
            $this->load->model('setting/setting');

            $this->model_setting_setting->editSetting('frontend_beallitasok', $beallit);

            $this->load->model('setting/extension');

            $this->session->data['success'] = $this->language->get('text_success');

            $this->redirect($this->url->link('design/megjelenito_frontend', 'token=' . $this->session->data['token'], 'SSL'));

        }

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');

        $this->data['entry_megnevezes']          = $this->language->get('entry_megnevezes');
        $this->data['entry_keyword']             = $this->language->get('entry_keyword');
        $this->data['entry_meta_description']    = $this->language->get('entry_meta_description');
        $this->data['entry_description']         = $this->language->get('entry_description');
        $this->data['entry_tag']                 = $this->language->get('entry_tag');
        $this->data['entry_filter']              = $this->language->get('entry_filter');
        $this->data['entry_images']              = $this->language->get('entry_images');
        $this->data['entry_category']         = $this->language->get('entry_category');
        $this->data['entry_termek_filter_csoport'] = $this->language->get('entry_termek_filter_csoport');
        $this->data['entry_model']            = $this->language->get('entry_model');
        $this->data['entry_cikkszam']         = $this->language->get('entry_cikkszam');
        $this->data['entry_cikkszam2']        = $this->language->get('entry_cikkszam2');
        $this->data['entry_filter']           = $this->language->get('entry_filter');
        $this->data['entry_termek_filter_csoport']     = $this->language->get('entry_termek_filter_csoport');
        $this->data['entry_sku']              = $this->language->get('entry_sku');
        $this->data['entry_upc']              = $this->language->get('entry_upc');
        $this->data['entry_location']         = $this->language->get('entry_location');
        $this->data['entry_manufacturer']     = $this->language->get('entry_manufacturer');
        $this->data['entry_shipping']         = $this->language->get('entry_shipping');
        $this->data['entry_date_available']   = $this->language->get('entry_date_available');
        $this->data['entry_date_kaphato_ig']  = $this->language->get('entry_date_kaphato_ig');
        $this->data['entry_quantity']         = $this->language->get('entry_quantity');
        $this->data['mennyisegi_egyseg']      = $this->language->get('mennyisegi_egyseg');
        $this->data['csomagolasi_egyseg']     = $this->language->get('csomagolasi_egyseg');
        $this->data['csomagolasi_mennyiseg']  = $this->language->get('csomagolasi_mennyiseg');

        $this->data['entry_minimum']          = $this->language->get('entry_minimum');
        $this->data['entry_stock_status']     = $this->language->get('entry_stock_status');
        $this->data['entry_price']            = $this->language->get('entry_price');
        $this->data['entry_tax_class']        = $this->language->get('entry_tax_class');
        $this->data['entry_points']           = $this->language->get('entry_points');
        $this->data['entry_option_points']    = $this->language->get('entry_option_points');
        $this->data['entry_subtract']         = $this->language->get('entry_subtract');
        $this->data['entry_weight_class']     = $this->language->get('entry_weight_class');
        $this->data['entry_weight']           = $this->language->get('entry_weight');
        $this->data['entry_length']           = $this->language->get('entry_length');
        $this->data['entry_dimension']        = $this->language->get('entry_dimension');
        $this->data['entry_image']            = $this->language->get('entry_image');
        $this->data['entry_customer_group']   = $this->language->get('entry_customer_group');
        $this->data['vevo_kedvezmeny']        = $this->language->get('vevo_kedvezmeny');
        $this->data['entry_date_start']       = $this->language->get('entry_date_start');
        $this->data['entry_date_end']         = $this->language->get('entry_date_end');
        $this->data['entry_priority']         = $this->language->get('entry_priority');
        $this->data['entry_attribute']        = $this->language->get('entry_attribute');
        $this->data['entry_attribute_group']  = $this->language->get('entry_attribute_group');
        $this->data['entry_text']             = $this->language->get('entry_text');
        $this->data['entry_option']           = $this->language->get('entry_option');
        $this->data['entry_option_value']     = $this->language->get('entry_option_value');
        $this->data['entry_required']         = $this->language->get('entry_required');
        $this->data['entry_status']           = $this->language->get('entry_status');
        $this->data['entry_sort_order']       = $this->language->get('entry_sort_order');
        $this->data['entry_category']         = $this->language->get('entry_category');
        $this->data['entry_download']         = $this->language->get('entry_download');
        $this->data['entry_related']          = $this->language->get('entry_related');
        $this->data['entry_tag']   	          = $this->language->get('entry_tag');
        $this->data['entry_reward']           = $this->language->get('entry_reward');
        $this->data['entry_layout']           = $this->language->get('entry_layout felülbírálása');
        $this->data['entry_stock_status_id']             = $this->language->get('entry_stock_status_id');
        $this->data['entry_manufacturer_id']             = $this->language->get('entry_manufacturer_id');
        $this->data['entry_tax_class_id']                = $this->language->get('entry_tax_class_id');
        $this->data['entry_weight_class_id']             = $this->language->get('entry_weight_class_id');
        $this->data['entry_width']                       = $this->language->get('entry_width');
        $this->data['entry_height']                      = $this->language->get('entry_height');
        $this->data['entry_length_class_id']             = $this->language->get('entry_length_class_id');
        $this->data['entry_date_added']                  = $this->language->get('entry_date_added');
        $this->data['entry_date_modified']               = $this->language->get('entry_date_modified');
        $this->data['entry_viewed']                      = $this->language->get('entry_viewed');
        $this->data['entry_megyseg']                     = $this->language->get('entry_megyseg');
        $this->data['entry_csomagolasi_egyseg']          = $this->language->get('entry_csomagolasi_egyseg');
        $this->data['entry_ar_latszik']                  = $this->language->get('entry_ar_latszik');
        $this->data['entry_date_ervenyes_ig']            = $this->language->get('entry_date_ervenyes_ig');
        $this->data['entry_darabaru_meter']              = $this->language->get('entry_darabaru_meter');
        $this->data['entry_frontend_input_neve']         = $this->language->get('entry_frontend_input_neve');
        $this->data['entry_form_termek_elhelyezkedes']   = $this->language->get('entry_form_termek_elhelyezkedes');
        $this->data['entry_termek_kiemeles_modositasa']  = $this->language->get('entry_termek_kiemeles_modositasa');
        $this->data['entry_kepek_helyszinenkent']        = $this->language->get('entry_kepek_helyszinenkent');
        $this->data['entry_tab_videok']                  = $this->language->get('entry_tab_videok');
        $this->data['entry_tab_pdf']                     = $this->language->get('entry_tab_pdf');
        $this->data['entry_emailreg_jelszo']             = $this->language->get('entry_emailreg_jelszo');
        $this->data['entry_admin_rovid_leiras']          = $this->language->get('entry_admin_rovid_leiras');
        $this->data['entry_admin_tesztek']               = $this->language->get('entry_admin_tesztek');
        $this->data['entry_admin_tartozekok']            = $this->language->get('entry_admin_tartozekok');
        $this->data['entry_admin_altalanos_tartozekok']  = $this->language->get('entry_admin_altalanos_tartozekok');
        $this->data['entry_product_gyarto_termeknevben'] = $this->language->get('entry_product_gyarto_termeknevben');
        $this->data['entry_video']                       = $this->language->get('entry_video');
        $this->data['entry_lizing']                      = $this->language->get('entry_lizing');
        $this->data['entry_admin_szazalek'] = $this->language->get('entry_admin_szazalek');
        $this->data['entry_admin_eredeti_ar'] = $this->language->get('entry_admin_eredeti_ar');
        $this->data['entry_form_utalvany'] = $this->language->get('entry_form_utalvany');
        $this->data['entry_nokep_megjelenites_tiltasa'] = $this->language->get('entry_nokep_megjelenites_tiltasa');
        $this->data['entry_kep_megjelenites_tiltasa'] = $this->language->get('entry_kep_megjelenites_tiltasa');
        $this->data['entry_lejarat_nelkuli']            = $this->language->get('entry_lejarat_nelkuli');
        $this->data['entry_elolnezet']           = $this->language->get('entry_elolnezet');
        $this->data['entry_csomagolasi_mennyiseg'] = $this->language->get('entry_csomagolasi_mennyiseg');
        $this->data['entry_admin_letoltheto_kep']  = $this->language->get('entry_admin_letoltheto_kep');
        $this->data['entry_admin_elhelyezkedes'] = $this->language->get('entry_admin_elhelyezkedes');
        $this->data['entry_admin_biztonsagi_kod'] = $this->language->get('entry_admin_biztonsagi_kod');
        $this->data['entry_admin_letoltheto'] = $this->language->get('entry_admin_letoltheto');
        $this->data['entry_admin_sorrend'] = $this->language->get('entry_admin_sorrend');
        $this->data['entry_ellenorzes']          = $this->language->get('entry_ellenorzes');
        $this->data['entry_default']            = $this->language->get('entry_default');
        $this->data['tab_frontend_adat_bekeres'] = $this->language->get('tab_frontend_adat_bekeres');
        $this->data['tab_frontend_lista_megjeleni'] = $this->language->get('tab_frontend_lista_megjeleni');

        if (isset($this->request->post['megjelenit_admin_product'])) {
            $admin = $this->request->post['megjelenit_admin_product'];
        } else {
            $admin = $this->config->get("megjelenit_admin_product");
        }

        $lejarat_nelkuli = array(
            'lejarat_nelkuli'           => isset($admin['lejarat_nelkuli']) ? $admin['lejarat_nelkuli'] : 0,
            'lejarat_nelkuli_neve'      => isset($admin['lejarat_nelkuli_neve']) ? $admin['lejarat_nelkuli_neve'] : "",
            'lejarat_nelkuli_default'   => isset($admin['lejarat_nelkuli_default']) ? $admin['lejarat_nelkuli_default'] : 0
        );
        $this->data['lejarat_nelkuli'] = $lejarat_nelkuli;


        if (isset($this->session->data['success']))
            $this->data['success'] = $this->language->get('text_success');
        else
            $this->data['success'] = false;

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('design/megjelenito_frontend', 'token=' . $this->session->data['token'], 'SSL'),

            'separator' => ' :: '
        );

        $this->data['action'] = $this->url->link('design/megjelenito_frontend', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['cancel'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['token'] = $this->session->data['token'];

        if (isset($this->request->post['megjelenit_admin_product'])) {
            $admin = $this->request->post['megjelenit_admin_product'];
        } else {
            $admin = $this->config->get("megjelenit_admin_product");
        }

        if (isset($this->request->post['megjelenit_modul_frontend'])) {
            $this->data['megjelenit_modul_frontend'] = $this->request->post['megjelenit_modul_frontend'];
        } else {
            $this->data['megjelenit_modul_frontend'] = $this->config->get('megjelenit_modul_frontend');
        }
        if (isset($this->request->post['megjelenit_frontend_sajat_termek'])) {
            $this->data['megjelenit_frontend_sajat_termek'] = $this->request->post['megjelenit_frontend_sajat_termek'];
        } else {
            $this->data['megjelenit_frontend_sajat_termek'] = $this->config->get('megjelenit_frontend_sajat_termek');
        }
        if (isset($this->request->post['megjelenit_frontend_termek_felvitel'])) {
            $this->data['megjelenit_frontend_termek_felvitel'] = $this->request->post['megjelenit_frontend_termek_felvitel'];
        } else {
            $this->data['megjelenit_frontend_termek_felvitel'] = $this->config->get('megjelenit_frontend_termek_felvitel');
        }
        if (isset($this->request->post['megjelenit_frontend_jelentesek'])) {
            $this->data['megjelenit_frontend_jelentesek'] = $this->request->post['megjelenit_frontend_jelentesek'];
        } else {
            $this->data['megjelenit_frontend_jelentesek'] = $this->config->get('megjelenit_frontend_jelentesek');
        }
        if (isset($this->request->post['megjelenit_frontend_ertesitesi_jelentes'])) {
            $this->data['megjelenit_frontend_ertesitesi_jelentes'] = $this->request->post['megjelenit_frontend_ertesitesi_jelentes'];
        } else {
            $this->data['megjelenit_frontend_ertesitesi_jelentes'] = $this->config->get('megjelenit_frontend_ertesitesi_jelentes');
        }
        if (isset($this->request->post['megjelenit_frontend_letöltések'])) {
            $this->data['megjelenit_frontend_letöltések'] = $this->request->post['megjelenit_frontend_letöltések'];
        } else {
            $this->data['megjelenit_frontend_letöltések'] = $this->config->get('megjelenit_frontend_letöltések');
        }
        if (isset($this->request->post['megjelenit_frontend_megnezett'])) {
            $this->data['megjelenit_frontend_megnezett'] = $this->request->post['megjelenit_frontend_megnezett'];
        } else {
            $this->data['megjelenit_frontend_megnezett'] = $this->config->get('megjelenit_frontend_megnezett');
        }
        if (isset($this->request->post['megjelenit_frontend_letoltott'])) {
            $this->data['megjelenit_frontend_letoltott'] = $this->request->post['megjelenit_frontend_letoltott'];
        } else {
            $this->data['megjelenit_frontend_letoltott'] = $this->config->get('megjelenit_frontend_letoltott');
        }
        if (isset($this->request->post['megjelenit_frontend_hatteradatok'])) {
            $this->data['megjelenit_frontend_hatteradatok'] = $this->request->post['megjelenit_frontend_hatteradatok'];
        } else {
            $this->data['megjelenit_frontend_hatteradatok'] = $this->config->get('megjelenit_frontend_hatteradatok');
        }
        if (isset($this->request->post['megjelenit_frontend_utalvany'])) {
            $this->data['megjelenit_frontend_utalvany'] = $this->request->post['megjelenit_frontend_utalvany'];
        } else {
            $this->data['megjelenit_frontend_utalvany'] = $this->config->get('megjelenit_frontend_utalvany');
        }
        if (isset($this->request->post['megjelenit_frontend_arajanlat'])) {
            $this->data['megjelenit_frontend_arajanlat'] = $this->request->post['megjelenit_frontend_arajanlat'];
        } else {
            $this->data['megjelenit_frontend_arajanlat'] = $this->config->get('megjelenit_frontend_arajanlat');
        }







        $megjelenit_admin_product = array(
            'filter_csoport'   => isset($admin['filter_csoport']) ? $admin['filter_csoport'] : 0,
            'filter_csoport_sorrend' => isset($admin['filter_csoport_sorrend']) ? $admin['filter_csoport_sorrend'] : 0,
            'filter_csoport_neve' => isset($admin['filter_csoport_neve']) ? $admin['filter_csoport_neve'] : "",
            'filter_csoport_ellenorzes' => isset($admin['filter_csoport_ellenorzes']) ? $admin['filter_csoport_ellenorzes'] : 0
        );
        $this->data['filter_csoport'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'megnevezes'   => isset($admin['megnevezes']) ? $admin['megnevezes'] : 0,
            'megnevezes_sorrend' => isset($admin['megnevezes_sorrend']) ? $admin['megnevezes_sorrend'] : 0,
            'megnevezes_neve' => isset($admin['megnevezes_neve']) ? $admin['megnevezes_neve'] : "",
            'megnevezes_ellenorzes' => isset($admin['megnevezes_ellenorzes']) ? $admin['megnevezes_ellenorzes'] : 0
        );
        $this->data['megnevezes'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'keyword'   => isset($admin['keyword']) ? $admin['keyword'] : 0,
            'keyword_sorrend' => isset($admin['keyword_sorrend']) ? $admin['keyword_sorrend'] : 0,
            'keyword_neve' => isset($admin['keyword_neve']) ? $admin['keyword_neve'] : "",
            'keyword_ellenorzes' => isset($admin['keyword_ellenorzes']) ? $admin['keyword_ellenorzes'] : 0
        );
        $this->data['keyword'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'meta_description'   => isset($admin['meta_description']) ? $admin['meta_description'] : 0,
            'meta_description_sorrend' => isset($admin['meta_description_sorrend']) ? $admin['meta_description_sorrend'] : 0,
            'meta_description_neve' => isset($admin['meta_description_neve']) ? $admin['meta_description_neve'] : "",
            'meta_description_ellenorzes' => isset($admin['meta_description_ellenorzes']) ? $admin['meta_description_ellenorzes'] : 0
        );
        $this->data['meta_description'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'description'   => isset($admin['description']) ? $admin['description'] : 0,
            'description_sorrend' => isset($admin['description_sorrend']) ? $admin['description_sorrend'] : 0,
            'description_neve' => isset($admin['description_neve']) ? $admin['description_neve'] : "",
            'description_ellenorzes' => isset($admin['description_ellenorzes']) ? $admin['description_ellenorzes'] : 0
        );
        $this->data['description'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'tag'   => isset($admin['tag']) ? $admin['tag'] : 0,
            'tag_sorrend' => isset($admin['tag_sorrend']) ? $admin['tag_sorrend'] : 0,
            'tag_neve' => isset($admin['tag_neve']) ? $admin['tag_neve'] : "",
            'tag_ellenorzes' => isset($admin['tag_ellenorzes']) ? $admin['tag_ellenorzes'] : 0
        );
        $this->data['tag'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'category'   => isset($admin['category']) ? $admin['category'] : 0,
            'category_sorrend' => isset($admin['category_sorrend']) ? $admin['category_sorrend'] : 0,
            'category_neve' => isset($admin['category_neve']) ? $admin['category_neve'] : "",
            'category_ellenorzes' => isset($admin['category_ellenorzes']) ? $admin['category_ellenorzes'] : 0
        );
        $this->data['category'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'filter'   => isset($admin['filter']) ? $admin['filter'] : 0,
            'filter_sorrend' => isset($admin['filter_sorrend']) ? $admin['filter_sorrend'] : 0,
            'filter_neve' => isset($admin['filter_neve']) ? $admin['filter_neve'] : "",
            'filter_ellenorzes' => isset($admin['filter_ellenorzes']) ? $admin['filter_ellenorzes'] : 0
        );
        $this->data['filter'] = $megjelenit_admin_product;


        $megjelenit_admin_product = array(
            'model'   => isset($admin['model']) ? $admin['model'] : 0,
            'model_sorrend' => isset($admin['model_sorrend']) ? $admin['model_sorrend'] : 0,
            'model_neve' => isset($admin['model_neve']) ? $admin['model_neve'] : "",
            'model_ellenorzes' => isset($admin['model_ellenorzes']) ? $admin['model_ellenorzes'] : 0
        );
        $this->data['model'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'sku'   => isset($admin['sku']) ? $admin['sku'] : 0,
            'sku_sorrend' => isset($admin['sku_sorrend']) ? $admin['sku_sorrend'] : 0,
            'sku_neve' => isset($admin['sku_neve']) ? $admin['sku_neve'] : "",
            'sku_ellenorzes' => isset($admin['sku_ellenorzes']) ? $admin['sku_ellenorzes'] : 0
        );
        $this->data['sku'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'meret'   => isset($admin['meret']) ? $admin['meret'] : 0,
            'meret_sorrend' => isset($admin['meret_sorrend']) ? $admin['meret_sorrend'] : 0,
            'meret_neve' => isset($admin['meret_neve']) ? $admin['meret_neve'] : "",
            'meret_ellenorzes' => isset($admin['meret_ellenorzes']) ? $admin['meret_ellenorzes'] : 0
        );
        $this->data['meret'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'upc'   => isset($admin['upc']) ? $admin['upc'] : 0,
            'upc_sorrend' => isset($admin['upc_sorrend']) ? $admin['upc_sorrend'] : 0,
            'upc_neve' => isset($admin['upc_neve']) ? $admin['upc_neve'] : "",
            'upc_ellenorzes' => isset($admin['upc_ellenorzes']) ? $admin['upc_ellenorzes'] : 0
        );
        $this->data['upc'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'location'   => isset($admin['location']) ? $admin['location'] : 0,
            'location_sorrend' => isset($admin['location_sorrend']) ? $admin['location_sorrend'] : 0,
            'location_neve' => isset($admin['location_neve']) ? $admin['location_neve'] : "",
            'location_ellenorzes' => isset($admin['location_ellenorzes']) ? $admin['location_ellenorzes'] : 0
        );
        $this->data['location'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'quantity'   => isset($admin['quantity']) ? $admin['quantity'] : 0,
            'quantity_sorrend' => isset($admin['quantity_sorrend']) ? $admin['quantity_sorrend'] : 0,
            'quantity_neve' => isset($admin['quantity_neve']) ? $admin['quantity_neve'] : "",
            'quantity_ellenorzes' => isset($admin['quantity_ellenorzes']) ? $admin['quantity_ellenorzes'] : 0
        );
        $this->data['quantity'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'stock_status_id'   => isset($admin['stock_status_id']) ? $admin['stock_status_id'] : 0,
            'stock_status_id_sorrend' => isset($admin['stock_status_id_sorrend']) ? $admin['stock_status_id_sorrend'] : 0,
            'stock_status_id_neve' => isset($admin['stock_status_id_neve']) ? $admin['stock_status_id_neve'] : "",
            'stock_status_id_ellenorzes' => isset($admin['stock_status_id_ellenorzes']) ? $admin['stock_status_id_ellenorzes'] : 0
        );
        $this->data['stock_status_id'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'image'   => isset($admin['image']) ? $admin['image'] : 0,
            'image_sorrend' => isset($admin['image_sorrend']) ? $admin['image_sorrend'] : 0,
            'image_neve' => isset($admin['image_neve']) ? $admin['image_neve'] : "",
            'image_ellenorzes' => isset($admin['image_ellenorzes']) ? $admin['image_ellenorzes'] : 0
        );
        $this->data['image'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'images'   => isset($admin['images']) ? $admin['images'] : 0,
            'images_sorrend' => isset($admin['images_sorrend']) ? $admin['images_sorrend'] : 0,
            'images_neve' => isset($admin['images_neve']) ? $admin['images_neve'] : "",
            'images_ellenorzes' => isset($admin['images_ellenorzes']) ? $admin['images_ellenorzes'] : 0
        );
        $this->data['images'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'elolnezet'   => isset($admin['elolnezet']) ? $admin['elolnezet'] : 0,
            'elolnezet_sorrend' => isset($admin['elolnezet_sorrend']) ? $admin['elolnezet_sorrend'] : 0,
            'elolnezet_neve' => isset($admin['elolnezet_neve']) ? $admin['elolnezet_neve'] : "",
            'elolnezet_ellenorzes' => isset($admin['elolnezet_ellenorzes']) ? $admin['elolnezet_ellenorzes'] : 0
        );
        $this->data['elolnezet'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'manufacturer_id'   => isset($admin['manufacturer_id']) ? $admin['manufacturer_id'] : 0,
            'manufacturer_id_sorrend' => isset($admin['manufacturer_id_sorrend']) ? $admin['manufacturer_id_sorrend'] : 0,
            'manufacturer_id_neve' => isset($admin['manufacturer_id_neve']) ? $admin['manufacturer_id_neve'] : "",
            'manufacturer_id_ellenorzes' => isset($admin['manufacturer_id_ellenorzes']) ? $admin['manufacturer_id_ellenorzes'] : 0
        );
        $this->data['manufacturer_id'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'shipping'   => isset($admin['shipping']) ? $admin['shipping'] : 0,
            'shipping_sorrend' => isset($admin['shipping_sorrend']) ? $admin['shipping_sorrend'] : 0,
            'shipping_neve' => isset($admin['shipping_neve']) ? $admin['shipping_neve'] : "",
            'shipping_ellenorzes' => isset($admin['shipping_ellenorzes']) ? $admin['shipping_ellenorzes'] : 0
        );
        $this->data['shipping'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'price'   => isset($admin['price']) ? $admin['price'] : 0,
            'price_sorrend' => isset($admin['price_sorrend']) ? $admin['price_sorrend'] : 0,
            'price_neve' => isset($admin['price_neve']) ? $admin['price_neve'] : "",
            'price_ellenorzes' => isset($admin['price_ellenorzes']) ? $admin['price_ellenorzes'] : 0
        );
        $this->data['price'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'szazalek'   => isset($admin['price']) ? $admin['szazalek'] : 0,
            'szazalek_sorrend' => isset($admin['szazalek_sorrend']) ? $admin['szazalek_sorrend'] : 0,
            'szazalek_neve' => isset($admin['szazalek_neve']) ? $admin['szazalek_neve'] : "",
            'szazalek_ellenorzes' => isset($admin['szazalek_ellenorzes']) ? $admin['szazalek_ellenorzes'] : 0
        );
        $this->data['szazalek'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'points'   => isset($admin['price']) ? $admin['price'] : 0,
            'points_sorrend' => isset($admin['points_sorrend']) ? $admin['points_sorrend'] : 0,
            'points_neve' => isset($admin['points_neve']) ? $admin['points_neve'] : "",
            'points_ellenorzes' => isset($admin['points_ellenorzes']) ? $admin['points_ellenorzes'] : 0
        );
        $this->data['points'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'tax_class_id'   => isset($admin['tax_class_id']) ? $admin['tax_class_id'] : 0,
            'tax_class_id_sorrend' => isset($admin['tax_class_id_sorrend']) ? $admin['tax_class_id_sorrend'] : 0,
            'tax_class_id_neve' => isset($admin['tax_class_id_neve']) ? $admin['tax_class_id_neve'] : "",
            'tax_class_id_ellenorzes' => isset($admin['tax_class_id_ellenorzes']) ? $admin['tax_class_id_ellenorzes'] : 0
        );
        $this->data['tax_class_id'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'date_available'   => isset($admin['date_available']) ? $admin['date_available'] : 0,
            'date_available_sorrend' => isset($admin['date_available_sorrend']) ? $admin['date_available_sorrend'] : 0,
            'date_available_neve' => isset($admin['date_available_neve']) ? $admin['date_available_neve'] : "",
            'date_available_ellenorzes' => isset($admin['date_available_ellenorzes']) ? $admin['date_available_ellenorzes'] : 0
        );
        $this->data['date_available'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'weight'   => isset($admin['weight']) ? $admin['weight'] : 0,
            'weight_sorrend' => isset($admin['weight_sorrend']) ? $admin['weight_sorrend'] : 0,
            'weight_neve' => isset($admin['weight_neve']) ? $admin['weight_neve'] : "",
            'weight_ellenorzes' => isset($admin['weight_ellenorzes']) ? $admin['weight_ellenorzes'] : 0
        );
        $this->data['weight'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'weight_class_id'   => isset($admin['weight_class_id']) ? $admin['weight_class_id'] : 0,
            'weight_class_id_sorrend' => isset($admin['weight_class_id_sorrend']) ? $admin['weight_class_id_sorrend'] : 0,
            'weight_class_id_neve' => isset($admin['weight_class_id_neve']) ? $admin['weight_class_id_neve'] : "",
            'weight_class_id_ellenorzes' => isset($admin['weight_class_id_ellenorzes']) ? $admin['weight_class_id_ellenorzes'] : 0
        );
        $this->data['weight_class_id'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'length'   => isset($admin['length']) ? $admin['length'] : 0,
            'length_sorrend' => isset($admin['length_sorrend']) ? $admin['length_sorrend'] : 0,
            'length_neve' => isset($admin['length_neve']) ? $admin['length_neve'] : "",
            'length_ellenorzes' => isset($admin['length_ellenorzes']) ? $admin['length_ellenorzes'] : 0
        );
        $this->data['length'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'width'   => isset($admin['width']) ? $admin['width'] : 0,
            'width_sorrend' => isset($admin['width_sorrend']) ? $admin['width_sorrend'] : 0,
            'width_neve' => isset($admin['width_neve']) ? $admin['width_neve'] : "",
            'width_ellenorzes' => isset($admin['width_ellenorzes']) ? $admin['width_ellenorzes'] : 0
        );
        $this->data['width'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'height'   => isset($admin['height']) ? $admin['height'] : 0,
            'height_sorrend' => isset($admin['height_sorrend']) ? $admin['height_sorrend'] : 0,
            'height_neve' => isset($admin['height_neve']) ? $admin['height_neve'] : "",
            'height_ellenorzes' => isset($admin['height_ellenorzes']) ? $admin['height_ellenorzes'] : 0
        );
        $this->data['height'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'length_class_id'   => isset($admin['length_class_id']) ? $admin['length_class_id'] : 0,
            'length_class_id_sorrend' => isset($admin['length_class_id_sorrend']) ? $admin['length_class_id_sorrend'] : 0,
            'length_class_id_neve' => isset($admin['length_class_id_neve']) ? $admin['length_class_id_neve'] : "",
            'length_class_id_ellenorzes' => isset($admin['length_class_id_ellenorzes']) ? $admin['length_class_id_ellenorzes'] : 0
        );
        $this->data['length_class_id'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'subtract'   => isset($admin['subtract']) ? $admin['subtract'] : 0,
            'subtract_sorrend' => isset($admin['subtract_sorrend']) ? $admin['subtract_sorrend'] : 0,
            'subtract_neve' => isset($admin['subtract_neve']) ? $admin['subtract_neve'] : "",
            'subtract_ellenorzes' => isset($admin['subtract_ellenorzes']) ? $admin['subtract_ellenorzes'] : 0
        );
        $this->data['subtract'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'minimum'   => isset($admin['minimum']) ? $admin['minimum'] : 0,
            'minimum_sorrend' => isset($admin['minimum_sorrend']) ? $admin['minimum_sorrend'] : 0,
            'minimum_neve' => isset($admin['minimum_neve']) ? $admin['minimum_neve'] : "",
            'minimum_ellenorzes' => isset($admin['minimum_ellenorzes']) ? $admin['minimum_ellenorzes'] : 0
        );
        $this->data['minimum'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'sort_order'   => isset($admin['sort_order']) ? $admin['sort_order'] : 0,
            'sort_order_sorrend' => isset($admin['sort_order_sorrend']) ? $admin['sort_order_sorrend'] : 0,
            'sort_order_neve' => isset($admin['sort_order_neve']) ? $admin['sort_order_neve'] : "",
            'sort_order_ellenorzes' => isset($admin['sort_order_ellenorzes']) ? $admin['sort_order_ellenorzes'] : 0
        );
        $this->data['sort_order'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'status'   => isset($admin['status']) ? $admin['status'] : 0,
            'status_sorrend' => isset($admin['status_sorrend']) ? $admin['status_sorrend'] : 0,
            'status_neve' => isset($admin['status_neve']) ? $admin['status_neve'] : "",
            'status_ellenorzes' => isset($admin['status_ellenorzes']) ? $admin['status_ellenorzes'] : 0
        );
        $this->data['status'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'date_added'   => isset($admin['date_added']) ? $admin['date_added'] : 0,
            'date_added_sorrend' => isset($admin['date_added_sorrend']) ? $admin['date_added_sorrend'] : 0,
            'date_added_neve' => isset($admin['date_added_neve']) ? $admin['date_added_neve'] : "",
            'date_added_ellenorzes' => isset($admin['date_added_ellenorzes']) ? $admin['date_added_ellenorzes'] : 0
        );
        $this->data['date_added'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'date_modified'   => isset($admin['date_modified']) ? $admin['date_modified'] : 0,
            'date_modified_sorrend' => isset($admin['date_modified_sorrend']) ? $admin['date_modified_sorrend'] : 0,
            'date_modified_neve' => isset($admin['date_modified_neve']) ? $admin['date_modified_neve'] : "",
            'date_modified_ellenorzes' => isset($admin['date_modified_ellenorzes']) ? $admin['date_modified_ellenorzes'] : 0
        );
        $this->data['date_modified'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'viewed'   => isset($admin['viewed']) ? $admin['viewed'] : 0,
            'viewed_sorrend' => isset($admin['viewed_sorrend']) ? $admin['viewed_sorrend'] : 0,
            'viewed_neve' => isset($admin['viewed_neve']) ? $admin['viewed_neve'] : "",
            'viewed_ellenorzes' => isset($admin['viewed_ellenorzes']) ? $admin['viewed_ellenorzes'] : 0
        );
        $this->data['viewed'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'cikkszam'   => isset($admin['cikkszam']) ? $admin['cikkszam'] : 0,
            'cikkszam_sorrend' => isset($admin['cikkszam_sorrend']) ? $admin['cikkszam_sorrend'] : 0,
            'cikkszam_neve' => isset($admin['cikkszam_neve']) ? $admin['cikkszam_neve'] : "",
            'cikkszam_ellenorzes' => isset($admin['cikkszam_ellenorzes']) ? $admin['cikkszam_ellenorzes'] : 0
        );
        $this->data['cikkszam'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'megyseg'   => isset($admin['megyseg']) ? $admin['megyseg'] : 0,
            'megyseg_sorrend' => isset($admin['megyseg_sorrend']) ? $admin['megyseg_sorrend'] : 0,
            'megyseg_neve' => isset($admin['megyseg_neve']) ? $admin['megyseg_neve'] : "",
            'megyseg_ellenorzes' => isset($admin['megyseg_ellenorzes']) ? $admin['megyseg_ellenorzes'] : 0
        );
        $this->data['megyseg'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'csomagolasi_mennyiseg'   => isset($admin['csomagolasi_mennyiseg']) ? $admin['csomagolasi_mennyiseg'] : 0,
            'csomagolasi_mennyiseg_sorrend' => isset($admin['csomagolasi_mennyiseg_sorrend']) ? $admin['csomagolasi_mennyiseg_sorrend'] : 0,
            'csomagolasi_mennyiseg_neve' => isset($admin['csomagolasi_mennyiseg_neve']) ? $admin['csomagolasi_mennyiseg_neve'] : "",
            'csomagolasi_mennyiseg_ellenorzes' => isset($admin['csomagolasi_mennyiseg_ellenorzes']) ? $admin['csomagolasi_mennyiseg_ellenorzes'] : 0
        );
        $this->data['csomagolasi_mennyiseg'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'csomagolasi_egyseg'   => isset($admin['csomagolasi_egyseg']) ? $admin['csomagolasi_egyseg'] : 0,
            'csomagolasi_egyseg_sorrend' => isset($admin['csomagolasi_egyseg_sorrend']) ? $admin['csomagolasi_egyseg_sorrend'] : 0,
            'csomagolasi_egyseg_neve' => isset($admin['csomagolasi_egyseg_neve']) ? $admin['csomagolasi_egyseg_neve'] : "",
            'csomagolasi_egyseg_ellenorzes' => isset($admin['csomagolasi_egyseg_ellenorzes']) ? $admin['csomagolasi_egyseg_ellenorzes'] : 0
        );
        $this->data['csomagolasi_egyseg'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'date_ervenyes_ig'   => isset($admin['date_ervenyes_ig']) ? $admin['date_ervenyes_ig'] : 0,
            'date_ervenyes_ig_sorrend' => isset($admin['date_ervenyes_ig_sorrend']) ? $admin['date_ervenyes_ig_sorrend'] : 0,
            'date_ervenyes_ig_neve' => isset($admin['date_ervenyes_ig_neve']) ? $admin['date_ervenyes_ig_neve'] : "",
            'date_ervenyes_ig_ellenorzes' => isset($admin['date_ervenyes_ig_ellenorzes']) ? $admin['date_ervenyes_ig_ellenorzes'] : 0
        );
        $this->data['date_ervenyes_ig'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'darabaru_meter'   => isset($admin['darabaru_meter']) ? $admin['darabaru_meter'] : 0,
            'darabaru_meter_sorrend' => isset($admin['darabaru_meter_sorrend']) ? $admin['darabaru_meter_sorrend'] : 0,
            'darabaru_meter_neve' => isset($admin['darabaru_meter_neve']) ? $admin['darabaru_meter_neve'] : "",
            'darabaru_meter_ellenorzes' => isset($admin['darabaru_meter_ellenorzes']) ? $admin['darabaru_meter_ellenorzes'] : 0
        );
        $this->data['darabaru_meter'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'cikkszam2'   => isset($admin['cikkszam2']) ? $admin['cikkszam2'] : 0,
            'cikkszam2_sorrend' => isset($admin['cikkszam2_sorrend']) ? $admin['cikkszam2_sorrend'] : 0,
            'cikkszam2_neve' => isset($admin['cikkszam2_neve']) ? $admin['cikkszam2_neve'] : "",
            'cikkszam2_ellenorzes' => isset($admin['cikkszam2_ellenorzes']) ? $admin['cikkszam2_ellenorzes'] : 0
        );
        $this->data['cikkszam2'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'eredeti_ar'   => isset($admin['eredeti_ar']) ? $admin['eredeti_ar'] : 0,
            'eredeti_ar_sorrend' => isset($admin['eredeti_ar_sorrend']) ? $admin['eredeti_ar_sorrend'] : 0,
            'eredeti_ar_neve' => isset($admin['eredeti_ar_neve']) ? $admin['eredeti_ar_neve'] : "",
            'eredeti_ar_ellenorzes' => isset($admin['eredeti_ar_ellenorzes']) ? $admin['eredeti_ar_ellenorzes'] : 0
        );
        $this->data['eredeti_ar'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'letoltheto'   => isset($admin['letoltheto']) ? $admin['letoltheto'] : 0,
            'letoltheto_sorrend' => isset($admin['letoltheto_sorrend']) ? $admin['letoltheto_sorrend'] : 0,
            'letoltheto_neve' => isset($admin['letoltheto_neve']) ? $admin['letoltheto_neve'] : "",
            'letoltheto_ellenorzes' => isset($admin['letoltheto_ellenorzes']) ? $admin['letoltheto_ellenorzes'] : 0
        );
        $this->data['letoltheto'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'elhelyezkedes'   => isset($admin['elhelyezkedes']) ? $admin['elhelyezkedes'] : 0,
            'elhelyezkedes_sorrend' => isset($admin['elhelyezkedes_sorrend']) ? $admin['elhelyezkedes_sorrend'] : 0,
            'elhelyezkedes_neve' => isset($admin['elhelyezkedes_neve']) ? $admin['elhelyezkedes_neve'] : ""
        );
        $this->data['elhelyezkedes_kiemeles'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'biztonsagi_kod'   => isset($admin['biztonsagi_kod']) ? $admin['biztonsagi_kod'] : 0,
            'biztonsagi_kod_sorrend' => isset($admin['biztonsagi_kod_sorrend']) ? $admin['biztonsagi_kod_sorrend'] : 0,
            'biztonsagi_kod_neve' => isset($admin['biztonsagi_kod_neve']) ? $admin['biztonsagi_kod_neve'] : "",
            'biztonsagi_kod_ellenorzes' => isset($admin['biztonsagi_kod_ellenorzes']) ? $admin['biztonsagi_kod_ellenorzes'] : 0
        );
        $this->data['biztonsagi_kod'] = $megjelenit_admin_product;

        $megjelenit_admin_product = array(
            'lista_model'   => isset($admin['lista_model']) ? $admin['lista_model'] : 0,
            'lista_model_sorrend' => isset($admin['lista_model_sorrend']) ? $admin['lista_model_sorrend'] : 0
        );
        $this->data['lista_model'] = $megjelenit_admin_product;





        $this->template = 'design/megjelenito_frontend.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }




}
?>