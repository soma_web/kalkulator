<?php
class ControllerDesignMegjelenitoracs extends Controller {
	private $error = array(); 
     
  	public function index() {
        if ($this->request->server['REQUEST_METHOD'] == 'POST') {

            if (isset($this->request->post['racs_beallitas_racs_csoportok']) ) {
                $this->request->post['racs_beallitas_racs_csoportok'] = $this->racsRendez($this->request->post['racs_beallitas_racs_csoportok']);
            }

            foreach($this->request->post as $key=>$value) {
                $beallit[$key] = $value;
            }
            $this->document->setTitle($this->language->get('heading_title'));
            $this->load->model('setting/setting');

            $this->model_setting_setting->editSetting('racs_beallitas', $beallit);

            $this->load->model('setting/extension');

            $this->session->data['success'] = $this->language->get('text_success');

            if (!empty($this->request->get['elonezet'])) {
                exit;
            } else {
                if (!empty($_GET['sablonvalaszto'])) {
                    $this->redirect($this->url->link('design/megjelenito_racs&sablonvalaszto=1', 'token=' . $this->session->data['token'], 'SSL'));

                } else {
                    $this->redirect($this->url->link('design/megjelenito_racs', 'token=' . $this->session->data['token'], 'SSL'));
                }
            }

        }

        $this->load->model('catalog/information');
        $this->data['information_pages'] 	= $this->model_catalog_information->getInformations();

        $this->load->model('pavblog/menu');
        $this->data['blog_pages'] 	= $this->model_pavblog_menu->getChild();

        $this->load->language('design/megjelenito');
        $this->load->language('catalog/product');

        $this->data['entry_penznem_kiiras'] = $this->language->get('entry_penznem_kiiras');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');
        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['button_remove']        = $this->language->get('button_remove');
        $this->data['text_select_all'] = $this->language->get('text_select_all');
        $this->data['text_unselect_all'] = $this->language->get('text_unselect_all');
        $this->data['entry_status']           = $this->language->get('entry_status');



        if (isset($this->session->data['success']))
            $this->data['success'] = $this->language->get('text_success');
        else
            $this->data['success'] = false;

    	$this->data['heading_title'] = 'Rács nézet';
 


  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
            'text'      => "Szuperadmin - Rács nézet",
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => "Szuperadmin - Rács nézet beállítás",
            'href'      => $this->url->link('design/megjelenito_racs', 'token=' . $this->session->data['token'], 'SSL'),

            'separator' => ' :: '
   		);


        $this->data['action'] = $this->url->link('design/megjelenito_racs', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];


        /* racs tpl szerkesztő */
        $racs_beallitas_racs_csoportok = $this->config->get('racs_beallitas_racs_csoportok');
        if (is_null($racs_beallitas_racs_csoportok)) {
            $racs_beallitas_racs_csoportok = array();
        }


        foreach($racs_beallitas_racs_csoportok as $key=>$value){
            if (!is_array($value)) {
                $racs_beallitas_racs_csoportok[$key] = array(
                    'default'   => array(
                    'stauts' =>'0'
                    )
                );
            }
            if (substr($key,0,8) == "#csoport") {
                //unset ($racs_beallitas_racs_csoportok[$key]);
            }
        }
        $this->data['racs_beallitas_racs_csoportok'] = $racs_beallitas_racs_csoportok;

        $racs_beallitas_racs_csoportok_kiegeszito = array_keys($racs_beallitas_racs_csoportok);

        $this->data['racs_beallitas_racs_csoportok_kiegeszito'] = $racs_beallitas_racs_csoportok_kiegeszito;


        /*if (is_dir(DIR_CATALOG . "view/theme/" . $this->config->get('config_template') . '/template/common/racs')) {
            $files = glob(DIR_CATALOG . 'view/theme/'.$this->config->get('config_template').'/template/common/racs/*.tpl');
            $files_beallitasok = glob(DIR_CATALOG . 'view/theme/'.$this->config->get('config_template').'/template/common/racs/*.php');
        } else {*/
            $files = glob(DIR_CATALOG . 'view/theme/default/template/module/termek_doboz/*.tpl');
            $files_beallitasok = glob(DIR_CATALOG . 'view/theme/default/template/module/termek_doboz/*.php');
        //}


        $datas=array();
        foreach ($files as $file) {
            $beallitas_adatok=array();
            $extension = basename($file, '.tpl');
            $beallitas = str_replace(".tpl",".php",$file);
            $datas[$extension] = array();


            if (in_array($beallitas,$files_beallitasok)) {
                $_=array();
                require($beallitas);

                $beallitas_adatok = array_merge($beallitas_adatok, $_);
                foreach($beallitas_adatok as $keydata=>$beallitas_adat) {
                    $datas[$extension][$keydata] = $beallitas_adat;
                }
            }
        }
        $this->data['racs_beallitas_racs_csoportok_files'] = $datas;

        foreach($this->data['racs_beallitas_racs_csoportok_files'] as $template=>$value) {
            if ($this->config->get("racs_beallitas_alap") && !array_key_exists($template,$this->config->get("racs_beallitas_alap")) ) {
                $talalt = false;
                foreach($this->data['racs_beallitas_racs_csoportok'] as $ertek) {
                    if (isset($ertek['template'][$template]) ) {
                        $talalt = true;
                        break;
                    }
                }
                if (!$talalt) {
                    unset ($this->data['racs_beallitas_racs_csoportok_files'][$template]);
                }
            }
        }

        if ($this->config->get("racs_beallitas_alap")) {
            foreach($datas as $template=>$value) {
                if (array_key_exists($template,$this->config->get("racs_beallitas_alap")) ) {
                    $datas[$template]['status'] = 1;
                }
            }
        } else {
            foreach($datas as $template=>$value) {
                $talalt = false;
                foreach($this->data['racs_beallitas_racs_csoportok'] as $ertek) {
                    if (isset($ertek['templates'][$template]) ) {
                        $talalt = true;
                        break;
                    }
                }
                if ($talalt) {
                    $datas[$template]['status'] = 1;
                }

            }
        }
        $this->data['racs_beallitas_alap'] = $datas;
        $this->data['sablonvalaszto'] = !empty($_GET['sablonvalaszto']) ? 1 : '';




        if (isset($this->request->post['racs_beallitas_racs'])) {
            $admin = $this->request->post['racs_beallitas_racs'];
        } else {
            $admin = $this->config->get("racs_beallitas_racs");
        }

        $racs_beallitas_racs = array(
            'modularis'   => isset($admin['modularis']) ? $admin['modularis'] : 0
        );
        $this->data['racs_beallitas_racs'] = $racs_beallitas_racs;


        /* racs tpl szerkesztő vége */





        $this->template = 'design/megjelenito_racs.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
  	}


    private function racsRendez($tomb) {

        foreach($tomb as $key=>$value) {
            if (isset($value['templates'])) {
                $tomb[$key]['templates'] = $this->config->rendezes($value['templates'],"sort_order","ASC");
            }
        }

        /*$gyujto = array();
        foreach($tomb as $key=>$value) {
            $gyujto[] = array(
                'kulcs' => ''.$key,
                'ertek' => $value
            );
        }
        //$gyujto = $this->config->rendezes($gyujto,"kulcs");
        $vissza = array();
        foreach($gyujto as $value) {
            if (isset($value['ertek']['templates'])) {
                $value['ertek']['templates'] = $this->config->rendezes($value['ertek']['templates'],"sort_order");
            }
            $vissza[$value['kulcs']] = $value['ertek'];


        }*/
        return $tomb;

    }
}

?>