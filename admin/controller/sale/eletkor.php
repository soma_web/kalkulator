<?php
class ControllerSaleEletkor extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('sale/eletkor');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/eletkor');

        $this->data['heading_title']    = $this->language->get('heading_title');
        $this->data['button_insert']    = $this->language->get('button_insert');
        $this->data['button_delete']    = $this->language->get('button_delete');
        $this->data['column_eletkor']   = $this->language->get('column_eletkor');
        $this->data['column_sort_order'] = $this->language->get('column_sort_order');
        $this->data['column_action']    = $this->language->get('column_action');
        $this->data['text_no_results']    = $this->language->get('text_no_results');
        $this->data['error_warning']    = '';
        $this->data['success'] = '';


        $this->getList();
    }

    public function insert() {
        $this->load->language('sale/eletkor');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/eletkor');

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $eletkor = $this->config->get('eletkor');
            $last_id = 0;

            if ($eletkor) {
                if (is_array($eletkor) ) {
                    foreach ($eletkor as $value) {
                        if ($value['eletkor_id'] > $last_id) {
                            $last_id = $value['eletkor_id'];
                        }
                    }
                }
            }
            $last_id++;
            if ( !empty($_REQUEST['megnevezes']) ) {
                $kiir["eletkor"] = $this->config->get("eletkor");
                $kiir["eletkor"][] = array(
                    'megnevezes' => $_REQUEST['megnevezes'],
                    'sort_order' => $_REQUEST['sort_order'],
                    'eletkor_id' => $last_id
                );
            }

            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('eletkor', $kiir);
            $this->session->data['success'] = $this->language->get('text_success');
            $this->redirect($this->url->link('sale/eletkor', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getForm();
    }

    public function update() {
        $this->load->language('sale/eletkor');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/eletkor');

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $kiir["eletkor"] = $this->config->get("eletkor");
            foreach ($kiir["eletkor"] as $value) {
                if ($value['eletkor_id'] ==  $_REQUEST['eletkor_id']) {
                    $modositott["eletkor"][] = array(
                        'megnevezes' => $_REQUEST['megnevezes'],
                        'sort_order' => $_REQUEST['sort_order'],
                        'eletkor_id' => $_REQUEST['eletkor_id']
                    );
                } else {
                    $modositott["eletkor"][] = array(
                        'megnevezes' => $value['megnevezes'],
                        'sort_order' => $value['sort_order'],
                        'eletkor_id' => $value['eletkor_id']
                    );
                }
            }

            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('eletkor', $modositott);
            $this->session->data['success'] = $this->language->get('text_success');


            $this->redirect($this->url->link('sale/eletkor', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('sale/eletkor');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/eletkor');

        if (isset($this->request->post['selected']) ) {

            $kiir["eletkor"] = $this->config->get("eletkor");

            foreach ($kiir["eletkor"] as $value) {

                if ( !in_array($value['eletkor_id'],$_REQUEST['selected']) ) {
                    $modositott["eletkor"][] = array(
                        'megnevezes' => $value['megnevezes'],
                        'sort_order' => $value['sort_order'],
                        'eletkor_id' => $value['eletkor_id']
                    );
                }
            }

            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('eletkor', $modositott);
            $this->session->data['success'] = $this->language->get('text_success');


            $this->redirect($this->url->link('sale/eletkor', 'token=' . $this->session->data['token'], 'SSL'));

        }

        $this->getList();
    }

    private function getList() {

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('sale/eletkor', 'token=' . $this->session->data['token'] , 'SSL'),
            'separator' => ' :: '
        );

        $this->data['insert'] = $this->url->link('sale/eletkor/insert', 'token=' . $this->session->data['token'] , 'SSL');
        $this->data['delete'] = $this->url->link('sale/eletkor/delete', 'token=' . $this->session->data['token'] , 'SSL');



        $this->data['eletkors'] = array();
        $eletkor = $this->config->get("eletkor");

        if ($eletkor) {
            foreach ($eletkor as $value) {
                $action = array(
                    'text' => $this->language->get('text_edit'),
                    'href' => $this->url->link('sale/eletkor/update', 'token=' . $this->session->data['token'].'&eletkor_id='.$value['eletkor_id'].'&megnevezes='.$value['megnevezes'].'&sort_order='.$value['sort_order'], 'SSL')
                );

                $this->data['eletkors'][] = array(
                    'megnevezes' => $value['megnevezes'],
                    'sort_order' => $value['sort_order'],
                    'eletkor_id' => $value['eletkor_id'],
                    'action'     => $action,
                    'selected'       => isset($this->request->post['selected']) && in_array($value['eletkor_id'], $this->request->post['selected']),

                );
            }
        }


        $sortingSettings = array(
            0       =>  array(
                'orderby' =>  'sort_order',
                'sortorder'     => 'ASC'
            )
        );

        $rendezo = new ArrayOfArrays($this->data['eletkors']);
        $rendezo->multiSorting($sortingSettings,true);
        $this->data['eletkors'] = $rendezo->getArrayCopy();


        $this->template = 'sale/eletkor_list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    private function getForm() {
        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');
        $this->data['text_percent'] = $this->language->get('text_percent');
        $this->data['text_amount'] = $this->language->get('text_amount');

        $this->data['entry_name'] = $this->language->get('entry_name');
        $this->data['entry_description'] = $this->language->get('entry_description');
        $this->data['entry_code'] = $this->language->get('entry_code');
        $this->data['entry_discount'] = $this->language->get('entry_discount');
        $this->data['entry_logged'] = $this->language->get('entry_logged');
        $this->data['entry_shipping'] = $this->language->get('entry_shipping');
        $this->data['entry_type'] = $this->language->get('entry_type');
        $this->data['entry_total'] = $this->language->get('entry_total');
        $this->data['entry_category'] = $this->language->get('entry_category');
        $this->data['entry_product'] = $this->language->get('entry_product');
        $this->data['entry_date_start'] = $this->language->get('entry_date_start');
        $this->data['entry_date_end'] = $this->language->get('entry_date_end');
        $this->data['entry_uses_total'] = $this->language->get('entry_uses_total');
        $this->data['entry_uses_customer'] = $this->language->get('entry_uses_customer');
        $this->data['entry_status'] = $this->language->get('entry_status');

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');

        $this->data['tab_general'] = $this->language->get('tab_general');
        $this->data['tab_eletkor_history'] = $this->language->get('tab_eletkor_history');

        $this->data['token'] = $this->session->data['token'];

        if (isset($this->request->get['eletkor_id'])) {
            $this->data['eletkor_id'] = $this->request->get['eletkor_id'];
        } else {
            $this->data['eletkor_id'] = 0;
        }

        if (isset($this->request->get['megnevezes'])) {
            $this->data['megnevezes'] = $this->request->get['megnevezes'];
        } else {
            $this->data['megnevezes'] = '';
        }
        if (isset($this->request->get['sort_order'])) {
            $this->data['sort_order'] = $this->request->get['sort_order'];
        } else {
            $this->data['sort_order'] = 0;
        }

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }



        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('sale/eletkor', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        if (!isset($this->request->get['eletkor_id'])) {
            $this->data['action'] = $this->url->link('sale/eletkor/insert', 'token=' . $this->session->data['token'], 'SSL');
        } else {
            $this->data['action'] = $this->url->link('sale/eletkor/update', 'token=' . $this->session->data['token'] . '&eletkor_id=' . $this->request->get['eletkor_id'], 'SSL');
        }

        $this->data['cancel'] = $this->url->link('sale/eletkor', 'token=' . $this->session->data['token'], 'SSL');


        $this->template = 'sale/eletkor_form.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    private function validateForm() {
        if (!$this->user->hasPermission('modify', 'sale/eletkor')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 128)) {
            $this->error['name'] = $this->language->get('error_name');
        }

        if ((utf8_strlen($this->request->post['code']) < 3) || (utf8_strlen($this->request->post['code']) > 10)) {
            $this->error['code'] = $this->language->get('error_code');
        }

        $eletkor_info = $this->model_sale_eletkor->geteletkorByCode($this->request->post['code']);

        if ($eletkor_info) {
            if (!isset($this->request->get['eletkor_id'])) {
                $this->error['warning'] = $this->language->get('error_exists');
            } elseif ($eletkor_info['eletkor_id'] != $this->request->get['eletkor_id'])  {
                $this->error['warning'] = $this->language->get('error_exists');
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    private function validateDelete() {
        if (!$this->user->hasPermission('modify', 'sale/eletkor')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    public function history() {
        $this->language->load('sale/eletkor');

        $this->load->model('sale/eletkor');

        $this->data['text_no_results'] = $this->language->get('text_no_results');

        $this->data['column_order_id'] = $this->language->get('column_order_id');
        $this->data['column_customer'] = $this->language->get('column_customer');
        $this->data['column_amount'] = $this->language->get('column_amount');
        $this->data['column_date_added'] = $this->language->get('column_date_added');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $this->data['histories'] = array();

        $results = $this->model_sale_eletkor->geteletkorHistories($this->request->get['eletkor_id'], ($page - 1) * 10, 10);

        foreach ($results as $result) {
            $this->data['histories'][] = array(
                'order_id'   => $result['order_id'],
                'customer'   => $result['customer'],
                'amount'     => $result['amount'],
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
            );
        }

        $history_total = $this->model_sale_eletkor->getTotaleletkorHistories($this->request->get['eletkor_id']);

        $pagination = new Pagination();
        $pagination->total = $history_total;
        $pagination->page = $page;
        $pagination->limit = 10;
        $pagination->url = $this->url->link('sale/eletkor/history', 'token=' . $this->session->data['token'] . '&eletkor_id=' . $this->request->get['eletkor_id'] . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();

        $this->template = 'sale/eletkor_history.tpl';

        $this->response->setOutput($this->render());
    }
}
?>