<?php
class ControllerModuleAbandonedCartReminder extends Controller {
	private $error = array(); 
	
	public function install(){		
       /* $this->db->query("ALTER TABLE  `" . DB_PREFIX . "customer` ADD  `date_last_action` DATETIME NOT NULL AFTER  `date_added` , ADD  `number_reminder_sent` INT NOT NULL AFTER  `date_last_action`, ADD  `number_reward_sent` INT NOT NULL AFTER  `number_reminder_sent`");*/
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET date_last_action=NOW()");
	}
	
	public function uninstall(){		
      /*  $this->db->query("ALTER TABLE `" . DB_PREFIX . "customer` DROP `date_last_action`, DROP `number_reminder_sent`, DROP `number_reward_sent`;");	*/
	}
	
	
	public function index() {   
		$this->load->language('module/abandoned_cart_reminder');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		$this->load->model('tool/image');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('abandoned_cart_reminder', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['tab_setting'] = $this->language->get('tab_setting');
		$this->data['tab_preview'] = $this->language->get('tab_preview');
		$this->data['tab_help']    = $this->language->get('tab_help');
		
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_coupon_fixed'] = $this->language->get('text_coupon_fixed');
		$this->data['text_coupon_percent'] = $this->language->get('text_coupon_percent');
		
		$this->data['text_content_top'] = $this->language->get('text_content_top');
		$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$this->data['text_column_left'] = $this->language->get('text_column_left');
		$this->data['text_column_right'] = $this->language->get('text_column_right');
		
		$this->data['text_preview_info'] = $this->language->get('text_preview_info');
		
		$this->data['entry_secret_code']   = $this->language->get('entry_secret_code');
		$this->data['entry_delay']         = $this->language->get('entry_delay');
		$this->data['entry_max_reminders'] = $this->language->get('entry_max_reminders');
		$this->data['entry_use_html_email'] = $this->language->get('entry_use_html_email');
		$this->data['entry_log_admin']     = $this->language->get('entry_log_admin');
		$this->data['entry_add_coupon']    = $this->language->get('entry_add_coupon');
		$this->data['entry_coupon_type']   = $this->language->get('entry_coupon_type');
		$this->data['entry_coupon_amount'] = $this->language->get('entry_coupon_amount');
		$this->data['entry_coupon_expire'] = $this->language->get('entry_coupon_expire');
		$this->data['entry_reward_limit'] = $this->language->get('entry_reward_limit');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_preview'] = $this->language->get('button_preview');
		$this->data['button_send'] = $this->language->get('button_send');
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = array();
		}
				
		if (isset($this->error['secret_code'])) {
			$this->data['error_secret_code'] = $this->error['secret_code'];
		} else {
			$this->data['error_secret_code'] = '';
		}
		
		if (isset($this->error['delay'])) {
			$this->data['error_delay'] = $this->error['delay'];
		} else {
			$this->data['error_delay'] = '';
		}
		
		if (isset($this->error['max_reminders'])) {
			$this->data['error_max_reminders'] = $this->error['max_reminders'];
		} else {
			$this->data['error_max_reminders'] = '';
		}
		
		if (isset($this->error['use_html_email'])) {
			$this->data['error_use_html_email'] = $this->error['use_html_email'];
		} else {
			$this->data['error_use_html_email'] = '';
		}
		
		if (isset($this->error['coupon_amount'])) {
			$this->data['error_coupon_amount'] = $this->error['coupon_amount'];
		} else {
			$this->data['error_coupon_amount'] = '';
		}

		if (isset($this->error['coupon_expire'])) {
			$this->data['error_coupon_expire'] = $this->error['coupon_expire'];
		} else {
			$this->data['error_coupon_expire'] = '';
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/abandoned_cart_reminder', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/abandoned_cart_reminder', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->request->post['abandoned_cart_reminder_secret_code'])) {
			$this->data['abandoned_cart_reminder_secret_code'] = $this->request->post['abandoned_cart_reminder_secret_code'];
		} elseif ($this->config->get('abandoned_cart_reminder_secret_code')) { 
			$this->data['abandoned_cart_reminder_secret_code'] = $this->config->get('abandoned_cart_reminder_secret_code');
		} else {
			$this->data['abandoned_cart_reminder_secret_code'] = '';
		}
		
		if (isset($this->request->post['abandoned_cart_reminder_delay'])) {
			$this->data['abandoned_cart_reminder_delay'] = $this->request->post['abandoned_cart_reminder_delay'];
		} elseif ($this->config->get('abandoned_cart_reminder_delay')) { 
			$this->data['abandoned_cart_reminder_delay'] = $this->config->get('abandoned_cart_reminder_delay');
		} else {
			$this->data['abandoned_cart_reminder_delay'] = 3;
		}
		
		if (isset($this->request->post['abandoned_cart_reminder_max_reminders'])) {
			$this->data['abandoned_cart_reminder_max_reminders'] = $this->request->post['abandoned_cart_reminder_max_reminders'];
		} elseif ($this->config->get('abandoned_cart_reminder_max_reminders')) { 
			$this->data['abandoned_cart_reminder_max_reminders'] = $this->config->get('abandoned_cart_reminder_max_reminders');
		} else {
			$this->data['abandoned_cart_reminder_max_reminders'] = 3;
		}
		
		if (isset($this->request->post['abandoned_cart_reminder_use_html_email'])) {
			$this->data['abandoned_cart_reminder_use_html_email'] = $this->request->post['abandoned_cart_reminder_use_html_email'];
		} elseif ($this->config->get('abandoned_cart_reminder_use_html_email')) { 
			$this->data['abandoned_cart_reminder_use_html_email'] = $this->config->get('abandoned_cart_reminder_use_html_email');
		} else {
			$this->data['abandoned_cart_reminder_use_html_email'] = '';
		}
		
		if (isset($this->request->post['abandoned_cart_reminder_log_admin'])) {
			$this->data['abandoned_cart_reminder_log_admin'] = $this->request->post['abandoned_cart_reminder_log_admin'];
		} elseif ($this->config->get('abandoned_cart_reminder_log_admin')) { 
			$this->data['abandoned_cart_reminder_log_admin'] = $this->config->get('abandoned_cart_reminder_log_admin');
		} else {
			$this->data['abandoned_cart_reminder_log_admin'] = '';
		}
		
		if (isset($this->request->post['abandoned_cart_reminder_add_coupon'])) {
			$this->data['abandoned_cart_reminder_add_coupon'] = $this->request->post['abandoned_cart_reminder_add_coupon'];
		} elseif ($this->config->get('abandoned_cart_reminder_add_coupon')) { 
			$this->data['abandoned_cart_reminder_add_coupon'] = $this->config->get('abandoned_cart_reminder_add_coupon');
		} else {
			$this->data['abandoned_cart_reminder_add_coupon'] = 0;
		}
		
		if (isset($this->request->post['abandoned_cart_reminder_coupon_type'])) {
			$this->data['abandoned_cart_reminder_coupon_type'] = $this->request->post['abandoned_cart_reminder_coupon_type'];
		} elseif ($this->config->get('abandoned_cart_reminder_coupon_type')) { 
			$this->data['abandoned_cart_reminder_coupon_type'] = $this->config->get('abandoned_cart_reminder_coupon_type');
		} else {
			$this->data['abandoned_cart_reminder_coupon_type'] = 0;
		}
		
		if (isset($this->request->post['abandoned_cart_reminder_coupon_amount'])) {
			$this->data['abandoned_cart_reminder_coupon_amount'] = $this->request->post['abandoned_cart_reminder_coupon_amount'];
		} elseif ($this->config->get('abandoned_cart_reminder_coupon_amount')) { 
			$this->data['abandoned_cart_reminder_coupon_amount'] = $this->config->get('abandoned_cart_reminder_coupon_amount');
		} else {
			$this->data['abandoned_cart_reminder_coupon_amount'] = '';
		}
		
		if (isset($this->request->post['abandoned_cart_reminder_coupon_expire'])) {
			$this->data['abandoned_cart_reminder_coupon_expire'] = $this->request->post['abandoned_cart_reminder_coupon_expire'];
		} elseif ($this->config->get('abandoned_cart_reminder_coupon_expire')) { 
			$this->data['abandoned_cart_reminder_coupon_expire'] = $this->config->get('abandoned_cart_reminder_coupon_expire');
		} else {
			$this->data['abandoned_cart_reminder_coupon_expire'] = 2;
		}
		
		if (isset($this->request->post['abandoned_cart_reminder_reward_limit'])) {
			$this->data['abandoned_cart_reminder_reward_limit'] = $this->request->post['abandoned_cart_reminder_reward_limit'];
		} elseif ($this->config->get('abandoned_cart_reminder_reward_limit')) { 
			$this->data['abandoned_cart_reminder_reward_limit'] = $this->config->get('abandoned_cart_reminder_reward_limit');
		} else {
			$this->data['abandoned_cart_reminder_reward_limit'] = '';
		}
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['front_base_url'] = defined('HTTPS_CATALOG')? HTTPS_CATALOG : HTTP_CATALOG;
		} else {
			$this->data['front_base_url'] = HTTP_CATALOG;
		}
		
		$this->data['token'] = $this->session->data['token'];
				
		$this->template = 'module/abandoned_cart_reminder.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/abandoned_cart_reminder')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (utf8_strlen($this->request->post['abandoned_cart_reminder_secret_code']) < 5) {
			$this->error['secret_code'] = $this->language->get('error_secret_code');
		}

		if (utf8_strlen($this->request->post['abandoned_cart_reminder_delay']) < 1) {
			$this->error['delay'] = $this->language->get('error_delay');
		}	

		if (utf8_strlen($this->request->post['abandoned_cart_reminder_max_reminders']) < 1) {
			$this->error['max_reminders'] = $this->language->get('error_max_reminders');
		}	
		
		if ($this->request->post['abandoned_cart_reminder_use_html_email'] == 1 && !$this->isHTMLEmailExtensionInstalled() ) {
			$this->error['use_html_email'] = $this->language->get('error_html_email_not_installed');
			$this->error['warning'] = $this->language->get('error_html_email_not_installed');
		}

		if ($this->request->post['abandoned_cart_reminder_add_coupon']){
			if (utf8_strlen($this->request->post['abandoned_cart_reminder_coupon_amount']) < 1) {
				$this->error['coupon_amount'] = $this->language->get('error_coupon_amount');
			}
			
			if (utf8_strlen($this->request->post['abandoned_cart_reminder_coupon_expire']) < 1) {
				$this->error['coupon_expire'] = $this->language->get('error_coupon_expire');
			}
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
	private function isHTMLEmailExtensionInstalled() {
		$installed = false;
		
		if ($this->config->get('html_email_default_word') && file_exists(DIR_APPLICATION . 'model/tool/html_email.php') && file_exists(DIR_CATALOG . 'model/tool/html_email.php')) {
			$installed = true;	
		}
		
		return $installed;
	}
	
}
?>