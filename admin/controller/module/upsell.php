<?php
class ControllerModuleUpsell extends Controller {

	private $error = array(); 
	
	public function index() {   
		$this->load->language('module/upsell');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {


            if (isset($this->request->post['upsell_beallitas_upsell_csoportok']) ) {
                $this->request->post['upsell_beallitas_upsell_csoportok'] = $this->upsellRendez($this->request->post['upsell_beallitas_upsell_csoportok']);
            }

            foreach($this->request->post as $key=>$value) {
                $beallit[$key] = $value;
            }
            $this->document->setTitle($this->language->get('heading_title'));
            $this->load->model('setting/setting');

            $this->model_setting_setting->editSetting('upsell', $beallit);

            $this->load->model('setting/extension');

            $this->session->data['success'] = $this->language->get('text_success');
            $this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');


		$this->data['entry_status']                     = $this->language->get('entry_status');
		$this->data['entry_fejlec_latszik']             = $this->language->get('entry_fejlec_latszik');
		$this->data['entry_megfelelo_termekek']         = $this->language->get('entry_megfelelo_termekek');
		$this->data['entry_automata_bellitas']          = $this->language->get('entry_automata_bellitas');
		$this->data['entry_max_termek']                 = $this->language->get('entry_max_termek');
		$this->data['entry_design_beallitas']           = $this->language->get('entry_design_beallitas');
		$this->data['entry_bought_relacio']             = $this->language->get('entry_bought_relacio');
		$this->data['entry_bought_order_status']        = $this->language->get('entry_bought_order_status');

        $this->data['text_enabled']                     = $this->language->get('text_enabled');
        $this->data['text_disabled']                    = $this->language->get('text_disabled');
		$this->data['text_kosar_nem_latszik']           = $this->language->get('text_kosar_nem_latszik');
        $this->data['text_kosarba_tett_termek_latszik'] = $this->language->get('text_kosarba_tett_termek_latszik');
        $this->data['text_kosar_tartalma_latszik']      = $this->language->get('text_kosar_tartalma_latszik');
        $this->data['text_yes']                         = $this->language->get('text_yes');
        $this->data['text_no']                          = $this->language->get('text_no');

		$this->data['button_save']      = $this->language->get('button_save');
		$this->data['button_cancel']    = $this->language->get('button_cancel');
        $this->data['button_remove']    = $this->language->get('button_remove');
        $this->data['text_select_all']  = $this->language->get('text_select_all');
        $this->data['text_unselect_all']= $this->language->get('text_unselect_all');
        $this->data['entry_status']     = $this->language->get('entry_status');

        $this->data['termek_csoportok'][0] = $this->language->get('text_disabled');
        $this->data['termek_csoportok'][1] = $this->language->get('text_auto');
        $this->data['termek_csoportok'][2] = $this->language->get('text_szinten_vasarolt');
        $this->data['termek_csoportok'][3] = $this->language->get('text_kapcsolodo');
        $this->data['termek_csoportok'][4] = $this->language->get('text_tartozekok');
        $this->data['termek_csoportok'][5] = $this->language->get('text_kiemelt');
        $this->data['termek_csoportok'][6] = $this->language->get('text_bestseller');
        $this->data['termek_csoportok'][7] = $this->language->get('text_latest');



        if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/upsell', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/upsell', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

        $this->load->model('module/also_bought');
        $this->data['orders_statuses'] = $this->model_module_also_bought->getOrderStatuses();


        $this->data['upsell'] = array();
		
		if (isset($this->request->post['upsell'])) {
			$this->data['upsell'] = $this->request->post['upsell'];
		} elseif ($this->config->get('upsell')) {
			$this->data['upsell'] = $this->config->get('upsell');
		}

        /* upsell tpl szerkesztő */
        $upsell_beallitas_upsell_csoportok = $this->config->get('upsell_beallitas_upsell_csoportok');
        if (is_null($upsell_beallitas_upsell_csoportok)) {
            $upsell_beallitas_upsell_csoportok = array();
        }


        foreach($upsell_beallitas_upsell_csoportok as $key=>$value){
            if (!is_array($value)) {
                $upsell_beallitas_upsell_csoportok[$key] = array(
                    'default'   => array(
                        'stauts' =>'0'
                    )
                );
            }
            if (substr($key,0,8) == "#csoport") {
                //unset ($upsell_beallitas_upsell_csoportok[$key]);
            }
        }
        $this->data['upsell_beallitas_upsell_csoportok'] = $upsell_beallitas_upsell_csoportok;

        $upsell_beallitas_upsell_csoportok_kiegeszito = array_keys($upsell_beallitas_upsell_csoportok);

        $this->data['upsell_beallitas_upsell_csoportok_kiegeszito'] = $upsell_beallitas_upsell_csoportok_kiegeszito;


        $files = glob(DIR_CATALOG . 'view/theme/default/template/module/upsell/*.tpl');
        $files_beallitasok = glob(DIR_CATALOG . 'view/theme/default/template/module/upsell/*.php');

        $datas=array();
        foreach ($files as $file) {
            $beallitas_adatok=array();
            $extension = basename($file, '.tpl');
            $beallitas = str_replace(".tpl",".php",$file);
            $datas[$extension] = array();


            if (in_array($beallitas,$files_beallitasok)) {
                $_=array();
                require($beallitas);

                $beallitas_adatok = array_merge($beallitas_adatok, $_);
                foreach($beallitas_adatok as $keydata=>$beallitas_adat) {
                    $datas[$extension][$keydata] = $beallitas_adat;
                }
            }
        }
        $this->data['upsell_beallitas_upsell_csoportok_files'] = $datas;


        /* upsell tpl szerkesztő vége */



        $this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = 'module/upsell.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/upsell')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}

    private function upsellRendez($tomb) {

        foreach($tomb as $key=>$value) {
            if (isset($value['templates'])) {
                $tomb[$key]['templates'] = $this->config->rendezes($value['templates'],"sort_order","ASC");
            }
        }
        return $tomb;


        /*$gyujto = array();
        foreach($tomb as $key=>$value) {
            $gyujto[] = array(
                'kulcs' => ''.$key,
                'ertek' => $value
            );
        }
        $gyujto = $this->config->rendezes($gyujto,"kulcs");
        $vissza = array();
        foreach($gyujto as $value) {
            if (isset($value['ertek']['templates'])) {
                $value['ertek']['templates'] = $this->config->rendezes($value['ertek']['templates'],"sort_order");
            }
            $vissza[$value['kulcs']] = $value['ertek'];


        }
        return $vissza;*/

    }
}
?>