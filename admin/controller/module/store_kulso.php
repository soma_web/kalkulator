<?php
class ControllerModuleStoreKulso extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->load->language('module/store_kulso');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('store_kulso', $this->request->post);
					 
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['entry_fejlec_latszik'] = $this->language->get('entry_fejlec_latszik');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_content_top'] = $this->language->get('text_content_top');
		$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$this->data['text_column_left'] = $this->language->get('text_column_left');
		$this->data['text_column_right'] = $this->language->get('text_column_right');
		$this->data['text_slider'] = $this->language->get('text_slider');
		$this->data['text_slider_use'] = $this->language->get('text_slider_use');

		$this->data['entry_admin'] = $this->language->get('entry_admin');
		$this->data['entry_text'] = $this->language->get('entry_text');
		$this->data['entry_mirror'] = $this->language->get('entry_mirror');
		$this->data['entry_around'] = $this->language->get('entry_around');
		$this->data['entry_speed'] = $this->language->get('entry_speed');
		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['entry_opacity'] = $this->language->get('entry_opacity');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_position'] = $this->language->get('entry_position');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_width'] = $this->language->get('entry_width');
		$this->data['entry_height'] = $this->language->get('entry_height');
		$this->data['entry_egyeb_link'] = $this->language->get('entry_egyeb_link');
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['entry_egyeb_link_title'] = $this->language->get('entry_egyeb_link_title');
		$this->data['entry_egyeb_link_link'] = $this->language->get('entry_egyeb_link_link');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');

        $this->data['text_browse'] = $this->language->get('text_browse');
        $this->data['text_clear'] = $this->language->get('text_clear');
        $this->data['text_image_manager'] = $this->language->get('text_image_manager');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/store_kulso', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/store_kulso', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['token'] =  $this->session->data['token'];



        if (isset($this->request->post['store_kulso_text'])) {
			$this->data['store_kulso_text'] = $this->request->post['store_kulso_text'];
		} else {
			$this->data['store_kulso_text'] = $this->config->get('store_kulso_text');
		}


        if (isset($this->request->post['store_kulso_image_width'])) {
            $this->data['store_kulso_image_width'] = $this->request->post['store_kulso_image_width'];
        } else {
            $this->data['store_kulso_image_width'] = $this->config->get('store_kulso_image_width');
        }
        if (isset($this->request->post['store_kulso_image_height'])) {
            $this->data['store_kulso_image_height'] = $this->request->post['store_kulso_image_height'];
        } else {
            $this->data['store_kulso_image_height'] = $this->config->get('store_kulso_image_height');
        }



        if (isset($this->request->post['store_egyeb_link'])) {
            $this->data['store_egyeb_link'] = $this->request->post['store_egyeb_link'];
        } else {
            $this->data['store_egyeb_link'] = $this->config->get('store_egyeb_link');
        }

        if (isset($this->request->post['store_egyeb_link_link'])) {
            $this->data['store_egyeb_link_link'] = $this->request->post['store_egyeb_link_link'];
        } else {
            $this->data['store_egyeb_link_link'] = $this->config->get('store_egyeb_link_link');
        }

        if (isset($this->request->post['store_egyeb_link_image'])) {
            $this->data['store_egyeb_link_image'] = $this->request->post['store_egyeb_link_image'];
        } else {
            $this->data['store_egyeb_link_image'] = $this->config->get('store_egyeb_link_image');
        }
        $this->load->model("tool/image");

        if (!empty($this->data['store_egyeb_link_image']) && file_exists(DIR_IMAGE . $this->data['store_egyeb_link_image'])) {
            $this->data['thumb'] = $this->model_tool_image->resize($this->data['store_egyeb_link_image'], 100, 100);
        } else {
            $this->data['thumb'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
        }
        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);


        $this->load->model('localisation/language');
        $this->data['languages'] = $this->model_localisation_language->getLanguages();

        foreach($this->data['languages'] as $language) {
            if ($this->config->get('config_language') != $language['code']) {
                $store_egyeb_link_title = 'store_egyeb_link_title_'.$language['code'];
                if ( isset($this->request->post[$store_egyeb_link_title]) ) {
                    $this->data[$store_egyeb_link_title] = $this->request->post[$store_egyeb_link_title];
                } else {
                    $this->data[$store_egyeb_link_title] = $this->config->get($store_egyeb_link_title);
                }
            }
        }

        if (isset($this->request->post['store_egyeb_link_title'])) {
            $this->data['store_egyeb_link_title'] = $this->request->post['store_egyeb_link_title'];
        } else {
            $this->data['store_egyeb_link_title'] = $this->config->get('store_egyeb_link_title');
        }



		$this->data['modules'] = array();
		
		if (isset($this->request->post['store_kulso_module'])) {
			$this->data['modules'] = $this->request->post['store_kulso_module'];
		} elseif ($this->config->get('store_kulso_module')) {
			$this->data['modules'] = $this->config->get('store_kulso_module');
		}
		
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
				
		$this->template = 'module/store_kulso.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/store_kulso')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>