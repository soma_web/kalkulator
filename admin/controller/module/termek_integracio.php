<?php
class ControllerModuleTermekIntegracio extends Controller {
	private $error = array();

    public function index() {
        $this->load->language('module/termek_integracio');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/termek_integracio');

        $this->getList();
    }

    public function insert() {
        $this->load->language('module/termek_integracio');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/termek_integracio');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_module_termek_integracio->addIntegracio($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('module/termek_integracio', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function update() {
        $this->load->language('module/termek_integracio');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/termek_integracio');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_module_termek_integracio->editIntegracio($this->request->get['termek_integracio_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('module/termek_integracio', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('module/termek_integracio');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/termek_integracio');

        if (isset($this->request->post['selected']) && $this->validateForm()) {
            foreach ($this->request->post['selected'] as $termek_integracio_id) {
                $this->model_module_termek_integracio->deleteIntegracio($termek_integracio_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('module/termek_integracio', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    public function getList() {
        isset($this->request->get['sort'])  ? $sort     = $this->request->get['sort']   : $sort     = 'selected_product_id';
        isset($this->request->get['order']) ? $order    = $this->request->get['order']  : $order    = 'ASC';
        isset($this->request->get['page'])  ? $page     = $this->request->get['page']   : $page     = 1;

        $url = '';
        isset($this->request->get['sort'])  ? $url .= '&sort=' . $this->request->get['sort']    : '';
        isset($this->request->get['order']) ? $url .= '&order=' . $this->request->get['order']  : '';
        isset($this->request->get['page'])  ? $url .= '&page=' . $this->request->get['page']    : '';

        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: '
        );
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('module/termek_integracio', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: '
        );

        $this->data['insert']   = $this->url->link('module/termek_integracio/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $this->data['delete']   = $this->url->link('module/termek_integracio/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->data['heading_title']        = $this->language->get('heading_title');
        $this->data['text_no_results']      = $this->language->get('text_no_results');
        $this->data['column_termek_integracio_id'] = $this->language->get('column_termek_integracio_id');
        $this->data['column_name']          = $this->language->get('column_name');
        $this->data['column_status']        = $this->language->get('column_status');
        $this->data['column_action']        = $this->language->get('column_action');
        $this->data['column_connects']      = $this->language->get('column_connects');
        $this->data['button_insert']        = $this->language->get('button_insert');
        $this->data['button_delete']        = $this->language->get('button_delete');
        $this->data['button_legyartas']     = $this->language->get('button_legyartas');
        $this->data['text_enabled']         = $this->language->get('text_enabled');
        $this->data['text_disabled']        = $this->language->get('text_disabled');
        $this->data['text_url_magyarazat']  = HTTP_CATALOG.$this->language->get('text_url_magyarazat');
        $this->data['text_url_elerhetosegek']  = $this->language->get('text_url_elerhetosegek');


        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }
        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['sort_termek_integracio_id'] = $this->url->link('module/termek_integracio', 'token=' . $this->session->data['token'] . '&sort=termek_integracio_id' . $url, 'SSL');
        $this->data['sort_name'] = $this->url->link('module/termek_integracio', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
        $this->data['sort_status'] = $this->url->link('module/termek_integracio', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');


        $this->data['termek_integraciok'] = array();

        $data = array(
            'sort'  => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit' => $this->config->get('config_admin_limit')
        );


        $url = '';
        isset($this->request->get['sort'])  ? $url .= '&sort=' . $this->request->get['sort'] : '';
        isset($this->request->get['order']) ? $url .= '&order=' . $this->request->get['order'] : '';
        isset($this->request->get['page'])  ? $url .= '&page=' . $this->request->get['page'] : '';

        $termek_integracio_total = $this->model_module_termek_integracio->getTotalIntegrations($data);

        $results = $this->model_module_termek_integracio->getIntegrations($data);

        if (!empty($results)) {
            foreach ($results as $result) {
                $action = array();

                $action[] = array(
                    'text' => $this->language->get('text_edit'),
                    'href' => $this->url->link('module/termek_integracio/update', 'token=' . $this->session->data['token'] . '&termek_integracio_id=' . $result['termek_integracio_id'] . $url, 'SSL')
                );


                $status         = $result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled');

                $connects = '';
                if ($result['connects']) {
                    $connects = array_map(function ($v, $k) {
                        return $k . '' . $v;
                    }, array(), array_keys($result['connects']));
                    $i = 0;
                    foreach ($connects as $key => $value) {
                        if ($i == 4) {
                            $i = 0;
                            $connects[$key] = '<br>'.$connects[$key];
                        }
                        $i++;
                    }
                    $connects = implode(', ', $connects);
                }

                $this->data['termek_integraciok'][] = array(
                    'termek_integracio_id'       => $result['termek_integracio_id'],
                    'name'          => $result['name'],
                    'name_event'    => 'UPDATE '.DB_PREFIX.'termek_integracio set name=\'modositas_ertek\' WHERE termek_integracio_id='.$result['termek_integracio_id'],
                    'status'        => $status,
                    'status_value'  => $result['status'],
                    'status_event'  => 'UPDATE '.DB_PREFIX.'termek_integracio set status=\'modositas_ertek\' WHERE termek_integracio_id='.$result['termek_integracio_id'],
                    'connects'      => $connects,
                    'selected'      => isset($this->request->post['selected']) && in_array($result['termek_integracio_id'], $this->request->post['selected']),
                    'action'        => $action
                );

            }
        }

        $this->data['token']            = $this->session->data['token'];
        $this->data['modositas_route']  = "module/termek_integracio";

        $this->config->get('engedelyez_integracio_argep')           ? $engedelyezett_integraciok[] = 'argep.xml' : '';
        $this->config->get('engedelyez_integracio_olcso')           ? $engedelyezett_integraciok[] = 'olcso.xml' : '';
        $this->config->get('engedelyez_integracio_kirakat')         ? $engedelyezett_integraciok[] = 'kirakat.xml' : '';
        $this->config->get('engedelyez_integracio_joaron')          ? $engedelyezett_integraciok[] = 'joaron.csv' : '';
        $this->config->get('engedelyez_integracio_arukereso')       ? $engedelyezett_integraciok[] = 'arukereso.xml': '';
        $this->config->get('engedelyez_integracio_arkozpont')       ? $engedelyezett_integraciok[] = 'arkozpont.xml' : '';
        $this->config->get('engedelyez_integracio_osszehasonlitom') ? $engedelyezett_integraciok[] = 'osszehasonlitom.xml' : '';
        $this->config->get('engedelyez_integracio_olcsobbat')       ? $engedelyezett_integraciok[] = 'olcsobbat.xml' : '';

        $this->data['engedelyezett_integraciok'] = $engedelyezett_integraciok;

        $pagination = new Pagination();
        $pagination->total = $termek_integracio_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('module/termek_integracio', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();

        $this->data['sort'] = $sort;
        $this->data['order'] = $order;

        $this->template = 'module/termek_integracio_list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    public function getForm() {
		$this->load->language('module/termek_integracio');
        $this->load->model('module/termek_integracio');

		$this->document->setTitle($this->language->get('heading_title'));

				
		$this->data['heading_title']        = $this->language->get('heading_title');

		$this->data['text_enabled']         = $this->language->get('text_enabled');
		$this->data['text_disabled']        = $this->language->get('text_disabled');
		$this->data['text_all']             = $this->language->get('text_all');
		$this->data['text_all_product']     = $this->language->get('text_all_product');
		$this->data['text_all_select_product'] = $this->language->get('text_all_select_product');
		$this->data['text_darab']           = $this->language->get('text_darab');
		$this->data['text_magyarazat']      = $this->language->get('text_magyarazat');
		$this->data['text_description']     = $this->language->get('text_description');
		$this->data['text_short_description']= $this->language->get('text_short_description');
		$this->data['text_yes']             = $this->language->get('text_yes');
		$this->data['text_no']              = $this->language->get('text_no');

		$this->data['entry_status']         = $this->language->get('entry_status');
		$this->data['entry_ar_tol_ig']      = $this->language->get('entry_ar_tol');
		$this->data['entry_category']       = $this->language->get('entry_category');
		$this->data['entry_levalogatott']   = $this->language->get('entry_levalogatott');
		$this->data['entry_hozzaad_neve']   = $this->language->get('entry_hozzaad_neve');
		$this->data['entry_hozzaad_model']  = $this->language->get('entry_hozzaad_model');
		$this->data['entry_levelogatott']   = $this->language->get('entry_levelogatott');
		$this->data['entry_product_status'] = $this->language->get('entry_product_status');
		$this->data['entry_name']           = $this->language->get('entry_name');
		$this->data['entry_kapcsolodo']     = $this->language->get('entry_kapcsolodo');
		$this->data['entry_shipping']       = $this->language->get('entry_shipping');
		$this->data['entry_shipping_time_in_stock']     = $this->language->get('entry_shipping_time_in_stock');
		$this->data['entry_shipping_time_not_stock']    = $this->language->get('entry_shipping_time_not_stock');
		$this->data['entry_warranty']       = $this->language->get('entry_warranty');
		$this->data['entry_text_pieces']    = $this->language->get('entry_text_pieces');
		$this->data['entry_description']    = $this->language->get('entry_description');
		$this->data['entry_pickpack']       = $this->language->get('entry_pickpack');
		$this->data['entry_shipping_price'] = $this->language->get('entry_shipping_price');

		$this->data['button_levalogat']     = $this->language->get('button_levalogat');
		$this->data['button_save']          = $this->language->get('button_save');
		$this->data['button_cancel']        = $this->language->get('button_cancel');
		$this->data['button_remove']        = $this->language->get('button_remove');
        $this->data['token']                = $this->session->data['token'];

        if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['image'])) {
			$this->data['error_image'] = $this->error['image'];
		} else {
			$this->data['error_image'] = array();
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/termek_integracio', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

        $url = '';
        isset($this->request->get['sort'])  ? $url .= '&sort='  . $this->request->get['sort']   : '';
        isset($this->request->get['order']) ? $url .= '&order=' . $this->request->get['order']  : '';
        isset($this->request->get['page'])  ? $url .= '&page='  . $this->request->get['page']   : '';

		$this->data['url'] = $url;
		$this->data['action'] = $this->url->link('module/termek_integracio', 'token=' . $this->session->data['token'].$url, 'SSL');
		$this->data['cancel'] = $this->url->link('module/termek_integracio', 'token=' . $this->session->data['token'].$url, 'SSL');


        $termek_integraciok = array();
        if (isset($this->request->get['termek_integracio_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $termek_integraciok = $this->model_module_termek_integracio->getIntegration($this->request->get['termek_integracio_id']);
        }

        if (isset($this->request->post['name'])) {
            $this->data['name'] = $this->request->post['name'];
        } elseif (!empty($termek_integraciok)) {
            $this->data['name'] = $termek_integraciok['name'];
        } else {
            $this->data['name'] = '';
        }
        if (isset($this->request->post['status'])) {
            $this->data['status'] = $this->request->post['status'];
        } elseif (!empty($termek_integraciok)) {
            $this->data['status'] = $termek_integraciok['status'];
        } else {
            $this->data['status'] = 0;
        }
        if (isset($this->request->post['shipping'])) {
            $this->data['shipping'] = $this->request->post['shipping'];
        } elseif (!empty($termek_integraciok)) {
            $this->data['shipping'] = $termek_integraciok['shipping'];
        } else {
            $this->data['shipping'] = 0;
        }
        if (isset($this->request->post['shipping_time_in_stock'])) {
            $this->data['shipping_time_in_stock'] = $this->request->post['shipping_time_in_stock'];
        } elseif (!empty($termek_integraciok['shipping_time_in_stock'])) {
            $this->data['shipping_time_in_stock'] = $termek_integraciok['shipping_time_in_stock'];
        } else {
            $this->data['shipping_time_in_stock'] = 3;
        }
        if (isset($this->request->post['shipping_time_not_stock'])) {
            $this->data['shipping_time_not_stock'] = $this->request->post['shipping_time_not_stock'];
        } elseif (!empty($termek_integraciok['shipping_time_not_stock'])) {
            $this->data['shipping_time_not_stock'] = $termek_integraciok['shipping_time_not_stock'];
        } else {
            $this->data['shipping_time_not_stock'] = 3;
        }
        if (isset($this->request->post['warranty'])) {
            $this->data['warranty'] = $this->request->post['warranty'];
        } elseif (!empty($termek_integraciok['warranty'])) {
            $this->data['warranty'] = $termek_integraciok['warranty'];
        } else {
            $this->data['warranty'] = 0;
        }
        if (isset($this->request->post['pieces'])) {
            $this->data['pieces'] = $this->request->post['pieces'];
        } elseif (!empty($termek_integraciok['pieces'])) {
            $this->data['pieces'] = $termek_integraciok['pieces'];
        } else {
            $this->data['pieces'] = 0;
        }
        if (isset($this->request->post['description'])) {
            $this->data['description'] = $this->request->post['description'];
        } elseif (!empty($termek_integraciok['description'])) {
            $this->data['description'] = $termek_integraciok['description'];
        } else {
            $this->data['description'] = 'description';
        }
        if (isset($this->request->post['pickpack'])) {
            $this->data['pickpack'] = $this->request->post['pickpack'];
        } elseif (!empty($termek_integraciok['pickpack'])) {
            $this->data['pickpack'] = $termek_integraciok['pickpack'];
        } else {
            $this->data['pickpack'] = 0;
        }
        if (isset($this->request->post['shipping_price'])) {
            $this->data['shipping_price'] = $this->request->post['shipping_price'];
        } elseif (!empty($termek_integraciok['shipping_price'])) {
            $this->data['shipping_price'] = $termek_integraciok['shipping_price'];
        } else {
            $this->data['shipping_price'] = 0;
        }
        if (isset($this->request->post['levalogatott'])) {
            $this->data['levalogatott'] = $this->request->post['levalogatott'];
        } elseif (!empty($termek_integraciok['products'])) {
            $this->data['levalogatott'] = $termek_integraciok['products'];
        } else {
            $this->data['levalogatott'] = array();
        }
        if (isset($this->request->post['connects'])) {
            $this->data['connects'] = $this->request->post['connects'];
        } elseif (!empty($termek_integraciok['connects'])) {
            $this->data['connects'] = $termek_integraciok['connects'];
        } else {
            $this->data['connects'] = array();
        }


        $engedelyezett_integraciok = array();

        $this->config->get('engedelyez_integracio_argep')           ? $engedelyezett_integraciok[] = array('type' => 'argep',           'name'=>'Árgép') : '';
        $this->config->get('engedelyez_integracio_olcso')           ? $engedelyezett_integraciok[] = array('type' => 'olcso',           'name'=>'Olcsó') : '';
        $this->config->get('engedelyez_integracio_kirakat')         ? $engedelyezett_integraciok[] = array('type' => 'kirakat',         'name'=>'Kirakat') : '';
        $this->config->get('engedelyez_integracio_joaron')          ? $engedelyezett_integraciok[] = array('type' => 'joaron',          'name'=>'Jó áron') : '';
        $this->config->get('engedelyez_integracio_arukereso')       ? $engedelyezett_integraciok[] = array('type' => 'arukereso',       'name'=>'Árúkereső') : '';
        $this->config->get('engedelyez_integracio_arkozpont')       ? $engedelyezett_integraciok[] = array('type' => 'arkozpont',       'name'=>'Árközpont') : '';
        $this->config->get('engedelyez_integracio_osszehasonlitom') ? $engedelyezett_integraciok[] = array('type' => 'osszehasonlitom', 'name'=>'Összehasonlítom') : '';
        $this->config->get('engedelyez_integracio_olcsobbat')       ? $engedelyezett_integraciok[] = array('type' => 'olcsobbat',       'name'=>'Olcsóbbat') : '';

        $this->data['engedelyezett_integraciok'] = $engedelyezett_integraciok;

        $this->load->model('catalog/category');

        $this->data['categories'] = array();
        $arr2 = $this->model_catalog_category->getCategoriesAll();
        $categoria_tomb = $this->model_catalog_category->buildTree2($arr2,0,'');
        $categoria_tomb = $this->model_catalog_category->onlyFindProduct($categoria_tomb);
        $this->data['categories'] = $this->model_catalog_category->categorySort($categoria_tomb);



        $this->load->model('setting/extension');

        $extensions = $this->model_setting_extension->getInstalled('shipping');
        $no_module = $this->model_setting_extension->getNoModule();

        $this->data['shippings'] = array();

        if ($extensions) {
            foreach ($extensions as $file) {
                if (!in_array($file, $no_module) && $this->config->get($file . '_status')){

                    $this->load->language('shipping/' . $file);

                    $this->data['shippings'][] = array(
                        'name'       => $this->config->get($file . '_header') ? $this->config->get($file . '_header') : $this->language->get('heading_title'),
                        'sort_order' => $this->config->get($file . '_sort_order'),
                        'file_name' => $file
                    );
                }
            }
        }

        $this->data['descriptions'] = array();
        $this->data['descriptions'][] = 'description';
        $this->data['descriptions'][] = 'short_description';



        $this->template = 'module/termek_integracio_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/termek_integracio')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (isset($this->request->post['termek_integracio_module'])) {

		}	
				
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}

    public function levalogat() {
        $json = array();

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            if ($this->request->request['filter_ar']) {
                $data['ar_tol_ig'] = explode(',', $this->request->request['filter_ar']);
            }
            if ($this->request->request['filter_categorys']) {
                $data['categoryes'] = explode(',', $this->request->request['filter_categorys']);
            }
            if ($this->request->request['filter_levalogatott']) {
                $data['products'] = explode(',', $this->request->request['filter_levalogatott']);
            }
            if ($this->request->request['filter_status']) {
                $data['status'] = $this->request->request['filter_status'];
            } else {
                $data['status'] = 0;
            }

            $this->load->model('module/termek_integracio');
            $json['products'] = $this->model_module_termek_integracio->getSzurtProducts($data);

            $json['success'] = $this->language->get('text_success');
        } else {
            $json['error'] = $this->language->get('error_save');
        }

        $this->response->setOutput(json_encode($json));
    }

    public function mentes() {

        $json = array();
        if ($this->request->get['elso'] == "true") {
            if (isset($this->session->data['termek'])) {
                unset ($this->session->data['termek']);
            }
        }
        if (!isset($this->session->data['termek'])) {
            $this->session->data['termek'] = $this->request->post;
        } else {
            foreach($this->request->post as $key=>$option) {
                if (is_array($option)) {
                    foreach($option as $opt_key=>$value) {
                        if (is_array($value)) {
                            foreach($value as $po_key=>$p_option_value) {
                                if (is_array($p_option_value)) {
                                    foreach($p_option_value as $product_key=>$product_option_value) {
                                        if (is_array($product_option_value)) {
                                            foreach($product_option_value as $product_key_zaro=>$option_value_zaro) {
                                                $this->session->data['termek'][$key][$opt_key][$po_key][$product_key][$product_key_zaro] = $option_value_zaro;
                                            }
                                        } else { $this->session->data['termek'][$key][$opt_key][$po_key][] = $product_option_value; }
                                    }
                                } else { $this->session->data['termek'][$key][$opt_key][] = $p_option_value; }
                            }
                        } else {
                            $this->session->data['termek'][$key][] = $value;
                        }
                    }
                } else {
                    $this->session->data['termek'][] = $option;
                }
            }
        }




        if ($this->request->get['utolso'] == "true" ) {
            $this->load->model('module/termek_integracio');
            $data = $this->session->data['termek'];
            unset ($this->session->data['termek']);

            if (!empty($this->request->get["termek_integracio_id"])) {
                $this->model_module_termek_integracio->editIntegracio($this->request->get['termek_integracio_id'], $data);
            } else {
                $this->model_module_termek_integracio->addIntegracio($data);
            }
        } else {
            $this->response->setOutput(json_encode($json));
        }

    }
    protected function validateForm() {

        if (!$this->user->hasPermission('modify', 'module/termek_integracio')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    public function modosit() {
        $this->load->language('module/termek_integracio');
        $siker = false;
        $json = array();

        if (!$this->user->hasPermission('modify', 'module/termek_integracio')) {
            $json['error'] = $this->language->get('error_permission');
        }

        if (empty($json['error']) && !empty($_POST['eljaras'])) {
            $sql = str_replace('modositas_ertek',$_POST['ertek'],$_POST['eljaras']);
            $siker = $this->db->query($sql);
            if ($siker) {
                $json['success'] =  $this->language->get('success_modositas');
            } else {
                $json['error'] = $this->language->get('error_sql');

            }
        } elseif (empty($json['error'])) {
            $json['error'] = $this->language->get('error_post');

        }

        $this->response->setOutput(json_encode($json));

    }
}
?>