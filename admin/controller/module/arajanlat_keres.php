<?php
class ControllerModuleArajanlatKeres extends Controller {
	private $error = array(); 
	
	public function index() {
		$this->load->language('module/arajanlat_keres');
        $this->load->model('module/arajanlat_keres');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('arajanlat_keres', $this->request->post);
					
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title']        = $this->language->get('heading_title');

		$this->data['text_enabled']         = $this->language->get('text_enabled');
		$this->data['text_disabled']        = $this->language->get('text_disabled');

		$this->data['entry_status']         = $this->language->get('entry_status');
		$this->data['entry_ar_tol']         = $this->language->get('entry_ar_tol');
		$this->data['entry_ar_ig']          = $this->language->get('entry_ar_ig');
		$this->data['entry_levalogatott']   = $this->language->get('entry_levalogatott');
		$this->data['entry_hozzaad']        = $this->language->get('entry_hozzaad');
		$this->data['entry_kivett']         = $this->language->get('entry_kivett');

		$this->data['button_levalogat']     = $this->language->get('button_levalogat');
		$this->data['button_save']          = $this->language->get('button_save');
		$this->data['button_cancel']        = $this->language->get('button_cancel');
		$this->data['button_remove']        = $this->language->get('button_remove');
        $this->data['token']                 = $this->session->data['token'];

        if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['image'])) {
			$this->data['error_image'] = $this->error['image'];
		} else {
			$this->data['error_image'] = array();
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/arajanlat_keres', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/arajanlat_keres', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['modules'] = array();
		
		if (isset($this->request->post['arajanlat_keres_module'])) {
			$this->data['modules'] = $this->request->post['arajanlat_keres_module'];
		} elseif ($this->config->get('arajanlat_keres_module')) {
			$this->data['modules'] = $this->config->get('arajanlat_keres_module');
		}

        if (isset($this->data['modules']['altalanos']) && $this->data['modules']['altalanos']) {
            $products = array();
            foreach($this->data['modules']['altalanos'] as $value) {
                $products[] = $this->model_module_arajanlat_keres->getProduct($value);
            }
            $this->data['modules']['altalanos'] = $products;
        }

        if (isset($this->data['modules']['hozzaadott']) && $this->data['modules']['hozzaadott']) {
            $products = array();
            foreach($this->data['modules']['hozzaadott'] as $value) {
                $products[] = $this->model_module_arajanlat_keres->getProduct($value);
            }
            $this->data['modules']['hozzaadott'] = $products;
        }

        if (isset($this->data['modules']['kivett']) && $this->data['modules']['kivett']) {
            $products = array();
            foreach($this->data['modules']['kivett'] as $value) {
                $products[] = $this->model_module_arajanlat_keres->getProduct($value);
            }
            $this->data['modules']['kivett'] = $products;
        }


		$this->template = 'module/arajanlat_keres.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/arajanlat_keres')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (isset($this->request->post['arajanlat_keres_module'])) {

		}	
				
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}

    public function levalogat() {
        $json = array();

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $data['ar_tol'] = $this->request->request['arajanlat_keres_module']['ar_tol'];
            $data['ar_ig']  = $this->request->request['arajanlat_keres_module']['ar_ig'];
            $data['kivett'] = isset($this->request->request['arajanlat_keres_module']['kivett']) ? $this->request->request['arajanlat_keres_module']['kivett'] : "";

            $this->load->model('module/arajanlat_keres');
            $json['products'] = $this->model_module_arajanlat_keres->getSzurtProducts($data);

            $json['success'] = $this->language->get('text_success');
        } else {
            $json['error'] = $this->language->get('error_save');
        }

        $this->response->setOutput(json_encode($json));
    }
}
?>