<?php
class ControllerReportSaleOrderCsvbe extends Controller {
	public function index() {  
		$this->load->language('report/sale_order');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

        if (isset($this->request->get['filter_korosztaly'])) {
            $filter_korosztaly = $this->request->get['filter_korosztaly'];
        } else {
            $filter_korosztaly = '';
        }

        if (isset($this->request->get['filter_nem'])) {
            $filter_nem = $this->request->get['filter_nem'];
        } else {
            $filter_nem = '';
        }

        if (isset($this->request->get['filter_ingyenes'])) {
            $filter_ingyenes = $this->request->get['filter_ingyenes'];
        } else {
            $filter_ingyenes = '';
        }

        if (isset($this->request->get['filter_iskolai_vegzettseg'])) {
            $filter_iskolai_vegzettseg = $this->request->get['filter_iskolai_vegzettseg'];
        } else {
            $filter_iskolai_vegzettseg = '';
        }

        if (isset($this->request->get['filter_group'])) {
			$filter_group = $this->request->get['filter_group'];
		} else {
			$filter_group = 'week';
		}
		
		if (isset($this->request->get['filter_order_status_id'])) {
			$filter_order_status_id = $this->request->get['filter_order_status_id'];
		} else {
			$filter_order_status_id = 0;
		}	
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$url = '';
						
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

        if (isset($this->request->get['filter_korosztaly'])) {
            $url .= '&filter_korosztaly=' . $this->request->get['filter_korosztaly'];
        }

        if (isset($this->request->get['filter_nem'])) {
            $url .= '&filter_nem=' . $this->request->get['filter_nem'];
        }

        if (isset($this->request->get['filter_ingyenes'])) {
            $url .= '&filter_ingyenes=' . $this->request->get['filter_ingyenes'];
        }
		
		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}		

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}
								
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

        if (isset($this->request->get['filter_iskolai_vegzettseg'])) {
            $url .= '&filter_iskolai_vegzettseg=' . $this->request->get['filter_iskolai_vegzettseg'];
        }

   		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/sale_order', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->load->model('report/sale');
		
		$this->data['orders'] = array();
		
		$data = array(
			'filter_date_start'	     => $filter_date_start, 
			'filter_date_end'	     => $filter_date_end, 
			'filter_group'           => $filter_group,
            'filter_korosztaly'      => $filter_korosztaly,
            'filter_nem'             => $filter_nem,
            'filter_ingyenes'        => $filter_ingyenes,
            'filter_iskolai_vegzettseg'         => $filter_iskolai_vegzettseg,
			'filter_order_status_id' => $filter_order_status_id
		);
		
		#$order_total = $this->model_report_sale->getTotalOrders($data);
		
		$results = $this->model_report_sale->getOrders($data);
		
		foreach ($results as $result) {
			$this->data['orders'][] = array(
				'date_start' => date($this->language->get('date_format_short'), strtotime($result['date_start'])),
				'date_end'   => date($this->language->get('date_format_short'), strtotime($result['date_end'])),
				'orders'     => $result['orders'],
				'products'   => $result['products'],
				'tax'        => $this->currency->format($result['tax'], $this->config->get('config_currency')),
				'total'      => $this->currency->format($result['total'], $this->config->get('config_currency'))
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');
		
		$this->data['column_date_start'] = $this->language->get('column_date_start');
		$this->data['column_date_end'] = $this->language->get('column_date_end');
    	$this->data['column_orders'] = $this->language->get('column_orders');
		$this->data['column_products'] = $this->language->get('column_products');
		$this->data['column_tax'] = $this->language->get('column_tax');
		$this->data['column_total'] = $this->language->get('column_total');
        $this->data['column_iskolai_vegzettseg'] = $this->language->get('column_iskolai_vegzettseg');
		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		$this->data['entry_group'] = $this->language->get('entry_group');	
		$this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_korosztaly'] = $this->language->get('entry_korosztaly');
        $this->data['entry_nem'] = $this->language->get('entry_nem');
        $this->data['entry_ingyenes'] = $this->language->get('entry_ingyenes');
		$this->data['button_filter'] = $this->language->get('button_filter');
        $this->data['entry_iskolai_vegzettseg'] = $this->language->get('entry_iskolai_vegzettseg');
		
		$this->data['token'] = $this->session->data['token'];

        $this->data['iskolai_vegzettsegek'] = $this->config->get("vegzettseg");

		$this->load->model('localisation/order_status');
		
		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$this->data['groups'] = array();

		$this->data['groups'][] = array(
			'text'  => $this->language->get('text_year'),
			'value' => 'year',
		);

		$this->data['groups'][] = array(
			'text'  => $this->language->get('text_month'),
			'value' => 'month',
		);

		$this->data['groups'][] = array(
			'text'  => $this->language->get('text_week'),
			'value' => 'week',
		);

		$this->data['groups'][] = array(
			'text'  => $this->language->get('text_day'),
			'value' => 'day',
		);


        $this->load->model('localisation/country');
        $this->data['eletkors'] = $this->model_localisation_country->getEletkor();

        $this->data['nems'][] = array(
            'ertek' => 1,
            'nev'   => $this->language->get('select_ferfi')
        );
        $this->data['nems'][] = array(
            'ertek' => 2,
            'nev'   => $this->language->get('select_holgy')
        );

        $this->data['ingyenesek'][] = array(
            'ertek' => 0,
            'nev'   => $this->language->get('select_utalvany')
        );
        $this->data['ingyenesek'][] = array(
            'ertek' => 1,
            'nev'   => $this->language->get('select_kupon')
        );

		$url = '';
						
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		
		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}		

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

        if (isset($this->request->get['filter_korosztaly'])) {
            $url .= '&filter_korosztaly=' . $this->request->get['filter_korosztaly'];
        }
        if (isset($this->request->get['filter_nem'])) {
            $url .= '&filter_nem=' . $this->request->get['filter_nem'];
        }
        if (isset($this->request->get['filter_ingyenes'])) {
            $url .= '&filter_ingyenes=' . $this->request->get['filter_ingyenes'];
        }
				
		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;		
		$this->data['filter_group'] = $filter_group;
		$this->data['filter_order_status_id'] = $filter_order_status_id;
        $this->data['filter_korosztaly']        = $filter_korosztaly;
        $this->data['filter_nem']               = $filter_nem;
        $this->data['filter_ingyenes']          = $filter_ingyenes;
        $this->data['filter_iskolai_vegzettseg']               = $filter_iskolai_vegzettseg;

        $this->load->model('tool/ExportCSV');
        $this->model_tool_ExportCSV->setHeader(array(
            'date_start'        => $this->data['column_date_start'],
            'date_end'          => $this->data['column_date_end'],
            'orders'            => $this->data['column_orders'],
            'products'          => $this->data['column_products'],
            'tax'               => $this->data['column_tax'],
            'total'             => $this->data['column_total']
        ));
        if(!empty($results)) {
            $this->model_tool_ExportCSV->setDatas($results);
        } else {
            $this->model_tool_ExportCSV->setDatas(array());
        }
        $this->model_tool_ExportCSV->sendToOutput();
	}
}
?>