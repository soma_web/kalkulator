<?php
class ControllerReportSaleCategories extends Controller {
	public function index() {  
		$this->load->language('report/sale_categories');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_categories'])) {
			$filter_categories = $this->request->get['filter_categories'];
		} else {
			$filter_categories = '';
		}

        $url = '';
        if (isset($this->request->get['filter_categories'])) {
            $url .= '&filter_categories=' . $this->request->get['filter_categories'];
        }
        $this->data['filter_categories'] = $filter_categories;

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

   		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/sale_categories', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->load->model('report/sale');

        $queryFilter = array(
            'filter_categories'      => $filter_categories,
            'start'                  => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit'                  => $this->config->get('config_admin_limit')
        );

        $categoryData = $this->model_report_sale->getProductsByCategory();
        $ajanlat_kupon = $categoryData['ingyenes'];
        $ajanlat_voucher = $categoryData['fizetos'];
        $categoryOrders = $this->model_report_sale->getCategoryOrders($queryFilter);
        $categories = array_key_exists('categories', $categoryOrders) ? $categoryOrders['categories'] : array();
        $fizetosek = array_key_exists('fizetosek', $categoryOrders) ? $categoryOrders['fizetosek'] : array();
        $ingyenesek = array_key_exists('ingyenesek', $categoryOrders) ? $categoryOrders['ingyenesek'] : array();
        $total = array_key_exists('total', $categoryOrders) ? $categoryOrders['total'] : 0;

        $sorok = array();
        foreach($categories as $category_id=>$category) {
            $sor = $category;
            if(array_key_exists($category_id, $fizetosek)) {
                $sor['vasarolt_voucher'] = $fizetosek[$category_id];
            } else {
                $sor['vasarolt_voucher'] = 0;
            }

            if(array_key_exists($category_id, $ingyenesek)) {
                $sor['vasarolt_kupon'] = $ingyenesek[$category_id];
            } else {
                $sor['vasarolt_kupon'] = 0;
            }

            if(array_key_exists($category_id, $ajanlat_kupon)) {
                $sor['ajanlat_kupon'] = $ajanlat_kupon[$category_id];
            } else {
                $sor['ajanlat_kupon'] = 0;
            }

            if(array_key_exists($category_id, $ajanlat_voucher)) {
                $sor['ajanlat_voucher'] = $ajanlat_voucher[$category_id];
            } else {
                $sor['ajanlat_voucher'] = 0;
            }

            $sor['osszesen'] = $sor['ajanlat_voucher'] + $sor['ajanlat_kupon'];
            $sorok[] = $sor;
        }

        $sorted = $this->config->rendezes($sorok, 'osszesen', 'DESC');

        $this->data['sorok'] = $sorted;

		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');
		
		$this->data['column_categories'] = $this->language->get('column_categories');
		$this->data['column_ajanlat_kupon'] = $this->language->get('column_ajanlat_kupon');
    	$this->data['column_ajanlat_voucher'] = $this->language->get('column_ajanlat_voucher');
		$this->data['column_vasarolt_kupon'] = $this->language->get('column_vasarolt_kupon');
		$this->data['column_vasarolt_voucher'] = $this->language->get('column_vasarolt_voucher');

		$this->data['button_nyomtatas'] = $this->language->get('button_nyomtatas');
		$this->data['button_exportalas'] = $this->language->get('button_exportalas');
        $this->data['button_filter'] = $this->language->get('button_filter');

		$this->data['token'] = $this->session->data['token'];

		$pagination = new Pagination();
		$pagination->total = $total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('report/sale_categories', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
			
		$this->data['pagination'] = $pagination->render();

        $this->data['csv_be']  = $this->url->link('report/sale_categories_csvbe', 'token=' . $this->session->data['token']  .$url, 'SSL');


		$this->template = 'report/sale_categories.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
}
?>