<?php
class ControllerShippingItemProduct extends Controller {
	private $error = array(); 
	
	public function index() {  
		$this->load->language('shipping/item_product');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            if (!isset($this->request->post['item_product_gls_csomagpont'])){
                $this->request->post['item_product_gls_csomagpont'] = 0;
            } else {
                $this->request->post['item_product_gls_csomagpont'] = 1;
            }

			$this->model_setting_setting->editSetting('item_product', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');
									
			$this->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
		}

        $this->data['megjelenit_gls'] = $this->config->get('megjelenit_gls');

		$this->data['heading_title']        = $this->language->get('heading_title');

		$this->data['text_enabled']         = $this->language->get('text_enabled');
		$this->data['text_disabled']        = $this->language->get('text_disabled');
		$this->data['text_all_zones']       = $this->language->get('text_all_zones');
		$this->data['text_none']            = $this->language->get('text_none');
		$this->data['text_magas']           = $this->language->get('text_magas');
		$this->data['text_alacsony']        = $this->language->get('text_alacsony');
        $this->data['text_gls_csomagpont']  = $this->language->get('text_gls_csomagpont');

        $this->data['entry_title_heading']  = $this->language->get('entry_title_heading');
        $this->data['entry_magas_alacsony'] = $this->language->get('entry_magas_alacsony');
        $this->data['entry_title_text']     = $this->language->get('entry_title_text');
		
		$this->data['entry_tax_class']      = $this->language->get('entry_tax_class');
		$this->data['entry_geo_zone']       = $this->language->get('entry_geo_zone');
		$this->data['entry_status']         = $this->language->get('entry_status');
		$this->data['entry_sort_order']     = $this->language->get('entry_sort_order');
		$this->data['entry_mail'] = $this->language->get('entry_mail');

		$this->data['button_save']          = $this->language->get('button_save');
		$this->data['button_cancel']        = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
  		
		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('shipping/item_product', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('shipping/item_product', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['item_product_tax_class_id'])) {
			$this->data['item_product_tax_class_id'] = $this->request->post['item_product_tax_class_id'];
		} else {
			$this->data['item_product_tax_class_id'] = $this->config->get('item_product_tax_class_id');
		}

		if (isset($this->request->post['item_product_mail'])) {
			$this->data['item_product_mail'] = $this->request->post['item_product_mail'];
		} else {
			$this->data['item_product_mail'] = $this->config->get('item_product_mail');
		}

        if (isset($this->request->post['item_product_gls_csomagpont'])) {
            $this->data['item_product_gls_csomagpont'] = $this->request->post['item_product_gls_csomagpont'];
        } else {
            $this->data['item_product_gls_csomagpont'] = $this->config->get('item_product_gls_csomagpont');
        }
				
		if (isset($this->request->post['item_product_geo_zone_id'])) {
			$this->data['item_product_geo_zone_id'] = $this->request->post['item_product_geo_zone_id'];
		} else {
			$this->data['item_product_geo_zone_id'] = $this->config->get('item_product_geo_zone_id');
		}
		
		if (isset($this->request->post['item_product_status'])) {
			$this->data['item_product_status'] = $this->request->post['item_product_status'];
		} else {
			$this->data['item_product_status'] = $this->config->get('item_product_status');
		}
		
		if (isset($this->request->post['item_product_sort_order'])) {
			$this->data['item_product_sort_order'] = $this->request->post['item_product_sort_order'];
		} else {
			$this->data['item_product_sort_order'] = $this->config->get('item_product_sort_order');
		}

        if (isset($this->request->post['item_product_header'])) {
            $this->data['item_product_header'] = $this->request->post['item_product_header'];
        } else {
            $this->data['item_product_header'] = $this->config->get('item_product_header');
        }

        if (isset($this->request->post['item_product_text'])) {
            $this->data['item_product_text'] = $this->request->post['item_product_text'];
        } else {
            $this->data['item_product_text'] = $this->config->get('item_product_text');
        }

        if (isset($this->request->post['item_product_magas_alacsony'])) {
            $this->data['item_product_magas_alacsony'] = $this->request->post['item_product_magas_alacsony'];
        } else {
            $this->data['item_product_magas_alacsony'] = $this->config->get('item_product_magas_alacsony');
        }

        $this->load->model('localisation/language');
        $this->data['languages'] = $this->model_localisation_language->getLanguages();


        foreach ( $this->data['languages'] as $language) {
            if (isset($this->request->post['item_product_header_'.$language['language_id']])) {
                $this->data['item_product_header_'.$language['language_id']] = $this->request->post['item_product_header_'.$language['language_id']];
            } else {
                $this->data['item_product_header_'.$language['language_id']] = $this->config->get('item_product_header_'.$language['language_id']);
            }
            if (isset($this->request->post['item_product_text_'.$language['language_id']])) {
                $this->data['item_product_text_'.$language['language_id']] = $this->request->post['item_product_text_'.$language['language_id']];
            } else {
                $this->data['item_product_text_'.$language['language_id']] = $this->config->get('item_product_text_'.$language['language_id']);
            }
        }


        $this->load->model('localisation/tax_class');
		
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		
		$this->load->model('localisation/geo_zone');
		
		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();


        $this->load->model('setting/extension');

        $extensions = $this->model_setting_extension->getInstalled('payment');
        $no_module = $this->model_setting_extension->getNoModule();


        foreach ($extensions as $key => $value) {
            if (!file_exists(DIR_APPLICATION . 'controller/payment/' . $value . '.php')) {
                $this->model_setting_extension->uninstall('payment', $value);

                unset($extensions[$key]);
            } elseif (in_array($value, $no_module)){
                unset($extensions[$key]);
            } else {
                $this->load->language('payment/' . $value);

                if (isset($this->request->post['item_product_payment_'.$value])) {
                    $this->data['item_product_payment_'.$value] = $this->request->post['item_product_payment_'.$value];
                } elseif ( $this->config->get('item_product_payment_'.$value)) {
                    $this->data['item_product_payment_'.$value] = $this->config->get('item_product_payment_'.$value);
                } else {
                    $this->data['item_product_payment_'.$value] = 0;
                }

                $this->data['payment']['payment_' . $value] = array(
                    'name'      => $this->language->get('heading_title'),
                    'valasztva' => $this->data['item_product_payment_'.$value]
                );


            }
        }


        $extensions = $this->model_setting_extension->getInstalled('shipping');

        foreach ($extensions as $key => $value) {
            if (!file_exists(DIR_APPLICATION . 'controller/shipping/' . $value . '.php')) {
                $this->model_setting_extension->uninstall('shipping', $value);

                unset($extensions[$key]);
            } elseif (in_array($value, $no_module)){
                unset($extensions[$key]);
            } else {
                $this->load->language('shipping/' . $value);

                if (isset($this->request->post['item_product_shipping_'.$value])) {
                    $this->data['item_product_shipping_'.$value] = $this->request->post['item_product_shipping_'.$value];
                } elseif ( $this->config->get('item_product_shipping_'.$value)) {
                    $this->data['item_product_shipping_'.$value] = $this->config->get('item_product_shipping_'.$value);
                } else {
                    $this->data['item_product_shipping_'.$value] = 0;
                }

                $this->data['shipping']['shipping_' . $value] = array(
                    'name'      => $this->language->get('heading_title'),
                    'valasztva' => $this->data['item_product_shipping_'.$value]
                );


            }
        }

		$this->template = 'shipping/item_product.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/item_product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>