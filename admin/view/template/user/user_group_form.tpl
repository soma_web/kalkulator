<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/user-group.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
            <tr>
                <td><span class="required">*</span> <?php echo $entry_name; ?></td>
                <td><input type="text" name="name" value="<?php echo $name; ?>" />
                    <?php if ($error_name) { ?>
                        <span class="error"><?php echo $error_name; ?></span>
                    <?php  } ?>
                </td>
            </tr>

            <?php if ($felhasznalo_atiranyitas) { ?>
                <tr>
                    <td><?php echo $entry_url; ?></td>
                    <td><input type="text" name="url" value="<?php echo $url; ?>" /></td>
                </tr>
            <?php } ?>

            <tr>
                <td><?php echo $entry_menu; ?></td>
                <td>
                    <div style="display: inline-block">
                        <div id="visible" class="scrollbox">
                            <?php $class = 'odd'; ?>
                            <?php foreach ($permissions as $permission) { ?>
                                <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                <div class="<?php echo $class; ?>">
                                    <?php if (in_array($permission, $visible)) { ?>
                                        <input type="checkbox" name="permission[visible][]" value="<?php echo $permission; ?>" checked="checked" />
                                        <?php echo $permission; ?>
                                    <?php } else { ?>
                                        <input type="checkbox" name="permission[visible][]" value="<?php echo $permission; ?>" />
                                        <?php echo $permission; ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a>
                    </div>
                    <div style="display: inline-block; vertical-align: top">
                        <img src="view/image/nagyitas.jpg" class="nagyito" onclick="Meretez(this,'visible')">
                    </div>
                </td>
            </tr>

          <tr>
            <td><?php echo $entry_access; ?></td>
            <td>
                <div style="display: inline-block">
                    <div id="access" class="scrollbox">
                        <?php $class = 'odd'; ?>
                        <?php foreach ($permissions as $permission) { ?>
                            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                            <div class="<?php echo $class; ?>">
                                <?php if (in_array($permission, $access)) { ?>
                                    <input type="checkbox" name="permission[access][]" value="<?php echo $permission; ?>" checked="checked" />
                                    <?php echo $permission; ?>
                                <?php } else { ?>
                                    <input type="checkbox" name="permission[access][]" value="<?php echo $permission; ?>" />
                                    <?php echo $permission; ?>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a>
                </div>
                <div style="display: inline-block; vertical-align: top">
                    <img src="view/image/nagyitas.jpg" class="nagyito" onclick="Meretez(this,'access')">
                </div>
            </td>
          </tr>

          <tr>
            <td><?php echo $entry_modify; ?></td>
            <td>
                <div style="display: inline-block">
                    <div id="modify"  class="scrollbox">
                        <?php $class = 'odd'; ?>
                        <?php foreach ($permissions as $permission) { ?>
                            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                            <div class="<?php echo $class; ?>">
                                <?php if (in_array($permission, $modify)) { ?>
                                    <input type="checkbox" name="permission[modify][]" value="<?php echo $permission; ?>" checked="checked" />
                                    <?php echo $permission; ?>
                                <?php } else { ?>
                                    <input type="checkbox" name="permission[modify][]" value="<?php echo $permission; ?>" />
                                    <?php echo $permission; ?>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a>
                </div>
                <div style="display: inline-block; vertical-align: top">
                    <img src="view/image/nagyitas.jpg" class="nagyito"  onclick="Meretez(this,'modify')">
                </div>
            </td>
          </tr>
        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<script>
    function Meretez(para,para1) {
        if ( $("#"+para1).hasClass("nagyitott")) {
            $(".scrollbox").removeClass("nagyitott");
            $(para).attr("src","view/image/nagyitas.jpg");
            $('html,body').animate({scrollTop: $("#"+para1).offset().top - 10},'slow');
        } else {
            $(".scrollbox").removeClass("nagyitott");
            $("img.nagyito").attr("src","view/image/nagyitas.jpg");
            $(para).attr("src","view/image/kicsinyites.jpg");
            $("#"+para1).addClass("nagyitott");
            $('html,body').animate({scrollTop: $("#"+para1).offset().top - 10},'slow');
        }
    }
</script>