<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
<div class="heading">
    <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
</div>
<div class="content">

<?php if(!empty($confirmdata)) { ?>
    <h2><?php echo $entry_process_or_msg; ?></h2>
    <p>
        <?php echo $confirmdata.'<hr />'; ?>
    </p>
<?php } ?>

<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
<table class="form">
<tr>
    <td><!--<span class="required">*</span>--><?php echo $entry_email; ?></td>
    <td><input type="text" name="budapestbank_email" value="<?php echo $budapestbank_email; ?>" />
        <?php if ($error_email) { ?>
        <span class="error"><?php echo $error_email; ?></span>
        <?php } ?></td>
</tr>
<tr>
    <td><?php echo $entry_test; ?></td>
    <td><?php if ($budapestbank_test) { ?>
        <input type="radio" name="budapestbank_test" value="1" checked="checked" />
        <?php echo $text_yes; ?>
        <input type="radio" name="budapestbank_test" value="0" />
        <?php echo $text_no; ?>
        <?php } else { ?>
        <input type="radio" name="budapestbank_test" value="1" />
        <?php echo $text_yes; ?>
        <input type="radio" name="budapestbank_test" value="0" checked="checked" />
        <?php echo $text_no; ?>
        <?php } ?></td>
</tr>
<tr>
    <td><?php echo $entry_transaction; ?></td>
    <td><select name="budapestbank_transaction">
            <?php if ($budapestbank_transaction == 'sms') { ?>
            <option value="sms" selected="selected"><?php echo $text_sms; ?></option>
            <?php } else { ?>
            <option value="sms"><?php echo $text_sms; ?></option>
            <?php } ?>

            <?php if ($budapestbank_transaction == 'dms') { ?>
            <option value="dms" selected="selected"><?php echo $text_dms; ?></option>
            <?php } else { ?>
            <option value="dms"><?php echo $text_dms; ?></option>
            <?php } ?>
        </select></td>
</tr>
<tr>
    <td><?php echo $entry_debug; ?></td>
    <td><select name="budapestbank_debug">
            <?php if ($budapestbank_debug) { ?>
            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
            <option value="0"><?php echo $text_disabled; ?></option>
            <?php } else { ?>
            <option value="1"><?php echo $text_enabled; ?></option>
            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
            <?php } ?>
        </select></td>
</tr>

<!-- NEWLY ADDED -->

<tr>
    <td><?php echo $entry_deviza; ?></td>
    <td>
        <select name="budapestbank_deviza">
                <option value="978" <?php if($budapestbank_deviza=='978') { echo 'selected="selected"'; } ?> >EUR</option>
                <option value="840" <?php if($budapestbank_deviza=='840') { echo 'selected="selected"'; } ?> >USD</option>
                <option value="348" <?php if($budapestbank_deviza=='348') { echo 'selected="selected"'; } ?> >HUF</option>
                <option value="941" <?php if($budapestbank_deviza=='941') { echo 'selected="selected"'; } ?> >RSD</option>
                <option value="703" <?php if($budapestbank_deviza=='703') { echo 'selected="selected"'; } ?> >SKK</option>
                <option value="440" <?php if($budapestbank_deviza=='440') { echo 'selected="selected"'; } ?> >LTL</option>
                <option value="233" <?php if($budapestbank_deviza=='233') { echo 'selected="selected"'; } ?> >EEK</option>
                <option value="643" <?php if($budapestbank_deviza=='643') { echo 'selected="selected"'; } ?> >RUB</option>
                <option value="891" <?php if($budapestbank_deviza=='891') { echo 'selected="selected"'; } ?> >YUM</option>
        </select>
    </td>
</tr>


<tr>
    <td><?php echo $entry_cert_url; ?></td>
    <td><input type="text" name="budapestbank_cert_url" value="<?php echo $budapestbank_cert_url; ?>" /></td>
</tr>

<tr>
    <td><?php echo $entry_cert_pass; ?></td>
    <td><input type="text" name="budapestbank_cert_pass" value="<?php echo $budapestbank_cert_pass; ?>" /></td>
</tr>

<tr>
    <td><?php echo $entry_client_ip; ?></td>
    <td><input type="text" name="budapestbank_client_ip" value="<?php echo $budapestbank_client_ip; ?>" /></td>
</tr>

<tr>
    <td><?php echo $entry_client_language; ?></td>
    <td><input type="text" name="budapestbank_client_language" value="<?php echo $budapestbank_client_language; ?>" /></td>
</tr>


<!-- NEWLY ADDED -->

<tr>
    <td><?php echo $entry_total; ?></td>
    <td><input type="text" name="budapestbank_total" value="<?php echo $budapestbank_total; ?>" /></td>
</tr>
<tr>
    <td><?php echo $entry_canceled_reversal_status; ?></td>
    <td><select name="budapestbank_canceled_reversal_status_id">
            <?php foreach ($order_statuses as $order_status) { ?>
            <?php if ($order_status['order_status_id'] == $budapestbank_canceled_reversal_status_id) { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
            <?php } ?>
            <?php } ?>
        </select></td>
</tr>
<tr>
    <td><?php echo $entry_completed_status; ?></td>
    <td><select name="budapestbank_completed_status_id">
            <?php foreach ($order_statuses as $order_status) { ?>
            <?php if ($order_status['order_status_id'] == $budapestbank_completed_status_id) { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
            <?php } ?>
            <?php } ?>
        </select></td>
</tr>
<tr>
    <td><?php echo $entry_denied_status; ?></td>
    <td><select name="budapestbank_denied_status_id">
            <?php foreach ($order_statuses as $order_status) { ?>
            <?php if ($order_status['order_status_id'] == $budapestbank_denied_status_id) { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
            <?php } ?>
            <?php } ?>
        </select></td>
</tr>
<tr>
    <td><?php echo $entry_expired_status; ?></td>
    <td><select name="budapestbank_expired_status_id">
            <?php foreach ($order_statuses as $order_status) { ?>
            <?php if ($order_status['order_status_id'] == $budapestbank_expired_status_id) { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
            <?php } ?>
            <?php } ?>
        </select></td>
</tr>
<tr>
    <td><?php echo $entry_failed_status; ?></td>
    <td><select name="budapestbank_failed_status_id">
            <?php foreach ($order_statuses as $order_status) { ?>
            <?php if ($order_status['order_status_id'] == $budapestbank_failed_status_id) { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
            <?php } ?>
            <?php } ?>
        </select></td>
</tr>
<tr>
    <td><?php echo $entry_pending_status; ?></td>
    <td><select name="budapestbank_pending_status_id">
            <?php foreach ($order_statuses as $order_status) { ?>
            <?php if ($order_status['order_status_id'] == $budapestbank_pending_status_id) { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
            <?php } ?>
            <?php } ?>
        </select></td>
</tr>
<tr>
    <td><?php echo $entry_processed_status; ?></td>
    <td><select name="budapestbank_processed_status_id">
            <?php foreach ($order_statuses as $order_status) { ?>
            <?php if ($order_status['order_status_id'] == $budapestbank_processed_status_id) { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
            <?php } ?>
            <?php } ?>
        </select></td>
</tr>
<tr>
    <td><?php echo $entry_refunded_status; ?></td>
    <td><select name="budapestbank_refunded_status_id">
            <?php foreach ($order_statuses as $order_status) { ?>
            <?php if ($order_status['order_status_id'] == $budapestbank_refunded_status_id) { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
            <?php } ?>
            <?php } ?>
        </select></td>
</tr>
<tr>
    <td><?php echo $entry_reversed_status; ?></td>
    <td><select name="budapestbank_reversed_status_id">
            <?php foreach ($order_statuses as $order_status) { ?>
            <?php if ($order_status['order_status_id'] == $budapestbank_reversed_status_id) { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
            <?php } ?>
            <?php } ?>
        </select></td>
</tr>
<tr>
    <td><?php echo $entry_voided_status; ?></td>
    <td><select name="budapestbank_voided_status_id">
            <?php foreach ($order_statuses as $order_status) { ?>
            <?php if ($order_status['order_status_id'] == $budapestbank_voided_status_id) { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
            <?php } ?>
            <?php } ?>
        </select></td>
</tr>
<tr>
    <td><?php echo $entry_geo_zone; ?></td>
    <td><select name="budapestbank_geo_zone_id">
            <option value="0"><?php echo $text_all_zones; ?></option>
            <?php foreach ($geo_zones as $geo_zone) { ?>
            <?php if ($geo_zone['geo_zone_id'] == $budapestbank_geo_zone_id) { ?>
            <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
            <?php } ?>
            <?php } ?>
        </select></td>
</tr>
<tr>
    <td><?php echo $entry_status; ?></td>
    <td><select name="budapestbank_status">
            <?php if ($budapestbank_status) { ?>
            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
            <option value="0"><?php echo $text_disabled; ?></option>
            <?php } else { ?>
            <option value="1"><?php echo $text_enabled; ?></option>
            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
            <?php } ?>
        </select></td>
</tr>
<tr>
    <td><?php echo $entry_sort_order; ?></td>
    <td><input type="text" name="budapestbank_sort_order" value="<?php echo $budapestbank_sort_order; ?>" size="1" /></td>
</tr>
</table>
</form>

<!-- List Transactions -->

<h3>Transactions</h3>

<table border=0 >
    <tr bgcolor=\"#c8c8c8\">
        <td>id</td>
        <td>date</td>
        <td>trans_id</td>
        <td>amount</td>
        <td>currency</td>
        <td>ip</td>
        <td>description</td>
        <td>language</td>
        <td>RESULT</td>
        <td>RESULT CODE</td>
        <td>3D Secure</td>
        <td>Card nr.</td>
        <td>DMS</td>
        <td>Reverse</td>
        <td>Update result</td>
    </tr>

<?php
    foreach ( $bptrans as $row ) {

    if($row['result_code'] == '000'){
    $color = "bgcolor=\"#b0f0b0\"";

    }elseif($row['result_code'] == '400' OR $row['result'] == 'AUTOREVERSED' OR $row['result'] == 'REVERSED'){
    $color = "bgcolor=\"#B2D1F0\"";

    }elseif($row['result_code'] == '???' OR $row['result'] == 'CREATED' OR $row['result'] == 'TIMEOUT' OR $row['result'] == 'PENDING'){
    $color = "bgcolor=\"#f3f3f3\"";

    }else{
    $color = "bgcolor=\"#ffbaba\"";
    }


    if($row['result_code'] == '400'){
    $color2 = "bgcolor=\"#B2D1F0\"";

    }else{
    $color2 = "";
    }




    if($row['dms_ok'] == 'NO' AND $row['result_code'] == '000'){
    $makedmstrans = "            <td><a href='index.php?route=payment/budapestbank&action=confirm_dms&id=".$row['id'].$token."'>makeDMStrans</a></td>";

    }elseif($row['dms_ok'] == 'YES'){
    $makedmstrans = '            <td style="color: gray;">DMS done; amount='.$row['makeDMS_amount'].'</td>';
    }else{
    $makedmstrans = '            <td>---</td>';
    }



    if($row['result_code']=='000' AND $row['result'] =='OK'){
    $reverse = " <td><a href='index.php?route=payment/budapestbank&action=confirm_reverse&id=".$row['id'].$token."'>REVERSE</a></td>";

    }elseif($row['result']=='REVERSED' AND $row['reversal_amount'] == '0'){
    $reverse = '  <td style="color: gray;">Autoreversal</td>';

    }elseif($row['result_code']=='400' OR $row['result']=='REVERSED'){
    $reverse = '  <td style="color: gray;">Reversed; amount='.$row['reversal_amount'].'</td>';

    }else{
    $reverse = '  <td style="color: gray;">---</td>';
    }

    $update = " <td><a href='index.php?route=payment/budapestbank&action=update&id=". $row['id'] .$token."'>Update</a></td>";



        printf("
        <tr bgcolor=\"#f3f3f3\">
            <td>%s</td>
            <td>%s</td>
            <td><span title=\"%s\">%s</span></td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td %s><span title=\"%s\">%s</span></td>
            <td %s><span title=\"%s\">%s</span></td>
            <td><span title=\"%s\">%s</span></td>
            <td><span title=\"%s\">%s</span></td>


            %s
            %s
            %s
        </tr>
        ",
        $row['id'], $row['t_date'], $row['response'], $row['trans_id'], $row['amount'], $row['currency'], $row['client_ip_addr'], urldecode($row['description']), $row['language'], $color,  $row['response'], $row['result'], $color2, $row['response'], $row['result_code'], $row['response'], $row['result_3dsecure'], $row['response'], $row['card_number'], $makedmstrans, $reverse, $update );

    }
?>

    </table>


<!-- ERROR -->
<h3>Errors</h3>

    <table>
        <tr bgcolor=\"#c8c8c8\">
            <td>id</td>
            <td>Error time</td>
            <td>Action</td>
            <td>Response</td>
        </tr>

        <?php
            foreach ($bperror as $row) {
                printf("<tr bgcolor=\"#f3f3f3\"><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>", $row['id'], $row['error_time'], $row['action'], $row['response']);
            }
        ?>
    </table>

<h3>Batch</h3>

<table>
    <tr bgcolor=\"#c8c8c8\">
        <td>id</td>
        <td>close date</td>
        <td>RESULT</td>
        <td>RESULT CODE</td>
        <td>count_reversal</td>
        <td>count_transaction</td>
        <td>amount_reversal</td>
        <td>amount_transaction</td>
    </tr>

    <?php
    foreach ($bpbatch as $row) {
        printf("<tr bgcolor=\"#f3f3f3\"><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>", $row['id'], $row['close_date'], $row['result'], $row['result_code'], $row['count_reversal'], $row['count_transaction'], $row['amount_reversal'], $row['amount_transaction']);
    }
    ?>

    </table>

    <br><br><br>To manually close business day, press this -->  <a href='<?php echo $closeaction; ?>'>Close Business Day</a><br>

<!-- PATCH 1.01 -->

</div>
</div>
</div>
<?php echo $footer; ?>