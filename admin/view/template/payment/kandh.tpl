<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">    
    <div class="heading">
      <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general" class="page">
          <table class="form">
            <tr>
              <td><?php echo $entry_order_status; ?></td>
              <td><select name="kandh_order_status_id">
                  <?php foreach ($order_statuses as $order_status) { ?>
                  <?php if ($order_status['order_status_id'] == $kandh_order_status_id) { ?>
                  <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
            </tr>
            <tr>
              <td><?php echo $entry_order_failed_status; ?></td>
              <td>
              	<select name="kandh_failed_status">
                  <?php foreach ($order_statuses as $order_status) { ?>
                  <?php if ($order_status['order_status_id'] == $kandh_failed_status) { ?>
                  <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </td>
            </tr>
            <tr>
              <td><?php echo $entry_status; ?></td>
              <td>
              	<select name="kandh_status">
                  <?php if ($kandh_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select>
              </td>
            </tr>
            <tr>
              <td><?php echo $entry_shop_code; ?></td>
              <td><input type="text" name="kandh_shop_code" value="<?php echo $kandh_shop_code; ?>" size="10" /></td>
            </tr>
            
            <tr>
              <td><?php echo $entry_prod_key_file; ?> (<?php echo $kandh_prod_key_file; ?>)</td>
              <td><input type="file" name="kandh_prod_key_file" />
              <input type="hidden" name="kandh_tmp_prod_key_file" value="<?php echo $kandh_prod_key_file; ?>" />
              </td>
            </tr>
            
            <tr>
              <td><?php echo $entry_payment_language; ?></td>
              <td>
              	<select name="kandh_payment_language">
                  	<option <?php echo ($kandh_payment_language == 'HU') ? 'selected' : ''?> value="HU">Magyar</option>
                  	<option <?php echo ($kandh_payment_language == 'DE') ? 'selected' : ''?> value="DE">Német</option>
                  	<option <?php echo ($kandh_payment_language == 'ES') ? 'selected' : ''?> value="ES">Spanyol</option>
                  	<option <?php echo ($kandh_payment_language == 'EN') ? 'selected' : ''?> value="EN">Angol</option>
                  	<option <?php echo ($kandh_payment_language == 'FI') ? 'selected' : ''?> value="FI">Finn</option>
                  	<option <?php echo ($kandh_payment_language == 'FR') ? 'selected' : ''?> value="FR">Francia</option>
                  	<option <?php echo ($kandh_payment_language == 'IT') ? 'selected' : ''?> value="IT">Olasz</option>
                  	<option <?php echo ($kandh_payment_language == 'NL') ? 'selected' : ''?> value="NL">Holland</option>
                  	<option <?php echo ($kandh_payment_language == 'NO') ? 'selected' : ''?> value="NO">Norvég</option>
                  	<option <?php echo ($kandh_payment_language == 'PL') ? 'selected' : ''?> value="PL">Lengyel</option>
                  	<option <?php echo ($kandh_payment_language == 'PT') ? 'selected' : ''?> value="PT">Portugál</option>
                  	<option <?php echo ($kandh_payment_language == 'RO') ? 'selected' : ''?> value="RO">Román</option>
                  	<option <?php echo ($kandh_payment_language == 'SK') ? 'selected' : ''?> value="SK">Szlovák</option>
                  	<option <?php echo ($kandh_payment_language == 'SV') ? 'selected' : ''?> value="SV">Svéd</option>
                </select>
              </td>
            </tr>
            
            <tr>
              <td><?php echo $entry_payment_currency; ?></td>
              <td>
              	<select name="kandh_payment_currency">
                  	<option <?php echo ($kandh_payment_currency == 'HUF') ? 'selected' : ''?> value="HUF">Forint</option>
                  	<option <?php echo ($kandh_payment_currency == 'EUR') ? 'selected' : ''?> value="EUR">Euro</option>
                </select>
              </td>
            </tr>
        
            <tr>
              <td><?php echo $entry_test_mode; ?></td>
              <td>
              	<select name="kandh_is_test">
                  <?php if ($kandh_is_test) { ?>
                  <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                  <option value="0"><?php echo $text_no; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_yes; ?></option>
                  <option value="0" selected="selected"><?php echo $text_no; ?></option>
                  <?php } ?>
                </select>
              </td>
            </tr>
            
            <tr>
              <td><?php echo $entry_sort_order; ?></td>
              <td><input type="text" name="kandh_sort_order" value="<?php echo $kandh_sort_order; ?>" size="1" /></td>
            </tr>
          </table>
        </div>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?> 
