<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <div class="box">
    <div class="heading">
        <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
        <div class="nyomtat">
            <a onclick="window.print();" class="button"><?php echo $button_nyomtatas; ?></a>
            <a href='<?php echo $csv_be; ?>' class="button"><?php echo $button_exportalas; ?></a>
        </div>
    </div>
    <div class="content">

      <table class="list">
        <thead>
          <tr>
            <td class="left"><?php echo $column_uploaders; ?></td>
            <td class="center"><?php echo $column_uploaded; ?></td>
            <td class="center"><?php echo $column_selled; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php if ($sorok) { ?>
          <?php foreach ($sorok as $sor) { ?>
          <tr>
            <td class="left"><?php echo $sor['name']; ?></td>
            <td class="center"><?php echo $sor['feltoltott']; ?></td>
            <td class="center"><?php echo $sor['vasaroltak']; ?></td>
          </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>

<?php echo $footer; ?>