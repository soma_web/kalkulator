<?php  echo $header; ?>

<style>

    .block {
        /*margin-bottom: 10px;*/
    }

    .ujblock {
        margin: 10px 0;
    }

    .kiemel {
        background: #ddd;

    }

    .csoport_beallitasok > div {
        position: relative;
    }

    .mezo_tabla{
        display: inline-block;
        margin: 5px 10px;
    }

    img.mezo_tabla{
        position: relative;
        top: 10px;
    }

    .mezo_xml_zar {
        background-color: #eee;
        margin-bottom: 10px;
    }

    .tablak {
        position: absolute;
        top: 0;
        display: none;
        padding: 10px 20px;
        background-color: #ddd;
        min-width: 200px;
    }

    .tags_hatter {
        background-color: #d1d1d1;
    }
    #torles_valaszt {
        display: inline;
    }

    .tabla_tag_neve {
        margin-bottom: 13px;
        display: inline-block;
        font-weight: 100;
    }
    .tabla_tag_nyito{
        font-size: 15px;
        margin-left: 10px;
    }
    .group_by span {
        vertical-align: top;
    }
    .input_allando {
        background-color: beige;
        margin-left: 20px;
    }

    .select_tabla_div {
        position: relative;
    }
    .select_tabla_div select {
        background-color: beige;

    }

    .select_tabla_div > select {
        margin-left: 20px;
    }


    .select_mezo_div {
        position: relative;
        display: inline-block;
    }

    a.button_mezo {
        text-decoration: none;
        color: #FFF;
        display: inline-block;
        padding: 5px 15px 5px 15px;
        background: cornflowerblue;
        -webkit-border-radius: 10px 10px 10px 10px;
        -moz-border-radius: 10px 10px 10px 10px;
        -khtml-border-radius: 10px 10px 10px 10px;
        border-radius: 10px 10px 10px 10px;
    }

    .szuro_parameterek {
        display: block;
        margin-bottom: 10px;
        padding: 10px;
        background-color: beige;
        box-shadow: 2px 3px #999;
    }
    .szuro_parameterek span {
        display: inline-block;
        width: 50%;
    }
    .szuro_parameterek span,  .szuro_parameterek span b {
        vertical-align: top;
    }

</style>

<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/product.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons">
                <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
                <a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
            </div>
        </div>
       <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <div id="header_altalanos">
                    <a onclick="addCsoport();" class="button">Csoport hozzáadása</a>

                    <?php if (isset($xml_beallitasok) && $xml_beallitasok) { ?>
                        <?php $elemszam = 0?>
                        <?php foreach($xml_beallitasok as $key_aktualis=>$value) {?>
                            <div class="csoportok" id="csoport_<?php echo $key_aktualis?>" >

                                <div class="csoportok_nyito" style="padding: 5px 0 30px 20px;" onclick="csikiCsuki(this,<?php echo $key_aktualis;?>)">
                                    <a onclick="$('#csoport<?php echo "_".$key_aktualis;?>').remove();" class="button"><?php echo $button_remove; ?></a>
                                    <a style="text-decoration: none; margin-right: 40px; float: left">
                                        Csoport neve: <input type="text" name="xml_beallitasok[<?php echo $key_aktualis?>][export_name]" value="<?php echo $value['export_name'];?>" size="25">
                                    </a>
                                </div>

                                <div class="csoport_beallitasok">
                                    <div class="csoport_<?php echo $key_aktualis;?>">

                                        <?php if (isset($value['tarolas']) && $value['tarolas'] == 1) { ?>
                                            <?php $tarolas_display = "display: none"?>
                                            Futás után: <input type="radio" name="xml_beallitasok[<?php echo $key_aktualis?>][tarolas]" value="1" checked="checked" onclick="tarolasNyitZaro(this, <?php echo $key_aktualis?>)" >Letöltés
                                                    <input type="radio" name="xml_beallitasok[<?php echo $key_aktualis?>][tarolas]" value="2" onclick="tarolasNyitZaro(this,<?php echo $key_aktualis?>)">Tárolás
                                        <?php } else { ?>
                                            <?php $tarolas_display = "display: inline"?>
                                            Futás után: <input type="radio" name="xml_beallitasok[<?php echo $key_aktualis?>][tarolas]" value="1" onclick="tarolasNyitZaro(this,<?php echo $key_aktualis?>)" >Letöltés
                                            <input type="radio" name="xml_beallitasok[<?php echo $key_aktualis?>][tarolas]" value="2" checked="checked" onclick="tarolasNyitZaro(this,<?php echo $key_aktualis?>)">Tárolás
                                        <?php } ?>
                                        <span id="tarolas_helye_<?php echo $key_aktualis?>" style="padding-left: 50px; <?php echo $tarolas_display?>">
                                            Tárolás helye: <input  type="text" name="xml_beallitasok[<?php echo $key_aktualis?>][tarolas_helye]" value="<?php echo isset($value['tarolas_helye']) ? $value['tarolas_helye'] : ""?>">
                                        </span>


                                        <div class="nulladik_sor">
                                            <?php $nulladiksor = '';?>

                                            <?php if(isset($value['nulladik_sor']) ) { ?>
                                                <?php $nulladiksor = $value['nulladik_sor'];?>
                                            <?php } ?>
                                            Nulladik sor: <input type="text" name="xml_beallitasok[<?php echo $key_aktualis?>][nulladik_sor]" size="70" value="<?php echo $nulladiksor?>">
                                        </div>

                                        <div class="ujblock">
                                            <a onclick="addBlock('<?php echo $key_aktualis?>');" class="button top">Új XML tag</a>
                                            <?php if(isset($value['xml_block']))  { ?>
                                                <?php $count_nyit = 0?>
                                                <?php $count_zaro = 0;?>

                                                <?php foreach($value['xml_block'] as $xml_tag_key=>$xlm_tag_ek) { ?>
                                                    <?php if (substr($xml_tag_key,0,4) == 'nyit') { ?>
                                                        <?php $count_nyit++; ?>
                                                    <?php } elseif (substr($xml_tag_key,0,4) == 'zaro' ) { ?>
                                                        <?php $count_zaro++; ?>
                                                    <?php } ?>

                                                <?php  } ?>

                                                <?php if($count_nyit > $count_zaro) { ?>
                                                    <a id="close_block-<?php echo $key_aktualis?>" onclick="closeBlock('<?php echo $key_aktualis?>');" class="button top">Close XML tag</a>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>

                                        <?php if(isset($value['xml_block']))  { ?>

                                            <?php foreach($value['xml_block'] as $key_block=>$block) { ?>
                                                <?php $count = explode('-',$key_block); ?>
                                                <?php $sor = $count[1]?>
                                                <?php $count_px = ((count($count)-1)*20); ?>

                                                <?php if(substr($key_block,0,4) == "nyit") { ?>

                                                    <div id="<?php echo $key_block?>" class="block nyit" style="margin-left: <?php echo $count_px?>px" >
                                                        <input class="mezo_xml_nyit"  type="text" size="14" name="xml_beallitasok[<?php echo $key_aktualis?>][xml_block][<?php echo $key_block?>][nyito_tag]" style="margin-right: 13px;" value="<?php echo $block['nyito_tag']?>">
                                                        <img  class="beszur" style="margin-right: 5px; width: 14px"   src="view/image/insert.png" alt="" title="Új tag beszúrása" onclick="ujTagNyitoUtan(<?php echo $key_aktualis?>,'<?php echo $key_block?>')" >
                                                        <img  class="torol_sor" style="margin-right: 5px"   src="view/image/delete.png" alt= "" title="Aktuális sor törlése" onclick="sorTagTorol('<?php echo $key_block?>')" >
                                                        <img  class="torol" style="margin-right: 13px; width: 14px;"   src="view/image/all-remove.png" alt="" title="Teljes befoglaló struktúra törlése " onclick="tagTorol('<?php echo $key_block?>')" ><br>
                                                        <div class="tablak">
                                                            <span class="tabla_tag_neve">Nyitó tag neve:</span>
                                                            <?php $nyito_tag_neve = str_replace("<","",$block['nyito_tag'])?>
                                                            <?php $nyito_tag_neve = str_replace(">","",$nyito_tag_neve)?>
                                                            <?php $nyito_tag_neve = str_replace("</","",$nyito_tag_neve)?>
                                                            <?php $nyito_tag_neve = str_replace("&lt;","",$nyito_tag_neve)?>
                                                            <?php $nyito_tag_neve = str_replace("&gt;","",$nyito_tag_neve)?>

                                                            <span class="tabla_tag_nyito"><?php echo $nyito_tag_neve?></span>
                                                            <img  style="position: absolute; right: 6px; top: 8px;"   src="view/image/close.png" alt=""
                                                                        onclick="$(this).parent().slideUp(600); $('.block').removeClass('kiemel');" >

                                                            <?php $display = 'display: block'; ?>
                                                            <?php if( !isset($block['tabla']) ) { ?>
                                                                <?php $display = 'display: none'; ?>
                                                            <?php } ?>



                                                            <div class="szuro_parameterek" style="<?php echo $display?>">
                                                                <span>Szűrő paraméterek:</span><br><br>

                                                                <span style="font-weight: 100">Felsőbb szinten létrehozott <b>tábla->mező:</b></span>
                                                                <textarea class="tabla_szuro_kapcsolat" name="xml_beallitasok[<?php echo $key_aktualis?>][xml_block][<?php echo $key_block?>][tabla_szuro_kapcsolat]"><?php echo isset($block['tabla_szuro_kapcsolat']) ? $block['tabla_szuro_kapcsolat'] : ""?></textarea>
                                                                <br>

                                                                <span style="font-weight: 100">Külső paraméter <b>reláció</b>: </span>
                                                                <textarea class="kulso_szuro_kapcsolat" name="xml_beallitasok[<?php echo $key_aktualis?>][xml_block][<?php echo $key_block?>][kulso_szuro_kapcsolat]"><?php echo isset($block['kulso_szuro_kapcsolat']) ? $block['kulso_szuro_kapcsolat'] : ""?></textarea>
                                                                <br>
                                                            </div>





                                                            <table>
                                                                <?php if( isset($block['tabla']) ) { ?>
                                                                    <?php foreach($block['tabla'] as $table_key=>$value_tabla) { ?>

                                                                        <tr style="margin-bottom: 20px" class="add_<?php echo $key_aktualis?>_<?php echo $table_key?> tabla_nev">
                                                                            <td>Tábla:</td>
                                                                            <td>
                                                                                <select class="tabla_valaszt" name="xml_beallitasok[<?php echo $key_aktualis?>][xml_block][<?php echo $key_block?>][tabla][<?php echo $table_key?>][name]" onchange="tablaValaszt('<?php echo $key_aktualis?>','<?php echo $key_block?>','<?php echo $table_key?>')">
                                                                                    <option value=''>--- Válasszon ---</option>

                                                                                    <?php foreach($szerkezet as $szerkezet_key=>$szerkezet_tabla) {?>
                                                                                        <?php if($value_tabla['name'] == $szerkezet_key){ ?>
                                                                                            <option selected="selected" value="<?php echo $szerkezet_key?>"><?php echo str_replace(DB_PREFIX,"",$szerkezet_key)?></option>
                                                                                        <?php } else { ?>
                                                                                            <option value="<?php echo $szerkezet_key?>"><?php echo str_replace(DB_PREFIX,"",$szerkezet_key)?></option>
                                                                                        <?php } ?>
                                                                                    <?php }?>
                                                                                </select>
                                                                             </td>


                                                                            <td>
                                                                                <span class="top" style="display: inline-block; width: 65px;">Főtábla: </span>
                                                                                <?php $check = isset($value_tabla['fotabla']) ? 'checked="checked"': '' ?>
                                                                                <input class="fotabla" type="checkbox" <?php echo $check?>
                                                                                       name="xml_beallitasok[<?php echo $key_aktualis?>][xml_block][<?php echo $key_block?>][tabla][<?php echo $table_key?>][fotabla]" value="1"
                                                                                       onclick="checkEllenoriz('<?php echo $key_aktualis?>','<?php echo $key_block?>','<?php echo $table_key?>',this)"/>
                                                                            </td>

                                                                            <td>
                                                                                <img class="torol_img" src="view/image/delete.png" alt=""
                                                                                     onclick="$('#<?php echo $key_block?> .add_<?php echo $key_aktualis?>_<?php echo $table_key?>' ).remove();
                                                                                            tablaValaszt('<?php echo $key_aktualis?>','<?php echo $key_block?>','<?php echo $table_key?>');
                                                                                         return false; ">
                                                                                <br>
                                                                            </td>

                                                                            </tr>

                                                                        <tr class="add_<?php echo $key_aktualis?>_<?php echo $table_key?>">
                                                                            <td>Kapcsolat:</td>
                                                                            <td>
                                                                                <?php if(isset($value_tabla['fotabla'])) { ?>

                                                                                    <select disabled style="opacity: 0.4" class="kapcsolat_valaszt select_<?php echo $key_aktualis?>_<?php echo $key_block?>"
                                                                                            name="xml_beallitasok[<?php echo $key_aktualis?>][xml_block][<?php echo $key_block?>][tabla][<?php echo $table_key?>][kapcsolodas]" >
                                                                                        <option value=""  selected="selected">-- Válasszon --</option>
                                                                                        <option value="LEFT">LEFT JOIN</option>
                                                                                        <option value="RIGHT">RIGHT JOIN</option>
                                                                                        <option value="INNER">INNER JOIN</option>
                                                                                    </select>

                                                                                <?php } else { ?>

                                                                                    <select class="kapcsolat_valaszt select_<?php echo $key_aktualis?>_<?php echo $key_block?>"
                                                                                            id="select_<?php echo $key_aktualis?>_<?php echo $key_block?>_<?php echo $table_key?>"
                                                                                            name="xml_beallitasok[<?php echo $key_aktualis?>][xml_block][<?php echo $key_block?>][tabla][<?php echo $table_key?>][kapcsolodas]" >
                                                                                    <option value="">-- Válasszon --</option>
                                                                                    <?php if($value_tabla['kapcsolodas'] == "LEFT") { ?>
                                                                                        <option value="LEFT"  selected="selected">LEFT JOIN</option>
                                                                                        <option value="RIGHT">RIGHT JOIN</option>
                                                                                        <option value="INNER">INNER JOIN</option>

                                                                                    <?php } elseif($value_tabla['kapcsolodas'] == "RIGHT"){ ?>
                                                                                        <option value="LEFT">LEFT JOIN</option>
                                                                                        <option value="RIGHT"  selected="selected">RIGHT JOIN</option>
                                                                                        <option value="INNER">INNER JOIN</option>

                                                                                    <?php } elseif($value_tabla['kapcsolodas'] == "INNER"){ ?>
                                                                                        <option value="LEFT" >LEFT JOIN</option>
                                                                                        <option value="RIGHT">RIGHT JOIN</option>
                                                                                        <option value="INNER"  selected="selected">INNER JOIN</option>

                                                                                    <?php } else { ?>
                                                                                        <option value="LEFT">LEFT JOIN</option>
                                                                                        <option value="RIGHT">RIGHT JOIN</option>
                                                                                        <option value="INNER">INNER JOIN</option>
                                                                                    <?php } ?>
                                                                                    </select>
                                                                                <?php } ?>


                                                                            </td>
                                                                        </tr>


                                                                    <tr class="add_<?php echo $key_aktualis?>_<?php echo $table_key?>">
                                                                        <td>On kapcsolat:</td>
                                                                        <td>
                                                                            <textarea class="on_kapcsolat" name="xml_beallitasok[<?php echo $key_aktualis?>][xml_block][<?php echo $key_block?>][tabla][<?php echo $table_key?>][on_kapcsolat]"><?php echo $value_tabla['on_kapcsolat'];?></textarea>
                                                                        </td>

                                                                        <td>Feltétel:</td>
                                                                        <td>
                                                                            <textarea class="feltetel" name="xml_beallitasok[<?php echo $key_aktualis?>][xml_block][<?php echo $key_block?>][tabla][<?php echo $table_key?>][where_feltetel]"><?php echo $value_tabla['where_feltetel'];?></textarea>
                                                                        </td>
                                                                    </tr>

                                                                    <tr class="add_<?php echo $key_aktualis?>_<?php echo $table_key?> add_sorrend">

                                                                        <td>Sorrend:</td>
                                                                        <td>
                                                                            <img  class="beszur_sorrend" style="margin-right: 5px; width: 14px"   src="view/image/insert.png" alt="" title="Tábla sorrend hozzáadása" onclick="ujTablaSorrend('<?php echo $key_aktualis?>','<?php echo $key_block?>','<?php echo $table_key?>',this)" >

                                                                        </td>

                                                                    </tr>
                                                                    <?php if (isset($value_tabla['sorrend']))  { ?>
                                                                            <?php foreach($value_tabla['sorrend'] as $sorrend) { ?>
                                                                                <tr class="add_<?php echo $key_aktualis?>_<?php echo $table_key?> select_sorrend">
                                                                                    <td></td>
                                                                                    <td>
                                                                                        <select class="sorrend" name="xml_beallitasok[<?php echo $key_aktualis?>][xml_block][<?php echo $key_block?>][tabla][<?php echo $table_key?>][sorrend][]">
                                                                                            <option value=''>--- Válasszon ---</option>
                                                                                            <?php foreach ($value_tabla['mezok'] as $tablamezoi) { ?>
                                                                                                <?php $selected = $tablamezoi == $sorrend ? "selected='selected'" : "";?>
                                                                                                <option  value="<?php echo $tablamezoi?>" <?php echo $selected?>><?php echo str_replace($value_tabla['name'].'.','',$tablamezoi)?></option>
                                                                                            <?php } ?>
                                                                                        </select>
                                                                                        </td>
                                                                                    <td>
                                                                                    <img class="torol_img" src="view/image/delete.png" alt="" onclick="$(this).parent().parent().remove(); return false;"><br>
                                                                                    </td>
                                                                                </tr>
                                                                            <?php } ?>
                                                                    <?php } ?>

                                                                    <tr class="add_<?php echo $key_aktualis?>_<?php echo $table_key?> tabla_vege">
                                                                        <td><br><br></td>
                                                                     </tr>

                                                                     <?php } ?>
                                                                 <?php } ?>



                                                            </table>



                                                            <div class="group_by" style="<?php echo $display?>">
                                                                <span>Group By:</span>
                                                                <textarea class="group_by_area" name="xml_beallitasok[<?php echo $key_aktualis?>][xml_block][<?php echo $key_block?>][group_by]"><?php echo isset($block['group_by']) ? $block['group_by'] : ""?></textarea>
                                                            </div>
                                                            <a class="button button_uj_tabla" onclick="addTabla(<?php echo $key_aktualis?>,'<?php echo $key_block?>',1)">Új tábla</a>
                                                        </div>

                                                        <?php if(isset($block['mezok'])) { ?>
                                                            <?php foreach($block['mezok'] as $key_mezok=>$mezo) { ?>

                                                                <?php foreach($mezo as $key=>$sorok) { ?>
                                                                    <?php if($key == "tabla") { ?>
                                                                        <div class="select_tabla_div"  sor="<?php echo $key_mezok?>">
                                                                            <select class="select_tabla"  onchange="mezoTablaSelect('<?php echo $key_aktualis?>','<?php echo $key_block?>',this,'<?php echo $key_mezok?>')"
                                                                                name="xml_beallitasok[<?php echo $key_aktualis?>][xml_block][<?php echo $key_block?>][mezok][<?php echo $key_mezok?>][tabla][tabla]">
                                                                                <option value="">--- Válasszon táblát ---</option>

                                                                                <?php if(isset($block['tablak'])) { ?>
                                                                                    <?php foreach($block['tablak'] as $vonatkozo_tabla) { ?>
                                                                                        <?php $selected = '' ?>
                                                                                        <?php if($sorok['tabla'] == $vonatkozo_tabla) { $selected = 'selected="selected"'; } ?>
                                                                                        <option value="<?php echo $vonatkozo_tabla?>" <?php echo $selected; ?> ><?php echo str_replace(DB_PREFIX,"",$vonatkozo_tabla); ?></option>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            </select>


                                                                            <?php if ( isset($sorok['mezo']) &&  $sorok['mezo'] ) { ?>
                                                                                <div class="select_mezo_div">
                                                                                    <select class="select_mezo"
                                                                                            name="xml_beallitasok[<?php echo $key_aktualis?>][xml_block][<?php echo $key_block?>][mezok][<?php echo $key_mezok?>][tabla][mezo]">
                                                                                        <option value="">--- Válasszon mezőt ---</option>

                                                                                        <?php foreach($sorok['vonatkozo_mezok'] as $vonatkozo_mezo) { ?>
                                                                                            <?php $selected = '' ?>
                                                                                            <?php if($sorok['mezo'] == $vonatkozo_mezo) { $selected = 'selected="selected"'; } ?>
                                                                                            <option value="<?php echo $vonatkozo_mezo;?>"  <?php echo $selected; ?> ><?php echo $vonatkozo_mezo;?></option>
                                                                                        <?php } ?>

                                                                                    </select>

                                                                                    <?php $tizedes = isset($sorok['kerekites']) ? $sorok['kerekites'] : 0 ?>
                                                                                    <select class="select_kerekites" name="xml_beallitasok[<?php echo $key_aktualis?>][xml_block][<?php echo $key_block?>][mezok][<?php echo $key_mezok?>][tabla][kerekites]">
                                                                                        <option>--- Kerekítés ---</option>
                                                                                        <option value="1" <?php echo $tizedes == 1 ? "selected='selected'" : ""; ?> >Egészre</option>
                                                                                        <option value="2" <?php echo $tizedes == 2 ? "selected='selected'" : ""; ?> >1 tizedesre</option>
                                                                                        <option value="3" <?php echo $tizedes == 3 ? "selected='selected'" : ""; ?> >2 tizedesre</option>
                                                                                        <option value="4" <?php echo $tizedes == 4 ? "selected='selected'" : ""; ?> >3 tizedesre</option>
                                                                                    </select>

                                                                                    <img  style="margin-right: 13px" class="mezo_torol"  src="view/image/delete.png" alt="" onclick="$(this).parent().parent().remove();" ><br>
                                                                                </div>
                                                                            <?php } else { ?>
                                                                                <img  style="margin-right: 13px" class="mezo_torol"  src="view/image/delete.png" alt="" onclick="$(this).parent().remove();" >
                                                                            <?php } ?>

                                                                            <?php if(isset($block['tablak'])) { ?>
                                                                                <?php foreach($block['tablak'] as $hidden_tablak) { ?>
                                                                                    <input type="hidden" class="vonatkozo_tablak" name="xml_beallitasok[<?php echo $key_aktualis?>][xml_block][<?php echo $key_block?>][tablak][<?php echo $hidden_tablak?>]" value="<?php echo $hidden_tablak?>">
                                                                                <?php } ?>
                                                                            <?php } ?>


                                                                        </div>
                                                                    <?php } elseif($key='allando') { ?>
                                                                        <input type="text" class="input_allando" sor="<?php echo $key_mezok?>"
                                                                               name="xml_beallitasok[<?php echo $key_aktualis?>][xml_block][<?php echo $key_block?>][mezok][<?php echo $key_mezok;?>][allando]"
                                                                                value="<?php echo $sorok?>">
                                                                        <img  style="margin-right: 13px" class="mezo_torol"  src="view/image/delete.png" alt="" onclick="$(this).prev().remove(); $(this).next('br').remove(); $(this).remove()" ><br>

                                                                    <?php } ?>

                                                                <?php } ?>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </div>

                                                <?php } else { ?>
                                                    <div id='<?php echo $key_block?>' class="block zaro" style='margin-left: <?php echo $count_px?>px'>
                                                        <input class="mezo_xml_zar"  type="text" size="14" name="xml_beallitasok[<?php echo $key_aktualis?>][xml_block][<?php echo $key_block?>][zaro_tag]" style="margin-right: 13px; " value="<?php echo $block['zaro_tag']?>">
                                                        <img class="beszur"  style="margin-right: 13px; width: 15px" class="mezo_tabla"  src="view/image/insert.png" alt="" title="Tag hozzáfűzése" onclick="ujTagZaroUtan(<?php echo $key_aktualis?>,'<?php echo $key_block?>')" ><br>
                                                    </div>
                                                <?php }  ?>
                                            <?php } ?>

                                        <?php } ?>
                                    </div>
                                </div>


                            </div>
                        <?php } ?>
                    <?php }?>
                </div>
                <input type="hidden" name="nyitott_sor" value="" id="nyitott_sor">
                <input type="hidden" name="nyitott_block" value="" id="nyitott_block">
                <input type="hidden" name="nyitott_tabla" value="" id="nyitott_tabla">
                <input type="hidden" name="scroll_top" value="" id="scroll_top">


            </form>
    </div>
</div>


<script>

    $(document).ready(function(){
        tagAtnevez();
        tagAktualis();
        setTimeout(" $('.warning, .success').slideUp(500)",4000);

        <?php if ($nyitott_sor !== "") { ?>
            $('#csoport_<?php echo $nyitott_sor;?> .csoport_beallitasok').css('display','block');
            add_ujMezoAllandoGombok(<?php echo $nyitott_sor?>);

            <?php if (isset($nyitott_block) && $nyitott_block !== "") { ?>
                $('#csoport_<?php echo $nyitott_sor;?> #<?php echo $nyitott_block?>').addClass('kiemel');
                <?php if (isset($nyitott_tabla) && $nyitott_tabla !== "") { ?>
                    var szelesseg = $(".csoport_<?php echo $nyitott_sor;?>").width()+50;

                    $('#csoport_<?php echo $nyitott_sor;?> #<?php echo $nyitott_block?> .tablak').css('display','block');
                    $(".csoport_<?php echo $nyitott_sor;?> #<?php echo $nyitott_block?> .tablak").css("left",szelesseg+"px");
                <?php } ?>
            <?php } ?>
        <?php } ?>

        <?php if (isset($scroll_top) && $scroll_top !== "") { ?>
            $('html, body').animate({ scrollTop: <?php echo $scroll_top*-1?> }, "slow");
        <?php } ?>
    });

    var xml_row = <?php echo isset($key_aktualis) ? $key_aktualis+1 : 0?>;

    <?php if ($nyitott_sor !== "") { ?>
        $('#csoport_<?php echo $nyitott_sor;?> .csoport_beallitasok').css('display','block');
    <?php } ?>

    function addCsoport(){
        $('.csoport_beallitasok').slideUp(500);
        $('.csoportok_nyito').removeClass("active");
        var html = "";

        html += '<div class="csoportok" id="csoport_'+xml_row+'" >';

            html += '<div class="csoportok_nyito" style="padding: 5px 0 30px 20px;" onclick="csikiCsuki(this,'+xml_row+')">';
                html += '<a onclick="$(\'#csoport_'+xml_row+'\').remove();" class="button"><?php echo $button_remove; ?></a>';
                html += '<a style="text-decoration: none; margin-right: 40px; float: left">';
                    html +='Csoport neve: <input type="text" name="xml_beallitasok['+xml_row+'][export_name]" value="" size="25">';
                html +='</a>';
            html += '</div>';

            html += '<div class="csoport_beallitasok">';
                html += '<div class="csoport_'+xml_row+'">';

                    html += 'Futás után: <input type="radio" name="xml_beallitasok['+xml_row+'][tarolas]" value="1" checked="checked" onclick="tarolasNyitZaro(this, '+xml_row+')" >Letöltés';
                    html += '<input type="radio" name="xml_beallitasok['+xml_row+'][tarolas]" value="2" onclick="tarolasNyitZaro(this,'+xml_row+')">Tárolás';
                    html += '<span id="tarolas_helye_'+xml_row+'" style="padding-left: 50px; display: none">';
                        html += 'Tárolás helye: <input type="text" name="xml_beallitasok['+xml_row+'][tarolas_helye]" value="">';
                    html += '</span>';

                    html += '<div class="nulladik_sor">';
                        html += 'Nulladik sor: <input type="text" name="xml_beallitasok['+xml_row+'][nulladik_sor]" size="70">';
                    html += '</div>';

                    html += '<div class="ujblock">';
                        html += '<a onclick="addBlock(\''+xml_row+'\');" class="button top">Új XML tag</a>';
                    html += '</div>';

                html += '</div>';
            html += '</div>';
        html += '</div>';
        xml_row++;
        $("#header_altalanos").append(html);
        $('#csoport_'+(xml_row-1)+' .csoport_beallitasok').slideDown(500);
        $('#csoport_'+(xml_row-1)+' .csoportok_nyito').addClass("active");
    }



    function addBlock(sor){

        var tablasor = 0;
        var kovetkezo_id = 'nyit-'+sor+'-0';
        var utolso_elem = $(".csoport_"+sor+" .block").length;
        var block = '-0';
        count = 2;
        if (utolso_elem > 0) {

            var utolso_elem_id = $(".csoport_" + sor + " .block")[utolso_elem - 1].attributes.id.value;

            var utolso_elem_id_tomb = utolso_elem_id.split("-");
            var count = utolso_elem_id_tomb.length-1;

            var tomb_block = utolso_elem_id.split("-");
            tomb_block.splice(0);
            tomb_block.splice(1);
            block = tomb_block.join('-');


            if (utolso_elem_id_tomb[0] == "nyit") {
                kovetkezo_id = utolso_elem_id + "-0";
                count ++;
            }

            if (utolso_elem_id_tomb[0] == "zaro") {
                utolso_elem_id_tomb[0] = "nyit";
                utolso_elem_id_tomb[count] = (utolso_elem_id_tomb[count]*1)+1;
                kovetkezo_id = utolso_elem_id_tomb.join('-');
            }

        }

        var html = NyitoHtml(sor,kovetkezo_id,count);

        $('.csoport_'+sor).append(html);
        if ($('#close_block-'+sor).length < 1) {
            $('.csoport_' + sor + ' .ujblock').append('<a id="close_block-' + sor + '" onclick="closeBlock(' + sor + ');" class="button top">Close XML tag</a>');
        }
        tagAtnevez();
        tagAktualis();

        add_ujMezoAllandoGombok(sor);

    }

    function closeBlock(sor){

        var utolso_elem = $(".csoport_"+sor+" .block").length;

        if (utolso_elem > 0) {

            var utolso_elem_id = $(".csoport_" + sor + " .block")[utolso_elem - 1].attributes.id.value;

            var utolso_elem_id_tomb = utolso_elem_id.split("-");
            var count = utolso_elem_id_tomb.length-1;


            if (utolso_elem_id_tomb[0] == "nyit") {
                utolso_elem_id_tomb[0] = "zaro";
            } else if (utolso_elem_id_tomb[0] == "zaro") {
                utolso_elem_id_tomb.splice(count,1);
            }

            var kovetkezo_id = utolso_elem_id_tomb.join('-');
            count = --utolso_elem_id_tomb.length;

            var html = ZaroHtml(sor,kovetkezo_id,count);
            $('.csoport_' + sor).append(html);
            if (count < 3) {
                $('#close_block-'+sor).remove();
            }
            add_ujMezoAllandoGombok(sor);
            tagAktualis();

        }
    }


    function ujTagNyitoUtan(sor,aktualis_id){

        var tomb_id = aktualis_id.split("-");
        var count = tomb_id.length-1;
        tomb_id[0] = "nyit";
        count++;
        tomb_id[count] = 0;

        var kovetkezo_id = tomb_id.join('-');

        var html = NyitoHtml(sor,kovetkezo_id,count);
        $("#"+aktualis_id).after(html);


        var kovetkezo_zaro_id = kovetkezo_id.replace("nyit","zaro");


        var html = ZaroHtml(sor,kovetkezo_zaro_id,count);
        $("#"+kovetkezo_id).after(html);

        idAtkalkulal(kovetkezo_zaro_id);
        //add_ujMezoAllandoGombok(sor,aktualis_id);
    }

    function ujTagZaroUtan(sor,aktualis_id) {

        var tomb_id = aktualis_id.split("-");
        var count = tomb_id.length-1;
        tomb_id[0] = "nyit";
        tomb_id[count] = (tomb_id[count]*1)+1;

        var kovetkezo_id = tomb_id.join('-');

        var html = NyitoHtml(sor,kovetkezo_id,count);
        $("#"+aktualis_id).after(html);

        var kovetkezo_zaro_id = kovetkezo_id.replace("nyit","zaro");

        var html = ZaroHtml(sor,kovetkezo_zaro_id,count);
        $("#"+kovetkezo_id).after(html);

        idAtkalkulal(kovetkezo_zaro_id);
    }

    function idAtkalkulal(uj_id) {

        var sor = masodikElem(uj_id);
        var elem = new Elem('0_0');
        count  = 0;
        szamol = [];

        $('[id*=nyit-'+sor+'-], [id*=zaro-'+sor+'-]').each(function(element) {
            elem.setElem(this.id);
            if ( elem.getNyitoZaro() == "nyit") {
                szamol[elem.getCount()] = szamol[elem.getCount()] > -1 ? ++szamol[elem.getCount()] : 0;
            }
            if (count > elem.getCount()) {
                szamol.splice(count);
            }
            var uj_id = '';
            for(var i=0; szamol.length > i; i++) {
                if (i==0) {
                    uj_id = elem.getNyitoZaro();
                } else if (i==1){
                    uj_id += "-"+elem.getSor();
                } else if (!szamol[i] && szamol[i] != 0) {
                } else {
                    uj_id += '-'+szamol[i];
                }
            }
            $(this).attr('id',uj_id);

            var ujId = new Elem(uj_id);
            var darab = (ujId.getCount()*20);

            $(this).css("margin-left",darab);

            count = elem.getCount();
            if (elem.getNyitoZaro() == "nyit" ) {
                sor = elem.getSor();
                $(this).find('.torol_sor').attr('onclick','sorTagTorol("'+uj_id+'")');
                $(this).find('.torol').attr('onclick','tagTorol("'+uj_id+'")');
                $(this).find('.beszur').attr('onclick','ujTagNyitoUtan("'+sor+'","'+uj_id+'")');

                $(this).find('.mezo_mezo').attr('onclick','addOszlopMezo("'+sor+'","'+uj_id+'")');
                $(this).find('.mezo_allando').attr('onclick','addOszlopAllando("'+sor+'","'+uj_id+'")');
                $(this).find('.mezo_tabla').attr('onclick','addTabla("'+sor+'","'+uj_id+'")');
                $(this).find('.button_uj_tabla').attr('onclick','addTabla("'+sor+'","'+uj_id+'",1)');


                $(this).find('.mezo_xml_nyit').attr('name','xml_beallitasok['+sor+'][xml_block]['+uj_id+'][nyito_tag])');
                $(this).find('.group_by_area').attr('name','xml_beallitasok['+sor+'][xml_block]['+uj_id+'][group_by])');
                $(this).find('.tabla_szuro_kapcsolat').attr('name','xml_beallitasok['+sor+'][xml_block]['+uj_id+'][tabla_szuro_kapcsolat])');
                $(this).find('.kulso_szuro_kapcsolat').attr('name','xml_beallitasok['+sor+'][xml_block]['+uj_id+'][kulso_szuro_kapcsolat])');

                regi_id = elem.getNyitoId();

                $(this).find('.tabla_valaszt, .fotabla, .torol_img, .kapcsolat_valaszt, .on_kapcsolat, .feltetel, .sorrend, .input_allando, .select_tabla, .select_mezo, .vonatkozo_tablak, .select_kerekites').each(function(){

                    if (this.className == "tabla_valaszt") {
                        var name = this.name.replace(regi_id,uj_id);
                        $(this).attr("name",name);

                        var onchange_attr = $(this).attr('onchange').replace(regi_id,uj_id);
                        $(this).attr("onchange",onchange_attr);

                    } else if (this.className == "fotabla"){
                        var name = this.name.replace(regi_id,uj_id);
                        $(this).attr("name",name);

                        var onclick_attr = $(this).attr('onclick').replace(regi_id,uj_id);
                        $(this).attr("onclick",onclick_attr);

                    } else if (this.className == "torol_img"){
                        var img_onclick = $(this).attr("onclick").replace(regi_id,uj_id);
                        $(this).attr("onclick",img_onclick);

                    } else if (this.classList[0] == "kapcsolat_valaszt"){
                        var name = this.name.replace(regi_id,uj_id);
                        $(this).attr("name",name);

                        var class_ = this.className.replace(regi_id,uj_id);
                        $(this).attr("class",class_);


                   /* } else if (this.className == "on_kapcsolat"){
                        var name = this.name.replace(regi_id,uj_id);
                        $(this).attr("name",name);

                    } else if (this.className == "feltetel"){
                        var name = this.name.replace(regi_id,uj_id);
                        $(this).attr("name",name);*/

                    /*} else if (this.className == "sorrend"){
                        var name = this.name.replace(regi_id,uj_id);
                        $(this).attr("name",name);*/



                    /*} else if (this.className == "input_allando"){
                        var name = this.name.replace(regi_id,uj_id);
                        $(this).attr("name",name);*/

                    } else if (this.className == "select_tabla"){
                        var name = this.name.replace(regi_id,uj_id);
                        var onchange_attr = $(this).attr('onchange').replace(regi_id,uj_id);

                        $(this).attr("name",name);
                        $(this).attr("onchange",onchange_attr);

                    } else if (this.className == "select_mezo" || this.className == "vonatkozo_tablak" || this.className == "select_kerekites"
                        || this.className == "input_allando" || this.className == "sorrend" || this.className == "feltetel" || this.className == "on_kapcsolat" ) {
                        var name = this.name.replace(regi_id,uj_id);
                        $(this).attr("name",name);

                    /*} else if (this.className == "vonatkozo_tablak"){
                        var name = this.name.replace(regi_id,uj_id);
                        $(this).attr("name",name);

                    } else if (this.className == "select_kerekites"){
                        var name = this.name.replace(regi_id,uj_id);
                        $(this).attr("name",name);*/
                    }

                });
            } else {
                $(this).find('.beszur').attr('onclick','ujTagZaroUtan("'+sor+'","'+uj_id+'")');
                $(this).find('.mezo_xml_zar').attr('name','xml_beallitasok['+sor+'][xml_block]['+uj_id+'][zaro_tag])');
            }
        });
        tagAtnevez();
        tagAktualis();

        add_ujMezoAllandoGombok(sor);
    }

    function NyitoHtml(sor,kovetkezo_id,count){

        var html = "";
        html += '<div id="'+kovetkezo_id+'" class="block nyit" style="margin-left: '+count*20+'px">';
            html += '<input class="mezo_xml_nyit"  type="text" size="14" name="xml_beallitasok['+sor+'][xml_block]['+kovetkezo_id+'][nyito_tag]" style="margin-right: 13px;" value="<nyito>">';
            html += '<img  class="beszur" style="margin-right: 5px; width: 14px"   src="view/image/insert.png" alt="" title="Új tag beszúrása" onclick="ujTagNyitoUtan(\''+sor+'\',\''+kovetkezo_id+'\')" >';
            html += '<img  class="torol_sor" style="margin-right: 5px"   src="view/image/delete.png" alt= "" title="Aktuális sor törlése" onclick="sorTagTorol(\''+kovetkezo_id+'\')" >';
            html += '<img  class="torol" style="margin-right: 13px; width: 14px;"   src="view/image/all-remove.png" alt="" title="Teljes befoglaló struktúra törlése " onclick="tagTorol(\''+kovetkezo_id+'\')" ><br>';
            html += '<div class="tablak">';

                html += '<span class="tabla_tag_neve">Nyitó tag neve:</span>';
                html += '<span class="tabla_tag_nyito">nyito</span>';

                html += '<img  style="position: absolute; right: 6px; top: 8px;"   src="view/image/close.png" alt=""';
                html += ' onclick="$(this).parent().slideUp(600); $(\'.block\').removeClass(\'kiemel\'); " >';

                html += '<div class="szuro_parameterek" style="none">';
                    html += '<span>Szűrő paraméterek:</span><br><br>';

                    html += '<span style="font-weight: 100">Felsőbb szinten létrehozott <b>tábla->mező:</b></span>';
                    html += '<textarea class="tabla_szuro_kapcsolat" name="xml_beallitasok['+sor+'][xml_block]['+kovetkezo_id+'][tabla_szuro_kapcsolat]"></textarea>';
                    html += '<br>';

                    html += '<span style="font-weight: 100">Külső paraméter <b>reláció</b>: </span>';
                    html += '<textarea class="kulso_szuro_kapcsolat" name="xml_beallitasok['+sor+'][xml_block]['+kovetkezo_id+'][kulso_szuro_kapcsolat]"></textarea>';
                    html += '<br>';
                html += '</div>';

                html += '<table>';
                html += '</table>';
                html += '<div class="group_by" style="display: none">';
                    html += '<span>Group By:</span>';
                    html += '<textarea class="group_by_area" name="xml_beallitasok['+sor+'][xml_block]['+kovetkezo_id+'][group_by]"></textarea>';
                html +='</div>';
                html += '<a class="button button_uj_tabla" onclick="addTabla('+sor+',\''+kovetkezo_id+'\',1);">Új tábla</a>';

            html += '</div>';
        html += '</div>';

        return html;
    }

    function addOnclickHtmOszlopl(sor,sor_id){
        var html = "";
        html += '<a  onclick="addOszlopMezo('+sor+',\''+sor_id+'\');" class="button_mezo mezo_mezo"  style="margin-right: 13px">Új mező</a>';
        html += '<a  onclick="addOszlopAllando('+sor+',\''+sor_id+'\');" class="button_mezo mezo_allando"  style="margin-right: 13px">Új állando</a>';

        return html;
    }

    function addOnclickHtmTabla(sor,kovetkezo_id){

        var html = "";
        if ($('#'+kovetkezo_id+' .tabla_valaszt').length > 0 ) {
            html += '<a  onclick="addTabla('+sor+',\''+kovetkezo_id+'\');" class="button mezo_tabla"  style="margin-right: 13px">Tábla szerkesztése</a>';
        } else {
            html += '<a  onclick="addTabla('+sor+',\''+kovetkezo_id+'\');" class="button mezo_tabla"  style="margin-right: 13px">Új tábla</a>';
        }

        return html;
    }

    function ZaroHtml(sor,kovetkezo_id,count) {

        var html = '';
        html += '<div id='+kovetkezo_id+' class="block zaro" style="margin-left: '+(count)*20+'px">';
            html += '<input class="mezo_xml_zar"  type="text" size="14" name="xml_beallitasok['+sor+'][xml_block]['+kovetkezo_id+'][zaro_tag]" style="margin-right: 13px;" value="<zaro>">';
            html += '<img class="beszur"  style="margin-right: 13px; width: 15px" class="mezo_tabla"  src="view/image/insert.png" alt="" title="Tag hozzáfűzése" onclick="ujTagZaroUtan('+sor+',\''+kovetkezo_id+'\')" ><br>';
        html += '</div>';

        return html;
    }

    function tagTorol(sor_id){

        var elem = new Elem(sor_id);

        $('.block').removeClass("tags_hatter");
        $('[id*='+sor_id+']').addClass("tags_hatter");
        $('[id*='+elem.getZaroId()+']').addClass("tags_hatter");

        if (confirm('A teljes befoglaló struktúra törölve lesz! Folytatja?')) {
            $("[id*="+sor_id+"]").remove();
            sor_id = sor_id.replace("nyit","zaro");
            $("[id*="+sor_id+"]").remove();

            var tomb = sor_id.split("-");
            var last_key = tomb.length-1;
            var sor = tomb[1];

            tomb[last_key] = tomb[last_key]-1;

            var elozo_id = tomb.join('-');

            idAtkalkulal(elozo_id);

            add_ujMezoAllandoGombok(sor);
        }
        $('.block').removeClass("tags_hatter");

    }

    function sorTagTorol(sor_id){

        var elem = new Elem(sor_id);

        $('.block').removeClass("tags_hatter");
        $("#"+sor_id).addClass("tags_hatter");
        $("#"+elem.getZaroId()).addClass("tags_hatter");

        if (confirm('Csak az aktuális nyitó és záró sor lesz töröve! Folytatja?')) {

            var elem = new Elem(sor_id);

            $('#'+elem.getNyitoId()).remove();
            $('#'+elem.getZaroId()).remove();
            idAtkalkulal(sor_id);
            add_ujMezoAllandoGombok(elem.getSor());
        }
        $('.block').removeClass("tags_hatter");
        //idAtkalkulal(sor_id);

    }


    function mezoTorol(sor,sor_id){

        $("#"+sor_id+" .mezo_torol").next("br").remove();
        $("#"+sor_id+" .input_allando, #"+sor_id+" .mezo_torol").remove();

        add_ujMezoAllandoGombok(sor);
    }

    function Elem(sor_id){
        var tomb = sor_id.split("-");

        this.nyito_zaro = tomb[0];
        this.sor = tomb[1];
        this.count = tomb.length;
        this.zaro_id = sor_id.replace("nyit","zaro");
        this.nyito_id = sor_id.replace("zaro","nyit");
    }

    Elem.prototype = {
        nyito_zaro : null,
        sor: 0,
        count: 0,
        zaro_id: 0,
        nyito_id: 0,
        getNyitoZaro : function() {
            return this.nyito_zaro;
        },
        getSor: function() {
            return this.sor;
        },
        getCount: function() {
            return this.count-1;
        },
        getZaroId: function() {
            return this.zaro_id;
        },

        getNyitoId: function() {
            return this.nyito_id;
        },

        setElem: function(sor_id) {
            var tomb = sor_id.split("-");

            this.nyito_zaro = tomb[0];
            this.sor = tomb[1];
            this.count = tomb.length;
            this.zaro_id = sor_id.replace("nyit","zaro");
            this.nyito_id = sor_id.replace("zaro","nyit");
        }
    }


    function elsoElem(sor_id) {
        var tomb = sor_id.split("-");
        return tomb[0];
    }


    function masodikElem(sor_id) {
        var tomb = sor_id.split("-");
        return tomb[1];
    }

    function add_ujMezoAllandoGombok(sor) {

        $(".mezo_mezo, .mezo_allando, .mezo_tabla").remove();

        $('[id*=nyit-'+sor+'-], [id*=zaro-'+sor+'-]').each(function(element) {

            if (elsoElem(this.id) == "nyit") {
                if ( $(this).next().length > 0) {
                    if ( elsoElem($(this).next().attr("id")) == "zaro") {
                        if ( $(this).find('.input_allando').length == 0 || true) {
                            var html = addOnclickHtmOszlopl(masodikElem(this.id),this.id);
                            $("#"+this.id+" .mezo_xml_nyit").after(html);
                        }
                    }  else {
                        $(this).find(".select_tabla_div").remove();
                        $(this).find(".mezo_torol").next("br").remove();
                        $(this).find(".input_allando").remove();
                        $(this).find(".mezo_torol").remove();
                        var html = addOnclickHtmTabla(masodikElem(this.id),this.id);
                        $("#"+this.id+" .mezo_xml_nyit").after(html);
                    }
                }
            }
        });
    }

    function tagAtnevez() {

        $(".mezo_xml_nyit, .mezo_xml_zar").change(function() {
            var zaro_id = $(this).parent().attr("id");
            var tomb_ihez = zaro_id.split("-");

            var tagname = this.value;
            tagname = tagname.replace("</","");
            tagname = tagname.replace(">","");
            tagname = tagname.replace("<","");
            tagname = tagname.trim();


            $("#"+zaro_id+" .tabla_tag_nyito").html(tagname);

            if (tomb_ihez[0] == "nyit") {
                var tag = "<"+tagname+">";
                $("#"+zaro_id+" .mezo_xml_nyit").attr("value",tag);

                zaro_id = zaro_id.replace("nyit","zaro");
                var zaro_tag = tagname.split(" ");
                tag = "</"+zaro_tag[0]+">";
                $("#"+zaro_id+" .mezo_xml_zar").attr("value",tag);

            } else if (tomb_ihez[0] == "zaro") {
                debugger;
                var zaro_tag = tagname.split(" ");
                var tag = "</"+zaro_tag[0]+">";
                $("#"+zaro_id+" .mezo_xml_zar").attr("value",tag);

                zaro_id = zaro_id.replace("zaro","nyit");
                var eredeti_nyito   = $("#"+zaro_id+" .mezo_xml_nyit").val();
                eredeti_nyito       = eredeti_nyito.replace("</","");
                eredeti_nyito       = eredeti_nyito.replace(">","");
                eredeti_nyito       = eredeti_nyito.replace("<","");
                eredeti_nyito       = eredeti_nyito.trim();
                var eredeti_nyito_tomb = eredeti_nyito.split(" ");
                eredeti_nyito_tomb[0] = zaro_tag[0];
                tag = eredeti_nyito_tomb.join(" ");

                tag = "<"+tag+">";
                $("#"+zaro_id+" .mezo_xml_nyit").attr("value",tag);
            }
        });
    }

    function tagAktualis() {

        $(".beszur, .button, .torol, .tablak").bind("click",function() {
            $('.block').removeClass("tags_hatter")

        });

        $(".mezo_xml_nyit, .mezo_xml_zar").bind("click",function() {
            var block_id = $(this).parent().attr("id");

            var elem = new Elem(block_id);
            $('.block').removeClass("tags_hatter")
            $('[id*="'+elem.getNyitoId()+'"], [id*="'+elem.getZaroId()+'"]').addClass("tags_hatter");
        });
    }


    function addTabla(sor,sor_id,ujtabla){

      /*  if (typeof(bezarhat) == "undefined" ){
            bezarhat="false";
        }*/

        if ($(".csoport_"+sor+" #"+sor_id+ " .tablak").css('display') == 'block' && !ujtabla) {
            $(".csoport_"+sor+" #"+sor_id+ " .tablak").slideUp('slow');
            $('.block').removeClass('kiemel');
            return false;
        }


        var tablasor = $(".csoport_"+sor+" #"+sor_id+" .tabla_nev").length;


        var utolso_tablasor;
        var maximum_valtozo = "";
        if (tablasor > 0) {
            for(var i=0; tablasor > i; i++) {
                var a = $(".csoport_"+sor+" #"+sor_id+" .tabla_nev")[i].attributes.class.value;
                if (a > maximum_valtozo) {
                    maximum_valtozo = a;
                }
                utolso_tablasor = a.split(" ")[0];
            }

            var levagott_valtozo = maximum_valtozo.substr(0,maximum_valtozo.indexOf(" "));
            levagott_valtozo = levagott_valtozo.split("_");
            tablasor = levagott_valtozo[2]*1+1;
        }


        var szelesseg = $(".csoport_"+sor).width()+50;

        var html = '';

        html += '<tr style="margin-bottom: 20px" class="add_'+sor+'_'+tablasor+' tabla_nev">';
            html += '<td>Tábla:</td>';
            html += '<td>';
                html += '<select class="tabla_valaszt" name="xml_beallitasok['+sor+'][xml_block]['+sor_id+'][tabla]['+tablasor+'][name]" onchange="tablaValaszt('+sor+',\''+sor_id+'\',\''+tablasor+'\')">';
                html += '<option value="">--- Válasszon ---</option>';

                <?php foreach($szerkezet as $szerkezet_key=>$szerkezet_tabla) {?>
                    html += '<option value="<?php echo $szerkezet_key?>"><?php echo str_replace(DB_PREFIX,"",$szerkezet_key)?></option>';
                <?php }?>
                html += '</select>';
            html += '</td>';


            html += '<td>';
                html += '<span class="top" style="display: inline-block; width: 65px;">Főtábla: </span>';
                html += '<input class="fotabla" type="checkbox" name="xml_beallitasok['+sor+'][xml_block]['+sor_id+'][tabla]['+tablasor+'][fotabla]" value="1" onclick="checkEllenoriz('+sor+',\''+sor_id+'\',\''+tablasor+'\',this)"/>';
            html += '</td>';

            html += '<td>';
                html += '<img class="torol_img" src="view/image/delete.png" alt="" onclick="$(\'#'+sor_id+' .add_'+sor+'_'+tablasor+'\' ).remove(); tablaValaszt('+sor+',\''+sor_id+'\',\''+tablasor+'\'); return false;"><br>';
            html += '</td>';

        html += '</tr>';

        html += '<tr class="add_'+sor+'_'+tablasor+'">';
            html += '<td>Kapcsolat:</td>';
            html += '<td>';
                html += '<select name="xml_beallitasok['+sor+'][xml_block]['+sor_id+'][tabla]['+tablasor+'][kapcsolodas]"';
                                html += ' class="kapcsolat_valaszt select_'+sor+'_'+sor_id+'">';
                    html += '<option value=""  selected="selected">-- Válasszon --</option>';
                    html += '<option value="LEFT">LEFT JOIN</option>';
                    html += '<option value="RIGHT">RIGHT JOIN</option>';
                    html += '<option value="INNER">INNER JOIN</option>';
                html += '</select>';
            html += '</td>';
        html += '</tr>';


        html += '<tr class="add_'+sor+'_'+tablasor+'">';
            html += '<td>On kapcsolat:</td>';
            html += '<td>';
                html += '<textarea class="on_kapcsolat" name="xml_beallitasok['+sor+'][xml_block]['+sor_id+'][tabla]['+tablasor+'][on_kapcsolat]"></textarea>';
            html += '</td>';

            html += '<td>Feltétel:</td>';
            html += '<td>';
                html += '<textarea class="feltetel" name="xml_beallitasok['+sor+'][xml_block]['+sor_id+'][tabla]['+tablasor+'][where_feltetel]"></textarea>';
            html += '</td>';
        html += '</tr>';

        html += '<tr class="add_'+sor+'_'+tablasor+' add_sorrend">';

            html += '<td>Sorrend:</td>';
            html += '<td>';
                html += '<img  class="beszur_sorrend" style="margin-right: 5px; width: 14px"   src="view/image/insert.png" alt="" title="Tábla sorrend hozzáadása" onclick="ujTablaSorrend(\''+sor+'\',\''+sor_id+'\',\''+tablasor+'\',this)" >';
            html += '</td>';

        html += '</tr>';

        html += '<tr class="add_'+sor+'_'+tablasor+' tabla_vege">';
            html += '<td><br><br></td>';
        html += '</tr>';

        $(".block").removeClass("kiemel");
        $(".csoport_"+sor+" #"+sor_id).addClass('kiemel');
        $(".csoport_"+sor+" #"+sor_id+ " .tablak").css("left",szelesseg+"px");


        if ( ujtabla ) {
            $(".csoport_"+sor+" #"+sor_id+" .tablak table").append(html);
        }

        if ($(".csoport_"+sor+" #"+sor_id+ " .tablak .feltetel").length > 0) {
            $(".csoport_"+sor+" #"+sor_id+ " .tablak .group_by").css('display','block');
            $(".csoport_"+sor+" #"+sor_id+ " .tablak .szuro_parameterek").css('display','block');
        } else {
            $(".csoport_"+sor+" #"+sor_id+ " .tablak .group_by").css('display','none');
            $(".csoport_"+sor+" #"+sor_id+ " .tablak .szuro_parameterek").css('display','none');
        }

        $(".block .tablak").css("display","none");
        $(".csoport_"+sor+" #"+sor_id+ " .tablak").slideDown('slow');

        var position = (sor*45+200);
        $('html, body').animate({ scrollTop: position }, "slow");


    }

    function ujTablaSorrend(sor,sor_id,tablasor,para) {
        var tabla_nev = $(".csoport_"+sor+" #"+sor_id+ " .tablak .add_"+sor+"_"+tablasor+".tabla_nev select").val();
        $.ajax({
            url: 'index.php?route=design/megjelenito_xml/mezoLista&token=<?php echo $token; ?>',
            type: "post",
            data: 'tabla_nev=' + tabla_nev,
            dataType: 'json',

            success: function(json) {
                if (json) {
                    var html = '';
                    html += '<tr class="add_'+sor+'_'+tablasor+' select_sorrend"> ';
                        html += '<td></td>';
                        html += '<td>';
                            html += '<select class="sorrend" name="xml_beallitasok['+sor+'][xml_block]['+sor_id+'][tabla]['+tablasor+'][sorrend][]">';
                                html += '<option value="">--- Válasszon ---</option>';
                                for(var i=0; json.length > i; i++) {
                                    html += '<option  value="'+json[i]+'">'+json[i]+'</option>';
                                }
                            html += '</select>';
                        html +='</td>';
                        html += '<td>';
                            html += '<img class="torol_img" src="view/image/delete.png" alt="" onclick="$(this).parent().parent().remove(); return false;"><br>';
                        html +='</td>';
                    html += '</tr>';
                    $("#"+sor_id+' .tablak table .add_'+sor+'_'+tablasor+'.tabla_vege').before(html);

                }
            }

        });
    }

    function addOszlopAllando(sor,sor_id){

        var allando_mezo = 0;
        $("#"+sor_id+ ' .select_tabla_div, #'+sor_id+' .input_allando').each(function(){
            debugger;
            allando_mezo = $(this).attr('sor') > allando_mezo ? $(this).attr('sor') : ++allando_mezo;
        });

        var html = '';
        html += '<input type="text" class="input_allando" sor="'+allando_mezo+'" name="xml_beallitasok['+sor+'][xml_block]['+sor_id+'][mezok]['+allando_mezo+'][allando]" >';
        html += '<img  style="margin-right: 13px" class="mezo_torol"  src="view/image/delete.png" alt="" onclick="$(this).prev().remove(); $(this).next(\'br\').remove(); $(this).remove()" ><br>';

        $("#"+sor_id).append(html);
    }

    function addOszlopMezo(sor,sor_id){
        var sor_id_tomb = sor_id.split('-');
        var tabla_nevek = [];
        var keresett_id = 'nyit-'+sor;
        var tabla_nev = '';
        for(var i=2; sor_id_tomb.length > i; i++) {
            keresett_id += '-'+sor_id_tomb[i];

            $('#'+keresett_id+' .tablak .tabla_valaszt').each(function(){
                tabla_nev = $(this).val();
                 if (tabla_nev) {
                 tabla_nevek.push(tabla_nev);
                 }
            });
        }

        $.ajax({
            url: 'index.php?route=design/megjelenito_xml/oszlopTablaLista&token=<?php echo $token; ?>',
            type: "post",
            data: 'tabla_nevek=' + tabla_nevek,
            dataType: 'json',

            success: function(json) {
                if (json) {

                    var allando_mezo = 0;
                    $("#"+sor_id+ ' .select_tabla_div, #'+sor_id+' .input_allando').each(function(){
                        allando_mezo = $(this).attr('sor') > allando_mezo ? $(this).attr('sor') : ++allando_mezo;
                    });


                    var html = '';
                    var html_hidden = '';
                    html += '<div class="select_tabla_div" sor="'+allando_mezo+'">';
                    html += '<select class="select_tabla"  onchange="mezoTablaSelect('+sor+',\''+sor_id+'\',this,'+allando_mezo+')"';
                    html += ' name="xml_beallitasok['+sor+'][xml_block]['+sor_id+'][mezok]['+allando_mezo+'][tabla][tabla]">';
                        html += '<option value="">--- Válasszon táblát ---</option>';

                        for(var i=0; json.length > i; i++){
                            html += '<option value="'+json[i]+'">'+json[i].replace("<?php echo DB_PREFIX; ?>","")+'</option>'
                            html_hidden += '<input type="hidden"  class="vonatkozo_tablak" name="xml_beallitasok['+sor+'][xml_block]['+sor_id+'][tablak]['+json[i]+']" value="'+json[i]+'">';
                        }

                    html += '</select>';
                    html += '<img  style="margin-right: 13px" class="mezo_torol"  src="view/image/delete.png" alt="" onclick="$(this).parent().remove();" >';
                    html += html_hidden;
                    html +='</div>';
                    $("#"+sor_id ).append(html);
                }
            }
        });
    }

    function mezoTablaSelect(sor,sor_id,para,allando_mezo){

        var tabla_nev = $(para).val();

        $.ajax({
            url: 'index.php?route=design/megjelenito_xml/oszlopMezoLista&token=<?php echo $token; ?>',
            type: "post",
            data: 'tabla_nev=' + tabla_nev,
            dataType: 'json',

            success: function(json) {
                if (json) {
                    $(para).next('img').remove();

                    $(para).parent().find('.select_mezo_div').remove();
                    var html = '';
                    html += '<div class="select_mezo_div">';
                    html += '<select class="select_mezo" ';
                    html += ' name="xml_beallitasok['+sor+'][xml_block]['+sor_id+'][mezok]['+allando_mezo+'][tabla][mezo]">';
                    html += '<option value="">--- Válasszon mezőt ---</option>';

                    for(var i=0; json.length > i; i++){
                        html += '<option value="'+json[i]+'">'+json[i]+'</option>'
                    }

                    html += '</select>';

                    html += '<select class="select_kerekites" name="xml_beallitasok['+sor+'][xml_block]['+sor_id+'][mezok]['+allando_mezo+'][tabla][kerekites]">';
                        html += '<option value="">--- Kerekítés ---</option>';
                        html += '<option value="1" >Egészre</option>';
                        html += '<option value="2" >1 tizedesre</option>';
                        html += '<option value="3" >2 tizedesre</option>';
                        html += '<option value="4" >3 tizedesre</option>';
                    html += '</select>';

                    html += '<img  style="margin-right: 13px" class="mezo_torol"  src="view/image/delete.png" alt="" onclick="$(this).parent().parent().remove();" ><br>';
                    html += '</div>';
                    $(para).parent().append(html);
                }
            }
        });

        debugger;

    }


    function csikiCsuki(para,sor){
        if (this.event.target == para || this.event.target.localName == "span") {
            if ($(para).next().css("display") != "none") {
                $(para).next().slideUp(500);
                $(para).removeClass("active");

            } else {
                $('.csoportok_nyito').removeClass("active");
                $('.csoport_beallitasok').hide();
                $(para).next().slideDown(500);
                $(para).addClass("active");

            }
        }
        add_ujMezoAllandoGombok(sor);

    }

    function checkEllenoriz(sor,sor_id,aktualis_tabla,aktualis_elem) {
        $("#csoport_"+sor+" #"+sor_id+" input[type=checkbox]").prop("checked",false);
        $(aktualis_elem).prop("checked",true);

        $(".select_"+sor+"_"+sor_id).attr("disabled",false).css("opacity","1");
        $(aktualis_elem).parent().parent().next().find('select').attr("disabled",true).css("opacity","0.4");
        //$("[azonosito='select_"+sor+"_"+sor_id+"_"+aktualis_tabla+"']").attr("disabled",true).css("opacity","0.4");
    }

    function tablaValaszt(sor,sor_id,tablasor){

        if ($(".csoport_"+sor+" #"+sor_id+ " .tablak .feltetel").length > 0) {
            $(".csoport_"+sor+" #"+sor_id+ " .tablak .group_by").css('display','block');
            $(".csoport_"+sor+" #"+sor_id+ " .tablak .szuro_parameterek").css('display','block');
        } else {
            $(".csoport_"+sor+" #"+sor_id+ " .tablak .group_by").css('display','none');
            $(".csoport_"+sor+" #"+sor_id+ " .tablak .szuro_parameterek").css('display','none');
            $(".csoport_"+sor+" #"+sor_id+ " .tablak .group_by_area").html('');
            $(".csoport_"+sor+" #"+sor_id+ " .tablak .tabla_szuro_kapcsolat").html('');
            $(".csoport_"+sor+" #"+sor_id+ " .tablak .kulso_szuro_kapcsolat").html('');
        }

        $('#nyitott_sor').val(sor);
        $('#nyitott_block').val(sor_id);
        $('#nyitott_tabla').val(tablasor);
        $('#scroll_top').val($('html').offset().top);

        $('#form').submit();
    }

    function tarolasNyitZaro(para,sor) {
        if (para.value == 1) {
            $('#tarolas_helye_'+ sor).fadeOut(600);
        } else {
            $('#tarolas_helye_'+ sor).fadeIn(600);
        }

    }



</script>







<?php echo $footer; ?>