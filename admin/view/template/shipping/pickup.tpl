<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
          <div  style="float: left">
        <table class="form">
            <tr>
                <td><?php echo $entry_title_heading; ?></td>
                <td><input type="text" name="pickup_header" value="<?php echo $pickup_header; ?>" /></td>
            </tr>
            <tr>
                <td><?php echo $entry_title_text; ?></td>
                <td><input type="text" name="pickup_text" value="<?php echo $pickup_text; ?>" /></td>
            </tr>
          <tr>
            <td><?php echo $entry_geo_zone; ?></td>
            <td><select name="pickup_geo_zone_id">
                <option value="0"><?php echo $text_all_zones; ?></option>
                <?php foreach ($geo_zones as $geo_zone) { ?>
                <?php if ($geo_zone['geo_zone_id'] == $pickup_geo_zone_id) { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="pickup_status">
                <?php if ($pickup_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>

            <tr>
                <td><?php echo $entry_mail; ?></td>
                <td><textarea name="pickup_mail" style="width: 100%; height: 70px;"><?php echo $pickup_mail; ?></textarea></td>
            </tr>

          <tr>
            <td><?php echo $entry_sort_order; ?></td>
            <td><input type="text" name="pickup_sort_order" value="<?php echo $pickup_sort_order; ?>" size="1" /></td>
          </tr>

            <tr>
                <td><?php echo $entry_warehouse; ?></td>
                <td>
                <?php if ($pickup_warehouse == 1) { ?>
                    <input type="checkbox" checked="checked" name="pickup_warehouse" value="1" />
                <?php } else {?>
                    <input type="checkbox" name="pickup_warehouse" value="1" />
                <?php } ?>
                </td>
            </tr>


        </table>
    </div>
      <div  style="float: left; margin-left: 60px;"><h2>Fizetési módok</h2>
          <table class="form">
              <?php foreach($payment as $key =>$value) {?>
                  <tr>
                      <td><?php echo $value['name']; ?></td>
                      <td><select name="pickup_<?php echo $key?>">
                              <?php if ($value['valasztva']) { ?>
                                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                  <option value="0"><?php echo $text_disabled; ?></option>
                              <?php } else { ?>
                                  <option value="1"><?php echo $text_enabled; ?></option>
                                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                              <?php } ?>
                          </select></td>
                  </tr>
              <?php } ?>
          </table>
      </div>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>