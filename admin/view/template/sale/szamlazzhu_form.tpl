<form id="xml_szamlazz_beallitas" style="position: absolute; border-radius: 4px 4px 6px 6px; border: 1px solid rgb(187, 187, 187); padding: 20px; display: none; background: rgb(239, 239, 239);">
    <table id="xml_szamlazz_table" class="tranzo">
        <thead>
        <tr>
            <td style="font-size: 16px;">
                Beállítások
            </td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td title="xml_szamlazz[eloleg_szamla]" style="width: 244px;">Előleg számla:
            <td style="width: 144px;">
                <?php if (isset($szamlazzhu['eloleg_szamla']) && $szamlazzhu['eloleg_szamla'] == 1) { ?>
                    <input type="radio" name="xml_szamlazz[eloleg_szamla]" value="1" checked="checked" />
                    Igen
                    <input type="radio" name="xml_szamlazz[eloleg_szamla]" value="0" />
                    Nem
                <?php } else { ?>
                    <input type="radio" name="xml_szamlazz[eloleg_szamla]" value="1" />
                    Igen
                    <input type="radio" name="xml_szamlazz[eloleg_szamla]" value="0" checked="checked" />
                    Nem
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td title="xml_szamlazz[vegszamla]" style="width: 244px;">Végszámla:
            <td style="width: 144px;">
                <?php if (isset($szamlazzhu['vegszamla']) && $szamlazzhu['vegszamla'] == 1) { ?>
                    <input type="radio" name="xml_szamlazz[vegszamla]" value="1" checked="checked" />
                    Igen
                    <input type="radio" name="xml_szamlazz[vegszamla]" value="0" />
                    Nem
                <?php } else { ?>
                    <input type="radio" name="xml_szamlazz[vegszamla]" value="1" />
                    Igen
                    <input type="radio" name="xml_szamlazz[vegszamla]" value="0" checked="checked" />
                    Nem
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td title="xml_szamlazz[dijbekero]" style="width: 244px;">Díjbekérő:
            <td style="width: 144px;">
                <?php if (isset($szamlazzhu['dijbekero']) && $szamlazzhu['dijbekero'] == 1) { ?>
                    <input type="radio" name="xml_szamlazz[dijbekero]" value="1" checked="checked" />
                    Igen
                    <input type="radio" name="xml_szamlazz[dijbekero]" value="0" />
                    Nem
                <?php } else { ?>
                    <input type="radio" name="xml_szamlazz[dijbekero]" value="1" />
                    Igen
                    <input type="radio" name="xml_szamlazz[dijbekero]" value="0" checked="checked" />
                    Nem
                <?php } ?>
            </td>
        </tr>

        </tbody>
        <tbody>
        <tr>
            <td title="xml_szamlazz[fizetve]" style="width: 244px;">Fizetve:
            <td style="width: 144px;">
                <?php if (isset($szamlazzhu['fizetve']) && $szamlazzhu['fizetve'] == 1) { ?>
                    <input type="radio" name="xml_szamlazz[fizetve]" value="1" checked="checked" />
                    Igen
                    <input type="radio" name="xml_szamlazz[fizetve]" value="0" />
                    Nem
                <?php } else { ?>
                    <input type="radio" name="xml_szamlazz[fizetve]" value="1" />
                    Igen
                    <input type="radio" name="xml_szamlazz[fizetve]" value="0" checked="checked" />
                    Nem
                <?php } ?>
            </td>
        </tr>
        </tbody>
        <tbody>
        <tr>
            <td style="min-width: 140px; display: table">Példány szám:</td>
            <td>
                <input type="number" name="xml_szamlazz[peldany_szam]" value="<?php if(isset($szamlazzhu['peldany_szam'])){echo $szamlazzhu['peldany_szam'];};?>">
            </td>
        </tr>
        </tbody>
        <tbody>
        <tr>
            <td style="min-width: 140px; display: table">Keltezés dátuma:</td>
            <td>
                <input type="date" name="xml_szamlazz[kelt_datum]" value="<?php if(isset($szamlazzhu['kelt_datum'])){echo $szamlazzhu['kelt_datum'];};?>" size="4">
            </td>
        </tr>
        <?php if(isset($error_szamlazzhu) && $error_szamlazzhu['hibakod'] == 173){ ?>
            <tr>
                <td colspan="2">
                    <span class="error"><?php echo $error_szamlazzhu['text']; ?></span>
                </td>
            </tr>
        <?php } ?>
        <tr>
            <td style="min-width: 140px; display: table">Teljesítés dátuma:</td>
            <td>
                <input type="date" name="xml_szamlazz[teljesites_datum]" value="<?php if(isset($szamlazzhu['teljesites_datum'])){echo $szamlazzhu['teljesites_datum'];};?>" size="4">
            </td>
        </tr>
        <tr>
            <td style="min-width: 140px; display: table">Fizetési határidő dátuma:</td>
            <td>
                <input type="date" name="xml_szamlazz[fizetesi_hatarido_datum]" value="<?php if(isset($szamlazzhu['fizetesi_hatarido_datum'])){echo $szamlazzhu['fizetesi_hatarido_datum'];};?>" size="4">
            </td>
        </tr>
        </tbody>
        <tbody>
        <tr>
            <td style="min-width: 140px; display: table">Fizetési mód:</td>
            <td>
                <input name="xml_szamlazz[fizetesi_mod]" value="<?php echo preg_replace("/<.+?>/i", "", $payment_method); ?>" disabled>
            </td>
        </tr>
        <tr>
            <td style="min-width: 140px; display: table">Árfolyam Bank:</td>
            <td>
                <input type="text" name="xml_szamlazz[arfolyam_bank]" value="Magyar Nemzeti Bank(MNB)" disabled>
            </td>
        </tr>
        <tr>
            <td style="min-width: 140px; display: table">Pénznem:</td>
            <td>
                <select name="xml_szamlazz[penznem]">
                    <?php foreach ($currencies as $currency) { ?>
                        <?php if ($currency['code'] == $config_currency) { ?>
                            <option value="<?php echo $currency['code']; ?>" selected="selected"><?php echo $currency['title']; ?></option>
                        <?php } else { ?>
                            <option value="<?php echo $currency['code']; ?>"><?php echo $currency['title']; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td style="min-width: 140px; display: table">Árfolyam:</td>
            <td>
                <select name="xml_szamlazz[arfolyam]" disabled>
                    <?php foreach ($currencies as $currency) { ?>
                        <?php if ($currency['code'] == $config_currency) { ?>
                            <option  nyelv="<?php echo $currency['code']; ?>" value="<?php echo $currency['value']; ?>" selected="selected"><?php echo $currency['value']; ?></option>
                        <?php } else { ?>
                            <option  nyelv="<?php echo $currency['code']; ?>" value="<?php echo $currency['value']; ?>"><?php echo $currency['value']; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </td>
        </tr>

        </tbody>

        <tbody>
        <tr>
            <td style="min-width: 140px; display: table">Számla nyelve:</td>
            <td>
                <select name="xml_szamlazz[szamla_nyelve]">
                    <?php foreach ($languages as $language) { ?>
                        <?php if ($language['code'] == $config_admin_language) { ?>
                            <option value="<?php echo $language['code']; ?>" selected="selected"><?php echo $language['name']; ?></option>
                        <?php } else { ?>
                            <option value="<?php echo $language['code']; ?>"><?php echo $language['name']; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td style="min-width: 140px; display: table">Számlaszám előtag:</td>
            <td>
                <input type="text" name="xml_szamlazz[szamlaszam_elotag]" value="<?php echo $szamlazz_hu_module['szamlazz_hu_elotag']?>" size="4">
            </td>
        </tr>
        </tbody>
        <tbody>
        <tr>
            <td colspan="2" style="min-width: 140px; display: table">Megjegyzés:</td>
        </tr>
        <tr>
            <td colspan="2">
                <textarea name="xml_szamlazz[megjegyzes]"><?php if(isset($szamlazzhu['megjegyzes'])){echo $szamlazzhu['megjegyzes'];};?></textarea>
            </td>
        </tr>
        </tbody>
        <?php if(isset($error_szamlazzhu) && $error_szamlazzhu['hibakod'] != 173){ ?>
            <tbody>
            <tr>
                <td>
                    <span class="error"><?php echo $error_szamlazzhu['text']; ?></span>
                </td>
                <td>
                    <span class="error" style="cursor: pointer" onclick="xmlSzamlazz()">Bezár</span>
                </td>
            </tr>
            </tbody>
        <?php }else{ ?>
            <tr>
                <td colspan="3" style="text-align: right;">
                    <a onclick="xmlSzamlazzIndit(<?php echo $order_id; ?>,$('#xml_szamlazz_beallitas'))" class="button" style="position: relative;">Hozzáadás</a>
                </td>
            </tr>
        <?php } ?>
    </table>
</form>
<script>
    function URLParameter(sParam){
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++){
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam){
                return sParameterName[1];
            }
        }
    }

    function xmlSzamlazz() {
        if ($('#xml_szamlazz_beallitas').css("display") == "none") {
            $('#xml_szamlazz_beallitas').slideDown(500);
        } else {
            $('#xml_szamlazz_beallitas').slideUp(500);
        }
    }

    function xmlSzamlazzIndit(order_id,form) {
        var disabled = $('#xml_szamlazz_beallitas').find("input:disabled,select:disabled");
        $('#xml_szamlazz_beallitas').find("input:disabled").removeAttr("disabled");
        $('#xml_szamlazz_beallitas').find("select:disabled").removeAttr("disabled");
        var formdata = form.serializeArray();
        var datum = form.find("input[type='date']");
        var ellenor = true;
        var hibas = new Array();

        for(var k = 0; k < formdata.length; k++){
            for(var n = 0; n < datum.length; n++){
                if(formdata[k].name == datum.eq(n).attr("name")){
                    if(!valid_date(formdata[k].value)){
                        hibas[n] = formdata[k].name;
                    }
                }
            }
        }
        if($("input[name='xml_szamlazz[peldany_szam]']").attr("value") == ""){
            hibas[hibas.length] = "xml_szamlazz[peldany_szam]";
        }

        if(hibas.length == 0){
            if($(".required_field").length != 0){
                $(".required_field").remove();
            }
            var utm = "";

            for(var i = 0; i < formdata.length; i++){
                utm += "&"+formdata[i].name+"="+formdata[i].value;
            }

            var url = 'index.php?route=tool/xml_export_szamlazz/xmlSql&token=<?php echo $token; ?>';
            url += '&honnan='+URLParameter("route");
            url += '&order_id='+order_id;
            url += utm;
            location = url;
            add_disable(disabled);
        }
        else{
            required(hibas);
            add_disable(disabled);
        }
    }

    $("#xml_szamlazz_table tbody:not(thead):nth-child(2) input[type='radio']").bind("click",function(){
        var sorok = $(this).parent("td").parent("tr").parent("tbody").find("tr");
        for(var i = 0; i< sorok.length; i++){
            sorok.eq(i).find("td:last-child").find("input[type='radio']").removeAttr("checked");
            sorok.eq(i).find("td:last-child").find("input[type='radio']:last-child").prop("checked","checked");
        }
        $(this).prop("checked","checked");

        if(URLParameter("hibakod") != undefined && URLParameter("hibakod").length != 0){
            debugger;
            var order_id = URLParameter("order_id");
            var url = 'index.php?route='+URLParameter("route")+'&token=<?php echo $token; ?>';
            url += "&order_id="+order_id;
            url += "&open="+$(this).attr("name");
            location = url;
        }

    });

    $("#xml_szamlazz_beallitas").find("input[type='date']").bind("click",function(){
        remove_required_field();
    });

    $("#xml_szamlazz_beallitas").find("input[name='xml_szamlazz[peldany_szam]']").bind("click",function(){
        remove_required_field();
    });

    function remove_required_field(){
        $(".required_field").remove();
    }

    $("#xml_szamlazz_beallitas select[name='xml_szamlazz[penznem]']").on("change", function() {
        debugger;
        var value = $(this).find("option:selected").val();
        var arfolyam = $("select[name='xml_szamlazz[arfolyam]']").find("option");
        arfolyam.removeAttr("selected");
        for(var i = 0; i < arfolyam.length; i++){
            if(arfolyam.eq(i).attr("nyelv") == value){
                arfolyam.eq(i).prop("selected","selected");
            }
        }
    });

    function required(hibas){
        debugger;
        var elem = "";
        for(var i = 0; i < hibas.length; i++){
            elem = $("input[name='"+hibas[i]+"']");

            if($(".required_field").length == 0){
                elem.parent("td").css("position","relative");
                elem.parent("td").append("<div class='required_field'>Kérjük töltse ki ezt a mezőt!</div>");
            }
        }

    }

    function add_disable(disabled){
        for(var j = 0; j < disabled.length; j++){
            for(var l = 0; l < $("input").length; l++){
                if($("input").eq(l).attr("name") == disabled.eq(j).attr("name")){
                    $("input").eq(l).attr("disabled","disabled");
                }
            }
            debugger;
            for(var h = 0; h < $("select").length; h++){
                if($("select").eq(h).attr("name") == disabled.eq(j).attr("name")){
                    $("select").eq(h).attr("disabled","disabled");
                }
            }
        }
    }

    function valid_date(date){
        var regex = new RegExp("\\d{4}\-\\d{1,2}\-\\d{1,2}","g");
        if(date.match(regex) != null){
            return true;
        }else{
            return false;
        }
    }
</script>
<?php if(isset($error_szamlazzhu) || isset($open_lenyilo)){ ?>
    <script>
        $(document).ready(function(){
            xmlSzamlazz();
            <?php if(isset($open_lenyilo)){ ?>
            for(var i = 0; i < $("input").length; i++){
                debugger;
                if(($("input").eq(i).attr("name") == "<?php echo $open_lenyilo;?>") && $("input").eq(i).attr("value") == 1){
                    $("input").eq(i).click();
                }
            }
            <?php } ?>
        });
    </script>
<?php } ?>