<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box" style="position: relative">
  <div class="product_list_oszlop" style="display: none">
      <img  style="position: absolute; right: 4px; top: 5px;"   src="view/image/close.png" alt="" title="Bezár"
            onclick="$(this).parent().slideUp('slow');"  >

      <h3 style="margin-top: 16px">Jelölje be a megjeleníteni kívánt oszlopokat</h3>



      <input type="checkbox" name="termek_kep" value="1"
          <?php echo $this->config->get('termek_kep') == 1 ? 'checked="checked"' : ''?> >
          <?php echo $column_image;?><br>

      <input type="checkbox" name="termek_product_id" value="1"
          <?php echo $this->config->get('termek_product_id') == 1 ? 'checked="checked"' : ''?> >
      <?php echo $column_product_id;?><br>

      <input type="checkbox" name="termek_neve" value="1"
          <?php echo $this->config->get('termek_neve') == 1 ? 'checked="checked"' : ''?> >
          <?php echo $column_name;?><br>

      <?php if ( $this->config->get('megjelenit_list_admin_model') == 1) { ?>
          <input type="checkbox" name="termek_model" value="1"
              <?php echo $this->config->get('termek_model') == 1 ? 'checked="checked"' : ''?> >
              <?php echo $column_model; ?><br>
      <?php } ?>

      <?php if ( $this->config->get('megjelenit_list_admin_cikkszam') == 1) { ?>
          <input type="checkbox" name="termek_cikkszam" value="1"
              <?php echo $this->config->get('termek_cikkszam') == 1 ? 'checked="checked"' : ''?> >
              <?php echo $column_cikkszam; ?><br>
      <?php } ?>

      <?php if ( $this->config->get('megjelenit_list_admin_cikkszam2') == 1) { ?>
          <input type="checkbox" name="termek_cikkszam2" value="1"
              <?php echo $this->config->get('termek_cikkszam2') == 1 ? 'checked="checked"' : ''?> >
              <?php echo $column_cikkszam2; ?><br>
      <?php } ?>

      <input type="checkbox" name="termek_ara" value="1"
          <?php echo $this->config->get('termek_ara') == 1 ? 'checked="checked"' : ''?> >
          <?php echo $column_price;?><br>

      <input type="checkbox" name="termek_kategoria" value="1"
          <?php echo $this->config->get('termek_kategoria') == 1 ? 'checked="checked"' : ''?> >
          <?php echo $column_category;?><br>

      <?php if ( $this->config->get('megjelenit_list_admin_modositas') == 1) { ?>
          <input type="checkbox" name="termek_modositas_datuma" value="1"
              <?php echo $this->config->get('termek_modositas_datuma') == 1 ? 'checked="checked"' : ''?> >
              <?php echo $column_date_modify; ?><br>
      <?php } ?>

      <?php if ( $this->config->get('megjelenit_list_admin_hozzaadas') == 1) { ?>
          <input type="checkbox" name="termek_letrehozas_datuma" value="1"
              <?php echo $this->config->get('termek_letrehozas_datuma') == 1 ? 'checked="checked"' : ''?> >
              <?php echo $column_date_added; ?><br>
      <?php } ?>

      <?php if ( $this->config->get('megjelenit_list_admin_kifuto') == 1) { ?>
          <input type="checkbox" name="termek_kifuto" value="1"
              <?php echo $this->config->get('termek_kifuto') == 1 ? 'checked="checked"' : ''?> >
          <?php echo $column_kifuto; ?><br>
      <?php } ?>

      <?php if ( $this->config->get('megjelenit_list_admin_ujdonsag') == 1) { ?>
          <input type="checkbox" name="termek_ujdonsag" value="1"
              <?php echo $this->config->get('termek_ujdonsag') == 1 ? 'checked="checked"' : ''?> >
          <?php echo $column_ujdonsag; ?><br>
      <?php } ?>

      <?php if ( $this->config->get('megjelenit_list_admin_elorendeles') == 1) { ?>
          <input type="checkbox" name="termek_elorendeles" value="1"
              <?php echo $this->config->get('termek_elorendeles') == 1 ? 'checked="checked"' : ''?> >
          <?php echo $column_elorendeles; ?><br>
      <?php } ?>

      <?php if ( $this->config->get('megjelenit_list_admin_ar_tiltasa') == 1) { ?>
          <input type="checkbox" name="termek_ar_tiltasa" value="1"
              <?php echo $this->config->get('termek_ar_tiltasa') == 1 ? 'checked="checked"' : ''?> >
          <?php echo $column_ar_tiltasa; ?><br>
      <?php } ?>

      <?php if ( $this->config->get('megjelenit_list_admin_megrendelem') == 1) { ?>
          <input type="checkbox" name="termek_megrendelem" value="1"
              <?php echo $this->config->get('termek_megrendelem') == 1 ? 'checked="checked"' : ''?> >
          <?php echo $column_megrendelem; ?><br>
      <?php } ?>

      <?php if ( $this->config->get('megjelenit_list_admin_extra_garancia') == 1) { ?>
          <input type="checkbox" name="termek_extra_garancia" value="1"
              <?php echo $this->config->get('termek_extra_garancia') == 1 ? 'checked="checked"' : ''?> >
          <?php echo $column_garancia_ertek; ?><br>
      <?php } ?>

      <input type="checkbox" name="termek_mennyisege" value="1"
          <?php echo $this->config->get('termek_mennyisege') == 1 ? 'checked="checked"' : ''?> >
          <?php echo $column_quantity;?><br>

      <input type="checkbox" name="termek_statusza" value="1"
          <?php echo $this->config->get('termek_statusza') == 1 ? 'checked="checked"' : ''?> >
          <?php echo $column_status;?><br>

      <a  onclick="oszlopBealit()" style="margin-left: auto; display: table;" class="button">Beállít</a>
  </div>

  <div class="heading">
      <img style="position: relative; top: 6px; margin-right: 5px;" src="view/image/product.png" alt="" />
          <a class="button" onclick="$('.product_list_oszlop').css('display') == 'none' ? $('.product_list_oszlop').slideDown('slow') : $('.product_list_oszlop').slideUp('slow')"> <?php echo $heading_title; ?></a>


      <div class="buttons">
          <?php if(isset($megjelenit_admin_altalanos['csv_export_import']) && $megjelenit_admin_altalanos['csv_export_import'] == 1) {?>
          <div style="position: relative; display: inline-table">

              <a style="position: relative"  class="button"
                    onclick="
                        $('.modell_szuro').css('display','none');
                        $('#csv_export_select').css('display') == 'none' ? $('#csv_export_select').slideDown(700) : $('#csv_export_select').slideUp(700);
                        $('#csv_export_select :nth-child(1)').prop('selected', true);
                        $('.import_').css('display','none')" >
                <?php echo $button_csv_export; ?>
              </a>

                <span id="csv_export_select" class="export_import_select export_">
                    <select name="csv_export" onchange="
                            (this.value == 1) ? $('#modell_szur').slideDown(700) : '';
                            (this.value != 0) ? $('#csv_export_select').slideUp(700) : '';
                            (this.value == 2) ? $('#modell_szur_kapcsolodo').slideDown(700) : '';
                            (this.value == 3) ? $('#modell_szur_ajanlott').slideDown(700) : '';
                            (this.value == 4) ? $('#modell_szur_altalanos').slideDown(700) : ''; ">

                        <option value="0" selected="selected"><?php echo $text_valasszon; ?></option>
                        <option value="1"><?php echo $text_termekjellemzok; ?></option>
                        <option value="2"><?php echo $text_kapcsolodotermekek; ?></option>
                        <option value="3"><?php echo $text_ajanlott_tartozekok; ?></option>
                        <option value="4"><?php echo $text_altalanos_tartozekok; ?></option>
                    </select>
                </span>

                <span id="modell_szur" class="export_import_select modell_szuro export_" >
                    <span>
                      <?php echo $column_status; ?><select name="csv_status">
                          <option value="0" selected="selected"><?php echo $text_valasszon; ?></option>
                          <option value="1"><?php echo $text_engedelyezett; ?></option>
                          <option value="2"><?php echo $text_letiltott; ?></option>
                      </select>
                    </span>
                    <span>
                        <?php echo $column_model; ?> <input type="text" name="modell_szuro" value="" size="16">
                        <input type="button" name="modell_buttom" value="Tovább" style="display: table; margin-left: auto">
                    </span>
                </span>

                <span id="modell_szur_kapcsolodo" class="export_import_select modell_szuro export_" >
                    <span><b><?php echo $heading_title; ?></b><br>
                        <?php echo $column_status; ?><select name="csv_status_kapcsolodo">
                            <option value="0" selected="selected"><?php echo $text_valasszon; ?></option>
                            <option value="1"><?php echo $text_engedelyezett; ?></option>
                            <option value="2"><?php echo $text_letiltott; ?></option>
                        </select>
                    </span>
                    <span>
                        <?php echo $column_model; ?> <input type="text" name="modell_szuro_kapcsolodo" value="" size="16">
                    </span>
                    <span style="margin-top: 19px; display: block;"><b><?php echo $text_kapcsolodotermekek; ?></b><br>
                        <?php echo $column_status; ?><select name="csv_status_kapcsolodo_termekek">
                            <option value="0" selected="selected"><?php echo $text_valasszon; ?></option>
                            <option value="1"><?php echo $text_engedelyezett; ?></option>
                            <option value="2"><?php echo $text_letiltott; ?></option>
                        </select>
                    </span>
                    <span>
                        <?php echo $column_model; ?> <input type="text" name="modell_szuro_kapcsolodo_termekek" value="" size="16">
                        <input type="button" name="modell_button_kapcsolodo" value="Tovább" style="display: table; margin-left: auto; margin-top: 5px;">
                    </span>
                </span>



                <span id="modell_szur_ajanlott" class="export_import_select modell_szuro export_" >
                    <span><b><?php echo $heading_title; ?></b><br>
                        <?php echo $column_status; ?><select name="csv_status_ajanlott">
                            <option value="0" selected="selected"><?php echo $text_valasszon; ?></option>
                            <option value="1"><?php echo $text_engedelyezett; ?></option>
                            <option value="2"><?php echo $text_letiltott; ?></option>
                        </select>
                    </span>
                    <span>
                        <?php echo $column_model; ?> <input type="text" name="modell_szuro_ajanlott" value="" size="16">
                    </span>
                    <span style="margin-top: 19px; display: block;"><b><?php echo $text_kapcsolodotermekek; ?></b><br>
                        <?php echo $column_status; ?><select name="csv_status_ajanlott_termekek">
                            <option value="0" selected="selected"><?php echo $text_valasszon; ?></option>
                            <option value="1"><?php echo $text_engedelyezett; ?></option>
                            <option value="2"><?php echo $text_letiltott; ?></option>
                        </select>
                    </span>
                    <span>
                        <?php echo $column_model; ?> <input type="text" name="modell_szuro_ajanlott_termekek" value="" size="16">
                        <input type="button" name="modell_button_ajanlott" value="Tovább" style="display: table; margin-left: auto; margin-top: 5px;">
                    </span>
                </span>



                <span id="modell_szur_altalanos" class="export_import_select modell_szuro export_" >
                    <span><b><?php echo $heading_title; ?></b><br>
                        <?php echo $column_status; ?><select name="csv_status_altalanos">
                            <option value="0" selected="selected"><?php echo $text_valasszon; ?></option>
                            <option value="1"><?php echo $text_engedelyezett; ?></option>
                            <option value="2"><?php echo $text_letiltott; ?></option>
                        </select>
                    </span>
                    <span>
                        <?php echo $column_model; ?> <input type="text" name="modell_szuro_altalanos" value="" size="16">
                    </span>
                    <span style="margin-top: 19px; display: block;"><b><?php echo $text_kapcsolodotermekek; ?></b><br>
                        <?php echo $column_status; ?><select name="csv_status_altalanos_termekek">
                            <option value="0" selected="selected"><?php echo $text_valasszon; ?></option>
                            <option value="1"><?php echo $text_engedelyezett; ?></option>
                            <option value="2"><?php echo $text_letiltott; ?></option>
                        </select>
                    </span>
                    <span>
                        <?php echo $column_model; ?> <input type="text" name="modell_szuro_altalanos_termekek" value="" size="16">
                        <input type="button" name="modell_button_altalanos" value="Tovább" style="display: table; margin-left: auto; margin-top: 5px;">
                    </span>
                </span>

              </div>

              <div style="position: relative; display: inline-table">
                  <a onclick="  $('select[name=\'csv_import\'] :nth-child(1)').prop('selected', true);
                            $('select[name=\'csv_files\'] :nth-child(1)').prop('selected', true);
                            $('.export_, .import_').css('display','none');
                            $('#csv_import_select').css('display') == 'none' ? $('#csv_import_select').slideDown(700) : $('#csv_import_select').slideUp(700);"
                                  class="button" style="position: relative"><?php echo $button_csv_import; ?>
                  </a>

                  <span id="csv_import_select" class="export_feltolt_select import_valaszt import_">
                      <select name="csv_import" id="csv_import_kivalasztas"
                                onchange="this.value == 1 ? $('#csv_file_feltolt').slideDown(700) : '';
                                          this.value == 2 ? $('#csv_beolvas').slideDown(700) : '';
                                          $('select[name=\'csv_files\'] :nth-child(1)').prop('selected', true);">
                            <option value="0" selected="selected"><?php echo $text_valasszon; ?></option>
                            <option value="1"><?php echo $text_file_feltoltes; ?></option>
                            <option value="2"><?php echo $text_file_importalas; ?> </option>
                      </select>
                  </span>


                  <span id="csv_file_feltolt" style="display: none;" class="export_feltolt_select import_">
                      <form name=talloz enctype='multipart/form-data' action="<? echo $csvinsert;?>" method='post'>
                          <span style="display: table; margin: auto; padding: 12px; font-weight: bold;"><?php echo $text_kerem_toltsefel; ?></span>

                          <input type="hidden" name="MAX_FILE_SIZE" value="300000" />
                          <input name='userfile' type='file' accept='text/csv' id=filein onChange=kk(this)>
                          <input type='submit' name="kepekrogzit" style='display:none; margin-left: auto;' id=inputfeltolt value='Feltöltés'
                              onclick="$('.export_export_select').slideUp(600)">
                          <input type="hidden" id="hiddenobj" name="melyik_editkep">
                      </form>
                  </span>

                  <span id="csv_beolvas" style="display: none;" class="export_feltolt_select import_valaszt import_">
                      <?php if ($csv_files) { ?>
                          <select name="csv_files"  onchange="this.value != '0' ? csvBeolvas(this.value) : ''" >
                              <option value="0" selected="selected"><?php echo $text_import_valasszon; ?></option>
                              <?php foreach($csv_files as $file) { ?>
                                  <option value="<?php echo $file?>" ><?php echo basename($file, '.csv'); ?></option>
                              <?php } ?>
                          </select>
                      <?php } ?>
                  </span>
              </div>
          <?php } ?>
          <a onclick="location = '<?php echo $insert; ?>'" class="button"><?php echo $button_insert; ?></a>
          <a onclick="$('#form').attr('action', '<?php echo $copy; ?>'); $('#form').submit();" class="button"><?php echo $button_copy; ?></a>
          <a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a>
      </div>
    </div>


    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
                <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>





                <?php if ($this->config->get('termek_kep')) { ?>
                    <td class="center"><?php echo $column_image; ?></td>
                <?php } ?>

                <?php if ($this->config->get('termek_product_id')) { ?>
                    <td class="left">
                        <?php if ($sort == 'p.product_id') { ?>
                            <a href="<?php echo $sort_product_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_product_id; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_product_id; ?>"><?php echo $column_product_id; ?></a>
                        <?php } ?>
                    </td>
                <?php } ?>

                <?php if ($this->config->get('termek_neve')) { ?>
                    <td class="left">
                        <?php if ($sort == 'pd.name') { ?>
                            <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                        <?php } ?>
                    </td>
                <?php } ?>

                <?php if ( $this->config->get('termek_model') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_model') == 1) { ?>
                        <td class="left"><?php if ($sort == 'p.model') { ?>
                                <a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_model; ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $sort_model; ?>"><?php echo $column_model; ?></a>
                            <?php } ?>
                        </td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_cikkszam') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_cikkszam') == 1) { ?>
                        <td class="left"><?php if ($sort == 'p.cikkszam') { ?>
                                <a href="<?php echo $sort_cikkszam; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_cikkszam; ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $sort_cikkszam; ?>"><?php echo $column_cikkszam; ?></a>
                            <?php } ?>
                        </td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_cikkszam2') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_cikkszam2') == 1) { ?>
                        <td class="left"><?php if ($sort == 'p.cikkszam2') { ?>
                                <a href="<?php echo $sort_cikkszam2; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_cikkszam2; ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $sort_cikkszam2; ?>"><?php echo $column_cikkszam2; ?></a>
                            <?php } ?>
                        </td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_ara') == 1) { ?>
                    <td class="left">
                        <?php if ($sort == 'p.price') { ?>
                            <a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_price; ?>"><?php echo $column_price; ?></a>
                        <?php } ?>
                    </td>
                <?php } ?>

                <?php if ( $this->config->get('termek_kategoria') == 1) { ?>
                    <td class="left kategoria">
                        <?php if ($sort == 'p2c.category_id') { ?>
                            <a href="<?php echo $sort_category; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_category; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_category; ?>"><?php echo $column_category; ?></a>
                        <?php } ?>
                    </td>
                <?php } ?>

                <?php if ( $this->config->get('termek_modositas_datuma') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_modositas') == 1) { ?>
                        <td class="left">
                            <?php if ($sort == 'p.date_modified') { ?>
                                <a href="<?php echo $sort_date_modify; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_modify; ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $sort_date_modify; ?>"><?php echo $column_date_modify; ?></a>
                            <?php } ?>
                        </td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_letrehozas_datuma') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_hozzaadas') == 1) { ?>
                        <td class="left">
                            <?php if ($sort == 'p.date_added') { ?>
                                <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                          <?php } else { ?>
                                <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                          <?php } ?>
                        </td>
                    <?php } ?>
                <?php } ?>


                <?php if ( $this->config->get('termek_kifuto') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_kifuto') == 1) { ?>
                        <td class="left"><?php if ($sort == 'p.kifuto') { ?>
                                <a href="<?php echo $sort_kifuto; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_kifuto; ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $sort_kifuto; ?>"><?php echo $column_kifuto; ?></a>
                            <?php } ?>
                        </td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_ujdonsag') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_ujdonsag') == 1) { ?>
                        <td class="left"><?php if ($sort == 'p.ujdonsag') { ?>
                                <a href="<?php echo $sort_ujdonsag; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_ujdonsag; ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $sort_ujdonsag; ?>"><?php echo $column_ujdonsag; ?></a>
                            <?php } ?>
                        </td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_elorendeles') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_elorendeles') == 1) { ?>
                        <td class="left"><?php if ($sort == 'p.elorendeles') { ?>
                                <a href="<?php echo $sort_elorendeles; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_elorendeles; ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $sort_elorendeles; ?>"><?php echo $column_elorendeles; ?></a>
                            <?php } ?>
                        </td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_ar_tiltasa') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_ar_tiltasa') == 1) { ?>
                        <td class="left"><?php if ($sort == 'p.ar_tiltasa') { ?>
                                <a href="<?php echo $sort_ar_tiltasa; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_ar_tiltasa; ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $sort_ar_tiltasa; ?>"><?php echo $column_ar_tiltasa; ?></a>
                            <?php } ?>
                        </td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_megrendelem') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_megrendelem') == 1) { ?>
                        <td class="left"><?php if ($sort == 'p.megrendelem') { ?>
                                <a href="<?php echo $sort_megrendelem; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_megrendelem; ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $sort_megrendelem; ?>"><?php echo $column_megrendelem; ?></a>
                            <?php } ?>
                        </td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_extra_garancia') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_extra_garancia') == 1) { ?>
                        <td class="left"><?php if ($sort == 'p.garancia_ertek') { ?>
                                <a href="<?php echo $sort_garancia_ertek; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_garancia_ertek; ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $sort_garancia_ertek; ?>"><?php echo $column_garancia_ertek; ?></a>
                            <?php } ?>
                        </td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_mennyisege') == 1) { ?>
                    <td class="right">
                        <?php if ($sort == 'p.quantity') { ?>
                            <a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_quantity; ?></a>
                          <?php } else { ?>
                              <a href="<?php echo $sort_quantity; ?>"><?php echo $column_quantity; ?></a>
                          <?php } ?>
                    </td>
                <?php } ?>

                <?php if ( $this->config->get('termek_statusza') == 1) { ?>
                    <td class="left">
                        <?php if ($sort == 'p.status') { ?>
                            <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                        <?php } ?>
                    </td>

                 <?php } ?>
                <td class="center"><?php echo $column_action; ?></td>


            </tr>
          </thead>
          <tbody>
            <tr class="filter">
                <td></td>

                <?php if ($this->config->get('termek_kep')) { ?>
                    <td></td>
                <?php } ?>

                <?php if ($this->config->get('termek_product_id')) { ?>
                    <td><input type="text" name="filter_product_id" value="<?php echo $filter_product_id; ?>" size="4"/></td>
                <?php } ?>

                <?php if ($this->config->get('termek_neve')) { ?>
                    <td><input type="text" name="filter_name" value="<?php echo $filter_name; ?>" /></td>
                <?php } ?>

                <?php if ( $this->config->get('termek_model') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_model') == 1) { ?>
                        <td><input type="text" name="filter_model" value="<?php echo $filter_model; ?>" /></td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_cikkszam') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_cikkszam') == 1) { ?>
                        <td><input type="text" name="filter_cikkszam" value="<?php echo $filter_cikkszam; ?>" /></td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_cikkszam2') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_cikkszam2') == 1) { ?>
                        <td><input type="text" name="filter_cikkszam2" value="<?php echo $filter_cikkszam2; ?>" /></td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_ara') == 1) { ?>
                    <td align="left"><input type="text" name="filter_price" value="<?php echo $filter_price; ?>" size="8"/></td>
                <?php } ?>

                <?php if ( $this->config->get('termek_kategoria') == 1) { ?>
                    <td><select name="filter_category" id="filter_category" class="kategoria">
                        <option value="*"><?php echo $text_select?></option>
                        <?php foreach ($categories as $category) { ?>
                            <?php if ($category['category_id']==$filter_category) { ?>
                                <option value="<?php echo $category['category_id']; ?>" selected="selected"><?php echo $category['name']; ?></option>
                            <?php } else { ?>
                                <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </td>
                <?php } ?>

                <?php if ( $this->config->get('termek_modositas_datuma') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_modositas') == 1) { ?>
                      <td align="left"><input type="text" name="filter_modify_date" class="date" value="<?php echo $filter_modify_date; ?>" size="8"/></td>
                    <?php } ?>
                <?php } ?>


                <?php if ( $this->config->get('termek_letrehozas_datuma') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_hozzaadas') == 1) { ?>
                        <td align="left"><input type="text" name="filter_date_added" class="date" value="<?php echo $filter_date_added; ?>" size="8"/></td>
                    <?php } ?>
                <?php } ?>


                <?php if ( $this->config->get('termek_kifuto') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_kifuto') == 1) { ?>
                        <td></td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_ujdonsag') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_ujdonsag') == 1) { ?>
                        <td></td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_elorendeles') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_elorendeles') == 1) { ?>
                        <td></td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_ar_tiltasa') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_ar_tiltasa') == 1) { ?>
                        <td></td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_megrendelem') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_megrendelem') == 1) { ?>
                        <td></td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_extra_garancia') == 1) { ?>
                    <?php if ( $this->config->get('megjelenit_list_admin_extra_garancia') == 1) { ?>
                        <td></td>
                    <?php } ?>
                <?php } ?>

                <?php if ( $this->config->get('termek_mennyisege') == 1) { ?>
                    <td align="right"><input type="text" name="filter_quantity" value="<?php echo $filter_quantity; ?>" style="text-align: right; width:50px" /></td>
                <?php } ?>

                <?php if ( $this->config->get('termek_statusza') == 1) { ?>
                    <td>
                        <select name="filter_status">
                            <option value="*"></option>
                            <?php if ($filter_status) { ?>
                                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <?php } else { ?>
                                <option value="1"><?php echo $text_enabled; ?></option>
                            <?php } ?>

                            <?php if (!is_null($filter_status) && !$filter_status) { ?>
                                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                            <?php } else { ?>
                                <option value="0"><?php echo $text_disabled; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                <?php } ?>

                <td align="right"><a onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
            </tr>
            <?php if ($products) { ?>
            <?php foreach ($products as $product) { ?>
                <tr>
                    <td style="text-align: center;">
                        <?php if ($product['selected']) { ?>
                            <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" checked="checked" />
                        <?php } else { ?>
                            <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" />
                        <?php } ?>
                    </td>


                    <?php if ($this->config->get('termek_kep')) { ?>
                        <td class="center">
                            <img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" style="padding: 1px; border: 1px solid #DDDDDD;" />
                        </td>
                    <?php } ?>

                    <?php if ($this->config->get('termek_product_id')) { ?>
                        <td class="left"><?php echo $product['product_id']; ?></td>
                        </td>
                    <?php } ?>

                    <?php if ($this->config->get('termek_neve')) { ?>
                        <td class="left <?php echo $product['class']?>"><?php echo $product['name']; ?></td>
                    <?php } ?>

                    <?php if ($this->config->get('termek_model')) { ?>
                        <?php if ( $this->config->get('megjelenit_list_admin_model') == 1) { ?>
                            <td class="left"><?php echo $product['model']; ?></td>
                        <?php } ?>
                    <?php } ?>

                    <?php if ($this->config->get('termek_cikkszam')) { ?>
                        <?php if ( $this->config->get('megjelenit_list_admin_cikkszam') == 1) { ?>
                            <td class="left"><?php echo $product['cikkszam']; ?></td>
                        <?php } ?>
                    <?php } ?>

                    <?php if ($this->config->get('termek_cikkszam2')) { ?>
                        <?php if ( $this->config->get('megjelenit_list_admin_cikkszam2') == 1) { ?>
                            <td class="left"><?php echo $product['cikkszam2']; ?></td>
                        <?php } ?>
                    <?php } ?>

                    <?php if ($this->config->get('termek_ara')) { ?>
                        <td class="left">

                            <?php if ($product['special']) { ?>
                                <span class="list_ertek" onclick="listElemValueModify(this)">
                                    <span style="text-decoration: line-through;"><?php echo $product['price']; ?></span><br/>
                                    <span style="color: #b00;"><?php echo $product['special']; ?></span>
                                </span>
                                <span class="list_modify" style="display: none">
                                    <input type="text" name="price" value="<?php echo $product['price']; ?>" modositas="<?php echo $product['price_event']?>">
                                </span>

                            <?php } else { ?>
                                <span class="list_ertek" onclick="listElemValueModify(this)">
                                    <?php echo $product['price']; ?>
                                </span>
                                <span class="list_modify" style="display: none">
                                    <input type="text" name="price" value="<?php echo $product['price']; ?>" modositas="<?php echo $product['price_event']?>">
                                </span>
                            <?php } ?>
                        </td>
                    <?php } ?>


                    <?php if ($this->config->get('termek_kategoria')) { ?>
                        <td class="left kategoria">
                            <?php foreach ($categories as $category) { ?>
                                <?php if (in_array($category['category_id'], $product['category'])) { ?>
                                    <?php echo $category['name'];?><br>
                                <?php } ?>
                            <?php } ?>
                        </td>
                    <?php } ?>

                    <?php if ($this->config->get('termek_modositas_datuma')) { ?>
                        <?php if ( $this->config->get('megjelenit_list_admin_modositas') == 1) { ?>
                            <td class="left"><?php echo $product['date_modified']; ?></td>
                        <?php } ?>
                    <?php } ?>

                    <?php if ($this->config->get('termek_letrehozas_datuma')) { ?>
                        <?php if ( $this->config->get('megjelenit_list_admin_hozzaadas') == 1) { ?>
                            <td class="left"><?php echo $product['date_added']; ?></td>
                        <?php } ?>
                    <?php } ?>

                    <?php if ($this->config->get('termek_kifuto')) { ?>
                        <?php if ( $this->config->get('megjelenit_list_admin_kifuto') == 1) { ?>
                            <td class="left"><?php echo $product['kifuto']; ?></td>
                        <?php } ?>
                    <?php } ?>

                    <?php if ($this->config->get('termek_ujdonsag')) { ?>
                        <?php if ( $this->config->get('megjelenit_list_admin_ujdonsag') == 1) { ?>
                            <td class="left"><?php echo $product['ujdonsag']; ?></td>
                        <?php } ?>
                    <?php } ?>

                    <?php if ($this->config->get('termek_elorendeles')) { ?>
                        <?php if ( $this->config->get('megjelenit_list_admin_elorendeles') == 1) { ?>
                            <td class="left">
                                <span class="list_ertek" onclick="listElemValueModify(this)">
                                    <?php echo $product['elorendeles']; ?>
                                </span>
                                <span class="list_modify" style="display: none">
                                    <input type="text" name="elorendeles" value="<?php echo $product['elorendeles']; ?>" modositas="<?php echo $product['elorendeles_event']?>" size="2">
                                </span>
                            </td>
                        <?php } ?>
                    <?php } ?>

                    <?php if ($this->config->get('termek_ar_tiltasa')) { ?>
                        <?php if ( $this->config->get('megjelenit_list_admin_ar_tiltasa') == 1) { ?>
                            <td class="left">
                                <span class="list_ertek" onclick="listElemValueModify(this)">
                                    <?php echo $product['ar_tiltasa']; ?>
                                </span>
                                <span class="list_modify" style="display: none">
                                    <input type="text" name="ar_tiltasa" value="<?php echo $product['ar_tiltasa']; ?>" modositas="<?php echo $product['ar_tiltasa_event']?>" size="2">
                                </span>
                            </td>
                        <?php } ?>
                    <?php } ?>

                    <?php if ($this->config->get('termek_megrendelem')) { ?>
                        <?php if ( $this->config->get('megjelenit_list_admin_megrendelem') == 1) { ?>
                            <td class="left"><?php echo $product['megrendelem']; ?></td>
                        <?php } ?>
                    <?php } ?>

                    <?php if ($this->config->get('termek_extra_garancia')) { ?>
                        <?php if ( $this->config->get('megjelenit_list_admin_extra_garancia') == 1) { ?>
                            <td class="left"><?php echo $product['garancia_ertek']; ?></td>
                        <?php } ?>
                    <?php } ?>


                    <?php if ($this->config->get('termek_mennyisege')) { ?>
                        <td class="right">
                            <?php if ($product['quantity'] <= 0) { ?>
                                <span style="color: #FF0000;"><?php echo $product['quantity']; ?></span>
                            <?php } elseif ($product['quantity'] <= 5) { ?>
                                <span style="color: #FFA500;"><?php echo $product['quantity']; ?></span>
                            <?php } else { ?>
                                <span style="color: #008000;"><?php echo $product['quantity']; ?></span>
                            <?php } ?>
                        </td>
                    <?php } ?>

                    <?php if ($this->config->get('termek_statusza')) { ?>
                        <td class="left">
                            <span class="list_ertek" onclick="listElemValueModify(this)"><?php echo $product['status']; ?></span>

                            <span class="list_modify" style="display: none">
                             <select name="" modositas="<?php echo $product['status_event']?>" onchange="$(this).blur();">
                                 <?php if (!empty($product['status_value'])) { ?>
                                     <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                     <option value="0"><?php echo $text_disabled; ?></option>
                                 <?php } else { ?>
                                     <option value="1"><?php echo $text_enabled; ?></option>
                                     <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                 <?php } ?>
                             </select>
                        </span>

                        </td>
                    <?php } ?>

                    <td class="right">
                        <?php foreach ($product['action'] as $action) { ?>
                            [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                        <?php } ?>
                    </td>

                </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="8"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript">

$(document).ready(function(){
    setTimeout(" $('.warning, .success').slideUp(500)",5000);
    $('.date').datepicker({dateFormat: 'yy-mm-dd'});

    //$(".box > .content").css("width",$(".list").width()+"px");


});

    $(document).keyup(function(e){

        if(e.keyCode === 27)
            $(".export_, .import_").slideUp();

    });

function filter() {
	url = 'index.php?route=catalog/product&token=<?php echo $token; ?>';
    url += '&sort=<?php echo $sort; ?>';
    url += '&order=<?php echo $order; ?>';

    var filter_name = $('input[name=\'filter_name\']').attr('value');
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

    var filter_product_id = $('input[name=\'filter_product_id\']').attr('value');
    if (filter_product_id) {
        url += '&filter_product_id=' + encodeURIComponent(filter_product_id);
    }

    var filter_modify_date = $('input[name=\'filter_modify_date\']').attr('value');
	if (filter_modify_date) {
		url += '&filter_modify_date=' + encodeURIComponent(filter_modify_date);
	}

    var filter_date_added = $('input[name=\'filter_date_added\']').attr('value');
	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}

	var filter_model = $('input[name=\'filter_model\']').attr('value');
	if (filter_model) {
		url += '&filter_model=' + encodeURIComponent(filter_model);
	}
    var filter_cikkszam = $('input[name=\'filter_cikkszam\']').attr('value');
    if (filter_cikkszam) {
        url += '&filter_cikkszam=' + encodeURIComponent(filter_cikkszam);
    }
    var filter_cikkszam2 = $('input[name=\'filter_cikkszam2\']').attr('value');
    if (filter_cikkszam2) {
        url += '&filter_cikkszam2=' + encodeURIComponent(filter_cikkszam2);
    }
	
	var filter_price = $('input[name=\'filter_price\']').attr('value');
	if (filter_price) {
		url += '&filter_price=' + encodeURIComponent(filter_price);
	}

    if ($('select[name=\'filter_category\']').length > 0) {
        var filter_category = $('select[name=\'filter_category\']').attr('value');
        if (filter_category != '*') {
            url += '&filter_category=' + encodeURIComponent(filter_category);
        }
    }

	var filter_quantity = $('input[name=\'filter_quantity\']').attr('value');
	if (filter_quantity) {
		url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
	}

    if ($('select[name=\'filter_status\']').length > 0) {

        var filter_status = $('select[name=\'filter_status\']').attr('value');
        if (filter_status != '*') {
            url += '&filter_status=' + encodeURIComponent(filter_status);
        }
	}

    <?php if (!empty($sort)) { ?>
        url += '&sort=<?php echo $sort?>';
    <?php } ?>
    <?php if (!empty($order)) { ?>
        url += '&order=<?php echo $order?>';
    <?php } ?>

	location = url;
}
$('#form .filter input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
$('input[name=\'filter_name\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item.name,
						//value: item.product_id
						value: item.name
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_name\']').val(ui.item.label);
						
		return false;
	}
});

$('input[name=\'filter_model\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_model=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.model,
						value: item.model
						//value: item.product_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_model\']').val(ui.item.label);
						
		return false;
	}
});


    $('input[name=\'filter_cikkszam\']').autocomplete({
        delay: 0,
        source: function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_cikkszam=' +  encodeURIComponent(request.term),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item.cikkszam,
                            value: item.cikkszam
                            //value: item.product_id
                        }
                    }));
                }
            });
        },
        select: function(event, ui) {
            $('input[name=\'filter_cikkszam\']').val(ui.item.label);

            return false;
        }
    });

    $('input[name=\'filter_cikkszam2\']').autocomplete({
        delay: 0,
        source: function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_cikkszam2=' +  encodeURIComponent(request.term),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item.cikkszam2,
                            value: item.cikkszam2
                            //value: item.product_id
                        }
                    }));
                }
            });
        },
        select: function(event, ui) {
            $('input[name=\'filter_cikkszam2\']').val(ui.item.label);

            return false;
        }
    });

    $("input[name='modell_buttom']").bind("click",function(){
        $(".export_").slideUp(600);
        $(".modell_szuro").slideUp(700);

        var modell_szuro    =  $("input[name=\'modell_szuro\']").val();
        var csv_status      = $("select[name=\'csv_status\']").val();
        location ='index.php?route=catalog/product/csvExportAttribute&token=<?php echo $token; ?>&modell_szuro='+modell_szuro+'&csv_status='+csv_status;

    });

    $("input[name='modell_button_kapcsolodo']").bind("click",function(){
        $(".export_").slideUp(600);
        $(".modell_szuro").slideUp(700);

        var modell_szuro    = $("input[name=\'modell_szuro_kapcsolodo\']").val();
        var csv_status      = $("select[name=\'csv_status_kapcsolodo\']").val();
        var modell_szuro_termekek    = $("input[name=\'modell_szuro_kapcsolodo_termekek\']").val();
        var csv_status_termekek      = $("select[name=\'csv_status_kapcsolodo_termekek\']").val();
        location ='index.php?route=catalog/product/csvExportKapcsolodo&token=<?php echo $token; ?>&modell_szuro='+modell_szuro+'&csv_status='+csv_status+'&modell_szuro_termekek='+modell_szuro_termekek+'&csv_status_termekek='+csv_status_termekek;
    });

    $("input[name='modell_button_ajanlott']").bind("click",function(){
        $(".export_").slideUp(600);
        $(".modell_szuro").slideUp(700);

        var modell_szuro    = $("input[name=\'modell_szuro_ajanlott\']").val();
        var csv_status      = $("select[name=\'csv_status_ajanlott\']").val();
        var modell_szuro_ajanlott_termekek    = $("input[name=\'modell_szuro_ajanlott_termekek\']").val();
        var csv_status_ajanlott_termekek      = $("select[name=\'csv_status_ajanlott_termekek\']").val();
        location ='index.php?route=catalog/product/csvExportAjanlott&token=<?php echo $token; ?>&modell_szuro='+modell_szuro+'&csv_status='+csv_status+'&modell_szuro_ajanlott_termekek='+modell_szuro_ajanlott_termekek+'&csv_status_ajanlott_termekek='+csv_status_ajanlott_termekek;
    });

    $("input[name='modell_button_altalanos']").bind("click",function(){
        $(".export_").slideUp(600);
        $(".modell_szuro").slideUp(700);

        var modell_szuro    = $("input[name=\'modell_szuro_altalanos\']").val();
        var csv_status      = $("select[name=\'csv_status_altalanos\']").val();
        var modell_szuro_altalanos_termekek    = $("input[name=\'modell_szuro_altalanos_termekek\']").val();
        var csv_status_altalanos_termekek      = $("select[name=\'csv_status_altalanos_termekek\']").val();
        location ='index.php?route=catalog/product/csvExportAltalanos&token=<?php echo $token; ?>&modell_szuro='+modell_szuro+'&csv_status='+csv_status+'&modell_szuro_altalanos_termekek='+modell_szuro_altalanos_termekek+'&csv_status_altalanos_termekek='+csv_status_altalanos_termekek;
    });

    function kk(para1){
        var kiterjesztes=para1.value.substr(para1.value.length-3,3);

        if (kiterjesztes.toUpperCase() =="CSV"){
            $('#inputfeltolt').css("display","table");
        } else{
            alert("Csak CSV kiterjesztésű file tölthető fel!");
            $('#inputfeltolt').css("display","none");
        }
    }

    function csvBeolvas(para) {
        $('.import_').css("display","none");

        $.ajax({
         url: 'index.php?route=catalog/product/csvImport&token=<?php echo $token; ?>&csv_file='+para,
         dataType: 'json',
         type: 'post',
            success: function(json) {
                $('.success, .warning, .error').remove();

                if (json['error']) {
                    if ( json['error']) {
                        $('.breadcrumb').after('<div class="warning" style="display: none;">' + json['error'] + '</div>');
                        $('.warning').slideDown(500);
                        setTimeout( "$('.warning').slideUp(500)",4000);
                    }
                }

                if (json['success']) {
                    $('.breadcrumb').after('<div class="success" style="display: none;">' + json['success'] + '</div>');
                    $('.success').slideDown(500);
                    setTimeout( "$('.success').slideUp(500)",4000);

                }

                html = '<select name="csv_files"  onchange="this.value != \'0\' ? csvBeolvas(this.value) : \'\'" >';
                    html += '<option value="0" selected="selected"><?php echo $text_import_valasszon; ?></option>';
                    if (json['csv_files']) {
                        for(var i=0; json['csv_files'].length > i; i++) {
                            html +='<option value="'+json['csv_files'][i]+'" >'+json['csv_name'][i]+'</option>';

                        }
                    }
                html +='</select>';
                $('select[name="csv_files"]').html(html);
            },
            error: function(e) {
                debugger;

            }

         });

    }

    function oszlopBealit() {
        var url = 'index.php?route=catalog/product/oszlopok&token=<?php echo $token?>';

        oszlopok = [];
        $('.product_list_oszlop input:checked').each(function(element){
            oszlopok.push(this.name);
        });

        location='index.php?route=catalog/product/oszlopok&token=<?php echo $token?>'+'&oszlopok=' + oszlopok.join(',');

    }

    key_escape = false;
    aktualis_kivalaztott = '';
    eredet_ertek = '';
    aktualis_list_elem = '';

    $(document).ready(function(){
        uzenofal_torles = setTimeout(" $('.warning, .success').slideUp('slow')",5000);
    });

    $('.list_modify input, .list_modify select').focusout(function() {
        clearTimeout(uzenofal_torles);


        if (key_escape) {
            $('.warning, .success').slideUp('slow',function(){
                $('.success, .warning, .error').remove();
            });

            $(this).val(eredet_ertek);

            $(this).parent().slideUp("slow",function(){
                $(this).prev().css('display','block');
            });


        } else {
            if ($(this).prop('nodeName') == "SELECT") {
                var html = $(this).find('option:selected').html();

            } else {
                var html = this.value;

            }
            $(this).parent().prev().html(html);


            $(this).parent().slideUp("slow",function(){
                $(this).prev().css('display','block');

            });


            var eljaras = $(this).attr('modositas');
            var ertek   = this.value;

            if (eredet_ertek != this.value) {
                $.ajax({
                    url: 'index.php?route=catalog/product/modosit&token=<?php echo $token; ?>',
                    type: "post",
                    data: 'eljaras='+ eljaras+'&ertek='+ertek,

                    dataType: 'json',

                    success: function(json) {
                        $('.success, .warning, .error').remove();
                        if (json['success']) {
                            var html = '<div class="success" style="display: none;">'+json['success']+'</div>';
                            $('.breadcrumb').after(html);
                            $('.success').slideDown('slow');

                        }

                        if (json['error']) {
                            var html = '<div class="warning" style="display: none;">'+json['error']+'</div>';
                            $('.breadcrumb').after(html);
                            $('.warning').slideDown('slow');
                            $(aktualis_kivalaztott).val(eredet_ertek);
                            $(aktualis_list_elem).html(aktualis_list_ertek);
                        }
                        uzenofal_torles = setTimeout(" $('.warning, .success').slideUp('slow')",5000);

                    },
                    error: function(e) {
                    }


                });
            }
        }
    });

    function listElemValueModify(aktualis) {
        $('body').off('click');
        var td_width = $(aktualis).closest('td').width();
        if ($('.list_modify[style*="display: block"]').length == 0) {
            key_escape = false;
            $(aktualis).closest('td').width(td_width);

            $(aktualis).css('display','none');
            $('.list_modify').css('display','none');

            $(aktualis).next().slideDown('slow',function(){
                $(aktualis).next().children().focus();
            }).css("display","block");

            eredet_ertek = $(aktualis).next().children().val();
            aktualis_kivalaztott = $(aktualis).next().children();
            aktualis_list_elem = $(aktualis);
            aktualis_list_ertek = $(aktualis).html();

        } else {
            $('.list_modify').slideUp('slow',function(){
                $(this).prev().css('display','block');
            });
        }
    }
    $('body').on('click', function(e){
        var element_neve = $(e.target).prop('nodeName');
        if (element_neve != "SELECT" && element_neve != "OPTION" && element_neve != "INPUT" && $(e.target).attr('class') != 'list_ertek') {
            $('.list_modify').slideUp('slow',function(){
                $(this).prev().css('display','block');
            });
        }
    });

    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            key_escape = true;
            $(aktualis_kivalaztott).blur();

        } else if (e.keyCode == 13) {
            key_escape = false;
            $(aktualis_kivalaztott).blur();
        }
    });
</script>
<?php echo $footer; ?>