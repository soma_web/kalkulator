<?php echo $header; ?>
<div id="content" class="main-content">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li<?php echo ($breadcrumb['active']) ? ' class="active"' : ''; ?>><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>

  <div class="alerts">
    <div class="container" id="alerts">
      <?php foreach ($alerts as $type => $_alerts) { ?>
        <?php foreach ((array)$_alerts as $alert) { ?>
          <?php if ($alert) { ?>
      <div class="alert alert-<?php echo ($type == "error") ? "danger" : $type; ?> fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $alert; ?>
      </div>
          <?php } ?>
        <?php } ?>
      <?php } ?>
    </div>
  </div>

  <div class="navbar-placeholder">
    <nav class="navbar navbar-default" role="navigation" id="bull5i-navbar">
      <div class="nav-container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
            <span class="sr-only"><?php echo $text_toggle_navigation; ?></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <span class="navbar-brand"><i class="fa fa-question-circle fa-fw ext-icon"></i> <?php echo $heading_title; ?></span>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
          <div class="nav navbar-nav btn-group navbar-btn navbar-right">
            <button type="button" class="btn btn-default" id="apply" action="<?php echo $save; ?>"><?php echo $button_apply; ?></button>
            <button type="button" class="btn btn-default" id="save" action="<?php echo $save; ?>"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
            <a href="<?php echo $cancel; ?>" class="btn btn-default"><i class="fa fa-ban"></i> <?php echo $button_cancel; ?></a>
          </div>
        </div>
      </div>
    </nav>
  </div>

  <div class="bull5i-content bull5i-container">
    <div id="page-overlay" class="bull5i-overlay fade">
      <div class="page-overlay-progress"><i class="fa fa-refresh fa-spin fa-5x text-muted"></i></div>
    </div>

    <form action="<?php echo $save; ?>" method="post" enctype="multipart/form-data" id="beForm" class="form-horizontal" role="form">
      <div class="panel panel-default">
        <div class="panel-body">
          <fieldset>
            <?php if ($qa_id) { ?>
            <div class="row">
              <div class="col-sm-12 col-lg-6">
                <div class="form-group">
                  <label class="col-sm-3 col-md-2 col-lg-4 control-label" for="inputID"><?php echo $entry_id; ?></label>
                  <div class="col-sm-2 col-md-1 col-lg-3">
                    <input type="text" class="form-control uneditable-input" value="<?php echo $qa_id; ?>" id="inputID" disabled>
                    <input type="hidden" name="qa_id" value="<?php echo $qa_id; ?>">
                    <input type="hidden" name="store_id" value="<?php echo $store_id; ?>">
                  </div>
                </div>
              </div>
              <div class="col-sm-12 col-lg-6">
                <div class="form-group">
                  <label class="col-sm-3 col-md-2 col-lg-4 control-label" for="inputDateModified"><?php echo $entry_date_modified; ?></label>
                  <div class="col-sm-3 col-md-4 col-lg-8">
                    <input type="text" class="form-control uneditable-input" value="<?php echo $date_modified_formatted; ?>" id="inputDateModified" disabled>
                    <input type="hidden" name="date_modified" value="<?php echo $date_modified; ?>">
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 col-lg-6">
                <div class="form-group">
                  <label class="col-sm-3 col-md-2 col-lg-4 control-label" for="inputDateAsked"><?php echo $entry_date_asked; ?></label>
                  <div class="col-sm-3 col-md-4 col-lg-6">
                    <input type="text" class="form-control uneditable-input" value="<?php echo $date_asked_formatted; ?>" id="inputDateAsked" disabled>
                    <input type="hidden" name="date_asked" value="<?php echo $date_asked; ?>">
                  </div>
                </div>
              </div>
              <div class="col-sm-12 col-lg-6">
                <div class="form-group">
                  <label class="col-sm-3 col-md-2 col-lg-4 control-label" for="inputDateAnswered"><?php echo $entry_date_answered; ?></label>
                  <div class="col-sm-3 col-md-4 col-lg-8">
                    <input type="text" class="form-control uneditable-input" value="<?php echo $date_answered_formatted; ?>" id="inputDateAnswered" disabled>
                    <input type="hidden" name="date_answered" value="<?php echo $date_answered; ?>">
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
            <div class="row">
              <div class="col-sm-12 col-lg-6 col-lg-push-6">
                <?php if ($qa_id) { ?>
                <div class="form-group">
                  <div class="col-sm-offset-3 col-sm-3 col-md-offset-2 col-md-4 col-lg-offset-4 col-lg-8">
                    <div class="checkbox">
                      <label for="inputUpdateQuestionAnsweredDate">
                        <input type="checkbox" name="update_date_answered" value="1" id="inputUpdateQuestionAnsweredDate"<?php echo ($update_date_answered) ? ' checked' : ''; ?>> <?php echo $entry_update_date_answered; ?>
                      </label>
                    </div>
                  </div>
                </div>
                <?php } ?>
              </div>
              <div class="col-sm-12 col-lg-6 col-lg-pull-6">
                <div class="form-group<?php echo isset($errors['product_id']) ? ' has-error' : ''; ?>">
                  <label class="col-sm-3 col-md-2 col-lg-4 control-label input-required" for="inputProduct"><?php echo $entry_product; ?></label>
                  <div class="col-sm-3 col-lg-6">
                    <input type="text" name="product_name" class="form-control typeahead product id_based" value="<?php echo $product_name; ?>" id="inputProduct" data-target="product_id" placeholder="<?php echo $text_autocomplete; ?>">
                    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
                  </div>
                  <div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 col-lg-offset-4 col-lg-8 error-container" data-error-target="inputProduct">
                    <?php if (isset($errors['product_id'])) { ?>
                    <span class="help-block error-text"><?php echo $errors['product_id']; ?></span>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 col-lg-6">
                <div class="form-group">
                  <label class="col-sm-3 col-md-2 col-lg-4 control-label" for="selectStatus"><?php echo $entry_status; ?></label>
                  <div class="col-sm-3 col-md-2 col-lg-4">
                    <select name="status" id="selectStatus" class="form-control">
                      <option value="1"<?php echo ((int)$status) ? ' selected' : ''; ?>><?php echo $text_enabled; ?></option>
                      <option value="0"<?php echo (!(int)$status) ? ' selected' : ''; ?>><?php echo $text_disabled; ?></option>
                    </select>
                  </div>
                </div>
              </div>
              <?php if (!$multistore) { ?>
              <div class="col-sm-12 col-lg-6">
                <div class="form-group">
                  <div class="col-sm-offset-3 col-sm-3 col-md-offset-2 col-md-4 col-lg-offset-4 col-lg-8">
                    <div class="checkbox">
                      <label for="inputNotifyCustomer">
                        <input type="checkbox" name="notify_customer" value="1" id="inputNotifyCustomer"<?php echo ($notify_customer) ? ' checked' : ''; ?>> <?php echo $entry_notify; ?>
                      </label>
                      <span class="help-block"><?php echo $help_notify; ?></span>
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>
            </div>
            <?php if ($multistore) { ?>
            <div class="row">
              <div class="col-sm-12 col-lg-6">
                <div class="form-group">
                  <label class="col-sm-3 col-md-2 col-lg-4 control-label" for="selectStore"><?php echo $entry_stores; ?></label>
                  <div class="col-sm-4 col-lg-6">
                    <select name="question_stores[]" id="selectStore" class="form-control" multiple>
                      <?php foreach ($stores as $store_id => $store) { ?>
                      <option value="<?php echo $store_id; ?>"<?php echo (in_array($store_id, $question_stores)) ? ' selected': ''; ?>><?php echo $store['name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 col-lg-6">
                <div class="form-group">
                  <div class="col-sm-offset-3 col-sm-9 col-md-offset-2 col-md-10 col-lg-offset-4 col-lg-8">
                    <div class="checkbox">
                      <label for="inputNotifyCustomer">
                        <input type="checkbox" name="notify_customer" value="1" id="inputNotifyCustomer"<?php echo ($notify_customer) ? ' checked' : ''; ?>> <?php echo $entry_notify; ?>
                      </label>
                      <span class="help-block"><?php echo $help_notify; ?></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } else { ?>
              <?php foreach ($question_stores as $store_id) { ?>
            <input type="hidden" name="question_stores[]" value="<?php echo $store_id; ?>" />
              <?php } ?>
            <?php } ?>
          </fieldset>
          <fieldset>
            <legend><?php echo $text_question; ?></legend>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label class="col-sm-3 col-md-2 control-label" for="inputLanguage"><?php echo $entry_language; ?></label>
                  <div class="col-sm-3 col-md-3 fc-auto-width">
                    <select name="language_id" id="selectLanguage" class="form-control">
                      <?php foreach ($languages as $lang_id => $language) { ?>
                      <option value="<?php echo $lang_id; ?>"<?php echo ($lang_id == $language_id) ? ' selected': ''; ?>><?php echo $language['name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 col-lg-6">
                <div class="form-group">
                  <label class="col-sm-3 col-md-2 col-lg-4 control-label" for="inputQuestionAuthorName"><?php echo $entry_author_name; ?></label>
                  <div class="col-sm-3 col-md-2 col-lg-4">
                    <input type="text" name="question_author_name" class="form-control" value="<?php echo $question_author_name; ?>" id="inputQuestionAuthorName">
                    <input type="hidden" name="customer_id" value="<?php echo $customer_id; ?>">
                  </div>
                </div>
              </div>
              <div class="col-sm-12 col-lg-6">
                <div class="form-group">
                  <label class="col-sm-3 col-md-2 col-lg-4 control-label" for="inputCustomer"><?php echo $entry_customer; ?></label>
                  <div class="col-sm-3 col-lg-6">
                    <input type="text" name="customer_name" class="form-control typeahead customer" value="<?php echo $customer_name; ?>" id="inputCustomer" placeholder="<?php echo $text_autocomplete; ?>">
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group<?php echo isset($errors['question_author_email']) ? ' has-error' : ''; ?>">
                  <label class="col-sm-3 col-md-2 control-label" for="inputQuestionAuthorEmail"><?php echo $entry_email; ?></label>
                  <div class="col-sm-4 col-md-3">
                    <input type="text" name="question_author_email" class="form-control" value="<?php echo $question_author_email; ?>" id="inputQuestionAuthorEmail">
                  </div>
                  <div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 error-container" data-error-target="inputQuestionAuthorEmail">
                    <?php if (isset($errors['question_author_email'])) { ?>
                    <span class="help-block error-text"><?php echo $errors['question_author_email']; ?></span>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label class="col-sm-3 col-md-2 control-label" for="inputQuestionAuthorPhone"><?php echo $entry_phone; ?></label>
                  <div class="col-sm-3 col-md-2">
                    <input type="text" name="question_author_phone" class="form-control" value="<?php echo $question_author_phone; ?>" id="inputQuestionAuthorPhone">
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label class="col-sm-3 col-md-2 control-label" for="inputQuestionAuthorCustom"><?php echo $entry_custom; ?></label>
                  <div class="col-sm-4 col-md-3">
                    <input type="text" name="question_author_custom" class="form-control" value="<?php echo $question_author_custom; ?>" id="inputQuestionAuthorCustom">
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group<?php echo isset($errors['question']) ? ' has-error' : ''; ?>">
                  <label class="col-sm-3 col-md-2 control-label input-required" for="inputQuestion"><?php echo $entry_question; ?></label>
                  <div class="col-sm-9 col-md-10">
                    <textarea name="question" id="inputQuestion" class="form-control" rows="4"><?php echo $question; ?></textarea>
                  </div>
                  <div class="col-sm-offset-3 col-md-offset-2 col-sm-9 col-md-10 error-container" data-error-target="inputQuestion">
                    <?php if (isset($errors['question'])) { ?>
                    <span class="help-block error-text"><?php echo $errors['question']; ?></span>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
          </fieldset>
          <fieldset>
            <legend><?php echo $text_answer; ?></legend>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label class="col-sm-3 col-md-2 control-label" for="inputAnswerAuthorName"><?php echo $entry_answer_name; ?></label>
                  <div class="col-sm-3 col-md-2">
                    <input type="text" name="answer_author_name" class="form-control" value="<?php echo $answer_author_name; ?>" id="inputAnswerAuthorName">
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label class="col-sm-3 col-md-2 control-label" for="inputAnswer"><?php echo $entry_answer; ?></label>
                  <div class="col-sm-9 col-md-10">
                    <textarea name="answer" id="inputAnswer" class="form-control" rows="4"><?php echo $answer; ?></textarea>
                  </div>
                </div>
              </div>
            </div>
          </fieldset>
        </div>
      </div>
    </form>
  </div>
</div>
<script type="text/javascript"><!--
(function(bull5i,$,undefined){
bull5i.texts=$.extend({},bull5i.texts,{error_ajax_request:"<?php echo addslashes($error_ajax_request); ?>"});
<?php foreach ($typeahead as $type => $attr) { switch ($type) { case 'product': ?>$('.<?php echo $type; ?>.typeahead').typeahead({name:'<?php echo $type; ?>',<?php if (isset($attr['prefetch'])) { ?>prefetch:'<?php echo $attr['prefetch']; ?>',<?php } else if (isset($attr['remote'])) { ?>remote:'<?php echo $attr['remote']; ?>',<?php } ?>limit:10,template:['<p><span style="white-space:nowrap;">{{value}}</span></p>'].join(''),engine:Hogan});<?php break; case 'customer': ?>$('.<?php echo $type; ?>.typeahead').typeahead({name:'<?php echo $type; ?>',<?php if (isset($attr['prefetch'])) { ?>prefetch:'<?php echo $attr['prefetch']; ?>',<?php } else if (isset($attr['remote'])) { ?>remote:'<?php echo $attr['remote']; ?>',<?php } ?>limit:10,template:['<p><span style="white-space:nowrap;"><strong>{{value}}</strong><em style="padding-left:10px;">({{email}})</em></span></p>'].join(''),engine:Hogan});<?php break; default: break; } } ?>
$(".typeahead.input-sm").length&&$(".typeahead.input-sm").siblings("input.tt-hint").addClass("hint-small"),$(".typeahead.id_based").on("typeahead:selected",function(e,t,a){var o=$(this).data("target");$(this).data("ta-selected",t).data("ta-name",a),$(this).closest("div.form-group").find("input[type=hidden][name="+o+"]").val(t.id),$(this).val()!=t.value&&$(this).typeahead("setQuery",t.value)}).on("typeahead:autocompleted",function(e,t,a){var o=$(this).data("target");$(this).data("ta-selected",t).data("ta-name",a),$(this).closest("div.form-group").find("input[type=hidden][name="+o+"]").val(t.id),$(this).val()!=t.value&&$(this).typeahead("setQuery",t.value)}).on("typeahead:closed",function(){var e=$(this).data("target");void 0==$(this).data("ta-selected")?($(this).typeahead("setQuery",""),$(this).closest("div.form-group").find("input[type=hidden][name="+e+"]").val("")):$(this).val()!=$(this).data("ta-selected").value&&($(this).typeahead("setQuery",""),$(this).closest("div.form-group").find("input[type=hidden][name="+e+"]").val(""),$(this).removeData("ta-selected"),$(this).removeData("ta-name"))}),$(".typeahead.customer").on("typeahead:selected",function(e,t,a){$(this).data("ta-selected",t).data("ta-name",a),$("input[type=hidden][name=customer_id]").val(t.id),$("input[name=question_author_email]").val(t.email),$("input[name=question_author_name]").val(t.value),$(this).val()!=t.value&&$(this).typeahead("setQuery",t.value)}).on("typeahead:autocompleted",function(e,t,a){$(this).data("ta-selected",t).data("ta-name",a),$("input[type=hidden][name=customer_id]").val(t.id),$("input[name=question_author_email]").val(t.email),$("input[name=question_author_name]").val(t.value),$(this).val()!=t.value&&$(this).typeahead("setQuery",t.value)}).on("typeahead:closed",function(){void 0==$(this).data("ta-selected")?$("input[type=hidden][name=customer_id]").val(""):$(this).val()!=$(this).data("ta-selected").value&&($("input[type=hidden][name=customer_id]").val(""),$(this).removeData("ta-selected"),$(this).removeData("ta-name"))}),CKEDITOR.replace("question",{filebrowserBrowseUrl:"index.php?route=common/filemanager&token=<?php echo $token; ?>",filebrowserImageBrowseUrl:"index.php?route=common/filemanager&token=<?php echo $token; ?>",filebrowserFlashBrowseUrl:"index.php?route=common/filemanager&token=<?php echo $token; ?>",filebrowserUploadUrl:"index.php?route=common/filemanager&token=<?php echo $token; ?>",filebrowserImageUploadUrl:"index.php?route=common/filemanager&token=<?php echo $token; ?>",filebrowserFlashUploadUrl:"index.php?route=common/filemanager&token=<?php echo $token; ?>"}),CKEDITOR.replace("answer",{filebrowserBrowseUrl:"index.php?route=common/filemanager&token=<?php echo $token; ?>",filebrowserImageBrowseUrl:"index.php?route=common/filemanager&token=<?php echo $token; ?>",filebrowserFlashBrowseUrl:"index.php?route=common/filemanager&token=<?php echo $token; ?>",filebrowserUploadUrl:"index.php?route=common/filemanager&token=<?php echo $token; ?>",filebrowserImageUploadUrl:"index.php?route=common/filemanager&token=<?php echo $token; ?>",filebrowserFlashUploadUrl:"index.php?route=common/filemanager&token=<?php echo $token; ?>"});
}(window.bull5i=window.bull5i||{},jQuery));
//--></script>
<?php echo $footer; ?>
