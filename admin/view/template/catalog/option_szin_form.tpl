<?php echo $header; ?>

<style>
    .box .content table.szincsoport * {
        border: none;
    }
</style>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
          <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
          <a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
      </div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form" style="display: inline-table; width: auto; vertical-align: top;">
          <tr>
            <td><span class="required">*</span> <?php echo $entry_name; ?></td>

            <td>


                <?php foreach ($languages as $language) { ?>
                    <input type="text" name="option_szin_description[<?php echo $language['language_id']; ?>][name]"
                         value="<?php echo isset($option_szin_description[$language['language_id']]) ? $option_szin_description[$language['language_id']]['name'] : ''; ?>" />
                    <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />

                <?php } ?>

            </td>
          </tr>

            <tr>
                <td><?php echo $entry_szinkod; ?></td>
                <td><input type="text" name="szinkod" value="<?php echo $szinkod; ?>" size="30" /></td>
            </tr>

            <tr>
                <td><?php echo $entry_azonosito; ?></td>
                <td><input type="text" name="azonosito" value="<?php echo $azonosito; ?>" size="13" readonly/></td>
            </tr>

          <tr>
            <td><?php echo $entry_sort_order; ?></td>
            <td><input type="text" name="sort_order" value="<?php echo $sort_order; ?>" size="1" /></td>
          </tr>
        </table>

          <table class="form szincsoport" style="display: inline-table; width: auto; vertical-align: top;">
              <tr>
                  <td style="text-align: right"><?php echo $entry_szin_group; ?></td>
                  <td style="border: 1px solid #ddd;"><div class="scrollbox">
                          <?php $class = 'even'; ?>
                          <!--<div class="<?php echo $class; ?>">
                              <?php if (in_array(0, $szin_group)) { ?>
                                  <input type="checkbox" name="szin_group[]" value="0" checked="checked" />
                                  <?php echo $text_default; ?>
                              <?php } else { ?>
                                  <input type="checkbox" name="szin_group[]" value="0" />
                                  <?php echo $text_default; ?>
                              <?php } ?>
                          </div>-->

                          <?php foreach ($groups as $group) { ?>
                              <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                              <div class="<?php echo $class; ?>">
                                  <?php if (in_array($group['option_szin_group_id'], $szin_group)) { ?>
                                      <input type="checkbox" name="szin_group[]" value="<?php echo $group['option_szin_group_id']; ?>" checked="checked" />
                                      <?php echo $group['name']; ?>
                                  <?php } else { ?>
                                      <input type="checkbox" name="szin_group[]" value="<?php echo $group['option_szin_group_id']; ?>" />
                                      <?php echo $group['name']; ?>
                                  <?php } ?>
                              </div>
                          <?php } ?>
                      </div>
                  </td>
              </tr>
        </table>

      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>