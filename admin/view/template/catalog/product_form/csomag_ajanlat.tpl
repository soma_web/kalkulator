<tr>
    <td><?php echo $entry_package; ?></td>
    <td><input type="text" name="package_quantity" value="1" size="2" /> x
        Termék neve: <input type="text" name="package" value="" />
        Model: <input type="text" name="package_model" value="" />
    </td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>

        <table id="product-package" style="display: <?php echo $product_packages ? '' : 'none'?>">
            <thead>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><?php echo $entry_package_alapar?></td>
                    <td><?php echo $entry_package_csomagar?></td>
                    <td><?php echo $entry_package_csomagar_ossz?></td>
                    <td></td>

                </tr>
            </thead>
            <tbody>
                <?php $class = 'odd'; ?>
                <?php foreach ($product_packages as $product_package) { ?>
                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                    <tr id="product-package<?php echo $product_package['package_id']; ?>" class="<?php echo $class; ?> package_row">

                        <td class="<?php echo $class; ?>">
                            <input type="number" name="product_package[<?php echo $product_package['package_id']; ?>][quantity]" value="<?php echo $product_package['quantity']; ?>"
                               class="package_quantity" onchange="packagePriceCalc('<?php echo $product_package['package_id']; ?>')"/> x
                        </td>

                        <td class="<?php echo $class; ?>">
                            <span class="kapcsolodo_model"><?php echo (isset($product_package['model']) && $product_package['model']) ? $product_package['model']." - " : ""; ?></span>
                        </td>

                        <td class="<?php echo $class; ?>">
                            <span class="package_product_name"><?php echo $product_package['name']; ?></span>
                        </td>

                        <td class="<?php echo $class; ?>" style="text-align: right">
                            <span class="price_format"><?php echo ' '.$product_package['price_format'] ?></span>
                        </td>


                        <td class="<?php echo $class; ?>" style="text-align: right">
                            <span class="price_format">
                                <input type="text" name="product_package[<?php echo $product_package['package_id']; ?>][price]" value="<?php echo $product_package['package_price'] ?>"
                                       class="package_price" onchange="packagePriceCalc('<?php echo $product_package['package_id']; ?>')">
                                <?php echo $penznem?>
                            </span>
                        </td>

                        <td class="<?php echo $class; ?>" style="text-align: right">
                            <span class="price_format" id="package_price_row_format_<?php echo $product_package['package_id']?>"><?php echo $product_package['package_price_row_format'] ?></span>
                        </td>

                        <td class="<?php echo $class;?>">
                            <img src="view/image/delete.png" />
                        </td>
                        <input type="hidden" class="csomag_tetel_ar" value="<?php echo $product_package['price'] ?>" />
                        <input type="hidden" name="product_package[<?php echo $product_package['package_id']; ?>][package_id]" value="<?php echo $product_package['package_id']; ?>" class="package_products"/>
                    </tr>

                <?php } ?>
            </tbody>

            <tfoot>
                <tr id="cs_osszesito">
                    <td colspan="5" class="right">
                        <span class="cs_name"><?php echo $entry_csomagar_ossz; ?></span>
                    </td>
                    <td class="right bold">
                        <span class="cs_price"><?php echo $package_price_all_format; ?></span>
                    </td>
                    <td></td>
                </tr>
            </tfoot>
        </table>

    </td>
</tr>

<script>
    price_rows = 0;

    $(document).ready(function(){
        packagePriceAll();

    });

    $('input[name=\'package\'], input[name=\'package_model\']').bind('click', function(){ $(this).autocomplete("search"); } );

    $('input[name=\'package\']').autocomplete({
        delay: 0,
        source: function(request, response) {
            package_products = [];
            $('.package_products').each(function(element) {
                package_products.push(this.value);
            });

            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&product_id=<?php echo $_REQUEST['product_id'];?>&filter_name=' +  encodeURIComponent(request.term)+'&package_products='+package_products,
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            name: item.name,
                            value: item.name,
                            ertek: item.product_id,
                            model: item.model,
                            price: item.price,
                            price_format: item.price_format,
                            package_price: item.package_price,
                            package_price_format: item.package_price_format,
                            package_price_row:    item.package_price_row,
                            package_price_row_format: item.package_price_row_format

                        }
                    }));
                }
            });

        },
        select: function(event, ui) {
            $('input[name=\'package\']').blur();

            $('#product-package' + ui.item.ertek).remove();

            var quantity = $('input[name=\'package_quantity\']').val();

            var html = '';
            html += '<tr id="product-package' + ui.item.ertek + '" class="package_row">';

                html += '<td>';
                    html += '<input type="number" name="product_package['+ui.item.ertek+'][quantity]" value="'+quantity+'"';
                        html += 'class="package_quantity" onchange="packagePriceCalc(\''+ui.item.ertek+'\')"/> x';
                html += '</td>';

                html += '<td>';

                    if (ui.item.model) {
                        html += '<span class="kapcsolodo_model">';
                        html += ui.item.model;
                        html += " - ";
                        html += '</span>';
                    }
                html += '</td>';


                html += '<td>';
                    html += '<span class="package_product_name">'+ui.item.name+'</span>';
                html += '</td>';

                html += '<td style="text-align: right">';
                    html += '<span class="price_format">'+ui.item.price_format+'</span>';
                html += '</td>';


                html += '<td style="text-align: right">';
                    html += '<span class="price_format">';
                        html += '<input type="text" name="product_package['+ui.item.ertek+'][price]" value="'+ui.item.package_price+'"';
                            html += 'class="package_price" onchange="packagePriceCalc(\''+ui.item.ertek+'\')">';
                        html += '<?php echo $penznem?>';
                    html += '</span>';
                html += '</td>';

                html += '<td style="text-align: right">';
                    html += '<span class="price_format" id="package_price_row_format_'+ui.item.ertek+'">'+ui.item.package_price_row_format+'</span>';
                html += '</td>';

                html += '<td>';
                    html += '<img src="view/image/delete.png" />';
                html += '</td>';
                html += '<input type="hidden" class="csomag_tetel_ar" value="'+ui.item.price+'" />';
                html += '<input type="hidden" name="product_package[' + ui.item.ertek + '][package_id]" value="' + ui.item.ertek + '" class="package_products"/>';
            html += '</tr>';

            $('#product-package tbody').append(html);

            $('#product-package tbody tr:odd td').attr('class', 'odd');
            $('#product-package tbody tr:even td').attr('class', 'even');
            packagePriceCalc(ui.item.ertek);

            return false;
        }
    });

    $('input[name=\'package_model\']').autocomplete({
        delay: 0,
        source: function(request, response) {
            package_products = [];
            $('.package_products').each(function(element) {
                package_products.push(this.value);
            });

            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&product_id=<?php echo $_REQUEST['product_id'];?>&filter_model=' +  encodeURIComponent(request.term)+'&package_products='+package_products,
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            name: item.name,
                            value: item.model,
                            ertek: item.product_id,
                            model: item.model,

                            price: item.price,
                            price_format: item.price_format,
                            package_price: item.package_price,
                            package_price_format: item.package_price_format,
                            package_price_row:    item.package_price_row,
                            package_price_row_format: item.package_price_row_format

                        }
                    }));
                }
            });

        },
        select: function(event, ui) {
            $('input[name=\'package_model\']').blur();

            $('#product-package' + ui.item.ertek).remove();

            var quantity = $('input[name=\'package_quantity\']').val();


            var html = '';
            html += '<tr id="product-package' + ui.item.ertek + '" class="package_row">';

                html += '<td>';
                html += '<input type="number" name="product_package['+ui.item.ertek+'][quantity]" value="'+quantity+'"';
                html += 'class="package_quantity" onchange="packagePriceCalc(\''+ui.item.ertek+'\')"/> x';
                html += '</td>';

                html += '<td>';

                if (ui.item.model) {
                    html += '<span class="kapcsolodo_model">';
                    html += ui.item.model;
                    html += " - ";
                    html += '</span>';
                }
                html += '</td>';


                html += '<td>';
                    html += '<span class="package_product_name">'+ui.item.name+'</span>';
                html += '</td>';

                html += '<td style="text-align: right">';
                    html += '<span class="price_format">'+ui.item.price_format+'</span>';
                html += '</td>';


                html += '<td style="text-align: right">';
                    html += '<span class="price_format">';
                        html += '<input type="text" name="product_package['+ui.item.ertek+'][price]" value="'+ui.item.package_price+'"';
                        html += 'class="package_price" onchange="packagePriceCalc(\''+ui.item.ertek+'\')">';
                        html += '<?php echo $penznem?>';
                    html += '</span>';
                html += '</td>';

                html += '<td style="text-align: right">';
                    html += '<span class="price_format" id="package_price_row_format_'+ui.item.ertek+'">'+ui.item.package_price_row_format+'</span>';
                html += '</td>';

                html += '<td>';
                    html += '<img src="view/image/delete.png" />';
                html += '</td>';
                html += '<input type="hidden" class="csomag_tetel_ar" value="'+ui.item.price+'" />';
                html += '<input type="hidden" name="product_package[' + ui.item.ertek + '][package_id]" value="' + ui.item.ertek + '" class="package_products"/>';
            html += '</tr>';


            $('#product-package tbody').append(html);

            $('#product-package tbody tr:odd td').attr('class', 'odd');
            $('#product-package tbody tr:even td').attr('class', 'even');

            packagePriceCalc(ui.item.ertek);

            return false;
        }
    });



    function packagePriceCalc(product_id) {

        if (product_id) {
            var price_row = $('input[name="product_package[' + product_id + '][price]"]').val() * $('input[name="product_package[' + product_id + '][quantity]"]').val();
            price_row = number_format(price_row) + "<?php echo $penznem?>";
            $('#package_price_row_format_'+product_id).html(price_row);
        }
        packagePriceAll();
        $('.cs_price').html(number_format(price_rows)+"<?php echo $penznem?>");
    }

    function packagePriceAll() {
        price_rows = 0;
        $('.package_row').each(function(e){
            var mennyiseg = $(this).find('.package_quantity').val();
            var ertek = $(this).find('.package_price').val();
            price_rows += mennyiseg*ertek;
        });

        if ($('.package_row').length > 0) {
            $('#product-package').fadeIn();
        } else {
            $('#product-package').fadeOut();

        }

        $('.cs_price').html(number_format(price_rows)+"<?php echo $penznem?>");

        if ($('input[name="price"]').val() != price_rows) {
            priceButtonAktualizal(true);
        } else {
            priceButtonAktualizal(false);
        }

    }

    function priceButtonAktualizal(para) {
        if (para && price_rows > 0) {
            if ($('#cs_aktualizalas').length == 0) {
                html = '';
                html += '<tr>';
                    html += '<td colspan="7" class="right">';
                        html += '<input type="button" id="cs_aktualizalas" value="<?php echo $entry_csomagar_akutalizal; ?>" />';
                    html += '</td>';
                html += '</tr>';
                $('#product-package tfoot tr').after(html);
            }
        } else {
            if ($('#cs_aktualizalas').length > 0) {
                $('#cs_aktualizalas').parent().parent().remove();
            }
        }
    }

    $('#product-package').on('click', 'img', function() {
        $(this).parent().parent().remove();

        $('#product-package tbody tr:odd td').attr('class', 'odd');
        $('#product-package tbody tr:even td').attr('class', 'even');
        packagePriceAll();

    });

    $('#product-package').on('click', '#cs_aktualizalas', function() {
        $('input[name="price"]').val(price_rows);
        packagePriceAll();

    });



</script>