<?php  echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/product.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
          <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
          <a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
      </div>
    </div>
    <div class="content">
      <div id="tabs" class="htabs">
          <a href="#tab-general"><?php echo $tab_general; ?></a>
          <a href="#tab-data"><?php echo $tab_data; ?></a>
          <a href="#tab-links"><?php echo $tab_links; ?></a>
          <?php if ( $megjelenit_termekful['tulajdonsag_ful'] == 0) { ?>
            <a href="#tab-attribute"><?php echo $tab_attribute; ?></a>
          <? }?>
          <?php if ( $megjelenit_termekful['valasztek_ful'] == 0) { ?>
              <a href="#tab-option"><?php echo $tab_option; ?></a>
          <? }?>
          <?php if ( $megjelenit_termekful['mennyisegi_ful'] == 0) { ?>
            <a href="#tab-discount"><?php echo $tab_discount; ?></a>
          <? }?>
          <?php if ( $megjelenit_termekful['vevo_ful'] == 0) { ?>
              <a href="#tab-vevok"><?php echo $tab_vevok; ?></a>
          <? }?>
          <?php if ( $megjelenit_termekful['akcios_ful'] == 0) { ?>
              <a href="#tab-special"><?php echo $tab_special; ?></a>
          <? }?>
          <?php if ( $megjelenit_termekful['kep_ful'] == 0) { ?>
              <a href="#tab-image"><?php echo $tab_image; ?></a>
          <? }?>
          <?php if ( $megjelenit_termekful['jutalom_ful'] == 0) { ?>
          <a href="#tab-reward"><?php echo $tab_reward; ?></a>
          <? }?>
          <?php if ( $megjelenit_termekful['elhelyezkedes'] == 0) { ?>
              <a href="#tab-elhelyezkedes"><?php echo $tab_elhelyezkedes; ?></a>
          <?php } ?>
      </div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <? if (file_exists(DIR_TEMPLATE."catalog/vatera.tpl") && $this->config->get('megjelenit_vatera') == 1){
          require_once(DIR_TEMPLATE."catalog/vatera.tpl"); ?>
      <? }?>

        <div id="tab-general">
          <div id="languages" class="htabs">
            <?php foreach ($languages as $language) { ?>
            <a href="#language<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
            <?php } ?>
          </div>

          <?php foreach ($languages as $language) { ?>
          <div id="language<?php echo $language['language_id']; ?>">
            <table class="form">
              <tr>
                <td><span class="required">*</span> <?php echo $entry_name; ?></td>
                <td><input type="text" name="product_description[<?php echo $language['language_id']; ?>][name]" size="100" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['name'] : ''; ?>" />
                  <?php if (isset($error_name[$language['language_id']])) { ?>
                  <span class="error"><?php echo $error_name[$language['language_id']]; ?></span>
                  <?php } ?></td>
              </tr>

              <tr>
                <td><?php echo $entry_meta_keyword; ?></td>
                <td><textarea name="product_description[<?php echo $language['language_id']; ?>][meta_keyword]" cols="40" rows="5"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_keyword'] : ''; ?></textarea></td>
              </tr>
                <tr>
                    <td><?php echo $entry_meta_description; ?></td>
                    <td><textarea name="product_description[<?php echo $language['language_id']; ?>][meta_description]" cols="40" rows="5"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_description'] : ''; ?></textarea></td>
                </tr>
              <tr>
                <td><?php echo $entry_description; ?></td>
                <td><textarea name="product_description[<?php echo $language['language_id']; ?>][description]" id="description<?php echo $language['language_id']; ?>"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['description'] : ''; ?></textarea></td>
              </tr>
              <tr>
                <td><?php echo $entry_tag; ?></td>
                <td><input type="text" name="product_tag[<?php echo $language['language_id']; ?>]" value="<?php echo isset($product_tag[$language['language_id']]) ? $product_tag[$language['language_id']] : ''; ?>" size="80" /></td>
              </tr>
            </table>
          </div>
          <?php } ?>


        </div>
        <div id="tab-data">
          <table class="form">
              <?php if ($this->config->get('megjelenit_form_admin_model') == 1 ) { ?>
                  <tr>
                      <td><!--<span class="required">*</span>--> <?php echo $entry_model; ?></td>
                      <td><input type="text" name="model" value="<?php echo $model; ?>" />
                          <?php if ($error_model) { ?>
                             <span class="error"><?php echo $error_model; ?></span>
                         <?php } ?>
                      </td>
                  </tr>
              <?php } ?>

              <?php if ($this->config->get('megjelenit_form_admin_cikkszam') == 1 ) { ?>
                  <tr>
                      <td><?php echo $entry_cikkszam; ?></td>
                      <td><input type="text" name="cikkszam" value="<?php echo $cikkszam; ?>" /></td>
                  </tr>
              <?php } ?>

              <?php if ($this->config->get('megjelenit_form_admin_cikkszam2') == 1 ) { ?>
                  <tr>
                      <td><?php echo $entry_cikkszam2; ?></td>
                      <td><input type="text" name="cikkszam2" value="<?php echo $cikkszam2; ?>" /></td>
                  </tr>
              <?php } ?>

              <tr>
                  <td><?php echo $entry_biztonsagi_kod; ?></td>
                  <td>
                      <?php if(isset($correctsecuritycode) && $correctsecuritycode == '1') { ?>
                          <input type="checkbox" checked="checked" name="correctsecuritycode" value="1" />
                      <?php } else { ?>
                          <input type="checkbox" name="correctsecuritycode" value="1" />
                      <?php } ?>
                  </td>
              </tr>
              <tr>
              <td><?php echo $entry_status; ?></td>
              <td><select name="status">
                <?php if ($status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
                 </select></td>
              </tr>
            <tr>
              <td><?php echo $entry_sku; ?></td>
              <td><input type="text" name="sku" value="<?php echo $sku; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_upc; ?></td>
              <td><input type="text" name="upc" value="<?php echo $upc; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_location; ?></td>
              <td><input type="text" name="location" value="<?php echo $location; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_price; ?></td>
              <td><input type="text" name="price" value="<?php echo $price; ?>" /></td>
            </tr>


            <tr>
                <td><?php echo $entry_szazalek; ?></td>
                <td>
                <?php if ($szazalek == 1) { ?>
                    <input type="radio" name="szazalek" value="1" checked="checked"  class="frontend_szazalek"/>
                    <?php echo $text_yes; ?>
                    <input type="radio" name="szazalek" value="0" class="frontend_szazalek"/>
                    <?php echo $text_no; ?>
                <?php } else { ?>
                    <input type="radio" name="szazalek" value="1" class="frontend_szazalek"/>
                    <?php echo $text_yes; ?>
                    <input type="radio" name="szazalek" value="0" checked="checked" class="frontend_szazalek"/>
                    <?php echo $text_no; ?>
                <?php } ?>
                </td>
            </tr>

            <?php if($this->config->get('megjelenit_form_admin_eredeti_ar')) { ?>
                <tr>
                    <td><?php echo $entry_eredeti_ar; ?></td>
                    <td><input type="text" name="eredeti_ar" value="<?php echo $eredeti_ar; ?>" /></td>
                </tr>
            <?php } ?>

            <?php if($this->config->get('megjelenit_form_admin_utalvany')) { ?>
                  <tr>
                      <td><?php echo $entry_utalvany; ?></td>
                      <td>
                          <?php if ($utalvany == 1) { ?>
                              <input type="radio" name="utalvany" value="1" checked="checked"/>
                              <?php echo $text_yes; ?>
                              <input type="radio" name="utalvany" value="0"/>
                              <?php echo $text_no; ?>
                          <?php } else { ?>
                              <input type="radio" name="utalvany" value="1"/>
                              <?php echo $text_yes; ?>
                              <input type="radio" name="utalvany" value="0" checked="checked"/>
                              <?php echo $text_no; ?>
                          <?php } ?>
                      </td>
                  </tr>
            <?php } ?>


            <tr>
              <td><?php echo $entry_tax_class; ?></td>
              <td><select name="tax_class_id">
                  <option value="0"><?php echo $text_none; ?></option>
                  <?php foreach ($tax_classes as $tax_class) { ?>
                  <?php if ($tax_class['tax_class_id'] == $tax_class_id) { ?>
                  <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
            </tr>
            <tr>
              <td><?php echo $entry_quantity; ?></td>
              <td><input type="text" name="quantity" value="<?php echo $quantity; ?>" size="4" /></td>
            </tr>
            <tr>
              <td><?php echo $mennyisegi_egyseg; ?></td>
              <td><input type="text" name="megyseg" value="<?php echo $megyseg; ?>" size="10" /></td>
            </tr>
            <?php if($this->config->get('megjelenit_csomagolas_admin') == 1){ ?>
                <tr>
                    <td><?php echo $csomagolasi_egyseg; ?></td>
                    <td><input type="text" name="csom_egyseg" value="<?php echo $csom_egyseg ?>" size="10" /></td>
                </tr>

                <tr>
                  <td><?php echo $csomagolasi_mennyiseg; ?></td>
                    <td><input type="text" name="csom_mennyiseg" value="<?php echo $csom_mennyiseg ?>" size="10" /></td>
                </tr>
            <?php }?>

            <tr>
              <td><?php echo $entry_minimum; ?></td>
              <td><input type="text" name="minimum" value="<?php echo $minimum; ?>" size="2" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_subtract; ?></td>
              <td><select name="subtract">
                  <?php if ($subtract) { ?>
                  <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                  <option value="0"><?php echo $text_no; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_yes; ?></option>
                  <option value="0" selected="selected"><?php echo $text_no; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
            <?php if($this->config->get('megjelenit_meddig_kaphato')) { ?>
              <tr>
                  <td><?php echo $entry_date_kaphato_ig; ?></td>
                  <td><input type="text" name="date_ervenyes_ig" value="<?php echo $date_ervenyes_ig; ?>" size="12" class="date" /></td>
              </tr>
            <?php }?>
            <tr>
              <td><?php echo $entry_stock_status; ?></td>
              <td><select name="stock_status_id">
                  <?php foreach ($stock_statuses as $stock_status) { ?>
                  <?php if ($stock_status['stock_status_id'] == $stock_status_id) { ?>
                  <option value="<?php echo $stock_status['stock_status_id']; ?>" selected="selected"><?php echo $stock_status['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $stock_status['stock_status_id']; ?>"><?php echo $stock_status['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
            </tr>
            <tr>
              <td><?php echo $entry_shipping; ?></td>
              <td><?php if ($shipping) { ?>
                <input type="radio" name="shipping" value="1" checked="checked" />
                <?php echo $text_yes; ?>
                <input type="radio" name="shipping" value="0" />
                <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="shipping" value="1" />
                <?php echo $text_yes; ?>
                <input type="radio" name="shipping" value="0" checked="checked" />
                <?php echo $text_no; ?>
                <?php } ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_keyword; ?></td>
              <td><input type="text" name="keyword" value="<?php echo $keyword; ?>" /></td>
            </tr>

            <tr>
              <td><?php echo $entry_image; ?></td>
              <td><div class="image"><img src="<?php echo $thumb; ?>" alt="" id="thumb" /><br />
                  <input type="hidden" name="image" value="<?php echo $image; ?>" id="image" />
                  <a onclick="image_upload('image', 'thumb');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
            </tr>

            <tr>
              <td><?php echo $entry_letoltes; ?></td>
              <td>
                  <div class="image" style="text-align:left; margin-right: 35px;" id="kupon_nezegeto">
                      <a href="<?php echo $letoltheto_popup; ?>"  class="colorbox" rel="colorbox" >
                          <img style="width: 100px" src="<?php echo $letoltheto_thumb; ?>" alt="" id="letoltheto_thumb" /><br />
                          <input type="hidden" name="letoltheto_kep" value="<?php echo $letoltheto_kep; ?>" id="letoltheto_kep" />
                      </a>
                      <a onclick="image_upload('letoltheto_kep', 'letoltheto_thumb');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#letoltheto_thumb').attr('src', '<?php echo $no_image; ?>'); $('#letoltheto_kep').attr('value', '');"><?php echo $text_clear; ?></a>
                  </div>
                  <!--<a class="button" onclick="kuponKeszit()"><?php echo $text_kupon_keszit; ?></a>-->
              </td>

            </tr>

            <?php if($this->config->get('megjelenit_mikortol_kaphato')) { ?>
                <tr>
                  <td><?php echo $entry_date_available; ?></td>
                  <td><input type="text" name="date_available" value="<?php echo $date_available; ?>" size="12" class="date" /></td>
                </tr>
            <?php }?>
            <tr>
              <td><?php echo $entry_dimension; ?></td>
              <td><input type="text" name="length" value="<?php echo $length; ?>" size="4" />
                <input type="text" name="width" value="<?php echo $width; ?>" size="4" />
                <input type="text" name="height" value="<?php echo $height; ?>" size="4" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_length; ?></td>
              <td><select name="length_class_id">
                  <?php foreach ($length_classes as $length_class) { ?>
                  <?php if ($length_class['length_class_id'] == $length_class_id) { ?>
                  <option value="<?php echo $length_class['length_class_id']; ?>" selected="selected"><?php echo $length_class['title']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $length_class['length_class_id']; ?>"><?php echo $length_class['title']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
            </tr>
            <tr>
              <td><?php echo $entry_weight; ?></td>
              <td><input type="text" name="weight" value="<?php echo $weight; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_weight_class; ?></td>
              <td><select name="weight_class_id">
                  <?php foreach ($weight_classes as $weight_class) { ?>
                  <?php if ($weight_class['weight_class_id'] == $weight_class_id) { ?>
                  <option value="<?php echo $weight_class['weight_class_id']; ?>" selected="selected"><?php echo $weight_class['title']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $weight_class['weight_class_id']; ?>"><?php echo $weight_class['title']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
            </tr>
            <tr>
              <td><?php echo $entry_sort_order; ?></td>
              <td><input type="text" name="sort_order" value="<?php echo $sort_order; ?>" size="2" /></td>
            </tr>
          </table>
        </div>


        <div id="tab-links">
          <table class="form">
            <tr>
              <td><?php echo $entry_manufacturer; ?></td>
              <td><select name="manufacturer_id">
                  <option value="0" selected="selected"><?php echo $text_none; ?></option>
                  <?php foreach ($manufacturers as $manufacturer) { ?>
                  <?php if ($manufacturer['manufacturer_id'] == $manufacturer_id) { ?>
                  <option value="<?php echo $manufacturer['manufacturer_id']; ?>" selected="selected"><?php echo $manufacturer['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $manufacturer['manufacturer_id']; ?>"><?php echo $manufacturer['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
            </tr>


              <tr>
                  <td><?php echo $entry_category; ?></td>
                  <td><div class="scrollbox">
                          <?php $class = 'odd'; ?>
                          <?php foreach ($categories as $category) { ?>
                              <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                              <div class="<?php echo $class; ?>">
                                  <?php if (in_array($category['category_id'], $product_category)) { ?>
                                      <input type="checkbox" name="product_category[]" value="<?php echo $category['category_id']; ?>" checked="checked" />
                                      <?php echo $category['name']; ?>
                                  <?php } else { ?>
                                      <input type="checkbox" name="product_category[]" value="<?php echo $category['category_id']; ?>" />
                                      <?php echo $category['name']; ?>
                                  <?php } ?>
                              </div>
                          <?php } ?>
                      </div>
                      <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a></td>
              </tr>

              <tr>
                  <td><?php echo $entry_filter; ?></td>
                  <td>
                      <div style="float: left">
                          Rendezettség: </br></br>
                          <a id="varos_rendez_valaszt" class="kiemel" onclick="varosRendez()"><?php echo 'Város szerint'; ?></a> /
                          <a id="uzlet_rendez_valaszt" onclick="uzletRendez()"><?php echo 'Üzlet szerint'; ?></a>
                          </br>
                          </br>
                          <div id="varos_rendez">
                              <div class="scrollbox">
                                  <?php $class = 'odd'; ?>
                                  <?php foreach ($szurok as $szuro) { ?>
                                      <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                      <div class="<?php echo $class; ?>">
                                          <?php if (in_array($szuro, $product_filters)) { ?>
                                              <input type="checkbox" name="product_filter[]" value="<?php echo $szuro['filter_id']; ?>"
                                                     select_id="<?php echo $szuro['filter_select_id']; ?>" checked="checked" onclick="checkRaad(this)"/>
                                              <?php echo $szuro['name']; ?>
                                          <?php } else { ?>
                                              <input type="checkbox" name="product_filter[]" value="<?php echo $szuro['filter_id']; ?>"
                                                     select_id="<?php echo $szuro['filter_select_id']; ?>" onclick="checkRaad(this)"/>
                                              <?php echo $szuro['name']; ?>
                                          <?php } ?>
                                      </div>
                                  <?php } ?>
                              </div>
                          </div>

                          <div id="uzlet_rendez" style="display: none">
                              <div class="scrollbox">
                                  <?php $class = 'odd'; ?>
                                  <?php foreach ($szurok2 as $szuro) { ?>
                                      <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                      <div class="<?php echo $class; ?>">
                                          <?php if (in_array($szuro, $product_filters)) { ?>
                                              <input type="checkbox" name="product_filter2[]" value="<?php echo $szuro['filter_id']; ?>"
                                                     select_id="<?php echo $szuro['filter_select_id']; ?>" checked="checked" onclick="checkRaad(this)"/>
                                              <?php echo $szuro['name']; ?>
                                          <?php } else { ?>
                                              <input type="checkbox" name="product_filter2[]" value="<?php echo $szuro['filter_id']; ?>"
                                                     select_id="<?php echo $szuro['filter_select_id']; ?>" onclick="checkRaad(this)"/>
                                              <?php echo $szuro['name']; ?>
                                          <?php } ?>
                                      </div>
                                  <?php } ?>
                              </div>
                          </div>
                          </br>
                          <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a>
                          </br>
                      </div>
                      <div style="float: left">
                          <span style="margin-left: 20px">Csoportos kijelölés</span>
                          </br></br></br></br>
                          <div class="scrollbox" style="width: auto; margin-left: 20px;" >
                              <?php $class = 'odd'; ?>
                              <?php foreach ($filter_selects as $filter_select) { ?>
                                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                  <div class="<?php echo $class; ?>">
                                      <?php if (in_array($filter_select, $product_filters)) { ?>
                                          <input type="checkbox" name="product_filter_select[]" value="<?php echo $filter_select['filter_select_id']; ?>"
                                                 checked="checked" onclick="selectValaszt(this)"/>
                                          <?php echo $filter_select['name']; ?>
                                      <?php } else { ?>
                                          <input type="checkbox" name="product_filter_select[]" value="<?php echo $filter_select['filter_select_id']; ?>"
                                                 onclick="selectValaszt(this)" />
                                          <?php echo $filter_select['name']; ?>

                                      <?php } ?>
                                  </div>
                              <?php } ?>
                          </div>
                      </div>
                  </td>
              </tr>

              <tr>
              <td><?php echo $entry_store; ?></td>
              <td><div class="scrollbox">
                  <?php $class = 'even'; ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array(0, $product_store)) { ?>
                    <input type="checkbox" name="product_store[]" value="0" checked="checked" />
                    <?php echo $text_default; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="product_store[]" value="0" />
                    <?php echo $text_default; ?>
                    <?php } ?>
                  </div>
                  <?php foreach ($stores as $store) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array($store['store_id'], $product_store)) { ?>
                    <input type="checkbox" name="product_store[]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                    <?php echo $store['name']; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="product_store[]" value="<?php echo $store['store_id']; ?>" />
                    <?php echo $store['name']; ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div></td>
            </tr>
            <tr>
              <td><?php echo $entry_download; ?></td>
              <td><div class="scrollbox">
                  <?php $class = 'odd'; ?>
                  <?php foreach ($downloads as $download) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array($download['download_id'], $product_download)) { ?>
                    <input type="checkbox" name="product_download[]" value="<?php echo $download['download_id']; ?>" checked="checked" />
                    <?php echo $download['name']; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="product_download[]" value="<?php echo $download['download_id']; ?>" />
                    <?php echo $download['name']; ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div></td>
            </tr>
            <tr>
              <td><?php echo $entry_related; ?></td>
              <td><input type="text" name="related" value="" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><div id="product-related" class="scrollbox">
                  <?php $class = 'odd'; ?>
                  <?php foreach ($product_related as $product_related) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div id="product-related<?php echo $product_related['product_id']; ?>" class="<?php echo $class; ?>"> <?php echo $product_related['name']; ?><img src="view/image/delete.png" />
                    <input type="hidden" name="product_related[]" value="<?php echo $product_related['product_id']; ?>" />
                  </div>
                  <?php } ?>
                </div></td>
            </tr>

              <tr>
                  <td><?php echo $entry_image_letoltheto; ?></td>
                  <td><div class="image"><img src="<?php echo $letoltheto_thumb; ?>" alt="" id="letoltheto_thumb" /><br />
                          <input type="hidden" name="letoltheto" value="<?php echo $letoltheto; ?>" id="letoltheto" />
                          <a onclick="image_upload('letoltheto', 'letoltheto_thumb');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;
                          <a onclick="$('#letoltheto_thumb').attr('src', '<?php echo $no_image; ?>'); $('#letoltheto').attr('value', '');"><?php echo $text_clear; ?></a>
                      </div>
                  </td>
              </tr>
          </table>
        </div>


      <?php if ( $megjelenit_termekful['tulajdonsag_ful'] == 0) { ?>
        <div id="tab-attribute">
          <table id="attribute" class="list">
            <thead>
              <tr>
                <td class="left"><?php echo $entry_attribute; ?></td>
                <td class="left"><?php echo $entry_text; ?></td>
                <td></td>
              </tr>
            </thead>
            <?php $attribute_row = 0; ?>
            <?php foreach ($product_attributes as $product_attribute) { ?>
            <tbody id="attribute-row<?php echo $attribute_row; ?>">
              <tr>
                <td class="left"><input type="text" name="product_attribute[<?php echo $attribute_row; ?>][name]" value="<?php echo $product_attribute['name']; ?>" />
                  <input type="hidden" name="product_attribute[<?php echo $attribute_row; ?>][attribute_id]" value="<?php echo $product_attribute['attribute_id']; ?>" /></td>
                <td class="left"><?php foreach ($languages as $language) { ?>
                  <textarea name="product_attribute[<?php echo $attribute_row; ?>][product_attribute_description][<?php echo $language['language_id']; ?>][text]" cols="40" rows="5"><?php echo isset($product_attribute['product_attribute_description'][$language['language_id']]) ? $product_attribute['product_attribute_description'][$language['language_id']]['text'] : ''; ?></textarea>
                  <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                  <?php } ?></td>
                <td class="left"><a onclick="$('#attribute-row<?php echo $attribute_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
              </tr>
            </tbody>
            <?php $attribute_row++; ?>
            <?php } ?>
            <tfoot>
              <tr>
                <td colspan="2"></td>
                <td class="left"><a onclick="addAttribute();" class="button"><?php echo $button_add_attribute; ?></a></td>
              </tr>
            </tfoot>
          </table>
        </div>
      <?php } ?>

      <?php if ( $megjelenit_termekful['valasztek_ful'] == 0) { ?>
        <div id="tab-option">
          <div id="vtab-option" class="vtabs">
            <?php $option_row = 0; ?>
            <?php foreach ($product_options as $product_option) { ?>
            <a href="#tab-option-<?php echo $option_row; ?>" id="option-<?php echo $option_row; ?>"><?php echo $product_option['name']; ?>&nbsp;<img src="view/image/delete.png" alt="" onclick="$('#vtabs a:first').trigger('click'); $('#option-<?php echo $option_row; ?>').remove(); $('#tab-option-<?php echo $option_row; ?>').remove(); return false;" /></a>
            <?php $option_row++; ?>
            <?php } ?>
            <span id="option-add">
            <input name="option" value="" style="width: 130px;" />
            &nbsp;<img src="view/image/add.png" alt="<?php echo $button_add_option; ?>" title="<?php echo $button_add_option; ?>" /></span></div>
          <?php $option_row = 0; ?>
          <?php $option_value_row = 0; ?>
          <?php foreach ($product_options as $product_option) { ?>
          <div id="tab-option-<?php echo $option_row; ?>" class="vtabs-content">
            <input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_id]" value="<?php echo $product_option['product_option_id']; ?>" />
            <input type="hidden" name="product_option[<?php echo $option_row; ?>][name]" value="<?php echo $product_option['name']; ?>" />
            <input type="hidden" name="product_option[<?php echo $option_row; ?>][option_id]" value="<?php echo $product_option['option_id']; ?>" />
            <input type="hidden" name="product_option[<?php echo $option_row; ?>][type]" value="<?php echo $product_option['type']; ?>" />
            <table class="form">
              <tr>
                <td><?php echo $entry_required; ?></td>
                <td><select name="product_option[<?php echo $option_row; ?>][required]">
                    <?php if ($product_option['required']) { ?>
                    <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                    <option value="0"><?php echo $text_no; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_yes; ?></option>
                    <option value="0" selected="selected"><?php echo $text_no; ?></option>
                    <?php } ?>
                  </select></td>
              </tr>
              <?php if ($product_option['type'] == 'text') { ?>
              <tr>
                <td><?php echo $entry_option_value; ?></td>
                <td><input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="<?php echo $product_option['option_value']; ?>" /></td>
              </tr>
              <?php } ?>
              <?php if ($product_option['type'] == 'textarea') { ?>
              <tr>
                <td><?php echo $entry_option_value; ?></td>
                <td><textarea name="product_option[<?php echo $option_row; ?>][option_value]" cols="40" rows="5"><?php echo $product_option['option_value']; ?></textarea></td>
              </tr>
              <?php } ?>
              <?php if ($product_option['type'] == 'file') { ?>
              <tr style="display: none;">
                <td><?php echo $entry_option_value; ?></td>
                <td><input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="<?php echo $product_option['option_value']; ?>" /></td>
              </tr>
              <?php } ?>
              <?php if ($product_option['type'] == 'date') { ?>
              <tr>
                <td><?php echo $entry_option_value; ?></td>
                <td><input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="<?php echo $product_option['option_value']; ?>" class="date" /></td>
              </tr>
              <?php } ?>
              <?php if ($product_option['type'] == 'datetime') { ?>
              <tr>
                <td><?php echo $entry_option_value; ?></td>
                <td><input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="<?php echo $product_option['option_value']; ?>" class="datetime" /></td>
              </tr>
              <?php } ?>
              <?php if ($product_option['type'] == 'time') { ?>
              <tr>
                <td><?php echo $entry_option_value; ?></td>
                <td><input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="<?php echo $product_option['option_value']; ?>" class="time" /></td>
              </tr>
              <?php } ?>
            </table>
            <?php if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') { ?>
            <table id="option-value<?php echo $option_row; ?>" class="list">
              <thead>
                <tr>
                  <td class="left"><?php echo $entry_option_value; ?></td>
                  <td class="right"><?php echo $entry_quantity; ?></td>
                  <td class="left"><?php echo $entry_subtract; ?></td>
                  <td class="right"><?php echo $entry_price; ?></td>
                  <td class="right"><?php echo $entry_option_points; ?></td>
                  <td class="right"><?php echo $entry_weight; ?></td>
                  <td></td>
                </tr>
              </thead>
              <?php foreach ($product_option['product_option_value'] as $product_option_value) { ?>
              <tbody id="option-value-row<?php echo $option_value_row; ?>">
                <tr>
                  <td class="left"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][option_value_id]">
                      <?php if (isset($option_values[$product_option['option_id']])) { ?>
                      <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
                      <?php if ($option_value['option_value_id'] == $product_option_value['option_value_id']) { ?>
                      <option value="<?php echo $option_value['option_value_id']; ?>" selected="selected"><?php echo $option_value['name']; ?></option>
                      <?php } else { ?>
                      <option value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                      <?php } ?>
                    </select>
                    <input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][product_option_value_id]" value="<?php echo $product_option_value['product_option_value_id']; ?>" /></td>
                  <td class="right"><input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][quantity]" value="<?php echo $product_option_value['quantity']; ?>" size="3" /></td>
                  <td class="left"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][subtract]">
                      <?php if ($product_option_value['subtract']) { ?>
                      <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                      <option value="0"><?php echo $text_no; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $text_yes; ?></option>
                      <option value="0" selected="selected"><?php echo $text_no; ?></option>
                      <?php } ?>
                    </select></td>
                  <td class="right"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][price_prefix]">
                      <?php if ($product_option_value['price_prefix'] == '+') { ?>
                      <option value="+" selected="selected">+</option>
                      <?php } else { ?>
                      <option value="+">+</option>
                      <?php } ?>
                      <?php if ($product_option_value['price_prefix'] == '-') { ?>
                      <option value="-" selected="selected">-</option>
                      <?php } else { ?>
                      <option value="-">-</option>
                      <?php } ?>
                    </select>
                    <input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][price]" value="<?php echo $product_option_value['price']; ?>" size="5" /></td>
                  <td class="right"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][points_prefix]">
                      <?php if ($product_option_value['points_prefix'] == '+') { ?>
                      <option value="+" selected="selected">+</option>
                      <?php } else { ?>
                      <option value="+">+</option>
                      <?php } ?>
                      <?php if ($product_option_value['points_prefix'] == '-') { ?>
                      <option value="-" selected="selected">-</option>
                      <?php } else { ?>
                      <option value="-">-</option>
                      <?php } ?>
                    </select>
                    <input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][points]" value="<?php echo $product_option_value['points']; ?>" size="5" /></td>
                  <td class="right"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][weight_prefix]">
                      <?php if ($product_option_value['weight_prefix'] == '+') { ?>
                      <option value="+" selected="selected">+</option>
                      <?php } else { ?>
                      <option value="+">+</option>
                      <?php } ?>
                      <?php if ($product_option_value['weight_prefix'] == '-') { ?>
                      <option value="-" selected="selected">-</option>
                      <?php } else { ?>
                      <option value="-">-</option>
                      <?php } ?>
                    </select>
                    <input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][weight]" value="<?php echo $product_option_value['weight']; ?>" size="5" /></td>
                  <td class="left"><a onclick="$('#option-value-row<?php echo $option_value_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
                </tr>
              </tbody>
              <?php $option_value_row++; ?>
              <?php } ?>
              <tfoot>
                <tr>
                  <td colspan="6"></td>
                  <td class="left"><a onclick="addOptionValue('<?php echo $option_row; ?>');" class="button"><?php echo $button_add_option_value; ?></a></td>
                </tr>
              </tfoot>
            </table>
            <select id="option-values<?php echo $option_row; ?>" style="display: none;">
              <?php if (isset($option_values[$product_option['option_id']])) { ?>
              <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
              <option value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
            <?php } ?>
          </div>
          <?php $option_row++; ?>
          <?php } ?>
        </div>
      <?php } ?>

      <?php if ( $megjelenit_termekful['mennyisegi_ful'] == 0) { ?>
        <div id="tab-discount">
          <table id="discount" class="list">
            <thead>
              <tr>
                <td class="left"><?php echo $entry_customer_group; ?></td>
                <td class="right"><?php echo $entry_quantity; ?></td>
                <td class="right"><?php echo $entry_priority; ?></td>
                <td class="right"><?php echo $entry_price; ?></td>
                <td class="left"><?php echo $entry_date_start; ?></td>
                <td class="left"><?php echo $entry_date_end; ?></td>
                <td></td>
              </tr>
            </thead>
            <?php $discount_row = 0; ?>
            <?php foreach ($product_discounts as $product_discount) { ?>
            <tbody id="discount-row<?php echo $discount_row; ?>">
              <tr>
                <td class="left"><select name="product_discount[<?php echo $discount_row; ?>][customer_group_id]">
                    <?php foreach ($customer_groups as $customer_group) { ?>
                    <?php if ($customer_group['customer_group_id'] == $product_discount['customer_group_id']) { ?>
                    <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select></td>
                <td class="right"><input type="text" name="product_discount[<?php echo $discount_row; ?>][quantity]" value="<?php echo $product_discount['quantity']; ?>" size="2" /></td>
                <td class="right"><input type="text" name="product_discount[<?php echo $discount_row; ?>][priority]" value="<?php echo $product_discount['priority']; ?>" size="2" /></td>
                <td class="right"><input type="text" name="product_discount[<?php echo $discount_row; ?>][price]" value="<?php echo $product_discount['price']; ?>" /></td>
                <td class="left"><input type="text" name="product_discount[<?php echo $discount_row; ?>][date_start]" value="<?php echo $product_discount['date_start']; ?>" class="date" /></td>
                <td class="left"><input type="text" name="product_discount[<?php echo $discount_row; ?>][date_end]" value="<?php echo $product_discount['date_end']; ?>" class="date" /></td>
                <td class="left"><a onclick="$('#discount-row<?php echo $discount_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
              </tr>
            </tbody>
            <?php $discount_row++; ?>
            <?php } ?>
            <tfoot>
              <tr>
                <td colspan="6"></td>
                <td class="left"><a onclick="addDiscount();" class="button"><?php echo $button_add_discount; ?></a></td>
              </tr>
            </tfoot>
          </table>
        </div>
      <?php } ?>

      <?php if ( $megjelenit_termekful['vevo_ful'] == 0) { ?>
        <div id="tab-vevok">
            <table id="vevo" class="list">
                <thead>
                <tr>
                    <td class="left"><?php echo $vevo_kedvezmeny; ?></td>
                    <td class="right"><?php echo $entry_quantity; ?></td>
                    <td class="right"><?php echo $entry_price; ?></td>
                    <td class="left"><?php echo $entry_date_start; ?></td>
                    <td class="left"><?php echo $entry_date_end; ?></td>
                    <td></td>
                </tr>
                </thead>
                <?php $vevo_row = 0; ?>
                <?php foreach ($product_vevok as $product_vevo) { ?>
                    <tbody id="vevo-row<?php echo $vevo_row; ?>">
                    <tr>
                        <td class="left"><select name="product_vevo[<?php echo $vevo_row; ?>][customer_id]">
                                <?php foreach ($vevo_groups as $vevo_group) { ?>
                                    <?php if ($vevo_group['customer_id'] == $product_vevo['customer_id']) { ?>
                                        <option value="<?php echo $vevo_group['customer_id']; ?>" selected="selected"><?php echo $vevo_group['name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $vevo_group['customer_id']; ?>"><?php echo $vevo_group['name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select></td>
                        <td class="right"><input type="text" name="product_vevo[<?php echo $vevo_row; ?>][quantity]" value="<?php echo $product_vevo['quantity']; ?>" size="2" /></td>
                        <td class="right"><input type="text" name="product_vevo[<?php echo $vevo_row; ?>][price]" value="<?php echo $product_vevo['price']; ?>" /></td>
                        <td class="left"><input type="text" name="product_vevo[<?php echo $vevo_row; ?>][date_start]" value="<?php echo $product_vevo['date_start']; ?>" class="date" /></td>
                        <td class="left"><input type="text" name="product_vevo[<?php echo $vevo_row; ?>][date_end]" value="<?php echo $product_vevo['date_end']; ?>" class="date" /></td>
                        <td class="left"><a onclick="$('#vevo-row<?php echo $vevo_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
                    </tr>
                    </tbody>
                    <?php $vevo_row++; ?>
                <?php } ?>
                <tfoot>
                <tr>
                    <td  colspan="6" class="right"><a onclick="addVevo();" class="button"><?php echo $button_add_vevo; ?></a></td>
                </tr>
                </tfoot>
            </table>
        </div>
      <?php } ?>


      <?php if ( $megjelenit_termekful['akcios_ful'] == 0) { ?>
        <div id="tab-special">
          <table id="special" class="list">
            <thead>
              <tr>
                <td class="left"><?php echo $entry_customer_group; ?></td>
                <td class="right"><?php echo $entry_priority; ?></td>
                <td class="right"><?php echo $entry_price; ?></td>
                <td class="left"><?php echo $entry_date_start; ?></td>
                <td class="left"><?php echo $entry_date_end; ?></td>
                <td></td>
              </tr>
            </thead>
            <?php $special_row = 0; ?>
            <?php foreach ($product_specials as $product_special) { ?>
            <tbody id="special-row<?php echo $special_row; ?>">
              <tr>
                <td class="left"><select name="product_special[<?php echo $special_row; ?>][customer_group_id]">
                    <?php foreach ($customer_groups as $customer_group) { ?>
                    <?php if ($customer_group['customer_group_id'] == $product_special['customer_group_id']) { ?>
                    <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select></td>
                <td class="right"><input type="text" name="product_special[<?php echo $special_row; ?>][priority]" value="<?php echo $product_special['priority']; ?>" size="2" /></td>
                <td class="right"><input type="text" name="product_special[<?php echo $special_row; ?>][price]" value="<?php echo $product_special['price']; ?>" /></td>
                <td class="left"><input type="text" name="product_special[<?php echo $special_row; ?>][date_start]" value="<?php echo $product_special['date_start']; ?>" class="date" /></td>
                <td class="left"><input type="text" name="product_special[<?php echo $special_row; ?>][date_end]" value="<?php echo $product_special['date_end']; ?>" class="date" /></td>
                <td class="left"><a onclick="$('#special-row<?php echo $special_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
              </tr>
            </tbody>
            <?php $special_row++; ?>
            <?php } ?>
            <tfoot>
              <tr>
                <td colspan="5"></td>
                <td class="left"><a onclick="addSpecial();" class="button"><?php echo $button_add_special; ?></a></td>
              </tr>
            </tfoot>
          </table>
        </div>
      <?php } ?>


      <?php if ( $megjelenit_termekful['kep_ful'] == 0) { ?>
        <div id="tab-image">
          <table id="images" class="list">
            <thead>
              <tr>
                <td class="left"><?php echo $entry_image; ?></td>
                <td class="right"><?php echo $entry_sort_order; ?></td>
                <td></td>
              </tr>
            </thead>
            <?php $image_row = 0; ?>
            <?php foreach ($product_images as $product_image) { ?>
            <tbody id="image-row<?php echo $image_row; ?>">
              <tr>
                <td class="left"><div class="image"><img src="<?php echo $product_image['thumb']; ?>" alt="" id="thumb<?php echo $image_row; ?>" />
                    <input type="hidden" name="product_image[<?php echo $image_row; ?>][image]" value="<?php echo $product_image['image']; ?>" id="image<?php echo $image_row; ?>" />
                    <br />
                    <a onclick="image_upload('image<?php echo $image_row; ?>', 'thumb<?php echo $image_row; ?>');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb<?php echo $image_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#image<?php echo $image_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
                <td class="right"><input type="text" name="product_image[<?php echo $image_row; ?>][sort_order]" value="<?php echo $product_image['sort_order']; ?>" size="2" /></td>
                <td class="left"><a onclick="$('#image-row<?php echo $image_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
              </tr>
            </tbody>
            <?php $image_row++; ?>
            <?php } ?>
            <tfoot>
              <tr>
                <td colspan="2"></td>
                <td class="left"><a onclick="addImage();" class="button"><?php echo $button_add_image; ?></a></td>
              </tr>
            </tfoot>
          </table>
        </div>
      <?php } ?>


      <?php if ( $megjelenit_termekful['jutalom_ful'] == 0) { ?>
        <div id="tab-reward">
          <table class="form">
            <tr>
              <td><?php echo $entry_points; ?></td>
              <td><input type="text" name="points" value="<?php echo $points; ?>" /></td>
            </tr>
          </table>
          <table class="list">
            <thead>
              <tr>
                <td class="left"><?php echo $entry_customer_group; ?></td>
                <td class="right"><?php echo $entry_reward; ?></td>
              </tr>
            </thead>
            <?php foreach ($customer_groups as $customer_group) { ?>
            <tbody>
              <tr>
                <td class="left"><?php echo $customer_group['name']; ?></td>
                <td class="right"><input type="text" name="product_reward[<?php echo $customer_group['customer_group_id']; ?>][points]" value="<?php echo isset($product_reward[$customer_group['customer_group_id']]) ? $product_reward[$customer_group['customer_group_id']]['points'] : ''; ?>" /></td>
              </tr>
            </tbody>
            <?php } ?>
          </table>
        </div>
      <?php } ?>


      <?php if ( $megjelenit_termekful['elhelyezkedes'] == 0) { ?>

          <script>

              function Osszesit(sor) {

                  var elhelyezkedes    = $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_id]\']");
                  var elhelyezkedes_id = $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_id]\']");
                  var elhelyezkedes_kivalasztva = elhelyezkedes[0].selectedIndex;
                  var elhelyezkedes_ar=  elhelyezkedes[0][elhelyezkedes_kivalasztva].getAttribute("simple_ar");
                  var elhelyezkedes_netto_ar=  elhelyezkedes[0][elhelyezkedes_kivalasztva].getAttribute("simple_ar_netto");
                  var elhelyezkedes_neve=  elhelyezkedes[0][elhelyezkedes_kivalasztva].getAttribute("neve");


                  var elhelyezkedes_alcsoport = $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_alcsoport_id]\']");
                  var elhelyezkedes_alcsoport_kivalasztva = elhelyezkedes_alcsoport[0].selectedIndex;
                  var elhelyezkedes_alcsoport_ar=  elhelyezkedes_alcsoport[0][elhelyezkedes_alcsoport_kivalasztva].getAttribute("simple_ar");
                  var elhelyezkedes_alcsoport_netto_ar=  elhelyezkedes_alcsoport[0][elhelyezkedes_alcsoport_kivalasztva].getAttribute("simple_ar_netto");
                  var elhelyezkedes_alcsoport_neve=  elhelyezkedes_alcsoport[0][elhelyezkedes_alcsoport_kivalasztva].getAttribute("neve");

                  var kiemelesek = $("[name=\'product_elhelyezkedes["+sor+"][kiemelesek_id]\']");
                  var kiemelesek_kivalasztva = kiemelesek[0].selectedIndex;
                  var kiemelesek_ar=  kiemelesek[0][kiemelesek_kivalasztva].getAttribute("simple_ar");
                  var kiemelesek_netto_ar=  kiemelesek[0][kiemelesek_kivalasztva].getAttribute("simple_ar_netto");
                  var kiemelesek_neve=  kiemelesek[0][kiemelesek_kivalasztva].getAttribute("neve");

                  var ar_osszesen       = Number(elhelyezkedes_ar)+Number(elhelyezkedes_alcsoport_ar)+Number(kiemelesek_ar);
                  var ar_osszesen_netto = Number(elhelyezkedes_netto_ar)+Number(elhelyezkedes_alcsoport_netto_ar)+Number(kiemelesek_netto_ar);



                  var datum_tol = $("[name=\'product_elhelyezkedes["+sor+"][datum_tol]\']").val();
                  var datum_ig = $("[name=\'product_elhelyezkedes["+sor+"][datum_ig]\']").val();

                  var datum_kezdo = new Date(datum_tol);
                  var datum_vege  = new Date(datum_ig);

                  var timeDiff = Math.abs(datum_vege.getTime() - datum_kezdo.getTime()+1);
                  var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                  if (diffDays > 0 ){
                  } else {
                      diffDays = 0;
                  }

                  var ar_osszesen_netto_darab = diffDays * ar_osszesen_netto;
                  var ar_osszesen_darab       = diffDays * ar_osszesen;


                  $("#ar_sor_total"+sor).html("Fizetendő :"+number_format(ar_osszesen_darab)+"<?php echo $penznem?>");

                  $("[name=\'product_elhelyezkedes["+sor+"][sor]\']").val(sor);

                  $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_neve]\']").val(elhelyezkedes_neve);
                  $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_netto_ara]\']").val(elhelyezkedes_netto_ar);
                  $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_brutto_ara]\']").val(elhelyezkedes_ar);

                  $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_alcsoport_neve]\']").val(elhelyezkedes_alcsoport_neve);
                  $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_alcsoport_netto_ara]\']").val(elhelyezkedes_alcsoport_netto_ar);
                  $("[name=\'product_elhelyezkedes["+sor+"][elhelyezkedes_alcsoport_brutto_ara]\']").val(elhelyezkedes_alcsoport_ar);

                  $("[name=\'product_elhelyezkedes["+sor+"][kiemelesek_neve]\']").val(kiemelesek_neve);
                  $("[name=\'product_elhelyezkedes["+sor+"][kiemelesek_netto_ara]\']").val(kiemelesek_netto_ar);
                  $("[name=\'product_elhelyezkedes["+sor+"][kiemelesek_brutto_ara]\']").val(kiemelesek_ar);

                  $("[name=\'product_elhelyezkedes["+sor+"][netto_ar]\']").val(ar_osszesen_netto);
                  $("[name=\'product_elhelyezkedes["+sor+"][brutto_ar]\']").val(ar_osszesen);

                  $("[name=\'product_elhelyezkedes["+sor+"][total_netto_ar]\']").val(ar_osszesen_netto_darab);
                  $("[name=\'product_elhelyezkedes["+sor+"][total_brutto_ar]\']").val(ar_osszesen_darab);

                  var elhelyezes_id=   $("[name=\'product_elhelyezkedes["+sor+"][elhelyezes_id]\']").val();
                  var idoszak =   $("[name=\'product_elhelyezkedes["+sor+"][idoszak]\']").val();

                  if (elhelyezkedes.val() > 0 || kiemelesek.val() > 0) {
                      $('#ures_id'+sor).slideUp(500);
                      $('#elhelyezkedes-row'+sor+' .szelesseg').css("border","1px solid");

                  }

                  if (idoszak > 0) {
                      $('#ures_idoszak'+sor).slideUp(500);
                      $("[name=\'product_elhelyezkedes["+sor+"][idoszak]\']").css("border","2px inset");

                  }

                  if (datum_tol > "0000-00-00") {
                      $('#ures_datum_kezdo'+sor).slideUp(500);
                      $("[name=\'product_elhelyezkedes["+sor+"][datum_tol]\']").css("border","2px inset");

                  }

                  $.ajax({
                      url: 'index.php?route=catalog/elhelyezkedes/elhelyezkedesSzabadHely&token=<?php echo $token; ?>',
                      type: "post",
                      data: 'datum_kezdo='                 + datum_tol
                          + '&datum_vege='                 + datum_ig
                          + '&idoszak='                    + idoszak
                          + '&elhelyezes_id='              + elhelyezes_id
                          + '&elhelyezkedes_id='           + elhelyezkedes.val()
                          + '&elhelyezkedes_alcsoport_id=' + elhelyezkedes_alcsoport.val()
                          + '&sor=' + sor
                          + '&kiemelesek_id='              + kiemelesek.val(),

                      dataType: 'json',

                      success: function(json) {
                          $('.success, .warning, .error').remove();

                          $('#elhelyezkedes-row'+json['sor']+" select" ).css("border","1px solid");

                          if (json['error']) {
                              if ( json['error']['elhelyezkedes']) {
                                  $('#uzenetsor'+json['sor']).append('<div class="warning" style="display: none;">' + json['error']['elhelyezkedes'] + '</div>');

                                  $('.warning').slideDown(500);
                                  $("[name=\'product_elhelyezkedes["+json['sor']+"][elhelyezkedes_id]\']").css("border","2px solid #df190a");

                              }
                              if ( json['error']['elhelyezkedes_alcsoport']) {
                                  $('#uzenetsor'+json['sor']).append('<div class="warning" style="display: none;">' + json['error']['elhelyezkedes_alcsoport'] + '</div>');

                                  $('.warning').slideDown(500);
                                  $("[name=\'product_elhelyezkedes["+json['sor']+"][elhelyezkedes_alcsoport_id]\']").css("border","2px solid #df190a");

                              }
                              if( json['error']['kiemelesek']) {
                                  $('#uzenetsor'+json['sor']).append('<div class="warning" style="display: none;">' + json['error']['kiemelesek'] + '</div>');

                                  $('.warning').slideDown(500);
                                  $("[name=\'product_elhelyezkedes["+json['sor']+"][kiemelesek_id]\']").css("border","2px solid #df190a");

                              }
                          } else {
                              $('#elhelyezkedes-row'+json['sor']+" select" ).css("border","1px solid");
                          }
                      },
                      error: function(e) {
                      }


                  });

              }

          </script>


          <div id="tab-elhelyezkedes">
          <?php $disabled = ""?>
          <table id="elhelyezkedes" class="list">
          <thead>
          <tr>
              <td class="left"><?php echo $entry_elhelyezkedes; ?></td>
              <td class="left"><?php echo $entry_kiemelt_keret; ?></td>
              <td class="left"><?php echo $entry_idoszak_darab; ?></td>
              <td class="left"><?php echo $entry_idoszak; ?></td>
              <td class="left"><?php echo $entry_date_start; ?></td>
              <td class="left"><?php echo $entry_date_end; ?></td>
              <td class="left"><?php echo $entry_priority; ?></td>
              <td class="left"><?php echo $entry_fizetes_status; ?></td>
              <td class="left"><?php echo $entry_status; ?></td>
              <td class="left"><?php echo $entry_fizetendo; ?></td>
              <td></td>
          </tr>
          </thead>

          <tbody id="elhelyezkedes-rowNULL" style="display: none">
          <tr>
              <td colspan=11 id="uzenetsorNULL" style="background-color: #eee; border:none" >
              </td>
          </tr>
          <tr>
              <td class="left">
                  <div  class="select_block" >
                      <select name="product_elhelyezkedes[NULL][elhelyezkedes_id]" class="elhelyezkedes"
                              onchange="AratKiikr(this,'elhelyezkedesNULL','NULL')" <?php echo $disabled; ?> >
                          <option value="0"><?php echo $text_select; ?></option>
                          <?php foreach ($elhelyezkedesek_selects as $elhelyezkedesek_select) { ?>


                              <option value             = "<?php echo $elhelyezkedesek_select['elhelyezkedes_id']; ?>"
                                      ara               = "<?php echo number_format($this->tax->calculate($elhelyezkedesek_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>"
                                      neve              = "<?php echo $elhelyezkedesek_select['megnevezes']; ?>"
                                      simple_ar_netto   = "<?php echo $elhelyezkedesek_select['ara']; ?>"
                                      simple_ar         = "<?php echo $this->tax->calculate($elhelyezkedesek_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')) ?>" >
                                  <?php echo $elhelyezkedesek_select['megnevezes']." - ".number_format($this->tax->calculate($elhelyezkedesek_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>
                              </option>

                          <?php } ?>
                      </select>
                      <span id="elhelyezkedesNULL"  class="ar_megjelenit"></span>
                  </div>

                  <div  class="select_block" style="display: none" id="alcsoportNULL" >
                      <select name="product_elhelyezkedes[NULL][elhelyezkedes_alcsoport_id]"  class="elhelyezkedes_alcsoport"
                              onchange="AratKiikr(this,'elhelyezkedes_alcsoportNULL','NULL' )" <?php echo $disabled; ?> >
                          <option value="0"><?php echo $text_select; ?></option>
                          <?php foreach ($elhelyezkedesek_alcsoport_selects as $elhelyezkedesek_alcsoport_select) { ?>
                              <option value             = "<?php echo $elhelyezkedesek_alcsoport_select['elhelyezkedes_alcsoport_id']; ?>"
                                      ara               = "<?php echo number_format($this->tax->calculate($elhelyezkedesek_alcsoport_select['ara'], $elhelyezkedesek_alcsoport_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>"
                                      neve              = "<?php echo $elhelyezkedesek_alcsoport_select['megnevezes']; ?>"
                                      simple_ar_netto   = "<?php echo $elhelyezkedesek_alcsoport_select['ara']; ?>"
                                      simple_ar         = "<?php  echo $this->tax->calculate($elhelyezkedesek_alcsoport_select['ara'], $elhelyezkedesek_alcsoport_select['tax_class_id'],  $this->config->get('config_tax')) ?>" >
                                  <?php echo $elhelyezkedesek_alcsoport_select['megnevezes']." - ".number_format($this->tax->calculate($elhelyezkedesek_alcsoport_select['ara'], $elhelyezkedesek_alcsoport_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>
                              </option>

                          <?php } ?>
                      </select>
                      <span id="elhelyezkedes_alcsoportNULL"  class="ar_megjelenit"></span>
                  </div>

              <td class="left">
                  <div  class="select_block" >
                      <select name="product_elhelyezkedes[NULL][kiemelesek_id]" class="kiemelesek"
                              onchange="AratKiikr(this,'kiemelesekNULL','NULL')"  <?php echo $disabled; ?> >

                          <option value="0"><?php echo $text_select; ?></option>
                          <?php foreach ($kiemelesek_selects as $kiemelesek_select) { ?>

                              <option value             = "<?php echo $kiemelesek_select['kiemelesek_id']; ?>"
                                      ara               = "<?php echo number_format($this->tax->calculate($kiemelesek_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>"
                                      neve              = "<?php echo $kiemelesek_select['megnevezes']; ?>"
                                      simple_ar_netto   = "<?php echo $kiemelesek_select['ara']; ?>"
                                      simple_ar         = "<?php  echo $this->tax->calculate($kiemelesek_select['ara'], $kiemelesek_select['tax_class_id'],  $this->config->get('config_tax')) ?>" >
                                  <?php echo substr($kiemelesek_select['megnevezes'],0,70)." - ".number_format($this->tax->calculate($kiemelesek_select['ara'], $kiemelesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>
                              </option>


                          <?php } ?>
                      </select>
                      <span id="kiemelesekNULL" class="ar_megjelenit"></span>
                  </div>

              </td>

              <td class="left">
                  <input type="text" name="product_elhelyezkedes[NULL][idoszak]"  sor="NULL" neve="mennyi"
                         value="" size="2" onchange="datumig_atszamol(this)"/>
              </td>

              <td class="left">
                  <select name="product_elhelyezkedes[NULL][idoszak_id]" sor="NULL" neve="idoszak_id"
                          onchange="datumig_atszamol(this)" onKeyUp="datumig_atszamol(this)"   >
                      <?php foreach($idoszakok as $ido) { ?>
                          <option value="<?php echo $ido['id']?>"><?php echo $ido['name']; ?></option>
                      <?php } ?>
                  </select>


              </td>

              <td class="left"><input  type="text" name="product_elhelyezkedes[NULL][datum_tol]" sor="NULL" neve="datum_tol"  value=""  class="elhelyezkedes_datum" size=8/></td>
              <td class="left"><input  type="text" name="product_elhelyezkedes[NULL][datum_ig]"  sor="NULL" neve="datum_ig" readonly="readonly" value="" class="" size=8/></td>
              <td class="left"><input type="text" name="product_elhelyezkedes[NULL][priority]"   value="" size="2" /></td>

              <td class="left"><select name="product_elhelyezkedes[NULL][penzugyi_status_id]">
                      <option value="0"><?php echo $text_select; ?></option>
                      <?php foreach ($penzugyi_status_selects as $penzugyi_status_select) { ?>
                          <?php $kiemeltclass=""; ?>
                          <?php if ($penzugyi_status_select['teljesitett_sor'] == 1) { ?>
                              <?php $kiemeltclass="option_kiemelt"; ?>
                          <?php } ?>

                          <option value="<?php echo $penzugyi_status_select['penzugyi_status_id']; ?>" class="<?php echo $kiemeltclass?>"><?php echo $penzugyi_status_select['megnevezes']; ?></option>
                      <?php } ?>
                  </select>
              </td>


              <td class="left"><select name="product_elhelyezkedes[NULL][fizetes_elbiralas_status_id]">
                      <option value="0"><?php echo $text_select; ?></option>
                      <?php foreach ($fizetes_elbiralas_selects as $fizetes_elbiralas_select) { ?>
                          <?php $kiemeltclass=""; ?>
                          <?php if ($fizetes_elbiralas_select['teljesitett_sor'] == 1) { ?>
                              <?php $kiemeltclass="option_kiemelt"; ?>
                          <?php } ?>

                          <option value="<?php echo $fizetes_elbiralas_select['fizetes_elbiralas_status_id']; ?>"  class="<?php echo $kiemeltclass?>"><?php echo $fizetes_elbiralas_select['megnevezes']; ?></option>

                      <?php } ?>
                  </select>
              </td>

              <td >
                          <span id="ar_sor_totalNULL"  class="ar_megjelenit_ossesen">
                          </span>
              </td>

              <td class="left"><a onclick="$('#elhelyezkedes-rowNULL').remove();" class="button"><?php echo $button_remove; ?></a></td>

              <input type="hidden" name="product_elhelyezkedes[NULL][total_netto_ar]"                      value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][total_brutto_ar]"                     value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][netto_ar]"                            value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][brutto_ar]"                           value="" >

              <input type="hidden" name="product_elhelyezkedes[NULL][elhelyezkedes_neve]"                  value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][elhelyezkedes_netto_ara]"             value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][elhelyezkedes_brutto_ara]"            value="" >

              <input type="hidden" name="product_elhelyezkedes[NULL][elhelyezkedes_alcsoport_neve]"        value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][elhelyezkedes_alcsoport_netto_ara]"   value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][elhelyezkedes_alcsoport_brutto_ara]"  value="" >

              <input type="hidden" name="product_elhelyezkedes[NULL][kiemelesek_neve]"                     value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][kiemelesek_netto_ara]"                value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][kiemelesek_brutto_ara]"               value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][sor]"                                 value="" >
              <input type="hidden" name="product_elhelyezkedes[NULL][kell_alcsoport]"                      value="" >


          </tr>
          </tbody>


          <?php foreach ($product_elhelyezkedess as $product_elhelyezkedes) { ?>
              <tbody id="elhelyezkedes-row<?php echo $product_elhelyezkedes['sor']; ?>">
              <tr>
                  <td colspan=11 id="uzenetsor<?php echo $product_elhelyezkedes['sor']?>" style="background-color: #eee; border:none">
                  </td>
              </tr>
              <tr>
              <td class="left">

                  <div  class="select_block" >
                      <select name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_id]" class="elhelyezkedes"
                              onchange="AratKiikr(this,'elhelyezkedes<?php echo $product_elhelyezkedes['sor']?>','<?php echo $product_elhelyezkedes['sor']?>','<?php echo $product_elhelyezkedes['product_id']; ?>')" <?php echo $disabled; ?> >
                          <?php $megtalalva = false; ?>
                          <option value="0"><?php echo $text_select; ?></option>
                          <?php foreach ($elhelyezkedesek_selects as $elhelyezkedesek_select) { ?>

                              <?php if ($elhelyezkedesek_select['elhelyezkedes_id'] == $product_elhelyezkedes['elhelyezkedes_id']) { ?>
                                  <?php $megtalalva=true; ?>
                                  <option value           = "<?php echo $product_elhelyezkedes['elhelyezkedes_id']; ?>" selected="selected"
                                          ara             = "<?php echo number_format($product_elhelyezkedes['elhelyezkedes_brutto_ara'],0,'','.').$penznem ; ?>"
                                          neve            = "<?php echo $product_elhelyezkedes['elhelyezkedes_neve']; ?>"
                                          simple_ar_netto = "<?php echo $product_elhelyezkedes['elhelyezkedes_netto_ara']; ?>"
                                          simple_ar       = "<?php echo $product_elhelyezkedes['elhelyezkedes_brutto_ara'] ?>" >

                                      <?php echo $product_elhelyezkedes['elhelyezkedes_neve']." - ".number_format($product_elhelyezkedes['elhelyezkedes_brutto_ara'],0,'','.').$penznem ; ?>
                                  </option>

                                  <?php $elhelyezkedes_ar = $this->tax->calculate($product_elhelyezkedes['elhelyezkedes_netto_ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')); ?>

                              <?php } else { ?>

                                  <option value             = "<?php echo $elhelyezkedesek_select['elhelyezkedes_id']; ?>"
                                          ara               = "<?php echo number_format($this->tax->calculate($elhelyezkedesek_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>"
                                          neve              = "<?php echo $elhelyezkedesek_select['megnevezes']; ?>"
                                          simple_ar_netto   = "<?php echo $elhelyezkedesek_select['ara']; ?>"
                                          simple_ar         = "<?php echo $this->tax->calculate($elhelyezkedesek_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')) ?>" >
                                      <?php echo $elhelyezkedesek_select['megnevezes']." - ".number_format($this->tax->calculate($elhelyezkedesek_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>
                                  </option>
                              <?php } ?>
                          <?php } ?>

                          <?php if (!$megtalalva && $product_elhelyezkedes['elhelyezkedes_id'] > 0) { ?>
                              <option value           = "<?php echo $product_elhelyezkedes['elhelyezkedes_id']; ?>" selected="selected"
                                      ara             = "<?php echo number_format($product_elhelyezkedes['elhelyezkedes_brutto_ara'],0,'','.').$penznem ; ?>"
                                      neve            = "<?php echo $product_elhelyezkedes['elhelyezkedes_neve']; ?>"
                                      simple_ar_netto = "<?php echo $product_elhelyezkedes['elhelyezkedes_netto_ara']; ?>"
                                      simple_ar       = "<?php echo $product_elhelyezkedes['elhelyezkedes_brutto_ara']; ?>" >

                                  <?php echo $product_elhelyezkedes['elhelyezkedes_neve']." - ".number_format($product_elhelyezkedes['elhelyezkedes_brutto_ara'],0,'','.').$penznem ; ?>
                              </option>

                              <?php $elhelyezkedes_ar = $product_elhelyezkedes['elhelyezkedes_brutto_ara']; ?>
                          <?php } ?>

                      </select>
                  </div>

                  <?php if ($product_elhelyezkedes['kell_alcsoport'] != 1) { ?>
                      <?php $latszik = "display:none" ?>
                  <?php } else {?>
                      <?php $latszik = "display:block" ?>
                  <?php } ?>
                  <div  class="select_block" style="<?php echo $latszik; ?>" id="alcsoport<?php echo $product_elhelyezkedes['sor'];?>">
                      <select name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_alcsoport_id]" class="elhelyezkedes_alcsoport"
                              onchange="AratKiikr(this,'elhelyezkedes_alcsoport<?php echo $product_elhelyezkedes['sor']?>','<?php echo $product_elhelyezkedes['sor']?>' )" <?php echo $disabled; ?> >
                          <?php $megtalalva = false; ?>

                          <option value="0"><?php echo $text_select; ?></option>
                          <?php foreach ($elhelyezkedesek_alcsoport_selects as $elhelyezkedesek_alcsoport_select) { ?>
                              <?php if ($elhelyezkedesek_alcsoport_select['elhelyezkedes_alcsoport_id'] == $product_elhelyezkedes['elhelyezkedes_alcsoport_id']) { ?>
                                  <?php $megtalalva=true; ?>

                                  <option value             = "<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_id']; ?>" selected="selected"
                                          ara               = "<?php echo number_format($product_elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'],0,'','.').$penznem ; ?>"
                                          neve              = "<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_neve']; ?>"
                                          simple_ar_netto   = "<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_netto_ara']; ?>"
                                          simple_ar         = "<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'] ?>">
                                      <?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_neve']." - ".number_format($product_elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'],0,'','.').$penznem ; ?>
                                  </option>
                                  <?php $elhelyezkedes_alcsoport_ar = $product_elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'];?>

                              <?php } else { ?>

                                  <option value             = "<?php echo $elhelyezkedesek_alcsoport_select['elhelyezkedes_alcsoport_id']; ?>"
                                          ara               = "<?php echo number_format($this->tax->calculate($elhelyezkedesek_alcsoport_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>"
                                          neve              = "<?php echo $elhelyezkedesek_alcsoport_select['megnevezes']; ?>"
                                          simple_ar_netto   = "<?php echo $elhelyezkedesek_alcsoport_select['ara']; ?>"
                                          simple_ar         = "<?php  echo $this->tax->calculate($elhelyezkedesek_alcsoport_select['ara'], $elhelyezkedesek_alcsoport_select['tax_class_id'],  $this->config->get('config_tax')) ?>" >
                                      <?php echo $elhelyezkedesek_alcsoport_select['megnevezes']." - ".number_format($this->tax->calculate($elhelyezkedesek_alcsoport_select['ara'], $elhelyezkedesek_alcsoport_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>
                                  </option>
                              <?php } ?>

                          <?php } ?>

                          <?php if (!$megtalalva && $product_elhelyezkedes['elhelyezkedes_alcsoport_id'] > 0) { ?>

                              <option value             = "<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_id']; ?>" selected="selected"
                                      ara               = "<?php echo number_format($product_elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'],0,'','.').$penznem ; ?>"
                                      neve              = "<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_neve']; ?>"
                                      simple_ar_netto   = "<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_netto_ara']; ?>"
                                      simple_ar         = "<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara']; ?>">
                                  <?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_neve']." - ".number_format($product_elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'],0,'','.').$penznem ; ?>
                              </option>
                              <?php $elhelyezkedes_alcsoport_ar = $product_elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara'];?>

                          <?php } ?>
                      </select>
                  </div>

                  <?php if ($product_elhelyezkedes['kell_alcsoport'] != 1) { ?>
                      <?php $latszik = "display:none" ?>
                      <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_alcsoport_id]" value="0" >
                  <?php } ?>

                  <?php $elhelyezkedes_ar = $product_elhelyezkedes['elhelyezkedes_brutto_ara']; ?>


              </td>



              <td class="left">
                  <div  class="select_block" >
                      <select name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][kiemelesek_id]" class="kiemelesek"
                              onchange="AratKiikr(this,'kiemelesek<?php echo $product_elhelyezkedes['sor']?>','<?php echo $product_elhelyezkedes['sor']?>')"  <?php echo $disabled; ?> >
                          <?php $megtalalva = false; ?>

                          <option value="0"><?php echo $text_select; ?></option>
                          <?php foreach ($kiemelesek_selects as $kiemelesek_select) { ?>
                              <?php if ($kiemelesek_select['kiemelesek_id'] == $product_elhelyezkedes['kiemelesek_id']) { ?>
                                  <?php $megtalalva=true; ?>

                                  <option value             = "<?php echo $product_elhelyezkedes['kiemelesek_id']; ?>" selected="selected"
                                          ara               = "<?php echo number_format($product_elhelyezkedes['kiemelesek_brutto_ara'],0,'','.').$penznem ; ?>"
                                          neve              = "<?php echo $product_elhelyezkedes['kiemelesek_neve']; ?>"
                                          simple_ar_netto   = "<?php echo $product_elhelyezkedes['kiemelesek_netto_ara']; ?>"
                                          simple_ar         = "<?php echo $product_elhelyezkedes['kiemelesek_brutto_ara']; ?>" >
                                      <?php echo $product_elhelyezkedes['kiemelesek_neve']." - ".number_format($product_elhelyezkedes['kiemelesek_brutto_ara'],0,'','.').$penznem ; ?>
                                  </option>
                                  <?php $kiemelesek_ar = $product_elhelyezkedes['kiemelesek_brutto_ara'];?>

                              <?php } else { ?>
                                  <option value             = "<?php echo $kiemelesek_select['kiemelesek_id']; ?>"
                                          ara               = "<?php echo number_format($this->tax->calculate($kiemelesek_select['ara'], $elhelyezkedesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>"
                                          neve              = "<?php echo $kiemelesek_select['megnevezes']; ?>"
                                          simple_ar_netto   = "<?php echo $kiemelesek_select['ara']; ?>"
                                          simple_ar         = "<?php  echo $this->tax->calculate($kiemelesek_select['ara'], $kiemelesek_select['tax_class_id'],  $this->config->get('config_tax')) ?>" >
                                      <?php echo substr($kiemelesek_select['megnevezes'],0,70)." - ".number_format($this->tax->calculate($kiemelesek_select['ara'], $kiemelesek_select['tax_class_id'],  $this->config->get('config_tax')),0,'','.').$penznem ; ?>
                                  </option>
                              <?php } ?>
                          <?php } ?>

                          <?php if (!$megtalalva && $product_elhelyezkedes['kiemelesek_id'] > 0) { ?>
                              <option value             = "<?php echo $product_elhelyezkedes['kiemelesek_id']; ?>" selected="selected"
                                      ara               = "<?php echo number_format($product_elhelyezkedes['kiemelesek_brutto_ara'],0,'','.').$penznem ; ?>"
                                      neve              = "<?php echo $product_elhelyezkedes['kiemelesek_neve']; ?>"
                                      simple_ar_netto   = "<?php echo $product_elhelyezkedes['kiemelesek_netto_ara']; ?>"
                                      simple_ar         = "<?php echo $product_elhelyezkedes['kiemelesek_brutto_ara'] ?>" >
                                  <?php echo $product_elhelyezkedes['kiemelesek_neve']." - ".number_format($product_elhelyezkedes['kiemelesek_brutto_ara'],0,'','.').$penznem ; ?>
                              </option>
                              <?php $kiemelesek_ar = $product_elhelyezkedes['kiemelesek_brutto_ara'];?>
                          <?php } ?>
                      </select>

                  </div>




                  <?php $kiemelesek_ar = $this->tax->calculate($product_elhelyezkedes['kiemelesek_netto_ara'], 0, 0);?>
              </td>



              <td class="left">
                  <input type="text" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][idoszak]"  sor="<?php echo $product_elhelyezkedes['sor']; ?>" neve="mennyi"
                         value="<?php echo $product_elhelyezkedes['idoszak']?>" size="2" onchange="datumig_atszamol(this)"/>
              </td>


              <td class="left">
                  <select name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][idoszak_id]" sor="<?php echo $product_elhelyezkedes['sor']; ?>" neve="idoszak_id"
                          onchange="datumig_atszamol(this)" onKeyUp="datumig_atszamol(this)"   >
                      <?php foreach($idoszakok as $ido) { ?>
                          <?php if ($ido['id'] == $product_elhelyezkedes['idoszak_id']) { ?>
                              <option value="<?php echo $ido['id']?>"  selected="selected"><?php echo $ido['name']; ?></option>
                          <?php } else { ?>
                              <option value="<?php echo $ido['id']?>"><?php echo $ido['name']; ?></option>
                          <?php } ?>
                      <?php } ?>
                  </select>


              </td>

              <td class="left"><input  type="text" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][datum_tol]" sor="<?php echo $product_elhelyezkedes['sor']; ?>" neve="datum_tol"  value="<?php  echo $product_elhelyezkedes['datum_tol']?>"  class="elhelyezkedes_datum" size=8/></td>
              <td class="left"><input  type="text" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][datum_ig]"  sor="<?php echo $product_elhelyezkedes['sor']; ?>" neve="datum_ig" readonly="readonly" value="<?php  echo $product_elhelyezkedes['datum_ig']?>" class="" size=8/></td>
              <td class="left"><input type="text" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][priority]"    value="<?php  echo $product_elhelyezkedes['priority']?>" size="2" /></td>

              <td class="left"><select name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][penzugyi_status_id]">
                      <option value="0"><?php echo $text_select; ?></option>
                      <?php foreach ($penzugyi_status_selects as $penzugyi_status_select) { ?>
                          <?php $kiemeltclass=""; ?>
                          <?php if ($penzugyi_status_select['teljesitett_sor'] == 1) { ?>
                              <?php $kiemeltclass="option_kiemelt"; ?>
                          <?php } ?>

                          <?php if ($penzugyi_status_select['penzugyi_status_id'] == $product_elhelyezkedes['penzugyi_status_id']) { ?>
                              <option value="<?php echo $penzugyi_status_select['penzugyi_status_id']; ?>" selected="selected"  class="<?php echo $kiemeltclass?>"><?php echo $penzugyi_status_select['megnevezes']; ?></option>

                          <?php } else { ?>
                              <option value="<?php echo $penzugyi_status_select['penzugyi_status_id']; ?>" class="<?php echo $kiemeltclass?>"><?php echo $penzugyi_status_select['megnevezes']; ?></option>
                          <?php } ?>
                      <?php } ?>
                  </select>
              </td>


              <td class="left"><select name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][fizetes_elbiralas_status_id]">
                      <option value="0"><?php echo $text_select; ?></option>
                      <?php foreach ($fizetes_elbiralas_selects as $fizetes_elbiralas_select) { ?>
                          <?php $kiemeltclass=""; ?>
                          <?php if ($fizetes_elbiralas_select['teljesitett_sor'] == 1) { ?>
                              <?php $kiemeltclass="option_kiemelt"; ?>
                          <?php } ?>

                          <?php if ($fizetes_elbiralas_select['fizetes_elbiralas_status_id'] == $product_elhelyezkedes['fizetes_elbiralas_status_id']) { ?>
                              <option value="<?php echo $fizetes_elbiralas_select['fizetes_elbiralas_status_id']; ?>" selected="selected"  class="<?php echo $kiemeltclass?>"><?php echo $fizetes_elbiralas_select['megnevezes']; ?></option>

                          <?php } else { ?>
                              <option value="<?php echo $fizetes_elbiralas_select['fizetes_elbiralas_status_id']; ?>"  class="<?php echo $kiemeltclass?>"><?php echo $fizetes_elbiralas_select['megnevezes']; ?></option>

                          <?php } ?>
                      <?php } ?>
                  </select>
              </td>

              <td >
                                      <span id="ar_sor_total<?php echo $product_elhelyezkedes['sor']?>"  class="ar_megjelenit_ossesen">
                                          <?php echo "Fizetendő :" . number_format($product_elhelyezkedes['total_brutto_ar'],0,"",".").$penznem; ?>
                                      </span>

              </td>

              <td class="left"><a onclick="$('#elhelyezkedes-row<?php echo $product_elhelyezkedes['sor']; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>

              <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][total_netto_ar]"                      value="<?php echo $product_elhelyezkedes['total_netto_ar']?>"  >
              <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][total_brutto_ar]"                     value="<?php echo $product_elhelyezkedes['total_brutto_ar']?>" >
              <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][netto_ar]"                            value="<?php echo $product_elhelyezkedes['netto_ar']?>"  >
              <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][brutto_ar]"                           value="<?php echo $product_elhelyezkedes['brutto_ar']?>" >

              <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_neve]"                  value="<?php echo $product_elhelyezkedes['elhelyezkedes_neve']?>" >
              <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_netto_ara]"             value="<?php echo $product_elhelyezkedes['elhelyezkedes_netto_ara']?>" >
              <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_brutto_ara]"            value="<?php echo $product_elhelyezkedes['elhelyezkedes_brutto_ara']?>" >

              <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_alcsoport_neve]"        value="<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_neve']?>" >
              <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_alcsoport_netto_ara]"   value="<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_netto_ara']?>" >
              <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][elhelyezkedes_alcsoport_brutto_ara]"  value="<?php echo $product_elhelyezkedes['elhelyezkedes_alcsoport_brutto_ara']?>" >

              <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][kiemelesek_neve]"                     value="<?php echo $product_elhelyezkedes['kiemelesek_neve']?>" >
              <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][kiemelesek_netto_ara]"                value="<?php echo $product_elhelyezkedes['kiemelesek_netto_ara']?>" >
              <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][kiemelesek_brutto_ara]"               value="<?php echo $product_elhelyezkedes['kiemelesek_brutto_ara']?>" >
              <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][sor]"                                 value="<?php echo $product_elhelyezkedes['sor']?>" >
              <input type="hidden" name="product_elhelyezkedes[<?php echo $product_elhelyezkedes['sor']; ?>][kell_alcsoport]"                      value="<?php echo $product_elhelyezkedes['kell_alcsoport']?>" >

              </tr>
              </tbody>
          <?php } ?>
          <tfoot>
          <tr>
              <td colspan="10"></td>

              <td class="left"><a onclick="addElhelyezkedes();" class="button"><?php echo $button_add_elhelyezkedes; ?></a></td>
          </tr>
          </tfoot>
          </table>
          </div>
      <?php } ?>
      <!-- <div id="tab-design">
         <table class="list">
           <thead>
             <tr>
               <td class="left"><?php echo $entry_store; ?></td>
               <td class="left"><?php echo $entry_layout; ?></td>
             </tr>
           </thead>
           <tbody>
             <tr>
               <td class="left"><?php echo $text_default; ?></td>
               <td class="left"><select name="product_layout[0][layout_id]">
                   <option value=""></option>
                   <?php foreach ($layouts as $layout) { ?>
                   <?php if (isset($product_layout[0]) && $product_layout[0] == $layout['layout_id']) { ?>
                   <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                   <?php } else { ?>
                   <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                   <?php } ?>
                   <?php } ?>
                 </select></td>
             </tr>
           </tbody>
           <?php foreach ($stores as $store) { ?>
           <tbody>
             <tr>
               <td class="left"><?php echo $store['name']; ?></td>
               <td class="left"><select name="product_layout[<?php echo $store['store_id']; ?>][layout_id]">
                   <option value=""></option>
                   <?php foreach ($layouts as $layout) { ?>
                   <?php if (isset($product_layout[$store['store_id']]) && $product_layout[$store['store_id']] == $layout['layout_id']) { ?>
                   <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                   <?php } else { ?>
                   <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                   <?php } ?>
                   <?php } ?>
                 </select></td>
             </tr>
           </tbody>
           <?php } ?>
         </table>
       </div>-->
      </form>
    </div>
  </div>
</div>

<script>
    $(document).ready(function() {
        var ingyenes = "<?php echo $utalvany?>";
        if (ingyenes == 0) {
            $("#szazalek").css("display","none");
        }


        $('.elhelyezkedes_datum').datepicker({
            dateFormat: 'yy-mm-dd',
            minDate: 'd',

            onSelect: function(text){
                debugger;

                datumig_atszamol(this,text);

            }

        });
    });
</script>


<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script> 
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
CKEDITOR.replace('description<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
<?php } ?>
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'related\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id
					}
				}));
			}
		});
		
	}, 
	select: function(event, ui) {
		$('#product-related' + ui.item.value).remove();
		
		$('#product-related').append('<div id="product-related' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" /><input type="hidden" name="product_related[]" value="' + ui.item.value + '" /></div>');

		$('#product-related div:odd').attr('class', 'odd');
		$('#product-related div:even').attr('class', 'even');
				
		return false;
	}
});




    $('input[name=\'category\']').autocomplete({
        delay: 500,
        source: function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item.name,
                            value: item.category_id
                        }
                    }));
                }
            });
        },
        select: function(event, ui) {
            $('#product-category' + ui.item.value).remove();

            $('#product-category').append('<div id="product-category' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" alt="" /><input type="hidden" name="product_category[]" value="' + ui.item.value + '" /></div>');

            $('#product-category div:odd').attr('class', 'odd');
            $('#product-category div:even').attr('class', 'even');

            return false;
        },
        focus: function(event, ui) {
            return false;
        }
    });

    $('#product-category div img').on('click', function() {
        $(this).parent().remove();

        $('#product-category div:odd').attr('class', 'odd');
        $('#product-category div:even').attr('class', 'even');
    });

// Filter
$('input[name=\'filter\']').autocomplete({
    delay: 500,
    source: function(request, response) {
        $.ajax({
            url: 'index.php?route=catalog/filter/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
            dataType: 'json',
            success: function(json) {
                response($.map(json, function(item) {
                    return {
                        label: item.name,
                        value: item.filter_id
                    }
                }));
            }
        });
    },
    select: function(event, ui) {
        $('#product-filter' + ui.item.value).remove();

        $('#product-filter').append('<div id="product-filter' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" alt="" /><input type="hidden" name="product_filter[]" value="' + ui.item.value + '" /></div>');

        $('#product-filter div:odd').attr('class', 'odd');
        $('#product-filter div:even').attr('class', 'even');

        return false;
    },
    focus: function(event, ui) {
        return false;
    }
});

$('#product-filter div img').on('click', function() {
    $(this).parent().remove();

    $('#product-filter div:odd').attr('class', 'odd');
    $('#product-filter div:even').attr('class', 'even');
});



$('#product-related div img').on('click', function() {
	$(this).parent().remove();
	
	$('#product-related div:odd').attr('class', 'odd');
	$('#product-related div:even').attr('class', 'even');	
});
//--></script> 
<script type="text/javascript"><!--
    var attribute_row = <?php echo isset($attribute_row) ? $attribute_row : 0 ?>;

function addAttribute() {
	html  = '<tbody id="attribute-row' + attribute_row + '">';
    html += '  <tr>';
	html += '    <td class="left"><input type="text" name="product_attribute[' + attribute_row + '][name]" value="" /><input type="hidden" name="product_attribute[' + attribute_row + '][attribute_id]" value="" /></td>';
	html += '    <td class="left">';
	<?php foreach ($languages as $language) { ?>
	html += '<textarea name="product_attribute[' + attribute_row + '][product_attribute_description][<?php echo $language['language_id']; ?>][text]" cols="40" rows="5"></textarea><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />';
    <?php } ?>
	html += '    </td>';
	html += '    <td class="left"><a onclick="$(\'#attribute-row' + attribute_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
    html += '  </tr>';	
    html += '</tbody>';
	
	$('#attribute tfoot').before(html);
	
	attributeautocomplete(attribute_row);
	
	attribute_row++;
}

$.widget('custom.catcomplete', $.ui.autocomplete, {
	_renderMenu: function(ul, items) {
		var self = this, currentCategory = '';
		
		$.each(items, function(index, item) {
			if (item.category != currentCategory) {
				ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
				
				currentCategory = item.category;
			}
			
			self._renderItem(ul, item);
		});
	}
});

function attributeautocomplete(attribute_row) {
	$('input[name=\'product_attribute[' + attribute_row + '][name]\']').catcomplete({
		delay: 0,
		source: function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/attribute/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
				dataType: 'json',
				success: function(json) {	
					response($.map(json, function(item) {
						return {
							category: item.attribute_group,
							label: item.name,
							value: item.attribute_id
						}
					}));
				}
			});
		}, 
		select: function(event, ui) {
			$('input[name=\'product_attribute[' + attribute_row + '][name]\']').attr('value', ui.item.label);
			$('input[name=\'product_attribute[' + attribute_row + '][attribute_id]\']').attr('value', ui.item.value);
			
			return false;
		}
	});
}

$('#attribute tbody').each(function(index, element) {
	attributeautocomplete(index);
});
//--></script> 
<script type="text/javascript"><!--
    var option_row = <?php echo isset($option_row) ? $option_row : 0; ?>;

$('input[name=\'option\']').catcomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/option/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						category: item.category,
						label: item.name,
						value: item.option_id,
						type: item.type,
						option_value: item.option_value
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		html  = '<div id="tab-option-' + option_row + '" class="vtabs-content">';
		html += '	<input type="hidden" name="product_option[' + option_row + '][product_option_id]" value="" />';
		html += '	<input type="hidden" name="product_option[' + option_row + '][name]" value="' + ui.item.label + '" />';
		html += '	<input type="hidden" name="product_option[' + option_row + '][option_id]" value="' + ui.item.value + '" />';
		html += '	<input type="hidden" name="product_option[' + option_row + '][type]" value="' + ui.item.type + '" />';
		html += '	<table class="form">';
		html += '	  <tr>';
		html += '		<td><?php echo $entry_required; ?></td>';
		html += '       <td><select name="product_option[' + option_row + '][required]">';
		html += '	      <option value="1"><?php echo $text_yes; ?></option>';
		html += '	      <option value="0"><?php echo $text_no; ?></option>';
		html += '	    </select></td>';
		html += '     </tr>';
		
		if (ui.item.type == 'text') {
			html += '     <tr>';
			html += '       <td><?php echo $entry_option_value; ?></td>';
			html += '       <td><input type="text" name="product_option[' + option_row + '][option_value]" value="" /></td>';
			html += '     </tr>';
		}
		
		if (ui.item.type == 'textarea') {
			html += '     <tr>';
			html += '       <td><?php echo $entry_option_value; ?></td>';
			html += '       <td><textarea name="product_option[' + option_row + '][option_value]" cols="40" rows="5"></textarea></td>';
			html += '     </tr>';						
		}
		 
		if (ui.item.type == 'file') {
			html += '     <tr style="display: none;">';
			html += '       <td><?php echo $entry_option_value; ?></td>';
			html += '       <td><input type="text" name="product_option[' + option_row + '][option_value]" value="" /></td>';
			html += '     </tr>';			
		}
						
		if (ui.item.type == 'date') {
			html += '     <tr>';
			html += '       <td><?php echo $entry_option_value; ?></td>';
			html += '       <td><input type="text" name="product_option[' + option_row + '][option_value]" value="" class="date" /></td>';
			html += '     </tr>';			
		}
		
		if (ui.item.type == 'datetime') {
			html += '     <tr>';
			html += '       <td><?php echo $entry_option_value; ?></td>';
			html += '       <td><input type="text" name="product_option[' + option_row + '][option_value]" value="" class="datetime" /></td>';
			html += '     </tr>';			
		}
		
		if (ui.item.type == 'time') {
			html += '     <tr>';
			html += '       <td><?php echo $entry_option_value; ?></td>';
			html += '       <td><input type="text" name="product_option[' + option_row + '][option_value]" value="" class="time" /></td>';
			html += '     </tr>';			
		}
		
		html += '  </table>';
			
		if (ui.item.type == 'select' || ui.item.type == 'radio' || ui.item.type == 'checkbox' || ui.item.type == 'image') {
			html += '  <table id="option-value' + option_row + '" class="list">';
			html += '  	 <thead>'; 
			html += '      <tr>';
			html += '        <td class="left"><?php echo $entry_option_value; ?></td>';
			html += '        <td class="right"><?php echo $entry_quantity; ?></td>';
			html += '        <td class="left"><?php echo $entry_subtract; ?></td>';
			html += '        <td class="right"><?php echo $entry_price; ?></td>';
			html += '        <td class="right"><?php echo $entry_option_points; ?></td>';
			html += '        <td class="right"><?php echo $entry_weight; ?></td>';
			html += '        <td></td>';
			html += '      </tr>';
			html += '  	 </thead>';
			html += '    <tfoot>';
			html += '      <tr>';
			html += '        <td colspan="6"></td>';
			html += '        <td class="left"><a onclick="addOptionValue(' + option_row + ');" class="button"><?php echo $button_add_option_value; ?></a></td>';
			html += '      </tr>';
			html += '    </tfoot>';
			html += '  </table>';
            html += '  <select id="option-values' + option_row + '" style="display: none;">';
			
            for (i = 0; i < ui.item.option_value.length; i++) {
				html += '  <option value="' + ui.item.option_value[i]['option_value_id'] + '">' + ui.item.option_value[i]['name'] + '</option>';
            }

            html += '  </select>';			
			html += '</div>';	
		}
		
		$('#tab-option').append(html);
		
		$('#option-add').before('<a href="#tab-option-' + option_row + '" id="option-' + option_row + '">' + ui.item.label + '&nbsp;<img src="view/image/delete.png" alt="" onclick="$(\'#vtab-option a:first\').trigger(\'click\'); $(\'#option-' + option_row + '\').remove(); $(\'#tab-option-' + option_row + '\').remove(); return false;" /></a>');
		
		$('#vtab-option a').tabs();
		
		$('#option-' + option_row).trigger('click');		
		
		$('.date').datepicker({dateFormat: 'yy-mm-dd'});
		$('.datetime').datetimepicker({
			dateFormat: 'yy-mm-dd',
			timeFormat: 'h:m'
		});	
			
		$('.time').timepicker({timeFormat: 'h:m'});	
				
		option_row++;
		
		return false;
	}
});
//--></script> 
<script type="text/javascript"><!--
    var option_value_row = <?php echo isset($option_value_row) ? $option_value_row : 0 ; ?>;

function addOptionValue(option_row) {	
	html  = '<tbody id="option-value-row' + option_value_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][option_value_id]">';
	html += $('#option-values' + option_row).html();
	html += '    </select><input type="hidden" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][product_option_value_id]" value="" /></td>';
	html += '    <td class="right"><input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][quantity]" value="" size="3" /></td>'; 
	html += '    <td class="left"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][subtract]">';
	html += '      <option value="1"><?php echo $text_yes; ?></option>';
	html += '      <option value="0"><?php echo $text_no; ?></option>';
	html += '    </select></td>';
	html += '    <td class="right"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][price_prefix]">';
	html += '      <option value="+">+</option>';
	html += '      <option value="-">-</option>';
	html += '    </select>';
	html += '    <input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][price]" value="" size="5" /></td>';
	html += '    <td class="right"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][points_prefix]">';
	html += '      <option value="+">+</option>';
	html += '      <option value="-">-</option>';
	html += '    </select>';
	html += '    <input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][points]" value="" size="5" /></td>';	
	html += '    <td class="right"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][weight_prefix]">';
	html += '      <option value="+">+</option>';
	html += '      <option value="-">-</option>';
	html += '    </select>';
	html += '    <input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][weight]" value="" size="5" /></td>';
	html += '    <td class="left"><a onclick="$(\'#option-value-row' + option_value_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#option-value' + option_row + ' tfoot').before(html);

	option_value_row++;
}
//--></script> 
<script type="text/javascript"><!--
    var discount_row = <?php echo isset($discount_row) ? $discount_row : 0 ; ?>;

    function addDiscount() {
        html  = '<tbody id="discount-row' + discount_row + '">';
        html += '  <tr>';
        html += '    <td class="left"><select name="product_discount[' + discount_row + '][customer_group_id]">';
        <?php foreach ($customer_groups as $customer_group) { ?>
        html += '      <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>';
        <?php } ?>
        html += '    </select></td>';
        html += '    <td class="right"><input type="text" name="product_discount[' + discount_row + '][quantity]" value="" size="2" /></td>';
        html += '    <td class="right"><input type="text" name="product_discount[' + discount_row + '][priority]" value="" size="2" /></td>';
        html += '    <td class="right"><input type="text" name="product_discount[' + discount_row + '][price]" value="" /></td>';
        html += '    <td class="left"><input type="text" name="product_discount[' + discount_row + '][date_start]" value="" class="date" /></td>';
        html += '    <td class="left"><input type="text" name="product_discount[' + discount_row + '][date_end]" value="" class="date" /></td>';
        html += '    <td class="left"><a onclick="$(\'#discount-row' + discount_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
        html += '  </tr>';
        html += '</tbody>';

        $('#discount tfoot').before(html);

        $('#discount-row' + discount_row + ' .date').datepicker({dateFormat: 'yy-mm-dd'});

        discount_row++;
    }
//--></script>

<script type="text/javascript"><!--
    var vevo_row = <?php echo isset($vevo_row) ? $vevo_row : 0; ?>;

    function addVevo() {
        html  = '<tbody id="vevo-row' + vevo_row + '">';
        html += '  <tr>';
        html += '    <td class="left"><select name="product_vevo[' + vevo_row + '][customer_id]">';
        <?php foreach ($vevo_groups as $vevo_group) { ?>
        html += '      <option value="<?php echo $vevo_group['customer_id']; ?>"><?php echo $vevo_group['name']; ?></option>';
        <?php } ?>
        html += '    </select></td>';
        html += '    <td class="right"><input type="text" name="product_vevo[' + vevo_row + '][quantity]" value="" size="2" /></td>';
        html += '    <td class="right"><input type="text" name="product_vevo[' + vevo_row + '][price]" value="" /></td>';
        html += '    <td class="left"><input type="text" name="product_vevo[' + vevo_row + '][date_start]" value="" class="date" /></td>';
        html += '    <td class="left"><input type="text" name="product_vevo[' + vevo_row + '][date_end]" value="" class="date" /></td>';
        html += '    <td class="left"><a onclick="$(\'#vevo-row' + vevo_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
        html += '  </tr>';
        html += '</tbody>';

        $('#vevo tfoot').before(html);

        $('#vevo-row' + vevo_row + ' .date').datepicker({dateFormat: 'yy-mm-dd'});

        vevo_row++;
    }
//--></script>

<script type="text/javascript"><!--
    var special_row = <?php echo isset($special_row) ? $special_row : 0 ; ?>;
    var elhelyezkedes_row = <?php echo isset($elhelyezkedes_row) ? $elhelyezkedes_row : 0; ?>;

function addSpecial() {
	html  = '<tbody id="special-row' + special_row + '">';
	html += '  <tr>'; 
    html += '    <td class="left"><select name="product_special[' + special_row + '][customer_group_id]">';
    <?php foreach ($customer_groups as $customer_group) { ?>
    html += '      <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>';
    <?php } ?>
    html += '    </select></td>';		
    html += '    <td class="right"><input type="text" name="product_special[' + special_row + '][priority]" value="" size="2" /></td>';
	html += '    <td class="right"><input type="text" name="product_special[' + special_row + '][price]" value="" /></td>';
    html += '    <td class="left"><input type="text" name="product_special[' + special_row + '][date_start]" value="" class="date" /></td>';
	html += '    <td class="left"><input type="text" name="product_special[' + special_row + '][date_end]" value="" class="date" /></td>';
	html += '    <td class="left"><a onclick="$(\'#special-row' + special_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
    html += '</tbody>';
	
	$('#special tfoot').before(html);
 
	$('#special-row' + special_row + ' .date').datepicker({dateFormat: 'yy-mm-dd'});
	
	special_row++;
}


    function addElhelyezkedes() {

        $('#elhelyezkedes-rowNULL .elhelyezkedes_datum').attr('id',"");
        $('#elhelyezkedes-rowNULL .elhelyezkedes_datum').attr('class',"elhelyezkedes_datum");
        var emptyrow = $('#elhelyezkedes-rowNULL')[0];
        var newrow = emptyrow.cloneNode(true);
        newrow.id = "elhelyezkedes-row"+elhelyezkedes_row;
        newrow.innerHTML = newrow.innerHTML.replace(/NULL/g, elhelyezkedes_row);
        newrow.style.display = '';

        $('#elhelyezkedes tfoot').before(newrow);

        $('#elhelyezkedes-row' + elhelyezkedes_row + ' .elhelyezkedes_datum').datepicker({
            dateFormat: 'yy-mm-dd',
            minDate: 'd',

            onSelect: function(text){
                datumig_atszamol(this,text);

            }
        });

        elhelyezkedes_row++;
    }



    function datumig_atszamol(obj,text) {

        var melyik_sor=obj.attributes.sor.value;
        if (typeof(text) == "undefined") {
            text = $("[sor="+melyik_sor+"]").filter("[neve=datum_tol]").val();
            $("[sor="+melyik_sor+"]").blur();


            if (text == "" && false){
                return false;
            }
        }

        var newdate = new Date(text);
        var mennyiseg = $("[sor="+melyik_sor+"]").filter("[neve=mennyi]").val()*1;
        var idoszak_id = $("[sor="+melyik_sor+"]").filter("[neve=idoszak_id]").val();

        switch (idoszak_id) {
            case "1":
                newdate.setDate(newdate.getDate() + mennyiseg-1);
                break;

            case "2":
                mennyiseg = mennyiseg*7;
                newdate.setDate(newdate.getDate() + mennyiseg-1);
                break;

            case "3":
                newdate.setMonth(newdate.getMonth() + mennyiseg);
                break;

            case "4":
                newdate.setFullYear(newdate.getFullYear() + mennyiseg);
                break;
        }

        var nd = new Date(newdate);
        var honap = "0"+(nd.getMonth()+1);
        honap = honap.substr(honap.length-2);
        var nap = "0"+nd.getDate();
        nap = nap.substr(nap.length-2);
        var datum_ig = nd.getFullYear()+"-"+honap+"-"+nap;

        if (mennyiseg > 0 && nap != "00") {
            $("[sor="+melyik_sor+"]").filter("[neve=datum_ig]").attr("value",datum_ig);

        } else {
            $("[sor="+melyik_sor+"]").filter("[neve=datum_ig]").attr("value","");
            if (text > "0000-00-00") {
                $('#ures_datum_kezdo'+melyik_sor).slideUp(500);
                $("[name=\'product_elhelyezkedes["+melyik_sor+"][datum_tol]\']").css("border","2px inset");

            }
        }
        Osszesit(melyik_sor);

    }




    function AratKiikr(obj,ara,sor) {

        var ar = obj[obj.selectedIndex].getAttribute("ara");

        if (obj.classList[0] == "elhelyezkedes") {
            var elhelyezkedes_id = obj.value;

            $.ajax({
                url: 'index.php?route=catalog/elhelyezkedes/alcsoportVizsgal&token=<?php echo $token; ?>',
                type: 'post',
                data: 'elhelyezkedes_id='+elhelyezkedes_id +
                    '&sor=' + sor,

                dataType: 'json',

                success: function(json) {
                    $('.success, .warning, .error').remove();
                    if (json['kell_alcsoport']) {
                        $("#alcsoport"+json['sor']).slideDown(500);
                        $("input[type=hidden][name=\'product_elhelyezkedes["+json['sor']+"][elhelyezkedes_alcsoport_id]\']").attr('disabled', 'disabled');

                    } else {
                        $("[name=\'product_elhelyezkedes["+json['sor']+"][elhelyezkedes_alcsoport_id]\']").val(0)
                        $("#alcsoport"+json['sor']).slideUp(500);
                    }
                },
                error: function(e) {
                }


            });
        }

        $(obj).blur();

        Osszesit(sor);


    }

//--></script> 
<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(text) {
						$('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 800,
		height: 500,
		resizable: false,
		modal: false
	});
};
//--></script> 
<script type="text/javascript"><!--
    var image_row = <?php echo isset($image_row) ? $image_row : 0; ?>;

function addImage() {
    html  = '<tbody id="image-row' + image_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><div class="image"><img src="<?php echo $no_image; ?>" alt="" id="thumb' + image_row + '" /><input type="hidden" name="product_image[' + image_row + '][image]" value="" id="image' + image_row + '" /><br /><a onclick="image_upload(\'image' + image_row + '\', \'thumb' + image_row + '\');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$(\'#thumb' + image_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image' + image_row + '\').attr(\'value\', \'\');"><?php echo $text_clear; ?></a></div></td>';
	html += '    <td class="right"><input type="text" name="product_image[' + image_row + '][sort_order]" value="" size="2" /></td>';
	html += '    <td class="left"><a onclick="$(\'#image-row' + image_row  + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#images tfoot').before(html);
	
	image_row++;
}



    function kuponKeszit() {


        var nev = "product_description[<?php echo $this->config->get('config_language_id'); ?>][name]";
        /*var szoveg = "<?php //echo html_entity_decode(substr($product_description[$this->config->get('config_language_id')]['description'],0,-4))  ?>";*/
        var szoveg = CKEDITOR.instances.description2.getData();

        var a = jQuery('[name="product_filter[]"]').filter('[checked=checked]');
        var kep =  $('#thumb').attr("src");
        var filter = new Array();
        for (i=0; i< a.length; i++) {
            filter[i] = a[i].value;
        }


        $.ajax({
            type: "POST",
            url: 'index.php?route=catalog/kuponfile',
            data: 'name=' + encodeURIComponent($('input[name="'+nev+'"]').val())
                + '&szoveg='            + encodeURIComponent(szoveg)
                + '&image='             + kep
                + '&price='             + encodeURIComponent($('input[name="price"]').val())
                + '&szazalek='          + encodeURIComponent($('input[name="szazalek"]').val())
                + '&date_ervenyes_ig='  + encodeURIComponent($('input[name="date_ervenyes_ig"]').val())
                + '&upc='               + encodeURIComponent($('input[name="upc"]').val())
                + '&filters='           + filter,



            dataType: 'json',

            success: function(json) {

                html =  '<a href="'+json['utvonal_cache'] + '" class="colorbox" rel="colorbox" >';
                html += '<img style="width: 100px" src="' + json['utvonal_mentes'] + '" alt="" id="letoltheto_thumb" /><br/>';
                html += '<input type="hidden" name="letoltheto" value="' + json['utvonal_mentes2'] + '" id="letoltheto" />';
                html += '</a>';
                html += '<a onclick="image_upload(\'letoltheto\', \'letoltheto_thumb\');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="kepcsere(\'letoltheto\', \'letoltheto_thumb\')"><?php echo $text_clear; ?></a>';

                $("#kupon_nezegeto").html(html);

                $('.colorbox').colorbox({
                    overlayClose: true,
                    opacity: 0.5
                });

            },
            error: function(e){


            }

        });

    }




//--></script> 
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript"><!--
$('.date').datepicker({dateFormat: 'yy-mm-dd'});
$('.datetime').datetimepicker({
	dateFormat: 'yy-mm-dd',
	timeFormat: 'h:m'
});
$('.time').timepicker({timeFormat: 'h:m'});
//--></script> 
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
$('#languages a').tabs(); 
$('#vtab-option a').tabs();
//--></script>

<script type="text/javascript"><!--


    function uzletRendez(obj) {
        $("#varos_rendez").css("display","none");
        $("#uzlet_rendez").fadeIn(1000);
        $("#varos_rendez_valaszt").removeClass();
        $("#uzlet_rendez_valaszt").addClass("kiemel");
    }

    function varosRendez(obj) {
        $("#uzlet_rendez").css("display","none");
        $("#varos_rendez").fadeIn(1000);
        $("#uzlet_rendez_valaszt").removeClass();
        $("#varos_rendez_valaszt").addClass("kiemel");

    }

    function selectValaszt(obj) {
        if (obj.checked){
            $('input[name="product_filter[]"][select_id="'+obj.value+'"]').prop( "checked", true );
            $('input[name="product_filter2[]"][select_id="'+obj.value+'"]').prop( "checked", true );
        }
    }

    function checkRaad(obj) {
        if (obj.checked){
            $('input[name="product_filter[]"][value="'+obj.value+'"]').prop( "checked", true );
            $('input[name="product_filter2[]"][value="'+obj.value+'"]').prop( "checked", true );
        } else {
            $('input[name="product_filter[]"][value="'+obj.value+'"]').prop( "checked", false );
            $('input[name="product_filter2[]"][value="'+obj.value+'"]').prop( "checked", false );       }
    }


//--></script>

<?php echo $footer; ?>