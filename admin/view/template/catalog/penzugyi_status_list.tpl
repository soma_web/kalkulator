<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
          <a onclick="location = '<?php echo $insert; ?>'" class="button"><?php echo $button_insert; ?></a>
          <a onclick="$('form').attr('action', '<?php echo $delete; ?>'); $('form').submit();" class="button"><?php echo $button_delete; ?></a>

      </div>
    </div>
    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
                <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>

                <td class="left"><?php echo $column_megnevezes; ?></a></td>
              <td class="left"><?php echo $column_sort_order; ?></a></td>
                <td class="right"><?php echo $column_status; ?></a></td>
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>

          <tbody>
            <?php if ($penzugyi_statuss) { ?>
                <?php foreach ($penzugyi_statuss as $penzugyi_status) { ?>
                    <tr>
                        <td style="text-align: center;">
                            <?php if ($penzugyi_status['selected']) { ?>
                                <input type="checkbox" name="selected[]" value="<?php echo $penzugyi_status['penzugyi_status_id']; ?>" checked="checked" />
                            <?php } else { ?>
                                <input type="checkbox" name="selected[]" value="<?php echo $penzugyi_status['penzugyi_status_id']; ?>" />
                            <?php } ?>
                        </td>

                        <td class="left">
                            <?php $toldalek = "";?>
                            <?php if ($penzugyi_status['teljesitett_sor'] == 1) { ?>
                                <?php $toldalek .=  " <b>" . $entry_teljesitett_lista ."</b>"; ?>

                            <?php } ?>
                            <?php if ($penzugyi_status['alapertelmezett_sor'] == 1) { ?>
                                <?php $toldalek .=  " <b>" . $entry_alapertelmezett_lista ."</b>"; ?>
                            <?php }  ?>

                            <?php echo $penzugyi_status['megnevezes'] . " " . $toldalek; ?>
                        </td>

                        <td class="left"><?php echo $penzugyi_status['sort_order']; ?></td>
                        <td class="right"><?php echo $penzugyi_status['statusza']; ?></td>
                        <td class="right"><a href="<?php echo $penzugyi_status['action']['href']; ?>"><?php echo $penzugyi_status['action']['text']; ?></a></td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td class="center" colspan="3"><?php echo $text_no_results; ?></td>
                </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
    </div>
  </div>
</div>

<?php echo $footer; ?>