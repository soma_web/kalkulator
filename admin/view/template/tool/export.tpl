<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/backup.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
          <a onclick="$('#form').submit();" class="button">
              <span><?php echo $button_import; ?></span>
          </a>
          <!--<a onclick="location='<?php echo $export; ?>'" class="button">-->
          <a class="button" id="export_select">
              <span><?php echo $button_export; ?>
              </span>
          </a>
      </div>
    </div>
    <div class="content">
        <div id="export_valaszt_lista" >
            <span>Letöltés tól-ig: <input type="text" value="0"   id="letolt_tol" size="4"> -
                                   <input type="text" value="500" id="letolt_ig"  size="4">
            </span>
            <span><a onclick="letoltTolIg()" class="button">Letöltés</a></span>

        </div>

        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td colspan="2"><?php echo $entry_description; ?></td>
          </tr>
          <tr>
            <td width="25%"><?php echo $entry_restore; ?></td>
            <td><input type="file" name="upload" /></td>
          </tr>
        </table>
      </form>

    </div>
  </div>
</div>

<script>
    function letoltTolIg() {
        var letolt_tol = $("#letolt_tol").val();
        var letolt_ig  = $("#letolt_ig").val();
        debugger;
        location = 'index.php?route=tool/export/download&token=<?php echo $token; ?>&letolt_tol='+letolt_tol+'&letolt_ig='+letolt_ig;
        $("#export_valaszt_lista").fadeOut(400);

    }

    $("#export_select").bind("click",function(){
        $("#export_valaszt_lista").fadeIn(400);

    });
</script>

<?php echo $footer; ?>