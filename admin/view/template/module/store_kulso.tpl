<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
  </div>
  <div class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="form">
         <tr>
              <td><?php echo $entry_text; ?></td>
              <td><?php if ($store_kulso_text) { ?>
                      <input type="radio" name="store_kulso_text" value="1" checked="checked" />
                      <?php echo $text_yes; ?>
                      <input type="radio" name="store_kulso_text" value="0" />
                      <?php echo $text_no; ?>
                  <?php } else { ?>
                      <input type="radio" name="store_kulso_text" value="1" />
                      <?php echo $text_yes; ?>
                      <input type="radio" name="store_kulso_text" value="0" checked="checked" />
                      <?php echo $text_no; ?>
                  <?php } ?></td>
         </tr>

        <tr>
            <td><?php echo $entry_width; ?></td>
            <td><input name="store_kulso_image_width" value="<?php echo $store_kulso_image_width?>" size="7"></td>
        </tr>
        <tr>
            <td><?php echo $entry_height; ?></td>
            <td><input name="store_kulso_image_height" value="<?php echo $store_kulso_image_height?>"  size="7"></td>
        </tr>
      </table>

      <table>

          <tr>
              <td><?php echo $entry_egyeb_link; ?></td>
              <td>
                  <?php if (!empty($store_egyeb_link)) { ?>
                      <label><input type="radio" name="store_egyeb_link" value="1" checked="checked" /><?php echo $text_yes; ?></label>
                      <label><input type="radio" name="store_egyeb_link" value="0" /><?php echo $text_no; ?></label>
                  <?php } else { ?>
                      <label><input type="radio" name="store_egyeb_link" value="1" /><?php echo $text_yes; ?></label>
                      <label><input type="radio" name="store_egyeb_link" value="0" checked="checked" /><?php echo $text_no; ?></label>
                  <?php } ?>
            </td>
          </tr>


          <tr class="egyeb_link">
              <td><?php echo $entry_egyeb_link_link; ?></td>
              <td>
                  <input type="text" name="store_egyeb_link_link" value="<?php echo $store_egyeb_link_link; ?>" size="40"/>
              </td>
          </tr>

          <tr class="egyeb_link">
              <td><?php echo $entry_image; ?></td>
              <td valign="top"><div class="image"><img src="<?php echo $thumb; ?>" alt="" id="thumb" />
                      <input type="hidden" name="store_egyeb_link_image" value="<?php echo $store_egyeb_link_image; ?>" id="image" />
                      <br /><a onclick="image_upload('image', 'thumb');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
          </tr>


          <tr class="egyeb_link">

          <tr>
              <td><?php echo $entry_egyeb_link_title; ?></td>
              <td>
                  <table>
                      <?php foreach ($languages as $language) { ?>
                          <tr>
                              <td>
                                  <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><br />
                              </td>
                              <td>
                                  <?php if ($this->config->get('config_language') == $language['code']) { ?>
                                      <input type="text" name="store_egyeb_link_title" value="<?php echo $store_egyeb_link_title; ?>" size="20"/>
                                  <?php } else { ?>
                                      <input type="text" name="store_egyeb_link_title_<?php echo $language['code']?>" value="<?php echo ${'store_egyeb_link_title_'.$language['code']}; ?>" size="20"/>
                                  <?php } ?>
                              </td>
                          </tr>
                      <?php } ?>
                  </table>
              </td>
          </tr>
      </table>



    </form>
  </div>
</div>

<script>
    function image_upload(field, thumb) {
        $('#dialog').remove();

        $('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

        $('#dialog').dialog({
            title: '<?php echo $text_image_manager; ?>',
            close: function (event, ui) {
                if ($('#' + field).attr('value')) {
                    $.ajax({
                        url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).val()),
                        dataType: 'text',
                        success: function(data) {
                            $('#' + thumb).replaceWith('<img src="' + data + '" alt="" id="' + thumb + '" />');
                        }
                    });
                }
            },
            bgiframe: false,
            width: 800,
            height: 520,
            resizable: false,
            modal: false
        });
    };

    $("input[name='store_egyeb_link']").change(function(){
        debugger;
        if ($(this).val() == "1" ) {
            $('.egyeb_link').fadeIn();
        } else {
            $('.egyeb_link').fadeOut();
        }
    });
</script>

<?php echo $footer; ?>