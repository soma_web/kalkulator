
<?php echo $header; ?>


<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
  </div>
  <div class="content">
  
   <div id="tabs" class="htabs"><a href="#tab_general">General Options</a></div>
   

    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      

			<div id="tab_general">
				<table class="form">
				
					<tr>
          <td><?php echo $entry_color; ?></td>
          <td><select name="tg_helios_cp_default_color">
              
              <?php if (isset($tg_helios_cp_default_color)) {
              $selected = "selected";
              ?>
			  <option value="default" <?php if($tg_helios_cp_default_color=='default'){echo $selected;} ?>>Default</option>
              <option value="blue" <?php if($tg_helios_cp_default_color=='blue'){echo $selected;} ?>>Blue</option>
			  <option value="city" <?php if($tg_helios_cp_default_color=='city'){echo $selected;} ?>>City</option>
			  <option value="green" <?php if($tg_helios_cp_default_color=='green'){echo $selected;} ?>>Green</option>
			  <option value="pink" <?php if($tg_helios_cp_default_color=='pink'){echo $selected;} ?>>Pink</option>
			  <option value="yellow" <?php if($tg_helios_cp_default_color=='yellow'){echo $selected;} ?>>Yellow</option>
			  <option value="orange" <?php if($tg_helios_cp_default_color=='orange'){echo $selected;} ?>>Orange</option>
			  <option value="teal" <?php if($tg_helios_cp_default_color=='teal'){echo $selected;} ?>>Teal</option>
			  <option value="violet" <?php if($tg_helios_cp_default_color=='violet'){echo $selected;} ?>>Violet</option>
			  <?php } else { ?>
			  <option value="default">Default</option>
              <option value="blue">Blue</option>
              <option value="city">City</option>
              <option value="green">Green</option>
              <option value="pink">Pink</option>
			  <option value="yellow">yellow</option>
			  <option value="orange">Orange</option>
			  <option value="teal">Teal</option>
			  <option value="violet">Violet</option>
              <?php } ?>
           </select></td>
        </tr>
					
					<tr>
						<td>Currency:</td>
						<td><input type="checkbox" value="1" name="tg_helios_cp_currency"<?php if($tg_helios_cp_currency == '1') echo ' checked="checked"';?> /> Show</td>
					</tr>
		
					<tr>
						<td>Language:</td>
						<td><input type="checkbox" value="1" name="tg_helios_cp_language"<?php if($tg_helios_cp_language == '1') echo ' checked="checked"';?> /> Show</td>
					</tr>
					
					<tr>
						<td><?php echo $entry_view; ?></td>
						<td><select name="tg_helios_cp_default_view">
						<?php if (isset($tg_helios_cp_default_view)) {
							$selected = "selected";
						?>
							<option value="grid" <?php if($tg_helios_cp_default_view=='grid'){echo $selected;} ?>>Grid</option>
							<option value="list" <?php if($tg_helios_cp_default_view=='list'){echo $selected;} ?>>List</option>
						<?php } else { ?>
							<option value="grid">Grid</option>
							<option value="list">List</option>
						<?php } ?>
						</select></td>
					</tr>
					
				</table>
			</div> <!-- tab_general (end) --> 
	   
<script type="text/javascript" src="view/javascript/jquery/ui/ui.draggable.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/ui.resizable.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/ui.dialog.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/external/bgiframe/jquery.bgiframe.js"></script>
<script type="text/javascript"><!--
function image_upload(field, preview) {
	$('#dialog').remove();

	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&field=' + encodeURIComponent(field) + '&token=<?php echo $this->session->data['token']; ?>" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $this->session->data['token']; ?>',
					type: 'POST',
					data: 'image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(data) {
						$('#' + preview).replaceWith('<img src="' + data + '" alt="" id="' + preview + '" style="border: 1px solid #EEEEEE;" onclick="image_upload(\'' + field + '\', \'' + preview + '\');" />');
					}
				});
			}
		},
		bgiframe: false,
		width: 800,
		height: 520,
		resizable: false,
		modal: false
	});
};
//--></script>

<script type="text/javascript"><!--
$('#tabs a').tabs(); 
$('#languages a').tabs(); 
$('#vtab-option a').tabs();
//--></script> 