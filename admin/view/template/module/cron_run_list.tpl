<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>

    <div class="box">
        <div class="heading">
            <h1><img src="view/image/user.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons">
                <a onclick="location = '<?php echo $cron_list; ?>'" class="button"><?php echo $button_cron_list; ?></a>
            </div>
        </div>
        <div class="content">
            <table class="list">
                <thead>
                    <tr>
                        <td class="left"><?php echo $column_program; ?></td>

                        <td class="left"><?php echo $column_date_start; ?></td>

                        <td class="left"><?php echo $column_date_end; ?></td>

                        <td class="right"><?php echo $column_status; ?></td>
                        <td class="right"><?php echo $column_action; ?></td>

                    </tr>
                </thead>
                <tbody>
                    <?php if ($crons) { ?>
                        <?php foreach ($crons as $cron) { ?>
                            <tr>

                                <td class="left"><?php echo $cron['program']; ?></td>
                                <td class="left"><?php echo $cron['date_start']; ?></td>
                                <td class="left"><?php echo $cron['date_end']; ?></td>
                                <td class="right"><?php echo $cron['status']; ?></td>
                                <td class="right">
                                    <?php foreach ($cron['action'] as $action) { ?>
                                        [ <a href="<?php echo $action['href']; ?>" <?php echo !empty($action['target']) ? 'target='.$action['target'] : ''?>><?php echo $action['text']; ?></a> ]
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } else { ?>
                        <tr>
                            <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php echo $footer; ?> 