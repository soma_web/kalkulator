<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>

    <div class="box">
        <div class="heading">
            <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons">
                <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
                <a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
            </div>
        </div>
        <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="form">
                    <tr>
                        <td><?php echo $entry_status; ?></td>
                        <td><select name="arajanlat_keres_module[status]">
                                <?php if (isset($modules['status']) && $modules['status']) { ?>
                                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <option value="0"><?php echo $text_disabled; ?></option>
                                <?php } else { ?>
                                    <option value="1"><?php echo $text_enabled; ?></option>
                                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $entry_ar_tol; ?></td>
                        <td>
                            <input type="text" name="arajanlat_keres_module[ar_tol]" class="post" value="<?php echo isset($modules['ar_tol']) ? $modules['ar_tol'] : 0?>" size="8" />
                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $entry_ar_ig; ?></td>
                        <td>
                            <input type="text" name="arajanlat_keres_module[ar_ig]" class="post" value="<?php echo isset($modules['ar_ig']) ? $modules['ar_ig'] : 0?>" size="8" />
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <a onclick="levalogat()" class="button"><?php echo $button_levalogat; ?></a>
                        </td>
                    </tr>
                </table>

                <div id="arajanlat_keres">
                    <div class="arajanlat_keres">
                        <div class="fejlec">
                            <?php echo $entry_levalogatott; ?>
                        </div>

                        <div id="arajanlat_keres_alap" class="scrollbox" style="height: 300px">
                            <?php $class = 'odd'; ?>
                            <?php if (isset($modules['altalanos']) && $modules['altalanos']) { ?>
                                <?php foreach ($modules['altalanos'] as $product) { ?>
                                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                    <div id="arajanlat_alap-product_<?php echo $product['product_id']; ?>" class="<?php echo $class; ?>">
                                        <span><?php echo $product['name']; ?></span>
                                        <img src="view/image/success.png" class="img_success" title="Kiválaszt"/>
                                        <img src="view/image/delete.png" class="img_remove" title="Eltávolít"/>
                                        <input type="hidden" class="altalanos" name="arajanlat_keres_module[altalanos][]" value="<?php echo $product['product_id']; ?>" />
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>


                    <div class="arajanlat_keres">
                        <div class="fejlec">
                            <?php echo $entry_hozzaad; ?>  <input type="text" name="hozzaad" value="" />
                        </div>
                        <div id="arajanlat_keres_hozzaad" class="scrollbox" style="height: 300px">
                            <?php $class = 'odd'; ?>
                            <?php if (isset($modules['hozzaadott']) && $modules['hozzaadott']) { ?>
                                <?php foreach ($modules['hozzaadott'] as $product) { ?>
                                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                    <div id="arajanlat_hozzaadott-product_<?php echo $product['product_id']; ?>" class="<?php echo $class; ?>">
                                        <span><?php echo $product['name']; ?></span>
                                        <img src="view/image/success.png" class="img_success" title="Kiválaszt"/>
                                        <img src="view/image/delete.png" class="img_remove" title="Eltávolít"/>
                                        <input type="hidden" class="hozzaadott" name="arajanlat_keres_module[hozzaadott][]" value="<?php echo $product['product_id']; ?>" />
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="arajanlat_keres">
                        <div class="fejlec">
                            <?php echo $entry_kivett; ?>
                        </div>
                        <div id="arajanlat_keres_vegleges" class="scrollbox" style="height: 300px">
                            <?php $class = 'odd'; ?>
                            <?php if (isset($modules['kivett']) && $modules['kivett']) { ?>
                                <?php foreach ($modules['kivett'] as $product) { ?>
                                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                    <div id="arajanlat_kivett-product_<?php echo $product['product_id']; ?>" class="<?php echo $class; ?>"><?php echo $product['name']; ?> <img src="view/image/delete.png" />
                                        <input type="hidden" class="kivett" name="arajanlat_keres_module[kivett][]" value="<?php echo $product['product_id']; ?>" />
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>

<script>
    function levalogat() {
        $.ajax({
            url: 'index.php?route=module/arajanlat_keres/levalogat&token=<?php echo $token; ?>',
            type: 'POST',
            data: $('.post, .kivett').serialize(),
            dataType: 'json',
            success: function(json) {
                var html = "";
                var class_name = 'odd';
                for (var i=0; json['products'].length > i; i++) {
                    class_name = class_name == 'odd' ? 'even' : 'odd';
                    html += '<div id="arajanlat_alap-product_' +json['products'][i]['product_id']+ '" class="'+class_name+'">';
                        html += '<span>';
                            html += json['products'][i]['name'];
                        html += '</span>';
                        html += '<img src="view/image/success.png" class="img_success" title="Kiválaszt"/>';
                        html += '<img src="view/image/delete.png" class="img_remove" title="Eltávolít"/>';
                        html += '<input type="hidden"  class="altalanos"  name="arajanlat_keres_module[altalanos][]" value="'+json['products'][i]['product_id']+'" />';
                    html += '</div>';
                }
                $("#arajanlat_keres_alap").html(html);
            }
        });
    }


    $(document).delegate('#arajanlat_keres_alap div .img_remove','click', function() {
        $(this).parent().remove();

        $('#arajanlat_keres_alap div:odd').attr('class', 'odd');
        $('#arajanlat_keres_alap div:even').attr('class', 'even');
    });

    $(document).delegate('#arajanlat_keres_alap div .img_success','click', function() {
        $(this).parent().remove();

        $('#arajanlat_keres_alap div:odd').attr('class', 'odd');
        $('#arajanlat_keres_alap div:even').attr('class', 'even');

        data = $.map($('#arajanlat_keres_alap input'), function(element){
            return $(element).attr('value');
        });

        $('input[name=\'arajanlat_keres_module[altalanos]\']').attr('value', data.join());

        var product_id      = $(this).parent().find('input[type="hidden"]').val();
        var product_name    = $(this).parent().children()[0].innerHTML;
        var html = "";
        html += '<div id="arajanlat_kivett-product_' +product_id+ '">';
            html += product_name;
            html += '<img src="view/image/delete.png" class="img_remove" title="Eltávolít"/>';
            html += '<input type="hidden"  class="kivett"  name="arajanlat_keres_module[kivett][]" value="'+product_id+'" />';
        html += '</div>';
        $("#arajanlat_kivett-product_" +product_id).remove();

        $("#arajanlat_keres_vegleges").append(html);
        $('#arajanlat_keres_vegleges div:odd').attr('class', 'odd');
        $('#arajanlat_keres_vegleges div:even').attr('class', 'even');

    });




    $(document).delegate('#arajanlat_keres_hozzaad .img_remove','click', function() {
        $(this).parent().remove();

        $('#arajanlat_keres_hozzaad div:odd').attr('class', 'odd');
        $('#arajanlat_keres_hozzaad div:even').attr('class', 'even');
    });

    $(document).delegate('#arajanlat_keres_hozzaad .img_success','click', function() {
        $(this).parent().remove();

        $('#arajanlat_keres_hozzaad div:odd').attr('class', 'odd');
        $('#arajanlat_keres_hozzaad div:even').attr('class', 'even');

        data = $.map($('#arajanlat_keres_hozzaad input'), function(element){
            return $(element).attr('value');
        });

        $('input[name=\'arajanlat_keres_module[hozzaadott]\']').attr('value', data.join());

        var product_id      = $(this).parent().find('input[type="hidden"]').val();
        var product_name    = $(this).parent().children()[0].innerHTML;
        var html = "";
        html += '<div id="arajanlat_kivett-product_' +product_id+ '">';
            html += product_name;
            html += '<img src="view/image/delete.png" class="img_remove" title="Eltávolít"/>';
            html += '<input type="hidden"  class="kivett"  name="arajanlat_keres_module[kivett][]" value="'+product_id+'" />';
        html += '</div>';
        $("#arajanlat_kivett-product_" +product_id).remove();

        $("#arajanlat_keres_vegleges").append(html);
        $('#arajanlat_keres_vegleges div:odd').attr('class', 'odd');
        $('#arajanlat_keres_vegleges div:even').attr('class', 'even');
    });




    $(document).delegate('#arajanlat_keres_vegleges div img','click', function() {
        $(this).parent().remove();

        $('#arajanlat_keres_vegleges div:odd').attr('class', 'odd');
        $('#arajanlat_keres_vegleges div:even').attr('class', 'even');

        data = $.map($('#arajanlat_keres_vegleges input'), function(element){
            return $(element).attr('value');
        });

        $('input[name=\'arajanlat_keres_module[kivett]\']').attr('value', data.join());
    });

    $('input[name=\'hozzaad\']').bind('click', function(){ $(this).autocomplete("search"); } );

    $('input[name=\'hozzaad\']').autocomplete({
        delay: 0,
        source: function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
                data: $('.altalanos, .kivett, .hozzaadott').serialize(),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item.name,
                            value: item.name,
                            ertek: item.product_id,
                            model: item.model
                        }
                    }));
                }
            });

        },
        select: function(event, ui) {
            $('input[name=\'hozzaad\']').blur();

            $('#arajanlat_hozzaadott-product_' + ui.item.ertek).remove();
            var html = '';
            html ='<div id="arajanlat_hozzaadott-product_' + ui.item.ertek + '">'
                html += '<span>'+ui.item.label+'</span>';
                html += '<img src="view/image/success.png" class="img_success" title="Kiválaszt"/>';
                html += '<img src="view/image/delete.png" class="img_remove" title="Eltávolít"/>';
                html += '<input type="hidden"  class="hozzaadott"  name="arajanlat_keres_module[hozzaadott][]" value="' + ui.item.ertek + '" />';
            html += '</div>';
            $('#arajanlat_keres_hozzaad').append(html);

            $('#arajanlat_keres_hozzaad div:odd').attr('class', 'odd');
            $('#arajanlat_keres_hozzaad div:even').attr('class', 'even');

            return false;
        }
    });

</script>

<?php echo $footer; ?>