<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></a></span><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
    </div>
    <div class="erkezteto_tajokoztato">
        Ha szeretné, hogy a vásárlási folyamatban automatikusan kitöltésre kerüljenek a vendégként vásárló adatai, akkor a kimásolt URL-t egészítse ki az alábbi értékek valamelyikével.
        A paraméterek megadása opcionális.
        <br>
        <br>
        <b>&firstname=</b>teszt<b>&lastname=</b>teszt<b>&email=</b>teszt@teszt.hu<b>&telephone=</b>teszt<b>&address_1=</b>teszt<b>&city=</b>teszt<b>&postcode=</b>teszt<b>&country_id=</b>teszt
    </div>
    <div class="content">

      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div class="vtabs">
          <?php $module_row = 1; ?>
          <?php foreach ($modules as $module) { ?>
          <a href="#tab-module-<?php echo $module_row; ?>" id="module-<?php echo $module_row; ?>"><?php echo $tab_module . ' ' . $module_row; ?>&nbsp;<img src="view/image/delete.png" alt="" onclick="$('.vtabs a:first').trigger('click'); $('#module-<?php echo $module_row; ?>').remove(); $('#tab-module-<?php echo $module_row; ?>').remove(); return false;" /></a>
          <?php $module_row++; ?>
          <?php } ?>
          <span id="module-add"><?php echo $button_add_module; ?>&nbsp;<img src="view/image/add.png" alt="" onclick="addModule();" /></span> </div>
        <?php $module_row = 1; ?>
        <?php foreach ($modules as $module) { ?>
        <div id="tab-module-<?php echo $module_row; ?>" class="vtabs-content">
          <div id="language-<?php echo $module_row; ?>" class="htabs">
            <?php foreach ($languages as $language) { ?>
            <a href="#tab-language-<?php echo $module_row; ?>-<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
            <?php } ?>
          </div>

          <table class="form">
            <tr>
                <td><?php echo $entry_status; ?></td>
                <td><select name="erkezteto_oldal_module[<?php echo $module_row; ?>][status]">
                        <?php if ($module['status']) { ?>
                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <option value="0"><?php echo $text_disabled; ?></option>
                        <?php } else { ?>
                            <option value="1"><?php echo $text_enabled; ?></option>
                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                        <?php } ?>
                    </select>
                </td>
            <tr>

              <tr>
                  <td><?php echo $entry_kosar_penztar; ?></td>
                  <td><select name="erkezteto_oldal_module[<?php echo $module_row; ?>][hova]">
                          <?php if ($module['hova']) { ?>
                              <option value="1" selected="selected"><?php echo $text_kosar; ?></option>
                              <option value="0"><?php echo $text_penztar; ?></option>
                          <?php } else { ?>
                              <option value="1"><?php echo $text_kosar; ?></option>
                              <option value="0" selected="selected"><?php echo $text_penztar; ?></option>
                          <?php } ?>
                      </select>
                  </td>
              <tr>

              <tr>
                  <td><span class="required">*</span><?php echo $entry_product_id; ?></td>
                  <td>
                      <input type="text" name="erkezteto_oldal_module[<?php echo $module_row; ?>][product_id]" value="<?php echo !empty($module['product_id']) ? $module['product_id'] : ''?>" size="10"><br>
                      <?php if (isset($error_product_id[$module_row])) { ?>
                          <span class="error"><?php echo $error_product_id[$module_row]; ?></span>
                      <?php } ?>

                  </td>
              <tr>
          </table>

          <?php foreach ($languages as $language) { ?>
          <div id="tab-language-<?php echo $module_row; ?>-<?php echo $language['language_id']; ?>">
          <table class="form">


              <tr>
                <td><?php echo $entry_heading; ?></td>
                <td><input type="text" size="75" value="<?php echo HTTP_CATALOG.'index.php?route=module/erkezteto_oldal&oldal='.$module_row ?>" disabled='disabled' /></td>
              </tr>
              <tr>
                <td><?php echo $entry_description; ?></td>
                <td><textarea name="erkezteto_oldal_module[<?php echo $module_row; ?>][description][<?php echo $language['language_id']; ?>]" id="description-<?php echo $module_row; ?>-<?php echo $language['language_id']; ?>"><?php echo isset($module['description'][$language['language_id']]) ? $module['description'][$language['language_id']] : ''; ?></textarea></td>
              </tr>
            </table>
          </div>
          <?php } ?>


          <input type="hidden" name='erkezteto_oldal_module[<?php echo $module_row; ?>][layout_id]' value="0" >


        </div>
        <?php $module_row++; ?>
        <?php } ?>
      </form>
    </div>
    <div style="text-align:center; margin-top:20px; display: none">
      <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
        <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="hosted_button_id" value="B35J6PKLDZGGS">
        <input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal — The safer, easier way to pay online.">
        <img alt="" border="0" src="https://www.paypalobjects.com/tr_TR/i/scr/pixel.gif" width="1" height="1">
      </form>
      <?php echo $text_or; ?>
      <br />
      <?php echo $text_create_paypal; ?>
      <br /><br />
  </div>

<script type="text/javascript"><!--

    $(document).ready(function(){
        setTimeout(" $('.warning, .success').slideUp(500)",5000);
        $('.date').datepicker({dateFormat: 'yy-mm-dd'});
        $('.vtabs a').tabs();


    });
//--></script>

    <script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>
<script type="text/javascript"><!--
<?php $module_row = 1; ?>
<?php foreach ($modules as $module) { ?>
<?php foreach ($languages as $language) { ?>
    CKEDITOR.replace('description-<?php echo $module_row; ?>-<?php echo $language['language_id']; ?>', {
        height: '500px',
        filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
        filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
        filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
        filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
        filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
    });
<?php } ?>
    <?php $module_row++; ?>
<?php } ?>
    CKEDITOR.config.protectedSource.push(/<\?[\s\S]*?\?>/g);
//--></script> 
<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {	
	html  = '<div id="tab-module-' + module_row + '" class="vtabs-content">';
	html += '  <div id="language-' + module_row + '" class="htabs">';


    <?php foreach ($languages as $language) { ?>
    html += '    <a href="#tab-language-'+ module_row + '-<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>';
    <?php } ?>
	html += '  </div>';

    html += '<table class="form">';
        html += '<tr>';
            html += '<td><?php echo $entry_status; ?></td>';
            html += '<td><select name="erkezteto_oldal_module['+module_row+'][status]">';
                html += '<option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
                html += '<option value="0"><?php echo $text_disabled; ?></option>';
            html += '</select>';
            html += '</td>';
        html += '<tr>';


        html += '<tr>';
            html += '<td><?php echo $entry_kosar_penztar; ?></td>';
            html += '<td><select name="erkezteto_oldal_module[<?php echo $module_row; ?>][hova]">';
                html += '<option value="1" selected="selected"><?php echo $text_kosar; ?></option>';
                html += '<option value="0"><?php echo $text_penztar; ?></option>';

                html += '</select>';
            html += '</td>';
        html += '<tr>';

        html += '<tr>';
            html += '<td><span class="required">*</span><?php echo $entry_product_id; ?></td>';
            html += '<td><input type="text" name="erkezteto_oldal_module[<?php echo $module_row; ?>][product_id]" value="" size="10"></td>';
        html += '<tr>';
    html += '</table>';

	<?php foreach ($languages as $language) { ?>
	html += '    <div id="tab-language-'+ module_row + '-<?php echo $language['language_id']; ?>">';
	html += '      <table class="form">';
	html += '        <tr>';
	html += '          <td><?php echo $entry_heading; ?></td>';

    html += '           <td><input type="text" size="75" value="<?php echo HTTP_CATALOG.'index.php?route=module/erkezteto_oldal&oldal='?>'+module_row+'" disabled="disabled" /></td>';

	html += '        </tr>';
	html += '        <tr>';
	html += '          <td><?php echo $entry_description; ?></td>';
	html += '          <td><textarea name="erkezteto_oldal_module[' + module_row + '][description][<?php echo $language['language_id']; ?>]" id="description-' + module_row + '-<?php echo $language['language_id']; ?>"></textarea></td>';
	html += '        </tr>';
	html += '      </table>';
	html += '    </div>';
	<?php } ?>

    html +=   '<input type="hidden" name="erkezteto_oldal_module[' + module_row + '][layout_id]" value="0" >';
    html +=   '<input type="hidden" name="erkezteto_oldal_module[' + module_row + '][status]" value="0" >';




	html += '</div>';
    debugger;
	
	$('#form').append(html);
	
	<?php foreach ($languages as $language) { ?>
	CKEDITOR.replace('description-' + module_row + '-<?php echo $language['language_id']; ?>', {
        height: '500px',
        filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
		filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
		filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
		filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
		filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
	});  
	<?php } ?>
	
	$('#language-' + module_row + ' a').tabs();
	
	$('#module-add').before('<a href="#tab-module-' + module_row + '" id="module-' + module_row + '"><?php echo $tab_module; ?> ' + module_row + '&nbsp;<img src="view/image/delete.png" alt="" onclick="$(\'.vtabs a:first\').trigger(\'click\'); $(\'#module-' + module_row + '\').remove(); $(\'#tab-module-' + module_row + '\').remove(); return false;" /></a>');
	
	$('.vtabs a').tabs();
	
	$('#module-' + module_row).trigger('click');
	
	module_row++;
}
//--></script> 
<script type="text/javascript"><!--
//--></script>
<script type="text/javascript"><!--
<?php $module_row = 1; ?>
<?php foreach ($modules as $module) { ?>
$('#language-<?php echo $module_row; ?> a').tabs();
<?php $module_row++; ?>
<?php } ?> 
//--></script> 
<?php echo $footer; ?>