<div class="toolbar">
	<ul>
		<li><a href="<?php echo $link_to_module;?>"><span class="icon-module"></span><span><?php echo $this->language->get("text_ecslideshow_module"); ?></span></a></li>
		<li><a href="<?php echo $link_to_banner;?>"><span class="icon-banner"></span><span><?php echo $this->language->get("text_ecslideshow_banner"); ?></span></a></li>

	</ul>
</div>