<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>

    <div class="box">
        <div class="heading">
            <h1><img src="view/image/user.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons">
                <a onclick="location = '<?php echo $cron_futottak; ?>'" class="button"><?php echo $button_futottak_vissza; ?></a>
                <a onclick="location = '<?php echo $cron_list; ?>'" class="button"><?php echo $button_cron_list; ?></a>
            </div>
        </div>
        <div class="content" style="position: relative">
            <table class="list">
                <thead>
                    <tr>
                        <td class="left"><?php echo $column_date_start; ?></td>

                        <td class="left"><?php echo $column_date_end; ?></td>

                        <td class="right"><?php echo $column_action; ?></td>

                    </tr>
                </thead>
                <tbody>
                    <?php if ($results) { ?>
                        <?php foreach ($results as $result) { ?>
                            <tr>

                                <td class="left"><?php echo $result['date_start']; ?></td>
                                <td class="left"><?php echo $result['date_end']; ?></td>
                                <td class="right">
                                    [ <a id="eredmeny_<?php echo $result['cron_allapot_id']?>" class="futtatas_eredmenyek" cron_allapot_id="<?php echo $result['cron_allapot_id']?>"><?php echo $text_futtatas_eredmenyek?></a> ]
                                </td>
                            </tr>
                            <div id="eredmeny_megjelenites_<?php echo $result['cron_allapot_id']?>" class="eredmeny_megjelenites colorbox" rel="colorbox">
                                <img class="close_icon" title="<?php echo $text_close; ?>" alt="<?php echo $text_close;?>"  src="<?php echo $close?>" >
                                <?php echo $result['eredmeny']?>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <tr>
                            <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <div class="pagination"><?php echo $pagination; ?></div>

        </div>
    </div>
</div>
<?php echo $footer; ?>

<script>

    $(".futtatas_eredmenyek").bind("click",function(){
        $('.eredmeny_megjelenites').css('display','none');
        var cron_allapot_id = $(this).attr('cron_allapot_id');

        $('#eredmeny_megjelenites_'+cron_allapot_id).css('opacity','0');
        $('#eredmeny_megjelenites_'+cron_allapot_id).css('display','table');
        var width = $('#eredmeny_megjelenites_'+cron_allapot_id).width();
        $('#eredmeny_megjelenites_'+cron_allapot_id).css('display','none');
        $('#eredmeny_megjelenites_'+cron_allapot_id).css('opacity','1');

        $('#eredmeny_megjelenites_'+cron_allapot_id).fadeIn('slow').css('display','block').css('width',width);
    });

    $('#content').bind("click",function(e) {
        if (e.target.className != 'eredmeny_megjelenites colorbox' && e.target.className != 'close_icon' && e.target.className != 'futtatas_eredmenyek') {
            $('.eredmeny_megjelenites').fadeOut('slow');
        }
    });

    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            $('.eredmeny_megjelenites').fadeOut('slow');
        }
    });
    $(".close_icon").bind("click",function(){
        $('.eredmeny_megjelenites').fadeOut('slow');
    });


</script>
