<?php echo $header; ?>
    <div id="content">
        <div class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
        </div>
        <?php if ($error_warning) { ?>
            <div class="warning"><?php echo $error_warning; ?></div>
        <?php } ?>
        <div class="box">
            <div class="heading">
                <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
                <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
            </div>
            <div class="content">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                    <div id="tab-1" style="clear: both;">
                        <table class="form">
                            <thead>
                            <tr>
                                <td><?php echo $text_settings; ?></td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><?php echo $text_username; ?></td>
                                <td>
                                    <input name="szamlazz_hu_module[szamlazz_hu_username]" value="<?php if(isset($szamlazz_hu_module['szamlazz_hu_username'])){echo $szamlazz_hu_module['szamlazz_hu_username'];} ?>" type="text">
                                </td>
                                <td>
                                    (a Számlázz.hu-s felhasználó)
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo $text_password; ?></td>
                                <td>
                                    <input name="szamlazz_hu_module[szamlazz_hu_password]" value="<?php if(isset($szamlazz_hu_module['szamlazz_hu_password'])){echo $szamlazz_hu_module['szamlazz_hu_password'];} ?>" type="text">
                                </td>
                                <td>
                                    (a Számlázz.hu-s felhasználó jelszava)
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo $text_kulcstartojelszo; ?></td>
                                <td>
                                    <input name="szamlazz_hu_module[szamlazz_hu_kulcstartojelszo]" value="<?php if(isset($szamlazz_hu_module['szamlazz_hu_kulcstartojelszo'])){echo $szamlazz_hu_module['szamlazz_hu_kulcstartojelszo'];} ?>" type="text">
                                </td>
                                <td>
                                    (e-számla esetén a kulcstartót nyitó jelszó)
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo $text_valaszverzio; ?></td>
                                <td>
                                    <input name="szamlazz_hu_module[szamlazz_hu_valaszverzio]" value="<?php if(isset($szamlazz_hu_module['szamlazz_hu_valaszverzio'])){echo $szamlazz_hu_module['szamlazz_hu_valaszverzio'];} ?>" type="text">
                                </td>
                                <td>
                                    (1: egyszerű szöveges válaszüzenetet vagy pdf-et ad vissza. 2: xml válasz, ha kérte a pdf-et az base64 kódolással benne van az xml-ben.)
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo $text_aggregator; ?></td>
                                <td>
                                    <input name="szamlazz_hu_module[szamlazz_hu_aggregator]" value="<?php if(isset($szamlazz_hu_module['szamlazz_hu_aggregator'])){echo $szamlazz_hu_module['szamlazz_hu_aggregator'];} ?>" type="text">
                                </td>
                                <td>

                                </td>
                            </tr>

                            <tr>
                                <td><?php echo "Számlaszám előtag:" ?></td>
                                <td>
                                    <input name="szamlazz_hu_module[szamlazz_hu_elotag]" value="<?php if(isset($szamlazz_hu_module['szamlazz_hu_elotag'])){echo $szamlazz_hu_module['szamlazz_hu_elotag'];} ?>" type="text">
                                </td>
                                <td>

                                </td>
                            </tr>

                            <tr>
                                <td title="szamlazz_hu_module[szamlazz_hu_eszamla]" style="width: auto;"><?php echo $text_eszamla; ?>
                                <td style="width: 144px;">
                                    <?php if (isset($szamlazz_hu_module['szamlazz_hu_eszamla']) && $szamlazz_hu_module['szamlazz_hu_eszamla'] == 1) { ?>
                                        <input type="radio" name="szamlazz_hu_module[szamlazz_hu_eszamla]" value="1" checked="checked" />
                                        <?php echo $text_yes; ?>
                                        <input type="radio" name="szamlazz_hu_module[szamlazz_hu_eszamla]" value="0" />
                                        <?php echo $text_no; ?>
                                    <?php } else { ?>
                                        <input type="radio" name="szamlazz_hu_module[szamlazz_hu_eszamla]" value="1" />
                                        <?php echo $text_yes; ?>
                                        <input type="radio" name="szamlazz_hu_module[szamlazz_hu_eszamla]" value="0" checked="checked" />
                                        <?php echo $text_no; ?>
                                    <?php } ?>
                                </td>
                                <td>
                                    („igen” ha e-számlát kell készíteni)
                                </td>
                            </tr>
                            <tr>
                                <td title="szamlazz_hu_module[szamlazz_hu_szamla_download]" style="width: auto;"><?php echo $text_szamla_download; ?>
                                <td style="width: 144px;">
                                    <?php if (isset($szamlazz_hu_module['szamlazz_hu_szamla_download']) && $szamlazz_hu_module['szamlazz_hu_szamla_download'] == 1) { ?>
                                        <input type="radio" name="szamlazz_hu_module[szamlazz_hu_szamla_download]" value="1" checked="checked" />
                                        <?php echo $text_yes; ?>
                                        <input type="radio" name="szamlazz_hu_module[szamlazz_hu_szamla_download]" value="0" />
                                        <?php echo $text_no; ?>
                                    <?php } else { ?>
                                        <input type="radio" name="szamlazz_hu_module[szamlazz_hu_szamla_download]" value="1" />
                                        <?php echo $text_yes; ?>
                                        <input type="radio" name="szamlazz_hu_module[szamlazz_hu_szamla_download]" value="0" checked="checked" />
                                        <?php echo $text_no; ?>
                                    <?php } ?>
                                </td>
                                <td>
                                    („igen” ha a válaszban meg szeretnénk kapni az elkészült PDF számlát)
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="form">
                            <thead>
                            <tr>
                                <td>
                                    Email beállítások
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    Kinek:
                                </td>
                                <td>
                                    <input type="text" name="szamlazz_hu_module[szamlazz_hu_email_reply_to]" value="<?php if(isset($szamlazz_hu_module['szamlazz_hu_email_reply_to'])){echo $szamlazz_hu_module['szamlazz_hu_email_reply_to'];} ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Email Tárgya:
                                </td>
                                <td>
                                    <input type="text" name="szamlazz_hu_module[szamlazz_hu_email_targy]" value="<?php if(isset($szamlazz_hu_module['szamlazz_hu_email_targy'])){echo $szamlazz_hu_module['szamlazz_hu_email_targy'];} ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Email Szövege:
                                </td>
                                <td>
                                    <textarea id="szamlazz_hu_email_szoveg" type="text" name="szamlazz_hu_module[szamlazz_hu_email_szoveg]"><?php if(isset($szamlazz_hu_module['szamlazz_hu_email_szoveg'])){echo $szamlazz_hu_module['szamlazz_hu_email_szoveg'];} ?></textarea>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <table class="form">
                            <thead>
                            <tr>
                                <td>
                                Bankszámla beállítás
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    Bank:
                                </td>
                                <td>
                                    <input type="text" name="szamlazz_hu_module[szamlazz_hu_bank]" value="<?php if(isset($szamlazz_hu_module['szamlazz_hu_bank'])){echo $szamlazz_hu_module['szamlazz_hu_bank'];} ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Bankszámlaszám:
                                </td>
                                <td>
                                    <input type="text" name="szamlazz_hu_module[szamlazz_hu_bankszamlaszam]" value="<?php if(isset($szamlazz_hu_module['szamlazz_hu_bankszamlaszam'])){echo $szamlazz_hu_module['szamlazz_hu_bankszamlaszam'];} ?>">
                                </td>
                            </tr>

                            </tbody>



                            <tbody>
                            <tr>
                                <td>
                                    Bank (EUR):
                                </td>
                                <td>
                                    <input type="text" name="szamlazz_hu_module[szamlazz_hu_bank_eur]" value="<?php if(isset($szamlazz_hu_module['szamlazz_hu_bank_eur'])){echo $szamlazz_hu_module['szamlazz_hu_bank_eur'];} ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Bankszámlaszám (EUR):
                                </td>
                                <td>
                                    <input type="text" name="szamlazz_hu_module[szamlazz_hu_bankszamlaszam_eur]" value="<?php if(isset($szamlazz_hu_module['szamlazz_hu_bankszamlaszam_eur'])){echo $szamlazz_hu_module['szamlazz_hu_bankszamlaszam_eur'];} ?>">
                                </td>
                            </tr>

                            </tbody>
                        </table>


                        <table class="form">
                            <thead>
                                <tr>
                                    <td>
                                        Automatikus számla küldés:

                                        <select name="szamlazz_hu_module[automatikus]" id="automatikus">
                                            <option><?php echo $text_select?></option>
                                            <option value="1" <?php echo isset($szamlazz_hu_module['automatikus']) && $szamlazz_hu_module['automatikus'] == 1 ? 'selected' : '';?>><?php echo $text_yes?></option>
                                            <option value="2" <?php echo isset($szamlazz_hu_module['automatikus']) && $szamlazz_hu_module['automatikus'] == 2 ? 'selected' : '';?>><?php echo $text_no?></option>
                                        </select>
                                    </td>
                                </tr>

                                </thead>
                                <tbody style="<?php echo isset($szamlazz_hu_module['automatikus']) && $szamlazz_hu_module['automatikus'] == 1 ? '' : ''?>" id="szamla_body">


                                <tr>
                                    <td><?php echo $entry_order_status; ?></td>
                                    <td><select name="szamlazz_hu_module[szamlazz_order_status_id]">
                                            <?php foreach ($order_statuses as $order_status) { ?>
                                                <?php if ($order_status['order_status_id'] == $szamlazz_hu_module['szamlazz_order_status_id']) { ?>
                                                    <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </td>
                                </tr>


                                <tr style="background: #f5f5f5">
                                    <td title="szamlazz_hu_module[szamla_megjegyzes]" style="width: auto; padding-left: 30px;" class="non-top-border right"><b>Normál számla</b></td>
                                    <td style="width: 144px;"  class="non-top-border right-border">
                                        <div class="help">Megjegyzés:</div>
                                        <textarea style="width: 211px; height: 70px;" name="szamlazz_hu_module[szamla_megjegyzes]"><?php echo !empty($szamlazz_hu_module['szamla_megjegyzes']) ? $szamlazz_hu_module['szamla_megjegyzes'] : ''?></textarea>
                                        <br>
                                        <br>
                                        <div class="help">Email Tárgya:</div>
                                        <input type="text" name="szamlazz_hu_module[szamla_email_targya]" value="<?php if(isset($szamlazz_hu_module['szamla_email_targya'])){echo $szamlazz_hu_module['szamla_email_targya'];} ?>">
                                        <br>
                                        <br>
                                        <div class="help">Email Szövege:</div>
                                        <textarea  style="width: 211px; height: 70px;" type="text" name="szamlazz_hu_module[szamla_email_szovege]"><?php if(isset($szamlazz_hu_module['szamla_email_szovege'])){echo $szamlazz_hu_module['szamla_email_szovege'];} ?></textarea>
                                    </td>
                                </tr>


                                <tr style="background: #E4EEF7">
                                    <td title="szamlazz_hu_module[dijbekero_megjegyzes]" style="width: auto; padding-left: 30px;" class="non-top-border right"><b>Díjbekérő</b></td>
                                    <td style="width: 144px;"  class="non-top-border right-border">
                                        <div class="help">Megjegyzés:</div>
                                        <textarea  style="width: 211px; height: 70px;" name="szamlazz_hu_module[dijbekero_megjegyzes]"><?php echo !empty($szamlazz_hu_module['dijbekero_megjegyzes']) ? $szamlazz_hu_module['dijbekero_megjegyzes'] : ''?></textarea>
                                        <br>
                                        <br>
                                        <div class="help">Email Tárgya:</div>
                                        <input type="text" name="szamlazz_hu_module[dijbekero_email_targya]" value="<?php if(isset($szamlazz_hu_module['dijbekero_email_targya'])){echo $szamlazz_hu_module['dijbekero_email_targya'];} ?>">
                                        <br>
                                        <br>
                                        <div class="help">Email Szövege:</div>
                                        <textarea  style="width: 211px; height: 70px;" type="text" name="szamlazz_hu_module[dijbekero_email_szovege]"><?php if(isset($szamlazz_hu_module['dijbekero_email_szovege'])){echo $szamlazz_hu_module['dijbekero_email_szovege'];} ?></textarea>
                                    </td>
                                </tr>

                                <tr style="background: #f5f5f5">
                                    <td title="szamlazz_hu_module[eloleg_szamla_megjegyzes]" style="width: auto; padding-left: 30px;" class="non-top-border right"><b>Előlegszámla</b></td>
                                    <td style="width: 144px;"  class="non-top-border right-border">
                                        <div class="help">Megjegyzés:</div>
                                        <textarea  style="width: 211px; height: 70px;" name="szamlazz_hu_module[eloleg_szamla_megjegyzes]"><?php echo !empty($szamlazz_hu_module['eloleg_szamla_megjegyzes']) ? $szamlazz_hu_module['eloleg_szamla_megjegyzes'] : ''?></textarea>
                                        <br>
                                        <br>
                                        <div class="help">Email Tárgya:</div>
                                        <input type="text" name="szamlazz_hu_module[eloleg_email_targya]" value="<?php if(isset($szamlazz_hu_module['eloleg_email_targya'])){echo $szamlazz_hu_module['eloleg_email_targya'];} ?>">
                                        <br>
                                        <br>
                                        <div class="help">Email Szövege:</div>
                                        <textarea  style="width: 211px; height: 70px;" type="text" name="szamlazz_hu_module[eloleg_email_szovege]"><?php if(isset($szamlazz_hu_module['eloleg_email_szovege'])){echo $szamlazz_hu_module['eloleg_email_szovege'];} ?></textarea>
                                    </td>
                                </tr>

                                <tr style="background: #E4EEF7">
                                    <td title="szamlazz_hu_module[vegszamla_megjegyzes]" style="width: auto; padding-left: 30px;" class="non-top-border right"><b>Végszámla</b></td>
                                    <td style="width: 144px;"  class="non-top-border right-border">
                                        <div class="help">Megjegyzés:</div>
                                        <textarea  style="width: 211px; height: 70px;" name="szamlazz_hu_module[vegszamla_megjegyzes]"><?php echo !empty($szamlazz_hu_module['vegszamla_megjegyzes']) ? $szamlazz_hu_module['vegszamla_megjegyzes'] : ''?></textarea>
                                        <br>
                                        <br>
                                        <div class="help">Email Tárgya:</div>
                                        <input type="text" name="szamlazz_hu_module[vegszamla_email_targya]" value="<?php if(isset($szamlazz_hu_module['vegszamla_email_targya'])){echo $szamlazz_hu_module['vegszamla_email_targya'];} ?>">
                                        <br>
                                        <br>
                                        <div class="help">Email Szövege:</div>
                                        <textarea  style="width: 211px; height: 70px;" type="text" name="szamlazz_hu_module[vegszamla_email_szovege]"><?php if(isset($szamlazz_hu_module['vegszamla_email_szovege'])){echo $szamlazz_hu_module['vegszamla_email_szovege'];} ?></textarea>

                                    </td>
                                </tr>

                                <tr style="background: #f5f5f5">
                                    <td title="szamlazz_hu_module[szallitolevel_megjegyzes]" style="width: auto; padding-left: 30px;" class="non-top-border right"><b>Szállítólevél</b></td>
                                    <td style="width: 144px;"  class="non-top-border right-border">
                                        <div class="help">Megjegyzés:</div>
                                        <textarea  style="width: 211px; height: 70px;" name="szamlazz_hu_module[szallitolevel_megjegyzes]"><?php echo !empty($szamlazz_hu_module['szallitolevel_megjegyzes']) ? $szamlazz_hu_module['szallitolevel_megjegyzes'] : ''?></textarea>
                                        <br>
                                        <br>
                                        <div class="help">Email Tárgya:</div>
                                        <input type="text" name="szamlazz_hu_module[szallitolevel_email_targya]" value="<?php if(isset($szamlazz_hu_module['szallitolevel_email_targya'])){echo $szamlazz_hu_module['szallitolevel_email_targya'];} ?>">
                                        <br>
                                        <br>
                                        <div class="help">Email Szövege:</div>
                                        <textarea  style="width: 211px; height: 70px;" type="text" name="szamlazz_hu_module[szallitolevel_email_szovege]"><?php if(isset($szamlazz_hu_module['szallitolevel_email_szovege'])){echo $szamlazz_hu_module['szallitolevel_email_szovege'];} ?></textarea>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="non-left-border non-right-border">&nbsp;</td>
                                    <td class="non-left-border non-right-border">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td title="szamlazz_hu_module[fizetve]" style="width: auto;">Fizetve:
                                    <td style="width: 144px;">
                                        <?php if (isset($szamlazz_hu_module['fizetve']) && $szamlazz_hu_module['fizetve'] == 1) { ?>
                                            <input type="radio" name="szamlazz_hu_module[fizetve]" value="1" checked="checked" />
                                            Igen
                                            <input type="radio" name="szamlazz_hu_module[fizetve]" value="0" />
                                            Nem
                                        <?php } else { ?>
                                            <input type="radio" name="szamlazz_hu_module[fizetve]" value="1" />
                                            Igen
                                            <input type="radio" name="szamlazz_hu_module[fizetve]" value="0" checked="checked" />
                                            Nem
                                        <?php } ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Példány szám:</td>
                                    <td>
                                        <input type="number" name="szamlazz_hu_module[peldany_szam]"
                                               value="<?php echo isset($szamlazz_hu_module['peldany_szam']) ? $szamlazz_hu_module['peldany_szam'] : '';?>"
                                               style="max-width: 80px" >
                                    </td>
                                </tr>



                                <tr>
                                    <td >Fizetési határidő dátuma:</td>
                                    <td>
                                        <input type="number" name="szamlazz_hu_module[fizetesi_hatarido_datum]"
                                               value="<?php echo isset($szamlazz_hu_module['fizetesi_hatarido_datum']) ? $szamlazz_hu_module['fizetesi_hatarido_datum'] : '';?>"
                                               style="max-width: 80px" > nap
                                    </td>
                                </tr>

                                <tr>
                                    <td >További általános megjegyzés:</td>
                                    <td>
                                        <textarea name="szamlazz_hu_module[megjegyzes]" style="max-width: 300px; height: 40px"><?php echo isset($szamlazz_hu_module['megjegyzes']) ? $szamlazz_hu_module['megjegyzes'] : '';?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="non-bottom-border" style="padding-top: 20px;"><b>Rendelés állapothoz a számla típus beállítása</b>

                                    </td>
                                </tr>
                                <?php foreach($order_statuses as $order_status) { ?>
                                        <tr>
                                            <td><?php echo $order_status['name'];?></td>
                                            <td>
                                                <select name="szamla_tipusa[<?php echo $order_status['order_status_id']?>]">
                                                    <option value="0" <?php echo empty($order_status['szamla_tipusa']) ? 'selected' : ''?>>Nem állít ki számlát</option>
                                                    <option value="1" <?php echo !empty($order_status['szamla_tipusa']) && $order_status['szamla_tipusa'] == 1 ? 'selected' : ''?>>Normál számla</option>
                                                    <option value="2" <?php echo !empty($order_status['szamla_tipusa']) && $order_status['szamla_tipusa'] == 2 ? 'selected' : ''?>>Díjbekérő</option>
                                                    <option value="3" <?php echo !empty($order_status['szamla_tipusa']) && $order_status['szamla_tipusa'] == 3 ? 'selected' : ''?>>Előlegszámla</option>
                                                </select>
                                            </td>
                                        </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>


            </div>
        </div>
    </div>

    <script>
        $(".szamla_tipus").bind("click",function(){
            $(".szamla_tipus").prop("checked",false);
            $(".szamla_tipus_nem").prop("checked",true);
            $(this).prop("checked",true);
        });

        $("#automatikus").bind("click",function(){
            if (this.value == 1) {
                //$("#szamla_body").fadeIn("slow");
            } else {
                //$("#szamla_body").fadeOut("slow");
            }
        });
    </script>

<?php echo $footer; ?>