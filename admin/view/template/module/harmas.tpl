<?php echo $header; ?>

<div id="content">
<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
</div>
<?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
    <div class="heading">
        <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
        <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
            <table class="form">
                <table class="form">

                    <tr>
                        <td><?php echo $entry_fejlec_latszik; ?></td>
                        <td colspan=3><?php if ($harmas_module_fejlec) { ?>
                                <input type="radio" name="harmas_module_fejlec" value="1" checked="checked" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="harmas_module_fejlec" value="0" />
                                <?php echo $text_no; ?>
                            <?php } else { ?>
                                <input type="radio" name="harmas_module_fejlec" value="1" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="harmas_module_fejlec" value="0" checked="checked" />
                                <?php echo $text_no; ?>
                            <?php } ?>
                        </td>
                    </tr>
                <tr>
                    <td><?php echo $entry_product; ?></td>
                    <td>
                        Termék neve:<input type="text" name="product" value="" />
                        Model: <input type="text" name="product_model" value="" />
                    </td>

                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><div id="harmas-product" class="scrollbox">
                            <?php $class = 'odd'; ?>
                            <?php foreach ($products as $product) { ?>
                                <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                <div id="harmas-product<?php echo $product['product_id']; ?>" class="<?php echo $class; ?>">

                                    <?php echo (isset($product['model']) && $product['model']) ? '<span class="kapcsolodo_model">'.$product['model']." - </span>" : ""; ?>

                                    <?php echo $product['name']; ?>
                                    <img src="view/image/delete.png" />
                                    <input type="hidden" value="<?php echo $product['product_id']; ?>" />
                                </div>
                            <?php } ?>
                        </div>
                        <input type="hidden" name="harmas_product" value="<?php echo $harmas_product; ?>" /></td>
                </tr>
            </table>
            <table id="module" class="list">
                <thead>
                <tr>
                    <td class="left"><?php echo $entry_limit; ?></td>
                    <td class="left"><?php echo $entry_image; ?></td>
                    <td class="left"><?php echo $entry_layout; ?></td>
                    <td class="left"><?php echo $entry_position; ?></td>
                    <td class="left"><?php echo $entry_status; ?></td>
                    <td class="right"><?php echo $entry_sort_order; ?></td>
                    <?php if ($this->config->get('megjelenit_korhinta') == 1) { ?>
                        <td class="right"><?php echo $entry_show; ?></td>
                    <? }?>
                    <td></td>
                </tr>
                </thead>
                <?php $module_row = 0; ?>
                <?php foreach ($modules as $module) { ?>
                    <tbody id="module-row<?php echo $module_row; ?>">
                    <tr>
                        <td class="left"><input type="text" name="harmas_module[<?php echo $module_row; ?>][limit]" value="<?php echo $module['limit']; ?>" size="1" /></td>
                        <td class="left"><input type="text" name="harmas_module[<?php echo $module_row; ?>][image_width]" value="<?php echo $module['image_width']; ?>" size="3" />
                            <input type="text" name="harmas_module[<?php echo $module_row; ?>][image_height]" value="<?php echo $module['image_height']; ?>" size="3" />
                            <?php if (isset($error_image[$module_row])) { ?>
                                <span class="error"><?php echo $error_image[$module_row]; ?></span>
                            <?php } ?></td>
                        <td class="left"><select name="harmas_module[<?php echo $module_row; ?>][layout_id]">
                                <?php foreach ($layouts as $layout) { ?>
                                    <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                                        <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select></td>
                        <td class="left"><select name="harmas_module[<?php echo $module_row; ?>][position]">
                                <?php if ($module['position'] == 'content_top') { ?>
                                    <option value="content_top" selected="selected"><?php echo $text_content_top; ?></option>
                                <?php } else { ?>
                                    <option value="content_top"><?php echo $text_content_top; ?></option>
                                <?php } ?>
                                <?php if ($module['position'] == 'content_bottom') { ?>
                                    <option value="content_bottom" selected="selected"><?php echo $text_content_bottom; ?></option>
                                <?php } else { ?>
                                    <option value="content_bottom"><?php echo $text_content_bottom; ?></option>
                                <?php } ?>
                                <?php if ($module['position'] == 'column_left') { ?>
                                    <option value="column_left" selected="selected"><?php echo $text_column_left; ?></option>
                                <?php } else { ?>
                                    <option value="column_left"><?php echo $text_column_left; ?></option>
                                <?php } ?>
                                <?php if ($module['position'] == 'column_right') { ?>
                                    <option value="column_right" selected="selected"><?php echo $text_column_right; ?></option>
                                <?php } else { ?>
                                    <option value="column_right"><?php echo $text_column_right; ?></option>
                                <?php } ?>
                            </select></td>
                        <td class="left"><select name="harmas_module[<?php echo $module_row; ?>][status]">
                                <?php if ($module['status']) { ?>
                                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <option value="0"><?php echo $text_disabled; ?></option>
                                <?php } else { ?>
                                    <option value="1"><?php echo $text_enabled; ?></option>
                                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                            </select></td>
                        <td class="right"><input type="text" name="harmas_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>

                        <?php if ($this->config->get('megjelenit_korhinta') == 1) { $colspan = 7; ?>
                            <td class="right">
                                <select name="harmas_module[<?php echo $module_row; ?>][show]">
                                    <?php if ($module['show'] == 0) { ?>
                                        <option value="0" selected="selected"><?php echo $text_ures; ?></option>
                                        <option value="1"><?php echo $text_mcs5; ?></option>
                                        <option value="2"><?php echo $text_korhinta; ?></option>

                                    <?php } elseif ($module['show'] == 1) { ?>
                                        <option value="0"><?php echo $text_ures; ?></option>
                                        <option value="1" selected="selected"><?php echo $text_mcs5; ?></option>
                                        <option value="2"><?php echo $text_korhinta; ?></option>

                                    <?php } elseif ($module['show'] == 2) { ?>
                                        <option value="0"><?php echo $text_ures; ?></option>
                                        <option value="1"><?php echo $text_mcs5; ?></option>
                                        <option value="2" selected="selected"><?php echo $text_korhinta; ?></option>

                                    <?php } ?>
                                </select>
                            </td>
                        <? } else { $colspan = 6;?>
                            <input type="hidden" name="harmas_module[<?php echo $module_row; ?>][show]" value="0">
                        <? } ?>

                        <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
                    </tr>
                    </tbody>
                    <?php $module_row++; ?>
                <?php } ?>
                <tfoot>
                <tr>
                    <td colspan="<? echo $colspan?>"></td>
                    <td class="left"><a onclick="addModule();" class="button"><?php echo $button_add_module; ?></a></td>
                </tr>
                </tfoot>
            </table>
        </form>
    </div>
</div>
<script type="text/javascript">
    $('input[name=\'product\']').autocomplete({
        delay: 0,
        source: function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
                dataType: 'json',
                success: function(json) {
                    debugger;

                    response($.map(json, function(item) {
                        return {
                            label: item.name,
                            value: item.name,
                            model: item.model,
                            ertek: item.product_id
                        }
                    }));
                }
            });

        },
        select: function(event, ui) {
            $('#harmas-product' + ui.item.value).remove();
            debugger;

            var html ='<div id="harmas-product' + ui.item.ertek + '">';
            if (ui.item.model) {
                html += '<span class="kapcsolodo_model">';
                html += ui.item.model;
                html += " - ";
                html += '</span>';

            }
            html += ui.item.label + '<img src="view/image/delete.png" /><input type="hidden"  value="' + ui.item.ertek + '" /></div>';
            $('#harmas-product').append(html);


            $('#harmas-product div:odd').attr('class', 'odd');
            $('#harmas-product div:even').attr('class', 'even');

            data = $.map($('#harmas-product input'), function(element){
                return $(element).attr('value');
            });

            $('input[name=\'harmas_product\']').attr('value', data.join());

            return false;
        }
    });

    $('input[name=\'product_model\']').autocomplete({
        delay: 0,
        source: function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_model=' +  encodeURIComponent(request.term),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item.model,
                            value: item.model,
                            ertek: item.product_id,
                            model: item.model,
                            neve: item.name
                        }
                    }));
                }
            });

        },
        select: function(event, ui) {
            $('#harmas-product' + ui.item.ertek).remove();

            var html ='<div id="harmas-product' + ui.item.ertek + '">';

            if (ui.item.model) {
                html += '<span class="kapcsolodo_model">';
                html += ui.item.model;
                html += " - ";
                html += '</span>';
            }
            html += ui.item.neve + '<img src="view/image/delete.png" /><input type="hidden"  value="' + ui.item.ertek + '" /></div>';
            $('#harmas-product').append(html);


            $('#harmas-product div:odd').attr('class', 'odd');
            $('#harmas-product div:even').attr('class', 'even');

            data = $.map($('#harmas-product input'), function(element){
                return $(element).attr('value');
            });

            $('input[name=\'harmas_product\']').attr('value', data.join());

            return false;
        }
    });

    $('#harmas-product div img').on('click', function() {
        $(this).parent().remove();

        $('#harmas-product div:odd').attr('class', 'odd');
        $('#harmas-product div:even').attr('class', 'even');

        data = $.map($('#harmas-product input'), function(element){
            return $(element).attr('value');
        });

        $('input[name=\'harmas_product\']').attr('value', data.join());
    });
    </script>
<script type="text/javascript">
    var module_row = <?php echo $module_row; ?>;

    function addModule() {
        html  = '<tbody id="module-row' + module_row + '">';
        html += '  <tr>';
        html += '    <<td class="left"><input type="text" name="harmas_module[' + module_row + '][limit]" value="5" size="1" /></td>';
        html += '    <td class="left"><input type="text" name="harmas_module[' + module_row + '][image_width]" value="80" size="3" /> <input type="text" name="harmas_module[' + module_row + '][image_height]" value="80" size="3" /></td>';
        html += '    <td class="left"><select name="harmas_module[' + module_row + '][layout_id]">';
        <?php foreach ($layouts as $layout) { ?>
        html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
        <?php } ?>
        html += '    </select></td>';
        html += '    <td class="left"><select name="harmas_module[' + module_row + '][position]">';
        html += '      <option value="content_top"><?php echo $text_content_top; ?></option>';
        html += '      <option value="content_bottom"><?php echo $text_content_bottom; ?></option>';
        html += '      <option value="column_left"><?php echo $text_column_left; ?></option>';
        html += '      <option value="column_right"><?php echo $text_column_right; ?></option>';
        html += '    </select></td>';
        html += '    <td class="left"><select name="harmas_module[' + module_row + '][status]">';
        html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
        html += '      <option value="0"><?php echo $text_disabled; ?></option>';
        html += '    </select></td>';
        html += '    <td class="right"><input type="text" name="harmas_module[' + module_row + '][sort_order]" value="" size="3" /></td>';


        html += '   <?php if ($this->config->get('megjelenit_korhinta') == 1) { $colspan = 7; ?>';
        html += '    <td class="right"><select name="harmas_module[' + module_row + '][show]">';
        html += '     <option value="0" selected="selected"><?php echo $text_ures; ?></option>';
        html += '    <option value="1"><?php echo $text_mcs5; ?></option>';
        html += '    <option value="2"><?php echo $text_korhinta; ?></option>';
        html += '  <? } else { $colspan = 6;?>';
        html += '    <input type="hidden" name="harmas_module[' + module_row + '][show]" value="0"  />';
        html += '    <? } ?> ';

        html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
        html += '  </tr>';
        html += '</tbody>';

        $('#module tfoot').before(html);

        module_row++;
    }
    </script>




<!--<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
        </div>
        <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <table id="module" class="list">
                    <thead>
                    <tr>
                        <td class="left"><?php echo $entry_limit; ?></td>
                        <td class="left"><?php echo $entry_image; ?></td>
                        <td class="left"><?php echo $entry_layout; ?></td>
                        <td class="left"><?php echo $entry_position; ?></td>
                        <td class="left"><?php echo $entry_status; ?></td>
                        <td class="right"><?php echo $entry_sort_order; ?></td>
                        <td></td>
                    </tr>
                    </thead>
                    <?php $module_row = 0; ?>
                    <?php foreach ($modules as $module) { ?>
                        <tbody id="module-row<?php echo $module_row; ?>">
                        <tr>
                            <td class="left"><input type="text" name="harmas_module[<?php echo $module_row; ?>][limit]" value="<?php echo $module['limit']; ?>" size="1" /></td>
                            <td class="left"><input type="text" name="harmas_module[<?php echo $module_row; ?>][image_width]" value="<?php echo $module['image_width']; ?>" size="3" />
                                <input type="text" name="harmas_module[<?php echo $module_row; ?>][image_height]" value="<?php echo $module['image_height']; ?>" size="3" />
                                <?php if (isset($error_image[$module_row])) { ?>
                                    <span class="error"><?php echo $error_image[$module_row]; ?></span>
                                <?php } ?></td>
                            <td class="left"><select name="harmas_module[<?php echo $module_row; ?>][layout_id]">
                                    <?php foreach ($layouts as $layout) { ?>
                                        <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                                            <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select></td>
                            <td class="left"><select name="harmas_module[<?php echo $module_row; ?>][position]">
                                    <?php if ($module['position'] == 'content_top') { ?>
                                        <option value="content_top" selected="selected"><?php echo $text_content_top; ?></option>
                                    <?php } else { ?>
                                        <option value="content_top"><?php echo $text_content_top; ?></option>
                                    <?php } ?>
                                    <?php if ($module['position'] == 'content_bottom') { ?>
                                        <option value="content_bottom" selected="selected"><?php echo $text_content_bottom; ?></option>
                                    <?php } else { ?>
                                        <option value="content_bottom"><?php echo $text_content_bottom; ?></option>
                                    <?php } ?>
                                    <?php if ($module['position'] == 'column_left') { ?>
                                        <option value="column_left" selected="selected"><?php echo $text_column_left; ?></option>
                                    <?php } else { ?>
                                        <option value="column_left"><?php echo $text_column_left; ?></option>
                                    <?php } ?>
                                    <?php if ($module['position'] == 'column_right') { ?>
                                        <option value="column_right" selected="selected"><?php echo $text_column_right; ?></option>
                                    <?php } else { ?>
                                        <option value="column_right"><?php echo $text_column_right; ?></option>
                                    <?php } ?>
                                </select></td>
                            <td class="left"><select name="harmas_module[<?php echo $module_row; ?>][status]">
                                    <?php if ($module['status']) { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                    <?php } ?>
                                </select></td>
                            <td class="right"><input type="text" name="harmas_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>
                            <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
                        </tr>
                        </tbody>
                        <?php $module_row++; ?>
                    <?php } ?>
                    <tfoot>
                    <tr>
                        <td colspan="6"></td>
                        <td class="left"><a onclick="addModule();" class="button"><?php echo $button_add_module; ?></a></td>
                    </tr>
                    </tfoot>
                </table>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        var module_row = <?php echo $module_row; ?>;

        function addModule() {
            html  = '<tbody id="module-row' + module_row + '">';
            html += '  <tr>';
            html += '    <td class="left"><input type="text" name="harmas_module[' + module_row + '][limit]" value="5" size="1" /></td>';
            html += '    <td class="left"><input type="text" name="harmas_module[' + module_row + '][image_width]" value="80" size="3" /> <input type="text" name="harmas_module[' + module_row + '][image_height]" value="80" size="3" /></td>';
            html += '    <td class="left"><select name="harmas_module[' + module_row + '][layout_id]">';
            <?php foreach ($layouts as $layout) { ?>
            html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
            <?php } ?>
            html += '    </select></td>';
            html += '    <td class="left"><select name="harmas_module[' + module_row + '][position]">';
            html += '      <option value="content_top"><?php echo $text_content_top; ?></option>';
            html += '      <option value="content_bottom"><?php echo $text_content_bottom; ?></option>';
            html += '      <option value="column_left"><?php echo $text_column_left; ?></option>';
            html += '      <option value="column_right"><?php echo $text_column_right; ?></option>';
            html += '    </select></td>';
            html += '    <td class="left"><select name="harmas_module[' + module_row + '][status]">';
            html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
            html += '      <option value="0"><?php echo $text_disabled; ?></option>';
            html += '    </select></td>';
            html += '    <td class="right"><input type="text" name="harmas_module[' + module_row + '][sort_order]" value="" size="3" /></td>';
            html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
            html += '  </tr>';
            html += '</tbody>';

            $('#module tfoot').before(html);

            module_row++;
        }
        </script>-->





<?php echo $footer; ?>