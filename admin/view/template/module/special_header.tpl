<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
          <table class="form">
              <tr>
                  <td><?php echo $entry_fejlec_latszik; ?></td>
                  <td colspan=1><?php if ($modules['fejlec']) { ?>
                          <input type="radio" name="special_header[fejlec]" value="1" checked="checked" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="special_header[fejlec]" value="0" />
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="special_header[fejlec]" value="1" />
                          <?php echo $text_yes; ?>
                          <input type="radio" name="special_header[fejlec]" value="0" checked="checked" />
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>
              </tr>
              <tr>
                  <td><?php echo $entry_limit; ?></td>
                  <td class="left"><input type="text" name="special_header[limit]" value="<?php echo $modules['limit']; ?>" size="1" /></td>
              </tr>

              <tr>
                  <td><?php echo $entry_kiemelt; ?></td>
                  <?php if (isset($modules['kiemelt']) && $modules['kiemelt'] == 1) { ?>
                      <td class="left"><input type="checkbox" name="special_header[kiemelt]" value="1" checked="checked" /></td>
                  <?php } else {?>
                      <td class="left"><input type="checkbox" name="special_header[kiemelt]" value="1" /></td>
                  <?php } ?>
              </tr>

              <tr>
                  <td><?php echo $entry_status; ?></td>
                  <td class="left">
                      <select name="special_header[status]">
                          <?php if ($modules['status']) { ?>
                              <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                              <option value="0"><?php echo $text_disabled; ?></option>
                          <?php } else { ?>
                              <option value="1"><?php echo $text_enabled; ?></option>
                              <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                          <?php } ?>
                      </select>
                  </td>
              </tr>

          </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>