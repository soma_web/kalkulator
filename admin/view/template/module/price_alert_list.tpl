<table class="list">
<thead>
  <tr>
	<td class="left"><?php echo $column_customer; ?></td>
	<td class="left"><?php echo $column_email; ?></td>
	<td class="left"><?php echo $column_product; ?></td>
	<td class="left"><?php echo $column_price_seen; ?></td>
	<td class="left"><?php echo $column_price_desired; ?></td>
	<td class="left"><?php echo $column_date_added; ?></td>
  </tr>
</thead> 
<tbody>
  <?php if ($alerts) { ?>
  <?php foreach ($alerts as $alert) { ?>
  <tr>
	<td class="left"><?php echo $alert['name']; ?></td>
	<td class="left"><?php echo $alert['email']; ?></td>
	<td class="left"><a href="<?php echo $alert['product_href']; ?>"><?php echo $alert['product_name']; ?></a></td>
	<td class="left"><?php echo $alert['product_price']; ?></td>
	<td class="left"><?php echo $alert['desired_price']; ?></td>
	<td class="left"><?php echo $alert['date_added']; ?></td>
  </tr>
  <?php } ?>
  <?php } else { ?>
  <tr>
	<td class="center" colspan="6"><?php echo $text_no_results; ?></td>
  </tr>
  <?php } ?>
</tbody>
</table>

<div class="pagination"><?php echo $pagination; ?></div>