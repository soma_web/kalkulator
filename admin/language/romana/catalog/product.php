<?php
// Heading
$_['heading_title']          = 'Produse'; 

// Text  
$_['text_success']           = 'Succes: Ai modificat produsul!';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Implicit';
$_['text_image_manager']     = 'Administrator De Imagine';
$_['text_browse']            = 'Adauga imagine';
$_['text_clear']             = 'Sterge imagine';
$_['text_option']            = 'Opțiuni';
$_['text_option_value']      = 'Valoarea Opțiunii';
$_['text_percent']           = 'Procent';
$_['text_amount']            = 'Sumă fixă';

// Column
$_['column_name']            = 'Numele Produsului';
$_['column_model']           = 'Model';
$_['column_image']           = 'Imagine';
$_['column_price']           = 'Preț';
$_['column_quantity']        = 'Cantitate';
$_['column_status']          = 'Status';
$_['column_action']          = 'Acțiune';

// Entry
$_['entry_name']             = 'Numele Produsului:';
$_['entry_meta_keyword'] 	 = 'Meta Tag Cuvinte Cheie';
$_['entry_meta_description'] = 'Meta Tag Descriere:';
$_['entry_description']      = 'Descriere:';
$_['entry_store']            = 'Magazine:';
$_['entry_keyword']          = 'Cuvinte SEO URL:<br/><span class="help">Acesta trebuie să fie unic global.Se refera la cuvantul(cuvintele) care prescrie adresa url cand SEO URL este pornit.Ex: in loc de situl.ro/index.php?route=product/product&product_id=x ar fii situl.ro/cuvantul-seo-url</span>';
$_['entry_model']            = 'Cod Produs (Model):';
$_['entry_cikkszam']         = 'Cod Produs (Product Number):';
$_['entry_cikkszam2']         = 'Cod Produs 2 (Product Number):';
$_['entry_sku']              = 'SKU:';
$_['entry_upc']              = 'UPC:';
$_['entry_location']         = 'Locație:';
$_['entry_manufacturer']     = 'Producător:';
$_['entry_fizetendo']        = 'Preț:';
$_['entry_shipping']         = 'Necesită Transport:';
$_['entry_date_available']   = 'Dată Disponibilitate:';
$_['entry_quantity']         = 'Cantitate:';
$_['entry_minimum']          = 'Cantitate Minimă:<br/><span class="help">Forțează o comandă minimă admisă</span>';
$_['entry_stock_status']     = 'Statusul produselor neexistente in stoc<br/><span class="help">Status arătat atunci cînd unui produs i s-a terminat stocul</span>';
$_['entry_price']            = 'Preț:';
$_['entry_tax_class']        = 'Clasa de taxa:';
$_['entry_points']           = 'Puncte:<br/><span class="help">Numarul de puncte ce se necesită pentru cumpărarea acestui produs. Dacă nu dorești să se poata cumpara acest produs cu ajutorul punctelor, lasă 0.</span>';
$_['entry_option_points']    = 'Puncte:';
$_['entry_subtract']         = 'Scadere stoc:<br/><span class="help">Specificati daca scade stocul produsului/optiunii atunci cand este cumparat</span>';
$_['entry_weight_class']     = 'Clasa Greutații:';
$_['entry_weight']           = 'Greutate:';
$_['entry_length']           = 'Clasa Lungimii:';
$_['entry_dimension']        = 'Dimensiuni (L x l x Î):';
$_['entry_image']            = 'Imagine:';
$_['entry_customer_group']   = 'Grupul Clienților:';
$_['entry_date_start']       = 'Data Începerii:';
$_['entry_date_end']         = 'Data Încheierii:';
$_['entry_priority']         = 'Prioritate:';
$_['entry_attribute']        = 'Atribute:';
$_['entry_attribute_group']  = 'Grupul Atributelor:';
$_['entry_text']             = 'Text:';
$_['entry_option']           = 'Opțiuni:';
$_['entry_option_value']     = 'Valoarea Opțiunii:';
$_['entry_required']         = 'Obligatoriu:';
$_['entry_status']           = 'Status:';
$_['entry_sort_order']       = 'Ordinea:';
$_['entry_category']         = 'Categorii:';
$_['entry_download']         = 'Descărcări:';
$_['entry_related']          = 'Produse Asemănătoare:';
$_['entry_tag']          	 = 'Etichete Produs:<br /><span class="help">separate prin virgula</span>';
$_['entry_reward']           = 'Puncte Recompensă:';
$_['entry_layout']           = 'Treci Peste Aspect:<br/><span class="help">Implicit, un produs afiseaza in pagina modulele care sunt asociate aspectului Produs.Aici puteti seta ca acest produs sa afiseze modulele asociate altui aspect la alegere.';

// Error
$_['error_warning']          = 'Atenție: Vă rugăm verificați formularul pentru greșeli!';
$_['error_permission']       = 'Atenție: Nu ai permisiunea să modifici produsele!';
$_['error_name']             = 'Numele Produsului trebuie sa fie mare de 3 si mai puțin de 255 caractere!';
$_['error_model']            = 'Modelul Produsului trebuie sa fie mai mare de 3 si mai mic de 64 caractere!';
?>