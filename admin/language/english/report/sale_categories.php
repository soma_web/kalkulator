<?php

// Heading
$_['heading_title']     = 'Sales in categories';

// Text
$_['select_kupon']          = 'Coupon';
$_['select_utalvany']       = 'Voucher';

// Column
$_['column_categories']         = 'Categories';
$_['column_ajanlat_kupon']      = 'Uploaded (coupon)';
$_['column_ajanlat_voucher']    = 'Uploaded (voucher)';
$_['column_vasarolt_kupon']     = 'Selled (coupon)';
$_['column_vasarolt_voucher']   = 'Selled (voucher)';


?>