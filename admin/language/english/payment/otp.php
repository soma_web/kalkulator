<?php

// Heading
$_['heading_title']      = 'OTP';

// Text
$_['text_payment']       = 'Payment';
$_['text_success']       = 'You have successfully changed the details of OTP module!';
$_['text_otp'] 			= '<img src="view/image/otp_simple_logo.jpg" alt="OTP Credit card payment" title="OTP Credit card payment" style="border: 1px solid #EEEEEE; width: 30px" />';

// Entry
$_['entry_privkey']      = 'OTP key value:<span class="help">eg.: #02299991</span>';
$_['entry_order_status'] = 'Order status:';
$_['entry_geo_zone']     = 'Geozone:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Order:';
$_['entry_haromszereplos']      = 'The relevantharomszereplosshop.conf file name:<span class="help">eg.: haromszereplos_hadrianus.conf</span>';
$_['entry_otp_webshop_client']  = 'The relevant otp_webshop_client.conf file name:<span class="help">eg.: otp_webshop_client_hadrianus.conf</span>';
$_['entry_total']        = 'Minimum amount:<br /><span class="help">The cashier of the value of the order must be at least that amount, to become active!</span>';

// Error
$_['error_permission']   = 'Warning: Changing the OTP payment is not allowed for you';
?>
