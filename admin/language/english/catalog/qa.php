<?php
// Heading
$_['heading_title']                 = 'Questions &amp; Answers';

// Text
$_['text_search']                   = 'Search';
$_['text_success_update']           = '<strong>Success!</strong> Question has been updated!';
$_['text_success_insert']           = '<strong>Success!</strong> Question has been inserted!';
$_['text_success_copy']             = '<strong>Success!</strong> %s question(s) copied!';
$_['text_success_delete']           = '<strong>Success!</strong> %s question(s) deleted!';
$_['text_toggle_navigation']        = 'Toggle navigation';
$_['text_confirm_delete']           = 'Confirm Deletion!';
$_['text_are_you_sure']             = 'Delete cannot be undone! Are you sure you want to proceed?';
$_['text_filter']                   = 'Filter';
$_['text_clear_filter']             = 'Clear filter';
$_['text_autocomplete']             = '(Autocomplete)';
$_['text_no_results']               = 'There are no questions';
$_['text_pagination']               = 'Showing questions {start} to {end} of {total} ({pages} pages)';
$_['text_filtered_from']            = '(filtered from a total of %s questions)';
$_['text_anonymous']                = '<em>Anonymous</em>';
$_['text_all_languages']            = 'All Languages';
$_['text_display_language']         = 'Display questions in the selected language';
$_['text_first_page']               = 'First';
$_['text_previous_page']            = 'Previous';
$_['text_next_page']                = 'Next';
$_['text_last_page']                = 'Last';
$_['text_question']                 = 'Question';
$_['text_answer']                   = 'Answer';
$_['text_insert']                   = 'Insert';
$_['text_update']                   = 'Update';

// Column
$_['column_product']                = 'Product';
$_['column_question_author_name']   = 'Asked By';
$_['column_question']               = 'Question';
$_['column_answer']                 = 'Answer';
$_['column_answer_author_name']     = 'Answered By';
$_['column_date_asked']             = 'Asked On';
$_['column_date_answered']          = 'Answered On';
$_['column_date_modified']          = 'Last Modified';
$_['column_store']                  = 'Store';
$_['column_status']                 = 'Status';
$_['column_action']                 = 'Action';

// Buttons
$_['button_apply']                  = 'Apply';

// Help
$_['help_notify']                   = 'Send an e-mail notification to the question author. Notification is sent only if the question has been answered.';

// Entry
$_['entry_id']                      = 'ID:';
$_['entry_product']                 = 'Product:';
$_['entry_language']                = 'Language:';
$_['entry_author_name']             = 'Author name:';
$_['entry_answer_name']             = 'Answerer name:';
$_['entry_customer']                = 'Customer:';
$_['entry_email']                   = 'Email:';
$_['entry_phone']                   = 'Phone:';
$_['entry_custom']                  = 'Custom:';
$_['entry_question']                = 'Question:';
$_['entry_answer']                  = 'Answer:';
$_['entry_status']                  = 'Status:';
$_['entry_date_asked']              = 'Date asked:';
$_['entry_date_answered']           = 'Date answered:';
$_['entry_date_modified']           = 'Date modified:';
$_['entry_stores']                  = 'Stores:';
$_['entry_update_date_answered']    = 'Update date answered';
$_['entry_notify']                  = 'Notify question author';

// Error
$_['error_permission']              = '<strong>Warning!</strong> You do not have permission to modify questions!';
$_['error_warning']                 = '<strong>Warning!</strong> Please check the form carefully for errors!';
$_['error_product']                 = 'Please select a product!';
$_['error_email']                   = 'Please enter a valid e-mail address!';
$_['error_question']                = 'Question text must be at least 1 character long!';
$_['error_ajax_request']            = 'An AJAX error occured!';

?>
