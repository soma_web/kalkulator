<?php

// Heading
$_['heading_title']       = 'Location';
$_['button_insert']       = 'Insert';
$_['button_delete']       = 'Delete';
$_['error_warning']       = '';
$_['success']             = '';
$_['column_elhelyezkedes']  = 'Pub groups';
$_['column_sort_order']   = 'Order';
$_['column_action']       = 'Action';
$_['column_maximum']      = 'Maximum quantity';
$_['column_ara']          = 'Price';
$_['column_status']       = 'Status';
$_['column_alcsoport_status']  = 'Sub-group status';


$_['entry_keret_kiemeles']     = 'highlight frame:';
$_['entry_hatter_kiemeles']    = 'Highlight Background:';
$_['entry_nagymeret']          = 'great Price:';
$_['entry_sorrend']          = 'Order:';
$_['entry_darabszam']          = 'Maximum number of items:';
$_['entry_ara']          = 'Price:';
$_['entry_status']          = 'Status:';
$_['entry_tax_class']       = 'Tax class:';
$_['entry_alcsoport_status']  = 'Sub-group status:';

$_['text_engedelyezett']    = 'Enabled:';
$_['text_letiltott']        = 'Disabled:';

?>