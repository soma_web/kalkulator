<?php

// Heading
$_['heading_title']       = 'Location';
$_['button_insert']       = 'Insert';
$_['button_delete']       = 'Delete';
$_['error_warning']       = '';
$_['success']             = '';
$_['column_kiemelesek']   = 'Pub groups';
$_['column_action']       = 'Action';
$_['column_maximum']      = 'Maximum quantity';
$_['column_ara']          = 'Price';
$_['column_status']       = 'Status';
$_['column_action']       = 'Action';
$_['column_sort_order']   = 'Order';
$_['column_idoszak']      = 'Period';
$_['column_maximum']      = 'Maximum quantity';


$_['entry_ara']           = 'Price:';
$_['entry_status']        = 'Status:';
$_['entry_class']         = 'Sablon neve:';
$_['entry_sorrend']       = 'Order:';
$_['entry_idoszak']       = 'Period:';
$_['entry_tax_class']     = 'Tax class:';
$_['entry_darabszam']     = 'Maximum quantity:';
$_['entry_beallitas']     = 'Where to use:';


$_['text_engedelyezett']  = 'Enabled:';
$_['text_letiltott']      = 'Disabled:';
$_['text_engedelyezett']  = 'Enabled:';
$_['text_letiltott']      = 'Disabled:';

$_['text_nap']            = 'Day';
$_['text_het']            = 'Seven';
$_['text_honap']          = 'Month';
$_['text_ev']             = 'Year';

$_['text_altalanos']         = 'All products termékhez';
$_['text_ingyenes']          = 'Only free product';
$_['text_egyeb']             = 'Pay only the product';
?>