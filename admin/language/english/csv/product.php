<?php

// Heading
$_['heading_title'] = 'Product CSV Backup / Restore';

$_['entry_backup']  = 'CSV export';
$_['entry_restore'] = 'CSV import:';

$_['text_success']  = 'Siker: Import database managed!';
$_['text_tol']      = 'item number:';
$_['text_db']       = 'piece:';
$_['text_sorrend']  = 'Order';

$_['button_backup'] = 'CSV export';
$_['button_restore']= 'CSV import';

// Error
$_['error_permission'] = 'Warning: Changing the backup is not allowed for you!';
$_['error_empty']      = 'Warning: the uploaded file is empty!';
$_['error_empty_result'] = 'Warning: there is no equivalent to the setting data!';
?>