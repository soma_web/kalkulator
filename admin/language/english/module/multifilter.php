<?php
// Heading
$_['heading_title']       = 'Filter'; 

// Text
$_['text_module']                   = 'Modules';
$_['text_success']                  = 'Success: You have modified module filter!';
$_['text_content_top']              = 'Content Top';
$_['text_content_bottom']           = 'Content Bottom';
$_['text_column_left']              = 'Column Left';
$_['text_column_right']             = 'Column Right';
$_['text_szukit']                   = 'Narrow';
$_['text_bovit']                    = 'Broaden';
$_['text_name']                     = 'Name';
$_['text_szin']                     = 'Color';
$_['text_megjelenites']             = 'Display';
$_['text_megjelenites_szincsoport'] = 'Color group';
$_['text_megjelenites_szinek']      = 'Color';

$_['checkox_tpl']                   = 'Checkbox';
$_['color_tpl']                     = 'Color';
$_['negyzetes_tpl']                 = 'Grid';
// Entry

$_['entry_szuro_neve']    = 'Filter name';
$_['entry_folyamat']      = 'Filter method';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';
$_['entry_template']      = 'Template name:';
$_['entry_teljes_lista']  = 'Full list:';
$_['entry_szuresgomb_latszik']  = 'Filter button:';
$_['entry_kereses_latszik']  = 'General search:';
$_['entry_ajaxos']          = 'Ajax coll:';

//$_['entry_kategoria_vezerel']   = 'Category filtering to control the:';
$_['entry_kategoria_checkbox']  = 'Checkbox <b>category </b> on:';
$_['entry_kategoria_normal']    = 'Normal <b>category </b> on:';
$_['entry_kategoria_sub']       = '<b>Sub Category </b>on:';
$_['entry_szuro_csoport']       = 'Filter group on:';
$_['entry_szuro']               = 'Sub filter on:';
$_['entry_meret']               = 'Size ';
$_['entry_szin']                = 'Color picker';

$_['entry_gyarto']              = 'Manufacturer on:';
$_['entry_tulajdonsagok']       = 'Properties on:';
$_['entry_valasztek']           = 'Range on:';
$_['entry_raktaron']            = 'On stock:';
$_['entry_ar_szures']           = 'Filter on price:';
$_['entry_ar_szures_szazalek']  = 'Filter percent on price';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module filter!';
?>