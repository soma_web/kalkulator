<?php
// Heading
$_['heading_title']     		  = 'Redirect not existing pages';
$_['heading_title_exception']     = 'Redirect not existing pages - Exceptions';

// Текст
$_['text_tracking']    	    = 'Tracking:';
$_['text_enabled']     	    = 'Enabled';
$_['text_disabled']     	= 'Disabled';
$_['text_success']      	= 'Success: We reserve the the right to change!';
$_['text_save']         	= 'Save...';
$_['text_save_redirects'] 	= 'Save Redirects';
$_['text_exceptions']   	= 'Exceptions';

// Column
$_['column_old_url']     = 'Original URL';
$_['column_new_url']     = 'New URL';
$_['column_referer']     = 'Refer';
$_['column_date_added']  = 'Add';
$_['column_action']      = 'Save';
$_['column_status']      = 'Status';

// Entry
$_['entry_old_url']    = 'Original URL';
$_['entry_new_url']    = 'New URL';
$_['entry_status']     = 'Status:';
$_['entry_exceptions'] = 'Exceptions:<br /><span class="help">The list of words. If present in the URL, it can not shall be recorded. The words should be separated by return.</span>';

// Error
$_['error_permission']    = 'Figyelem: Önnek nincs jogosultsága módosítani az átirányítás modult!';

$_['error_exists']     = 'Warning: the address is already exists!';
$_['error_url']   	   = 'Warning: the address is min 3, max 255 character!';
$_['error_save']       = 'Error: please try again later!';