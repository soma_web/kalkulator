<?php
// Heading
$_['heading_title']       = 'Price Alert';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Price Alert!';
$_['text_heading_title']  = 'Heading Title';
$_['text_image_manager']  = 'Image manager';
$_['text_only_image']     = 'Only image (banner type)';
$_['text_image_box']      = 'Image inside \'box\'';
$_['text_yes']            = 'Yes';
$_['text_no']             = 'No';
$_['text_browse']         = 'Browse';
$_['text_clear']          = 'Clear';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Entry
$_['entry_secret_code']   = 'Secret code: <span class="help">at least 5 characters! ( a-z, A-Z, 0-9 ) <br />Required for Cron Job</span>';
$_['entry_image']         = 'Image';
$_['entry_dimension']     = 'Image Dimension: (W x H)';
$_['entry_display_type']  = 'Display Type';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module Price Alert!';
$_['error_secret_code']   = 'Secret code: at least 5 characters! ( a-z, A-Z, 0-9 )';
$_['error_image']         = 'Image - required';
$_['error_dimension']     = 'Dimensions - required';
?>