<?php
// Heading
$_['heading_title']       = 'Wholesale Price List';
$_['common_title']		  = 'Wholesale Price List';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified Wholesale Price List module!';

//Entry
$_['entry_login']         = 'Customer Login Required:<br /><span class="help">Only enable price list when customer is logged in.</span>';
$_['entry_column_picture']  = 'Picture Column:';
$_['entry_column_model']    = 'Model Column:';
$_['entry_column_rate']     = 'Rate Column:';
$_['entry_column_buy']      = 'Buy Column:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify Wholesale Price List module!';
?>