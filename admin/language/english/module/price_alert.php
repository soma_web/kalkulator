<?php
// Heading
$_['heading_title']                   = 'Price Alert';

// Tab
$_['tab_setting']                     = 'General Settings';
$_['tab_popup']                       = 'Popup Settings';
$_['tab_customer_email']              = 'Customer Email Template';
$_['tab_admin_email']                 = 'Admin Email Template';
$_['tab_alert']           			  = 'Active alerts';
$_['tab_history']         			  = 'Alerts History';
$_['tab_help']           			  = 'Help';

// Column
$_['column_customer']     			  = 'Customer';
$_['column_email']     			      = 'Email';
$_['column_product']     			  = 'Product';
$_['column_price_seen']     		  = 'Price seen';
$_['column_price_desired']     		  = 'Price desired';
$_['column_date_added']     		  = 'Date added';
$_['column_sent']     		          = 'Sent time';
$_['column_action']                   = 'Action';

// Button
$_['button_view_email']               = 'View email';
$_['button_check_send']               = 'Check / Send now';

// Text
$_['text_module']                     = 'Modules';
$_['text_success']                    = 'Success: You have modified module Price Alert!';
$_['text_status_attention']           = 'Important: IF Status option is set to "Disabled" then Price Alert is not displayed on product page!';
$_['text_check_send']                 = 'Cron job will check/send alerts at hour specified in command.<br />To manually check & send email alerts press button \'Check / Send now\'';

// Entry
$_['entry_status']                    = 'Status:<span class="help">Enable or Disable Price Alert Extension</span>';
$_['entry_secret_code']               = 'Cron Job Secret code: <span class="help">at least 5 characters! ( a-z, A-Z, 0-9 )</span>';
$_['entry_admin_notification']        = 'Admin notification (email):<span class="help">send email to admin when customer add new price alert</span>'; 
$_['entry_use_html_email']            = 'Send Reminder with <a href="http://www.oc-extensions.com/HTML-Email">HTML Email Extension</a>:<span class="help"><br />If HTML Email Extension is not installed on your store then is used default html mail (like in old versions of this extensions)</span>';

$_['entry_popup_set_alert']           = 'Set Price Alert Message:<span class="help">message seen by customer below price area (on product page)</span>';
$_['entry_popup_short_explanation']   = 'Short explanation:<span class="help">about price alert</span>';
$_['entry_popup_name']                = 'Name label:<span class="help">Ex: Name:</span>';
$_['entry_popup_email']               = 'Email label:<span class="help">Ex: Email:</span>';
$_['entry_popup_desired_price']       = 'Desired price label:<span class="help">Ex: Desired Price:</span>';
$_['entry_popup_button_set_alert']    = 'Button "Set Alert":<span class="help">Ex: Text for button "send alert"</span>';
$_['entry_popup_error_name']          = 'Name error:<span class="help">Ex: Name is required!</help>';
$_['entry_popup_error_email']         = 'Email error:<span class="help">Ex: E-Mail Address does not appear to be valid!</help>';
$_['entry_popup_error_desired_price'] = 'Desired price error:<span class="help">Ex: Desired Price does not appear to be valid!</help>';
$_['entry_popup_error_price_already'] = 'Desired price already available error:<span class="help">Ex: Product price is already below desired price!</help>';
$_['entry_popup_success']             = 'Alert success message:<span class="help">Ex: Your alert was registered. Will let you to know when product has desired price!</help>';

$_['entry_subject']                   = 'Subject:';
$_['entry_message']                   = 'Message:<span class="help"><br /><u>Special Keywords:</u><br />{name} = customer name<br />{products_list} = details about products with desired price<br />{store_name} = store name</span>';
$_['entry_admin_message']             = 'Message:<span class="help"><br /><u>Special Keywords:</u><br />{customer_name} = customer name<br />{customer_email} = customer email<br />{product_name} = product name<br />{product_name_with_link} = product name with link<br />{product_price} = price seen by customer<br />{desired_price} = desired price</span>';

// Entry - Alert list
$_['entry_customer']                  = 'Customer:';
$_['entry_email']                     = 'Email:';
$_['entry_product']                   = 'Product:';
$_['entry_date_added']                = 'Date added:';

// Error
$_['error_permission']                = 'Warning: You do not have permission to modify module Price Alert!';
$_['error_in_tab']                    = 'Please check again - errors in tab %s';
$_['error_required']                  = 'This field is required!';
$_['error_secret_code']               = 'Secret code: at least 5 characters! ( a-z, A-Z, 0-9 )';
$_['error_html_email_not_installed']  = 'Email can\'t be sent with <a href="http://www.oc-extensions.com/HTML-Email">HTML Email Extension</a> because this extension is not available on your store. Please set option to Disabled!';
$_['error_subject']                   = 'Email subject - required';
$_['error_message']                   = 'Email message - required';
?>