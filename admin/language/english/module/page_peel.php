<?php
// Heading
$_['heading_title']       = 'Page peel'; 

// Text
$_['text_module']         = 'Modules';
$_['text_heading_title']  = 'Heading Title';
$_['text_success']        = 'Success: You have modified module Page peel!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_browse']         = 'Browse';
$_['text_clear']          = 'Clear';
$_['text_image_manager']  = 'Image Manager';

// Entry
$_['entry_small_image']   = 'Small image:<br />75 X 75';
$_['entry_big_image']     = '<span class="required">*</span> Big image:<br />500 X 500';
$_['entry_link']          = 'Link / New Window';
$_['entry_new_window']    = 'Open in<br /> New Window?';
$_['entry_activate_link'] = 'Activate only<br /> for this link';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module page pell!';
$_['error_small_image']   = 'Choose small image';
$_['error_big_image']     = 'Choose big image';
?>