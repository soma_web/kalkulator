<?php
// Heading
$_['heading_title']    = 'Pickup From Store';
$_['text_gls_csomagpont']  = 'GLS Package Point';

// Text 
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified pickup from store!';

// Entry
$_['entry_warehouse']       = 'View collection points (shops / stores):';
$_['entry_geo_zone']   = 'Geo Zone:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';

$_['entry_title_heading'] = 'Title:';
$_['entry_title_text'] = 'Shipping mode:';
$_['entry_mail']       = 'Additional Alert E-Mails:<br /><span class="help">Any additional emails you want to receive the alert email, in addition to the main store email. (comma separated)</span>';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify pickup from store!';
?>