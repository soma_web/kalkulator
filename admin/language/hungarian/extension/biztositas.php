<?php

// Heading
$_['heading_title']  = 'Biztosítás';

// Text
$_['text_install']   = 'Telepítés';
$_['text_uninstall'] = 'Eltávolítás';

// Column
$_['column_from']    = 'Vásárlás értéke -tól';
$_['column_to']  = 'Vásárlás értéke -ig';
$_['column_value']   = 'Biztosítás díj';
$_['button_insert']  = 'Beszúrás';
$_['button_modify']  = 'Modosítás';
$_['button_delete']  = 'Törlés';
$_['button_save']    = 'Mentés';
$_['button_cancel']  = 'Mégse';
$_['column_modosit']  = 'Módosítás';
$_['column_torol']  = 'Törlés';
$_['text_success']  = 'A művelet sikeresen végrehajtódott';

// Error
$_['error_permission']  = 'Figyelmeztetés: Biztosítás módosítása az Ön számára nem engedélyezett!';
?>
