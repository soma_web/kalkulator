<?php

$_['heading_title']    = 'Legújabb Termékek RSS Feed';

// Text
$_['text_feed']        = 'Termék Feeds';
$_['text_success']     = 'Sikeresen módosította a Legújabb Termékek RSS Feed modult!';

// Entry
$_['entry_status']     = 'Status:';
$_['entry_limit']      = 'Termék korlátozás:<br/><span class="help">A feed-ben felsorolt termékek mennyisége. Azok az oldalak amik rendszeresen új termékeket vesznek fel magasabb értéket kell, hogy itt megadjanak.</span>';
$_['entry_show_date']  = 'Dátum mutatása:';
$_['entry_show_image'] = 'Kép mutatása:';
$_['entry_show_price'] = 'Ár mutatása:';
$_['entry_image_size'] = 'Kép mérete:';
$_['entry_data_feed']  = 'Data Feed Url:<br/><span class="help">Ez az URL az Ön feed-jére mutat. <br/>(Nem szerkeszthető)</span>';

// Error
$_['error_integer']    = 'A mező egész számot kell, hogy tartalmazzon!';
?>