<?php

// Heading
$_['heading_title']                 = 'Műszerfal';

// Text
$_['text_overview']                 = 'Áttekintés';
$_['text_statistics']               = 'Statisztika';
$_['text_latest_10_orders']         = 'Utolsó 10 rendelés';
$_['text_total_sale']               = 'Összes eladás:';
$_['text_total_sale_year']          = 'Évi összes eladás:';
$_['text_total_order']              = 'Összes rendelés:';
$_['text_total_customer']           = 'Ügyfelek száma:';
$_['text_total_customer_approval']  = 'Jóváhagyásra várakozó ügyfelek:';
$_['text_total_review_approval']    = 'Jóváhagyásra várakozó értékelés:';
$_['text_total_affiliate']          = 'Partnerek száma:';
$_['text_total_affiliate_approval'] = 'Jóváhagyásra váró partnerek:';
$_['text_day']                      = 'Ma';
$_['text_week']                     = 'Ezen a héten';
$_['text_month']                    = 'Ebben a hónapban';
$_['text_year']                     = 'Ebben az évben';
$_['text_order']                    = 'Összes rendelés';
$_['text_customer']                 = 'Összes vásárló';

// Column
$_['column_order']                  = 'Rendelésszám';
$_['column_customer']               = 'Vevő';
$_['column_status']                 = 'Állapot';
$_['column_date_added']             = 'Érkezett';
$_['column_total']                  = 'Végösszeg';
$_['column_firstname']              = 'Keresztnév';
$_['column_lastname']               = 'Vezetéknév';
$_['column_action']                 = 'Művelet';

// Entry
$_['entry_range']                   = 'Nézet:';

$_['text_total_qa']                 = 'Kérdések száma:';
$_['text_total_qa_answer']          = 'Válaszra váró kérdések:';

?>
