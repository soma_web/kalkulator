<?php

// Heading
$_['heading_title']    = 'Képszerkesztő';

// Text
$_['text_uploaded']    = 'Siker: A file-t feltöltöttük.';
$_['text_file_delete'] = 'A file törölve!';
$_['text_create']      = 'Siker: A könyvtár elkészült!';
$_['text_delete']      = 'Siker: A file vagy könyvtár törölve!';
$_['text_move']        = 'Siker: A file vagy könyvtár mozgatva!';
$_['text_copy']        = 'Siker: A file vagy könyvtár átmásolva!';
$_['text_rename']      = 'Siker: A file vagy könyvtár átnevezve!';
$_['text_confirm_image'] = 'A törlés nem vonható vissza! Biztos, hogy törli a képet?';
$_['text_confirm_path']  = 'A törlés nem vonható vissza! Biztos, hogy törli a teljes mappát?';
$_['text_kozvetlen_url']  = 'Közvetlen url:';
$_['text_kozvetlen_feltolt']  = 'Feltölt';

// Entry
$_['entry_folder']     = 'Új mappa:';
$_['entry_move']       = 'Mozgatás:';
$_['entry_copy']       = 'Név:';
$_['entry_rename']     = 'Név:';

// Error
$_['error_select']     = 'Figyelmeztetés: Kérem jelöljön ki egy könyvtárat vagy filet!';
$_['error_file']       = 'Figyelmeztetés: Kérem jelöljön ki egy filet!';
$_['error_directory']  = 'Figyelmeztetés: Kérem jelöljön ki egy könyvtárat!';
$_['error_default']    = 'Figyelmeztetés: Nem lehet megváltoztatni az alapértelmezett könyvtárat!';
$_['error_delete']     = 'Figyelmeztetés: Nem törölheti ezt a könyvtárat!';
$_['error_filename']   = 'Figyelmeztetés: A filenév 3 és 255 karakter hosszúságú lehet!';
$_['error_missing']    = 'Figyelmeztetés: A file vagy könyvtár nemlétezik!';
$_['error_exists']     = 'Figyelmeztetés: Ezen a néven már létezik file vagy könyvtár!';
$_['error_name']       = 'Figyelmeztetés: Kérem, írjon be egy új nevet!';
$_['error_move']       = 'Figyelmeztetés: Könyvtárba mozgatás nem létezik!';
$_['error_copy']       = 'Figyelmeztetés: Ez a file vagy könyvtár nem másolható!';
$_['error_rename']     = 'Figyelmeztetés: Ez a könyvtár nem nevezhető át!';
$_['error_file_type']  = 'Figyelmeztetés: Helytelen file tipus!';
$_['error_file_size']  = 'Figyelmeztetés: A file 3MB -nál kisebb legyen, és nem több, mint 1000px magas/széles!';
$_['error_uploaded']   = 'Figyelmeztetés: A filet nem lehetett feltölteni ismeretlen okból!';
$_['error_permission'] = 'Figyelmeztetés: Az engedély megtagadva!';
$_['error_no_exists_file']= 'Figyelmeztetés: Ezen a néven nem létezik file, kérem javítsa!';
$_['error_exists_file']= 'Figyelmeztetés: A file előzőleg már fel lett töltve!';


// Button
$_['button_folder']    = 'Új mappa';
$_['button_delete']    = 'Töröl';
$_['button_move']      = 'Mozgatás';
$_['button_copy']      = 'Másol';
$_['button_rename']    = 'Átnevez';
$_['button_upload']    = 'Feltölt';
$_['button_refresh']   = 'Frissít';
$_['button_submit']    = 'Mehet';
$_['button_short']     = 'Rendez:';
$_['button_short_nevsor'] = 'Név szerint';
$_['button_short_datum']  = 'Dátum szerint';

?>
