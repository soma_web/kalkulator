<?php

// Text
$_['text_approve_subject']  		    = '%s - A fiókja aktív!';
$_['text_approve_welcome']  	= 'Üdvözöljük és köszönjük, hogy regisztrált a %s!';
$_['text_welcome']  		    = 'Üdvözöljük, és köszönjük regisztrációját a következő áruházban: %s!';
$_['text_approve_login']    		    = 'Fiókja elkészült. Most már bejelentkezhet e-mail címével és jelszavával a következő helyen:';
$_['text_approve_services'] 		    = 'Bejelentkezés után hozzáférhet előző rendeléseihez és szerkesztheti adatait.';
$_['text_approve_thanks']   		    = 'Köszönöm,';
$_['text_transaction_subject']  = '%s - Számla kredit';
$_['text_transaction_received'] = 'Ön %s kreditet kapott!';
$_['text_transaction_total']    = 'Összesen %s kreditje van.' . "\n\n" . 'A fiókján található kredit automatikusan levonásra kerül a következő vásárláskor.';
$_['text_reward_subject']       = '%s - Jutalom pontok';
$_['text_reward_received']      = 'Ön %s jutalompontot kapott!';
$_['text_reward_total']         = 'Összesen %s jutalompontja van.';

?>
