<?php

// Heading
$_['heading_title']           = 'Parcelforce 48';

// Text
$_['text_shipping']           = 'Szállítás';
$_['text_success']            = 'Sikeresen módosította a Parcelforce 48 szállítási módot!';
$_['text_gls_csomagpont']  = 'GLS Csomagpont megjelenítése';

// Entry
$_['entry_rate']              = 'Parcelforce 48 érték:<br /><span class="help">Adjon meg értéket 5,2 tizedes jegyig. (12345.67) Például: .1:1,.25:1.27 - 0.1Kg-nál kisebb vagy egyenlő súly ára 1.00, Weights less than or equal to 0.25g-nál kisebb vagy egyenlő, de 0.1Kg-nál nagyobb súly ára 1.27. Ne írjon be KG-t vagy szimbólumot.</span>';
$_['entry_display_weight']    = 'Szállítási súly kijelzése:<br /><span class="help">Akarja, hogy ki legyen jelezve a szállítási súly? (pl. Szállítási súly : 2.7674 Kg)</span>';
$_['entry_display_insurance'] = 'Biztosítás kijelzése:<br /><span class="help">Akarja, hogy ki legyen jelezve a szállítás biztosítása? (pl. Biztosítva 500-ig)</span>';
$_['entry_display_time']      = 'Szállítási idő kijelzése:<br /><span class="help">Akarja, hogy ki legyen jelezve a szállítási idő? (pl. Szállítás 3-5 napon belül)</span>';
$_['entry_compensation']      = 'Parcelforce48 Kompenzációs érték:<br /><span class="help">Adjon meg értéket 5,2 tizedes jegyig. (12345.67) Páldául: 34:0,100:1,250:2.25 - A biztosítás 34-kosár tartalomig 0.00 extra költséggel jár, 100-nál több és 250 tartalomig 2.25 extra költséggel jár. Ne adjon meg pénznem szimbólumot.</span>';
$_['entry_tax']               = 'Adóosztály:';
$_['entry_geo_zone']          = 'Földrajzi zóna:';
$_['entry_status']            = 'Állapot:';
$_['entry_sort_order']        = 'Sorrend:';
$_['entry_title_heading'] = 'Fejléc megnevezése:';
$_['entry_title_text'] = 'Szállítási mód megnevezése:';
$_['entry_mail']        = 'Egyéb figyelmeztető e-mailek:<br /><span class="help">Egyéb figyelmeztető e-mailek, amiket kapni szeretne, a közponzi áruház  e-mailjei mellett. (vesszővel elválasztva)</span>';


// Error
$_['error_permission']        = 'Figyelem! Nincs engedélye a percelforce 48 szállítás módosítására.';
?>
