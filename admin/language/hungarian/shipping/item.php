<?php

// Heading
$_['heading_title']     = 'Árucikkenkénti szállítási díj';

// Text
$_['text_shipping']    = 'Szállítás';
$_['text_success']     = 'Siker: Az árucikkenkénti szállítási díj módosítása megtörtént!';
$_['text_gls_csomagpont']  = 'GLS Csomagpont megjelenítése';

// Entry
$_['entry_cost']       = 'Költség:';
$_['entry_tax']        = 'Adóosztály:';
$_['entry_geo_zone']   = 'Földrajzi zóna:';
$_['entry_status']     = 'Állapot:';
$_['entry_sort_order'] = 'Sorrend:';
$_['entry_title_heading'] = 'Fejléc megnevezése:';
$_['entry_title_text'] = 'Szállítási mód megnevezése:';
$_['entry_mail']        = 'Egyéb figyelmeztető e-mailek:<br /><span class="help">Egyéb figyelmeztető e-mailek, amiket kapni szeretne, a közponzi áruház  e-mailjei mellett. (vesszővel elválasztva)</span>';

// Error
$_['error_permission'] = 'Figyelmeztetés: Az árucikkenkénti szállítási díj módosítása az Ön számára nem engedélyezett!';
?>
