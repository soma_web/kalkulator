<?php 
//-----------------------------------------------------
// Newsletter Enhancements for Opencart
// Created by @DmitryNek (Dmitry Shkoliar)
// exmail.Nek@gmail.com
//-----------------------------------------------------

// Heading
$_['heading_title']							= 'Hírlevél Sablonok';

// Entry
$_['entry_name'] 							= 'Név:';
$_['entry_image_size'] 						= 'Termék képmérete:<br/><span class="help">Szélesség / Magasság</span>';
$_['entry_show_prices'] 					= 'Termék ár mutatása:';
$_['entry_description_length'] 				= 'Termékleírás hossza:<br/><span class="help">Karakterszám</span>';
$_['entry_columns'] 						= 'Oszlopok száma a termékek mutatására:';
$_['entry_specials_count'] 					= 'Speciális ajánlat a termékek számára:<br /><span class="help">Termékek mennyisége speciális ajánlattal ami tartalmazza a sablont ha a "Speciális Ajánlatú Termékeket Tartalmazza" opció ki van választva. Nulla érték az összes termék kiválasztását jelenti.</span>';
$_['entry_latest_count'] 					= 'Legutóbbi termékek száma:<br /><span class="help">Legutóbbi termékek száma ami tartalmazza a sablont ha a "Tartalmazza az Utolsó Termékeket" opció ki van választva.</span>';
$_['entry_popular_count'] 					= 'Népszerű termékek száma:<br /><span class="help">Legnépszerűbb termékek száma ami tartalmazza a sablont ha a "Tartalmazza az Legnépszerűbb Termékeket" opció ki van választva.</span>';
$_['entry_heading_color'] 					= 'Címsor színe:<br /><span class="help">Termékblokkok címsorainak színe</span>';
$_['entry_heading_style'] 					= 'Címsor stílusa:<br /><span class="help">További css stílusok.</span>';
$_['entry_name_color'] 						= 'Terméknév színe:<br /><span class="help">Terméknév szövegének a színe</span>';
$_['entry_name_style'] 						= 'Terméknév stílusa:<br /><span class="help">További css stílusok.</span>';
$_['entry_model_color'] 					= 'Termékmodell színe:<br /><span class="help">Termékmodell szövegének a színe.</span>';
$_['entry_model_style'] 					= 'Termékmodell stílusa:<br /><span class="help">További css stílusok.</span>';
$_['entry_price_color'] 					= 'Termék ár színe:<br /><span class="help">Termék ár szövegének a színe.</span>';
$_['entry_price_style'] 					= 'Termék ár stílusa:<br /><span class="help">További css stílusok.</span>';
$_['entry_price_color_special'] 			= 'Termék speciális ár színe:<br /><span class="help">Termék speciális ár szövegének a színe.</span>';
$_['entry_price_style_special'] 			= 'Termék speciális ár stílusa:<br /><span class="help">További css stílusok.</span>';
$_['entry_price_color_when_special'] 		= 'Termék régi árának a színe ha a speciális ár elérhető:<br /><span class="help">Termék régi ára szövegének a színe.</span>';
$_['entry_price_style_when_special'] 		= 'Termék régi árának a stílusa ha a speciális ár elérhető:<br /><span class="help">További css stílusok.</span>';
$_['entry_description_color'] 				= 'Termékleírás színe:<br /><span class="help">Termékleírás szövegének a színe.</span>';
$_['entry_description_style'] 				= 'Termékleírás stílusa:<br /><span class="help">További css stílusok.</span>';
$_['entry_template_body'] 					= 'Sablontest:';
$_['entry_yes'] 							= 'Igen';
$_['entry_no'] 								= 'Nem';
$_['entry_subject'] 						= 'Tárgy:';
$_['entry_defined_text'] 					= 'Meghatározott termékek címsor szövege:';
$_['entry_special_text'] 					= 'Speciális ajánlatú termékek címsor szövege:';
$_['entry_latest_text'] 					= 'Legutóbbi termékek címsor szövege:';
$_['entry_popular_text'] 					= 'Legnépszerűbb termékek címsor szövege:';
$_['entry_categories_text'] 				= 'Kategória termékek címsor szövege:';
$_['entry_product_template'] 				= 'Termékek sablonjai:';
$_['entry_show_savings'] 					= 'Kedvezmény megtakarítások megjelenítése:';
$_['entry_savings_color'] 					= 'Kedvezmény megtakarítások színe:<br /><span class="help">Kedvezménymegtakarítások szövegének a színe.</span>';
$_['entry_savings_style'] 					= 'Kedvezménymegtakarítások stíusa:<br /><span class="help">További css stílusok.</span>';
$_['entry_savings_text'] 					= 'Termékekkedvezmény % szövege:';
$_['entry_custom_css'] 					    = 'További Head Tartalom:';
$_['entry_text_message'] 					= 'Egyszerű szöveg verzió:';
$_['entry_description_select'] 				= 'Válasszon termék leírást:';

// Text 
$_['text_success']							= 'Siker: A kijelölt elemek törölve!';
$_['text_success_save']						= 'Sablon sikeresen mentve!';
$_['text_success_copy']						= 'Sablon sikeresen másolva!';
$_['text_settings']							= 'Általános sablon beállítások';
$_['text_styles']							= 'Sablon stílus';
$_['text_template']							= 'Sablon';
$_['text_new_template']						= 'Új Sablon';
$_['text_message_info']						= 'Üzenet személyreszabó tagek: <pre>{name}, {lastname}, {email}, {reward}</pre> váltja fel a megfelelő értékekkel.<br/><br/><span style="display:none; color:red;">A személyreszabott üzenetek nem támogatottak Partnereknek és Marketing kedvezményzetteknek!</span>';
$_['text_description_select']				= 'Hosszú leírás';
$_['text_short_description_select']			= 'Rövid leírás';

// Button 
$_['button_edit']							= 'Szerkesztés';
$_['button_copy_default']					= 'Másolja az alapértelmezettet';

// Column 
$_['column_actions']						= 'Műveletek';
$_['column_name']							= 'Név';
$_['column_last_change']					= 'Utolsó módosítás';

// Error 
$_['error_delete']							= 'A sablon nem törölhető, mert használatban van, itt: &laquo;Newsletter Stats&raquo; vagy &laquo;Newsletter Draft&raquo; vagy &laquo;Newsletter Shedule&raquo;.';
$_['error_copy']							= 'Hiba a sablon másolása közben.';

?>
