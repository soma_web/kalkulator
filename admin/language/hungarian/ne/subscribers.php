<?php 
//-----------------------------------------------------
// Newsletter Enhancements for Opencart
// Created by @DmitryNek (Dmitry Shkoliar)
// exmail.Nek@gmail.com
//-----------------------------------------------------

// Heading
$_['heading_title']							= 'Feliratkozói Hírlevél';

// Entry
$_['entry_yes']								= 'Igen';
$_['entry_no']								= 'Nem';

// Button 
$_['button_subscribe']						= 'Feliratkozás';
$_['button_unsubscribe']					= 'Leiratkozás';

// Column 
$_['column_actions']						= 'Műveletek';
$_['column_name']							= 'Név';
$_['column_email']							= 'Email';
$_['column_customer_group']					= 'Vásárlói Csoportok';
$_['column_newsletter']						= 'Előfizetés';
$_['column_store']							= 'Áruház';
$_['column_language']						= 'Nyelv';

// Text
$_['text_default']							= 'Alapértelmezett';

?>
