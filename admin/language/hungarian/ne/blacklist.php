<?php 
//-----------------------------------------------------
// Newsletter Enhancements for Opencart
// Created by @DmitryNek (Dmitry Shkoliar)
// exmail.Nek@gmail.com
//-----------------------------------------------------

// Heading
$_['heading_title']							= 'Hírlevél Feketelistás E-mailek';

// Text
$_['text_success']           				= 'Siker: Hozzáadtad a(z) %s emaileket!';
$_['text_success_delete']           		= 'Siker: A kiválasztott emailek törölve!';
$_['text_add_info']							= 'Az emaileket vesszővel vagy új sorral lehet tagolni:';

// Column
$_['column_email']							= 'Email';
$_['column_date']							= 'Időpont hozzáadva';

?>
