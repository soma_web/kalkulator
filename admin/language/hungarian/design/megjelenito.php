<?
$_['heading_title']                = 'Termék megjelenítés';

$_['tab_data']                     = 'Rács nézet';
$_['tab_email']                    = 'Email';
$_['tab_view']                     = 'Nézet';
$_['tab_kosar']                    = 'Kosár - Fizetés';
$_['tab_email']                    = 'Email';
$_['tab_frontend_top']             = 'Frontend Beállítások';
$_['tab_backend_top']              = 'Backend Beállítások';
$_['tab_fektetett']                = 'Fektetett rács nézet';
$_['tab_allitott']                 = 'Állított rács nézet';
$_['tab_lista']                    = 'Lista nézet';
$_['tab_altalanos']                = 'Általános beállítások';
$_['tab_altalanosadmin']           = 'Admin beállítások';
$_['tab_frontend']                 = 'Frontend feltöltés beállítások';
$_['tab_frontend_adat_bekeres']    = 'Frontend adatok bekérése';
$_['tab_frontend_lista_megjeleni'] = 'Frontend lista megjelenítés';
$_['tab_product']                  = 'Termék aloldal nézet';
$_['tab_nomodul']                  = 'Modulok engedélyezése';
$_['tab_alapmodulok']              = 'Alapmodulok';
$_['tab_szallitas']                = 'Szállítás';
$_['tab_fizetes']                  = 'Fizetés';
$_['tab_termekful']                = 'Termék fülek';
$_['tab_termektulajdonsag']        = 'Termék adatok';
$_['tab_rendeles']                 = 'Rendelések';
$_['tab_vevok']                    = 'Vevők';
$_['tab_export']                   = 'Export/Import';
$_['tab_regisztracio']             = 'Regisztráció';
$_['tab_regisztracio_ceges']       = 'Céges regisztráció';
$_['tab_header']                   = 'Header';
$_['tab_regisztracio_elolvastam']  = 'Fiók feltételek';
$_['text_agree']           = 'Elolvastam és megértettem az <a class="colorbox" href="%s" alt="%s"><b>%s</b></a>-et és az <a class="colorbox" href="%s" alt="%s"><b>%s</b></a>ot!';
$_['text_altalanos']               = 'Általános beállítások';
$_['text_modulok']                 = 'Modulok';
$_['text_ujdonsag']                = 'Újdonság mező megjelenítése';
$_['text_csv_vevok_title']         = 'CSV Export';
$_['text_csv_vevok_setting']       = 'Vevők CSV letöltés engedélyezése';
$_['text_partnerkod_vevok_setting']= 'Integrációs partnerkód:';
$_['text_group_name']              = 'Csoport neve:';

$_['entry_tab_videok']                = 'Videók fül tiltása:';
$_['entry_tab_pdf']                   = 'Pdf feltöltés fül tiltása:';
$_['entry_admin_letoltheto']          = 'Kapcsolódó letölthető kép megjelenítés:';
$_['entry_mennyisegre_szur']          = 'Lekérdezések szűrése mennyiségre:';
$_['entry_ervenesseg_datumra_szur']   = 'Lekérdezések szűrése érvényességi dátumra:';
$_['entry_allitott']                = 'Állított nézet';
$_['entry_kosarba']                 = 'Kosár gomb';
$_['entry_brutto_ar']               = 'Burtto ár:';
$_['entry_netto_ar']                = 'Nettó ár:';
$_['entry_negativ_ar']              = 'Ár megjelenítés negatív előjellel:';
$_['entry_brutto_netto']            = 'Sorrend: Brutto -> Nettó:';
$_['entry_description']             = 'Termék leírás:';
$_['entry_ikonok']                  = 'Ikonok:';
$_['entry_szukitett_regisztracio']  = 'Szűkített regisztráció engedélyezése:';
$_['entry_vonalkod_kuldes']         = 'Vonalkód küldésének megjelenítése:';
$_['entry_netto_ujsor']             = 'Ár megjelenítése "nettó" szöveg alatt:';
$_['entry_ikon_helye']              = 'Megjelenítés helye:';
$_['entry_uj']                      = 'Új ikon:';
$_['entry_pardarab']                = 'Pár darab ikon:';
$_['entry_szazalek']                = 'Százalék ikon:';
$_['entry_mennyiseg']               = 'Mennyiség kiválasztás kosárhoz:';
$_['entry_csomagolasi_mennyiseg']   = 'Csomagolási mennyiség:';
$_['entry_slideshow']               = 'Teljes szélességű slideshow:';
$_['entry_kep_szoveg']              = 'Sorrend: Kép -> Termék név:';
$_['entry_csomagolas_admin']        = 'Termék adminban csomagolási mennyiség látszik:';
$_['entry_jobb_egerbolt']           = 'Jobb egérgomb tiltása:';
$_['entry_masolhato']               = 'Oldalakról kimásolás tiltása:';
$_['entry_kivansaglista']           = 'Kívánláglista látszik:';
$_['entry_osszehasonlitas']         = 'Összehasonlítás látszik:';
$_['entry_kiskepek_megjelenitese']  = 'Kisképek nagykep alatt:';
$_['entry_kiskepek_slider']         = 'Kisképek megjelenítése sliderként:';
$_['entry_footer_kategoria']        = 'Footer alján megjelenő kategória lista:';
$_['entry_pagination']              = 'Lapszámozás megjelenítése a felső sorban:';
$_['entry_termek_jobb_jobb_elso']       = 'Kívánságlista, összehasonlítás:';
$_['entry_megjelenit_kosar_kivansag']   = 'Kívánságlista, összehasonlítás (Kosárban):';
$_['entry_termek_jobb_jobb_masodik']    = 'Tweet, Facebook:';
$_['entry_termek_jobb_jobb_harmadik']   = 'Print megjelenítés:';
$_['entry_termek_neve_fejlec']      = 'Termék név fejlécben megjelenítés:';
$_['entry_product_model_termeknevben']      = 'Model megjelenítése termék név előtt:';
$_['entry_termek_neve_dobozban']    = 'Termék név megjelenítés:';
$_['entry_form_eredeti_ar']         = 'Eredeti ár mező megjelenítés:';
$_['entry_form_utalvany']           = 'Ingyenes termék mező megjelenítés:';
$_['entry_utalvany_sorrendben']     = 'Ingyenes mező alapján is rendez:';
$_['entry_utalvany_sorrendben_asc'] = 'DESC:';
$_['entry_fixmenu']                 = 'Fix menü az oldal tetején:';
$_['entry_kiszereles']                 = 'Kiszerelés megjelenítése:';
$_['entry_kiszerelesmertek']           = 'Kiszerelés mértékegység megjelenítése:';
$_['entry_fixfilter']                 = 'Fix szűrő a bal oldalon:';
$_['entry_meddig_kaphato']          = 'Admin meddig kapható mező megjelenítése:';
$_['entry_mikortol_kaphato']        = 'Admin mikortól kapható mező megjelenítése:';
$_['entry_form_termek_elhelyezkedes']   = 'Termék elhelyezkedés sorrend engedélyezése:';
$_['entry_termek_kiemeles_modositasa']  = 'Termék kiemelés felvitel, törlés engedélyezése:';

$_['entry_model_adat']                  = 'Model/Cikkszám/Cikkszám 2 megjelenítése';
$_['entry_netto_adat']                  = 'Nettó ár megjelenítése';
$_['entry_admin_cart_shipping_region']                  = 'Kosár - becsült szállítási költség - megye megjelenítése';
$_['entry_admin_cart_shipping_postcode']                  = 'Kosár - becsült szállítási költség - irányítószám megjelenítése';

$_['entry_nokep_megjelenites_tiltasa'] = 'No-image megjelenítés tiltása:';
$_['entry_kep_megjelenites_tiltasa']   = 'Képet tiltó kapcsoló engedélyezése:';


$_['entry_emailreg_jelszo']            = 'Jelszó megjelenítés tiltása';
$_['entry_lizing']                     = 'Lízing:';

$_['entry_admin_csomag_ajanlat']       = 'Csomag ajánlat engedélyezése';
$_['entry_admin_rovid_leiras']         = 'Rövid leírás engedélyezése';
$_['entry_admin_tesztek']              = 'Tesztek engedélyezése';
$_['entry_admin_tartozekok']           = 'Tartozékok engedélyezése';
$_['entry_admin_altalanos_tartozekok'] = 'Álatlános tartozékok engedélyezése';


$_['entry_kosarba_fk']                 = 'Kosár gomb';
$_['entry_brutto_ar_fk']               = 'Brutto ár:';
$_['entry_netto_ar_fk']                = 'Nettó ár:';
$_['entry_brutto_netto_fk']            = 'Sorrend: Brutto -> Nettó:';
$_['entry_description_fk']             = 'Termék leírás:';
$_['entry_ikonok_fk']                  = 'Ikonok:';
$_['entry_ikon_helye_fk']              = 'Megjelenítés helye:';
$_['entry_uj_fk']                      = 'Új ikon:';
$_['entry_pardarab_fk']                = 'Pár darab ikon:';
$_['entry_szazalek_fk']                = 'Százalék ikon:';
$_['entry_mennyiseg_fk']               = 'Mennyiség kiválasztás kosárhoz:';
$_['entry_csomagolasi_mennyiseg_fk']   = 'Csomagolási mennyiség:';
$_['entry_slideshow_fk']               = 'Teljes szélességű slideshow:';
$_['entry_kep_szoveg_fk']              = 'Sorrend: Kép -> Termék név:';
$_['entry_csomagolas_admin_fk']        = 'Termék adminban csomagolási mennyiség látszik:';
$_['entry_jobb_egerbolt_fk']           = 'Jobb egérgomb tiltása:';
$_['entry_masolhato_fk']               = 'Oldalakról kimásolás tiltása:';
$_['entry_kivansaglista_fk']           = 'Kívánláglista látszik:';
$_['entry_osszehasonlitas_fk']         = 'Összehasonlítás látszik:';
$_['entry_facebook_fk']                = 'Facebook megosztás látszik:';
$_['entry_email_fk']                   = 'Email küldés látszik:';
$_['entry_minicart_fk']                = 'Kicsi kosárba ikon:';
$_['entry_megnezemgomb_fk']            = 'Megnézem gomb:';

$_['entry_termek_kaphato']             = 'Meddig kaphato megjelenítés:';
$_['entry_termek_filter_csoport']      = 'Szűrő csoport megjelenítés:';
$_['entry_frontend_input_neve']        = 'Mező neve:';
$_['entry_termek_filter']              = 'Szűrő megjelenítés:';
$_['entry_megnevezes']                 = 'Termék neve:';
$_['entry_images']                     = 'További képek:';


$_['entry_tulajdonsag_ful']            = 'Tulajdonság fül tiltása:';
$_['entry_valasztek_ful']              = 'Választék fül tiltása:';
$_['entry_mennyisegi_ful']             = 'Mennyiségi kedvezmény fül tiltása:';
$_['entry_vevo_ful']                   = 'Vevő Kedvezmény fül tiltása:';
$_['entry_akcios_ful']                 = 'Akciós ár fül tiltása:';
$_['entry_kep_ful']                    = 'Kép fül tiltása:';
$_['entry_jutalom_ful']                = 'Jutalom pontok fül tiltása:';
$_['entry_elhelyezkedes']              = 'Elhelyezkedés - kiemelés tiltása:';

$_['entry_emailreg']                   = 'Regisztráció - email';
$_['entry_emailreg_jelszo']            = 'Jelszó küldése:';
$_['entry_video']                      = 'Videó URL-hez mező: ';


$_['entry_termek_quantity']            = 'Mennyiség kiválasztás kosárhoz:';
$_['entry_termek_social']              = 'Social ikonok megjelenítése:';
$_['entry_termek_leiras']              = 'Temékleírás tab-on kívüli megjelenítése:';

$_['entry_lista_kosarba']                 = 'Kosár gomb';
$_['entry_lista_brutto_ar']               = 'Burtto ár';
$_['entry_lista_netto_ar']                = 'Nettó ár';
$_['entry_lista_brutto_netto']            = 'Sorrend: Brutto -> Nettó';
$_['entry_lista_description']             = 'Termék leírás:';
$_['entry_lista_uj']                      = 'Új ikon:';
$_['entry_lista_pardarab']                = 'Pár darab ikon:';
$_['entry_lista_szazalek']                = 'Százalék ikon:';
$_['entry_lista_mennyiseg']               = 'Mennyiség kiválasztás kosárhoz';
$_['entry_lista_csomagolasi_mennyiseg']   = 'Csomagolási mennyiség';
$_['entry_lista_kivansaglista']           = 'Kívánláglista látszik:';
$_['entry_lista_osszehasonlitas']         = 'Összehasonlítás látszik:';
$_['entry_termek_kapcsolod_allitott']     = 'Kapcsolódó termékek állított megjelenítés:';
$_['entry_lista_mennyisegi']              = 'Mennyiségi kedvezmény ikon:';


$_['entry_frontend_feltoltes']           = 'Frontend termékfeltöltés:';
$_['entry_termek_kapcsolod_group_filter']= 'Szűrő csoport -> szűrő megjelenítése:';
$_['entry_product_jelentkezzen']         = 'Jelentkezzen be kiírás látszik:';
$_['entry_product_elerhetoseg']          = 'Elérhetőség megjelenítés:';
$_['entry_product_suly']                 = 'Súly megjelenítés:';
$_['entry_product_mennyisegi_egyseg']    = 'Mennyisegi egyseg megjelenítés:';
$_['entry_product_cikkszam']             = 'Cikkszám megjelenítés:';
$_['entry_product_cikkszam2']            = 'Cikkszám 2 megjelenítés:';
$_['entry_product_model']                = 'Modell megjelenítés:';
$_['entry_product_gyarto']               = 'Gyartó megjelenítés:';
$_['entry_product_reward']               = 'Jutalom pontok:';
$_['entry_product_sku']                  = 'SKU:';
$_['entry_product_meret']                = 'Méret:';
$_['entry_product_sorrend']              = 'Sorrend:';
$_['entry_admin_szazalek']               = 'Ár megjelenítéséhez százalék válaszó:';
$_['entry_admin_eredeti_ar']             = 'Eredeti ár:';


$_['entry_product_velemeny_megosztas_doboz']= 'Vélemény - Megosztás doboz';

$_['entry_product_leiras']                  = 'Rövid leírás a termékkép mellett:';

$_['entry_product_koztes']                  = 'Képek - Tabok közti sáv megjelenítése:';
$_['entry_product_garancia']                = 'Garancia:';
$_['entry_product_szallitas']               = 'Szállítás:';
$_['entry_product_velemeny']                = 'Vélemény:';
$_['entry_product_osszehasonlit_kivansag']  = 'Összehasonlítás, kívánságlista:';
$_['entry_product_addthis']                 = 'Addthis (social ikonok):';



$_['entry_product_mennyisegi_ikon']        = 'Mennyiségi kedvezmény ikon:';

$_['entry_admin_sorrend']                = 'Sorrend:';


$_['entry_admin_model']                  = 'Admin termék Model megjelenítése: listában';
$_['entry_admin_cikkszam']               = 'Admin termék Cikkszám megjelenítése: listában';
$_['entry_admin_cikkszam2']              = 'Admin termék Cikkszám II. megjelenítése: listában';

$_['entry_form_model']                  = 'Model megjelenítése: ';
$_['entry_form_cikkszam']               = 'Cikkszám megjelenítése: ';
$_['entry_form_cikkszam2']              = 'Cikkszám II. megjelenítése: ';

$_['entry_form_min_mennyiseg']          = 'Minimális mennyiség megjelenítése: ';
$_['entry_form_szallitando']            = 'Szállítandó termék megjelenítése: ';
$_['entry_form_meret']                  = 'Méret megjelenítése: ';
$_['entry_form_hosszmertek']            = 'Hosszmérték megjelenítése: ';
$_['entry_form_suly']                   = 'Súly megjelenítése: ';
$_['entry_form_sulymertek']             = 'Súlymérték megjelenítése: ';
$_['entry_form_ketszintu_szuro']        = 'Kétszintű szűrő engedélyezése: ';
$_['entry_form_kapcsolodo_partnerek']        = 'Kapcsolódó partnerek megjelenítése: ';

$_['entry_ajandekutalvanyok_ful']   = 'Ajándékutalványok fül tiltása: ';
$_['entry_kupon']                   = 'Kupon tiltása: ';
$_['entry_ajandekutalvany']         = 'Ajándékutalvány tiltása: ';
$_['entry_jutalom']                 = 'Jutalom tiltása: ';

$_['entry_admin_keres_model']            = 'Szűrés model alapján:';
$_['entry_admin_keres_cikkszam']         = 'Szűrés cikkszám alapján:';
$_['entry_admin_keres_cikkszam2']        = 'Szűrés cikkszám2 alapján:';

$_['entry_kosar_kupon']                 = 'Kosárnál: Kupon, száll.ktg. számítás megjelenítés:';

$_['entry_megjelenit_minicart_kosar']   = 'Minicart megjelenítés';

$_['kep_szoveg']                        = 'kép és szöveg';
$_['kep_szam']                          = 'kép és darab';
$_['kep_szoveg_szam']                   = 'kép,szöveg és darab';
$_['kep_szam_szoveg']                   = 'kép,darab és szöveg';
$_['szam_kep']                          = 'darab és kép';
$_['szam_szoveg']                       = 'darab és szöveg';
$_['szam_kep_szoveg']                   = 'darab,kép és szöveg';
$_['szam_szoveg_kep']                   = 'darab,szöveg és kép';
$_['szoveg_szam']                       = 'szöveg és darab';
$_['szoveg_kep']                        = 'szöveg és kép';
$_['szoveg_szam_kep']                   = 'szöveg,darab és kép';
$_['szoveg_kep_szam']                   = 'szöveg,kép és darab';

$_['entry_kosar_suly']                  = 'Kosárnál: Fejlécben súly megjelenítés:';
$_['entry_penztar_fizetesi_mod']        = 'Pénztár: Fizetési mód megjelenítése:';
$_['entry_penznem_kiiras']              = 'Pénznem teljes nevének megjelenítése';
$_['entry_footer_popup']                = 'Láblécben popup bekapcsolása';
$_['entry_vizjel']                      = 'Vízjelzés bekapcsolva:';
$_['entry_admin_nyelv_ellenorzes']      = 'Adminban nyelvi ellenőrzés engedélyezése:';

$_['entry_admin_ar_ellenorzes']              = 'Adminban ar ellenőrzés engedélyezése:';
$_['entry_admin_ketszintu_szuro_ellenorzes'] = 'Adminban kétszintű szűrő engedélyezése:';
$_['entry_admin_ervenyes_ig_ellenorzes']     = 'Adminban ervényesség dátuma mező ellenőrzés engedélyezése:';

$_['entry_category_leiras']             = 'Kategória: Leírás megjelenítése:';
$_['entry_category_kep_fejlecben']      = 'Kategória: Fejléc alatt kategória kép megjelenítése:';
$_['entry_category_kep_fejlec_felett']  = 'Kategória: Fejléc felett kategória kép megjelenítése:';
$_['entry_category_null_fejlecben']     = 'Fejlécben nulla elemű kategória megjelenítése';
$_['entry_piacter']                     = 'Piactér bekapcsolva:';
$_['entry_mini_cart']                   = 'Kosár gomb teljes szöveg megjelenítés:';
$_['entry_kosar_stilus']                   = 'Kosár megjelenítése szöveggel és árral:';
$_['entry_vatera']                      = 'Vatera modul engedélyezése';
$_['entry_description_karakter_szam']   = 'Megjelenítendő karakterek száma:';
$_['entry_description_karakter_szam_fk']   = 'Megjelenítendő karakterek száma:';
$_['entry_gorgetosav']                  = 'Körhinta és görgetés a modulokra admin megjelenítése:';
$_['entry_sap']                          = 'SAP modulul engedelyezése:';
$_['entry_gls']                          = 'GLS Csomagpont bekapcsolható:';
$_['entry_elolnezet']                    = 'Előnézeti kép:';


$_['ikon_fent_vizszintes']  = "Doboz tetején vízszintes elrendezés";
$_['ikon_fent_fuggoleges']  = "Doboz tetején függőleges elrendezés";
$_['ikon_kep_felett']       = "Kép tetején vízszintes elrendezés";
$_['ikon_kep_mellett']      = "Kép mellett függőleges elrendezés";



$_['entry_stock_status_id']             = 'Raktár állapot id:';
$_['entry_stock_date']                  = 'Beérkezés várható dátumának megjelenítése';
$_['entry_manufacturer_id']             = 'Gyártó id:';
$_['entry_tax_class_id']                = 'Adó osztály id:';
$_['entry_weight_class_id']             = 'Súly osztály id:';
$_['entry_width']                       = 'Szélesség:';
$_['entry_ellenorzes']                  = 'Ellenőrzés:';
$_['entry_height']                      = 'Magasság:';
$_['entry_length_class_id']             = 'Hossz osztály id:';
$_['entry_date_added']                  = 'Hozzáadva:';
$_['entry_date_modified']               = 'Módosítva:';
$_['entry_viewed']                      = 'Megtekintve:';
$_['entry_megyseg']                     = 'Megyseg:';
$_['entry_csomagolasi_egyseg']          = 'Csomagolási egység:';
$_['entry_ar_latszik']                  = 'Ár látszik:';
$_['entry_date_ervenyes_ig']            = 'Meddig kapható:';
$_['entry_darabaru_meter']              = 'Darabáru méter:';
$_['entry_admin_elhelyezkedes']         = 'Elhelyezkedés:';
$_['entry_ellenorzokod']                = 'Ellenörzőkód:';

$_['entry_registerblock_szemelyes']                     = 'Személyes adatok blokk:';
$_['entry_registration_default']                        = 'Alapértelmezett regisztrációs beállítások:';
$_['entry_registration_company']                        = 'Alapértelmezett céges regisztrációs beállítások:';
$_['entry_registerblock_cim']                           = 'Címadatok blokk:';
$_['entry_registerblock_ceges_cim']                     = 'Cégadatok blokk:';
$_['entry_registerblock_paypal']                        = 'Paypal blokk:';
$_['entry_registerblock_hirlevel']                      = 'Hírlevél blokk:';

$_['entry_register_vezeteknev']                         = 'Vezetéknév:';
$_['entry_register_keresztnev']                         = 'Keresztnév:';
$_['entry_register_iskolai_vegzettseg']                 = 'Iskolai végzettség:';
$_['entry_register_cegnev']                             = 'Cégnév:';
$_['entry_register_adoszam']                            = 'Adószám:';
$_['entry_register_vallalkozasi_forma']                 = 'Vállalkozási forma:';
$_['entry_register_szekhely']                           = 'Székhely:';
$_['entry_register_ugyvezeto_neve']                     = 'Ügyvezető neve:';
$_['entry_register_ugyvezeto_telefonszama']             = 'Ügyvezető telefonszáma:';
$_['entry_register_email']                              = 'Email cím:';
$_['entry_register_email_megerosites']                  = 'Email cím megerősítése:';
$_['entry_register_telefon']                            = 'Telefonszám:';
$_['entry_register_fax']                                = 'Fax:';
$_['entry_register_paypal']                             = 'PayPal email ID:';
$_['entry_nem_regisztracio']                            = 'Nem:';
$_['entry_eletkor_regisztracio']                        = 'Életkor:';

$_['entry_register_address_cegnev']                     = '(címadatok) Cégnév:';
$_['entry_register_address_adoszam']                    = '(címadatok) Adószám:';
$_['entry_register_address_vallalkozasi_forma']         = '(címadatok) Vállalkozási forma:';
$_['entry_register_address_szekhely']                   = '(címadatok) Székhely:';
$_['entry_register_address_ugyvezeto_neve']             = '(címadatok) Ügyvezető neve:';
$_['entry_register_address_ugyvezeto_telefonszama']     = '(címadatok) Ügyvezető telefonszáma:';
$_['entry_register_address_street']                     = '(címadatok) Utca, házszám megjelenítése:';
$_['entry_register_address_kiegeszito']                 = '(címadatok) Kiegészítő címadatok:';
$_['entry_register_address_city']                       = '(címadatok) Város megjelenítése:';
$_['entry_register_address_postcode']                   = '(címadatok) Irányítószám megjelenítése:';
$_['entry_register_address_country']                    = '(címadatok) Ország megjelenítése:';
$_['entry_register_address_region']                     = '(címadatok) Megye megjelenítése:';


$_['entry_product_gyarto_termeknevben']                     = 'Gyártó megjelenítése termék név előtt:';

$_['entry_register_hirlevel_kategoriak']                = 'Hírlevél kategóriák';

$_['text_success']   = 'Sikeresen módosította a beállításokat!';

$_['entry_kepek_helyszinenkent']                        = 'Képek helyszínenként tiltása:';

$_['entry_admin_biztonsagi_kod']                              = 'Biztonsági kód a termékekhez:';
$_['entry_emailreg']                        = 'Regisztráció';




$_['entry_regisztracios_email_jelszo']                              = 'Jelszó elküldése:';

$_['entry_cimmodositas_a_szerkeztesben']                = 'Cím módosítása a fiók szerkeztés oldalon:';

$_['entry_emlekezz_ram']                                = 'Emlékezz rám funkció bejelentkezésnél:';
$_['entry_facebook_login_fiok']                              = 'Facebook bejelentkezés a fióknál:';
$_['entry_facebook_login_penztar']                              = 'Facebook bejelentkezés a pénztárnál:';
$_['entry_cookie_figyelmeztetes']                       = 'Figyelmeztetés a Cookie használatra:';
$_['entry_megjelenit_popup_eredeti_meret']              = 'A termék aloldalon, a felugró ablakban lévő kép eredeti méretének megjelenítése</br>(admin kép méret beállítás figyelmen kívül hagyása):';
$_['entry_admin_nincs_negativ_ar']                      = 'Nincs nullás/negativ ár a fizetős terméknél:';
$_['entry_admin_seourl_generalas']                      = '301 URL generálás gomb:';
$_['entry_admin_termek_cimkek']                         = 'Termék cimkék tiltása:';
$_['entry_admin_letoltheto_kep']                        = 'Letölthető kép engedélyezése:';

$_['entry_lejarat_nelkuli']                         = 'Lejárat nélküli kapcsoló:';
$_['entry_default']                                 = 'Alapértelmezett:';

$_['entry_tabs_settings']                     = 'Fülek beállítása:';
$_['entry_product_tab_description']           = 'Leírás fül megjelenítése:';
$_['entry_product_tab_meret']                 = 'Méret fül megjelenítése:';
$_['entry_product_tab_attribute_groups']      = 'Tulajdonságok fül megjelenítése:';
$_['entry_product_tab_review_status']         = 'Vélemények fül megjelenítése:';
$_['entry_product_tab_products']              = 'Kapcsolódó termékek fül megjelenítése:';
$_['entry_product_tab_test_description']      = 'Teszt fül megjelenítése:';
$_['entry_product_tab_image']                 = 'Képek fül megjelenítése:';



?>