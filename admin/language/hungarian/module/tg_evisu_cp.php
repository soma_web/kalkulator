<?php
// Heading

$_['heading_title']    = 'NMS Net Vezérlőpult';

// Text
$_['text_module']      = 'Modulok';
$_['text_success']     = 'Sikeres módosítás!';

// Tab
$_['tab_gen']         = 'Alapbeállítások';

// Entry
$_['entry_color']   = 'Alap támafelület:';
$_['entry_currency']   = 'Pénznem:';
$_['entry_language']   = 'Nyelv:';
$_['entry_view']  	 = 'Alapnézet: <br /><span class="help">Rács vagy lista nézet</span>';
$_['entry_product_view']  	 = 'Alap termékoldal kiosztás: <br /><span class="help">Alapfül vagy Accordeon stílus</span>';
$_['entry_status']     = 'Állapot:';



// Error
$_['error_permission'] = 'Nincs módosítási engedélye!';

?>