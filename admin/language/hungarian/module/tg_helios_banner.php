<?php
// Heading
$_['heading_title']       = 'TG Helios Header Banner';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Sikerült! Sikeresen módosította a bannert!';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Tartalom alja';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';

// Entry
$_['entry_banner']        = 'Banner:';
$_['entry_dimension']     = 'Dimension (W x H):';
$_['entry_layout']        = 'Elrendezés:';
$_['entry_position']      = 'Pozíció:';
$_['entry_status']        = 'Statusz:';
$_['entry_sort_order']    = 'Rendezési sorrend:';

// Error
$_['error_permission']    = 'Figyelmeztetés: Nincs jogosultsága módosítani bannert!';
$_['error_dimension']     = 'Szélesség, magasság, méret szükséges!';
?>