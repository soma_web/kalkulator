<?php

// Heading
$_['heading_title']    = 'Sokszintű alkategória';

// Text
$_['text_module']      = 'Modulok';
$_['text_success']        = 'Sikeresen módosította az alkategória modult!';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Taratalom alja';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';

// Entry
$_['entry_layout']        = 'Elrendezés:';
$_['entry_position']      = 'Pozíció:';
$_['entry_status']        = 'Állapot:';
$_['entry_sort_order']    = 'Sorrend:';
$_['entry_count']         = 'Termékek száma:';
$_['entry_fejlec_latszik'] = 'Fejléc megjelenítése:';
$_['entry_multi_sub_category_list'] = 'Alkategória lista megjelenítése:';


// Error
$_['error_permission'] = 'Figyelmeztetés: Az alkategória modul módosítása az Ön számára nem engedélyezett!';
?>
