<?php
// Heading
$_['heading_title']       = 'Wordpress';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Sikeresen módosította a wordpress modult!';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Tartalom alja';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';

// Entry
$_['entry_items_limit']   = 'Number of threads displayed - Szálak száma megjelenítve';
$_['entry_code']          = 'az Ön wordpress könyvtárának címe url:(http://www.12vcity.com/wordpress/), támogatás:<a href="http://www.12vcity.com">http://www.12vcity.com</a>';
$_['entry_layout']        = 'Elrendezés:';
$_['entry_position']      = 'Pozíció:';
$_['entry_status']        = 'Állapot:';
$_['entry_sort_order']    = 'Sorrend:';

// Error
$_['error_permission']    = 'Figyelem: Önnek nincs jogosultsága módosítani a wordpress modult!';
$_['error_code']          = 'Kód szükséges';
?>