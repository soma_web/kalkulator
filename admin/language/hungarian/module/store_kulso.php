<?php

// Heading
$_['heading_title']       = 'Áruház választó kezdőlap';

// Text
$_['text_module']      = 'Modulok';
$_['text_slider']      = 'Slider beállításai';
$_['text_slider_use']  = 'Slider használata';
$_['text_success']     = 'Sikeresen módosította az áruház modult!';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Taratalom alja';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';

// Entry
$_['entry_admin']         = 'Csak rendszergazdák:';
$_['entry_text']          = 'Szövegek tiltása:';
$_['entry_mirror']        = 'Kép tükrözése:';
$_['entry_opacity']       = 'Szélső képek átlátszósága (0.0 - 1.0):';
$_['entry_speed']         = 'Görgetési gyorsaság (0.0 (ki) - 1.0):';
$_['entry_around']        = 'Körbejárás:';
$_['entry_title']         = 'Kép aláírás:';
$_['entry_layout']        = 'Elrendezés:';
$_['entry_position']      = 'Pozíció:';
$_['entry_status']        = 'Állapot:';
$_['entry_sort_order']    = 'Sorrend:';
$_['entry_width']         = 'Áruház kép szélesség:';
$_['entry_height']        = 'Áruház kép magasság:';
$_['entry_egyeb_link']    = 'Egyéb link engedélyezése:';
$_['entry_image']         = 'Kép a linkhez:';
$_['entry_egyeb_link_title'] = 'Felirat a linkhez:';
$_['entry_egyeb_link_link']  = 'Link:';


// Error
$_['error_permission'] = 'Figyelmeztetés: Az áruház modul módosítása az Ön számára nem engedélyezett!';
?>
