<?php
// Heading
$_['heading_title']       = 'Elhagyott kosár emlékeztető';

// Tab
$_['tab_setting']         = 'Beállítások';
$_['tab_preview']         = 'Emlékeztetők küldése/ellenőrzése';
$_['tab_help']            = 'Segítség';

// Button
$_['button_preview']      = 'Előnézet';
$_['button_send']         = 'Küldés most';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Siker: módosította az Elhagyott kosár emlékeztető modult!';
$_['text_coupon_fixed']   = 'Meghatározott összeg';
$_['text_coupon_percent'] = 'Százalék';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Tartalom Bottom';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';
$_['text_preview_info']   = 'Fontos: Mentsen (előnézet lekérése előtt),  ha módosította a beállításokat, különben a régi beállításokat fogja használni a modul. Az előnézet funkció véletlenszerűen választ egyet a kiküldésre váró emlékeztetők közül.';

// Entry
$_['entry_secret_code']   = 'Titkos kód: <span class="help">Legalább 5 karakter! ( a-z, A-Z, 0-9 ) <br />A Cron alkalmazáshoz kell.</span>';
$_['entry_delay']         = 'Késleltetés (napokban): <span class="help">Napok száma, ami után a kosár inaktívnak minősül és küldhető neki emlékeztető.</span>';
$_['entry_max_reminders'] = 'Legfeljebb hány emlékeztetőt küldjünk ki: <span class="help">Az emlékeztetők maximális száma, amit a válasz nélküli vásárlóknak küldhetünk.<br /> Ajánlott szám: 3 </span>';
$_['entry_use_html_email']   = 'Send Reminder with <a href="http://www.oc-extensions.com/HTML-Email">HTML Email Extension</a>:<span class="help"><br />If HTML Email Extension is not installed on your store then is used default html mail (like in old versions of this extensions)</span>';
$_['entry_log_admin']     = 'Összegzés küldése a webáruház tulajdonosnak:<span class="help">A webáruház tulajdonos egy listát kap az értesített vásárlókról, akiknek elhagyott kosara van.</span>';
$_['entry_add_coupon']    = 'Kupon hozzáadása: <span class="help">Kupon ajándékozása a vásárlónak a vásárlásra biztatás érdekében.</span>';
$_['entry_coupon_type']   = 'Kupon típusa:';
$_['entry_coupon_amount'] = 'Kupon összege:<span class="help">Ha Százalék = X% <br />Ha meghatározott összeg = X [pénznem]</span>';
$_['entry_coupon_expire'] = 'A kupon lejár (napokban):';
$_['entry_reward_limit']  = 'Hány alkalommal ajándékozhatjuk meg kuponnal a vásárlót?<span class="help">Üres mező = végtelen</span>';


// Error
$_['error_permission']    = 'Figyelem: Önnek nincs jogosultsága az Elhagyott kosár emlékeztető modul módosításához!';
$_['error_secret_code']   = 'Titkos kód: legalább 5 karakter! ( a-z, A-Z, 0-9 )';
$_['error_delay']         = 'Késleltetés - kötelező';
$_['error_html_email_not_installed'] = 'Az üzenetet nem lehet kiküldeni a<a href="http://www.oc-extensions.com/HTML-Email">HTML Email Modullal</a> mivel ez a modul nincs telepítve az Ön áruházában. Kérem, állítsa át Letiltottra!';
$_['error_max_reminders'] = 'Emlékeztetők maximális száma - kötelező';
$_['error_coupon_amount'] = 'Kupon összege - kötelező';
$_['error_coupon_expire'] = 'A kupon lejárata - kötelező';
?>