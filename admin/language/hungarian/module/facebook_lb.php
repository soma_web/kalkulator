<?php
// Heading
$_['heading_title']       = 'Facebook Like Box';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Sikeresen módosította a Facebook like box modult!';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Tartalom alja';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';

$_['text_color_scheme_light'] = 'Világos';
$_['text_color_scheme_dark']  = 'Sötét';
$_['text_preview']            = 'Előnézet';

// Entry
$_['entry_page_url']           = 'Facebook<br /> Oldal URL';
$_['entry_profile_id']         = 'Facebook profil ID';
$_['entry_connections']        = 'Facebook kapcsolatok száma';
$_['entry_color_scheme']       = 'Szín<br /> séma';
$_['entry_faces']              = 'Mutat:<br /> Arcok';
$_['entry_stream']             = 'Mutat:<br /> Stream';
$_['entry_header']             = 'Mutat:<br /> Fejléc';
$_['entry_border_color']       = 'Szegély<br /> Szín';
$_['entry_dimension']          = 'Méret<br /> (Szélesség, Magasság)';
$_['entry_layout']        = 'Megjelenítés:';
$_['entry_position']      = 'pozíció:';
$_['entry_status']        = 'Állapot:';
$_['entry_sort_order']    = 'Sorrend:';


// Error
$_['error_permission']        = 'Figyelem: Önnek nincs jogosultsága módosítani a Facebook like box modult!';
$_['error_profile_id']        = 'Profil ID Szükséges';
$_['error_page_url']          = 'Oldal URL Szükséges';
$_['error_connections']       = 'Kapcsolat Szükséges';
$_['error_dimension']         = 'Szélesség Szükséges';
?>