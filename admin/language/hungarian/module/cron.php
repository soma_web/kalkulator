<?php
// Heading
$_['heading_title']       = 'Időzített programok beállítása';

// Text
$_['text_module']           = 'Modulok';
$_['text_success']          = 'Sikerült: Sikeresen módosította a cron beállítás modult!';
$_['text_cron_inditas']     = '<span class="help">Futtatáshoz a serveren állítsa be a cront. Javasolt futási idő: 5 perc. Futtassa a következőt: <b>'.HTTP_CATALOG.'index.php?route=module/cron</b></span>';

$_['text_naponta_rendszeresen'] = 'Naponta rendszeresen';
$_['text_naponta_idonkent'] = 'Naponta megadott időpontokban';
$_['text_hetente']          = 'Hetente';
$_['text_havonta']          = 'Havonta';

$_['text_egyperc']          = 'Percenként';
$_['text_otperc']           = 'Ötpercenként';
$_['text_tizperc']          = 'Tízpercenként';
$_['text_tizenotperc']      = 'Tizenötpercenként';
$_['text_huszperc']         = 'Húszpercenként';
$_['text_felora']           = 'Félóránként';
$_['text_ora']              = 'Óránként';
$_['text_futtatas']         = 'Futtatás';

$_['column_program'] = 'Program';
$_['column_rendszeresseg'] = 'Rendszeresség';
$_['column_status'] = 'Státusz';
$_['column_action'] = 'Művelet';
$_['column_date_start'] = 'Utolsó indítás';
$_['column_date_end'] = 'Futás vége';
$_['column_crond_id'] = 'Azonosító';

$_['entry_program'] = 'Program';
$_['entry_rendszeresseg'] = 'Rendszeresség';
$_['entry_status'] = 'Státusz';


$_['text_timepicker_currentText'] = 'Most';
$_['text_timepicker_closeText'] = 'Mehet';
$_['text_timepicker_timeOnlyTitle'] = 'Válasszon időpontot';
$_['text_timepicker_timeText'] = '';
$_['text_timepicker_hourText'] = 'Óra';
$_['text_timepicker_minuteText'] = 'Perc';
$_['text_timepicker_secondText'] = 'Másodperc';
$_['text_timepicker_timezoneText'] = 'Időzóna';
$_['text_lefutott']     = 'Program futása befejeződött!';
$_['text_folyamatban']  = 'Program futása folyamatban ...';
$_['text_futtatas_eredmenyek']  = 'Események';
$_['text_futtatas_eredmeny']  = 'Bővebben';

$_['text_hetfo']        = 'Hétfő';
$_['text_kedd']         = 'Kedd';
$_['text_szerda']       = 'Szerda';
$_['text_csutortok']    = 'Csütörtök';
$_['text_pentek']       = 'Péntek';
$_['text_szombat']      = 'Szombat';
$_['text_vasarnap']     = 'Vasárnap';

// Button

$_['button_add_idonkent']   = 'Új időpont hozzáadása';
$_['button_add_nap']        = 'Új nap hozzáadása';
$_['button_futottak']       = 'Futott programok listája';
$_['button_cron_list']      = 'Vissza: beállítások listája';
$_['button_futottak_vissza']= 'Vissza: futott programok listája';

// Error
$_['error_permission']      = 'Figyelmeztetés: A cron modul módosítása az Ön számára nem engedélyezett!';
?>