<?php

// Heading
$_['heading_title']       = 'Akciós termékek a fejlécből';

// Text
$_['text_module']      = 'Modulok';
$_['text_success']     = 'Sikeresen módosította a akciók modult!';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Taratalom alja';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';
$_['text_ures']          = 'Nincs animáció';
$_['text_mcs5']          = 'Görgető animáció';
$_['text_korhinta']      = 'Körhinta animáció';

// Entry
$_['entry_limit']               = 'Korlát:';
$_['entry_kep_szelesseg']       = 'Kép szélesség (width):';
$_['entry_kep_magassag']        = 'Kép magasság (height):';
$_['entry_status']              = 'Státusz:';
$_['entry_kiemelt']             = 'Csak a kiemelt akciók:';


// Error
$_['error_permission'] = 'Figyelmeztetés: Az akciók modul módosítása az Ön számára nem engedélyezett!';
$_['error_dimension']         = 'Meg kell adni a kép magasságát és szélességét!';
?>
