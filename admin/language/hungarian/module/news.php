<?php
//-----------------------------------------------------
// News Module for Opencart v1.5.6
// Modified by villagedefrance
// contact@villagedefrance.net
//-----------------------------------------------------

// Heading
$_['heading_title']          		= 'Hírek';
$_['text_news']              	    = 'Hírek';

// Text
$_['text_module']            	    = 'Modul';
$_['text_success']           	    = 'Sikeres: Sikeresen módosította <b>Hírek</b> !';
$_['text_yes']	       			    = 'Igen';
$_['text_no']	       			    = 'Nem';
$_['text_content_top']       	    = 'Tartalom teteje';
$_['text_content_bottom']	        = 'Tartalom alja';
$_['text_column_left']       	    = 'Bal oszlop';
$_['text_column_right']      	    = 'Jobb oszlop';
$_['text_default']           		= 'Alapértelmezett';
$_['text_image_manager'] 	        = 'Képkezelő';
$_['text_browse']            	    = 'Fájlok böngészése';
$_['text_clear']             		= 'Tiszta kép';
$_['text_edit']	       			    = 'Módosítás';
$_['text_module_settings']  	    = 'Megjegyzés: Ezek a beállítások az összes modulra érvényesek allul.';

// Column
$_['column_image']         		    = 'Kép';
$_['column_title']           		= 'Fejléc';
$_['column_date_added']  	        = 'Hozzáadás dátuma';
$_['column_viewed']  			    = 'Megnézett';
$_['column_status']          	    = 'Státusz';
$_['column_action']          	    = 'Akció';

// Tab
$_['tab_language']            	    = 'Nyelv';
$_['tab_setting']          		    = 'Általános beállítások';

// Version Check
$_['text_module_name'] 		        = 'Modul neve : ';
$_['text_module_list']			    = 'Modul kompatibilitás : ';
$_['text_store_version'] 		    = 'Opencart verzió : <strong>%s</strong>';
$_['text_template']			        = 'Aktuális Template : ';

$_['text_module_version'] 	        = 'Modul verzió : <strong>%s</strong>';
$_['text_module_revision'] 	        = 'Modul revízió : <strong>%s</strong>';
$_['text_v_update'] 			    = '<img src="view/image/warning.png" alt="" /> &nbsp; <strong>%s</strong> : frissítés elérhető !';
$_['text_v_no_update'] 		        = '<img src="view/image/success.png" alt="" /> &nbsp; <strong>%s</strong> : legfrisebb verzió.';
$_['text_r_update'] 			    = '<img src="view/image/warning.png" alt="" /> &nbsp; <strong>%s</strong> : frissítés elérhető !';
$_['text_r_no_update'] 		        = '<img src="view/image/success.png" alt="" /> &nbsp; <strong>%s</strong> : legfrisebb verzió.';
$_['text_no_revision'] 		        = '<img src="view/image/warning.png" alt="" /> &nbsp; <b>Revizió adatai nem találhatóak.</b> Kérlek frissíts !';
$_['text_no_file'] 				    = 'The Module Version Check File is not available from the remote server !';
$_['text_status'] 				    = 'MODULE VERSION STATUS';
$_['text_update'] 				    = 'MODULE UPDATE OPTIONS';
$_['text_getupdate'] 			    = 'Click on one of the links below to login to your account and download the latest update.';

$_['button_showhide'] 		        = 'Version Checker';
$_['button_support'] 			    = 'Support';

$_['text_v151']				        = 'v1.5.1, ';
$_['text_v152']				        = 'v1.5.2, ';
$_['text_v153']				        = 'v1.5.3, ';
$_['text_v154']				        = 'v1.5.4, ';
$_['text_v155']				        = 'v1.5.5, ';
$_['text_v156']				        = 'v1.5.6.';

// Entry
$_['entry_customtitle']      	    = 'Fejléc:<br /><span class="help">Ha üres, és a fejléc megjelenítés be van kapcsolva, akkor ez fog megjelenni.</span>';
$_['entry_header'] 			        = 'Fejléc megjelenítése:<br /><span class="help">A doboz mutatása legyen bekapcsolva.</span>';
$_['entry_icon']   				    = 'Ikon megjelenítése:<br /><span class="help">A doboz mutatása legyen bekapcsolva.</span>';
$_['entry_box']   				    = 'Doboz mutatása:';

$_['entry_headline_module']  		= 'A modul fejlécének mutatása:';
$_['entry_newspage_thumb']		    = 'Előnézeti méret:';
$_['entry_newspage_popup'] 		    = 'Popup mérete:';
$_['entry_newspage_addthis'] 	    = 'AddThis mutatása:';
$_['entry_headline_chars']   		= 'Fejléc karakterek maximális száma:<br /><span class="help"></span>';
$_['entry_chars']   		        = 'karakter';

$_['entry_title']         			= 'Hírek fejléc:';
$_['entry_meta_description'] 		= 'Hírek meta leírás:';
$_['entry_description']      		= 'Hírek szöveg:';
$_['entry_store']            		= 'Hírek áruház:<br /><span class="help"></span>';
$_['entry_keyword']        			= 'Hírek SEO Kulcs:<br /><span class="help"></span>';
$_['entry_image']            		= 'Hírek kép:<br /><span class="help"></span>';

$_['entry_limit']            		= 'Korlát:';
$_['entry_headline']         	    = 'Fejléc:';
$_['entry_numchars']       	        = 'Karakterek:';
$_['entry_layout']           	    = 'Elhelyezés:';
$_['entry_position']         	    = 'Pozíció:';
$_['entry_status']           	    = 'Státusz:';
$_['entry_sort_order']       	    = 'Sorrend:';

// Button
$_['button_news']            	    = 'Hírek Hozzáadás/Módosítás';
$_['button_module']          	    = 'Modul beállítások';
$_['button_save']				    = 'Mentés';
$_['button_apply']				    = 'Alkalmazás';

// Error
$_['error_permission']       	    = 'Warning: You do not have permission to modify module <b>News</b> !';
$_['error_newspage_thumb']	        = 'Newspage Thumbnail Size dimensions required!';
$_['error_newspage_popup']	        = 'Newspage Popup Size dimensions required!';
$_['error_title']            		= 'Headline must be greater than 3 and less than 250 characters!';
$_['error_description']      	    = 'Main Story must be greater than 3 characters!';
$_['error_numchars']      		    = 'Warning: You must enter the number of characters to display!';

?>
