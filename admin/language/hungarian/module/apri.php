<?php
// Heading
$_['heading_title']          = 'Automatikus véleményösztönző';

// Text
$_['text_module']            = 'Modulok';
$_['text_success']           = 'Sikerült: Az automatikus véleményösztönzőt módosította!';
$_['text_help_customized']   = 'Testreszabható email tárgyhoz és tartalomhoz felhasználhatóak a következő kifejezések:Keresztnév:{firstname} Vezetéknév:{lastname} Áruház neve:{store_name} Áruház email címe:{store_email} Áruház telefonszáma:{store_telephone} Megvásárolt termékek táblázatban:{purchased_products_table} Email leiratkozás link:{unsubscribe_link}<br />Check email_example.txt from zip archive';

// Tabs
$_['tab_general']            = 'Általános';
$_['tab_mail']               = 'Email';

// Entry
$_['entry_cron_password']    = 'Cron jelszó:';
$_['entry_start_date']       = 'Indítás dátuma: <span class="help">(Opcionális)meghívás küldése kizárólag az Indítás dátuma után feladott rendeléseknek.</span>';
$_['entry_days_after']       = 'Napok múlva: <span class="help">(Opcionális) meghívás küldése kizárólag rendelés után x nappal</span>';
$_['entry_allowed_statuses'] = 'Engedélyezett Rendelési Állapot:<span class="help">küldjön meghívást véleményezésre azon vásárlóknak, akiknek a rendelési állapota itt engedélyezve van:<br />Ajánlott: Complete</span>';
$_['entry_allow_unsubscribe']= 'Leiratkozás engedélyezése<span class="help">ha a beállítás engedélyezett, és a vásárló leiratkozott akkor a modul nem küld a vásárlónak több meghívást véleményezésre</span>';
$_['entry_log_to_admin']     = 'Összegzést küld az adminnak:';
$_['entry_use_html_email']   = 'Küldjön meghívást véleményezésre a <a href="http://www.oc-extensions.com/HTML-Email">HTML Email Modullal</a>:<span class="help"><br />Ha a HTML Email Modul nincs telepítve az áruházba, akkor az alapértelmezett html emailt küldi (mint a régi verziója a modulnak)</span>';
$_['entry_mail_subject']     = 'Email tárgy:';
$_['entry_mail_message']     = 'Email üzenet:';
$_['entry_mail_log_subject'] = 'Admin Összegzés Email - tárgy:';
$_['entry_mail_log_message'] = 'Admin Összegzés Email - üzenet:<span class="help">{recipients_list} - Emailre feliratkozott vásárlók listája</span>';

// Error
$_['error_permission']       = 'Figyelem: Önnek nincs jogosultsága módosítani az Automatikus véleményösztönző modult!';
$_['error_cron_password']    = 'Hiba: Cron jelszó - szükséges!';
$_['error_allowed_statuses'] = 'Hiba: Engedélyezett Rendelési Állapot - szükséges!';
$_['error_mail_subject']     = 'Hiba: Email tárgy szükséges!';
$_['error_mail_message']     = 'Hiba: Email üzenet szükséges!';
$_['error_mail_message_unsubscribe'] = 'Törölje a Leiratkozás Linket az emailből, ha az \'Leiratkozás engedélyezése\' opció le van tiltva';
$_['error_mail_log_subject'] = 'Hiba: Email Log tárgy szükséges!';
$_['error_mail_log_message'] = 'Hiba: Email Log üzenet szükséges!';
$_['error_html_email_not_installed'] = 'Véleményezésre meghívó emailt nem lehet elküldeni a <a href="http://www.oc-extensions.com/HTML-Email">HTML Email Modullal</a> mert ez a modul nem elérhető az Áruházában.';

?>