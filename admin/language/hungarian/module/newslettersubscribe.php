<?php
// Heading
$_['heading_title']    = 'Hírlevél';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Hírlevelek sikeresen módosítva!';
$_['text_left']           = 'Bal';
$_['text_right']          = 'Jobb';
$_['text_info']           = '';  
$_['text_module']         = 'Modulok';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Tartalom alja';
$_['text_column_left']    = 'Oszlop Bal';
$_['text_column_right']   = 'Oszlop Jobb';

// Entry
$_['entry_unsubscribe']  = 'Leiratkozás:';
$_['entry_position']     = 'Pozició:';
$_['entry_status']       = 'Állapot:';
$_['entry_sort_order']   = 'Sorrend:';
$_['entry_options']      = 'Beállítások:';
$_['entry_mail']   		 = 'Email Küldés';
$_['entry_thickbox']   	 = 'Jelölőnégyzet';
$_['entry_registered']   = 'Regisztrált Felhasználók:';
$_['entry_layout']       = 'Kiosztás:';

// Error
$_['error_permission'] = 'Nincs módosítási engedélyed';
?>