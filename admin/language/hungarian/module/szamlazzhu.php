<?php
// Heading
$_['heading_title']    = 'Számlázz.hu';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Sikerült: Az Számlázz.hu módosítása megtörtént!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';

// Entry
$_['entry_product']       = 'Products:';
$_['entry_area_id']       = 'Beállítások:';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';
$_['entry_classname']     = 'CSS Class:';
$_['entry_area']          = 'Beállítások';
$_['entry_code']          = 'HTML Content:';
$_['entry_yes']	          = 'Yes';
$_['entry_no']	          = 'No';
$_['entry_order_status']  = 'Automatikus számla, ha a rendelés státusza:';

$_['text_yes']	            = 'Igen';
$_['text_no']	            = 'Nem';
$_['text_settings']	        = 'Beállítások';
$_['text_username']	        = 'Felhasználónév:';
$_['text_password']	        = 'Jelszó:';
$_['text_eszamla']	        = 'Elektronikus számla:';
$_['text_szamla_download']	= 'Számla letöltése:';
$_['text_kulcstartojelszo']	= 'Kulcstartó jelszó:';
$_['text_peldanyszam']	    = 'Példány szám:';
$_['text_valaszverzio']	    = 'Válasz verzió(1 az alapértelmezett):';
$_['text_aggregator']	    = 'Aggregátor(alapértelmezetten üres):';



// Error
$_['error_permission']    = 'Warning: You do not have permission to modify this module!';
$_['error_code']          = 'Code Required';
?>
