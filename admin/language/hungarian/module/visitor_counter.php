<?php

$_['heading_title']      	  = 'Látogató Számláló';

$_['text_module']     	 	  = 'Modulok';
$_['text_success']            = 'Sikeresen módosította a Látogató Számláló modult!';;
$_['text_content_top']   	  = 'Tartalom Teteje';
$_['text_content_bottom']	  = 'Tartalom Alja';
$_['text_column_left']   	  = 'Bal Oszlop';
$_['text_column_right'] 	  = 'Jobb Oszlop';
$_['text_left']        		  = 'Bal';
$_['text_right']      		  = 'Jobb';
$_['text_total_visitors'] 	  = 'Összes Látogató';
$_['text_total_hits']    	  = 'Összes Megtekintés';

$_['entry_layout']      	  = 'Elrendezés:';
$_['entry_position']    	  = 'Pozíció:';
$_['entry_status']      	  = 'Állapot:';
$_['entry_sort_order']  	  = 'Sorrend:';
$_['entry_count_type']   	  = 'Fő Számláló:';

$_['error_permission']    = 'Figyelem: Önnek nincs jogosultsága módosítani a Látogató Számláló modult!';
?>
