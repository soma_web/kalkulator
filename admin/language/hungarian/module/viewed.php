<?php
// Heading
$_['heading_title']       = 'Legutóbb megtekintett';

// Text
$_['text_module']         = 'Modulok';
$_['text_success']        = 'Sikeresen módosította a beállításokat!';
$_['text_content_top']    = 'Tartalom teteje';
$_['text_content_bottom'] = 'Tartalom alja';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';
$_['text_yes']            = 'Igen';
$_['text_no']             = 'Nem';
$_['text_attention']      = 'Modul hozzáadása az elrendezésre: %s (product/product)<br/>De ha nem akarja, hogy ez a modul megjelenjen a termékek oldalon, állítsa be a paramétert <b>%s</b> a pozícióra <b>%s</b>';

// Entry
$_['entry_count']         = 'Kijelző:';
$_['entry_image']         = 'Kép (Sz x M):';
$_['entry_layout']        = 'Elrendezés:';
$_['entry_position']      = 'Pozició:';
$_['entry_status']        = 'Állapot:';
$_['entry_sort_order']    = 'Sorrend:';
$_['entry_show_on_product'] = 'Jelenjen meg a termékek oldalon:';

// Error 
$_['error_permission']    = 'Figyelem: Önnek nincs jogosultsága módosítani a Legutóbb megtekintett modult!';
$_['error_image']         = 'A kép szélességének és magasságának megadása szükséges!';
$_['error_layout']        = 'A modult engedélyezni kell az elrendezésre: %s (product/product)! Vagy szintén tiltsa le más elrendezéseken.';

?>