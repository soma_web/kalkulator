<?php
// Heading
$_['heading_title']    = 'NMS Net Fizetési képek beállítása';

// Text
$_['text_module']           = 'Modulok';
$_['text_success']          = 'Sikeres módosítás!';
$_['text_pleaseselect']     = 'Válassz';
$_['text_true']             = 'Igaz';
$_['text_false']            = 'Hamis';
$_['text_image_manager']    = 'Képsorozat kezelő';
$_['text_left']             = 'Bal';
$_['text_right']            = 'Jobb';
$_['text_middletop']        = 'Közép&nbsp;Felül';
$_['text_home']             = 'Főoldal';
$_['text_footer']           = 'Lábléc';

// Entry
$_['entry_position']   = 'Pozició: ';
$_['entry_image']        = 'Kép: <br /><span class="help">Kattintson a képre a cseréhez.</span>';
$_['entry_status']     = 'Állapot: ';
$_['entry_url']        = 'Link: ';
$_['entry_target']     = 'Célpont(target): ';
$_['entry_sort']       = 'Sorrend: ';



// Buttons
$_['button_addslide']  = 'Új fizetési kép hozzáadása';
$_['button_remove']    = 'Eltávolítás';

// Tab
$_['tab_gen']               = 'Alapvető beállítások';
$_['tab_sliderimage']                = 'Képkezelő';

// Error
$_['error_permission'] = 'Nincs módosítási joga!';

// Help

$_['help_linkurl']             = '<span class="help">Ez lesz a link a képhez. Írja be a teljes útvonalat, a http:// előtaggal együtt</span>';
$_['help_alttext']             = '<span class="help">Ez lesz a kép neve(title)</span>';
$_['help_browsetitle']         = 'Kép keresése';
$_['help_addslidehdr']         = 'Új képsor létrehozása';
$_['help_addslidetext']        = 'Kattintson az "Új fizetési kép hozzáadása" gombra.';
$_['help_height']              = 'Összesített képmagasság (pixelben!!). A túlméretezett képek elvágódnak ha ezt az értéket meghaladják';

?>