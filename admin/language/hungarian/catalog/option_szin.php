<?php
// Heading
$_['heading_title']       = 'Szín táblázat';

// Text
$_['text_success']        = 'Sikeresen módosította a szín táblázatot!';

// Column
$_['column_name']         = 'Szín neve';
$_['column_szinkod']      = 'Szín kódja';
$_['column_sort_order']   = 'Sorrend';
$_['column_action']       = 'Művelet';

// Entry
$_['entry_name']          = 'Szín neve:';
$_['entry_szinkod']       = 'Szín hexadecimális kódja:<br/><small>Több szín esetén / jellel, válassza el!</small>';
$_['entry_azonosito']     = 'Azonosító:';
$_['entry_sort_order']    = 'Sorrend:';
$_['entry_szin_group']    = 'Színcsoport:';


// Error
$_['error_permission']    = 'Önnek nincs joga módosítani az szín táblázatot!';
$_['error_name']          = 'Az szín nevének legalább 1 és legfeljebb 128 karakterből kell állnia!';
$_['error_type']          = 'Figyelem! színkód érték szükségesek!';
$_['error_option_value']  = 'Az színkód érték nevének legalább 1 és legfeljebb 128 karakterből kell állnia!';
$_['error_product']       = 'Figyelem: Ez az színkód nem törölhető, mert jelenleg hozzárendelt %s termék(ek)!';

?>