<?php
// Heading
$_['heading_title']       = 'Színcsoport';

// Text
$_['text_success']        = 'Sikeresen módosította a színcsoportot!';

// Column
$_['column_name']         = 'Színcsoport neve';
$_['column_sort_order']   = 'Sorrend';
$_['column_action']       = 'Művelet';
$_['column_szinkod']      = 'Színkód';


// Entry
$_['entry_name']          = 'Színcsoport neve:';
$_['entry_szinkod']       = 'Színcsoport hexadecimális kódja:<br/><small>Több szín esetén / jellel, válassza el!</small>';
$_['entry_sort_order']    = 'Sorrend:';


// Error
$_['error_permission']    = 'Önnek nincs joga módosítani az színcsoportot!';
$_['error_name']          = 'Az szín nevének legalább 1 és legfeljebb 128 karakterből kell állnia!';
$_['error_type']          = 'Figyelem! színkód érték szükségesek!';
$_['error_option_value']  = 'Az színkód érték nevének legalább 1 és legfeljebb 128 karakterből kell állnia!';
$_['error_product']       = 'Figyelem: Ez az színkód nem törölhető, mert jelenleg hozzárendelt %s termék(ek)!';

?>