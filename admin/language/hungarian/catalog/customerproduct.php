<?php
// Heading
$_['heading_title']          = 'Vevő termékek';

// Text  
$_['text_success']           = 'Sikerült: A termékek módosítása megtörtént!';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Alapértelmezett';
$_['text_image_manager']     = 'Kép szerkesztő';
$_['text_browse']            = 'Tallózás';
$_['text_clear']             = 'Törlés';
$_['text_option']            = 'Opció';
$_['text_option_value']      = 'Opció érték';
$_['text_percent']           = 'Százalék';
$_['text_amount']            = 'Fix összeg';

// Column
$_['column_name']            = 'Termék neve';
$_['column_model']           = 'Cikkszám';
$_['column_image']           = 'Kép';
$_['column_price']           = 'Ár';
$_['column_customer']        = 'Feltöltő neve:';
$_['column_quantity']        = 'Mennyiség';
$_['column_status']          = 'Státusz';
$_['column_action']          = 'Action';
$_['column_category']        = 'Kategóriák';

// Entry
$_['entry_name']             = 'Termék neve:';
$_['entry_meta_keyword'] 	 = 'Meta Tag kulcsszavak:';
$_['entry_meta_description'] = 'Meta Tag leírás:';
$_['entry_description']      = 'Leírás:';
$_['entry_store']            = 'Áruházak:';
$_['entry_keyword']          = 'SEO kulcsszavak:<br /><span class="help">Ne használjon szóközt helyette elválasztó jeleket - és ellenőrizze, hogy a kulcsszó globálisan egyedi.</span>';
$_['entry_model']            = 'Model:';
$_['entry_sku']              = 'SKU:<br/><span class="help">Stock Keeping Unit</span>';
$_['entry_upc']              = 'UPC:<br/><span class="help">Universal Product Code</span>';
$_['entry_ean']              = 'EAN:<br/><span class="help">European Article Number</span>';
$_['entry_jan']              = 'JAN:<br/><span class="help">Japanese Article Number</span>';
$_['entry_isbn']             = 'ISBN:<br/><span class="help">International Standard Book Number</span>';
$_['entry_mpn']              = 'MPN:<br/><span class="help">Manufacturer Part Number</span>';
$_['entry_location']         = 'Egyéb:';
$_['entry_manufacturer']     = 'Gyártó:';
$_['entry_shipping']         = 'Szállítandó termék:';
$_['entry_date_available']   = 'Mikortól kapható:';
$_['entry_date_kaphato_ig']  = 'Meddig kapható:';
$_['entry_quantity']         = 'Mennyiség:';
$_['entry_minimum']          = 'Minimális mennyiség :<br/><span class="help">Minimális vásárlási mennyiség</span>';
$_['entry_stock_status']     = 'Hiánycikk állapot: <br/><span class="help">Az az állaptot, amit megjelenik, ha egy termék nincs raktáron</span>';
$_['entry_price']            = 'Ár:';
$_['entry_tax_class']        = 'Áfakulcs:';
$_['entry_points']           = 'Pontok:<br/><span class="help">Ennyi pontra van szükséged a termék megvásárlásához. Amennyiben nincs rás szüksége 0-val kiléphet.</span>';
$_['entry_option_points']    = 'Pontok:';
$_['entry_subtract']         = 'Készlet kivonása::';
$_['entry_weight_class']     = 'Súlymérték:';
$_['entry_weight']           = 'Súly:';
$_['entry_length']           = 'Hosszmérték:';
$_['entry_dimension']        = 'Méret (Hosszúság × Szélesség × Magasság):';
$_['entry_image']            = 'Kép:<br /><span class="help">Kattints a módosításhoz!</span>';
$_['entry_customer_group']   = 'Ügyfél csoport:';
$_['entry_fizetes_status']   = 'Fizetés státusza:';
$_['entry_fizetes_status']   = 'Fizetés státusza:';

$_['vevo_kedvezmeny']        = 'Ügyfél:';
$_['entry_date_start']       = 'Kezdő dátum:';
$_['entry_date_end']         = 'Záró dátum:';
$_['entry_priority']         = 'Prioritás:';
$_['entry_attribute']        = 'Jellemző:';
$_['entry_attribute_group']  = 'Jellemző csoport:';
$_['entry_text']             = 'Szöveg:';
$_['entry_option']           = 'Választék:';
$_['entry_option_value']     = 'Választék érték:';
$_['entry_required']         = 'Szükséges:';
$_['entry_status']           = 'Állapot:';
$_['entry_sort_order']       = 'Sorrend:';
$_['entry_category']         = 'Kategóriák:';
$_['entry_download']         = 'Letöltések:';
$_['entry_related']          = 'Kapcsolódó termékek:<br /><span class="help">(Autocomplete)</span>';
$_['entry_tag']   	         = 'Termék címkék:<br /><span class="help">Vesszővel elválasztva</span>';
$_['entry_reward']           = 'Jutalékpont:';
$_['entry_layout']           = 'Elrendezés felülbírálása:';
$_['entry_filter']           = 'Szűrő:';


$_['text_recurring_help']    = 'Recurring amounts are calculated by the frequency and cycles. <br />
For example if you use a frequency of "week" and a cycle of "2", then the user will be billed every 2 weeks. <br />
The length is the number of times the user will make a payment, set this to 0 if you want payments until they are cancelled.';


$_['text_recurring_title']   = 'Recurring payments';
$_['text_recurring_trial']   = 'Trial period';
$_['entry_recurring']        = 'Recurring billing:';
$_['entry_recurring_price']  = 'Recurring price:';
$_['entry_recurring_freq']   = 'Recurring frequency:';
$_['entry_recurring_cycle']  = 'Recurring cycles:<span class="help">How often its billed, must be 1 or more</span>';
$_['entry_recurring_length'] = 'Recurring length:<span class="help">0 = until cancelled</span>';
$_['entry_trial']            = 'Trial period:';
$_['entry_trial_price']      = 'Trial recurring price:';
$_['entry_trial_freq']       = 'Trial recurring frequency:';
$_['entry_trial_cycle']      = 'Trial recurring cycles:<span class="help">How often its billed, must be 1 or more</span>';
$_['entry_trial_length']     = 'Trial recurring length:';

$_['text_length_day']        = 'Nap';
$_['text_length_week']       = 'Hét';
$_['text_length_month']      = 'Hónap';
$_['text_length_month_semi'] = 'Semi Month';
$_['text_length_year']       = 'Év';

// Error
$_['error_warning']          = 'Figyelem: Kérjük gondosan nézze át a hibákat az űrlapon!';
$_['error_permission']       = 'Figyelmeztetés: A termékek módosítása az Ön számára nem engedélyezett';
$_['error_name']             = 'A terméknév legalább 3, és legfeljebb 255 karakterből álljon!';
$_['error_model']            = 'A termék gyártmányának megnevezése legalább 3, és legfeljebb 64 karakterből álljon!';

// Elhelyezkedés
$_['entry_elhelyezkedes']           = 'Elhelyezkedés I.:';
$_['entry_elhelyezkedes_alcsoport'] = 'Elhelyezkedés II.:';
$_['entry_kiemelt_keret']           = 'Keret:';
$_['entry_idoszak']          = 'Időszak';
$_['entry_idoszak_darab']    = 'Mennyiség';

$_['text_nap']               = 'Nap';
$_['text_het']               = 'Hét';
$_['text_honap']             = 'Hónap';
$_['text_ev']                = 'Év';

$_['entry_biztonsagi_kod']                      = 'Biztonsági kód';
?>