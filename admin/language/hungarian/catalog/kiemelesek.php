<?php

// Heading
$_['heading_title']          = 'Kiemelések';
$_['button_insert']          = 'Beszúrás';
$_['button_delete']          = 'Törlés';
$_['error_warning']          = '';
$_['success']                = '';
$_['column_megnevezes']      = 'Kiemelések';
$_['column_ara']             = 'Ára';
$_['column_class']           = 'Sablon neve';
$_['column_status']          = 'Státusz';
$_['column_action']          = 'Művelet';
$_['column_sort_order']      = 'Sorrend';
$_['column_idoszak']         = 'Időszak';
$_['column_maximum']         = 'Maximum mennyiség';


$_['entry_ara']              = 'Ára:';
$_['entry_class']            = 'Sablon neve:';
$_['entry_status']           = 'Státusz:';
$_['entry_sorrend']          = 'Sorrend:';
$_['entry_idoszak']          = 'Időszak:';
$_['entry_tax_class']        = 'Adó osztály:';
$_['entry_darabszam']        = 'Maximum mennyiség:';

$_['text_engedelyezett']     = 'Engedélyezett:';
$_['text_letiltott']         = 'Letiltott:';

$_['text_nap']               = 'Nap';
$_['text_het']               = 'Hét';
$_['text_honap']             = 'Hónap';
$_['text_ev']                = 'Év';
?>