<?php
// Heading
$_['heading_title']     = 'Szűrők';

// Text
$_['text_success']      = 'Sikerült: A szűrés múdosítása megtörtént!';

// Column
$_['column_group']      = 'Szűrő csoport';
$_['column_sort_order'] = 'Sorrend';
$_['column_action']     = 'Módosítás';

// Entry
$_['entry_group']       = 'Szűrő csoport neve:';
$_['entry_name']        = 'Szűrő neve:';
$_['entry_sort_order']  = 'Sorrend:';
$_['entry_filter_select'] = 'Szűrő kiválasztása:';


// Error
$_['error_permission']  = 'Figyelmeztetés: Önnek nincs jogosultsága módosítani szűrők!';
$_['error_group']       = 'Szűrő Csoport név hossza legyen 1 és 64 karakter!';
$_['error_name']        = 'Szűrő név hossza legyen 1 és 64 karakter!';
?>