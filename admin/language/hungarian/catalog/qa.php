<?php
// Heading
$_['heading_title']                 = 'Kérdezz felelek';

// Text
$_['text_search']                   = 'Keresés';
$_['text_success_update']           = '<strong>Siker!</strong> A kérdés frissült!';
$_['text_success_insert']           = '<strong>Siker!</strong> Új kérdés jött létre!';
$_['text_success_copy']             = '<strong>Siker!</strong> %s kérdés a vágólapra került!';
$_['text_success_delete']           = '<strong>Siker!</strong> %s kérdés törlődött!';
$_['text_toggle_navigation']        = 'Navigáció váltás';
$_['text_confirm_delete']           = 'Erősítse meg a Törlést!';
$_['text_are_you_sure']             = 'A törlés visszavonhatatlan! Biztos, hogy folytatja?';
$_['text_filter']                   = 'Szűrés';
$_['text_clear_filter']             = 'Szűrő ürítése';
$_['text_autocomplete']             = '(Automatikus kiegészítés)';
$_['text_no_results']               = 'Nincsenek kérdések';
$_['text_pagination']               = 'Kérdések megjelenítése: {start} - {end}. Összesen: {total} ({pages} oldal)';
$_['text_filtered_from']            = '(szűrve az összesen %s kérdésből)';
$_['text_anonymous']                = '<em>Ismeretlen</em>';
$_['text_all_languages']            = 'Minden nyelv';
$_['text_display_language']         = 'Display questions in the selected language';
$_['text_first_page']               = 'Első';
$_['text_previous_page']            = 'Előző';
$_['text_next_page']                = 'Következő';
$_['text_last_page']                = 'Utolsó';
$_['text_question']                 = 'Kérdés';
$_['text_answer']                   = 'Válasz';
$_['text_insert']                   = 'Beszúrás';
$_['text_update']                   = 'Frissítés';

// Column
$_['column_product']                = 'Termék';
$_['column_question_author_name']   = 'Kérdező';
$_['column_question']               = 'Kérdés';
$_['column_answer']                 = 'Válasz';
$_['column_answer_author_name']     = 'Válaszadó';
$_['column_date_asked']             = 'Kérdezés időpontja';
$_['column_date_answered']          = 'Válaszadás időpontja';
$_['column_date_modified']          = 'Utolsó módosítás';
$_['column_store']                  = 'Áruház';
$_['column_status']                 = 'Állapot';
$_['column_action']                 = 'Művelet';

// Buttons
$_['button_apply']                  = 'Alkalmaz';

// Help
$_['help_notify']                   = 'E-mail értesítés küldése a kérdézőnek. Értesítés csak akkor megy ki, ha a kérdést megválaszolták.';

// Entry
$_['entry_id']                      = 'Kérdés azonosító:';
$_['entry_product']                 = 'Termék:';
$_['entry_language']                = 'Nyelv:';
$_['entry_author_name']             = 'Kérdező neve:';
$_['entry_answer_name']             = 'Válaszoló neve:';
$_['entry_customer']                = 'Vásárló:';
$_['entry_email']                   = 'E-mail cím:';
$_['entry_phone']                   = 'Telefonszám:';
$_['entry_custom']                  = 'Egyéb:';
$_['entry_question']                = 'Kérdés:';
$_['entry_answer']                  = 'Válasz:';
$_['entry_status']                  = 'Állapot:';
$_['entry_date_asked']              = 'Kérdezés dátuma:';
$_['entry_date_answered']           = 'Válaszadás dátuma:';
$_['entry_date_modified']           = 'Módosítás dátuma:';
$_['entry_stores']                  = 'Áruházak:';
$_['entry_update_date_answered']    = 'Válaszadás dátumának frissítése';
$_['entry_notify']                  = 'Értesítés küldése a kérdezőnek';

// Error
$_['error_permission']              = '<strong>Figyelem!</strong> Önnek nincs jogosultsága a kérdések módosítására!';
$_['error_warning']                 = '<strong>Figyelem!</strong> Kérem, nézze át az űrlapot!';
$_['error_product']                 = 'Kérem, válasszon terméket!';
$_['error_email']                   = 'Kérem, érvényes e-mail címet adjon meg!';
$_['error_question']                = 'A kérdés szövege legalább egy karakter hosszú legyen!';
$_['error_ajax_request']            = 'AJAX hiba történt!';

?>
