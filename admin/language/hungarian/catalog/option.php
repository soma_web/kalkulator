<?php
// Heading
$_['heading_title']       = 'Választék';

// Text
$_['text_success']        = 'Sikeresen módosította a választékokat!';
$_['text_choose']         = '-- Válasszon --';
$_['text_select']         = 'Select választó';
$_['text_radio']          = 'Rádió gomb';
$_['text_checkbox']       = 'Jelölőnényzet';
$_['text_image']          = 'Kép';
$_['text_input']          = 'Bemenet';
$_['text_text']           = 'Szöveg';
$_['text_textarea']       = 'Szövegmező';
$_['text_file']           = 'Fájl';
$_['text_date']           = 'Dátum';
$_['text_datetime']       = 'Dátum &amp; Idő';
$_['text_time']           = 'Idő';
$_['text_image_manager']  = 'Képkezelő';
$_['text_browse']         = 'Fájlok tallózása';
$_['text_clear']          = 'Kép törlése';
$_['text_table']          = 'Méret és szín';
$_['text_edit_szin']      = 'Színtáblázat módosítás';
$_['button_insert_szin']  = 'Színtáblázat beszúrás';
$_['entry_option_szin_select']  = '-- Válasszon színt!';



// Column
$_['column_name']         = 'Választék neve';
$_['column_sort_order']   = 'Sorrend';
$_['column_action']       = 'Művelet';

// Entry
$_['entry_name']          = 'Választék neve:';
$_['entry_description']   = 'Leírása:';
$_['entry_type']          = 'Típus:';
$_['entry_value']         = 'Választék értékének neve:';
$_['entry_image']         = 'Kép:';
$_['entry_sort_order']    = 'Sorrend:';
$_['entry_heading']      = 'Színkód (hexadecimális)<br/><small>Több szín esetén / jellel válassza el!</small>';

// Error
$_['error_permission']    = 'Önnek nincs joga módosítani az Választékokat!';
$_['error_name']          = 'Az választék nevének legalább 1 és legfeljebb 128 karakterből kell állnia!';
$_['error_type']          = 'Figyelem! Választék értékek szükségesek!';
$_['error_option_value']  = 'Az választék érték nevének legalább 1 és legfeljebb 128 karakterből kell állnia!';
$_['error_product']       = 'Figyelem: Ez az választék nem törölhető, mert jelenleg hozzárendelt %s termék(ek)!';

?>