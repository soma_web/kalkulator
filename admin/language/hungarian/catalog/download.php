<?php

// Heading
$_['heading_title']    = 'Letöltések';

// Text
$_['text_success']     = 'Siker: A letöltés frissítése megtörtént!';

// Column
$_['column_name']      = 'Letöltés neve';
$_['column_action']    = 'Művelet';

// Entry
$_['entry_name']       = 'Letöltés neve:';
$_['entry_filename']   = 'Fájlnév:';
$_['entry_remaining']  = 'Maradék letöltések:';
$_['entry_update']     = 'Küldés a korábbi vásárlóknak: <br /><span class="help">Jelölje be, ha a korábban vásárolt verziókat is frissíteni kívánja.</span>';

// Error
$_['error_permission'] = 'Figyelmeztetés: A letöltések módosítása az Ön számára nem engedélyezett!';
$_['error_name']       = 'A név legalább 3, és legfeljebb 64 karakterből álljon!';
$_['error_filename']   = 'A fájlnév legalább 3, és legfeljebb 128 karakterből álljon!';
$_['error_filetype']   = 'Érvénytelen a fájltípus!';
$_['error_product']    = 'Figyelmeztetés: Ezt a letöltést nem törölheti, mert hozzárendelték %s termékhez!';
?>
