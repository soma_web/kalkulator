<?php

// Heading
$_['heading_title']     = 'Eladások kategóriánként';

// Text
$_['select_kupon']          = 'Kupon';
$_['select_utalvany']       = 'Utalvany';

// Column
$_['column_categories']         = 'Kategóriák';
$_['column_ajanlat_kupon']      = 'Ajánlatok (kupon)';
$_['column_ajanlat_voucher']    = 'Ajánlatok (voucher)';
$_['column_vasarolt_kupon']     = 'Vásárlások (kupon)';
$_['column_vasarolt_voucher']   = 'Vásárlások (voucher)';


?>