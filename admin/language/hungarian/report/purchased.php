<?php
// Heading
$_['heading_title']   = 'Jelentés a megvásárolt termékekről';

// Column
$_['column_name']     = 'Termék neve';
$_['column_model']    = 'Típus';
$_['column_quantity'] = 'Mennyiség';
$_['column_total']    = 'Összes';
?>
