<?php
// Heading
$_['heading_title']					 = 'Budapest Bank';

// Text
$_['text_payment']					 = 'Fizetés';
$_['text_success']					 = 'Sikerült módosítani a Budapest Bank modult!'; //Success: You have modified PayPal account details!
$_['text_pp_standard']				 = 'Budapest Bank'; //<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal" title="PayPal" style="border: 1px solid #EEEEEE;" /></a>
$_['text_authorization']			 = 'Engedély';
$_['text_sale']						 = 'Eladás';

$_['text_sms']			             = 'SMS';
$_['text_dms']						 = 'DMS';

// Entry
$_['entry_email']					 = 'E-Mail:';
$_['entry_test']					 = 'Teszt mód:';
$_['entry_transaction']				 = 'Tranzakciós módszer: <br /><span class="help">Alapértelmezett: SMS</span>';
$_['entry_debug']					 = 'Debug mód:<br/><span class="help">System log használata.</span>';
$_['entry_total']                    = 'Min. érték:<br /><span class="help">Ezen összeg felett lesz csak aktív a modul.</span>';
$_['entry_canceled_reversal_status'] = 'Törölt és visszafordított státusz';
$_['entry_completed_status']         = 'Teljesített státusz:';
$_['entry_denied_status']			 = 'Megtagadva státusz:';
$_['entry_expired_status']			 = 'Lejárt státusz:';
$_['entry_failed_status']			 = 'Sikertelen státusz:';
$_['entry_pending_status']			 = 'Függő státusz:';
$_['entry_processed_status']		 = 'Feldolgozva státusz:';
$_['entry_refunded_status']			 = 'Visszatérített státusz:';
$_['entry_reversed_status']			 = 'Visszafordított státusz:';
$_['entry_voided_status']		     = 'Érvénytelenített státusz:';
$_['entry_geo_zone']				 = 'Geo zóna:';
$_['entry_status']					 = 'Státusz:';
$_['entry_sort_order']				 = 'Rendezési sorrend:';

//NEW
$_['entry_process_or_msg']			 = 'Megindított folyamat vagy eredmény:'; //Megindított folyamat vagy eredmény

$_['entry_deviza']					 = 'Deviza:';
$_['entry_cert_url']	    		 = 'Certifikációs fájl útvonala:<br/><span class="help">Kezdő útvonal: SYSTEM DIRECTORY</span>';
$_['entry_cert_pass']				 = 'Certifikációs jelszó:';
$_['entry_client_ip']				 = 'Kliens IP:';
$_['entry_client_language'] 		 = 'Kliens nyelv:<br/><span class="help">e.g. hu,en</span>';
//NEW


// Error
$_['error_permission']				 = 'Figyelem: Nincs jogosultságod módosítani a Budapest Bank modult!';
$_['error_email']					 = 'E-Mail szükséges!';



?>