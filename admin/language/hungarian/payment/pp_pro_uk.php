<?php

// Heading
$_['heading_title']      = 'PayPal Website Payment Pro (UK)';

// Text
$_['text_payment']       = 'Fizetés';
$_['text_success']       = 'Siker: A PayPal Website Payment Pro (UK) fiók részleteinek módosítása megtörtént!';
$_['text_pp_pro_uk']     = '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro (UK)" title="PayPal Website Payment Pro (UK)" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Engedély';
$_['text_sale']          = 'Eladás';

// Entry
$_['entry_vendor']       = 'Kereskedő ID:<br /><span class="help">Az Ön kereskedői bejelentkező ID-ja, amit akkor hozott létre, amikor regisztrálta a Weboldal Fizetések Pro fiókját.</span>';
$_['entry_user']         = 'Felhasználónév:<br /><span class="help">Ha beállít egy vagy több további felhasználót a fiókhoz, ez az érték az ID-ja lesz annak a felhasználónak aki meg van hatalmazva a tranzakciós folyamatokra. Viszont, ha nem állít be további felhasználót a fiókhoz, akkor a FELHASZNÁLÓ ugyanazt az értéket kapja mint az ELADÓ.</span>';
$_['entry_password']     = 'Jelszó:<br /><span class="help">A 6-32 karakterig terjedő jelszó, amit megadott miközben regisztrálta a fiókot.</span>';
$_['entry_partner']      = 'Partner:<br /><span class="help">Az ID amit kapott a meghatalmazott PayPal viszonteladótól, aki regisztrálta Önt a Payflow SDK-ba. Ha közvetlen a PayPa-tól szerezte fiókját, használja a PayPalUK-t.</span>';
$_['entry_test']         = 'Teszt Mód:<br /><span class="help">Használja az élő vagy a tesztelő (sandbox) átjáró szervert a tranzakció feldolgozásához?</span>';
$_['entry_transaction']  = 'Tranzakció Módszer:';
$_['entry_order_status'] = 'Rendelés állapota:';
$_['entry_geo_zone']     = 'Földrajzi zóna:';
$_['entry_status']       = 'Állapot:';
$_['entry_sort_order']   = 'Sorrend:';

// Error
$_['error_permission']   = 'Figyelmeztetés: Az PayPal Website Payment Pro (UK) történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_vendor']       = 'Kereskedő ID szükséges!';
$_['error_user']         = 'Felhasználónév szükséges!';
$_['error_password']     = 'Jelszó szükséges!';
$_['error_partner']      = 'Partner szükséges!';
?>
