<?php

// Heading
$_['heading_title']      = 'WorldPay';

// Text
$_['text_payment']       = 'Fizetés';
$_['text_success']       = 'Siker: A WorldPay fiók részleteinek módosítása megtörtént!';
$_['text_successful']    = 'Bekapcsol - Mindig sikeres';
$_['text_declined']      = 'Bekapcsol - Mindig csökkent';
$_['text_off']           = 'Kikapcsol';

// Entry
$_['entry_merchant']     = 'Kereskedő ID:';
$_['entry_password']     = 'Fizetési válasz Jelszó:<br /><span class="help">Ez a  WordPay beállítások paneljében kell beállítani.</span>';
$_['entry_callback']     = 'Közvetítő válasz URL:<br /><span class="help">Ez a  WordPay beállítások paneljében kell beállítani. Valamint be kell állítania a "Vásárlói válasz engedélyezését".</span>';
$_['entry_test']         = 'Teszt Mód:';
$_['entry_order_status'] = 'Rendelés állapota:';
$_['entry_geo_zone']     = 'Földrajzi zóna:';
$_['entry_status']       = 'Állapot:';
$_['entry_sort_order']   = 'Sorrend:';

// Error
$_['error_permission']   = 'Figyelmeztetés: Az WorldPay történ? fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_merchant']     = 'Keresked? ID szükséges!';
$_['error_password']     = 'Jelszó szükséges!';
?>
