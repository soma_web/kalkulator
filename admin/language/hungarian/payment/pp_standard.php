<?php

// Heading
$_['heading_title']      = 'PayPal';

// Text
$_['text_payment']       = 'Fizetés';
$_['text_success']       = 'Siker: A PayPal fiók részleteinek módosítása megtörtént!';
$_['text_pp_standard']   = '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal" title="PayPal" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Hitelesítés';
$_['text_sale']          = 'Eladás';

// Entry
$_['entry_email']        = 'E-mail cím:';
$_['entry_test']         = 'Teszt mód:';
$_['entry_transaction']  = 'Tranzakciós módszer:';
$_['entry_geo_zone']     = 'Földrajzi zóna:';
$_['entry_status']       = 'Állapot:';
$_['entry_sort_order']   = 'Sorrend:';
$_['entry_pdt_token']						= 'PDT Token:<br/><span class="help">A fizetési adatok átvitelének még nagyobb biztonsága és megbízhatóság érdekében. Hogyan tehető elérhetővé a PDT (Payment Data Transfer Token)? <a href="https://cms.paypal.com/us/cgi-bin/?&cmd=_render-content&content_ID=developer/howto_html_paymentdatatransfer" alt="">Katt ide!</a></span>';
$_['entry_itemized']						= 'Termékek mutatása egyenként:<br/><span class="help">Termékek egyenkénti felsorolása a Paypal számlán az eladó neve helyett.</span>';
$_['entry_debug']							= 'Debug mód:<br/><span class="help">Több információt tárol a rendszer logon túl.</span>';
$_['entry_order_status']					= 'Rendelési állapot: Teljesítve:<br /><span class="help">Állapot, ami fizetés teljesítése után megjelenítésre kerül.</span>';
$_['entry_order_status_pending']			= 'Rendelési állapot: Függőben:<br /><span class="help">A fizetés függőben; lásd az "Okok" változót. Újabb értesítést fog kapni, amint a fizetés állapota újra megváltozik.</span>';
$_['entry_order_status_denied']				= 'Rendelési állapot: Megtagadva:<br /><span class="help">Ön, a kereskedő megtagadta a fizetést. Ez csak akkor teljesülhet, ha korábban a rendelés függőben volt valamilyen ok miatt.</span>';
$_['entry_order_status_failed']				= 'Rendelési állapot: Sikertelen:<br /><span class="help">A fizetés sikertelen. Akkor jelenik meg, ha a fizetést a vásárló banszámlájáról megkísérelték.</span>';
$_['entry_order_status_refunded']			= 'Rendelési állapot: Visszatérítve:<br /><span class="help">Ön, a kereskedő visszafizeti a vásárló befizetését.</span>';
$_['entry_order_status_canceled_reversal']	= 'Rendelési állapot: Visszavonás törölve:<br /><span class="help">Egy visszavonást töröltek; például Ön, a  kereskedő meggyőzte vásárlóját, így a fizetendő összeg, melyet korábban visszavontak, újra megérkezett Önhöz.</span>';
$_['entry_order_status_reversed']			= 'Rendelési állapot: Visszafordítva:<br /><span class="help">A befizetés vissza lett fordítva valamilyen ok miatt. Az összeget az Ön bankszámlájára terhelték, és visszafizették  a vásárlónak. Az okot a  reason_code variable tartalmazza.</span>';
$_['entry_order_status_unspecified']		= 'Rendelési állapot: Ismeretlen hiba:';
$_['entry_debug']					 = 'Hibakeresési mód:<br/><span class="help">A rendszer naplóba jegyzi a további infórmációkat.</span>';
$_['entry_total']                    = 'Összesen:<br /><span class="help">A teljes ellenőrzésig el kell futnia a rendelésnek mielőtt ez a fizetési mód aktivizálódna.</span>';
$_['entry_canceled_reversal_status'] = 'Visszavonás halasztásának állapota:';
$_['entry_completed_status']         = 'Teljesítési állapot:';
$_['entry_denied_status']			 = 'Visszautasítási állapot:';
$_['entry_expired_status']			 = 'Lejárt állapot:';
$_['entry_failed_status']			 = 'Nem sikerült állapot:';
$_['entry_pending_status']			 = 'Függő állapot:';
$_['entry_processed_status']		 = 'Feldolgozottsági állapot:';
$_['entry_refunded_status']			 = 'Fizetési visszatérítés állapot:';
$_['entry_reversed_status']			 = 'Visszavonási állapot:';
$_['entry_voided_status']		     = 'Érvénytelenítési állapot:';
$_['entry_geo_zone']				 = 'Geo Zóna:';
$_['entry_status']					 = 'Állapot:';
$_['entry_sort_order']				 = 'Sorrend:';
$_['entry_crm_fizetesi_mod']  = 'CRM : fizetési mód:';
$_['entry_crm_fizetesi_hatarido']  = 'CRM : fizetési határidő (nap):';
// Error
$_['error_permission']   = 'Figyelmeztetés: A PayPal fiókon keresztüli fizetés módosítása az Ön számára nem engedélyezett';
$_['error_email']        = 'Az e-mail cím megadása kötelező!';
?>
