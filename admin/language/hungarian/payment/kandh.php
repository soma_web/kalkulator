<?php
// Heading
$_['heading_title']      = 'K&H Bankkártyás Fizetés';

// Text
$_['text_payment']       = 'Fizetés';
$_['text_success']       = 'Siker: A K&H fizetés módosítása megtörtént!';
$_['text_kandh'] 			= '<img src="view/image/payment/kandh.jpg" alt="K&H Bankkártyás fizetés" title="K&H Bankkártyás fizetés" style="border: 1px solid #EEEEEE;" />';

// Entry
$_['entry_order_status'] = 'Rendelés állapota:';
$_['entry_status']       = 'Állapot:';
$_['entry_order_failed_status']       = 'Hibás állapot:';
$_['entry_sort_order']   = 'Rendezés:';
$_['entry_test_mode']       = 'Teszt?:';
$_['entry_shop_code']       = 'A Bolt azonosítója:';

$_['entry_prod_key_file']   = 'Kulcs fájl:';
$_['entry_payment_language'] = 'A banki fizetőfelület nyelve:';
$_['entry_payment_currency'] = 'A fizetés pénzneme:';
$_['entry_test_userid'] = 'Teszt felhasználó id:';


// Error
$_['error_permission']   = 'Figyelmeztetés: Az Kijelentkezés történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_start_transaction'] = 'Hiba a tranzakció indítása során!';
$_['error_init_data'] = "Hiányzó rendelés vagy felhasználó adatok!";
?>