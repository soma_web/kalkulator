<?php
//Added Entry
$_['entry_reach_customer_group']    = 'Váltás vásárlóról, vásárlói csoportra:';
$_['entry_reach_value']             = 'When total order reach:';
$_['entry_reach_order_status']      = 'És a rendelés állapota:';
$_['text_group_note']               = 'A vásárlók át lesznek helyezve a <b>%s</b> csoportba, amikor <b>%s</b> a rendeléseik össz értéke eléri <b>%s</b>';
$_['entry_admin_notify']            = 'Admin Figyelmeztető Email:<span class="help">Email küldése a webáruház tulajdonosának a vásárlói csoport változásról.</span>';
$_['entry_customer_notify']         = 'Vásárló Figyelmeztető Email:<span class="help">Email küldése a vásárlónak, a vásárlói csoportjának változásról.</span>';
?>