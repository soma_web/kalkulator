<?php

// Heading
$_['heading_title']    = 'Vásárlói csoport';

// Text
$_['text_success']     = 'Siker! Módosította a vásárlói csoportot.';

// Column
$_['column_name']      = 'Vásárlói csoport név';
$_['column_action']    = 'Tevékenység';

// Entry
$_['entry_name']       = 'Vásárlói csoport név:';

// Error
$_['error_permission'] = 'Figyelem! Nincs engedélye, hogy módosítsa a vásárlói csoportot!';
$_['error_name']       = 'A vásárlói csoportot neve 3 és 64 karakter közé essen!';
$_['error_default']    = 'Figyelem! Ez a vásárlói csoport nem törölhető, mert jelenleg ez az alapértelmezett csoport az áruházban!';
$_['error_store']      = 'Figyelem! Ez a vásárlói csoport nem törölhető, mert jelenleg használatban van a %s áruházban!';
$_['error_customer']   = 'Figyelem! Ez a vásárlói csoport nem törölhető, mert jelenleg használatban van a %s vevőknél!';
?>
