<?php
// Text
$_['text_about']        = 'Körülbelül';
$_['text_livestream']   = 'FB LiveStream';
$_['text_login']        = 'Bejelentkezés';
$_['text_register']     = 'Fiók készítés';


$_['text_information']  = 'Információ';
$_['text_service']      = 'Ügyfélszolgálat';
$_['text_message']      = 'Üzenjen nekünk!';
$_['text_extra']        = 'Extrák';
$_['text_contact']      = 'Kontakt';
$_['text_submit']       = 'Elküld';
$_['text_contact']      = 'Elérhetőségeink';
$_['text_address']      = 'Iroda és raktár:';
$_['text_email']        = 'E-mail cím';
$_['text_content']      = 'Ide írhatja az üzenetét!';
$_['text_return']       = 'Garancia';
$_['text_sitemap']      = 'Oldaltérkép';
$_['text_manufacturer'] = 'Márkák';
$_['text_voucher']      = 'Ajándékutalványok';
$_['text_affiliate']    = 'Partnerprogram';
$_['text_special']      = 'Akciók';
$_['text_account']      = 'Fiókom';
$_['text_order']        = 'Rendeléstörténet';
$_['text_wishlist']     = 'Kívánságlista';
$_['text_newsletter']   = 'Hírlevél';
$_['text_powered'] 	    = '&copy; 2013 %s Minden jog fenntartva. | Webáruházat készítette és optimalizálta:  <a href="http://www.nmsnet.hu">IDEASOL Magyarország Kft.</a>';
$_['text_pricelist']    	= 'Árlista';
$_['text_kedvezmeny']    	= 'Kedvezmény';
?>