<?php
// Heading
$_['heading_title']	= 'Rendelése feldolgozva!';

// Text
$_['text_customer']	= '<p>Megrendelését sikeresen felvettük! Ha utánvétes szállítást választott, a megrendelt termékek 4-6 munkanapon belül érkeznek meg rendeltetési helyükre.
Ha elsőbbségi-ajánlott szállítást válaszott, kérjük, utalja át a végösszeget az e-mailben kapott számlaszámra/címre. Az összeg megérkezése után 2-4 napon belül kiszállítjuk a termékeket.</p>
<p>Korábbi rendeléseit a <a href="%s">Fiókom</a> oldalon a <a href="%s">Korábbi rendelések</a> hivatkozásra kattintással tekintheti meg.</p><p>Kérjük, hogy kérdéseit az <a href="%s">üzlettulajdonosnak</a> tegye fel.</p><p>Köszönjük, hogy nálunk vásárolt online!</p>';
$_['text_guest']	= '<p>Megrendelését sikeresen felvettük!</p><p>Köszönjük, hogy nálunk vásárolt online! Amennyiben kérdése lenne, kérem <a href="%s">léplen velünk kapcsolatba</a>.</p>';
$_['text_basket']	= 'Kosár';
$_['text_checkout']	= 'Pénztár';
$_['text_success']	= 'Siker';
?>