<?php
//-----------------------------------------------------
// Newsletter Enhancements for Opencart
// Created by @DmitryNek (Dmitry Shkoliar)
// exmail.Nek@gmail.com
//-----------------------------------------------------

// Text
$_['text_subscribe_success'] = 'Sikeres feliratkozás!';
$_['text_subscribe_exists'] = 'Ön már feliratkozott, megrendelés dátuma frissítve.';
$_['text_subscribe_not_valid_email'] = 'Kérjük adjon meg érvényes email címet.';
$_['text_subscribe_not_valid_data'] = 'Kérjük töltsön ki minden mezőt és adjon meg érvényes email címet.';
$_['text_subscribe_no_list'] = 'Kérjük válasszon legalább egy kategóriát.';
$_['text_subscribe_confirmation'] = 'Küldtünk Önnek egy megerősítő emailt-t a megrendelésről.';

$_['text_subscribe'] 			= 'Feliratkozás!';
$_['text_close']			 	= '&times;';
$_['text_timer'] 				= 'Várjon <pre></pre> percet mielőtt bezárja';

$_['entry_name'] 			= 'Név: ';
$_['entry_firstname'] 		= 'Keresztnév: ';
$_['entry_lastname'] 		= 'Vezetéknév: ';
$_['entry_email'] 			= 'Email cím: ';
$_['entry_list'] 			= 'Kereskedelmi kategória: ';

?>
