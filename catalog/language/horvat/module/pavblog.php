<?php 
$_['text_write_by'] = 'Írta: ';
$_['text_published_in'] = 'Kategória: ';
$_['text_created_date'] = 'Készítés dátuma: ';
$_['text_hits'] = 'Megtekintések: ';
$_['text_comment_count'] = 'Hozzászólások: ';
$_['text_readmore'] = 'Bővebben';
$_['text_leave_a_comment'] = 'Hozzászólás';


 $_['error_captcha'] = 'Hibás Ellenőrző Kód';
 $_['error_email'] = 'Hibás e-mail';
 $_['error_comment'] = 'Hibás hozzászólás';
 $_['error_user'] = 'Hibás név';
 $_['text_in_related_by_tag'] = 'Kapcsolódó blogok címkék szerint';
/**
 *
 */
 $_['text_children'] = 'Alkategóriák';
 $_['text_in_same_category'] = 'Azonos kategóriában';
 $_['text_list_comments'] = 'Kommentek';
 $_['text_created'] = 'Létrehozva';
 $_['text_postedby'] = 'Írta';
 $_['text_comment_link'] = 'Hozzászólok';
 $_['entry_name'] = 'Név';
 $_['entry_email'] = 'E-mail';
 $_['entry_comment'] = 'Hozzászólás';
 $_['text_submit'] = 'Küld';
 $_['text_tags'] = 'Címkék: ';
 
 $_['text_like_this'] = 'Tetszik:';
 $_['entry_captcha'] = 'Ellenőrzés';
 /**
  *
  */
  $_['filter_blog_header_title'] = 'Blogok szűrése %s szerint';
  $_['blogs_latest_header_title'] = 'Legutóbbi blogok';

  /*blogcategory module*/
  // Heading 
$_['blog_category_heading_title'] = 'Blog Kategória';

// Text
$_['text_latest']  = 'Legfrissebb';
$_['text_mostviewed']  = 'Legnézettebb';
$_['text_featured']  = 'Kiemelt';
$_['text_bestseller']  = 'Legtöbbet eladott';

/*blogcomment module*/
// Heading 
$_['blogcomment_heading_title'] = 'Legújabb hozzászólások';

$_['text_postedby'] = 'Írta';
/*bloglatest module*/
// Heading 
$_['bloglatest_heading_title'] = 'Legújabb';

// Text
$_['text_latest']  = 'Legújabb blogok';
$_['text_mostviewed']  = 'Legnézettebb';
$_['text_featured']  = 'Kiemelt';
$_['text_bestseller']  = 'Legtöbbet eladott';
$_['entry_show_readmore'] = 'Bővebben' ;
$_['text_readmore'] = 'Bővebben' ;
?>