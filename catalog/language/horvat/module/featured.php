<?php

// Heading
$_['heading_title']  = 'Kiemelt ajánlatok';

// Text
$_['text_reviews']     = '%s vélemény alapján!';
$_['text_egypar']     = 'Pár db';
$_['text_cikkszam']                 = 'Cikkszám: ';

$_['text_akcio_title']        = 'Akciós termék';
$_['text_pardarab_title']     = 'Pár darab';
$_['text_uj_title']           = 'Új termék';
?>
