<?php

// Text
$_['text_title']       = 'Banki előre átutalás';
$_['text_instruction'] = 'Utasítások a banki átutaláshoz';
$_['text_description'] = 'Kérjük, utalja át a teljes összeget a következő bankszámlaszámra.';
$_['text_payment']     = 'Rendelését az összeg jóváírása után teljesítjük.';
?>
