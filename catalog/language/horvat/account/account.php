<?php

// Heading
$_['heading_title']      = 'Fiókom';

// Text
$_['text_account']       = 'Fiók';
$_['text_my_account']    = 'Fiókom';
$_['text_my_orders']     = 'Rendeléseim';
$_['text_my_newsletter'] = 'Hírlevél';
$_['text_edit']          = 'Fiók szerkesztése';
$_['text_password']      = 'Jelszó változtatás';
$_['text_address']       = 'Címjegyzék bejegyzések módosítása';
$_['text_wishlist']      = 'Kívánságlista módosítása';
$_['text_order']         = 'Rendeléstörténet';
$_['text_download']      = 'Letöltések';
$_['text_reward']        = 'Hűségpontok';
$_['text_return']        = 'Garanciális igények megtekintése';
$_['text_transaction']   = 'Tranzakciók';
$_['text_newsletter']    = 'Hírlevél megrendelés / lemondás';
$_['text_newsletter1']   = 'Hírlevél proba1';
$_['text_download_utalvany']    = 'Utalvány letöltés';
$_['text_utalvanyellenorzes']   = 'Utalvány ellenőrzése';
$_['text_arajanlat']     = 'Árajánlataim';

$_['text_edit_paypal']               = 'Fiók módosítása';
$_['text_myproduct']                 = 'Saját termékeim';
$_['text_newproduct']                = 'Új termék felvitel';
$_['text_salereport']                = 'Értékesítési jelentés';
$_['text_productreport_megnezett']   = 'Megnézett kuponok';
$_['text_productreport_megvasarolt'] = 'Letöltött kuponok';
$_['text_jelentesek']                = 'Jelentések - statisztikák';
$_['text_productreport_vasarlok']    = 'Ügyfél letöltések';
$_['text_productreport_rendelesek']  = 'Letöltések csoportosítva';
$_['text_kategoria_hatteradatok']    = 'Szektorbeli kuponokról háttéradatok';
?>
