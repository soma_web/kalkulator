<?php

// Heading
$_['heading_title']                = 'Belépés a fiókba';

// Text
$_['text_facebook_account']        = 'Lépjen be egyszerűen a facebook azonosítójával:';
$_['text_account']                 = 'Fiók';
$_['text_login']                   = 'Belépés';
$_['text_new_customer']            = 'Új vevő vagyok';
$_['text_new_partner']             = 'Új partner vagyok';
$_['text_register']                = 'Regisztrálás';
$_['text_register_account']        = 'Fiók létrehozásával gyorsabban tud vásárolni, naprakész lehet a rendelései állapotát illetően, és figyelemmel kísérheti korábbi rendeléseit.';
$_['text_returning_customer']      = 'Visszatérő vevő';
$_['text_i_am_returning_customer'] = 'Visszatérő vevő vagyok.';

$_['text_returning_partner']      = 'Visszatérő partner';
$_['text_i_am_returning_partner'] = 'Visszatérő partner vagyok.';

$_['text_forgotten']               = 'Elfelejtett jelszó';

// Entry
$_['entry_email']                  = 'E-mail cím:';
$_['entry_password']               = 'Jelszó:';

// Error
$_['error_login']                  = 'Érvénytelen felhasználói név és/vagy jelszó!';
$_['entry_remember_me']            = 'Emlékezz rám';
?>
