<?php

// Heading
$_['heading_title']  = 'Előrendelés';

// Text
$_['text_location']  = 'Web trgovina';
$_['text_contact']   = 'Adataim';
$_['text_address']   = 'Cím:';
$_['text_email']     = 'E-mail cím:';
$_['text_submit']    = 'Küldés';
$_['text_telephone'] = 'Telefon:';
$_['text_fax']       = 'Fax:';
$_['text_message']   = '<p>Ajánlatkérés elküldése az üzlettulajdonosnak sikerült!</p>';
$_['text_close']     = 'Bezár';
$_['text_enquiry']   = 'Tisztelt Webáruház! Előrendelem a terméket, kérem értesítsenek, ha megérkezett. Köszönettel';


// Entry Fields
$_['entry_telefon']     = 'Telefonszám:';
$_['entry_name']        = 'Név:';
$_['entry_email']       = 'E-mail cím:';

$_['entry_varos']       = 'Város:';
$_['entry_iranyitoszam']= 'Irányítószám:';
$_['entry_ceg']         = 'Cég neve:';
$_['entry_adoszam']     = 'Cég adószáma:';
$_['entry_utca']        = 'Utca házszám:';

$_['entry_enquiry']     = 'Érdeklődés:';
$_['entry_captcha']     = 'Írja be az alábbi keretben lévő kódot:';

// Email
$_['email_subject']  = 'Érdeklődés %s';
$_['text_product_id']        = 'Azonosító';
$_['text_product_name']      = 'Neve';
$_['text_product_model']     = 'Modell';
$_['text_product_cikkszam']  = 'Cikkszám';
$_['text_product_gyarto']    = 'Gyártó';

// Errors
$_['error_name']     = 'A név legalább 3, és legfeljebb 32 karakterből álljon!';
$_['error_telefon']  = 'Kérem adja meg a telefonszámot!';
$_['error_iranyitoszam']    = 'Irányítószám megadása kötelező!';
$_['error_varos']  = 'Település megadása kötelező!';
$_['error_utca']  = 'Utca, házszám megadása kötelező!';
$_['error_ceg']  = 'Vállalkozás nevének megadása kötelező!';
$_['error_adoszam']  = 'Vállalkozás adószámának megadása kötelező!';
$_['error_email']    = 'Nem tűnik érvényesnek az e-mail cím!';
$_['error_enquiry']  = 'Az érdeklődés legalább 10, és legfeljebb 3000 karakterből álljon!';
$_['error_captcha']  = 'Nem egyezik a képen láthatóval az ellenőrző kód!';
$_['text_success']  = 'Sikerült! Az üzenetet az üzlettulajdonosnak elküldtük.';
?>
