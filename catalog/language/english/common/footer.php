<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_message']      = 'Send us a message!';
$_['text_submit']       = 'Send';
$_['text_extra']        = 'Extras';
$_['text_email']        = 'E-email address';
$_['text_address']      = 'Office and warehouse:';
$_['text_contact']      = 'Contacts';
$_['text_content']      = 'You can enter your message!';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Vouchers';
$_['text_kategoriak']   = 'Categories';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered'] 	    = '&copy; 2013 %s All rights reserved. | Webshop and optimized by:  <a target="_blank" href="http://www.ideasol.hu">IDEASOL Hungary Ltd., </a><a target="_blank" href="http://premium-webaruhazkeszites.hu/index.php">the webshop specialist</a>';

$_['text_kedvezmeny']    	= 'Discounts';
$_['text_pricelist'] = 'Price List';

$_['text_message']      = 'Send us a message!';
$_['text_email']        = 'E-mail';
$_['text_content']      = 'You can enter your message!';
?>