<?php
// Text
$_['text_home']           = 'Home';
$_['text_wishlist']       = 'Wish List (%s)';
$_['text_wishlist_short']       = 'Wish List';
$_['text_shopping_cart']  = 'Shopping Cart';
$_['text_search']         = 'Search';
$_['text_cart']           = 'Cart';
$_['text_items']    = '%s item - %s';
$_['text_partner']   = 'Partner';
$_['text_ajanlat']   = 'Fill in your proposal';
$_['text_maganszemelyknek']  = 'Individuals';
$_['text_blogs']           = 'Blogs';

$_['text_regisztral_smallest']  = '<a href="%s"><div class="bal_kicsi"><img src="catalog/view/theme/kupon/image/responsive/pos_r_mreg.png" /></div><div class="jobb_kicsi"><span>Register</span></div></a>';
$_['text_welcome_smallest']  = '<a href="%s"><div class="bal_kicsi"><img src="catalog/view/theme/kupon/image/responsive/pos_r_login.png" /></div><div class="jobb_kicsi"><span>Login</span></div></a>';
$_['text_kijelentkezes_smallest']  = '<a href="%s"><div class="bal_kicsi"><img src="catalog/view/theme/kupon/image/responsive/pos_r_logout.png" /></div><div class="jobb_kicsi"><span>Logout</span></div></a>';
$_['text_information_kicsi_smallest']  = '<div class="information_kicsi_smallest"><div class="bal_kicsi"><img src="catalog/view/theme/kupon/image/responsive/pos_r_info.png" /></div><div class="jobb_kicsi"><span>Information</span></div></div>';
$_['text_szuro_kicsi_smallest']  = '<div class="information_kicsi_smallest"><div class="bal_kicsi"><img src="catalog/view/theme/kupon/image/responsive/pos_r_filter.png" /></div><div class="jobb_kicsi"><span>Filter</span></div></div>';
$_['text_ajanlat_kicsi_smallest']  = '<a href="%s"><div class="bal_kicsi"><img src="catalog/view/theme/kupon/image/responsive/pos_r_creg.png" /></div><div class="jobb_kicsi"><span>Offer</span></div></a>';
$_['text_cart_kicsi_smallest']  = '<div class="cart_kicsi_smallest"><div class="bal_kicsi"><img src="catalog/view/theme/kupon/image/responsive/pos_r_cart.png" /></div><div class="jobb_kicsi"><span>Cart</span></div></div>';

$_['text_welcome_kicsi']  = '<a href="%s"><img src="catalog/view/theme/kupon/image/responsive/pos_r_login.png" /></a>';
$_['text_kijelentkezes_kicsi']  = '<a href="%s"><img src="catalog/view/theme/kupon/image/responsive/pos_r_logout.png" /></a>';
$_['text_regisztral_kicsi']  = '<a href="%s"><img src="catalog/view/theme/kupon/image/responsive/pos_r_mreg.png" /></a>';
$_['text_information_kicsi']  = '<img src="catalog/view/theme/kupon/image/responsive/pos_r_info.png" />';


$_['text_welcome']  = '<a href="%s">Login</a>';
$_['text_welcome_belep']  = '<a href="%s">Login</a>';
$_['text_kijelentkezes']  = '<a  href="%s">Logout</a>';
$_['text_regisztral']  = '<a href="%s">Register</a>';
$_['text_logged']   = '<span style="font-size: 13px; position: relative; top: 4px">Logged in:</span> <b><a style="font-size: 17px" href="%s">%s</a></b>';
$_['text_logged_minosegbe']   = '<span style="font-size: 13px; position: relative; top: 4px">Logged in:</span> <b><a style="font-size: 17px" href="%s">%s</a></b>';
$_['text_honlapom']  = '<a href="%s" title="Website">My site</a>';


$_['text_ajanlat_keres']  = 'Ask for price';

/*$_['text_welcome']        = 'Welcome visitor you can <a href="%s">login</a> or <a href="%s">create an account</a>.';
$_['text_logged']         = 'You are logged in as <a href="%s">%s</a> <b>(</b> <a href="%s">Logout</a> <b>)</b>';*/


$_['text_account']        = 'My Account';
$_['text_checkout']       = 'Checkout';
$_['text_motto']    = '&bdquo;Packed in quality&rdquo;';
$_['text_reg']    = 'Register';
$_['text_belep']    = 'Login';
$_['text_facebook_belep']    = 'Facebook Login';
$_['text_arlista']    = 'Price List';
$_['text_kapcsolat']    = 'Contact';
$_['text_termekek']    = 'Products';
$_['text_pricelist'] = 'Price List';
$_['text_language'] = 'Language';
$_['text_currency'] = 'Currency';
$_['text_pricelist']   = 'Pricelist';
$_['text_oldalterkep']    = 'Sitemap';
$_['text_information']  = 'Information';
$_['text_blogreceptek']  = 'Blog recipes';
$_['text_gyik']           = 'Frequently asked';

$_['menu_introduction']      = 'Introduction';
$_['menu_services']          = 'Services';
$_['menu_applications']      = 'Applications';
$_['menu_laser_knowledge']   = 'Laser knowledge';
$_['menu_contact']           = 'Contact';

$_['text_welcome_helios']  = 'Welcome! Please <a href="%s">log in</a> or <a href="%s">register</a>  new account!';
$_['text_logged_helios']   = 'Logged in <b><a href="%s">%s</a></b> <a href="%s">log out</a>';

$_['action']            = 'Special products';
?>