<?php
// Text

$_['text_title']       = '<span style="vertical-align: middle; line-height: 20px; height: 20px;">Payment by credit card </span> <img style="vertical-align: middle; height: 20px;" src="image/payu.png" alt="PayU" title="PayU" style="border: 1px solid #EEEEEE;" />';

//$_['text_title']       = 'Fizetés bankkártyával <a href="http://payu.hu/view/smarty/uploaded_images/120104100228-fizetesi-tajekoztato-20120102.pdf" target="_blank"><img src="image/payu.png" alt="PayU" title="PayU" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_description'] = 'Products %s Order No.: %s';

/*$_['text_payuterms']   = "I accept that the name of the page HUNGAROSACK Ltd. (1047 Budapest, Arany János u. 10th), user
of personal data stored in the database be opened PayU Hungary Ltd. (1074 Budapest, Rákóczi út 70-72.)
as a controller for. The range of data transmitted: username, name, e-mail address. The transmission of data to:
Users with the customer support, transaction confirmation and the
fraud-monitoring in order to protect users. <br />The data provided will not be used for marketing purposes.";*/


$_['text_payuterms']   = "I accept that the name of the page %s (%s), user
of personal data stored in the database be opened PayU Hungary Ltd. (1074 Budapest, Rákóczi út 70-72.)
as a controller for. The range of data transmitted: username, name, e-mail address. The transmission of data to:
Users with the customer support, transaction confirmation and the
fraud-monitoring in order to protect users. <br />The data provided will not be used for marketing purposes.";


$_['text_transaction_failure_title'] = 'Failed Transaction';
$_['text_transaction_failure_description'] = 'Please check you entered during the transaction details are correct.<br />
If all the data is entered correctly, in connection with the investigation of the cause of rejection<br />
please kindly contact card issuing bank.';


$_['text_transaction_success_title'] = 'Successful transactions';
$_['text_transaction_timeout_title'] = 'Transaction timeout';
$_['text_transaction_timeout_description'] = 'The transaction timed out. Please try again.';

?>
