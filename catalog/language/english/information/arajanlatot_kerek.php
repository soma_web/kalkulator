<?php
// Heading
$_['heading_title']  = 'Get a Quote';

// Text 
$_['text_location']  = 'Webshop';
$_['text_contact']   = 'Contact Form';
$_['text_address']   = 'Address:';
$_['text_email']     = 'E-Mail:';
$_['text_telephone'] = 'Telephone:';
$_['text_fax']       = 'Fax:';
$_['text_message']   = '<p>Send Enquiry managed the shop owner!</p>';
$_['text_submit']    = 'Send';
$_['text_map']       = 'Map';
$_['text_close']     = 'Close';

// Entry Fields
$_['entry_name']     = 'First Name:';
$_['entry_email']    = 'E-Mail Address:';
$_['entry_enquiry']  = 'Enquiry:';
$_['entry_captcha']  = 'Enter the code in the box below:';
$_['entry_telefon']  = 'Telephone number:';

// Email
$_['email_subject']  = 'Enquiry %s';


$_['text_product_id']        = 'Identification';
$_['text_product_name']      = 'Name';
$_['text_product_model']     = 'Model';
$_['text_product_cikkszam']  = 'Model';
$_['text_product_gyarto']    = 'Manufacturer';

// Errors
$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']  = 'Enquiry must be between 10 and 3000 characters!';
$_['error_captcha']  = 'Verification code does not match the image!';
?>
