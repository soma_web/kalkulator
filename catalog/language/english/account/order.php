<?php
// Heading 
$_['heading_title']         = 'Order History';

// Text
$_['text_account']          = 'Account';
$_['text_order']            = 'Order Information';
$_['text_order_detail']     = 'Order Details';
$_['text_invoice_no']       = 'Invoice No.:';
$_['text_order_id']         = 'Order ID:';
$_['text_status']           = 'Status:';
$_['text_date_added']       = 'Date Added:';
$_['text_customer']         = 'Customer:';
$_['text_shipping_address'] = 'Shipping Address';
$_['text_shipping_method']  = 'Shipping Method:';
$_['text_payment_address']  = 'Payment Address';
$_['text_payment_method']   = 'Payment Method:';
$_['text_products']         = 'Products:';
$_['text_total']            = 'Total:';
$_['text_comment']          = 'Order Comments';
$_['text_history']          = 'Order History';
$_['text_success']          = 'You have successfully added the products from order ID #%s to your cart!';
$_['text_empty']            = 'You have not made any previous orders!';
$_['text_error']            = 'The order you requested could not be found!';
$_['text_success']          = 'The previous assignments have been put in the shopping cart!';

// Column
$_['column_name']           = 'Product Name';
$_['column_model']          = 'Model';
$_['column_quantity']       = 'Quantity';
$_['column_price']          = 'Price';
$_['column_total']          = 'Total';
$_['column_action']         = 'Action';
$_['column_date_added']     = 'Date Added';
$_['column_status']         = 'Status';
$_['column_comment']        = 'Comment';

$_['column_netto']                  = '<span>Unit Price </span></br><span style="font-size: 10px; ">(Before tax)</span>';
$_['column_netto_total_price']      = '<span>Total Price</span></br><span style="font-size: 10px; "> (Before tax)</span>';
$_['column_price_netto_ar']         = '<span>Unit Price</span></br><span style="font-size: 10px; "> (After tax)</span>';
$_['column_total_netto_ar']         = '<span>Total Price</span></br><span style="font-size: 10px; "> (After tax)</span>';
$_['termek_ar_osszes']              = '<span>All</span>';
$_['szallitas']                     = '<span>Transport</span>';
$_['szallitas_netto_ar']            = '<span>Price (Before tax)</span>';
$_['szallitas_brutto_ar']           = '<span>Price (After tax)</span>';

$_['netto_osszes']          = '<span>Before tax Total</span>';
$_['afa_osszes']            = '<span>Tax Total</span>';
$_['fizetendo_osszes']      = '<span>Price Total</span>';

$_['column_price_netto_egysegar']    = '<span>Unit Price</span></br><span style="font-size: 10px; ">(Before tax)</span>';
$_['column_price_brutto_egysegar']   = '<span>Unit Price</span></br><span style="font-size: 10px; "> (After tax)</span>';
$_['column_price_netto']    = '<span>Price Total</span></br><span style="font-size: 10px; "> (Before tax)</span>';
$_['column_price_brutto']   = '<span>Price Total</span></br><span style="font-size: 10px; "> (After tax)</span>';


// Error
$_['error_warning']         = 'Note: You must select product and process to fulfill the request';
?>
