<?php
// Heading 
$_['heading_title']                = 'Account Login';

// Text
$_['text_facebook_account']        = 'You may simply be a facebook id:';
$_['text_account']                 = 'Account';
$_['text_login']                   = 'Login';
$_['text_new_customer']            = 'New Customer';
$_['text_new_partner']             = 'New partner';

$_['text_register']                = 'Register Account';
$_['text_register_account']        = 'By creating an account you will be able to shop faster, be up to date on an order\'s status, and keep track of the orders you have previously made.';
$_['text_returning_customer']      = 'Returning Customer';
$_['text_i_am_returning_customer'] = 'I am a returning customer';

$_['text_returning_partner']      = 'Returning partner';
$_['text_i_am_returning_partner'] = 'I am a returning partner.';

$_['text_forgotten']               = 'Forgotten Password';

// Entry
$_['entry_email']                  = 'E-Mail Address:';
$_['entry_password']               = 'Password:';

// Error
$_['error_login']                  = 'Warning: No match for E-Mail Address and/or Password.';
$_['entry_remember_me']            = 'Remember me';
?>