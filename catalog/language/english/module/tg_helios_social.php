<?php
// Heading 
$_['heading_title'] 	= 'TG Helios Social';

// Text
$_['text_facebook']  		= 'Become our fan at Facebook';
$_['text_twitter']			= 'Watch ThemeGlobal news at Twitter';
$_['text_vimeo']			= 'See ThemeGlobal event at Vimeo';
$_['text_googletalk']		= 'Chat with us on GTalk';
$_['text_yahoomessenger']		= 'Chat with us on Yahoo';
$_['text_skype']		= 'Chat with us on Skype';
$_['text_flickr']		= 'Watch ThemeGlobal images at Flickr';

?>