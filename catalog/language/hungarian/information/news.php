<?php
//-----------------------------------------------------
// News Module for Opencart v1.5.6   					
// Modified by villagedefrance                          		
// contact@villagedefrance.net                         		
//-----------------------------------------------------

// Heading 
$_['heading_title']   	= 'Aktuális Hírek';

// Text
$_['text_error']      	= 'Nincsenek hírek!';
$_['text_more']  		= 'Tovább';
$_['text_posted'] 		= 'Létrehozva: ';
$_['text_viewed'] 		= '(%s Látták) ';

// Buttons
$_['button_news']    	= 'Vissza a hírekhez';
$_['button_continue']	= 'Folytatás';
?>
