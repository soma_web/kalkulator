<?php
//-----------------------------------------------------
// News Module for Opencart v1.5.6   					
// Modified by villagedefrance                          		
// contact@villagedefrance.net                         		
//-----------------------------------------------------

// Heading 
$_['heading_title']	= 'Legfrisebb hírek';
$_['heading_osszeshir']	= 'Összes hír megtekintése';

// Text
$_['text_more']  	= 'Tovább';
$_['text_posted']	= 'Létrehozva:';

// Buttons
$_['buttonlist']     	= 'Összes hír megtekintése';
?>
