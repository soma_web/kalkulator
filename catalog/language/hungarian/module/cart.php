<?php
// Heading 
$_['heading_title'] = 'Kosár';

// Text
$_['text_items']            = '%s termék - %s';
$_['text_empty']            = 'A kosár jelenleg üres!';
$_['text_cart']             = 'A kosár megtekintése';
$_['text_checkout']         = 'Pénztár';
$_['text_ingyenes_fizetos'] = 'Pénztár / Letöltés';
$_['text_ingyenes']         = 'Letöltés';
$_['total_title']           = 'Összesen:';
$_['text_termek']           = 'Termék';
$_['text_success']          = 'Sikeresen törölte a(z) %s -t a kosárból!';

?>