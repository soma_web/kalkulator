<?php
// Heading 
$_['modal_title'] = 'Árváltozás figyelő';

// Text
$_['text_info']  = 'Tájékoztatást küld, ha a termék ára a megadott összeg alá megy. Kérjük töltse ki az alábbi űrlapot.';
$_['text_set_alert']  = 'Elküld';
$_['text_success']    = 'Árfigyelés elmentve.';

// Entry
$_['entry_name'] = 'Név:';
$_['entry_email'] = 'Email:';
$_['entry_desired_price'] = 'Kívánt ár:';

// Error
$_['error_name']           = 'Név legalább 3 karakter legyen!';
$_['error_email']          = 'Érvénytelen E-mail cím!';
$_['error_desired_price']  = 'Helytelen ár!';
?>