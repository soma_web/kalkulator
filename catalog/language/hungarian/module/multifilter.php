<?php
// Heading
$_['heading_title']                 = 'Multiszűr';
$_['heading_title_category']        = 'Kategória';
$_['heading_title_filter_group']    = 'Beváltás helye';
$_['heading_title_filter']          = 'Beváltási pont';
$_['heading_title_meret']           = 'Méret táblázat';
$_['heading_title_szin']            = 'Szín táblázat';
$_['heading_title_arszuro']         = 'Keresett ár: tól-ig';
$_['heading_title_gyarto']          = 'Gyártók';
$_['heading_title_raktaron']        = 'Raktárkészlet';
$_['entry_raktaron']                = 'Raktáron van';
$_['entry_nincs_raktaron']          = 'Előrendelés szükséges';
$_['entry_filter_ar_osszevont']     = 'Ár szűrő / Kereső';
$_['text_cikkszam']                 = 'Cikkszám: ';

$_['entry_filter_select_artol']     = 'Kedvezmény mértéke min.: ';
$_['entry_filter_select_arig']      = 'Kedvezmény mértéke: ';
$_['text_search']  = '';
$_['filter_name']  = '';
$_['text_szurok_torlese']           = 'Minden szűrés törlése';
?>