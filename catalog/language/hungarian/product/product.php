<?php

// Text
$_['text_pcs']       = 'Készlet';
$_['text_out_of_stock'] = '<span style="color:#999;">Elfogyott</span>';

$_['text_facebook_comment'] = "Facebook Hozzászólás";
$_['text_search']       = 'Keresész';
$_['text_unit_price']   = 'Egységár:';
$_['text_brand']        = 'Márka';
$_['text_manufacturer'] = 'Gyártó:';
$_['text_model']        = 'Modell:';
$_['text_cikkszam']     = 'Cikkszám:';
$_['text_cikkszam2']    = 'Termékkód:';
$_['text_reward']       = 'Jutalom pontok:';
$_['text_sku']          = 'Egyedi azonosító:';
$_['text_meret']        = 'Méret (Hosszúság × Szélesség × Magasság):';
$_['text_points']       = 'Ár jutalom pontban:';
$_['text_stock']        = 'Elérhetőség:';
$_['text_instock']      = 'Raktáron';
$_['text_nincs_raktaron']      = 'Nincs raktáron';
$_['text_compare']      = 'Termék összehasonlítás';
$_['text_wishlist']      = 'Kívánságlista';
$_['text_raktaron']      = 'Raktáron';
$_['text_price']        = 'Ár:';
$_['text_tax']          = 'Nettó:';
$_['text_tax_brutto']   = 'Bruttó:';
$_['text_discount']     = '%s vagy több : <b>%s</b>';
$_['text_option']       = 'Kapható választék:';
$_['text_qty']          = 'Darab:';
$_['text_minimum']      = '(Minimális rendelés: %s %s)';
$_['text_or']           = '- VAGY -';
$_['text_reviews']      = '%s vélemény';
$_['text_write']        = 'Írja meg véleményét';
$_['text_no_reviews']   = 'Legyen Ön az első, aki véleményt készít!';
$_['text_note']         = '<span style="color: #FF0000;">Megjegyzés:</span> A HTML-kód használata nem engedélyezett!';
$_['text_share']        = 'Megosztás';
$_['text_success']      = 'Köszönjük véleményét, melyet továbbítottunk jóváhagyásra a webmesternek.';
$_['text_upload']       = 'Sikeresen feltöltötte a fájlt!';
$_['text_wait']         = 'Kérem, várjon!';
$_['text_tags']         = 'Tag-ek:';
$_['text_error']        = 'A termék nem található!';
$_['text_temkekdij']    = 'Áraink a termékdíjat tartalmazzák!';
$_['text_kaphato']      = 'Beváltható:';
$_['text_szuro_csoport']= 'Beváltás helye:';
$_['text_szuro']        = 'Beváltási pont:';
$_['text_utalvany_erteke']  = 'Az utalvány értéke:';
$_['text_lizing']       = 'Lízing lehetőség';
$_['text_stock_in']     = 'Várható beérkezés:';
$_['text_total']        = 'Összesen:';
$_['text_login']        = 'Árak megjelenítéséhez, vásárláshoz lépjen be, vagy regisztráljon';


$_['text_seat']         = 'Ülőhely:';
$_['text_matchdate']    = 'Dátum:';
$_['text_message']      = 'Kérdését sikeresen fogadtuk!<br /><br /><br />';
$_['heading_title']     = 'Kérdés a termékről';

$_['text_bejelentkezes']        = 'Személyre szabott árért kérjük';
$_['text_bejelentkezes_button'] = 'jelentkezzen be';
$_['text_megyseg']              = 'Mennyiségi egység:';
$_['text_raktarinfo']           = 'Raktárinformáció:';
$_['text_suly']                 = 'Termék súlya:';

$_['text_raktar']               = 'Boltonkénti készlet';
$_['entry_garancia_honap']      = 'hónap';
$_['entry_garancia_ev']         = 'év';
$_['text_garancia']             = 'garancia';
$_['text_szallitas']            = 'Szállítási költség:';

$_['text_netto_ar']             = 'Nettó Ár';


$_['text_akcio_title']        = 'Akciós termék';
$_['text_pardarab_title']     = 'Pár darab';
$_['text_uj_title']           = 'Új termék';
$_['text_ujdonsag_title']     = 'Újdonság';
$_['text_ajandek_title']     = 'Ajándék';

$_['informaciok1']           = 'Hitel lehetőség';
$_['informaciok2']           = 'Csomag ajánlat';
$_['informaciok3']           = 'Szállítás, átvétel';
$_['informaciok4']           = 'Szállítási infok, árak';
$_['informaciok5']           = 'Fizetési módok';


// Entry
$_['entry_name']                = 'Az Ön neve:';
$_['entry_review']              = 'Az Ön véleménye:';
$_['entry_rating']              = 'Értékelés:';
$_['entry_good']                = 'Kitűnő';
$_['entry_bad']                 = 'Rossz';
$_['entry_captcha']             = 'Másolja be a lenti kódot:';
$_['entry_email']               = 'E-Mail cím';
$_['entry_enquiry']             = 'Kérdés';
$_['entry_garancia_honap']      = 'hónap';
$_['entry_garancia_ev']         = 'év';



// Tabs
$_['tab_description']       = 'Leírás';
$_['tab_attribute']         = 'Tulajdonságok';
$_['tab_review']            = 'Vélemények  (%s)';
$_['tab_related']           = 'Kapcsolódó termékek';
$_['tab_image']             = 'Képek';
$_['tab_booking']           = 'Szállásfoglalás';
$_['tab_ask']               = 'Tegye fel kérdését';
$_['tab_meret']             = 'Termék mérete';
$_['tab_test_description']  = 'Tesztek';
$_['tab_ajanlott']          = 'Ajánlott tartozékok';
$_['tab_helyettesito']      = 'Helyettesítő termékek';
$_['tab_kereso']            = 'Tartozék kereső';
$_['tab_ajandek']           = 'Ajándék';
$_['tab_facebook_comments'] = 'Facebook Kommentek';
$_['tab_downloads']        = 'Letöltések';

$_['text_cikkszam']     = 'Cikkszám:';

// Error
$_['error_name']        = 'Hiba: A vélemény címe hosszabb legyen 3, és rövidebb 25 karakternél!';
$_['error_email']       = 'Hibás e-mail cím!';
$_['error_enquiry']       = 'Túl rövid kérdés!';
$_['error_text']        = 'Hiba: A vélemény szövege hosszabb legyen 25, és rövidebb 1000 karakternél!';
$_['error_rating']      = 'Hiba: Kérjük, válassza ki az értékelést!';
$_['error_captcha']     = 'Hiba: Nem egyezik a képen láthatóval az ellenőrző kód!';
$_['error_upload']      = 'Fel kell tölteni!';
$_['error_filename']    = 'A fájlnévnek legalább 3 és legfeljebb 128 karakterből kell állnia!';
$_['error_filetype']    = 'Ismeretlen fájltípus!';

$_['text_ask']              = 'Tegye fel kérdését Nekünk!';
$_['text_question']         = 'Amit tőlünk kérdeztek:';
$_['text_answer']           = 'Válaszunk:';
$_['text_no_questions']     = 'Nem érkezett még kérdés erről a termékről.';
$_['text_no_answer']        = 'Nem érkezett még válasz a kérdésre.';
$_['text_success_question'] = 'Köszönjük érdeklődését. Továbbítottuk a Webáruház tulajdonosnak.';

$_['entry_question']        = 'Az Ön kérdése:';
$_['entry_email']           = 'E-mail címe:';
$_['entry_phone']           = 'Telefonszáma:';

$_['text_plus_afa']         = ' + Áfa';

$_['tab_qa']                = 'Kérdezz felelek';

$_['error_q_author']        = '<strong>Hiba!</strong> Kérjük, adja meg nevét!';
$_['error_q_question']      = '<strong>Hiba!</strong> A kérdés szövege 15 és 1000 karakter között kell, hogy legyen!';
$_['error_q_email']         = '<strong>Hiba!</strong> Kérjük, érvényes e-mail címet adjon meg!';
$_['error_q_phone']         = '<strong>Hiba!</strong> Kérjük, adja meg telefonszámát!';
$_['error_q_custom']        = '<strong>Hiba!</strong> %s szükséges!';
$_['error_q_captcha']       = '<strong>Hiba!</strong> A begépelt kód nem egyezik meg a képen lévővel!';

$_['button_continue']       = 'Küldés';


?>
