<?php

// Heading
$_['heading_title']     = 'Keresés';

// Text
$_['text_search']       = 'A keresési feltétellel megegyező termékek';
$_['text_keyword']      = 'Kulcsszavak';
$_['text_category']     = 'Összes kategória';
$_['text_sub_category'] = 'Szűrés kategóriára';
$_['text_critea']       = 'Keresési feltételek';
$_['text_empty']        = 'Nincs a keresési feltételnek eleget tevő termék.';
$_['text_quantity']     = 'Menny.:';
$_['text_manufacturer'] = 'Márka:';
$_['text_model']        = 'Termékkód:';
$_['text_points']       = 'Jutalom pont:';
$_['text_price']        = 'Ár:';
$_['text_tax']          = 'Nettó:';
$_['text_reviews']      = 'Based on %s reviews.';
$_['text_compare']      = 'Termék összehasonlítás (%s)';
$_['text_display']      = 'Nézet:';
$_['text_list']         = 'Lista';
$_['text_grid']         = 'Rács';
$_['text_stock_in']     = 'Várható beérkezés:';
$_['text_cikkszam']     = 'Cikkszám:';

$_['text_sort']         = 'Rendezés:';
$_['text_default']      = 'Alapértelmezett';
$_['text_name_asc']     = 'Név szerint növekvő';
$_['text_name_desc']    = 'Név szerint csökkenő';
$_['text_price_asc']    = 'Olcsóbbtól a drágábbig';
$_['text_price_desc']   = 'Drágábbtól az olcsóbbig';
$_['text_rating_asc']   = 'Értékelés (Legrosszabbak elől)';
$_['text_rating_desc']  = 'Értékelés (Legjobbak elől)';
$_['text_model_asc']    = 'Cikkszám szerint növekvő';
$_['text_model_desc']   = 'Cikkszám szerint csökkenő';

$_['text_kifuto_title']      = 'Kifutó termék';
$_['text_ujdonsag_title']    = 'Újdonság';
$_['text_uj']                = 'Új';
$_['text_akcios']            = 'Akció';
$_['text_pardarab']          = 'Pár darab';
$_['text_pardarab_title']    = 'Pár darab';
$_['text_mennyisegi']        = 'Mennyiségi kedvezmény';
$_['text_mennyisegi_title']  = 'Mennyiségi kedvezmény';
$_['text_akcio_title']       = 'Akciós termék';
$_['text_uj_title']          = 'Új termék';

$_['text_limit']        = 'Mutat:';

$_['entry_search_model']        = 'Cikkszám:';
$_['entry_search_cikkszam']     = 'Cikkszám:';
$_['entry_search_cikkszam2']    = 'Cikkszám:';
$_['entry_search_name']         = 'Termék:';

$_['text_raktar']               = 'Boltonkénti készlet';
$_['entry_garancia_honap']      = 'hónap';
$_['entry_garancia_ev']         = 'év';
$_['text_garancia']             = 'garancia';
$_['text_szallitas']            = 'Szállítási költség:';

// Entry
$_['entry_search']      = 'Keresés:';
$_['entry_description'] = 'Keresés a termékismertetőkben';
?>
