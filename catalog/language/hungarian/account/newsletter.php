<?php

// Heading
$_['heading_title']    = 'Hírlevél megrendelése';

// Text
$_['text_account']     = 'Fiók';
$_['text_newsletter']  = 'Hírlevél';
$_['text_success']     = 'Siker: Ön sikeresen módosította a hírlevél-megrendelést!';
$_['button_continue']  = 'Mentés';

// Entry
$_['entry_newsletter'] = 'Megrendelés';
?>
