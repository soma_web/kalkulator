<?php

// Heading
$_['heading_title']       = 'Fiókadataim';

// Text
$_['text_account']        = 'Fiók';
$_['text_edit']           = 'Adatmódosítás';
$_['text_your_details']   = 'Személyi adatok';
$_['text_success']        = 'Siker: A fiókadatok módosítása sikerült.';

// Entry
$_['entry_adoszam']       = 'Adószám:';
$_['entry_firstname']     = 'Vezetéknév:';
$_['entry_lastname']      = 'Keresztnév:';
$_['entry_date_of_birth'] = 'Születési dátum:';
$_['entry_email']         = 'E-mail cím:';
$_['entry_telephone']     = 'Telefon:';
$_['entry_fax']           = 'Fax:';
$_['entry_adoszam']       = 'Adószám:';
$_['entry_weblap']        = 'Weblap címe:';
$_['button_continue']     = 'Változtatások mentése';


// Error
$_['error_exists']        = 'Hiba: Már regisztrálták az e-mail címet!';
$_['error_firstname']     = 'Az keresztnév legalább 1, és legfeljebb 32 karakterből álljon!';
$_['error_lastname']      = 'A vezetéknév legalább 1, és legfeljebb 32 karakterből álljon!';
$_['error_email']         = 'Nem tűnik érvényesnek az e-mail cím!';
$_['error_telephone']     = 'A telefonszám legalább 3, és legfeljebb 32 karakterből álljon!';
$_['text_paypaltitle']    = 'Paypal számla részletek';
$_['entry_paypalemail']   = 'Paypal számla Email Id';

$_['select_ferfi']        = 'Férfi';
$_['select_no']           = 'Nő';
$_['entry_nem']           = 'Neme:';
$_['entry_eletkor']       = 'Életkor:';
$_['error_eletkor']       = 'Kérem válassza ki az életkorát';
$_['error_nem']           = 'Kérem válassza ki a nemét';

$_['entry_iskolai_vegzettseg']      = 'Iskolai végzettség';
$_['text_your_cegadatok']           = 'Cégadatok';
$_['entry_vallalkozasi_forma']      = 'Vállalkozási forma:';
$_['entry_szekhely']                = 'Székhely:';
$_['entry_ugyvezeto_neve']          = 'Ügyvezető neve:';
$_['entry_ugyvezeto_telefonszama']  = 'Ügyvezető telefonszáma:';
$_['entry_company']                 = 'Cégnév/Egyéni vállalkozó:';
$_['text_address']       = 'Címjegyzék bejegyzések módosítása';

?>
