<?php

// Text

$_['text_title'] = '<span style="position: relative"><strong>OTP Bankkártyás fizetés</strong><img src="image/otp_simple_logo.jpg" style="position: absolute; top: -7px; width: 34px; margin-left: 10px;"></span>';
$_['text_title2'] = '<strong>Bankkártyás fizetés</strong>';
$_['text_unable'] = 'Rendelési állapota nem található';
$_['otp_description'] = "<h2><b>Tisztelt Vásárlónk!</b></h2>
<p>Webáruházunk részére az OTP Bank Nyrt. biztosítja a kártyaelfogadás lehetőségét, biztonságos, SSL protokollt használó kártyaelfogadó
rendszere által. Bankkártyás fizetéskor Önt átirányítjuk az OTP Bank fizetőoldalára, így a fizetés közvetlenül a Bank által üzemeltetett, a nemzetközi kártyatársaságok szabályai és biztonsági előírásai szerint működő oldalon történik, és nem a webáruház oldalán.
Az internetes áruház az Ön kártya-, illetve a mögötte álló számla adatainak, számának, lejárati dátumának semmilyen formában nincs
birtokában, abba betekintést nem nyerhet.</p>
<p>Kérjük, készítse elő kártyáját! A bankkártyával történő fizetéshez az alábbi adatokra lesz szüksége:
<ul>
    <li>
        <b>Kártyaszám</b> (A kártya előlapján dombornyomással vagy nyomtatással szereplő, 13-19 jegyű szám.)
    </li>
    <li>
        <b>Lejárati dátum</b> (A kártya előlapján dombornyomással vagy nyomtatással szereplő, hh/éé formátumú szám.)
    </li>
    <li>
        <b>Érvényesítési kód</b> (A kártya hátlapján az aláírási panelen található számsor utolsó három jegye {CVV2, vagy CVC2}. Amennyiben az Ön
        kártyáján nem szerepel ilyen kód, a fizető oldalon található, erre vonatkozó mezőt legyen szíves üresen hagyni!)
    </li>
</ul></p>
<p>Áruházunkban az alábbi kártyatípusokkal fizethet:
<ul>
    <li>
        <b>MasterCard</b> (dombornyomott)
    </li>
    <li>
        <b>Visa</b> (dombornyomott)
    </li>
    <li>
        <b>American Express</b> (dombornyomott)
    </li>
    <li>
        <b>Electron</b> (nem dombornyomott) Ezen kártyák esetében a kibocsátó bank határozza meg, hogy lehetővé teszi-e a kártya internetes
        használatát. Amennyiben az Ön Electron kártyáját kibocsátó bank engedélyezte az interneten való kártyahasználatot, Ön természetesen
        fizethet kártyájával internetes áruházunkban. Pontos információért kérjük, forduljon a kártyáját kibocsátó bankhoz! Az OTP Bank által
        kibocsátott kártyák természetesen elfogadhatók.
    </li>
    <li>
        <b>Maestro</b> Az OTP Bank internetes fizetőfelületén bármely bank által kibocsátott Maestro kártya elfogadható. Az elfogadás feltétele, hogy
        az Ön kártyáját kibocsátó bank is támogassa a Maestro kártyák e-commerce tranzakciókra történő felhasználhatóságát. Kérjük, konzultáljon a bankjával!
    </li>
</ul>
</p>
<p>Az internetes kártyaelfogadás biztonságának további növelése érdekében az OTP Bank bevezette a <b>Verified by Visa/MasterCard Secure
Code (VbV/MSC) szolgáltatást</b>. A szolgáltatás lényege, hogy a kártyabirtokos részére a fizetésre használt kártyát kibocsátó bank megad valamilyen plusz azonosítási lehetőséget, amely a tranzakció során ellenőrzésre kerül, és egyértelműen azonosítja a kártyát használó személyt.</p>
<p>Amennyiben az <b>Ön kártyáját kibocsátó banknál nem elérhető a VbV/MSC szolgáltatás</b>, vagy Ön azt nem igényelte, a fizetési folyamat nem változik. A webáruház átirányítja Önt az OTP oldalára, ahol megadja kártyájának adatait (kártyaszám, lejárati dátum, érvényesítési kód),
és megtörténik a vásárolt áruk/szolgáltatások ellenértékének kiegyenlítése.</p>
<p>Amennyiben az <b>Ön bankja rendelkezik a VbV/MSC szolgáltatással</b>, és Ön ezt igénybe veszi, a fizetési tranzakció folyamata megváltozik.
Kártyája adatait (kártyaszám, lejárati dátum, érvényesítési kód) továbbra is OTP Bank fizetőfelületén kell megadnia. Ezt követően azonban a Bank
automatikusan átirányítja Önt a kártyáját kibocsátó bank megfelelő oldalára, ahol végre kell hajtania az azonosítási eljárást. A sikeres azonosítást
követően a fizetési tranzakció folytatódik, Ön értesítést kap a tranzakció sikerességéről, és visszairányításra kerül a webáruházi felületre. Amenynyiben az azonosítást nem tudja végrehajtani, a tranzakció sikertelenül zárul.</p>";


?>
