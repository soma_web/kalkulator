<?php
// Heading
$_['heading_title'] = 'Tranzakció';

// Text
$_['text_message']  = '<p>A tranzakció sikeres.</p><p>Köszönjük a vásárlást!</p>';
$_['text_error_message']  = '<p>Hiba történt! Kérlek vedd fel a kapcsolatot az oldal adminisztrátorával!</p>';

$_['text_account']  = 'Fiók';
$_['text_logout']   = 'Kilépés';

$_['text_budapestbank'] = 'Budapest Bank';

// Text
$_['text_title']    = 'Budapest Bank</br><img src="image/data/budapestbank/komp_nagy_3b.jpg">';
$_['text_reason'] 	= 'OKA';
$_['text_testmode']	= 'Figyelem: A Budapest Bank fizetés teszt módban van. A rendelés nem került felszámolásra.';

$_['text_total']	= 'Szállítás, Anyagmozgatás, Kezdvezmények és Adók'; //Shipping, Handling, Discounts & Taxes


$_['text_invoice']	= 'Rendelés azonosító: '; //Rendelési azonosító
$_['text_products']	= 'Termékek: '; //Termékek
$_['text_name']	    = 'Név: '; //Termékek
?>