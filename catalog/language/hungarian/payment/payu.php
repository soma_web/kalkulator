<?php
// Text

$_['text_title']       = '<span style="vertical-align: middle; line-height: 20px; height: 20px;">Fizetés bankkártyával </span> <img style="vertical-align: middle; height: 20px;" src="image/payu.png" alt="PayU" title="PayU" style="border: 1px solid #EEEEEE;" />';

$_['text_description'] = 'Termékek %s Rendelési szám: %s';


$_['text_payuterms']   = "Elfogadom, hogy a %s (%s) által a %s, felhasználói
adatbázisában tárolt alábbi személyes adataim átadásra kerüljenek a PayU Hungary Kft. (1074 Budapest, Rákóczi út 70-72.),
mint adatkezelő részére. A továbbított adatok köre: felhasználónév, név, e-mail cím. Az adattovábbítás célja:
a felhasználók részére történő ügyfélszolgálati segítségnyújtás, a tranzakciók visszaigazolása és a
felhasználók védelme érdekében végzett fraud-monitoring. <br />Az átadott adatok marketing célra nem kerülnek felhasználásra.";

$_['text_transaction_failure_title'] = 'Sikertelen tranzakció';
$_['text_transaction_failure_description'] = 'Kérjük, ellenőrizze a tranzakció során megadott adatok helyességét.<br />
Amennyiben minden adatot helyesen adott meg, a visszautasítás okának kivizsgálása kapcsán<br />
kérjük, szíveskedjen kapcsolatba lépni kártyakibocsátó bankjával.';


$_['text_transaction_success_title'] = 'Sikeres tranzakció';
$_['text_transaction_timeout_title'] = 'Tranzakció időtúllépés';
$_['text_transaction_timeout_description'] = 'A tranzakció időtúllépés miatt megszakadt. Kérjük próbálja újra.';

?>
