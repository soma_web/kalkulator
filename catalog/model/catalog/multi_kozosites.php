<?php
class ModelCatalogMultiKozosites extends Model {

    /**********************************************************************************************************************************************************/
    // Termékek lekérése a közösítéshez

    public $kozos;
    public $customer_group_id;
    public $customer_kedvezmeny = 1;

    public function getTulajdonsagokProducts($filter_tulajdonsagok="") {
        if (!empty($filter_tulajdonsagok)) {
            $this->load->model("catalog/multifilter");
            $sql = "SELECT  p.product_id FROM " . DB_PREFIX . "attribute a
                LEFT JOIN " . DB_PREFIX . "attribute_description	 ad ON (ad.attribute_id = a.attribute_id)
                LEFT JOIN " . DB_PREFIX . "product_attribute  pa ON (pa.attribute_id = a.attribute_id)
                RIGHT JOIN " . DB_PREFIX . "product  p ON (p.product_id = pa.product_id)
            WHERE ad.language_id = '" . $this->config->get('config_language_id') . "'
                AND p.status=1";

            $beallitasok = $this->config->get('multifilter_beallitasok');

            $szukit_bovit = $this->model_catalog_multifilter->tulajdonsagSzukitBovitLeosztas($filter_tulajdonsagok);



            /*$szukit = false;
            if ($beallitasok['tulajdonsagok']['szukit'] == 1) {
                $szukit = true;
            }*/
            if ($szukit_bovit['tulajdonsagok_szukit']) {
                $sql .= $this->db->sqlSortKozosit("attribute_id","product_attribute",$szukit_bovit['tulajdonsagok_szukit']);
            }
            if ($szukit_bovit['tulajdonsagok_bovit']) {
                $sql .= " AND pa.attribute_id IN (" .$szukit_bovit['tulajdonsagok_bovit']. ") ";
            }

            /*if ($szukit) {
                $sql .= $this->db->sqlSortKozosit("attribute_id","product_attribute",$filter_tulajdonsagok);
            }
            else {
                $sql .= " AND ad.attribute_id IN (" .$filter_tulajdonsagok. ") ";
            }*/

            $sql .= $this->szuresRaktar();
            $query = $this->db->query($sql);

            $vissza = array();
            foreach ($query->rows as $value) {
                $vissza[$value['product_id']] = $value['product_id'];
            }
        } else {
            $vissza = false;
        }

        return $vissza;

    }

    public function getTulajdonsagCsuszkaProducts($filter_csuszka="") {
        $vissza = false;

        if (!empty($filter_csuszka)) {

            $csuszka = explode(',',$filter_csuszka);

            if (!empty($this->request->request['tulajdonsag_csuszka_min']) && !empty($this->request->request['tulajdonsag_csuszka_max']) ) {
                $tol = explode('_',$csuszka[0]);
                $ig = explode('_',$csuszka[1]);

                if ($this->request->request['tulajdonsag_csuszka_min'] == $tol[2] && $this->request->request['tulajdonsag_csuszka_max'] == $ig[2]) {
                    return false;
                }
            }


            $sql = "SELECT  pa.product_id FROM " . DB_PREFIX . "product_attribute pa ";
            $sql .= " LEFT JOIN  ".DB_PREFIX."attribute a ON (a.attribute_id=pa.attribute_id)";
            $sql .= " LEFT JOIN  ".DB_PREFIX."product p ON (p.product_id=pa.product_id)";
            $sql .= " WHERE a.multifilter_csuszka =1 AND pa.language_id='".$this->config->get('config_language_id')."' ";

            $sql .= $this->tulajdonsagCsuszkaWhere($filter_csuszka);




            $sql .= $this->szuresRaktar();
            $query = $this->db->query($sql);

            $vissza = array();
            foreach ($query->rows as $value) {
                $vissza[$value['product_id']] = $value['product_id'];
            }
        }

        return $vissza;

    }

    public function tulajdonsagCsuszkaWhere($filter_csuszka) {
        $csuszka = explode(',',$filter_csuszka);
        $csuszka_tol = array();
        $csuszka_ig  = array();
        foreach($csuszka as $value) {
            if (substr($value,0,3) == "tol") {
                $csuszka_tol[] = $value;
            } else {
                $csuszka_ig[] = $value;
            }
        }

        $sql_sorok = array();
        foreach($csuszka_tol as $value) {
            $ertek = explode('_',$value);
            //$sql_sorok[$ertek[1]][] = " ( replace(pa.text,'.','')  >=".$ertek[2]." AND pa.attribute_id=".$ertek[1].") ";
            $sql_sorok[$ertek[1]][] = " ( pa.text  >=".$ertek[2]." AND pa.attribute_id=".$ertek[1].") ";
        }

        foreach($csuszka_ig as $value) {
            $ertek = explode('_',$value);
            //$sql_sorok[$ertek[1]][] = " ( replace(pa.text,'.','')  <=".$ertek[2]." AND pa.attribute_id=".$ertek[1].") ";
            $sql_sorok[$ertek[1]][] = " ( pa.text  <=".$ertek[2]." AND pa.attribute_id=".$ertek[1].") ";
        }

        $sql_sorok_and = array();
        foreach($sql_sorok as $attribute_csuszka) {
            $sql_sorok_and[] = " ( " . implode(' AND ',$attribute_csuszka). " ) ";
        }
        $sql = '';
        if ($sql_sorok_and) {
            $sql .= " AND ( ";
            $sql .= implode(' OR ',$sql_sorok_and);
            $sql .= " ) ";
        }

        return $sql;
    }


    public function getValasztekProducts($filter_valasztek="") {


        if (!empty($filter_valasztek)) {


            $sql = "SELECT  p.product_id, ovd.name, ov.sort_order, ov.option_value_id FROM " . DB_PREFIX . "option_value ov
                INNER JOIN " . DB_PREFIX . "option_value_description	 ovd ON (ovd.option_value_id = ov.option_value_id)
                INNER JOIN " . DB_PREFIX . "product_option_value	 pov ON (pov.option_value_id = ov.option_value_id)
                INNER JOIN " . DB_PREFIX . "product  p ON (p.product_id = pov.product_id)

                WHERE ovd.language_id = '" . $this->config->get('config_language_id') . "'
                 AND p.status = '1'";

            if ( !empty($filter_valasztek) ) {

                $beallitasok = $this->config->get('multifilter_beallitasok');

                $szukit = false;
                foreach ($beallitasok as $beallitas){
                    if ($beallitas['name'] == "valasztek" && $beallitas['szukit'] == 1) {
                        $szukit = true;
                    }
                }

                if ($szukit) {
                    $sql .= $this->db->sqlSortKozosit("option_value_id","product_option_value",$filter_valasztek);
                } else {
                    $sql .= " AND ov.option_value_id IN (" .$filter_valasztek. ") ";
                }
            }

            $sql .= $this->szuresRaktar();

            $sql .= "  ORDER BY ov.sort_order";
            $query = $this->db->query($sql);

            $vissza = array();
            foreach ($query->rows as $value) {
                $vissza[$value['product_id']] = $value['product_id'];
            }


        } else {
            $vissza = false;
        }

        return $vissza;

    }


    public function getFilterProducts($filter_artol=0, $filter_arig=0, $filter_arszazalek=0) {

        if ($filter_artol > 0 || $filter_arig > 0 || $filter_arszazalek > 0 ) {
            $marad = array();

            $sql = "SELECT * FROM " . DB_PREFIX . "product p WHERE ";

            $elso = true;
            if ($filter_artol > 0) {
                $elso = false;
                $sql .= " ( (szazalek = 0 OR szazalek IS NULL) AND abs(price) >= '" . $filter_artol . "') ";
                if ( $filter_arszazalek == 0)  {
                    $sql .= " OR (szazalek = 1 AND eredeti_ar*abs(price)/100 >= '" .$filter_artol. "') ";
                } else {
                    $sql .= " OR (szazalek = 1 AND abs(price) >= '" . $filter_arszazalek . "') ";
                }
            } elseif ($filter_arszazalek > 0)  {
                $sql .= " ( (szazalek = 1 AND abs(price) >= '" . $filter_arszazalek . "') OR
                    (szazalek = 0 AND abs(eredeti_ar) > abs(price) AND abs(price)/abs(eredeti_ar)*100 >= '" .$filter_arszazalek. "') )";
            }


            if ($filter_arig > 0) {
                if (!$elso ) {
                    $sql .= " AND ";
                }
                $sql .= " abs(price) <= '" .$filter_arig . "'";
            }

            $sql .= $this->szuresRaktar();
            $query = $this->db->query($sql);


            foreach ($query->rows as $value) {
                $marad[$value['product_id']] = $value['product_id'];
            }
            return $marad;
        } else {
            return false;
        }
    }

    public function getFilterProductsTolIg($artol_arrig="", $data_kozos=array()) {

        if (!empty($artol_arrig) || $data_kozos) {
            $customer_kedvezmeny = 1;
            if (isset($_SESSION['customer_id']) && $_SESSION['customer_id'] > 0) {
                $customer_id = $_SESSION['customer_id'];
                $leker =  $this->db->query("SELECT szazalek FROM " . DB_PREFIX . "customer WHERE customer_id = '".$customer_id."'");
                if ($leker->num_rows > 0){
                    $customer_kedvezmeny = 1 - $leker->row['szazalek']/100;
                    $this->customer_kedvezmeny = $customer_kedvezmeny;
                }
            }

            if ($this->customer->isLogged()) {
                $customer_group_id = $this->customer->getCustomerGroupId();
            } else {
                $customer_group_id = $this->config->get('config_customer_group_id');
            }
            $this->customer_group_id = $customer_group_id;
        }

        if (!empty($artol_arrig)) {

            $arszuro = new ModelCatalogMultifilter($this->registry);

            $artol_arrig = explode(',',$artol_arrig);
            $beallitasok = $this->config->get('multifilter_beallitasok');

            $sql = "SELECT p.product_id FROM " . DB_PREFIX . "product p
                LEFT JOIN " . DB_PREFIX . "tax_rule tr ON (p.tax_class_id = tr.tax_class_id AND tr.based='store')
                LEFT JOIN " . DB_PREFIX . "tax_rate trate ON (tr.tax_rate_id=trate.tax_rate_id)

            LEFT JOIN ( SELECT * FROM
            (SELECT price, product_id, customer_group_id FROM " . DB_PREFIX . "product_special
                                WHERE  (date_start <= curdate() OR date_start = '0000-00-00') AND (date_end >= curdate()  OR date_end = '0000-00-00' AND customer_group_id='" .$customer_group_id. "')
                                 ORDER BY priority DESC ) AS ps2 WHERE customer_group_id='" .$customer_group_id. "'  GROUP BY ps2.product_id )
                        AS special ON (p.product_id=special.product_id)";


            if (isset($_SESSION['customer_id']) && $_SESSION['customer_id'] > 0) {
                    $sql .= " LEFT JOIN ( SELECT * FROM
                                (SELECT price, product_id FROM " . DB_PREFIX . "product_customer_special
                                WHERE  (date_start <= curdate() OR date_start = '0000-00-00')
                                    AND (date_end >= curdate()  OR date_end = '0000-00-00')
                                    AND customer_id='" .$_SESSION['customer_id']. "'
                                    AND quantity < 2  ORDER BY price ASC ) AS vevo2
                            GROUP BY vevo2.product_id ) AS vevo ON (p.product_id=vevo.product_id)";

                } else {
                    $sql .= " LEFT JOIN " . DB_PREFIX . "product_customer_special vevo ON (p.product_id = vevo.product_id AND customer_id=0)";
                }

            $sql .= " WHERE p.status=1 ";

            if ($data_kozos) {
                $this->kozos = implode(',',$data_kozos);
                $sql .= " AND p.product_id IN ($this->kozos) ";
            }

            if (isset($artol_arrig[0]) ) {
                $sql .= $arszuro->sqlArszuro($artol_arrig[0],$customer_kedvezmeny,">");
            }

            if (isset($artol_arrig[1])) {
                $sql .= $arszuro->sqlArszuro($artol_arrig[1],$customer_kedvezmeny,"<");
            }

            $sql .= $this->szuresRaktar();
            $query = $this->db->query($sql);

            $marad = array();
            foreach ($query->rows as $value) {
                $marad[$value['product_id']] = $value['product_id'];
            }
            return $marad;

        } else {
            return false;
        }

    }

    public function getSzazalekProducts($filter_szazalek=0) {

        if ($filter_szazalek > 0) {
            $marad = array();

            $sql = "SELECT * FROM " . DB_PREFIX . "product p WHERE ";

            if ($filter_szazalek > 0) {
                $sql .= " (p.szazalek = 1 AND abs(p.price) >= '" .$filter_szazalek . "') OR (p.szazalek = 0 AND abs(p.price)/p.eredeti_ar > '" .$filter_szazalek. "')";
            }
            $sql .= $this->szuresRaktar();
            $query = $this->db->query($sql);

            foreach ($query->rows as $value) {
                $marad[$value['product_id']] = $value['product_id'];
            }
            return $marad;
        } else {
            return false;
        }
    }



    public function getUzletekProducts($data) {

        if ($data){
            $uzletek = explode(',', $data);

            $sql = "SELECT p.product_id FROM " . DB_PREFIX . "product_filter pf
                LEFT JOIN  " . DB_PREFIX . "product p ON (pf.product_id = p.product_id) WHERE  p.status=1 ";


            $beallitasok = $this->config->get('multifilter_beallitasok');

            $szukit = false;
            if ($beallitasok['szuro']['szukit'] == 1) {
                $sql .= $this->db->sqlSortKozosit("filter_select_id","product_filter",$data);
            } else {
                $sql .= " AND filter_select_id IN (".$data.")";
            }


            $sql .= $this->szuresRaktar();

            $sql .= " GROUP BY p.product_id ";
            $query = $this->db->query($sql);

            $vissza = array();

            foreach ($query->rows as $product) {
                $vissza[$product['product_id']] = $product['product_id'];
            }

            return $vissza;
        } else {
            return false;
        }
    }

    public function getVarosokProductS($data) {

        if ($data){
            $sql = "SELECT pf.product_id FROM " . DB_PREFIX . "product_filter pf
                LEFT JOIN  " . DB_PREFIX . "product p ON (pf.product_id = p.product_id) WHERE p.status=1 AND pf.filter_group_id IN (".$data.")";


            $beallitasok = $this->config->get('multifilter_beallitasok');

            $szukit = false;
            if ($beallitasok['szuro_csoport']['szukit'] == 1) {
                $sql .= $this->db->sqlSortKozosit("filter_group_id","product_filter",$data);
            } else {
                $sql .= " AND filter_group_id IN (".$data.")";
            }

            $sql .= $this->szuresRaktar();

            $query = $this->db->query($sql);

            $vissza = array();

            foreach ($query->rows as $product) {
                $vissza[$product['product_id']] = $product['product_id'];
            }


            return $vissza;

        } else {
            return false;
        }
    }

    public function getBoxKategoriakProducts($data) {

        if ($data){
            $parts = explode('_', (string)$data);
            $valasztott = $parts[count($parts)-1];

            $sql = "SELECT p.product_id, c.category_id,  p.quantity, p.date_ervenyes_ig, cd.name, c.sort_order, c.parent_id, c.piacter  FROM " . DB_PREFIX . "category c
               LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)
               LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id)
               LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p2c.category_id = c.category_id)
               INNER JOIN " . DB_PREFIX . "product p ON (p.product_id = p2c.product_id)

                   WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "'
                   AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
                   AND c.status = '1'
                   AND p.status = '1'
                   AND c.parent_id = 0
                   AND p2c.category_id IN (".$data.")
                   ";


            $sql .= $this->szuresRaktar();

            $sql .= " GROUP BY p.product_id";

            $query = $this->db->query($sql);

            if ($query->num_rows > 0) {
                $vissza = array();
                foreach ($query->rows as $product) {
                    $vissza[$product['product_id']] = $product['product_id'];
                }
                return $vissza;
            } else {
                return false;
            }
        } else {
            return false;
        }



    }

    public function getFoKategoriakProducts($data) {

        if ($data){
            $parts = explode('_', (string)$data);
            $valasztott = $parts[count($parts)-1];
        }

        $sql = "SELECT p.product_id, c.category_id,  p.quantity, p.date_ervenyes_ig, cd.name, c.sort_order, c.parent_id, c.piacter  FROM " . DB_PREFIX . "category c
           LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)
           LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id)
           LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p2c.category_id = c.category_id)
           LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = p2c.product_id)
               WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "'
               AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
               AND c.status = '1'";


        $sql .= $this->szuresRaktar();

        $sql .= " GROUP BY c.category_id  ORDER BY c.sort_order, LCASE(cd.name)";

        $query = $this->db->query($sql);

        $categories = $this->buildTree($query->rows, 0, "");


        $newCategories = array();
        foreach($categories as $catkey=>$category) {
            $vissza = $this->categoriaTermekkel($category);
            if($vissza) {
                $newCategories[] = $vissza;
            }

        }

        if (isset($parts)) {
            $ct_ids = $this->categoryFa($newCategories,$parts,array());
        }



        $sql = "SELECT p2c.product_id FROM " . DB_PREFIX . "product_to_category p2c
            LEFT JOIN  " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id) WHERE p.status=1 ";

        if (isset($ct_ids) && $ct_ids){

            $parts = implode(',',$ct_ids);

            $sql .= " AND p2c.category_id IN (".$parts.") ";
        }

        $sql .= $this->szuresRaktar();

        $sql .= " GROUP BY p.product_id ";


        $query = $this->db->query($sql);

        if ($query->num_rows > 0) {
            $vissza = array();
            foreach ($query->rows as $product) {
                $vissza[$product['product_id']] = $product['product_id'];
            }
            return $vissza;
        } else {
            return false;
        }
    }


    public function getAlKategoriakProducts($data) {

        $parts = array();
        if ($data){
            $parts = explode('_', (string)$data);
            $category_id = $parts[count($parts)-1];
        }
        $sql = "SELECT p.product_id, c.category_id,  p.quantity, p.date_ervenyes_ig, cd.name, c.sort_order, c.parent_id, c.piacter  FROM " . DB_PREFIX . "category c
           LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)
           LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id)
           LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p2c.category_id = c.category_id)
           INNER JOIN " . DB_PREFIX . "product p ON (p.product_id = p2c.product_id)
               WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "'
               AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
               AND c.status = '1'
               AND p.status = '1'";

        if (!empty($category_id)) {
            $sql .= " AND c.category_id = '".$category_id."' ";
        }

        if ($parts && false) {
            $sql .= " AND (";
            $ossz_category = count($parts);
            $i = 1;
            foreach($parts as $part) {
                $sql .= " c.category_id = '".$part."' ";
                if ($i < $ossz_category) {
                    $sql .= " OR ";
                }
                $i++;
            }
            $sql .= " ) ";

        }


        $sql .= $this->szuresRaktar();
        $sql .= " ORDER BY c.sort_order, LCASE(cd.name)";
        //$sql .= " GROUP BY c.category_id  ORDER BY c.sort_order, LCASE(cd.name)";
        $query = $this->db->query($sql);

       /* $categories = $this->buildTree($query->rows, 0, "");

        $newCategories = array();
        $termekek = array();
        foreach($categories as $catkey=>$category) {
            $vissza = $this->categoriaTermekkel($category);
            if($vissza) {
                $newCategories[] = $vissza;
            }

        }*/

        /*if (isset($parts)) {
            $ct_ids = $this->categoryFa($newCategories,$parts,array());
        }*/

        if (isset($parts)) {
            $termekek = array();
            foreach($query->rows as $category) {
                //if ($category['category_id'] == $category_id) {
                if (in_array($category['category_id'],$parts) ) {
                    $termekek[] = $category['product_id'];
                }
            }
            $ct_ids = $termekek;
        }

        $sql = "SELECT p2c.product_id FROM " . DB_PREFIX . "product_to_category p2c
            LEFT JOIN  " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id) WHERE p.status=1 ";

        if (isset($ct_ids) && count($ct_ids) > 0) {
            $parts = implode(',',$ct_ids);
            if (!empty($parts)) {
                $sql .= " AND p2c.product_id IN (".$parts.") ";
            } else {
                return false;
            }
        } else {
            return false;
        }

        $sql .= $this->szuresRaktar();
        $sql .= " GROUP BY p.product_id ";
        $query = $this->db->query($sql);

        if ($query->num_rows > 0) {
            $vissza = array();
            foreach ($query->rows as $product) {
                $vissza[$product['product_id']] = $product['product_id'];
            }
            return $vissza;
        } else {
            return false;
        }
    }


    function categoryFa($categories,$path,$tomb) {

        foreach ($categories as $category) {
            if (isset($path[0])) {
                if ($category['category_id'] == $path[0]) {
                    $tomb[] = $category['category_id'];
                    unset ($path[0]);
                    $path = array_merge($path,array());
                    if (isset($category['children'])) {
                        return $this->categoryFa($category['children'],$path,$tomb);
                    } else {
                        return $tomb;
                    }
                }
            } else {
                if (isset($category['children'])) {
                    $tomb[] = $category['category_id'];
                    $tombuj = $this->categoryFa($category['children'],$path,$tomb);
                    $tomb = array_unique(array_merge($tomb, $tombuj));
                } else {
                    $tomb[] = $category['category_id'];
                }
            }
        }
        return $tomb;
    }

    function categoriaTermekkel($categories) {


        if(is_array($categories) && !array_key_exists('product_id', $categories)) {
            $vissza = false;
            foreach($categories as $catkey=>$category) {
                $return = $this->categoriaTermekkel($category);
                if($return) {
                    $vissza = true;
                } else {
                    unset($categories[$catkey]);
                }
            }
            return $categories;

        } elseif(is_array($categories) && array_key_exists('children', $categories)) {
            $return = $this->categoriaTermekkel($categories['children']);
            if(is_array($return) && count($return) > 0) {
                $categories['children'] = $return;
            } else {
                unset($categories['children']);
                $mehet = true;
                if (!is_null($categories['product_id'])  ) {

                    return $categories;

                } else {
                    return false;
                }
            }
            return $categories;

        } else {
            if (!is_null($categories['product_id']) ) {
                return $categories;
            } else {
                return false;
            }
        }

    }

    function buildTree(array $elements, $parentId = 0, $path) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $ha_piacter="";
                if ($element['piacter'] == 1){
                    $ha_piacter.="&piacter=piacter";
                }

                $element['href'] =  $this->url->link('product/category', 'path=' . $path.$element['category_id'].$ha_piacter );
                $children = $this->buildTree($elements, $element['category_id'], $path.$element['category_id']."_");
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }



    public function getGyartoProducts($filter_manufacturer="") {

        if (!empty($filter_manufacturer)) {
            $sql = "SELECT mf.manufacturer_id, mf.name, mf.sort_order, p.product_id FROM " . DB_PREFIX . "manufacturer mf
                INNER JOIN " . DB_PREFIX . "product p ON (p.manufacturer_id = mf.manufacturer_id)
                    WHERE p.status = '1' ";


            $sql .= $this->szuresRaktar();
            $sql .= " AND mf.manufacturer_id IN (" .$filter_manufacturer. ")";
            $sql .= " ORDER BY mf.sort_order";
            $query = $this->db->query($sql);

            $vissza = array();
            foreach ($query->rows as $product) {
                $vissza[$product['product_id']] = $product['product_id'];
            }
            return $vissza;

        } else {
            return false;
        }
    }

    public function getMeretProducts($filter_meret="") {

        if (!empty($filter_meret)) {

            $cache = md5($filter_meret);
            $vissza = $this->cache->get('meret.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id').'.'. $cache);

            if (!$vissza) {
                $sql = "SELECT  p.product_id, od.name, o.sort_order, o.option_id FROM " . DB_PREFIX . "option o
                INNER JOIN " . DB_PREFIX . "option_description	 od ON (od.option_id = o.option_id)
                INNER JOIN " . DB_PREFIX . "product_option	 po ON (po.option_id = o.option_id)
                INNER JOIN " . DB_PREFIX . "product  p ON (p.product_id = po.product_id)

                WHERE od.language_id = '" . $this->config->get('config_language_id') . "'
                 AND p.status = '1'";

                if (!empty($filter_meret)) {

                    $beallitasok = $this->config->get('multifilter_beallitasok');

                    $szukit = false;
                    foreach ($beallitasok as $beallitas) {
                        if ($beallitas['name'] == "meret" && $beallitas['szukit'] == 1) {
                            $szukit = true;
                        }
                    }

                    if ($szukit) {
                        $sql .= $this->db->sqlSortKozosit("option_id", "product_option", $filter_meret);
                    } else {
                        $sql .= " AND o.option_id IN (" . $filter_meret . ") ";
                    }
                }

                $sql .= $this->szuresRaktar();

                $sql .= "  ORDER BY o.sort_order";
                $query = $this->db->query($sql);

                $vissza = array();
                foreach ($query->rows as $value) {
                    $vissza[$value['product_id']] = $value['product_id'];
                }
                $this->cache->set('meret.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $cache, $vissza);
            }


        } else {
            $vissza = false;
        }

        return $vissza;

    }


    public function getSzinProducts($filter_szin="") {

        if (!empty($filter_szin)) {
            $filter_szin = explode(',',$filter_szin);
            sort($filter_szin);
            $filter_szin = implode(',',$filter_szin);

            $cache = md5($filter_szin);
            $vissza = $this->cache->get('szin.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id').'.'. $cache);

            if (!$vissza) {


                $sql = "SELECT  p.product_id, oszd.name, osz.sort_order, osz.option_szin_id FROM " . DB_PREFIX . "option_szin osz
                    INNER JOIN " . DB_PREFIX . "option_szin_description	 oszd ON (oszd.option_szin_id = osz.option_szin_id)

                    INNER JOIN " . DB_PREFIX . "product_option_value	 pov ON (pov.option_szin_id = osz.option_szin_id)
                    INNER JOIN " . DB_PREFIX . "product  p ON (p.product_id = pov.product_id)

                    WHERE oszd.language_id = '" . $this->config->get('config_language_id') . "'
                     AND p.status = '1'";


                $beallitasok = $this->config->get('multifilter_beallitasok');

                $szukit = false;
                foreach ($beallitasok as $beallitas) {
                    if ($beallitas['name'] == "szin" && $beallitas['szukit'] == 1) {
                        $szukit = true;
                    }
                }

                if ($szukit) {
                    $sql .= $this->db->sqlSortKozosit("option_szin_id", "product_option_value", $filter_szin);
                } else {
                    $sql .= " AND osz.option_szin_id IN (" . $filter_szin . ") ";
                }

                $sql .= $this->szuresRaktar();

                $sql .= "  ORDER BY osz.sort_order";
                $query = $this->db->query($sql);

                $vissza = array();
                foreach ($query->rows as $value) {
                    $vissza[$value['product_id']] = $value['product_id'];
                }
                $this->cache->set('szin.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $cache, $vissza);
            }

        } else {
            $vissza = false;
        }

        return $vissza;

    }



    public function szuresRaktar() {

        $sql = "";
        $szuresek = $this->config->get('megjelenit_altalanos');

        if ($szuresek['mennyisegre_szur'] == 1 || (isset($_REQUEST['filter_raktaron']) && $_REQUEST['filter_raktaron'] == 2) ) {
            $sql .= " AND p.quantity > 0 ";
        } elseif (isset($_REQUEST['filter_raktaron']) && $_REQUEST['filter_raktaron'] == 1)  {
            $sql .= " AND p.quantity <= 0 ";
        }

        if ($szuresek['ervenesseg_datumra_szur'] == 1) {
            $sql .= " AND ( p.date_ervenyes_ig = '' OR p.date_ervenyes_ig = '0000-00-00' OR p.date_ervenyes_ig >= curdate() ) ";
        }

        return $sql;
    }

    public function arLekeres($kereso,$sorrend,$limit="") {

        if ($this->customer->isLogged()) {
            $customer_id = $this->customer->isLogged();
        } else {
            $customer_id = 0;
        }

        $cache = md5($kereso.$this->kozos);
        $price_data = $this->cache->get('product_ar.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id').'.'  .(int)$customer_id . '.' .$this->customer_kedvezmeny.'.'.$this->customer_group_id.'.'. $cache);

        if (!$price_data) {

            $sql = "SELECT p.product_id, p.tax_class_id, " . $kereso . "(p.price*" . $this->customer_kedvezmeny . ",
                                    COALESCE(  (SELECT price FROM " . DB_PREFIX . "product_special ps
                                                    WHERE ps.product_id = p.product_id
                                                        AND (date_start <= curdate() OR date_start = '0000-00-00')
                                                        AND (date_end >= curdate()  OR date_end = '0000-00-00')
                                                        AND customer_group_id='" . $this->customer_group_id . "'
                                                ORDER BY ps.priority ASC, ps.price ASC LIMIT 1)
                                            ,p.price)";

            if ($customer_id) {
                $sql .= ", COALESCE(  (SELECT price FROM " . DB_PREFIX . "product_customer_special pcs
                                            WHERE customer_id=" . $customer_id . "
                                                AND pcs.product_id=p.product_id
                                                AND (date_start <= NOW() OR date_start='0000-00-00')
                                                AND (date_end >= NOW() OR date_end='0000-00-00')
                                                AND quantity <= 1
                                            ORDER BY pcs.price ASC LIMIT 1)
                                        ,p.price)";
            }

            $sql .= ") as price ";


            $sql .= " FROM " . DB_PREFIX . "product p WHERE p.status=1 ";
            if ($this->kozos) {
                $sql .= " AND  p.product_id IN (" . $this->kozos . ") ";
            }


            /*$sql .= " ORDER BY price ".$sorrend;
            if ($limit) {
                $sql .= " LIMIT 0,1";
            }
            $query = $this->db->query($sql);
            return $query->num_rows > 0 ? $query->row : false;*/


            $sql .= " ORDER BY price ASC ";
            $query = $this->db->query($sql);
            $price_data = $query->rows;

            $this->cache->set('product_ar.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id').'.'  .(int)$customer_id . '.' .$this->customer_kedvezmeny.'.'.$this->customer_group_id.'.'. $cache,$price_data);
        }


        $vissza = false;
        if ($price_data) {
            $vissza = $sorrend == 'ASC' ? $price_data[0] : $price_data[count($price_data)-1];
        }
        return $vissza;

    }

}

?>