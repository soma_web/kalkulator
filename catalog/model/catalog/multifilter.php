<?php
//require_once(DIR_APPLICATION . 'model/catalog/multi_szurok.php');

class ModelCatalogMultifilter extends Model {



    public function getGlobalSzures($para) {



        $beallitasok        = $this->config->get('multifilter_beallitasok');
        $this->load->model('catalog/multi_kozosites');
        $this->load->model('catalog/multi_szurok');

        $fo_categoria_marad = false;
        $al_categoria_marad = false;
        $box_categoria_marad= false;
        $varos_marad        = false;
        $uzlet_marad        = false;
        $tulajdonsag_marad  = false;
        $valasztek_marad    = false;
        $gyarto_marad       = false;
        $raktar_marad       = false;

        $data_kozos = array();



        if ($beallitasok['tulajdonsagok']['value'] == 1 ) {
            $this->log->setMicroTime();
            $szukit_bovit = isset($para['filter_tulajdonsagok']) && $para['filter_tulajdonsagok'] ? $this->tulajdonsagSzukitBovitLeosztas($para['filter_tulajdonsagok']) : array();
            $this->log->GetMicroTime('Multifilter getGlobalSzures tulajdonsagSzukitBovitLeosztas');

        }

        if ($beallitasok['kategoria_checkbox']['value'] == 1 ) {
            $this->log->setMicroTime();
            $kategoria_box         =   isset($para['filter'])               ? $this->model_catalog_multi_kozosites->getBoxKategoriakProducts($para['filter']) : false;
            $data_kozos[] = $kategoria_box;
            $data_kozos_category = $kategoria_box;
            $this->log->GetMicroTime('Multifilter getGlobalSzures getBoxKategoriakProducts');


        }

        if ($beallitasok['kategoria_normal']['value'] == 1 ) {
            $this->log->setMicroTime();
            $fo_kategoriak         =   isset($para['path'])                 ? $this->model_catalog_multi_kozosites->getFoKategoriakProducts($para['path']) : false;
            $data_kozos[] = $fo_kategoriak;
            $data_kozos_category = $fo_kategoriak;
            $this->log->GetMicroTime('Multifilter getGlobalSzures getFoKategoriakProducts');
        }

        if ($beallitasok['kategoria_sub']['value'] == 1 ) {
            $this->log->setMicroTime();
            $sub_category          =   isset($para['path'])                 ? $this->model_catalog_multi_kozosites->getAlKategoriakProducts($para['path']) : false;
            $data_kozos[] = $sub_category;
            $data_kozos_category = $sub_category;
            $this->log->GetMicroTime('Multifilter getGlobalSzures getAlKategoriakProducts');

        }

        if ($beallitasok['szuro_csoport']['value'] == 1 ) {
            $this->log->setMicroTime();
            $uzletek               =   isset($para['filter_uzlet'])         ? $this->model_catalog_multi_kozosites->getUzletekProducts($para['filter_uzlet'] ) : false;
            $data_kozos[] = $uzletek;
            $this->log->GetMicroTime('Multifilter getGlobalSzures getUzletekProducts');
        }

        if ($beallitasok['szuro']['value'] == 1 ) {
            $this->log->setMicroTime();
            $varosok               =   isset($para['filter_varos'])         ? $this->model_catalog_multi_kozosites->getVarosokProductS($para['filter_varos'] ) : false;
            $data_kozos[] = $varosok;
            $this->log->GetMicroTime('Multifilter getGlobalSzures getVarosokProductS');
        }

        if ($beallitasok['tulajdonsagok']['value'] == 1 ) {
            $this->log->setMicroTime();
            $tulajdonsagok_szukit         =   isset($szukit_bovit['tulajdonsagok_szukit']) && $szukit_bovit['tulajdonsagok_szukit'] ? $this->model_catalog_multi_kozosites->getTulajdonsagokProducts($szukit_bovit['tulajdonsagok_szukit']) : false;
            $tulajdonsagok_bovit          =   isset($szukit_bovit['tulajdonsagok_bovit'])  && $szukit_bovit['tulajdonsagok_bovit']  ? $this->model_catalog_multi_kozosites->getTulajdonsagokProducts($szukit_bovit['tulajdonsagok_bovit'])  : false;

            $data_kozos[] = $tulajdonsagok_szukit;
            $this->log->GetMicroTime('Multifilter getGlobalSzures getTulajdonsagokProducts');
        }

        if ($beallitasok['tulajdonsagok']['value'] == 1 ) {
            $this->log->setMicroTime();
            if (!empty($para['filter_tulajdonsag_csuszka'])) {
                $tulajdonsag_csuszka = $this->model_catalog_multi_kozosites->getTulajdonsagCsuszkaProducts($para['filter_tulajdonsag_csuszka']);
                $data_kozos[] = $tulajdonsag_csuszka;
            }
            $this->log->GetMicroTime('Multifilter getGlobalSzures getTulajdonsagCsuszkaProducts');
        }


        if ($beallitasok['valasztek']['value'] == 1 ) {
            $this->log->setMicroTime();
            $valasztek             =   isset($para['filter_valasztek'])     ? $this->model_catalog_multi_kozosites->getValasztekProducts($para['filter_valasztek']) : false;
            $data_kozos[] = $valasztek;
            $this->log->GetMicroTime('Multifilter getGlobalSzures getValasztekProducts');
        }


        if ($beallitasok['ar_szures']['value'] == 1 || $beallitasok['ar_szures_szazalek']['value'] == 1) {
            $this->log->setMicroTime();
            $artol_arig            =   isset($para['filter_artol'])         ? $this->model_catalog_multi_kozosites->getFilterProducts($para['filter_artol'], $para['filter_arig'], $para['filter_arszazalek']) : false;
            $data_kozos[] = $artol_arig;
            $this->log->GetMicroTime('Multifilter getGlobalSzures getFilterProducts');
        }


        if (isset($beallitasok['meret']['value']) && $beallitasok['meret']['value'] == 1 ) {
            $this->log->setMicroTime();
            $meret                =   isset($para['filter_meret'])        ? $this->model_catalog_multi_kozosites->getMeretProducts($para['filter_meret']) : false;
            $meretnek_ideiglenes = $data_kozos;
            if (!empty($tulajdonsagok_bovit)) {
                $meretnek_ideiglenes[] = $tulajdonsagok_bovit;
            }

            $data_kozos[] = $meret;
            if (empty($tulajdonsagok_bovit)) {
                $tulajdonsagok_bovit = $meret;

            } else {
                if ($meret) {
                    foreach($meret as $value) {
                        if (!in_array($value,$tulajdonsagok_bovit)) {
                            $tulajdonsagok_bovit[$value] = $value;
                        }
                    }
                }
            }

            $this->log->GetMicroTime('Multifilter getGlobalSzures getMeretProducts');
        }

        if (isset($beallitasok['szin']['value']) && $beallitasok['szin']['value'] == 1 ) {
            $this->log->setMicroTime();
            $szin                =   isset($para['filter_szin'])        ? $this->model_catalog_multi_kozosites->getSzinProducts($para['filter_szin']) : false;
            $this->log->GetMicroTime('Multifilter getGlobalSzures getSzinProducts');

            $this->log->setMicroTime();
            $szinnek_ideiglenes   = $data_kozos;
            //$szinnek_ideiglenes[] = $tulajdonsagok_bovit;

            $data_kozos[] = $szin;


            if (!$tulajdonsagok_bovit) {
                $tulajdonsagok_bovit = $szin;

            } else {
                if ($szin) {
                    foreach($szin as $value) {
                        if (!in_array($value,$tulajdonsagok_bovit)) {
                            $tulajdonsagok_bovit[$value] = $value;
                        }
                    }
                }
            }
            $this->log->GetMicroTime('Multifilter getGlobalSzures getSzinProducts kozosités tulajdonságnak');
            $this->log->setMicroTime();

            if (!isset($meretnek_ideiglenes) || (isset($meretnek_ideiglenes) && !$meretnek_ideiglenes) ) {
                $meretnek_ideiglenes = $szin;

            } else {
                if ($szin) {
                    $szin_kigyujt = false;
                    foreach($szin as $value) {
                        if (!in_array($value,$meretnek_ideiglenes)) {
                            $szin_kigyujt[$value] = $value;
                            //$meretnek_ideiglenes[$value] = $value;
                        }
                    }
                    $meretnek_ideiglenes[] = $szin_kigyujt;
                }
            }
            $this->log->GetMicroTime('Multifilter getGlobalSzures getSzinProducts kozosités méretnek');

        }


        $gyartonak_ideiglenes   = $data_kozos;
        $gyartonak_ideiglenes[] = $tulajdonsagok_bovit;

        if ($beallitasok['gyarto']['value'] == 1 ) {
            $this->log->setMicroTime();
            $gyarto                =   isset($para['filter_gyarto'])        ? $this->model_catalog_multi_kozosites->getGyartoProducts($para['filter_gyarto']) : false;
            if ($beallitasok['gyarto']['szukit'] != 1 ) {  // bővített
                $kozos_gyartonak = $this->filterKozosites($gyartonak_ideiglenes);
            }
            $data_kozos[] = $gyarto;



            if (!isset($meretnek_ideiglenes) || !$meretnek_ideiglenes) {
                $meretnek_ideiglenes = $gyarto;

            } else {
                $meretnek_ideiglenes[] = $gyarto;
            }

            if (!isset($szinnek_ideiglenes) || !$szinnek_ideiglenes) {
                $szinnek_ideiglenes = $gyarto;

            } else {
                $szinnek_ideiglenes[] = $gyarto;

            }
            $this->log->GetMicroTime('Multifilter getGlobalSzures getGyartoProducts');
        }




        if (isset($beallitasok['meret']['value']) && $beallitasok['meret']['value'] == 1 ) {
            if ($beallitasok['meret']['szukit'] != 1 ) {  // bővített
                $kozos_meretnek = $this->filterKozosites($meretnek_ideiglenes);
            }
        }


        if (isset($beallitasok['szin']['value']) && $beallitasok['szin']['value'] == 1 ) {
            if ($beallitasok['szin']['szukit'] != 1 ) {  // bővített
                $kozos_szinnek = $this->filterKozosites($szinnek_ideiglenes);
            }
        }


        $kozos_tulajdonsagnakgnak = isset($kozos) && $kozos ? $kozos : $this->filterKozosites($data_kozos);
        $data_kozos[] = isset($tulajdonsagok_bovit) ? $tulajdonsagok_bovit: false;


        if ($beallitasok['ar_szures_tol_ig']['value'] == 1 ) {
            $this->log->setMicroTime();
            if (isset($para['esemeny_kivalto']) && $para['esemeny_kivalto'] == 'mf-artol') {
                $kozos_ar_nelkul = $this->filterKozosites($data_kozos);
                $filter_ar_tol_ig      =   isset($para['filter_ar_tol_ig'])     ? $this->model_catalog_multi_kozosites->getFilterProductsTolIg($para['filter_ar_tol_ig'], $kozos_ar_nelkul) : false;
                $kozos = $filter_ar_tol_ig;
            }
            $this->log->GetMicroTime('Multifilter getGlobalSzures getFilterProductsTolIg');
        }

        $kozos = !empty($kozos) ? $kozos : $this->filterKozosites($data_kozos);

        if (count($kozos) == 0) {
            $kozos = isset($data_kozos_category) ? $data_kozos_category: "";
        }

        if ($beallitasok['kategoria_normal']['value'] == 1 ) {
            $fo_categoria_marad = $this->model_catalog_multi_szurok->veglegesFoKategoria();
            if (!$fo_categoria_marad) {
                $vissza['varos_marad']          = false;
                $vissza['fo_categoria_marad']   = false;
                $vissza['al_categoria_marad']   = false;
                $vissza['uzlet_marad']          = false;
                $vissza['tulajdonsag_marad']    = false;
                $vissza['valasztek_marad']      = false;
                $vissza['gyarto_marad']         = false;
                $vissza['meret_marad']          = false;
                $vissza['szin_marad']           = false;
                $vissza['raktaron_marad']       = false;
                $vissza['artol_ig_marad']       = false;

                return $vissza;
            }
        }


        if ($beallitasok['kategoria_sub']['value'] == 1 ) {
            $this->log->setMicroTime();
            $parts = !empty($para['path']) ? explode('_',$para['path']) : (!empty($this->request->request['path']) ?  explode('_', (string)$this->request->request['path']) : array());
            $al_catetgory_id = isset($parts[0]) ? $parts[0] : 0;

            $al_categoria_marad = $this->model_catalog_multi_szurok->veglegesAlKategoria($parts,$kozos);
            $this->log->GetMicroTime('Multifilter getGlobalSzures veglegesAlKategoria');
        }

        if ($beallitasok['kategoria_checkbox']['value'] == 1 ) {
            $this->log->setMicroTime();
            $box_categoria_marad = $this->model_catalog_multi_szurok->veglegesBoxKategoria($kozos);
            if (!$box_categoria_marad) {
                $box_categoria_marad = array();
            }
            $this->log->GetMicroTime('Multifilter getGlobalSzures veglegesBoxKategoria');
        }

        if ($beallitasok['szuro_csoport']['value'] == 1 ) {
            $this->log->setMicroTime();
            $varos_marad =  !empty($kozos) ?  $this->model_catalog_multi_szurok->veglegesVarosok($kozos) : array();
            $this->log->GetMicroTime('Multifilter getGlobalSzures veglegesVarosok');
        }

        if ($beallitasok['szuro']['value'] == 1 ) {
            $this->log->setMicroTime();
            $uzlet_marad = !empty($kozos) ?  $this->model_catalog_multi_szurok->veglegesuzletek($kozos) : array();
            $this->log->GetMicroTime('Multifilter getGlobalSzures veglegesuzletek');
        }

        if ($beallitasok['tulajdonsagok']['value'] == 1 ) {
            $this->log->setMicroTime();
            $tulajdonsag_marad = !empty($kozos_tulajdonsagnakgnak) ? $this->model_catalog_multi_szurok->veglegesTulajdonsag($kozos_tulajdonsagnakgnak) : array();

            $tulajdonsag_marad_bovit = !empty($kozos) ? $this->model_catalog_multi_szurok->veglegesTulajdonsag($kozos) : array();

            if(!empty($tulajdonsag_marad_bovit)) {
                foreach($tulajdonsag_marad_bovit as $key=>$value) {
                    if ($value['multifilter_szukit'] == 2) {
                        foreach($tulajdonsag_marad as $bovitett) {
                            if ($bovitett['attribute_group_id'] == $value['attribute_group_id']) {
                                $tulajdonsag_marad_bovit[$key]['attributes'] = $bovitett['attributes'];
                                break;
                            }
                        }
                    }
                }
            }
            $tulajdonsag_marad = $tulajdonsag_marad_bovit;
            $this->log->GetMicroTime('Multifilter getGlobalSzures veglegesTulajdonsag');

        }

        if ($beallitasok['tulajdonsagok']['value'] == 1 ) {
            $this->log->setMicroTime();
            $tulajdonsag_csuszka_marad = !empty($kozos) ? $this->model_catalog_multi_szurok->veglegesTulajdonsagCsuszka($kozos): array();
            $this->log->GetMicroTime('Multifilter getGlobalSzures veglegesTulajdonsagCsuszka');
        }

        if ($beallitasok['valasztek']['value'] == 1 ) {
            $this->log->setMicroTime();
            $valasztek_marad = !empty($kozos) ? $this->model_catalog_multi_szurok->veglegesValasztek($kozos): array();
            $this->log->GetMicroTime('Multifilter getGlobalSzures veglegesValasztek');
        }


        if ($beallitasok['gyarto']['value'] == 1 ) {
            $this->log->setMicroTime();
            if ($beallitasok['gyarto']['szukit'] != 1 ) {  // bővített
                $gyarto_marad = $this->model_catalog_multi_szurok->veglegesGyartok($kozos_gyartonak);
            } else {
                $gyarto_marad = !empty($kozos) ? $this->model_catalog_multi_szurok->veglegesGyartok($kozos) : array();
            }
            $this->log->GetMicroTime('Multifilter getGlobalSzures veglegesGyartok');
        }

        if (isset($beallitasok['meret']['value']) && $beallitasok['meret']['value'] == 1 ) {
            $this->log->setMicroTime();
            $meret_marad = !empty($kozos) ? $this->model_catalog_multi_szurok->veglegesMeret( !empty($kozos_meretnek) ? $kozos_meretnek : $kozos): array();
            $this->log->GetMicroTime('Multifilter getGlobalSzures veglegesMeret');
        }

        if (isset($beallitasok['szin']['value']) && $beallitasok['szin']['value'] == 1 ) {
            $this->log->setMicroTime();
            $szin_marad = !empty($kozos) ? $this->model_catalog_multi_szurok->veglegesSzin( (isset($kozos_szinnek) && !empty($kozos_szinnek) ) ? $kozos_szinnek : $kozos): array();
            $this->log->GetMicroTime('Multifilter getGlobalSzures veglegesSzin');
        }


        if ($beallitasok['raktaron']['value'] == 1 ) {
            $this->log->setMicroTime();
            $raktar_marad = !empty($kozos) ? $this->model_catalog_multi_szurok->veglegesRaktar($kozos): array();
            $this->log->GetMicroTime('Multifilter getGlobalSzures veglegesRaktar');
        }

        if ($beallitasok['ar_szures_tol_ig']['value'] == 1 ) {
            $this->log->setMicroTime();
            $artol_[0] = $beallitasok['ar_szures_tol_ig']['tol'];
            $artol_[1] = $beallitasok['ar_szures_tol_ig']['ig'];

            if (isset($para['esemeny_kivalto']) && $para['esemeny_kivalto'] == 'mf-artol') {
                $ar_also_felso_hatar_teljes   = !empty($kozos) ? $this->model_catalog_multi_szurok->veglegesArTolIg($kozos_ar_nelkul)    : $artol_;
                $ar_also_felso_hatar_szurt = explode(',',$para['filter_ar_tol_ig']);

            } else {

                $ar_also_felso_hatar_teljes  = !empty($kozos) ? $this->model_catalog_multi_szurok->veglegesArTolIg($kozos)    : $artol_;

                if (!isset($para['esemeny_kivalto'])) {
                    $ar_also_felso_hatar_szurt = isset($para['filter_ar_tol_ig']) && $para['filter_ar_tol_ig']? explode(',',$para['filter_ar_tol_ig']) : $ar_also_felso_hatar_teljes;

                } else {
                    $ar_also_felso_hatar_szurt   = $ar_also_felso_hatar_teljes;
                }

            }

            $artol_ig_marad[0] = isset($ar_also_felso_hatar_teljes[0]) ? $ar_also_felso_hatar_teljes[0] : $artol_[0]; // alsó határ
            $artol_ig_marad[1] = isset($ar_also_felso_hatar_teljes[1]) ? $ar_also_felso_hatar_teljes[1] : $artol_[1]; // felső határ

            $artol_ig_marad[2] = isset($ar_also_felso_hatar_szurt[0])  ? $ar_also_felso_hatar_szurt[0] : $artol_ig_marad[0]; // alsó határ szurt
            $artol_ig_marad[3] = isset($ar_also_felso_hatar_szurt[1])  ? $ar_also_felso_hatar_szurt[1] : $artol_ig_marad[1]; // felső határ szurt
            $this->log->GetMicroTime('Multifilter getGlobalSzures veglegesArTolIg');

        }


        $vissza['varos_marad']          = $varos_marad;
        $vissza['box_categoria_marad']  = $box_categoria_marad;
        $vissza['fo_categoria_marad']   = $fo_categoria_marad;
        $vissza['al_categoria_marad']   = $al_categoria_marad;
        $vissza['uzlet_marad']          = $uzlet_marad;
        $vissza['tulajdonsag_marad']    = $tulajdonsag_marad;
        $vissza['valasztek_marad']      = $valasztek_marad;
        $vissza['gyarto_marad']         = $gyarto_marad;
        $vissza['meret_marad']          = isset($meret_marad) ? $meret_marad : false;
        $vissza['szin_marad']           = isset($szin_marad)  ? $szin_marad : false;
        $vissza['raktaron_marad']       = $raktar_marad;
        $vissza['artol_ig_marad']       = isset($artol_ig_marad) ? $artol_ig_marad : false;
        $vissza['tulajdonsag_csuszka_marad']       = $tulajdonsag_csuszka_marad;


        return $vissza;
    }


    public function filterKozosites($data) {

        $mainfilter = array();
        $kozos = null;

        foreach ($data as $filter) {
            if(is_array($filter)) {
                $mainfilter[] = $filter;
                if($kozos == null) {
                    $kozos = $filter;
                }
            }
        }

        if(count($mainfilter) > 1) {
            foreach($mainfilter as $currentfilter) {
                $kozos = array_intersect($kozos, $currentfilter);
            }
        }

        return $kozos;
    }






    public function getMultiProducts($data = array(),$total=false) {
        if ($this->customer->isLogged()) {
            $customer_group_id = $this->customer->getCustomerGroupId();
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }



        $cache = md5(http_build_query($data));

        if ($total) {
            $product_data = $this->cache->get('product_total.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . (int)$customer_group_id . '.' . $cache);
        } else {
            $product_data = $this->cache->get('product.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . (int)$customer_group_id . '.' . $cache);
        }

        if (!$product_data) {
            $beallitasok = $this->config->get('multifilter_beallitasok');
            $megjelenit_termekful = $this->config->get('megjelenit_admin_termekful');

            if ( $megjelenit_termekful['elhelyezkedes'] == 0) {
                $elhelyezkedes_products = $this->getElhelyezkedesProducts($data);
                if ($elhelyezkedes_products) {
                    $kivonas =  implode(',',$elhelyezkedes_products['product']);
                }
            }

            $customer_kedvezmeny = 1;

            if (isset($_SESSION['customer_id']) && $_SESSION['customer_id'] > 0) {
                $customer_id = $_SESSION['customer_id'];
                $leker =  $this->db->query("SELECT szazalek FROM " . DB_PREFIX . "customer WHERE customer_id = '".$customer_id."'");
                if ($leker->num_rows > 0){
                    $customer_kedvezmeny = 1 - $leker->row['szazalek']/100;
                }
            }

            if ($total) {
                $sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p ";
            } else {
                $sql = "SELECT p.product_id, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1'
                    GROUP BY r1.product_id) AS rating";

                if (isset($data['sort']) && $data['sort'] == "p.price") {
                    $sql .= $this->cart->getSqlMinimumPriceRow();
                }

                $sql .= " FROM " . DB_PREFIX . "product p ";
            }

            $sql .=" LEFT JOIN ( SELECT * FROM
                            (SELECT price, product_id, customer_group_id FROM " . DB_PREFIX . "product_special
                                WHERE  (date_start <= curdate() OR date_start = '0000-00-00') AND (date_end >= curdate()  OR date_end = '0000-00-00' AND customer_group_id='" .$customer_group_id. "')
                                 ORDER BY priority DESC ) AS ps2 WHERE customer_group_id='" .$customer_group_id. "'  GROUP BY ps2.product_id )
                        AS special ON (p.product_id=special.product_id)";


                if (isset($_SESSION['customer_id']) && $_SESSION['customer_id'] > 0) {
                    $sql .= " LEFT JOIN ( SELECT * FROM
                    (SELECT price, product_id FROM " . DB_PREFIX . "product_customer_special
                        WHERE  (date_start <= curdate() OR date_start = '0000-00-00') AND (date_end >= curdate()  OR date_end = '0000-00-00') AND customer_id='" .$_SESSION['customer_id']. "'
                         AND quantity < 2  ORDER BY price ASC ) AS vevo2
                        GROUP BY vevo2.product_id )
                         AS vevo ON (p.product_id=vevo.product_id)";

                } else {
                    $sql .= " LEFT JOIN " . DB_PREFIX . "product_customer_special vevo ON (p.product_id = vevo.product_id AND customer_id=0)";
                }




                $sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
                LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)

                LEFT JOIN " . DB_PREFIX . "tax_rule tr ON (p.tax_class_id = tr.tax_class_id AND tr.based='store')
                LEFT JOIN " . DB_PREFIX . "tax_rate trate ON (tr.tax_rate_id=trate.tax_rate_id)";

            if (!empty($data['filter_tag'])) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product_tag pt ON (p.product_id = pt.product_id)";
            }
            if (!empty($data['filter_category'])) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";
            }
            if (!empty($data['filter_varos']) || !empty($data['filter_uzlet'])) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product_filter f2v ON (p.product_id = f2v.product_id)";
            }
            if ( !empty($data['filter_tulajdonsagok']) || !empty($data['filter_tulajdonsag_csuszka'])) {
                if (is_array($data['filter_tulajdonsagok'])) {
                    foreach($data['filter_tulajdonsagok'] as $group_key=>$value) {
                        $value = implode(',',$value);
                        $szukit_bovit = $this->tulajdonsagSzukitBovitLeosztas($value);

                        if ($szukit_bovit['tulajdonsagok_szukit']) { // Javitani !!!!
                            $sql .= $this->db->sqlSortKozosit("attribute_id","product_attribute",$value);
                        }
                        if ($szukit_bovit['tulajdonsagok_bovit']) {
                            $sql .= " LEFT JOIN " . DB_PREFIX ."product_attribute pa".$group_key." ON (p.product_id = pa".$group_key.".product_id)";
                        }
                    }
                } else {
                    //$sql .= " LEFT JOIN " . DB_PREFIX . "product_attribute pa ON (p.product_id = pa.product_id)";
                }

            }
            if ( !empty($data['filter_tulajdonsag_csuszka'])) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product_attribute pa ON (p.product_id = pa.product_id)";
            }


                if ( (isset($data['filter_valasztek']) && !empty($data['filter_valasztek'])) || (isset($data['filter_szin']) && !empty($data['filter_szin'])) ) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product_option_value pov ON (p.product_id = pov.product_id)";
            }
            if ( isset($data['filter_meret']) && !empty($data['filter_meret']) ) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product_option po ON (p.product_id = po.product_id)";
            }


            $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'
                AND p.status = '1'
                AND p.date_available <= curdate()
                AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

            if (!$total) {
                if ( $megjelenit_termekful['elhelyezkedes'] == 0) {
                    if ($elhelyezkedes_products) {
                        $sql .= " AND p.product_id NOT IN(". $kivonas. ")";
                    }
                }
            }

            $szuresek = $this->config->get('megjelenit_altalanos');

            if ($szuresek['mennyisegre_szur'] == 1 || (isset($_REQUEST['filter_raktaron']) && $_REQUEST['filter_raktaron'] == 2) ) {
                $sql .= " AND p.quantity > 0 ";
            } elseif (isset($_REQUEST['filter_raktaron']) && $_REQUEST['filter_raktaron'] == 1)  {
                $sql .= " AND p.quantity <= 0 ";
            }

            if ($szuresek['ervenesseg_datumra_szur'] == 1) {
                $sql .= " AND ( p.date_ervenyes_ig = '' OR p.date_ervenyes_ig = '0000-00-00' OR p.date_ervenyes_ig >= curdate() ) ";
            }



            /* Árak */
            if (!empty($data['filter_ar_tol_ig']) ) {

                $artol_arrig = explode(',',$data['filter_ar_tol_ig']);
                if (isset($artol_arrig[0])) {
                    $sql .= $this->sqlArszuro($artol_arrig[0],$customer_kedvezmeny,">");
                }

                if (isset($artol_arrig[1]) ) {
                    $sql .= $this->sqlArszuro($artol_arrig[1],$customer_kedvezmeny,"<");
                }
            }

            if (!empty($data['filter_artol']) ) {
                $sql .= " AND (( (p.szazalek = 0 OR p.szazalek IS NULL) AND abs(p.price) >= '" . $data['filter_artol'] . "') ";
                if (empty($data['filter_arszazalek']) ) {
                    $sql .= "OR (p.szazalek = 1 AND p.eredeti_ar*abs(p.price)/100 >= '" .$data['filter_artol']. "') )";
                } else {
                    $sql .= "OR (p.szazalek = 1 AND abs(p.price) >= '" . $data['filter_arszazalek'] . "') )";
                }

            } elseif (!empty($data['filter_arszazalek']) ) {
                $sql .= " AND ( (p.szazalek = 1 AND abs(p.price) >= '" . $data['filter_arszazalek'] . "') OR
                (p.szazalek = 0 AND abs(p.eredeti_ar) > abs(p.price) AND abs(p.price)/abs(p.eredeti_ar)*100 >= '" .$data['filter_arszazalek']. "') )";
            }

            if (!empty($data['filter_arig']) ) {
                $sql .= " AND abs(p.price) <= '" . $data['filter_arig'] . "'";
            }
            /* Árak vége*/

            if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
                $sql .= " AND (";
                if (!empty($data['filter_name'])) {
                    $implode = array();

                    $words = explode(' ', $data['filter_name']);

                    foreach ($words as $word) {
                        if (!empty($data['filter_description'])) {
                            $implode[] = "LCASE(pd.name) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%' OR LCASE(pd.description) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%'";
                        } else {
                            $implode[] = "LCASE(pd.name) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%'";
                        }
                    }
                    if ($implode) {
                        $sql .= " " . implode(" OR ", $implode) . "";
                    }
                }

                if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
                    $sql .= " OR ";
                }
                if (!empty($data['filter_tag'])) {
                    $implode = array();
                    $words = explode(' ', $data['filter_tag']);
                    foreach ($words as $word) {
                        $implode[] = "LCASE(pt.tag) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%' AND pt.language_id = '" . (int)$this->config->get('config_language_id') . "'";
                    }
                    if ($implode) {
                        $sql .= " " . implode(" OR ", $implode) . "";
                    }
                }
                $sql .= ")";
            }


            if (!empty($data['filter_category'])) {
                $ct_id = explode(",",$data['filter_category']);
                //$ct_id = array_pop($ct_id);
                $ct_id = array_reverse($ct_id);
                if ($beallitasok['kategoria_checkbox']['value'] == 1 ) {
                    $sql .= " AND p2c.category_id IN(" .$data['filter_category']. ")";
                } else {
                    $sql .= " AND p2c.category_id IN(" .$ct_id[0]. ")";
                }
            }

            if (!empty($data['filter_varos'])) {
                if ($beallitasok['szuro_csoport']['szukit'] == 1) {
                    $sql .= $this->db->sqlSortKozosit("filter_group_id","product_filter",$data['filter_varos']);
                } else {
                    $sql .= " AND f2v.filter_group_id IN (".$data['filter_varos'].")";
                }
            }


            if (!empty($data['filter_uzlet'])) {
                if ($beallitasok['szuro_csoport']['szukit'] == 1) {
                    $sql .= $this->db->sqlSortKozosit("filter_select_id","product_filter",$data['filter_uzlet']);
                } else {
                    $sql .= " AND f2v.filter_select_id IN (".$data['filter_uzlet'].")";
                }
            }

            if ( !empty($data['filter_tulajdonsagok']) ) {
                if (is_array($data['filter_tulajdonsagok'])) {
                    foreach($data['filter_tulajdonsagok'] as $group_key=>$value) {
                        $value = implode(',',$value);
                        $szukit_bovit = $this->tulajdonsagSzukitBovitLeosztas($value);

                        if ($szukit_bovit['tulajdonsagok_szukit']) { // Javitani !!!!
                            $sql .= $this->db->sqlSortKozosit("attribute_id","product_attribute",$value);
                        }
                        if ($szukit_bovit['tulajdonsagok_bovit']) {
                            $sql .= " AND pa".$group_key.".attribute_id IN (" .$value. ") ";
                        }
                    }
                } else {
                    $szukit_bovit = $this->tulajdonsagSzukitBovitLeosztas($data['filter_tulajdonsagok']);

                    if ($szukit_bovit['tulajdonsagok_szukit']) {
                        $sql .= $this->db->sqlSortKozosit("attribute_id","product_attribute",$szukit_bovit['tulajdonsagok_szukit']);
                    }
                    if ($szukit_bovit['tulajdonsagok_bovit']) {
                        $sql .= " AND pa.attribute_id IN (" .$szukit_bovit['tulajdonsagok_bovit']. ") ";
                    }
                }
            }

            if ( !empty($data['filter_tulajdonsag_csuszka']) ) {
                $this->load->model('catalog/multi_kozosites');
                $sql .= $this->model_catalog_multi_kozosites->tulajdonsagCsuszkaWhere($data['filter_tulajdonsag_csuszka']);
                $sql .= " AND pa.language_id='".$this->config->get('config_language_id')."' ";


            }


            if ( isset($data['filter_valasztek']) && !empty($data['filter_valasztek']) ) {
                if ($beallitasok['valasztek']['szukit'] == 1) {
                    $sql .= $this->db->sqlSortKozosit("option_value_id","product_option_value",$data['filter_valasztek']);
                }
                else {
                    $sql .= " AND pov.option_value_id IN (" .$data['filter_valasztek']. ") ";
                }
            }

            if (!empty($data['filter_meret'])) {
                $sql .= " AND po.option_id IN(" .$data['filter_meret']. ")";
            }

            if (!empty($data['filter_szin'])) {
                $sql .= " AND pov.option_szin_id IN(" .$data['filter_szin']. ")";
            }

            if (!empty($data['filter_gyarto'])) {
                $sql .= " AND p.manufacturer_id IN(" .$data['filter_gyarto']. ")";
            }

            if (!empty($data['filter_manufacturer_id'])) {
                $sql .= " AND p.manufacturer_id IN(" .$data['filter_manufacturer_id']. ")";
            }


            if (!$total) {
                $sql .= " GROUP BY p.product_id";

                $sort_data = array(
                    'pd.name',
                    'p.model',
                    'p.cikkszam',
                    'p.quantity',
                    'p.price',
                    'rating',
                    'p.sort_order',
                    'p.date_added'
                );


                $utalvany_sorrend="";
                if ($this->config->get('megjelenit_utalvany_szerint_rendez') == 1) {
                    if ($this->config->get('megjelenit_utalvany_szerint_rendez_ask') == 1) {
                        $utalvany_sorrend = " p.utalvany DESC, ";
                    } else {
                        $utalvany_sorrend = " p.utalvany ASC, ";
                    }
                }

                if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                    if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
                        if ($data['sort'] == 'p.model') {

                            $megjelenit_product = $this->config->get('megjelenit_product');
                            if (isset($megjelenit_product['product_cikkszam_termeknevben']) && $megjelenit_product['product_cikkszam_termeknevben']) {
                                $sql .= " ORDER BY $utalvany_sorrend LCASE(p.cikkszam)";
                            } else {
                                $sql .= " ORDER BY $utalvany_sorrend LCASE(" . $data['sort'] . ")";
                            }

                        } else {
                            $sql .= " ORDER BY $utalvany_sorrend LCASE(" . $data['sort'] . ")";
                        }

                    } elseif (isset($data['sort']) && $data['sort'] == "p.price") {
                        $sql .= " ORDER BY " . $utalvany_sorrend . 'minimum_price';

                    } else {
                        $sql .= " ORDER BY " . $utalvany_sorrend . $data['sort'];
                    }
                } else {
                    $sql .= " ORDER BY p.sort_order";
                }

                if (isset($data['order']) && ($data['order'] == 'DESC')) {
                    $sql .= " DESC, LCASE(pd.name) DESC";
                } else {
                    $sql .= " ASC, LCASE(pd.name) ASC";
                }

                if (isset($data['start']) || isset($data['limit'])) {
                    if ($data['start'] < 0) {
                        $data['start'] = 0;
                    }

                    if ($data['limit'] < 1) {
                        $data['limit'] = 20;
                    }

                    $old_limit = $data['limit'];
                    $old_start = $data['start'];


                    if ( $megjelenit_termekful['elhelyezkedes'] == 0 && $elhelyezkedes_products) {
                        $limitbol = count($elhelyezkedes_products['teljes']);
                        if ( $data['start'] == 0) {
                            $data['limit'] = $limitbol <  $data['limit'] ? $data['limit'] -$limitbol : 0;

                        } elseif ( ($data['start']+$data['limit'])  <= $limitbol) {
                            $data['start'] = 0;
                            $data['limit'] = 0;

                        } elseif ($data['start'] == $limitbol  ) {
                            $data['start'] = 0;

                        } elseif ($data['start'] < $limitbol && ($data['start']+$data['limit']) >= $limitbol  ) {
                            $data['limit'] =  $data['start']+$data['limit']-$limitbol;
                            $data['start'] = 0;


                        } elseif ( $data['start']  >= $limitbol) {
                            $data['start'] = $data['start'] - $limitbol;
                        }

                    }

                    $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
                }
            }

            $product_data = array();

            $query = $this->db->query($sql);

            if ($total) {
                $product_data = $query->row['total'];
                $this->cache->set('product_total.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . (int)$customer_group_id . '.' . $cache, $product_data);

                return $query->row['total'];
            }

            $product = new ModelCatalogProduct($this->registry);

            if ( $megjelenit_termekful['elhelyezkedes'] == 0 && $elhelyezkedes_products) {
                $termekek = array();

                if (isset($old_start) || isset($old_limit)) {

                    if ($old_start == 0) {
                        $i = 0;
                        foreach($elhelyezkedes_products['teljes'] as $value) {
                            if ($i >= $old_limit) {
                                break;
                            }
                            $termekek[] = $value;
                            $i++;
                        }
                    } elseif ($old_start == $limitbol){

                    } elseif ($old_start < $limitbol){
                        $i = 0;
                        foreach($elhelyezkedes_products['teljes'] as $value) {

                            if ($i >= $old_start+ $old_limit){
                                break;
                            }
                            if ($i >= $old_start) {
                                $termekek[] = $value;
                            }
                            $i++;
                        }
                    }

                } else {
                    $termekek = $elhelyezkedes_products['teljes'];
                }

                foreach ($query->rows as $result) {
                    $termekek[] = $result;
                }

                foreach ($termekek as $result) {
                    $product_data[$result['product_id']] = $product->getProduct($result['product_id']);
                }
            } else {
                foreach ($query->rows as $result) {
                    $product_data[$result['product_id']] = $product->getProduct($result['product_id']);
                }
            }

            $this->cache->set('product.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . (int)$customer_group_id . '.' . $cache, $product_data);
        }

        return $product_data;
    }




    public function sqlTax($artipus,$relacio,$artol_arig,$customer_kedvezmeny=1) {
        return  " AND ( p.tax_class_id > 0  AND ( (trate.type='P' AND  round(".$artipus.".price*(trate.rate/100+1)*".$customer_kedvezmeny.") ".$relacio."= '" .$artol_arig. "')
                                                  OR  (trate.type='F' AND  (round(".$artipus.".price+(trate.rate)*".$customer_kedvezmeny.")) ".$relacio."= '" .$artol_arig. "')
                                                )

                  OR   (p.tax_class_id = 0  AND abs(".$artipus.".price)*".$customer_kedvezmeny." ".$relacio."= '" . $artol_arig."')
                      ) ";
    }

    public function sqlArszuro($artol_arrig,$customer_kedvezmeny=0,$relacio) {

        $vissza = " AND ( ";

        $special = "";
        $vevo    = "";
        $vevo_altalanos = "";

        $special = "(";
        $special .= " (special.price <= vevo.price OR vevo.price IS NULL) AND  (special.price > 0 AND special.price < p.price";
        if ($customer_kedvezmeny > 0 ) {
            $special .= " AND abs(special.price) <= abs(p.price)*$customer_kedvezmeny ";
        }
        $special .= $this->sqlTax("special", $relacio, $artol_arrig).' ) ';
        $special .= " OR (special.price >= p.price";
        $special .= $this->sqlTax("p", $relacio, $artol_arrig).' ) ';

        $special .= ")";


        $vevo .= " OR (vevo.price < special.price OR special.price IS NULL) AND  vevo.price > 0 AND vevo.price < p.price";
        if ($customer_kedvezmeny > 0 ) {
            $vevo .= " AND abs(vevo.price) <= abs(p.price)*$customer_kedvezmeny ";
        }
        $vevo .= $this->sqlTax("vevo", $relacio, $artol_arrig);


        if ($customer_kedvezmeny > 0 ) {
            $vevo_altalanos .= " OR (abs(p.price)*$customer_kedvezmeny < special.price OR special.price IS NULL) AND (abs(p.price)*$customer_kedvezmeny < vevo.price OR vevo.price IS NULL) ";
            $vevo_altalanos .= $this->sqlTax("p", $relacio, $artol_arrig,$customer_kedvezmeny);
        }

        $vissza .= $special.$vevo.$vevo_altalanos. ")";


        return $vissza;

    }

    public function getElhelyezkedesProducts($data=array()) {

        $fizetes_elbiralas = $this->config->get("fizetes_elbiralas");

        if ($fizetes_elbiralas) {
            foreach ($fizetes_elbiralas as $value) {
                if ($value['teljesitett_sor'] == 1) {
                    $fizetes_elbiralas_teljesitett = $value['fizetes_elbiralas_status_id'];
                }
            }
        }
        $penzugyi_status = $this->config->get("penzugyi_status");
        if ($penzugyi_status) {
            foreach ($penzugyi_status as $value) {
                if ($value['teljesitett_sor'] == 1) {
                    $penzugyi_status_teljesitett = $value['penzugyi_status_id'];
                }
            }
        }

        $sql = "SELECT p.product_id, pe.priority, pd.name, pe.elhelyezkedes_sort_order, pe.elhelyezkedes_alcsoport_sort_order, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1'
                GROUP BY r1.product_id) AS rating
                FROM " . DB_PREFIX . "product p
                LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
                LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)

                RIGHT JOIN " . DB_PREFIX . "product_elhelyezes pe ON
                    (   p.product_id = pe.product_id
                    AND pe.datum_tol <=curdate()
                    AND pe.datum_ig >=curdate()
                    AND pe.elhelyezkedes_sort_order != '999'";

        if ( isset($fizetes_elbiralas_teljesitett) ) {
            $sql .= " AND fizetes_elbiralas_status_id = '" .$fizetes_elbiralas_teljesitett. "'";
        }

        if ( isset($penzugyi_status_teljesitett) ) {
            $sql .= " AND penzugyi_status_id = '" .$penzugyi_status_teljesitett. "'";
        }

        $sql .= ")";

        if (!empty($data['filter_tag'])) {
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_tag pt ON (p.product_id = pt.product_id)";
        }

        if (!empty($data['filter_category'])) {
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";
        }

        if (!empty($data['filter_varos']) || !empty($data['filter_uzlet'])) {
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_filter f2v ON (p.product_id = f2v.product_id)";
        }


        $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'
                AND p.status = '1'
                AND p.date_available <= curdate()
                AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

        $szuresek = $this->config->get('megjelenit_altalanos');

        if ($szuresek['mennyisegre_szur'] == 1) {
            $sql .= " AND p.quantity > 0 ";
        }
        if ($szuresek['ervenesseg_datumra_szur'] == 1) {
            $sql .= " AND ( p.date_ervenyes_ig = '' OR p.date_ervenyes_ig = '0000-00-00' OR p.date_ervenyes_ig >= curdate() ) ";
        }

        if (!empty($data['filter_ar_tol_ig']) ) {
            $beallitasok = $this->config->get('multifilter_beallitasok');

            $artol_arrig = explode(',',$data['filter_ar_tol_ig']);
            if (isset($artol_arrig[0]) && $artol_arrig[0] > $beallitasok['ar_szures_tol_ig']['tol'] ) {
                $sql .= " AND abs(p.price) >= '" . $data['filter_ar_tol_ig'][0]."'";
            }
            if (isset($artol_arrig[1]) && $artol_arrig[1] < $beallitasok['ar_szures_tol_ig']['ig'] ) {
                $sql .= " AND abs(p.price) <= '" . $data['filter_ar_tol_ig'][1]."'";
            }
        }

        if (!empty($data['filter_artol']) ) {
            $sql .= " AND (( (p.szazalek = 0 OR p.szazalek IS NULL) AND abs(p.price) >= '" . $data['filter_artol'] . "') ";
            if (empty($data['filter_arszazalek']) ) {
                $sql .= "OR (p.szazalek = 1 AND p.eredeti_ar*abs(p.price)/100 >= '" .$data['filter_artol']. "') )";
            } else {
                $sql .= "OR (p.szazalek = 1 AND abs(p.price) >= '" . $data['filter_arszazalek'] . "') )";
            }

        } elseif (!empty($data['filter_arszazalek']) ) {
            $sql .= " AND ( (p.szazalek = 1 AND abs(p.price) >= '" . $data['filter_arszazalek'] . "') OR
                (p.szazalek = 0 AND abs(p.eredeti_ar) > abs(p.price) AND abs(p.price)/abs(p.eredeti_ar)*100 >= '" .$data['filter_arszazalek']. "') )";
        }


        if (!empty($data['filter_arig']) ) {
            $sql .= " AND abs(p.price) <= '" . $data['filter_arig'] . "'";
        }

        if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
            $sql .= " AND (";

            if (!empty($data['filter_name'])) {
                $implode = array();

                $words = explode(' ', $data['filter_name']);

                foreach ($words as $word) {
                    if (!empty($data['filter_description'])) {
                        $implode[] = "LCASE(pd.name) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%' OR LCASE(pd.description) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%'";
                    } else {
                        $implode[] = "LCASE(pd.name) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%'";
                    }
                }

                if ($implode) {
                    $sql .= " " . implode(" OR ", $implode) . "";
                }
            }

            if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
                $sql .= " OR ";
            }

            if (!empty($data['filter_tag'])) {
                $implode = array();

                $words = explode(' ', $data['filter_tag']);

                foreach ($words as $word) {
                    $implode[] = "LCASE(pt.tag) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%' AND pt.language_id = '" . (int)$this->config->get('config_language_id') . "'";
                }

                if ($implode) {
                    $sql .= " " . implode(" OR ", $implode) . "";
                }
            }

            $sql .= ")";
        }


        if (!empty($data['filter_category'])) {
            $sql .= " AND (";
            $category_szurok = explode(',',$data['filter_category']);
            $eloszor = true;
            foreach ($category_szurok as $value){
                if ($eloszor){
                    $sql .= "  p2c.category_id = '" . (int)$value . "'";
                    $eloszor = false;
                } else {
                    $sql .= "  OR p2c.category_id = '" . (int)$value . "'";
                }
            }
            $sql .= ") ";
        }

        if (!empty($data['filter_varos'])) {
            $sql .= " AND (";
            $category_szurok = explode(',',$data['filter_varos']);
            $eloszor = true;
            foreach ($category_szurok as $value){
                if ($eloszor){
                    $sql .= "  f2v.filter_group_id = '" . (int)$value . "'";
                    $eloszor = false;
                } else {
                    $sql .= "  OR f2v.filter_group_id = '" . (int)$value . "'";
                }
            }
            $sql .= ") ";
        }


        if (!empty($data['filter_uzlet'])) {
            $sql .= " AND (";
            $category_szurok = explode(',',$data['filter_uzlet']);
            $eloszor = true;
            foreach ($category_szurok as $value){
                if ($eloszor){
                    $sql .= "  f2v.filter_select_id = '" . (int)$value . "'";
                    $eloszor = false;
                } else {
                    $sql .= "  OR f2v.filter_select_id = '" . (int)$value . "'";
                }
            }
            $sql .= ") ";
        }

        if (!empty($data['filter_manufacturer_id'])) {
            $sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer_id'] . "'";
        }

        $elhelyezkedes_sorrend = " pe.elhelyezkedes_sort_order, pe.elhelyezkedes_alcsoport_sort_order  ";

        $sql .= " ORDER BY " . $elhelyezkedes_sorrend;

        $query=$this->db->query($sql);

        $vissza = false;

        if ($query->num_rows > 0) {
            $query = $this->config->rendezes( $query->rows, "product_id", "ASC", "priority","ASC");
            $product_id = $query[0]['product_id'];

            $product = array();
            $teljes = array();

            $teljes[]   = $query[0];

            foreach($query as $value) {
                if ($value['product_id'] != $product_id) {
                    $product_id = $value['product_id'];
                    $teljes[]   = $value;
                }
            }

            $teljes = $this->config->rendezes( $teljes, "elhelyezkedes_sort_order", "ASC", "elhelyezkedes_alcsoport_sort_order","ASC");
            foreach($teljes as $value){
                $product[] = $value['product_id'];
            }

            $vissza = array(
                'product' => $product,
                'teljes' => $teljes
            );
        }


        return $vissza;
    }







    public function getKiemeltTemplate($product_id) {

        $vissza = false;

        $fizetes_elbiralas_teljesitett  = $this->getFizetesTeljesitettId($product_id);
        $penzugyi_status_teljesitett    = $this->getPenzugyTeljesitettId($product_id);

        if ($fizetes_elbiralas_teljesitett && $penzugyi_status_teljesitett) {
            $sql = "SELECT * FROM " . DB_PREFIX . "product_elhelyezes WHERE
                    product_id                   = '" .$product_id. "'
                AND datum_tol                    <=curdate()
                AND datum_ig                     >=curdate()
                AND fizetes_elbiralas_status_id  = '" .$fizetes_elbiralas_teljesitett. "'
                AND penzugyi_status_id           = '" .$penzugyi_status_teljesitett. "'";

            $query = $this->db->query($sql);

            if ($query->num_rows > 0) {
                if ($query->num_rows > 1) {
                    $query = $this->config->rendezes( $query->rows, "priority","ASC");
                    $query = $query[0];
                } else {
                    $query = $query->row;
                }
                $vissza = array(
                    "kiemeles_tpl"          => $this->getTemplateName($query['kiemelesek_id']),
                    "sort_order"            => $query['elhelyezkedes_sort_order'],
                    "alcsoport_sort_order"  => $query['elhelyezkedes_alcsoport_sort_order']
                );
            }
        }
        return $vissza;
    }

    public function getFizetesTeljesitettId($product_id) {

        $vissza = false;

        $fizetes_elbiralas = $this->config->get("fizetes_elbiralas");
        if ($fizetes_elbiralas) {
            foreach ($fizetes_elbiralas as $value) {
                if ($value['teljesitett_sor'] == 1) {
                    $vissza = $value['fizetes_elbiralas_status_id'];
                    break;
                }
            }
        }

        return $vissza;
    }

    public function getPenzugyTeljesitettId($product_id) {

        $vissza = false;

        $penzugyi_status = $this->config->get("penzugyi_status");
        if ($penzugyi_status) {
            foreach ($penzugyi_status as $value) {
                if ($value['teljesitett_sor'] == 1) {
                    $vissza = $value['penzugyi_status_id'];
                }
            }
        }

        return $vissza;

    }

    public function getTemplateName($kiemelesek_id) {
        $vissza = false;

        $elhelyezkedesek = $this->config->get("kiemelesek");
        if ($elhelyezkedesek) {
            foreach ($elhelyezkedesek as $value) {
                if ($value['kiemelesek_id'] == $kiemelesek_id) {
                    $template = $value['class'];
                    if ( file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/kiemelesek/'.$template.'.tpl') ) {
                        $vissza = DIR_TEMPLATE . $this->config->get('config_template') . '/template/kiemelesek/'.$template.'.tpl';
                    } elseif (file_exists(DIR_TEMPLATE.'default/template/kiemelesek/'.$template.'.tpl') )  {
                        $vissza = DIR_TEMPLATE.'default/template/kiemelesek/'.$template.'.tpl';
                    }
                    break;
                }
            }
        }

        return $vissza;

    }

    public function setCategoryView($category_id) {
        $sql = "SELECT * FROM " . DB_PREFIX . "category_view WHERE
                    category_id                  = '" .$category_id. "'
                    AND datum                    = curdate()";

        $query = $this->db->query($sql);

        if ($query->num_rows == 0) {
            $sql = "INSERT INTO  " . DB_PREFIX . "category_view SET
                datum           = curdate(),
                category_id     = '".$category_id."',
                `counter`       = 1";
            $this->db->query($sql);
        } else {
            $this->db->query("UPDATE " . DB_PREFIX . "category_view SET `counter`       = (counter+1)
             WHERE  datum       = curdate() AND
                    category_id = '" .$category_id. "'");
        }
    }

    public function getFilterGroup($filter_group_id) {
        $sql = "SELECT * FROM " . DB_PREFIX . "filter_group_description fgd
                        LEFT JOIN " . DB_PREFIX . "filter_group fg ON (fgd.filter_group_id = fg.filter_group_id)
                        WHERE   fgd.language_id = '" . $this->config->get('config_language_id') . "'
                         AND fgd.filter_group_id = '" . (int)$filter_group_id . "'";

        $query = $this->db->query($sql);
        if ($query->num_rows == 0) {
            $vissza = false;
        } else {
            $vissza = $query->row;
        }

        return $vissza;
    }

    public function getFilterTulajdonsag($attribute_id) {

        $sql = "SELECT a.attribute_id, ad.name FROM " . DB_PREFIX . "attribute a
            LEFT JOIN " . DB_PREFIX . "attribute_description	 ad ON (ad.attribute_id = a.attribute_id)
                WHERE ad.language_id = '" . $this->config->get('config_language_id') . "'
                       AND a.attribute_id = '" . (int)$attribute_id . "' ";


        $query = $this->db->query($sql);
        if ($query->num_rows == 0) {
            $vissza = false;
        } else {
            $vissza = $query->row;
        }

        return $vissza;
    }

    public function getFilterValasztek($option_value_id) {

        $sql = "SELECT o.option_value_id, ovd.name FROM " . DB_PREFIX . "option_value o
            LEFT JOIN " . DB_PREFIX . "option_value_description	 ovd ON (o.option_value_id = ovd.option_value_id)
                WHERE ovd.language_id = '" . $this->config->get('config_language_id') . "'
                       AND o.option_value_id = '" . (int)$option_value_id . "' ";

        $query = $this->db->query($sql);
        if ($query->num_rows == 0) {
            $vissza = false;
        } else {
            $vissza = $query->row;
        }

        return $vissza;
    }

    public function getFilterGyarto($manufacturer_id) {

        $sql = "SELECT name FROM " . DB_PREFIX . "manufacturer
                       WHERE manufacturer_id = '" . (int)$manufacturer_id . "' ";

        $query = $this->db->query($sql);
        if ($query->num_rows == 0) {
            $vissza = false;
        } else {
            $vissza = $query->row;
        }

        return $vissza;
    }

    public function getFilterMeret($option_id) {


        $sql = "SELECT od.name FROM " . DB_PREFIX . "option o
                    LEFT JOIN " . DB_PREFIX . "option_description	 od ON (o.option_id = od.option_id)

                    WHERE od.language_id = '" . $this->config->get('config_language_id') . "'
                       AND o.option_id = '" . (int)$option_id . "' ";

        $query = $this->db->query($sql);
        if ($query->num_rows == 0) {
            $vissza = false;
        } else {
            $vissza = $query->row;
        }

        return $vissza;
    }

    public function getFilterSzin($option_szin_id) {

        $vissza = false;

        $sql = "SELECT oszd.name FROM " . DB_PREFIX . "option_szin osz
                    LEFT JOIN " . DB_PREFIX . "option_szin_description	        oszd  ON (oszd.option_szin_id = osz.option_szin_id)
                    WHERE oszd.language_id = '" . $this->config->get('config_language_id') . "'
                        AND osz.option_szin_id = '" . (int)$option_szin_id . "' ";

        $query = $this->db->query($sql);


        $beallitasok = $this->config->get('multifilter_beallitasok');
        if ( isset($beallitasok['szin']['szinek']) && ($beallitasok['szin']['szinek'] == 2 || $beallitasok['szin']['szinek'] == 3) ) {
            if ($query->num_rows > 0) {
                foreach($query->rows as $key=>$option_szin) {
                    $sql = "SELECT oszgd.name FROM ".DB_PREFIX."option_szin_to_group osz2g
                        LEFT JOIN " .DB_PREFIX. "option_szin_group oszg ON (oszg.option_szin_group_id = osz2g.option_szin_group_id)
                        LEFT JOIN " .DB_PREFIX. "option_szin_group_description oszgd ON (oszgd.option_szin_group_id = osz2g.option_szin_group_id)
                    WHERE osz2g.option_szin_id = '" .$option_szin_id. "'
                        AND oszgd.language_id='".$this->config->get('config_language_id')."'";

                    $eredmeny = $this->db->query($sql);
                    $vissza = $eredmeny->rows;

                }
            }


        } else {

            if ($query->num_rows > 0) {
                $vissza = $query->row;
            }
        }


        return $vissza;
    }


    public function getFilterSelect($filter_select_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "filter_select WHERE
            language_id = '" . $this->config->get('config_language_id') . "'
            AND filter_select_id = '" .(int)$filter_select_id. "'");

        return $query->row;
    }

    public function getPath($path="") {
        $vissza = "";

        if (!empty($path)) {
            $kategoriak = explode("_",$path);

            $vissza .= '&path=';
            foreach($kategoriak as $key=>$kategoria){
                if ($key != 0) {
                    $vissza .= '_';
                }
                $vissza .= $kategoria;
            }
        }

        return $vissza;
    }

    public function setPath($product_id="") {

        $vissza = "";
        $sql = "SELECT * FROM " . DB_PREFIX . "product_to_category p2c WHERE product_id='" .$product_id. "'";

        $query = $this->db->query($sql);

        $vissza .= '&path=';
        foreach($query->rows as $category) {
            $vissza .= $category['category_id'].'_';

        }
        return substr($vissza,0,-1);
    }


    public function getBreadcrumbs($szuro="") {

        $vissza = false;
        $beallitasok = $this->config->get('multifilter_beallitasok');

        if (isset($this->request->request[$szuro]) && $this->request->request[$szuro] != null && !empty($this->request->request[$szuro]) ) {

            $uzletek = explode(",",$this->request->request[$szuro]);
            $vissza = array();

            foreach($uzletek as $uzlet){
                if ($szuro == 'filter_uzlet') {
                    $uzlet_info = $this->getFilterSelect($uzlet);

                } elseif ($szuro == 'filter_varos') {
                    $uzlet_info = $this->getFilterGroup($uzlet);

                } elseif ($szuro == 'filter_gyarto') {
                    $uzlet_info = $this->getFilterGyarto($uzlet);

                } elseif ($szuro == 'filter_meret') {
                    $uzlet_info = $this->getFilterMeret($uzlet);

                } elseif ($szuro == 'filter_szin' && isset($beallitasok['szin']['szinek']) && ($beallitasok['szin']['szinek'] == 0) ) {
                    $uzlet_info = $this->getFilterSzin($uzlet);

                /*} elseif ($szuro == 'filter_szin' && isset($beallitasok['szin']['szinek']) && ($beallitasok['szin']['szinek'] > 0) ) {
                    $uzlet_info = $this->getFilterSzin($uzlet);*/


                } elseif ($szuro == 'filter_tulajdonsagok') {
                    $uzlet_info = $this->getFilterTulajdonsag($uzlet);

                } elseif ($szuro == 'filter_valasztek') {
                    $uzlet_info = $this->getFilterValasztek($uzlet);

                }

                $url1 = '';
                $url1 .= $this->getPath(  isset($this->request->request['path']) ? $this->request->request['path'] : array() );
                $url1 .= $this->getUrl($szuro);
                $url1 .= '&'.$szuro.'=' . $uzlet;

                if (isset($uzlet_info) && $uzlet_info) {

                    $vissza[] = array(
                        'text'      => $uzlet_info['name'],
                        'href'      => $this->url->link('product/multifilter', $url1),
                        'separator' => $this->language->get('text_separator')
                    );
                }
            }
        }
        return $vissza;
    }






    public function getUrl($jelenlegi="") {

        $vissza = "";

        if ($jelenlegi != "filter" && isset($this->request->request['filter'])) {
            $vissza .= '&filter=';
        }

        if ($jelenlegi != "filter_varos" && isset($this->request->request['filter_varos'])) {
            $vissza .= '&filter_varos=';
        }

        if ($jelenlegi != "filter_uzlet" && isset($this->request->request['filter_uzlet'])) {
            $vissza .= '&filter_uzlet=';
        }

        if ($jelenlegi != "filter_tulajdonsagok" && isset($this->request->request['filter_tulajdonsagok'])) {
            $vissza .= '&filter_tulajdonsagok=';
        }

        if ($jelenlegi != "filter_valasztek" && isset($this->request->request['filter_valasztek'])) {
            $vissza .= '&filter_valasztek=';
        }
        if ($jelenlegi != "filter_gyarto" && isset($this->request->request['filter_gyarto'])) {
            $vissza .= '&filter_gyarto=';
        }
        if ($jelenlegi != "filter_raktaron" && isset($this->request->request['filter_raktaron'])) {
            $vissza .= '&filter_raktaron=';
        }

        if ($jelenlegi != "filter_artol" && isset($this->request->request['filter_artol'])) {
            $vissza .= '&filter_artol=';
        }

        if ($jelenlegi != "filter_ar_tol_ig" && isset($this->request->request['filter_ar_tol_ig'])) {
            $vissza .= '&filter_ar_tol_ig=';
        }

        if ($jelenlegi != "filter_arig" && isset($this->request->request['filter_arig'])) {
            $vissza .= '&filter_arig=';
        }

        if ($jelenlegi != "filter_arszazalek" && isset($this->request->request['filter_arszazalek'])) {
            $vissza .= '&filter_arszazalek=';
        }

        return $vissza;

    }

    public function tulajdonsagSzukitBovitLeosztas($filter_tulajdonsagok) {
        $beallitasok = $this->config->get('multifilter_beallitasok');

        $sql = "SELECT * FROM " . DB_PREFIX . "attribute a
                            LEFT JOIN " . DB_PREFIX . "attribute_group ag  ON (a.attribute_group_id =ag.attribute_group_id)
                        WHERE attribute_id IN (".$filter_tulajdonsagok.")";
        $query = $this->db->query($sql);
        $tulajdonsagok_szukit   = array();
        $tulajdonsagok_bovit    = array();
        $vezerel_kategoriat     = array();
        foreach($query->rows as $value) {
            $tulajdonsag_szukit = $value['multifilter_szukit'] == 1 ? true : ($value['multifilter_szukit'] == 2 ? false : ($beallitasok['tulajdonsagok']['szukit'] == 1 ? true : false));
            if ($tulajdonsag_szukit) {
                $tulajdonsagok_szukit[] = $value['attribute_id'];
            } else {
                $tulajdonsagok_bovit[]  = $value['attribute_id'];
            }
            if ($value['vezerel_kategoriat'] == 1) {
                $vezerel_kategoriat[] = $value['attribute_id'];
            }

        }
        return array(
            'tulajdonsagok_szukit' =>  implode(",",$tulajdonsagok_szukit),
            'tulajdonsagok_bovit'  =>  implode(",",$tulajdonsagok_bovit),
            'vezerel_kategoriat'    =>  implode(",",$vezerel_kategoriat)
        );
    }

    public function getSzinGroups($szinek = array()) {


        $sql = "SELECT * FROM " .DB_PREFIX."option_szin_group oszg
                    LEFT JOIN " .DB_PREFIX. "option_szin_group_description oszgd ON (oszgd.option_szin_group_id = oszg.option_szin_group_id)";

        $o_sz_g_id = array();
        if ($szinek) {
            foreach ($szinek as $szin) {
                foreach ($szin['groups'] as $group) {
                    $o_sz_g_id[] = $group['option_szin_group_id'];
                }
            }
        }

        $sql .= " WHERE oszgd.language_id = '".$this->config->get('config_language_id')."'";
        if ($o_sz_g_id) {
            $sql .= " AND oszg.option_szin_group_id IN (" . implode(',', $o_sz_g_id) . ")";
        }
        $sql .= " ORDER BY sort_order ";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function tulajdonsagCsoportok($tulajdonsag) {
        $sql = "SELECT attribute_group_id, attribute_id FROM " . DB_PREFIX . "attribute
                WHERE  attribute_id IN (".$tulajdonsag.")";
        $query = $this->db->query($sql);
        $vissza = array();
        foreach($query->rows as $value) {
            $vissza[$value['attribute_group_id']][] = $value['attribute_id'];
        }

        return $vissza;

    }

}



?>