<?php

class ModelCatalogProductImagePlaces extends Model {
    CONST IMAGEKEY="imagebylocations_id";
    CONST PLACESKEY="imageinlocations_id";
    private $imagestable;
    private $placestable;

    public function __construct($registry) {
        $this->imagestable = DB_PREFIX."product_imagebylocations";
        $this->placestable = DB_PREFIX."product_imageinlocations";
        parent::__construct($registry);
    }

    public function getImagesByProductID($productID) {
        $query = $this->db->query("SELECT * FROM ".$this->imagestable." WHERE product_id='".(int)$productID."' ORDER BY sort_order");

        return $query->rows;
    }

    public function getPlacesByProductID($productID) {
        $query = $this->db->query("SELECT ".$this->placestable.".* FROM ".$this->placestable." INNER JOIN ".$this->imagestable." ON ".$this->placestable.".".self::IMAGEKEY."=".$this->imagestable.".".self::IMAGEKEY." WHERE ".$this->imagestable.".product_id='".(int)$productID."' ORDER BY sort_order");

        return $query->rows;
    }

    public function getPlacesByImageID($imageByLocationsID) {
        $query = $this->db->query("SELECT ".$this->placestable.".* FROM ".$this->placestable." WHERE ".self::IMAGEKEY."='".(int)$imageByLocationsID."'");

        return $query->rows;
    }

    public function setDatas($productID, $datas) {
        if(!is_array($datas)) {
            return true;
        }
        if(array_key_exists('NULL', $datas)) {
            unset($datas['NULL']);
        }
        $currentImages = $this->getImagesByProductID($productID);

        if(count($datas) == 0 && count($currentImages) == 0) {
            return true;
        }

        /**
         * Meglévő feleslegesek törlése
         * Módosítás, ha szükséges
         */
        foreach($currentImages as $imageKey=>$currentImage) {
            $found = false;
            foreach($datas as $dataKey=>$data) {
                if($currentImage['image'] == $data['image']) {
                    $found = true;
                    break;
                }
            }
            if(!$found) {
                $this->deleteImageByID($currentImage[self::IMAGEKEY]);
                $this->deletePlaceByImageID($currentImage[self::IMAGEKEY]);
                unset($currentImages[$imageKey]);
            } else {
                $data = $datas[$dataKey];
                $modified = false;
                $updateData = array();
                $updateData[self::IMAGEKEY] = $currentImage[self::IMAGEKEY];

                if($currentImage['image'] != $data['image']) {
                    $modified = true;
                }
                $updateData['image'] = $data['image'];

                if($currentImage['generated_text'] != $data['generated']) {
                    $modified = true;
                }
                $updateData['generated_text'] = $data['generated'];

                if($currentImage['location_name'] != $data['name']) {
                    $modified = true;
                }
                $updateData['location_name'] = $data['name'];

                if($currentImage['sort_order'] != $data['sort_order']) {
                    $modified = true;
                }
                $updateData['sort_order'] = $data['sort_order'];
                if($modified) {
                    $this->updateImage($updateData);
                }
                $this->checkPlaces($currentImage[self::IMAGEKEY], $data['locations']);

                unset($datas[$dataKey]);
            }
        }

        foreach($datas as $data) {
            $insert = array();
            $insert['image']                = isset($data['image']) ? $data['image'] : "";
            $insert['generated_text']       = isset($data['generated']) ? "1" : "0";
            $insert['location_name']        = isset($data['name']) ? $data['name'] : "";
            $insert['sort_order']           = isset($data['sort_order']) ? $data['sort_order'] : 0;
            $imageByLocationsID = $this->addImage($productID, $insert);
            if(isset($data['locations'])) {
                foreach($data['locations'] as $place) {
                    $this->addPlace($imageByLocationsID, $place);
                }
            }
        }

        return true;
    }

    public function deleteImageByID($imageByLocationsID) {
        $sql = "DELETE FROM ".$this->imagestable." WHERE ".self::IMAGEKEY."='".$imageByLocationsID."'";
        $this->db->query($sql);
    }

    public function deletePlaceByID($imageInLocationsID) {
        $sql = "DELETE FROM ".$this->placestable." WHERE ".self::PLACESKEY."='".$imageInLocationsID."'";
        $this->db->query($sql);
    }

    public function deletePlaceByImageID($imageByLocationsID) {
        $sql = "DELETE FROM ".$this->placestable." WHERE ".self::IMAGEKEY."='".$imageByLocationsID."'";
        $this->db->query($sql);
    }

    public function updateImage($updateData) {
        $sql = "UPDATE ".$this->imagestable." SET image='".$updateData['image']."', sort_order='".$updateData['sort_order']."', generated_text='".$updateData['generated_text']."', location_name='".$updateData['location_name']."'
        WHERE ".self::IMAGEKEY."='".$updateData[self::IMAGEKEY]."'";
        $this->db->query($sql);
    }

    public function addPlace($imageByLocationsID, $placeID) {
        $sql = "INSERT INTO ".$this->placestable."(imagebylocations_id, filter_select_id) VALUES('".$imageByLocationsID."', '".$placeID."')";
        $this->db->query($sql);
    }

    public function addImage($productID, $datas) {
        $image = isset($datas['image']) ? $datas['image'] : '';
        $sort_order = isset($datas['sort_order']) ? $datas['sort_order'] : '';
        $generated_text = isset($datas['generated_text']) ? $datas['generated_text'] : '0';
        $location_name = isset($datas['location_name']) ? $datas['location_name'] : '';
        $sql = "INSERT INTO ".$this->imagestable." SET product_id='".$productID."', image='".$image."', sort_order='".$sort_order."', generated_text='".$generated_text."', location_name='".$location_name."'";
        $this->db->query($sql);
        return $this->db->getLastId();

    }

    public function checkPlaces($imageByLocationsID, $places) {
        $originalPlaces = $this->getPlacesByImageID($imageByLocationsID);

        /**
         * Feleslegesek törlése
         */
        foreach($originalPlaces as $oPlaceKey=>$originalPlace) {
            $found = false;
            foreach($places as $placeKey=>$place) {
                if($place == $originalPlace['filter_select_id']) {
                    $found = true;
                    break;
                }
            }
            if(!$found) {
                $this->deletePlaceByID($originalPlace['imageinlocations_id']);
                unset($originalPlaces[$oPlaceKey]);
            } else {
                unset($places[$placeKey]);
            }
        }

        /**
         * Újak felvétele
         */
        foreach($places as $place) {
            $this->addPlace($imageByLocationsID, $place);
        }
    }
}

?>