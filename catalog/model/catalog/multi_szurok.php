<?
class ModelCatalogMultiSzurok extends Model {

   /* private $kozos;
    private $customer_group_id;
    private $customer_kedvezmeny = 1;*/

    public function veglegesRaktar($kozos="") {

        $szuresek = $this->config->get('megjelenit_altalanos');

        if ($szuresek['mennyisegre_szur'] != 1) {
            $sql = "SELECT product_id FROM " . DB_PREFIX . "product WHERE status = '1' ";

            if ($szuresek['ervenesseg_datumra_szur'] == 1) {
                $sql .= " AND ( date_ervenyes_ig = '' OR date_ervenyes_ig = '0000-00-00' OR date_ervenyes_ig >= curdate() ) ";
            }


            if (!empty($kozos)) {
                $termekek = implode(",",$kozos);
                $sql .= " AND product_id IN (" .$termekek. ")";
            }

            $raktaron = " AND quantity > 0 LIMIT 0,1";
            $nincs_raktaron = " AND quantity <= 0 LIMIT 0,1";

            $query_raktaron = $this->db->query($sql.$raktaron);
            $query_nincs_raktaron = $this->db->query($sql.$nincs_raktaron);

            $vissza = array(
                'raktaron'          => false,
                'nincs_raktaron'    => false
            );

            if ($query_raktaron->num_rows > 0) {
                $vissza['raktaron'] = true;
            }
            if ($query_nincs_raktaron->num_rows > 0) {
                $vissza['nincs_raktaron'] = true;
            }

            return $vissza;
        } else {
            return array();
        }
    }



    public function veglegesGyartok($kozos="") {

        $beallitasok = $this->config->get('multifilter_beallitasok');
        $teljes_lista = $beallitasok['gyarto']['teljes'];


        $sql = "SELECT mf.manufacturer_id, mf.name, mf.sort_order FROM " . DB_PREFIX . "manufacturer mf
            INNER JOIN " . DB_PREFIX . "product p ON (p.manufacturer_id = mf.manufacturer_id)

                WHERE p.status = '1' ";

        if (!empty($kozos) || $teljes_lista !=1) {
            $sql .= $this->szuresRaktar();
        }

        if (!empty($kozos)) {
            $termekek = implode(",",$kozos);
            $sql .= " AND p.product_id IN (" .$termekek. ")";
        }

        $sql .= " GROUP BY mf.manufacturer_id ORDER BY mf.sort_order";

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function veglegesMeret($kozos=array()) {

        $beallitasok = $this->config->get('multifilter_beallitasok');
        if (isset($beallitasok['meret'])) {

            $filter_szin = '';
            if (!empty($this->request->request['filter_szin'])) {
                $filter_szin = explode(',', $this->request->request['filter_szin']);
                sort($filter_szin);
                $filter_szin = implode(',', $filter_szin);
            }
            $cache = $filter_szin;
            $cache .= http_build_query($kozos);

            $cache = md5($cache);

            $vissza = $this->cache->get('meret.marad.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id').'.'. $cache);

            if (!$vissza) {


                $teljes_lista = $beallitasok['meret']['teljes'];


                $pr_op_id = array();
                if (!empty($filter_szin)) {
                    $sql = "SELECT pov.product_option_id FROM " . DB_PREFIX . "product_option_value pov
                    WHERE pov.option_szin_id IN (" . $filter_szin . ")";
                    $pr_op_id = $this->db->query($sql);
                    $pr_op_ids = array();
                    foreach ($pr_op_id->rows as $value) {
                        $pr_op_ids[] = $value['product_option_id'];
                    }

                }

                $sql = "SELECT o.option_id, od.name, o.sort_order FROM " . DB_PREFIX . "option o
                            LEFT JOIN " . DB_PREFIX . "option_description od ON (od.option_id = o.option_id)
                            LEFT JOIN " . DB_PREFIX . "product_option po ON (po.option_id = o.option_id)
                            LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = po.product_id)
                        WHERE od.language_id    = '" . $this->config->get('config_language_id') . "'
                                    AND o.type  ='checkbox_qty' ";

                if ($pr_op_id) {
                    $pr_op_ids = implode(',', $pr_op_ids);
                    $sql .= " AND po.product_option_id IN (" . $pr_op_ids . ")";
                }

                if (!empty($kozos) || $teljes_lista != 1) {
                    $sql .= $this->szuresRaktar();
                }

                if (!empty($kozos)) {
                    $termekek = implode(",", $kozos);
                    $sql .= " AND p.product_id IN (" . $termekek . ")";
                }

                $sql .= " GROUP BY o.option_id ORDER BY o.sort_order";

                $query = $this->db->query($sql);

                $this->cache->set('meret.marad.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $cache, $query->rows);

                return $query->rows;
            } else {
                return $vissza;
            }

        } else {
            return "";
        }
    }



    public function veglegesSzin($kozos=array()) {

        $filter_meret = '';
        if (!empty($this->request->request['filter_meret'])) {
            $filter_meret = explode(',', $this->request->request['filter_meret']);
            sort($filter_meret);
            $filter_meret = implode(',', $filter_meret);
        }
        $cache = $filter_meret;
        $cache .= http_build_query($kozos);

        $cache = md5($cache);

        $vissza = $this->cache->get('szin.marad' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id').'.'. $cache);

        if (!$vissza) {

            $beallitasok = $this->config->get('multifilter_beallitasok');


            $sql = "SELECT osz.option_szin_id, oszd.name, osz.sort_order, osz.szinkod FROM " . DB_PREFIX . "option_szin osz
            LEFT JOIN " . DB_PREFIX . "option_szin_description oszd ON (oszd.option_szin_id = osz.option_szin_id)
            LEFT JOIN " . DB_PREFIX . "product_option_value pov ON (pov.option_szin_id = osz.option_szin_id)
            LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = pov.product_id)
            LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)

                WHERE oszd.language_id = '" . $this->config->get('config_language_id') . "'
                AND p2s.store_id = '" . $this->config->get('config_store_id') . "'";

            if (!empty($kozos)) {
                $sql .= $this->szuresRaktar();
            }

            if (!empty($kozos)) {
                $termekek = implode(",", $kozos);
                $sql .= " AND p.product_id IN (" . $termekek . ")";
            }

            if ($filter_meret) {
                $sql .= " AND pov.option_id IN (" . $filter_meret . ")";

            }

            $sql .= " GROUP BY osz.option_szin_id ORDER BY osz.sort_order";

            $query = $this->db->query($sql);
            if ($query->num_rows > 0) {
                foreach ($query->rows as $key => $option_szin) {
                    $sql = "SELECT * FROM " . DB_PREFIX . "option_szin_to_group osz2g
                        LEFT JOIN " . DB_PREFIX . "option_szin_group oszg ON (oszg.option_szin_group_id = osz2g.option_szin_group_id)
                        LEFT JOIN " . DB_PREFIX . "option_szin_group_description oszgd ON (oszgd.option_szin_group_id = osz2g.option_szin_group_id)
                    WHERE osz2g.option_szin_id = '" . $option_szin['option_szin_id'] . "'
                        AND oszgd.language_id='" . $this->config->get('config_language_id') . "'";

                    $eredmeny = $this->db->query($sql);
                    $query->rows[$key]['groups'] = $eredmeny->rows;

                }
            }
            $this->cache->set('szin.marad.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $cache, $query->rows);

            return $query->rows;
        } else {
            return $vissza;
        }



        /*   Nincs $filter_meret
        SELECT osz.option_szin_id, oszd.name, osz.sort_order, osz.szinkod FROM hungaro_option_szin osz
            LEFT JOIN hungaro_option_szin_description oszd ON (oszd.option_szin_id = osz.option_szin_id)
            LEFT JOIN hungaro_product_option_value pov ON (pov.option_szin_id = osz.option_szin_id)
            LEFT JOIN hungaro_product p ON (p.product_id = pov.product_id)
            LEFT JOIN hungaro_product_to_store p2s ON (p.product_id = p2s.product_id)

                WHERE oszd.language_id = '2'
                AND p2s.store_id = '0'
        AND p.product_id IN (10741,11137,11162,11167,12569,12572) GROUP BY osz.option_szin_id ORDER BY osz.sort_order


        van $filter_meret
        SELECT osz.option_szin_id, oszd.name, osz.sort_order, osz.szinkod FROM hungaro_option_szin osz
            LEFT JOIN hungaro_option_szin_description oszd ON (oszd.option_szin_id = osz.option_szin_id)
            LEFT JOIN hungaro_product_option_value pov ON (pov.option_szin_id = osz.option_szin_id)
            LEFT JOIN hungaro_product p ON (p.product_id = pov.product_id)
            LEFT JOIN hungaro_product_to_store p2s ON (p.product_id = p2s.product_id)

                WHERE oszd.language_id = '2'
                AND p2s.store_id = '0'
        AND p.product_id IN (10732,10736,10737,10738,10740,10741,10774,10905,10936,11018,11025,11089,11091,11110,11111,11112,11129,11131,11139,11149,11162,11164,11166,11178,11182,11184,11186,11188,12513,12518,12599,12600,12738,12740,12742)
        AND pov.option_id IN (6) GROUP BY osz.option_szin_id ORDER BY osz.sort_order
        */
    }

    public function veglegesValasztek($kozos="") {

        $vissza = array();
        $beallitasok = $this->config->get('multifilter_beallitasok');
        $teljes_lista = $beallitasok['valasztek']['teljes'];


        $sql = "SELECT o.option_id, od.name, o.sort_order, p.product_id, o.type FROM " . DB_PREFIX . "option o
            LEFT JOIN " . DB_PREFIX . "option_description	 od ON (o.option_id = od.option_id)
            LEFT JOIN " . DB_PREFIX . "product_option  po ON (o.option_id = po.option_id)
            LEFT JOIN " . DB_PREFIX . "product  p ON (p.product_id = po.product_id)

                WHERE od.language_id = '" . $this->config->get('config_language_id') . "'
                    AND p.status = '1'
                    AND o.type != 'checkbox_qty'";


        if (!empty($kozos) || $teljes_lista !=1) {
            $sql .= $this->szuresRaktar();
        }

        if (!empty($kozos)) {
            $termekek = implode(",",$kozos);
            $sql .= " AND p.product_id IN (" .$termekek. ")";
        }

        $sql .= " GROUP BY o.option_id ORDER BY o.sort_order";

        $query = $this->db->query($sql);

        $vissza = array();
        if ($query->num_rows > 0) {
            foreach ($query->rows as $value) {
                $sql = "SELECT  p.product_id, ovd.name, ov.sort_order, ov.option_value_id FROM " . DB_PREFIX . "option_value ov
                    LEFT JOIN " . DB_PREFIX . "option_value_description	 ovd ON (ovd.option_value_id = ov.option_value_id)
                    LEFT JOIN " . DB_PREFIX . "product_option_value	 pov ON (pov.option_value_id = ov.option_value_id)
                    LEFT JOIN " . DB_PREFIX . "product  p ON (p.product_id = pov.product_id)

                WHERE ovd.language_id = '" . $this->config->get('config_language_id') . "'
                    AND ov.option_id = '" . $value['option_id'] . "'
                    AND p.status = '1'";

                if (!empty($kozos) || $teljes_lista !=1) {
                    $sql .= $this->szuresRaktar();
                }

                if (!empty($kozos)) {
                    $sql .= " AND p.product_id IN (" .$termekek. ")";
                }

                $sql .= " GROUP BY ov.option_value_id ORDER BY ov.sort_order";
                $query_option = $this->db->query($sql);

                if ($query_option->num_rows > 0){
                    $value['options'] = $query_option->rows;
                    $vissza[] = $value;
                }
            }

        }



        return $vissza;

    }

    public function veglegesVarosok($kozos="", $uzletek="") {

        $beallitasok = $this->config->get('multifilter_beallitasok');
        $teljes_lista = $beallitasok['szuro_csoport']['teljes'];

        $sql = "SELECT fg.filter_group_id, fgd.name, fg.sort_order FROM " . DB_PREFIX . "filter_group fg
            LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (fgd.filter_group_id = fg.filter_group_id)
            INNER JOIN  " . DB_PREFIX . "product_filter pf ON (pf.filter_group_id = fg.filter_group_id)
            LEFT JOIN  " . DB_PREFIX . "product p ON (pf.product_id=p.product_id)

            WHERE language_id = '" . $this->config->get('config_language_id') . "'";


        /* if (!empty($uzletek)) {
            $sql .= " AND pf.filter_select_id IN (" .$uzletek. ")";
        }*/

        if (!empty($kozos)) {
            $termekek = implode(",",$kozos);
            $sql .= " AND pf.product_id IN (" .$termekek. ")";
        }

        if (!empty($kozos) || $teljes_lista !=1) {
            $sql .= $this->szuresRaktar();
        }


        $sql .= " GROUP BY fg.filter_group_id ORDER BY fg.sort_order";

        $query = $this->db->query($sql);

        return $this->config->rendezes($query->rows,"sort_order");

    }

    public function veglegesTulajdonsag($kozos=array()) {

        $cache = md5(http_build_query($kozos));

        $vissza = $this->cache->get('attribute.marad.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'). $cache);
        if (!$vissza) {
            $vissza = array();
            $beallitasok = $this->config->get('multifilter_beallitasok');
            $teljes_lista = $beallitasok['tulajdonsagok']['teljes'];

            $sql = "SELECT ag.attribute_group_id, agd.name, ag.sort_order, p.product_id, ag.multifilter_szukit, ag.multifilter_csuszka FROM " . DB_PREFIX . "attribute_group ag
                LEFT JOIN " . DB_PREFIX . "attribute_group_description	 agd ON (agd.attribute_group_id = ag.attribute_group_id)
                LEFT JOIN " . DB_PREFIX . "attribute  a ON (a.attribute_group_id = ag.attribute_group_id)
                LEFT JOIN " . DB_PREFIX . "product_attribute  pa ON (pa.attribute_id = a.attribute_id)
                INNER JOIN " . DB_PREFIX . "product  p ON (p.product_id = pa.product_id)

                    WHERE agd.language_id = '" . $this->config->get('config_language_id') . "'
                           AND p.status = '1' ";

            if (!empty($kozos) || $teljes_lista !=1) {
                $sql .= $this->szuresRaktar();
            }

            $sql .= " GROUP BY ag.attribute_group_id ORDER BY ag.sort_order, a.sort_order";

            $query = $this->db->query($sql);
            if ($query->num_rows > 0) {
                foreach ($query->rows as $value) {
                    $sql = "SELECT  ad.name, a.sort_order, a.attribute_id, p.product_id, a.multifilter_csuszka FROM " . DB_PREFIX . "attribute a
                        LEFT JOIN " . DB_PREFIX . "attribute_description	 ad ON (ad.attribute_id = a.attribute_id)
                        LEFT JOIN " . DB_PREFIX . "product_attribute  pa ON (pa.attribute_id = a.attribute_id)
                        INNER JOIN " . DB_PREFIX . "product  p ON (p.product_id = pa.product_id)
                    WHERE ad.language_id = '" . $this->config->get('config_language_id') . "'
                        AND a.attribute_group_id = '" . $value['attribute_group_id'] . "'";


                    if (!empty($kozos)) {
                        $termekek = implode(",",$kozos);
                        $sql .= " AND p.product_id IN (" .$termekek. ")";
                    }

                    if (!empty($kozos) || $teljes_lista !=1) {
                        $sql .= $this->szuresRaktar();
                    }


                    $sql .= " GROUP BY a.attribute_id ORDER BY a.sort_order";

                    $query_attr = $this->db->query($sql);


                    if ($query_attr->num_rows > 0){
                        $value['attributes'] = $query_attr->rows;
                        $vissza[] = $value;

                        foreach($vissza as $vissza_key=>$attribute_group) {
                            if (is_array($attribute_group['attributes']) ) {
                                foreach($attribute_group['attributes'] as $attribute_key=>$attribute) {
                                    if ($attribute['multifilter_csuszka'] == 1) {
                                        $sql = "SELECT product_id, text FROM ".DB_PREFIX."product_attribute WHERE attribute_id=".$attribute['attribute_id']." AND language_id = '".$this->config->get('config_language_id')."'";
                                        $query = $this->db->query($sql);
                                        if ($query->num_rows > 0) {

                                            $vissza[$vissza_key]['attributes'][$attribute_key]['products'] = $this->config->rendezes($query->rows,"text");
                                            $i = 0;
                                            foreach($vissza[$vissza_key]['attributes'][$attribute_key]['products'] as $ertek) {
                                                if ($i == 0) {
                                                    $csuszka_tol = (float)$ertek['text'];
                                                    $csuszka_ig  = (float)$ertek['text'];
                                                    $i++;
                                                    continue;
                                                }
                                                $csuszka_ig = (float)$ertek['text'];

                                            }
                                            $vissza[$vissza_key]['attributes'][$attribute_key]['csuszka_tol'] = $csuszka_tol;
                                            $vissza[$vissza_key]['attributes'][$attribute_key]['csuszka_ig'] = $csuszka_ig;
                                        }
                                    }
                                }
                            }
                        }

                    }

                }

                $this->cache->set('attribute.marad.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') .$cache, $vissza);

            }
        }

        return $vissza;
    }


    public function veglegesTulajdonsagCsuszka($kozos="") {
            //$sql = "SELECT  pa.attribute_id, CONVERT(replace(pa.text,'.',''),UNSIGNED INTEGER) AS `text` FROM " . DB_PREFIX . "product_attribute pa ";
            $sql = "SELECT  pa.attribute_id, pa.text FROM " . DB_PREFIX . "product_attribute pa ";
            $sql .= " LEFT JOIN  ".DB_PREFIX."attribute a ON (a.attribute_id=pa.attribute_id)";
            $sql .= " LEFT JOIN  ".DB_PREFIX."product p ON (p.product_id=pa.product_id)";
            $sql .= " WHERE a.multifilter_csuszka =1 ";
            $sql .= " AND pa.language_id = '".$this->config->get('config_language_id')."' ";

            if (!empty($kozos) ) {
                $sql .= " AND pa.product_id IN (".implode(',',$kozos).")";
                $sql .= $this->szuresRaktar();
            }
        $sql .= " ORDER BY pa.attribute_id, `text`*1";


            $query = $this->db->query($sql);

            $vissza = array();
            $attribute_id = 0;
            foreach ($query->rows as $value) {
                if ($attribute_id != $value['attribute_id']) {
                    $attribute_id = $value['attribute_id'];
                    $vissza[$value['attribute_id']][0] = (float)$value['text'];
                }
                $vissza[$value['attribute_id']][1] = (float)$value['text'];
            }

        return $vissza;

    }







    public function veglegesuzletek($kozos="") {

        $beallitasok = $this->config->get('multifilter_beallitasok');
        $teljes_lista = $beallitasok['tulajdonsagok']['teljes'];

        $sql = "SELECT fs.filter_select_id, fs.name, fs.sort_order, pf.product_id, pf.filter_group_id FROM " . DB_PREFIX . "filter_select fs
            LEFT JOIN " . DB_PREFIX . "product_filter pf ON (pf.filter_select_id = fs.filter_select_id)
            LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)
                WHERE fs.language_id = '" . $this->config->get('config_language_id') . "'";

        if (!empty($kozos)) {
            $termekek = implode(",",$kozos);
            $sql .= " AND pf.product_id IN (" .$termekek. ")";
        }

        if (!empty($kozos) || $teljes_lista !=1) {
            $sql .= $this->szuresRaktar();
        }
        $sql .= " GROUP BY fs.filter_select_id ORDER BY fs.sort_order";

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function veglegesArTolIg($kozos=false) {
        $this->log->setMicroTime();

        $this->load->model("catalog/multi_kozosites");

        $this->model_catalog_multi_kozosites->kozos = $kozos ? implode(',',$kozos) : false;

        $this->model_catalog_multi_kozosites->customer_group_id = $this->config->get('config_customer_group_id');
        $this->model_catalog_multi_kozosites->customer_kedvezmeny = 1;

        $customer_id = $this->customer->isLogged();

        if ($customer_id) {
            $leker =  $this->db->query("SELECT szazalek FROM " . DB_PREFIX . "customer WHERE customer_id = '".$customer_id."'");
            if ($leker->num_rows > 0){
                $this->model_catalog_multi_kozosites->customer_kedvezmeny = 1 - $leker->row['szazalek']/100;
            }
            $this->model_catalog_multi_kozosites->customer_group_id = $this->customer->getCustomerGroupId();
        }


        $min = $this->model_catalog_multi_kozosites->arLekeres("least",'ASC',1);
        $max = $this->model_catalog_multi_kozosites->arLekeres("least",'DESC',1);

        $beallitasok = $this->config->get('multifilter_beallitasok');

        if(isset($beallitasok['ar_szures_tol_ig']['netto']) && $beallitasok['ar_szures_tol_ig']['netto'] > 0) {
            $price[0] = $min['price'];
            $price[1] = $max['price'];
        } else {
            $price[0] = round($this->tax->calculate($min['price'], $min['tax_class_id'],$this->config->get('config_tax')),0);
            $price[1] = round($this->tax->calculate($max['price'], $max['tax_class_id'],$this->config->get('config_tax')),0);
        }

        $this->log->GetMicroTime('Multifilter veglegesArTolIg() - ar_szures_tol_ig sor: 473');

        return $price;

    }

    public function veglegesFoKategoria() {

        $sql = "SELECT p.product_id, c.category_id,  p.quantity, p.date_ervenyes_ig, cd.name, c.sort_order, c.parent_id, c.piacter  FROM " . DB_PREFIX . "category c
           LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)
           LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id)
           LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p2c.category_id = c.category_id)
           INNER JOIN " . DB_PREFIX . "product p ON (p.product_id = p2c.product_id)
               WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "'
               AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
               AND c.status = '1'";


        $sql .= $this->szuresRaktar();

        $sql .= " GROUP BY c.category_id  ORDER BY c.sort_order, LCASE(cd.name)";

        $query = $this->db->query($sql);

        $categories = $this->buildTree($query->rows, 0, "");


        $newCategories = array();
        foreach($categories as $catkey=>$category) {
            $vissza = $this->categoriaTermekkel($category);
            if($vissza) {
                $newCategories[] = $vissza;
            }

        }

        return $newCategories;
    }

    public function categoriaTermekkel($categories) {


        if(is_array($categories) && !array_key_exists('product_id', $categories)) {
            $vissza = false;
            foreach($categories as $catkey=>$category) {
                $return = $this->categoriaTermekkel($category);
                if($return) {
                    $vissza = true;
                } else {
                    unset($categories[$catkey]);
                }
            }
            return $categories;

        } elseif(is_array($categories) && array_key_exists('children', $categories)) {
            $return = $this->categoriaTermekkel($categories['children']);
            if(is_array($return) && count($return) > 0) {
                $categories['children'] = $return;
            } else {
                unset($categories['children']);
                $mehet = true;
                if (!is_null($categories['product_id'])  ) {

                    return $categories;

                } else {
                    return false;
                }
            }
            return $categories;

        } else {
            if (!is_null($categories['product_id']) ) {
                return $categories;
            } else {
                return false;
            }
        }

    }

    public function getNewPaths($products) {

    }

    public function veglegesAlKategoria($category_id,$kozos=array()) {

        if (is_array($category_id)) {
            $cache = http_build_query($category_id);
        } else {
            $cache = $category_id;
        }

        if ($kozos) {
            $cache .= http_build_query($kozos);
        }
        $cache = md5($cache);

        $newCategories = $this->cache->get('category.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id').'.'.$cache);

        if (!$newCategories) {
            // Alkategória   - kategoria_sub

            $sql = "SELECT p.product_id, c.category_id,  p.quantity, p.date_ervenyes_ig, cd.name, c.sort_order, c.parent_id, c.piacter  FROM " . DB_PREFIX . "category c
           LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)
           LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id)
           LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p2c.category_id = c.category_id)
           LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = p2c.product_id)
               WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "'
               AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
               AND c.status = '1'";


            $sql .= $this->szuresRaktar();

            if (!empty($kozos)) {
                $termekek = implode(",", $kozos);
                $beallitasok = $this->config->get('multifilter_beallitasok');
                $teljes_lista = $beallitasok['kategoria_sub']['teljes'];
                if ($teljes_lista != 1) {
                    $sql .= " AND p.product_id IN (" . $termekek . ")";
                }
            }


            $sql .= " GROUP BY c.category_id  ORDER BY c.sort_order, LCASE(cd.name)";
            $query = $this->db->query($sql);


            $meg_categoria = array();
            if (!empty($kozos) && $query->num_rows > 0) {
                foreach ($query->rows as $cat) {
                    if ($cat['parent_id']) {
                        if (!$this->config->in_array_key_value($query->rows, 'category_id', $cat['parent_id'])) {
                            $meg_categoria[$cat['parent_id']] = $cat['parent_id'];
                        }
                    }
                }
            }

            if ($meg_categoria) {
                $sql = "SELECT p.product_id, c.category_id,  p.quantity, p.date_ervenyes_ig, cd.name, c.sort_order, c.parent_id, c.piacter  FROM " . DB_PREFIX . "category c
                LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)
                LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id)
                LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p2c.category_id = c.category_id)
                LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = p2c.product_id)
                WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "'
                    AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
                    AND c.status = '1'";

                $termekek = implode(",", $meg_categoria);
                $sql .= " AND c.category_id IN (" . $termekek . ")";
                $sql .= " GROUP BY c.category_id  ORDER BY c.sort_order, LCASE(cd.name)";
                $query_meg_category = $this->db->query($sql);
                if ($query_meg_category->num_rows > 0) {
                    $query->rows = array_merge($query->rows, $query_meg_category->rows);
                }
            }

            $categories = $this->buildTree($query->rows, 0, "");


            if (!empty($kozos) && empty($category_id)) {
                return $categories;
            }

            foreach ($categories as $key => $categorie) {
                if (!in_array($categorie['category_id'], $category_id)) {
                    unset ($categories[$key]);
                }
            }
            $this->load->model('module/multi_category');
            $newCategories = $this->model_module_multi_category->onlyFindProduct($categories);

            $this->cache->set('category.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id').'.'.$cache, $newCategories);

        }

        return $newCategories;

    }



    public function buildTree(array $elements, $parentId = 0, $path) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $ha_piacter="";
                if ($element['piacter'] == 1){
                    $ha_piacter.="&piacter=piacter";
                }

                $element['href'] =  $this->url->link('product/category', 'path=' . $path.$element['category_id'].$ha_piacter );
                $children = $this->buildTree($elements, $element['category_id'], $path.$element['category_id']."_");
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }



    public function szuresRaktar() {

        $sql = "";

        $szuresek = $this->config->get('megjelenit_altalanos');

        if ($szuresek['mennyisegre_szur'] == 1 || (isset($_REQUEST['filter_raktaron']) && $_REQUEST['filter_raktaron'] == 2) ) {
            $sql .= " AND p.quantity > 0 ";
        } elseif (isset($_REQUEST['filter_raktaron']) && $_REQUEST['filter_raktaron'] == 1)  {
            $sql .= " AND p.quantity <= 0 ";
        }

        if ($szuresek['ervenesseg_datumra_szur'] == 1) {
            $sql .= " AND ( p.date_ervenyes_ig = '' OR p.date_ervenyes_ig = '0000-00-00' OR p.date_ervenyes_ig >= curdate() ) ";
        }

        return $sql;
    }

}

?>