<?php
class ModelModuleUpsell extends Model {

    public function getFeaturedProducts($data) {
        $sql = "SELECT p.product_id FROM ".DB_PREFIX."product p WHERE p.product_id IN ($data) AND p.status = 1";
        $sql .= $this->cart->getProductsCartForUpsell();
        $sql .= $this->szuresRaktar();
        $query = $this->db->query($sql);

        if ($query->num_rows > 0) {
            foreach ($query->rows as $result) {
                $product_data[] =  $result['product_id'];
            }
            return $product_data;
        } else {
            return false;
        }
    }

    public function getAlsoBoughtProducts($data) {

        $orders_ids = '';

        $data['order_status_operand'] = isset($data['order_status_operand']) && !empty($data['order_status_operand']) ? $data['order_status_operand'] : ">=";

        $sql = "SELECT GROUP_CONCAT( DISTINCT op.order_id ORDER BY op.order_id DESC SEPARATOR  ',' ) AS group_orders
            FROM  `" . DB_PREFIX . "order_product` op
            LEFT JOIN  `" . DB_PREFIX . "order` o ON ( op.order_id = o.order_id )
            WHERE op.product_id =  '" . (int)$data['filter_product_id'] ."'
                AND o.order_status_id " . $this->db->escape($data['order_status_operand']) . "'" . (int)$data['order_status_id'] . "'";

        $query = $this->db->query($sql);

        if (!$query->row['group_orders']) {
            return false;
        }

        if ($query->row['group_orders'] != ""){
            $orders_ids = $query->row['group_orders'];
        }

        if (substr($orders_ids,-1) == ","){
            $orders_ids = substr($orders_ids,0,-1);
        }

        $sql = "SELECT op.product_id, SUM(op.quantity) as no_sells FROM " . DB_PREFIX . "order_product op
            INNER JOIN " . DB_PREFIX . "product p ON (p.product_id=op.product_id)
            WHERE op.product_id !='" . (int)$data['filter_product_id'] . "'
                AND op.order_id IN (" . $orders_ids . ")
                AND p.status = 1 ";

        $sql .= $this->cart->getProductsCartForUpsell();
        $sql .= $this->szuresRaktar();

        $sql .= " GROUP BY op.product_id ";

        $sort_data = array(
            'no_sells',
            'op.order_id',
            'op.quantity',
            'op.total'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY op.order_id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }


        $query = $this->db->query($sql);
        if ($query->num_rows == 0) return false;

        $this->load->model('catalog/product');
        $product_data = array();

        if ($query->num_rows > 0) {
            foreach ($query->rows as $result) {
                $product_data[] =  $result['product_id'];
            }
            return $product_data;
        } else {
            return false;
        }
    }








    private function szuresRaktar() {

        $sql = "";

        $szuresek = $this->config->get('megjelenit_altalanos');

        if ($szuresek['mennyisegre_szur'] == 1 ) {
            $sql .= " AND p.quantity > 0 ";
        }

        if ($szuresek['ervenesseg_datumra_szur'] == 1) {
            $sql .= " AND ( p.date_ervenyes_ig = '' OR p.date_ervenyes_ig = '0000-00-00' OR p.date_ervenyes_ig >= curdate() ) ";
        }

        return $sql;
    }


}