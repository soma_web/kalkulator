<?php
class ModelModuleErkeztetoOldal extends Model {

    public function getBisnodeData($bisnode_id) {
        $vissza = array();
        $sql = "SELECT  email,
                        partnername,
                        adoszam,
                        kapcsolattarto_vezetekneve,
                        kapcsolattarto_keresztneve,
                        phone,
                        szekhely_varos,
                        szekhely_utca_hazszam,
                        szekhely_irányitoszam
                FROM ".DB_PREFIX_KALKULATOR."gmail_cimek WHERE gmail_cimek_id='".$bisnode_id."'";
        $query = $this->db_kalkulator->query($sql);
        if ($query->row) {
            $vissza = $query->row;
        }

        return $vissza;

    }

    public function addBisnodeData($data) {
        if (!empty($data['email'])) {
            $_GET['email'] = $data['email'];
            $_REQUEST['email'] = $data['email'];
        }
        if (!empty($data['partnername'])) {
            $_GET['company'] = $data['partnername'];
            $_REQUEST['company'] = $data['partnername'];
        }
        if (!empty($data['adoszam'])) {
            $_GET['adoszam'] = $data['adoszam'];
            $_REQUEST['adoszam'] = $data['adoszam'];
        } else {
            $_REQUEST['adoszam'] = 'nincs';
        }


        if (!empty($data['kapcsolattarto_vezetekneve'])) {
            $_GET['firstname'] = $data['kapcsolattarto_vezetekneve'];
            $_REQUEST['firstname'] = $data['kapcsolattarto_vezetekneve'];
        }
        if (!empty($data['kapcsolattarto_keresztneve'])) {
            $_GET['lastname'] = $data['kapcsolattarto_keresztneve'];
            $_REQUEST['lastname'] = $data['kapcsolattarto_keresztneve'];
        }
        if (!empty($data['phone'])) {
            $_GET['telephone'] = $data['phone'];
            $_REQUEST['telephone'] = $data['phone'];
        }
        if (!empty($data['szekhely_varos'])) {
            $_GET['city'] = $data['szekhely_varos'];
            $_REQUEST['city'] = $data['szekhely_varos'];
        }
        if (!empty($data['szekhely_utca_hazszam'])) {
            $_GET['address_1'] = $data['szekhely_utca_hazszam'];
            $_REQUEST['address_1'] = $data['szekhely_utca_hazszam'];
        }
        if (!empty($data['szekhely_irányitoszam'])) {
            $_GET['postcode'] = $data['szekhely_irányitoszam'];
            $_REQUEST['postcode'] = $data['szekhely_irányitoszam'];
        }
    }

}

?>