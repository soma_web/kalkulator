<?php
class ModelModuleSpecialOfferReminder extends Model {
	public function addReminder($data){
		$sql = "INSERT INTO " . DB_PREFIX . "special_offer_reminder
		        SET name              ='" . $this->db->escape($data['sor_fullname']) . "', 
		            email             ='" . $this->db->escape($data['sor_email']) . "', 	
					product_id        ='" . (int)$data['sor_product_id'] . "',
					days_before       ='" . (int)$data['sor_days_before'] . "'";
			
		if ($this->customer->isLogged()){
			$customer_group_id = $this->customer->getCustomerGroupId();
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$sql .= ", customer_group_id ='" . (int)$customer_group_id . "'";	
		
		
		$this->db->query($sql);
	}
	
	public function editReminder($reminder_id, $data){
		$sql = "UPDATE " . DB_PREFIX . "special_offer_reminder
		        SET name            ='" . $this->db->escape($data['sor_fullname']) . "', 
		            email           ='" . $this->db->escape($data['sor_email']) . "', 
					product_id      ='" . (int)$data['sor_product_id'] . "',
					days_before     ='" . (int)$data['sor_days_before'] . "'";
		if ($this->customer->isLogged()){
			$customer_group_id = $this->customer->getCustomerGroupId();
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$sql .= ", customer_group_id ='" . (int)$customer_group_id . "' ";			
		$sql .= "WHERE reminder_id='" . (int)$reminder_id . "'";
		
		$this->db->query($sql);
	}
		
	public function existReminder($data){
		$sql = "SELECT reminder_id FROM " . DB_PREFIX . "special_offer_reminder WHERE LOWER(email)='" . strtolower($this->db->escape($data['sor_email'])) . "' AND product_id ='" . (int)$data['sor_product_id'] . "'";
		$query = $this->db->query($sql);
		
		if ($query->num_rows){
			return $query->row['reminder_id'];
		} else {
			return 0;
		}
	}
	
	public function deleteReminder($reminder_id){
		$this->db->query("DELETE FROM " . DB_PREFIX . "special_offer_reminder WHERE reminder_id='" . (int)$reminder_id . "'" );
	}
	
	public function getProductSpecialInfo($product_id){
		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getCustomerGroupId();
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}
	
		$sql = "SELECT * FROM " . DB_PREFIX . "product_special 
		        WHERE product_id='" . (int)$product_id . "' AND date_start < NOW() AND date_end > NOW() AND customer_group_id='" . (int)$customer_group_id . "'
				ORDER BY priority ASC 
				LIMIT 0,1";
		
		$query = $this->db->query($sql);		
		
		if ($query->num_rows){
			return $query->row;
		} else {
			return 0;
		}
	}
	
	public function getSubscribers(){
		$query = $this->db->query("SELECT DISTINCT name, email FROM " . DB_PREFIX . "special_offer_reminder");
		
		return $query->rows;
	}
	
	public function getSubscriberReminders($email){
		$sql = "SELECT spr.*, ps.* FROM " . DB_PREFIX . "special_offer_reminder spr 
			    LEFT JOIN " . DB_PREFIX . "product_special ps ON (spr.product_id = ps.product_id)
			    WHERE LOWER(spr.email)='" . strtolower($email) . "' AND DATEDIFF(ps.date_end, NOW()) = spr.days_before AND spr.customer_group_id = ps.customer_group_id";
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
}
?>