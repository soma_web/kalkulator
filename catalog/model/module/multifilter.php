<?php
class ModelModuleMultifilter extends Model {


    public function sessionUrit() {
        if (isset($this->session->data['path'])) unset($this->session->data['path']);
        if (isset($this->session->data['filter'])) unset($this->session->data['filter']);
        if (isset($this->session->data['filter_varos'])) unset($this->session->data['filter_varos']);
        if (isset($this->session->data['filter_artol'])) unset($this->session->data['filter_artol']);
        if (isset($this->session->data['filter_arig'])) unset($this->session->data['filter_arig']);
        if (isset($this->session->data['filter_ar_tol_ig'])) unset($this->session->data['filter_ar_tol_ig']);
        if (isset($this->session->data['filter_tulajdonsag_csuszka'])) unset($this->session->data['filter_tulajdonsag_csuszka']);
        if (isset($this->session->data['filter_arszazalek'])) unset($this->session->data['filter_arszazalek']);
        if (isset($this->session->data['filter_uzlet'])) unset($this->session->data['filter_uzlet']);
        if (isset($this->session->data['filter_tulajdonsagok'])) unset($this->session->data['filter_tulajdonsagok']);
        if (isset($this->session->data['filter_valasztek'])) unset($this->session->data['filter_valasztek']);
        if (isset($this->session->data['filter_gyarto'])) unset($this->session->data['filter_gyarto']);
        if (isset($this->session->data['filter_raktaron'])) unset($this->session->data['filter_raktaron']);
        if (isset($this->session->data['filter_meret'])) unset($this->session->data['filter_meret']);

    }
}