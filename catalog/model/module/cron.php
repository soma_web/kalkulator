<?php
class ModelModuleCron extends Model {

    public function getRunCrons() {
        $sql = " SELECT * FROM ".DB_PREFIX."cron WHERE status=1";
        $query = $this->db->query($sql);

        $modules = array();

        if ($query->num_rows > 0) {

            date_default_timezone_set('Europe/Budapest');

            $time = date("H:i:s");
            $datetime = date("Y-m-d H:i:s");

            foreach($query->rows as $cron) {
                $utolso_futas = $this->utolsoFutas($cron['cron_id']);
                if ($cron['rendszeresseg'] == 1) { // Naponta rendszeresen
                    echo '<br>utolsó futás: '.$utolso_futas;
                    if ($cron['idonkent'] == 1) {  // 5 perc
                        if (date('Y-m-d H:i:s', strtotime($utolso_futas.' + 5 minute')) <= $datetime) {
                            echo '5 perces elindítva';
                            $modules[] = array(
                                'cron'      => 'cron/'.$cron['program'],
                                'cron_id'   => $cron['cron_id']
                            );
                        }

                    } elseif ($cron['idonkent'] == 2) {  // 10 perc
                        if (date('Y-m-d H:i:s', strtotime($utolso_futas.' + 10 minute')) <= $datetime) {
                            echo '10 perces elindítva<br>';

                            $modules[] = array(
                                'cron'      => 'cron/'.$cron['program'],
                                'cron_id'   => $cron['cron_id']
                            );
                        }

                    } elseif ($cron['idonkent'] == 3) {  // 15 perc
                        if (date('Y-m-d H:i:s', strtotime($utolso_futas.' + 15 minute')) <= $datetime) {
                            echo '15 perces elindítva<br>';
                            $modules[] = array(
                                'cron'      => 'cron/'.$cron['program'],
                                'cron_id'   => $cron['cron_id']
                            );
                        }

                    } elseif ($cron['idonkent'] == 4) {  // 20 perc
                        if (date('Y-m-d H:i:s', strtotime($utolso_futas.' + 20 minute')) <= $datetime) {
                            echo '20 perces elindítva<br>';
                            $modules[] = array(
                                'cron'      => 'cron/'.$cron['program'],
                                'cron_id'   => $cron['cron_id']
                            );
                        }

                    } elseif ($cron['idonkent'] == 5) {  // 30 perc
                        if (date('Y-m-d H:i:s', strtotime($utolso_futas.' + 30 minute')) <= $datetime) {
                            echo '30 perces elindítva<br>';
                            $modules[] = array(
                                'cron'      => 'cron/'.$cron['program'],
                                'cron_id'   => $cron['cron_id']
                            );
                        }

                    } elseif ($cron['idonkent'] == 6) {  // 60 perc
                        if (date('Y-m-d H:i:s', strtotime($utolso_futas.' + 60 minute')) <= $datetime) {
                            echo '60 perces elindítva<br>';
                            $modules[] = array(
                                'cron'      => 'cron/'.$cron['program'],
                                'cron_id'   => $cron['cron_id']
                            );
                        }

                    } elseif ($cron['idonkent'] == 7) {  // 1 perc
                        if (date('Y-m-d H:i:s', strtotime($utolso_futas.' + 1 minute')) <= $datetime) {
                            echo '1 perces elindítva<br>';
                            $modules[] = array(
                                'cron'      => 'cron/'.$cron['program'],
                                'cron_id'   => $cron['cron_id']
                            );
                        }
                    }

                } elseif ($cron['rendszeresseg'] == 2) { // Naponta megadott időpontokban
                    $mikor_fusson = $this->kovetkezoFutasNaponta($cron['cron_id']);
                    if ($mikor_fusson) {

                        if (date('H:i:s', strtotime($utolso_futas)) <= $mikor_fusson) {
                            echo 'Napi időpontokban elindítva<br>';
                            $modules[] = array(
                                'cron'      => 'cron/'.$cron['program'],
                                'cron_id'   => $cron['cron_id']
                            );
                        }
                    }


                } elseif ($cron['rendszeresseg'] == 3) { // Hetente megadott időpontokban
                    $mikor_fusson = $this->kovetkezoFutasHetente($cron['cron_id']);
                    if ($mikor_fusson) {

                        if (date('Y-m-d H:i:s', strtotime($utolso_futas)) <= date('Y-m-d H:i:s', strtotime($mikor_fusson))) {
                            echo 'Napi időpontokban elindítva<br>';
                            $modules[] = array(
                                'cron'      => 'cron/'.$cron['program'],
                                'cron_id'   => $cron['cron_id']
                            );
                        }
                    }
                }
            }
        }

        return $modules;
    }

    private function utolsoFutas($cron_id) {
        $sql =  "SELECT date_start, date_end, cron_allapot_id FROM ".DB_PREFIX."cron_allapot ";
        $sql .= " WHERE cron_id = '".$cron_id."' AND (date_end IS NULL OR date_end = 0)";
        $query = $this->db->query($sql);

        $vissza = date('Y-m-d H:i:s');
        if ($query->num_rows > 0) {
            foreach($query->rows as $value) {
                if (date('Y-m-d H:i:s', strtotime($value['date_start'].' + 15 minute')) < date('Y-m-d H:i:s')) {
                    $sql = "UPDATE ".DB_PREFIX."cron_allapot SET date_end=NOW() WHERE cron_allapot_id='".$value['cron_allapot_id']."'";
                    $query = $this->db->query($sql);
                }
            }
        } else {
            $sql =  "SELECT date_start, date_end, cron_allapot_id FROM ".DB_PREFIX."cron_allapot ";
            $sql .= " WHERE cron_id = '".$cron_id."'";
            $sql .= " ORDER BY date_end DESC LIMIT 0,1";
            $query = $this->db->query($sql);
            if ($query->num_rows > 0) {
                $vissza = date('Y-m-d H:i:s', strtotime($query->row['date_end']));

            } else {
                $vissza = date('Y-m-d H:i:s', strtotime('1971-01-01 00:00:00'));
            }
        }
        return $vissza;

    }

    private function kovetkezoFutasNaponta($cron_id) {
        $sql = "SELECT pontosido FROM ".DB_PREFIX."cron_pontosido
                    WHERE
                        cron_id = '".$cron_id."' AND
                        pontosido <= '".date('H:i:s')."'
                    ORDER BY pontosido DESC LIMIT 0,1";
        $query = $this->db->query($sql);

        if ($query->num_rows > 0) {
            $vissza = $query->row['pontosido'];
        } else {
            $vissza = false;
        }
        return $vissza;
    }

    private function kovetkezoFutasHetente($cron_id) {
        $het_napja = date('w');
        $het_napja = $het_napja == 0 ? 7 : $het_napja + 1;

        $sql = "SELECT pontosido FROM ".DB_PREFIX."cron_pontosido
                    WHERE
                        cron_id = '".$cron_id."' AND
                        pontosido <= '".date('H:i:s')."' AND
                        het_napja = '".$het_napja."'
                    ORDER BY pontosido DESC LIMIT 0,1";
        $query = $this->db->query($sql);

        if ($query->num_rows > 0) {
            $vissza = $query->row['pontosido'];
        } else {
            $vissza = false;
        }
        return $vissza;
    }


}