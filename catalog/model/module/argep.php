<?php
class ModelModuleArgep extends Model {

    public function getProducts() {

        $customer_group_id = $this->config->get('config_customer_group_id');

        $sql = "SELECT p.product_id, p.cikkszam, p.price, cd.name AS category, m.name AS manufacturer, pd.name AS product_name, pd.short_description, p.image, ua.keyword, p.quantity, p.tax_class_id,
               (SELECT price FROM " . DB_PREFIX . "product_discount pd2
                    WHERE   pd2.product_id = p.product_id
                        AND pd2.customer_group_id = '" . (int)$customer_group_id . "'
                        AND pd2.quantity < '2'
                        AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW())
                        AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW()))
                    ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount,

                (SELECT price FROM " . DB_PREFIX . "product_special ps
                    WHERE   ps.product_id = p.product_id
                        AND ps.customer_group_id = '" . (int)$customer_group_id . "'
                        AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW())
                        AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW()))
                    ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special

        FROM " . DB_PREFIX . "product p
            LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
            LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)
            LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)
            LEFT JOIN " . DB_PREFIX . "category_description cd ON (cd.category_id = p2c.category_id AND cd.language_id=".(int)$this->config->get('config_language_id').")
            LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id)
            LEFT JOIN " . DB_PREFIX . "url_alias ua ON (ua.query = concat('product_id=',p.product_id))";


        $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'
                    AND p.status = '1'
                    AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

        $sql .= "   GROUP BY p.product_id";
        $sql .= " ORDER BY p.cikkszam";
        $query = $this->db->query($sql);

        return $query->rows;


    }

    public function tax($price,$tax_class_id, $calculate = true) {

        $vissza = round($price);

        if ($calculate) {
            $sql = "SELECT tax_rate_id FROM " . DB_PREFIX . "tax_rule WHERE tax_class_id=".$tax_class_id." LIMIT 0,1";
            $query = $this->db->query($sql);

            if ($query->num_rows > 0) {
                $sql = "SELECT rate FROM " . DB_PREFIX . "tax_rate WHERE tax_rate_id=".$query->row['tax_rate_id']." LIMIT 0,1";
                $query = $this->db->query($sql);
                if ($query->num_rows > 0) {
                    $tax = (int)$query->row['rate'];
                    $vissza = round($price * (1+$tax/100));
                }
            }
        }

        return $vissza;
    }
}