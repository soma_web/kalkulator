<?php
class ModelPaymentPayU extends Model {
    public function getMethod($address, $total) {
        $this->load->language('payment/payu');


        $currency = $this->currency->getCode();
        if($currency == "EUR" || $currency == "HUF" || $currency == "USD") $status=true;
            else $status=false;

        $method_data = array();

        if ($status) {
            $method_data = array(
                'code'       => 'payu',
                'title'      => $this->language->get('text_title'),
                'sort_order' => $this->config->get('payu_sort_order')
            );
        }

        return $method_data;
    }
}
?>