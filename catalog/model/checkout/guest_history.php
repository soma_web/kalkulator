<?php
class ModelCheckoutGuestHistory extends Model {

	public function addHistory($data) {
        echo '';
        if (empty($_SESSION['checkout_click_history_id'])) {
            $sql = "INSERT INTO ".DB_PREFIX."checkout_click_history SET
                `firstname`     = '".(!empty($data['firstname']) ? $data['firstname'] : '')."',
                `lastname`	    = '".(!empty($data['lastname']) ? $data['lastname'] : '')."',
                `email`	        = '".(!empty($data['email']) ? $data['email'] : '')."',
                `telefon`	    = '".(!empty($data['telephone']) ? $data['telephone'] : '')."',
                `orszag`	    = '".(!empty($data['payment']['country']) ? $data['payment']['country'] : '')."',
                `varos`	        = '".(!empty($data['payment']['city']) ? $data['payment']['city'] : '')."',
                `iranyitoszam`	= '".(!empty($data['payment']['postcode']) ? $data['payment']['postcode'] : '')."',
                `utca_hazszam`	= '".(!empty($data['payment']['address_1']) ? $data['payment']['address_1'] : '')."',
                `ip`	        = '".(!empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '')."',
                `date_added`	= NOW(),
                `formertek_id`  = '".(!empty($data['formertek_id']) ? $data['formertek_id'] : '')."'";
            $this->db->query($sql);
            $_SESSION['checkout_click_history_id'] = $this->db->getLastId();
        } else {
            $sql = "UPDATE ".DB_PREFIX."checkout_click_history SET
                `firstname`     = '".(!empty($data['firstname']) ? $data['firstname'] : '')."',
                `lastname`	    = '".(!empty($data['lastname']) ? $data['lastname'] : '')."',
                `email`	        = '".(!empty($data['email']) ? $data['email'] : '')."',
                `telefon`	    = '".(!empty($data['telephone']) ? $data['telephone'] : '')."',
                `orszag`	    = '".(!empty($data['payment']['country']) ? $data['payment']['country'] : '')."',
                `varos`	        = '".(!empty($data['payment']['city']) ? $data['payment']['city'] : '')."',
                `iranyitoszam`	= '".(!empty($data['payment']['postcode']) ? $data['payment']['postcode'] : '')."',
                `utca_hazszam`	= '".(!empty($data['payment']['address_1']) ? $data['payment']['address_1'] : '')."',
                `ip`	        = '".(!empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '')."',
                `date_added`	= NOW(),
                `formertek_id`  = '".(!empty($data['formertek_id']) ? $data['formertek_id'] : '')."'
                WHERE checkout_click_history_id=".$_SESSION['checkout_click_history_id'];
            $siker = $this->db->query($sql);
        }
        $this->addHistory_To(array('Személyes adatok'=>'Tovább'));
	}
    public function addHistory_To($data=array()) {
        if (empty($_SESSION['checkout_click_history_id'])) {
            $sql = "INSERT INTO ".DB_PREFIX."checkout_click_history SET
                `ip`	        = '".(!empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '')."',
                `date_added`	= NOW()";
            $siker = $this->db->query($sql);
            $_SESSION['checkout_click_history_id'] = $this->db->getLastId();
        }
        if ($data) {
            foreach($data as $key=>$value) {
                $sql = "INSERT INTO ".DB_PREFIX."checkout_click_history_to SET
                    checkout_click_history_id   = '".$_SESSION['checkout_click_history_id']."',
                    oldal_neve                  = '".$key."',
                    oldal_ertek                 = '".$value."'";
                $siker = $this->db->query($sql);
            }
        }


    }
}
?>