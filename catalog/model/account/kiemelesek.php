<?php
class ModelAccountKiemelesek extends Model {

    public function getKiemelesek($ingyenes) {

        $vissza = array();
        $elhelyezkedesek = $this->config->get("kiemelesek");

        if ($elhelyezkedesek) {
            foreach ($elhelyezkedesek as $value) {
                if ($value['status'] == 1) {
                    if( $value['beallitas'] == 0 ||  ($value['beallitas'] == 1 && $ingyenes == 1) || ($value['beallitas'] == 2 && $ingyenes != 1) ) {
                        $vissza[] = $value;
                    }
                }
            }
        }

        return $vissza;

    }

}
?>