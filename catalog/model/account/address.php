<?php
class ModelAccountAddress extends Model {
	public function addAddress($data) {
        $company = isset($data['company']) ? $this->db->escape($data['company']) : '';
        $adoszam = isset($data['adoszam']) ? $this->db->escape($data['adoszam']) : '';
        $firstname = isset($data['firstname']) ? $this->db->escape($data['firstname']) : '';
        $lastname = isset($data['lastname']) ? $this->db->escape($data['lastname']) : '';
        $address_1 = isset($data['address_1']) ? $this->db->escape($data['address_1']) : '';
        $address_2 = isset($data['address_2']) ? $this->db->escape($data['address_2']) : '';
        $postcode = isset($data['postcode']) ? $this->db->escape($data['postcode']) : '';
        $city = isset($data['city']) ? $this->db->escape($data['city']) : '';
        $zone_id = isset($data['zone_id']) ? (int)$data['zone_id'] : 0;
        $country_id = isset($data['country_id']) ? (int)$data['country_id'] : 0;
        $vallalkozasi_forma = isset($data['vallalkozasi_forma']) ? $this->db->escape($data['vallalkozasi_forma']) : '';
        $szekhely = isset($data['szekhely']) ? $this->db->escape($data['szekhely']) : '';
        $ugyvezeto_neve = isset($data['ugyvezeto_neve']) ? $this->db->escape($data['ugyvezeto_neve']) : '';
        $ugyvezeto_telefonszama = isset($data['ugyvezeto_telefonszama']) ? $this->db->escape($data['ugyvezeto_telefonszama']) : '';

		$this->db->query("INSERT INTO " . DB_PREFIX . "address SET
		        customer_id             = '" . (int)$this->customer->getId() . "',
		        company                 = '" . $company . "',
		        adoszam                 = '" . $adoszam . "',
		        firstname               = '" . $firstname . "',
		        lastname                = '" . $lastname . "',
		        address_1               = '" . $address_1 . "',
		        address_2               = '" . $address_2 . "',
		        postcode                = '" . $postcode . "',
		        city                    = '" . $city . "',
		        zone_id                 = '" . $zone_id . "',
		        country_id              = '" . $country_id . "',
		        vallalkozasi_forma      = '" . $vallalkozasi_forma. "',
      	        szekhely                = '" . $szekhely. "',
      	        ugyvezeto_neve          = '" . $ugyvezeto_neve. "',
      	        ugyvezeto_telefonszama  = '" . $ugyvezeto_telefonszama. "'"
        );
		
		$address_id = $this->db->getLastId();
		
		if (isset($data['default']) && $data['default'] == '1') {
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
		}
		if(isset($city) && $city) {
            $this->session->data['shipping_address']['city_new'] = $city;
        }
		return $address_id;
	}
	
	public function editAddress($address_id, $data) {
        $company = isset($data['company']) ? $this->db->escape($data['company']) : '';
        $adoszam = isset($data['adoszam']) ? $this->db->escape($data['adoszam']) : '';
        $firstname = isset($data['firstname']) ? $this->db->escape($data['firstname']) : '';
        $lastname = isset($data['lastname']) ? $this->db->escape($data['lastname']) : '';
        $address_1 = isset($data['address_1']) ? $this->db->escape($data['address_1']) : '';
        $address_2 = isset($data['address_2']) ? $this->db->escape($data['address_2']) : '';
        $postcode = isset($data['postcode']) ? $this->db->escape($data['postcode']) : '';
        $city = isset($data['city']) ? $this->db->escape($data['city']) : '';
        $zone_id = isset($data['zone_id']) ? (int)$data['zone_id'] : 0;
        $country_id = isset($data['country_id']) ? (int)$data['country_id'] : 0;
        $vallalkozasi_forma = isset($data['vallalkozasi_forma']) ? $this->db->escape($data['vallalkozasi_forma']) : '';
        $szekhely = isset($data['szekhely']) ? $this->db->escape($data['szekhely']) : '';
        $ugyvezeto_neve = isset($data['ugyvezeto_neve']) ? $this->db->escape($data['ugyvezeto_neve']) : '';
        $ugyvezeto_telefonszama = isset($data['ugyvezeto_telefonszama']) ? $this->db->escape($data['ugyvezeto_telefonszama']) : '';

		$this->db->query("UPDATE " . DB_PREFIX . "address SET
		        company                 = '" . $company . "',
		        adoszam                 = '" . $adoszam . "',
		        firstname               = '" . $firstname . "',
		        lastname                = '" . $lastname . "',
		        address_1               = '" . $address_1 . "',
		        address_2               = '" . $address_2 . "',
		        postcode                = '" . $postcode . "',
		        city                    = '" . $city . "',
		        zone_id                 = '" . $zone_id . "',
		        country_id              = '" . $country_id . "',
		        vallalkozasi_forma      = '" . $vallalkozasi_forma. "',
      	        szekhely                = '" . $szekhely. "',
      	        ugyvezeto_neve          = '" . $ugyvezeto_neve. "',
      	        ugyvezeto_telefonszama  = '" . $ugyvezeto_telefonszama. "'
                WHERE address_id  = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'"
        );
	
		if (isset($data['default']) && $data['default'] == '1') {
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
		}
	}
	
	public function deleteAddress($address_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");
	}	
	
	public function getAddress($address_id) {
		$address_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address
		WHERE address_id = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

        $address_query->row['country_id'] = !empty($address_query->row['country_id']) ? $address_query->row['country_id'] : 97;
		if ($address_query->num_rows) {
            if (isset($_SESSION['country'][$address_query->row['country_id']])) {
                $country_query = $_SESSION['country'][$address_query->row['country_id']];
            } else {
                $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");
                $_SESSION['country'][$address_query->row['country_id']] = $country_query;
            }


			if ($country_query->num_rows) {
				$country = $country_query->row['name'];
				$iso_code_2 = $country_query->row['iso_code_2'];
				$iso_code_3 = $country_query->row['iso_code_3'];
				$address_format = $country_query->row['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';	
				$address_format = '';
			}
			
			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");
			
			if ($zone_query->num_rows) {
				$zone = $zone_query->row['name'];
				$code = $zone_query->row['code'];
			} else {
				$zone = '';
				$code = '';
			}

            $customer_query = $this->db->query("SELECT feltolto FROM `" . DB_PREFIX . "customer` WHERE customer_id = '" . (int)$address_query->row['customer_id'] . "'");

            $address_data = array(
				'firstname'                 => $address_query->row['firstname'],
				'lastname'                  => $address_query->row['lastname'],
				'company'                   => $address_query->row['company'],
				'adoszam'                   => $address_query->row['adoszam'],
				'address_1'                 => $address_query->row['address_1'],
				'address_2'                 => $address_query->row['address_2'],
				'postcode'                  => $address_query->row['postcode'],
				'city'                      => $address_query->row['city'],
				'zone_id'                   => $address_query->row['zone_id'],
                'vallalkozasi_forma'        => $address_query->row['vallalkozasi_forma'],
                'szekhely'                  => $address_query->row['szekhely'],
                'ugyvezeto_neve'            => $address_query->row['ugyvezeto_neve'],
                'ugyvezeto_telefonszama'    => $address_query->row['ugyvezeto_telefonszama'],
				'zone'                      => $zone,
				'zone_code'                 => $code,
				'country_id'                => $address_query->row['country_id'],
				'country'                   => $country,
				'iso_code_2'                => $iso_code_2,
				'iso_code_3'                => $iso_code_3,
				'feltolto'                  => $customer_query->row['feltolto'],
				'address_format'            => $address_format

			);


			
			return $address_data;
		} else {
			return false;	
		}
	}
	
	public function getAddresses() {
		$address_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$this->customer->getId() . "'");
	
		foreach ($query->rows as $result) {
            if (isset($_SESSION['country'][$result['country_id']])) {
                $country_query = $_SESSION['country'][$result['country_id']];
            } else {
                $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$result['country_id'] . "'");
                $_SESSION['country'][$result['country_id']] = $country_query;
            }

			if ($country_query->num_rows) {
				$country = $country_query->row['name'];
				$iso_code_2 = $country_query->row['iso_code_2'];
				$iso_code_3 = $country_query->row['iso_code_3'];
				$address_format = $country_query->row['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';	
				$address_format = '';
			}
			
			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$result['zone_id'] . "'");
			
			if ($zone_query->num_rows) {
				$zone = $zone_query->row['name'];
				$code = $zone_query->row['code'];
			} else {
				$zone = '';
				$code = '';
			}		
		
			$address_data[] = array(
				'address_id'                => $result['address_id'],
				'firstname'                 => $result['firstname'],
				'lastname'                  => $result['lastname'],
				'company'                   => $result['company'],
				'adoszam'                   => $result['adoszam'],
				'address_1'                 => $result['address_1'],
				'address_2'                 => $result['address_2'],
				'postcode'                  => $result['postcode'],
				'city'                      => $result['city'],
				'zone_id'                   => $result['zone_id'],
                'vallalkozasi_forma'        => $result['vallalkozasi_forma'],
                'szekhely'                  => $result['szekhely'],
                'ugyvezeto_neve'            => $result['ugyvezeto_neve'],
                'ugyvezeto_telefonszama'    => $result['ugyvezeto_telefonszama'],
				'zone'                      => $zone,
				'zone_code'                 => $code,
				'country_id'                => $result['country_id'],
				'country'                   => $country,
				'iso_code_2'                => $iso_code_2,
				'iso_code_3'                => $iso_code_3,
				'address_format'            => $address_format
			);
		}		
		
		return $address_data;
	}	
	
	public function getTotalAddresses() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$this->customer->getId() . "'");
	
		return $query->row['total'];
	}
}
?>