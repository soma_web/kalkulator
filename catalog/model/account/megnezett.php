<?php
class ModelAccountMegnezett extends Model {

    public function getProductsViewed($data = array(),$cOrderID=0) {


        $sql = "SELECT pd.name, p.model, p.viewed FROM " . DB_PREFIX . "product p
            LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
             WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'
             AND p.viewed > 0
             AND p.product_id IN (".$cOrderID.")
             ORDER BY p.viewed DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalProductsViewed($cOrderID=0) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE viewed > 0  AND product_id IN (".$cOrderID.")");

        return $query->row['total'];
    }

    public function getTotalProductViews($cOrderID=0) {

        $query = $this->db->query("SELECT SUM(viewed) AS total FROM " . DB_PREFIX . "product WHERE product_id IN (".$cOrderID.")");

        return $query->row['total'];
    }

    public function reset($cOrderID) {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET viewed = '0'  WHERE product_id IN (".$cOrderID.")");
    }

    public function getPurchased($data = array()) {
        $sql = "SELECT op.name, op.model, SUM(op.quantity) AS quantity, SUM(op.total + op.total * op.tax / 100) AS total
		FROM " . DB_PREFIX . "order_product op
		LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id)";

        if (!empty($data['filter_order_status_id'])) {
            $sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
        } else {
            $sql .= " WHERE o.order_status_id > '0'";
        }

        if (!empty($data['filter_date_start'])) {
            $sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
        }

        if (!empty($data['filter_date_end'])) {
            $sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
        }

        //$sql .= " GROUP BY op.model ORDER BY total DESC";
        $sql .= " GROUP BY op.product_id ORDER BY total DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }


    function getcustormProduct(){
        $proId = array();

        $query = $this->db->query("SELECT product_id FROM `" . DB_PREFIX . "product_customer` WHERE customer_id =".(int)$this->customer->getId()) ;
        if ($query->num_rows > 0){
            foreach($query->rows as $value) {
                $proId[]= $value['product_id'];
            }
            $proids = implode(',',$proId);
        } else {
            $proids = 0;
        }

        return $proids;
    }
}
?>
