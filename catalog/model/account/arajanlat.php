<?php
class ModelAccountArajanlat extends Model {

	
	public function szerkesztArajanlat($arajanlat_id) {
        $this->load->model('module/arajanlat');

        $arajanlat_info = $this->getArajanlat($arajanlat_id);
        $this->session->data["arajanlat_neve"]  = $arajanlat_info['name'];
        $this->session->data["arajanlat_id"]    = $arajanlat_info['arajanlat_id'];

        if (isset( $this->session->data['arajanlat'])) {
            unset ( $this->session->data['arajanlat']);
        }

        if ($arajanlat_info['products']) {
            foreach($arajanlat_info['products'] as $product) {
                $gyujto = array();

                if (isset($product['options']) && $product['options']) {
                    foreach($product['options'] as $option) {
                        foreach($option['value'] as $key=>$value) {
                            if ($value['mennyiseg'] > 0) {
                                $gyujto[$option['product_option_id']][$key] = $value['product_option_value_id'].'|'.$value['mennyiseg'];
                            } elseif ($value['product_option_value_id']) {
                                $gyujto[$option['product_option_id']] = $value['product_option_value_id'];
                                echo '';
                            } else {
                                $gyujto[$option['product_option_id']] = $value['product_option_value_ertek'];
                            }
                        }
                    }
                }
                $this->model_module_arajanlat->add($product['product_id'],$product['mennyiseg'],$gyujto);
            }
        }

    }

	public function editArajanlat($arajanlat_id, $data) {

        $sql = "UPDATE " . DB_PREFIX . "arajanlat
            SET `name`          = '".$this->db->escape($data['ajanlat_neve'])."',
                customer_id     = '".$data['vevo_azonosito']."',
                `total`         = '".$data['total-ertek_osszesen']."',
                date_modified   = NOW(),
                szallito_neve              = '".$this->db->escape($data['szallito_neve'])."',
                szallito_varos             = '".$this->db->escape($data['szallito_varos'])."',
                szallito_utca              = '".$this->db->escape($data['szallito_utca'])."',
                szallito_orszag            = '".$this->db->escape($data['szallito_orszag'])."',
                szallito_adoszam           = '".$this->db->escape($data['szallito_adoszam'])."',
                szallito_bank              = '".$this->db->escape($data['szallito_bank'])."',
                szallito_bankszamla_szam   = '".$this->db->escape($data['szallito_bankszamla_szam'])."',
                szallito_bankszamla_swift  = '".$this->db->escape($data['szallito_bankszamla_swift'])."',
                szallito_bankszamla_iban   = '".$this->db->escape($data['szallito_bankszamla_iban'])."',
                vevo_neve                  = '".$this->db->escape($data['vevo_neve'])."',
                vevo_varos                 = '".$this->db->escape($data['vevo_varos'])."',
                vevo_utca                  = '".$this->db->escape($data['vevo_utca'])."',
                vevo_orszag                = '".$this->db->escape($data['vevo_orszag'])."',
                vevo_adoszam               = '".$this->db->escape($data['vevo_adoszam'])."',
                vevo_bank                  = '".$this->db->escape($data['vevo_bank'])."',
                vevo_bankszamla_szam       = '".$this->db->escape($data['vevo_bankszamla_szam'])."',
                ajanlat_kelte              = '".$this->db->escape($data['ajanlat_kelte'])."',
                ajanlat_szallitasi_hatarido= '".$this->db->escape($data['ajanlat_szallitasi_hatarido'])."',
                ajanlat_fizetesi_hatarido  = '".$this->db->escape($data['ajanlat_fizetesi_hatarido'])."',
                ajanlat_fizetesi_mod       = '".$this->db->escape($data['ajanlat_fizetesi_mod'])."',
                ajanlat_kuldo_neve         = '".$this->db->escape($data['ajanlat_kuldo_neve'])."',
                ajanlat_kuldo_telefon      = '".$this->db->escape($data['ajanlat_kuldo_telefon'])."',
                ajanlat_kuldo_email        = '".$this->db->escape($data['ajanlat_kuldo_email'])."',
                ajanlat_megszolitas        = '".$this->db->escape($data['ajanlat_megszolitas'])."',
                ajanlat_altalanos_szoveg_felul= '".$this->db->escape($data['ajanlat_altalanos_szoveg_felul'])."',
                ajanlat_altalanos_szoveg_alul= '".$this->db->escape($data['ajanlat_altalanos_szoveg_alul'])."'

            WHERE arajanlat_id='" .$arajanlat_id. "'";
        $query = $this->db->query($sql);


        foreach($data as $key=>$adat) {

            if (substr($key,0,7) == 'product') {
                $product = explode('-',$key);

                switch($product[0]) {
                    case 'product_szazalek';
                        $sql = "UPDATE " . DB_PREFIX . "arajanlat_product
                            SET szazalek = '".$adat."'
                            WHERE product_id = '".$product[1]."'";
                        $query = $this->db->query($sql);

                        break;

                    case 'product_egysegar':
                        $sql = "UPDATE " . DB_PREFIX . "arajanlat_product
                            SET egysegar = '".$adat."'
                            WHERE product_id = '".$product[1]."'";
                        $query = $this->db->query($sql);

                        break;

                    case 'product_mennyiseg':
                        $sql = "UPDATE " . DB_PREFIX . "arajanlat_product
                            SET mennyiseg = '".$adat."'
                            WHERE product_id = '".$product[1]."'";
                        $query = $this->db->query($sql);

                        break;

                    case 'product_ertek':
                        $tisztitott = preg_replace("/[^0-9]/", "",$adat);

                        $sql = "UPDATE " . DB_PREFIX . "arajanlat_product
                            SET ertek = '".$tisztitott."'
                            WHERE product_id = '".$product[1]."'";
                        $query = $this->db->query($sql);

                        break;

                    case 'product_arosszesen':
                        $sql = "UPDATE " . DB_PREFIX . "arajanlat_product
                            SET product_ertek_osszesen = '".$adat."'
                            WHERE product_id = '".$product[1]."'";
                        $query = $this->db->query($sql);

                        break;
                }

            } elseif (substr($key,0,6) == 'option') {
                $product = explode('-',$key);

                switch($product[0]) {
                    case 'option_mennyiseg';
                        $sql = "UPDATE " . DB_PREFIX . "arajanlat_option_value
                            SET mennyiseg = '".$adat."'
                            WHERE arajanlat_option_value_id = '".$product[1]."'";
                        $query = $this->db->query($sql);

                        break;

                    case 'option_value_szazalek';
                        $sql = "UPDATE " . DB_PREFIX . "arajanlat_option_value
                            SET szazalek = '".$adat."'
                            WHERE arajanlat_option_value_id = '".$product[1]."'";
                        $query = $this->db->query($sql);

                        break;

                    case 'option_value_egysegar';
                        $sql = "UPDATE " . DB_PREFIX . "arajanlat_option_value
                            SET egysegar = '".$adat."'
                            WHERE arajanlat_option_value_id = '".$product[1]."'";
                        $query = $this->db->query($sql);

                        break;

                    case 'option_value_ertek';
                        $tisztitott = preg_replace("/[^0-9]/", "",$adat);

                        $sql = "UPDATE " . DB_PREFIX . "arajanlat_option_value
                            SET ertek = '".$tisztitott."'
                            WHERE arajanlat_option_value_id = '".$product[1]."'";
                        $query = $this->db->query($sql);

                        break;
                }

            }
        }


    }
	
	public function copyArajanlat($arajanlat_id) {
		$arajanlat = $this->getArajanlat($arajanlat_id);
		$this->addArajanlat($arajanlat);
		echo '';
	}

	public function deleteArajanlat($arajanlat_id) {

        $sql = "DELETE FROM " . DB_PREFIX . "arajanlat              WHERE arajanlat_id = '" .$arajanlat_id. "'";
        $query = $this->db->query($sql);

        $sql = "DELETE FROM " . DB_PREFIX . "arajanlat_to_store     WHERE arajanlat_id = '" .$arajanlat_id. "'";
        $query = $this->db->query($sql);

        $sql = "DELETE FROM " . DB_PREFIX . "arajanlat_product      WHERE arajanlat_id = '" .$arajanlat_id. "'";
        $query = $this->db->query($sql);

        $sql = "DELETE FROM " . DB_PREFIX . "arajanlat_option       WHERE arajanlat_id = '" .$arajanlat_id. "'";
        $query = $this->db->query($sql);

        $sql = "DELETE FROM " . DB_PREFIX . "arajanlat_option_value WHERE arajanlat_id = '" .$arajanlat_id. "'";
        $query = $this->db->query($sql);
	}
	
	public function getArajanlats($data,$total=false) {



			if ($total) {
				$sql = "SELECT COUNT(DISTINCT a.arajanlat_id) as total FROM " . DB_PREFIX . "arajanlat a ";
                $sql .= " LEFT JOIN " .DB_PREFIX. "arajanlat_to_store a2s ON (a2s.arajanlat_id=a.arajanlat_id)";
                $sql .= " WHERE a2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

			} else {
				$sql = "SELECT a.*, CONCAT(c.firstname,' ', c.lastname) AS customer FROM " . DB_PREFIX . "arajanlat a";
				$sql .= " LEFT JOIN " .DB_PREFIX. "customer c ON (c.customer_id=a.customer_id)";
				$sql .= " LEFT JOIN " .DB_PREFIX. "arajanlat_to_store a2s ON (a2s.arajanlat_id=a.arajanlat_id)";
				$sql .= " WHERE a2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

				$sort_data = array(
					'name',
					'arajanlat_id',
					'customer',
					'date_added',
					'date_modified',
				);

                if (!empty($data['filter_azonosito_arajanlat'])) {
                    $sql .= " AND a.arajanlat_id = '" .(int)$data['filter_azonosito_arajanlat'] . "'";
                }
				if (!empty($data['filter_name_arajanlat'])) {
					$sql .= " AND LCASE(a.name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name_arajanlat'])) . "%'";
				}
				if (!empty($data['filter_customer_arajanlat']) && $data['filter_customer_arajanlat']) {
					$sql .= " AND LCASE(CONCAT(c.firstname,' ',c.lastname)) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_customer_arajanlat'])) . "%'";
				}
				if (!empty($data['filter_date_added_arajanlat'])) {
					$sql .= " AND DATE(a.date_added) = DATE('" . $this->db->escape($data['filter_date_added_arajanlat']) . "')";
				}
				if (!empty($data['filter_date_modified_arajanlat'])) {
					$sql .= " AND DATE(a.date_modified) = DATE('" . $this->db->escape($data['filter_date_modified_arajanlat']) . "')";
				}

                if (!empty($data['autocomplete']) && !empty($data['filter_customer_arajanlat']) ) {
                    $sql .= " GROUP BY a.customer_id ";
                }

                if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
					$sql .= " ORDER BY `" . $data['sort']."`";
				} else {
					$sql .= " ORDER BY arajanlat_id";
				}

				if (isset($data['order']) && ($data['order'] == 'DESC')) {
					$sql .= " DESC";
				} else {
					$sql .= " ASC";
				}


				if (isset($data['start']) || isset($data['limit'])) {
					if ($data['start'] < 0) {
						$data['start'] = 0;
					}

					if ($data['limit'] < 1) {
						$data['limit'] = 20;
					}

					$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				}



			}

			$query = $this->db->query($sql);

		if ($total) {
			return $query->row['total'];
		} else {
			if ($query->num_rows > 0) {
				foreach ($query->rows as $arajanlat_key=>$arajanlat) {
					$sql = "SELECT * FROM " .DB_PREFIX. "arajanlat_product WHERE arajanlat_id = '" .$arajanlat['arajanlat_id']. "'";



					$query_products = $this->db->query($sql);

					if ($query_products->num_rows > 0 ){
						$query->rows[$arajanlat_key]['products'] = $query_products->rows;

						foreach($query_products->rows as $arajanlat_product_key=>$arajanlat_product) {
							$sql = "SELECT * FROM " .DB_PREFIX. "arajanlat_option WHERE arajanlat_product_id = '" .$arajanlat_product['arajanlat_product_id']. "'";
							$query_options = $this->db->query($sql);

							if ($query_options->num_rows > 0) {
								$query->rows[$arajanlat_key]['products'][$arajanlat_product_key]['options'] = $query_options->rows;

								foreach($query_options->rows as $arajanlat_option_key=>$arajanlat_option) {
									$sql = "SELECT * FROM " .DB_PREFIX. "arajanlat_option_value WHERE arajanlat_option_id = '" .$arajanlat_option['arajanlat_option_id']. "'";
									$query_option_values = $this->db->query($sql);

									if ($query_option_values->num_rows > 0) {
										$query->rows[$arajanlat_key]['products'][$arajanlat_product_key]['options'][$arajanlat_option_key]['value'] = $query_option_values->rows;

									}

								}
							}

						}
					}
				}
			 	return $query->rows;

			} else return false;
		}
	}

	public function getArajanlat($arajanlat_id) {
		$sql  = "SELECT a.*, CONCAT(c.firstname,' ',c.lastname) AS customer_name, c.email  FROM " .DB_PREFIX. "arajanlat a ";
		$sql .= " LEFT JOIN " .DB_PREFIX. "customer c ON (c.customer_id=a.customer_id)";
		$sql .= "  WHERE a.arajanlat_id = '" .$arajanlat_id. "'";

		$query = $this->db->query($sql);

		if ($query->num_rows > 0 ){

			$sql  = "SELECT ap.*, p.image, p.price, pd.name, p.model, p.megyseg FROM " .DB_PREFIX. "arajanlat_product ap ";
			$sql .= " LEFT JOIN ".DB_PREFIX. "product p ON (p.product_id=ap.product_id)";
			$sql .= " LEFT JOIN ".DB_PREFIX. "product_description pd ON (pd.product_id=ap.product_id)";
			$sql .= " LEFT JOIN " .DB_PREFIX. "arajanlat_to_store a2s ON (a2s.arajanlat_id=ap.arajanlat_id)";

			$sql .= " WHERE ap.arajanlat_id = '" .$arajanlat_id. "'";
			$sql .= " AND pd.language_id = '" .(int)$this->config->get('config_language_id'). "'";
			$sql .= " AND a2s.store_id 	 = '" . (int)$this->config->get('config_store_id') . "'";

			$query_products = $this->db->query($sql);

			if ($query_products->num_rows > 0 ){
				$query->row['products'] = $query_products->rows;

				foreach($query_products->rows as $arajanlat_product_key=>$arajanlat_product) {
                    $mennyiseg = false;

                    $query->row['products'][$arajanlat_product_key]['option_mennyiseg'] = false;

					$sql = "SELECT ao.*, od.name AS option_name FROM " .DB_PREFIX. "arajanlat_option ao ";
					$sql .= " LEFT JOIN ".DB_PREFIX. "option_description od ON (od.option_id=ao.option_id)";

					$sql .= " WHERE ao.arajanlat_product_id = '" .$arajanlat_product['arajanlat_product_id']. "'";
					$sql .= "  AND od.language_id = '" .(int)$this->config->get('config_language_id'). "'";
					$query_options = $this->db->query($sql);

					if ($query_options->num_rows > 0) {
						$query->row['products'][$arajanlat_product_key]['options'] = $query_options->rows;

						foreach($query_options->rows as $arajanlat_option_key=>$arajanlat_option) {
                            $sql = "SELECT * FROM ".DB_PREFIX."option WHERE option_id='".$arajanlat_option['option_id']."'";
                            $query_option_type = $this->db->query($sql);

							$sql = "SELECT aov.*, ovd.name AS option_value_name FROM " .DB_PREFIX. "arajanlat_option_value aov ";
							$sql .= " LEFT JOIN ".DB_PREFIX. "option_value_description ovd ON (ovd.option_value_id=aov.option_value_id)";

							$sql .= " WHERE aov.arajanlat_option_id = '" .$arajanlat_option['arajanlat_option_id']. "'";




                            if ($query_option_type->num_rows > 0 && (   $query_option_type->row['type'] == 'select' ||
                                                                        $query_option_type->row['type'] == 'radio' ||
                                                                        $query_option_type->row['type'] == 'checkbox' ||
                                                                        $query_option_type->row['type'] == 'checkbox_qty' ||
                                                                        $query_option_type->row['type'] == 'image') ) {
							    $sql .= "  AND ovd.language_id = '" .(int)$this->config->get('config_language_id'). "'";
                            }

							$query_option_values = $this->db->query($sql);

							if ($query_option_values->num_rows > 0) {
								$query->row['products'][$arajanlat_product_key]['options'][$arajanlat_option_key]['value'] = $query_option_values->rows;
                                foreach($query_option_values->rows as $query_option_values_key=>$sor) {
                                    if ($sor['mennyiseg']) {
                                        $mennyiseg = true;
                                    }

                                    if ($sor['product_option_value_ertek']) {
                                        $query->row['products'][$arajanlat_product_key]['options'][$arajanlat_option_key]['value'][$query_option_values_key]['option_value_name'] = $sor['product_option_value_ertek'];
                                    }
                                    $sql = "SELECT price, price_prefix  FROM " .DB_PREFIX. "product_option_value ";
                                    $sql .= " WHERE product_option_value_id = '" .(int)$sor['product_option_value_id'] . "'";
                                    $query_pov = $this->db->query($sql);

                                    $query->row['products'][$arajanlat_product_key]['options'][$arajanlat_option_key]['value'][$query_option_values_key]['price'] = isset($query_pov->row['price']) ? $query_pov->row['price']: '';
                                    $query->row['products'][$arajanlat_product_key]['options'][$arajanlat_option_key]['value'][$query_option_values_key]['price_prefix'] = isset($query_pov->row['price_prefix']) ? $query_pov->row['price_prefix'] : '';
                                }
                                $query->row['products'][$arajanlat_product_key]['option_mennyiseg'] = $mennyiseg;
							}

						}
					}

				}
			}
		}
		return $query->row;
	}


	public function getCustomer($customer_id) {
		$customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id='" . $customer_id . "'");

		return $customer_query->row;


	}

	public function addArajanlat($data) {
		$sql = "INSERT INTO " .DB_PREFIX. "arajanlat SET
			`name`			= '".$data['name']."',
			`customer_id`	= '".$data['customer_id']."',
            `date_added`	= NOW(),
            szallito_neve              = '".$data['szallito_neve']."',
            szallito_varos             = '".$data['szallito_varos']."',
            szallito_utca              = '".$data['szallito_utca']."',
            szallito_orszag            = '".$data['szallito_orszag']."',
            szallito_adoszam           = '".$data['szallito_adoszam']."',
            szallito_bank              = '".$data['szallito_bank']."',
            szallito_bankszamla_szam   = '".$data['szallito_bankszamla_szam']."',
            szallito_bankszamla_swift  = '".$data['szallito_bankszamla_swift']."',
            szallito_bankszamla_iban   = '".$data['szallito_bankszamla_iban']."',
            vevo_neve                  = '".$data['vevo_neve']."',
            vevo_varos                 = '".$data['vevo_varos']."',
            vevo_utca                  = '".$data['vevo_utca']."',
            vevo_orszag                = '".$data['vevo_orszag']."',
            vevo_adoszam               = '".$data['vevo_adoszam']."',
            vevo_bank                  = '".$data['vevo_bank']."',
            vevo_bankszamla_szam       = '".$data['vevo_bankszamla_szam']."',
            ajanlat_kelte              = '".$data['ajanlat_kelte']."',
            ajanlat_szallitasi_hatarido= '".$data['ajanlat_szallitasi_hatarido']."',
            ajanlat_fizetesi_hatarido  = '".$data['ajanlat_fizetesi_hatarido']."',
            ajanlat_fizetesi_mod       = '".$data['ajanlat_fizetesi_mod']."',
            ajanlat_kuldo_neve         = '".$data['ajanlat_kuldo_neve']."',
            ajanlat_kuldo_telefon      = '".$data['ajanlat_kuldo_telefon']."',
            ajanlat_kuldo_email        = '".$data['ajanlat_kuldo_email']."',
            ajanlat_megszolitas        = '".$data['ajanlat_megszolitas']."',
            ajanlat_altalanos_szoveg_felul= '".$data['ajanlat_altalanos_szoveg_felul']."',
            ajanlat_altalanos_szoveg_alul= '".$data['ajanlat_altalanos_szoveg_alul']."',
            `total`         = '".$data['total']."'";

		$query = $this->db->query($sql);
		$arajanlat_id	= $this->db->getLastId();


        $sql = "INSERT INTO " .DB_PREFIX. "arajanlat_to_store SET
                    store_id        = '".(int)$this->config->get('config_store_id')."',
                    arajanlat_id    = '".(int)$arajanlat_id."'";
        $query_store = $this->db->query($sql);

        if (isset($data['products']) ) {
			foreach($data['products'] as $product) {
				$sql = "INSERT INTO " .DB_PREFIX. "arajanlat_product
					SET arajanlat_id 	= '" .$arajanlat_id. "',
						product_id		= '" .$product['product_id']. "',
						mennyiseg		= '" .$product['mennyiseg']. "',
						egysegar		= '" .$product['egysegar']. "',
						ertek		    = '" .$product['ertek']. "',
						ertek_osszesen	= '" .$product['ertek_osszesen']. "',
						szazalek	    = '" .$product['szazalek']. "',
						product_ertek_osszesen	= '" .$product['product_ertek_osszesen']. "'";
				$query 	= $this->db->query($sql);
				$arajanlat_product_id	= $this->db->getLastId();



                if (isset($product['options'])) {
					foreach($product['options'] as $option) {
						$sql = "INSERT INTO " .DB_PREFIX. "arajanlat_option
							SET arajanlat_product_id 	= '" .$arajanlat_product_id. "',
							    option_id		        = '" .$option['option_id']. "',
							    product_option_id       = '" .$option['product_option_id']. "'";
						$query 	= $this->db->query($sql);
						$arajanlat_option_id	= $this->db->getLastId();

						if (isset($option['value']) ) {
							foreach($option['value'] as $value) {
								$sql = "INSERT INTO " .DB_PREFIX. "arajanlat_option_value
									SET arajanlat_option_id         = '" .$arajanlat_option_id. "',
									 	option_value_id 	        = '" .$value['option_value_id']. "',
									 	mennyiseg 			        = '" .$value['mennyiseg']. "',
									 	product_option_value_id     = '" .$value['product_option_value_id']. "',
									 	product_option_value_ertek  = '" .$value['product_option_value_ertek']. "',
									 	egysegar                    = '" .$value['egysegar']. "',
									 	ertek                       = '" .$value['ertek']. "',
									 	szazalek                    = '" .$value['szazalek']. "'";
								$query 	= $this->db->query($sql);
							}

						}
					}
				}
			}
		}

	}

    public function toPdf($html) {



        require_once(DIR_SYSTEM . 'pdfmaker/dompdf_config.inc.php');

        $dompdf = new DOMPDF();
        $dompdf ->set_paper("A4");
        //$html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');

        $dompdf->load_html($html);
        $dompdf->render();

        $this->language->load('account/arajanlat');
        $text_page = $this->language->get('text_page');

        $canvas = $dompdf->get_canvas();
        $font = Font_Metrics::get_font("DejaVu Sans","helvetica", "bold");
        $canvas->page_text(35, 18, $text_page.": {PAGE_NUM} / {PAGE_COUNT}", $font, 9, array(0,0,0));


        $dompdf->stream("arajanlat.pdf");


    }

}
?>