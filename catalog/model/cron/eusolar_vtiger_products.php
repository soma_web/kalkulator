<?php
class ModelCronEusolarVtigerProducts extends Model {

	public function getChangeProducts() {
        $vissza = array();

        $sql = "CREATE TABLE if not exists product_to_webshop (
            `product_to_webshop_id`	    int(11),
            `hivo_tabla`	            varchar(62),
            `productid`	                int(11),
            `hivo_esemeny`	            varchar(32)
            ) engine=MyISAM default charset=UTF8";
        $query = $this->db_crm->query($sql);

        $sql = "SELECT * FROM product_to_webshop  WHERE hivo_tabla LIKE 'vtiger_products' OR hivo_tabla LIKE 'vtiger_service' ";
        $sql .= " ORDER BY product_to_webshop_id ASC ";

        $query = $this->db_crm->query($sql);
        if ($query->rows) {
            foreach($query->rows as $value) {
                $vissza[] = array(
                    'productid' => $value['productid'],
                    'product_to_webshop_id' => $value['product_to_webshop_id'],
                    'hivo_esemeny' => $value['hivo_esemeny']
                );
            }
        }

        return $vissza;

	}

    public function WriteProducts($vtiger_productid,$price_update=true) {

        $vissza = array(
            'felvitel'  => 0,
            'modositas'  => 0
        );

        $query_vtiger = $this->getVtigerProduct($vtiger_productid);
        if (!$query_vtiger) {
            return $vissza;
        }

        /* ---------------- Termék  ---------------- */

        $sql = "SELECT product_id FROM ".DB_PREFIX."product WHERE crm_product_id='".$vtiger_productid."'";
        $query = $this->db->query($sql);
        if ($query->num_rows == 0) {
            $sql = "INSERT INTO ".DB_PREFIX."product SET crm_product_id='".$vtiger_productid."'";
            $siker = $this->db->query($sql);
            $product_id = $this->db->getLastId();

            $sql = "INSERT INTO ".DB_PREFIX."product_to_store SET
                        product_id  ='".$product_id."',
                        store_id    = 0";
            $this->db->query($sql);

            $vissza['felvitel'] += 1;

        } else {
            $product_id = $query->row['product_id'];
            $vissza['modositas'] += 1;

            $sql = "SELECT product_id FROM ".DB_PREFIX."product_to_store
                                WHERE   product_id = '".$product_id."' AND
                                        store_id    = 0";
            $query = $this->db->query($sql);
            if (!$query->row) {
                $sql = "INSERT INTO ".DB_PREFIX."product_to_store SET
                                    product_id  ='".$product_id."',
                                    store_id    = 0";
                $siker = $this->db->query($sql);
            }
        }
        /* ---------------- Termék  vége ---------------- */





        /* ---------------- Termék neve ---------------- */

        $sql = "SELECT product_id FROM ".DB_PREFIX."product_description WHERE product_id = '".$product_id."' AND language_id='".(int)$this->config->get('config_language_id')."'";
        $query = $this->db->query($sql);
        if (!$query->row) {
            $sql = "INSERT INTO ".DB_PREFIX."product_description SET
                product_id              = '" . (int)$product_id . "',
			    language_id             = '" . (int)$this->config->get('config_language_id') . "',
			    `name`                    = '" . $this->db->escape($query_vtiger['productname']) . "'";
        } else {
            $sql = "UPDATE ".DB_PREFIX."product_description SET name = '" . $this->db->escape($query_vtiger['productname']) . "'
                        WHERE product_id   = '" . (int)$product_id . "' AND
                        	  language_id  = '" . (int)$this->config->get('config_language_id') . "'";
        }
        $siker = $this->db->query($sql);
        /* ---------------- Termék neve vége ---------------- */



        /* ---------------- Kategória ---------------- */
        if (!empty($query_vtiger['productcategoryid'])) {

            $sql = "SELECT category_id FROM ".DB_PREFIX."category WHERE integracios_kod = '".$query_vtiger['productcategoryid']."'";
            $query = $this->db->query($sql);
            if ($query->row) {
                $category_id = $query->row['category_id'];
                $sql = "SELECT category_id FROM ".DB_PREFIX."category_to_store
                            WHERE   category_id = '".$category_id."' AND
                                    store_id    = 0";
                $query = $this->db->query($sql);
                if (!$query->row) {
                    $sql = "INSERT INTO ".DB_PREFIX."category_to_store SET
                                category_id  ='".$category_id."',
                                store_id    = 0";
                    $siker = $this->db->query($sql);
                }

            } else {
                $sql = "INSERT INTO ".DB_PREFIX."category SET
                            `status`          = 1,
                            integracios_kod = '".$query_vtiger['productcategoryid']."',
                            `top`             = 1,
                            `column`          = 1,
                            date_added      = NOW(),
                            date_modified   = NOW()";
                $siker = $this->db->query($sql);
                $category_id = $this->db->getLastId();

                $sql = "INSERT INTO ".DB_PREFIX."category_to_store SET
                    category_id  ='".$category_id."',
                    store_id    = 0";
                $siker = $this->db->query($sql);
            }

            $sql = "SELECT category_id FROM ".DB_PREFIX."category_description WHERE category_id = '".$category_id."' AND language_id='".(int)$this->config->get('config_language_id')."'";
            $query = $this->db->query($sql);
            if ($query->row) {
                $sql = "UPDATE ".DB_PREFIX."category_description SET     `name` = '".$query_vtiger['productcategory']."'
                            WHERE   category_id = '".$category_id."' AND language_id='".(int)$this->config->get('config_language_id')."'";

                $siker = $this->db->query($sql);
            } else {
                $sql = "INSERT INTO ".DB_PREFIX."category_description SET
                            `name` = '".$query_vtiger['productcategory']."',
                            category_id = '".$category_id."',
                             language_id='".(int)$this->config->get('config_language_id')."'";
                $siker = $this->db->query($sql);
            }

            $sql = "SELECT * FROM ".DB_PREFIX."product_to_category WHERE product_id='".$product_id."' AND category_id = '".$category_id."'";
            $query = $this->db->query($sql);
            if (!$query->row) {
                $sql = "INSERT INTO ".DB_PREFIX."product_to_category SET
                                product_id  ='".$product_id."',
                                category_id ='".$category_id."'";
                $siker = $this->db->query($sql);
            }
        }
        /* ---------------- Kategória vége ---------------- */





        /* ---------------- Gyártó  ---------------- */
        $sql_manufacturer = "";
        if (!empty($query_vtiger['manufacturer'])) {
            $sql = "SELECT * FROM ".DB_PREFIX."manufacturer WHERE name LIKE '".$query_vtiger['manufacturer']."'";
            $query = $this->db->query($sql);
            if ($query->row) {
                $manufacturer_id = $query->row['manufacturer_id'];
            } else {
                $sql = "INSERT INTO ".DB_PREFIX."manufacturer SET name = '".$this->db->escape($query_vtiger['manufacturer'])."'";
                $siker = $this->db->query($sql);
                $manufacturer_id = $this->db->getLastId();

                $sql = "INSERT INTO ".DB_PREFIX."manufacturer_to_store SET
                            manufacturer_id ='".$manufacturer_id."',
                            store_id        = 0";
                $siker = $this->db->query($sql);
            }

            $sql_manufacturer = " ,manufacturer_id       = '".$manufacturer_id."'";
        }
        /* ---------------- Gyártó vége ---------------- */




        $sql = "UPDATE ".DB_PREFIX."product SET
                        model       = '".$query_vtiger['productcode']."',";

        $sql .= $price_update ? " price = '".$query_vtiger['unit_price']."'," : '';

        $sql .= "       weight      = '".$query_vtiger['weight']."',
                        quantity    = '".$query_vtiger['qtyinstock']."',
                        status      = '".$query_vtiger['status']."',
                        crm_tipus   = '".$query_vtiger['crm_tipus']."',
                        crm_category_id   = '".(!empty($query_vtiger['productcategoryid']) ? $query_vtiger['productcategoryid'] : '')."',
                        crm_product_id   = '".(!empty($vtiger_productid) ? $vtiger_productid : '')."'";
        $sql .= $sql_manufacturer;
        $sql .= " WHERE product_id = '".$product_id. "'";
        $siker = $this->db->query($sql);

        $this->cache->delete('product');
        $this->cache->delete('category');
        $this->cache->delete('category_all');

        return $vissza;
    }

    public function getVtigerProduct($vtiger_productid) {

        $vissza = array();

        $sql = "SELECT p.*, pcf.* FROM vtiger_products p
                    LEFT JOIN vtiger_productcf pcf ON (pcf.productid=p.productid)
                    WHERE p.productid = '" . $vtiger_productid . "'";
        $query = $this->db_crm->query($sql);

        if ($query->row) {
            if (!empty($query->row['productcategory'])) {
                $sql = "SELECT productcategoryid FROM vtiger_productcategory WHERE productcategory LIKE '" . $query->row['productcategory'] . "'";
                $query_category = $this->db_crm->query($sql);
                $query->row['productcategoryid'] = $query_category->row['productcategoryid'];
            } else {
                $query->row['productcategoryid'] = '';
            }

            $query->row['status'] = (!empty($query->row['cf_2395']) ? 1 : 0);
            $query->row['crm_tipus'] = 1;
            $vissza = $query->row;

        } else  {
            $sql = "SELECT s.*, scf.* FROM vtiger_service s
                    LEFT JOIN vtiger_servicecf scf ON (scf.serviceid=s.serviceid)
                    WHERE s.serviceid = '".$vtiger_productid."'";
            $query = $this->db_crm->query($sql);

            if ($query->row) {
                if (!empty($query->row['servicecategory'])) {
                    $sql = "SELECT servicecategoryid FROM vtiger_servicecategory WHERE servicecategory LIKE '" . $query->row['servicecategory'] . "'";
                    $query_category = $this->db_crm->query($sql);
                    $query->row['servicecategoryid'] = $query_category->row['servicecategoryid'];
                } else {
                    $query->row['servicecategoryid'] = '';
                }

                $vissza = array(
                    'productname'       => $query->row['servicename'],
                    'productcategory'   => $query->row['servicecategory'],
                    'productcategoryid' => $query->row['servicecategoryid'],
                    'manufacturer'      => '',
                    'productcode'       => '',
                    'unit_price'        => $query->row['unit_price'],
                    'weight'            => 0,
                    'qtyinstock'        => 0,
                    'status'            => !empty($query->row['cf_2534']) ? 1 : 0,
                    'crm_tipus'         => 2
                );
            } else {
                $sql = "SELECT * FROM vtiger_pricebook
                    WHERE pricebookid = '".$vtiger_productid."'";
                $query = $this->db_crm->query($sql);

                if ($query->row) {

                    $vissza = array(
                        'productname'       => $query->row['bookname'],
                        'productcategory'   => "Csomagok",
                        'productcategoryid' => '-1',
                        'manufacturer'      => '',
                        'productcode'       => '',
                        'unit_price'        => 0,
                        'weight'            => 0,
                        'qtyinstock'        => 0,
                        'status'            => 0,
                        'crm_tipus'         => 3

                    );
                }
            }
        }


        return $vissza;

    }


    public function csomagarakAktualizalasa($vtiger_productid) {
        $sql = "SELECT product_id, price FROM ".DB_PREFIX."product WHERE crm_product_id='".$vtiger_productid."'";
        $query = $this->db->query($sql);
        if ($query->row) {
            $sql = "UPDATE ".DB_PREFIX."product_package SET price='".$query->row['price']."' WHERE package_id='".$query->row['product_id']."'";
            $siker = $this->db->query($sql);
        }
        $sql = "SELECT product_id FROM ".DB_PREFIX."product_package WHERE package_id='".$query->row['product_id']."'";
        $query = $this->db->query($sql);
        if ($query->rows) {
            foreach($query->rows as $value) {
                $sql = "SELECT quantity, price FROM ".DB_PREFIX."product_package WHERE product_id='".$value['product_id']."'";
                $query_package = $this->db->query($sql);
                if ($query_package->rows) {
                    $ar = 0;
                    foreach($query_package->rows as $value_package) {
                        $ar += (int)$value_package['quantity']*(int)$value_package['price'];
                    }
                    echo '';
                    $sql = "UPDATE ".DB_PREFIX."product SET price='".$ar."' WHERE product_id='".$value['product_id']."'";
                    $siker = $this->db->query($sql);
                }
            }
        }
    }

    public function deleteTrigger($product_to_webshop_id) {
        $sql = "DELETE FROM product_to_webshop WHERE product_to_webshop_id='".$product_to_webshop_id."'";
        $siker = $this->db_crm->query($sql);

    }



}
?>