<?php
class ModelCronAltalanos extends Model {



    public function cronDocumentation($cron_id,$cron_allapot_id='') {
        if (!$cron_allapot_id) {
            $sql = "INSERT INTO ".DB_PREFIX."cron_allapot SET
                date_start  = NOW(),
                cron_id     = '".$cron_id."'";
            $siker = $this->db->query($sql);
            echo '<br>Könyvelés beszúrás siker: '.$siker."<br>";

            $cron_allapot_id = $this->db->getLastId();
            $_SESSION['cron_allapot_id'] = $cron_allapot_id;

            return $cron_allapot_id;

        } else {
            $sql = "UPDATE ".DB_PREFIX."cron_allapot SET
                date_end  = NOW()
                WHERE  cron_allapot_id='".$cron_allapot_id."'";
            $siker = $this->db->query($sql);
            $this->addFuttatasEredmeny('Futás vége!');
        }
        $sql = "DELETE FROM ".DB_PREFIX."cron_allapot WHERE date_start < NOW() - INTERVAL 30 DAY";
        $siker = $this->db->query($sql);


    }

    public function getCrmId() {

        $crm_id = '';
        if (count($this->db_crm)) {
            $sql = "SELECT id FROM vtiger_crmentity_seq";
            $query = $this->db_crm->query($sql);
            $crm_id = (int)$query->row['id']+1;

            $sql = "UPDATE vtiger_crmentity_seq SET id='".$crm_id."'";
            $query = $this->db_crm->query($sql);
        }

        return $crm_id;
    }

    public function getCrmNo($module='SalesOrder') {
        $vissza = false;

        if (count($this->db_crm)) {
            $sql = "SELECT num_id, prefix, cur_id FROM vtiger_modentity_num WHERE semodule='".$module."'";
            $query = $this->db_crm->query($sql);
            if ($query->num_rows > 0) {
                $cur_id = (int)$query->row['cur_id']+1;
                $sql = "UPDATE vtiger_modentity_num SET cur_id='".$cur_id."' WHERE semodule='".$module."'";
                $query_update = $this->db_crm->query($sql);
                $vissza = $query->row['prefix'].(string)$cur_id;
            }
        }

        return $vissza;
    }

    public function getModtrackerId() {

        $crm_id = '';
        if (count($this->db_crm)) {
            $sql = "SELECT id FROM vtiger_modtracker_basic_seq";
            $query = $this->db_crm->query($sql);
            $crm_id = (int)$query->row['id']+1;

            $sql = "UPDATE vtiger_modtracker_basic_seq SET id='".$crm_id."'";
            $query = $this->db_crm->query($sql);
        }
        return $crm_id;
    }

    public function addEntity($data) {
        $sql = "INSERT INTO vtiger_crmentity SET
                                    crmid       = '".$data['crmid']."',
                                    smcreatorid = '".$data['crmentity']['smcreatorid']."',
                                    smownerid   = '".$data['crmentity']['smownerid']."',
                                    modifiedby  = '".$data['crmentity']['modifiedby']."',
                                    setype      = '".$data['module']."',
                                    description = '".$data['crmentity']['description']."',
                                    createdtime = NOW(),
                                    modifiedtime= NOW(),
                                    viewedtime  = '".$data['crmentity']['viewedtime']."',
                                    status      = '".$data['crmentity']['status']."',
                                    version     = '".$data['crmentity']['version']."',
                                    presence    = '".$data['crmentity']['presence']."',
                                    deleted     = '".$data['crmentity']['deleted']."',
                                    label       = '".$data['crmentity']['label']."',
                                    cf_view     = '".$data['crmentity']['cf_view']."',
                                    cf_viewname = '".$data['crmentity']['cf_viewname']."'";

        $siker = $this->db_crm->query($sql);


        $modtracker_id = $this->getModtrackerId();
        $sql = "INSERT INTO vtiger_modtracker_basic SET
                                    id          = '".$modtracker_id."',
                                    crmid       = '".$data['crmid']."',
                                    `module`    = '".$data['module']."',
                                    whodid      = '".$data['modbasic']['whodid']."',
                                    status      = '".$data['modbasic']['status']."',
                                    changedon   = NOW() ";
        $siker = $this->db_crm->query($sql);



    }

    public function addRelation($data) {
        $sql = "INSERT INTO vtiger_crmentityrel SET
                                    crmid       = '".$data['crmid']."',
                                    `module`     = '".$data['module']."',
                                    relcrmid    = '".'Valami'."',
                                    relmodule   = 'Kerdoivek'";

        $modtracker_id = $this->getModtrackerId();
        $sql = "INSERT INTO vtiger_modtracker_relations SET
                                    id              = '".$modtracker_id."',
                                    targetmodule    = 'Kerdoivek',
                                    targetid        = '".'ID'."',
                                    changedon       = NOW()";
    }




    public function sendCrmToSzamlazz($order,$szamla_tipusa='',$data) {

        if (empty($order['account']['accountname']) && empty($order['contact']['firstname']) && empty($order['contact']['lastname'])) {
            $this->addFuttatasEredmeny('Hiányos kapcsolat!');
            return false;
        }


        $settings = $this->config->get("szamlazz_hu_module");

        $this->addFuttatasEredmeny('Számla létrehozás elindítva.');


        isset($settings['szamlazz_hu_eszamla']) && $settings['szamlazz_hu_eszamla'] == 0 ? $settings['szamlazz_hu_eszamla'] = 'false' : $settings['szamlazz_hu_eszamla'] = 'true';
        isset($settings['szamlazz_hu_szamla_download']) && $settings['szamlazz_hu_szamla_download'] == 0 ? $settings['szamlazz_hu_szamla_download'] = 'false' : $settings['szamlazz_hu_szamla_download'] = 'true';
        isset($settings['fizetve']) && $settings['fizetve'] == 0 ? $settings['fizetve'] = 'false' : $settings['fizetve'] = 'true';
        $szamla_tipusa_dokumentalashoz = '';

        if (!empty($szamla_tipusa)) {
            $settings['vegszamla']      = 'false';
            $settings['dijbekero']      = 'false';
            $settings['eloleg_szamla']  = 'false';
            $settings['szallitolevel']  = 'false';

            if ($szamla_tipusa == 1) {
                $settings['megjegyzes']                 = $settings['szamla_megjegyzes'].' '.$settings['megjegyzes'];
                $settings['szamlazz_hu_email_targy']    = !empty($settings['szamla_email_targya'])  ? $settings['szamla_email_targya']  : $settings['szamlazz_hu_email_targy'];
                $settings['szamlazz_hu_email_szoveg']   = !empty($settings['szamla_email_szovege']) ? $settings['szamla_email_szovege'] : $settings['szamla_email_szovege'];

                if (isset($order['fizetve'])) {
                    $settings['fizetve'] = 'false';
                    if ($order['fizetve'] == "Kiegyenlített") {
                        $settings['fizetve'] = 'true';
                    }
                } else {
                    $settings['fizetve']        = 'true';
                }
                $szamla_tipusa_dokumentalashoz = "Szamla";


            } elseif ($szamla_tipusa == 2) {
                $settings['dijbekero']      = 'true';
                $settings['fizetve']        = 'false';
                $settings['megjegyzes']     = $settings['dijbekero_megjegyzes'].' '.$settings['megjegyzes'];
                $settings['szamlazz_hu_email_targy']    = !empty($settings['dijbekero_email_targya'])  ? $settings['dijbekero_email_targya']  : $settings['dijbekero_email_targya'];
                $settings['szamlazz_hu_email_szoveg']   = !empty($settings['dijbekero_email_szovege']) ? $settings['dijbekero_email_szovege'] : $settings['dijbekero_email_szovege'];
                $szamla_tipusa_dokumentalashoz = "dijbekero";


            } elseif ($szamla_tipusa == 3) {
                $settings['eloleg_szamla']  = 'true';
                $settings['fizetve']        = 'true';
                $settings['megjegyzes']     = $settings['eloleg_szamla_megjegyzes'].' '.$settings['megjegyzes'];
                $settings['szamlazz_hu_email_targy']    = !empty($settings['eloleg_email_targya'])  ? $settings['eloleg_email_targya']  : $settings['eloleg_email_targya'];
                $settings['szamlazz_hu_email_szoveg']   = !empty($settings['eloleg_email_szovege']) ? $settings['eloleg_email_szovege'] : $settings['eloleg_email_szovege'];
                $szamla_tipusa_dokumentalashoz = "eloleg_szamla";


            } elseif ($szamla_tipusa == 4) {
                $settings['vegszamla']      = 'true';
                if (isset($order['fizetve'])) {
                    $settings['fizetve'] = 'false';
                    if ($order['fizetve'] == "Kiegyenlített") {
                        $settings['fizetve'] = 'true';
                    }
                } else {
                    $settings['fizetve']        = 'true';
                }
                $settings['megjegyzes']     = $settings['vegszamla_megjegyzes'].' '.$settings['megjegyzes'];
                $settings['szamlazz_hu_email_targy']    = !empty($settings['vegszamla_email_targya'])  ? $settings['vegszamla_email_targya']  : $settings['vegszamla_email_targya'];
                $settings['szamlazz_hu_email_szoveg']   = !empty($settings['vegszamla_email_szovege']) ? $settings['vegszamla_email_szovege'] : $settings['vegszamla_email_szovege'];
                $szamla_tipusa_dokumentalashoz = "vegszamla";


            } elseif ($szamla_tipusa == 5) {
                $settings['szallitolevel']  = 'true';
                $settings['fizetve']        = 'false';
                $settings['megjegyzes']     = $settings['szallitolevel_megjegyzes'].' '.$settings['megjegyzes'];
                $settings['szamlazz_hu_email_targy']    = !empty($settings['szallitolevel_email_targya'])  ? $settings['szallitolevel_email_targya']  : $settings['szallitolevel_email_targya'];
                $settings['szamlazz_hu_email_szoveg']   = !empty($settings['szallitolevel_email_szovege']) ? $settings['szallitolevel_email_szovege'] : $settings['szallitolevel_email_szovege'];
                $szamla_tipusa_dokumentalashoz = "szallitolevel";


                if (!empty($order['shipping']['bill_code']) && !empty($order['shipping']['bill_city']) && !empty($order['shipping']['bill_street'])) {
                    $order['payment']['bill_code']      = $order['shipping']['bill_code'];
                    $order['payment']['bill_city']      = $order['shipping']['bill_city'];
                    $order['payment']['bill_street']    = $order['shipping']['bill_street'];
                }

            } elseif ($szamla_tipusa == 6) {
                $settings['szallitolevel']  = 'true';
                $settings['fizetve']        = 'true';
                $settings['megjegyzes']     = $settings['szamla_megjegyzes'].' '.$settings['megjegyzes'];
                $szamla_tipusa_dokumentalashoz = "Webshop rendelés bankkártyás fizetéssel";

            }




        } else {
            empty($settings['eloleg_szamla'])   ? $settings['eloleg_szamla']    = 'false' : $settings['eloleg_szamla']  = 'true';
            empty($settings['vegszamla'])       ? $settings['vegszamla']        = 'false' : $settings['vegszamla']      = 'true';
            empty($settings['dijbekero'])       ? $settings['dijbekero']        = 'false' : $settings['dijbekero']      = 'true';
            empty($settings['szallitolevel'])   ? $settings['szallitolevel']    = 'false' : $settings['szallitolevel']  = 'true';
        }


        if (isset($settings['peldany_szam']) && $settings['peldany_szam'] == "") {
            $settings['peldany_szam'] = 0;
        }

        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<xmlszamla xmlns="http://www.szamlazz.hu/xmlszamla" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.szamlazz.hu/xmlszamla xmlszamla.xsd ">';
        $xml .= '<beallitasok>';
        $xml .= '    <felhasznalo>'.$settings['szamlazz_hu_username'].'</felhasznalo>';
        $xml .= '    <jelszo>'.$settings['szamlazz_hu_password'].'</jelszo>';
        $xml .= '    <eszamla>'.$settings['szamlazz_hu_eszamla'].'</eszamla>';
        $xml .= '    <kulcstartojelszo>'.$settings['szamlazz_hu_kulcstartojelszo'].'</kulcstartojelszo>';
        $xml .= '    <szamlaLetoltes>'.$settings['szamlazz_hu_szamla_download'].'</szamlaLetoltes>';
        $xml .= '    <szamlaLetoltesPld>'.$settings['peldany_szam'].'</szamlaLetoltesPld>';
        $xml .= '    <valaszVerzio>'.$settings['szamlazz_hu_valaszverzio'].'</valaszVerzio>';
        $xml .= '    <aggregator>'.$settings['szamlazz_hu_aggregator'].'</aggregator>';
        $xml .= '</beallitasok>';


        $this->addFuttatasEredmeny('Számla létrehozás : beallitasok OK.');

        $fizmod         = $data['fizmod'];
        $penznem        = $order['currency_id'] == 1 ? "HUF" : "EUR";
        $szamlaNyelve   = $data['szamlaNyelve'];
        $arfolyam       = $data['arfolyam'];


        $xml .= '<fejlec>';
        $xml .= '   <keltDatum>'.date('Y-m-d').'</keltDatum>';
        $xml .= '   <teljesitesDatum>'.(!empty($order['teljesites']) ? $order['teljesites'] : date('Y-m-d')).'</teljesitesDatum>';
        $xml .= '   <fizetesiHataridoDatum>'.$order['duedate'].'</fizetesiHataridoDatum>';
        $xml .= '   <fizmod>'.$fizmod.'</fizmod>';
        $xml .= '   <penznem>'.$penznem.'</penznem>';
        $xml .= '   <szamlaNyelve>'.$szamlaNyelve.'</szamlaNyelve>';
        $xml .= '   <megjegyzes>'.$settings['megjegyzes'].'</megjegyzes>';
        $xml .= '   <arfolyamBank>MNB</arfolyamBank>';
        $xml .= '   <arfolyam>'.$arfolyam.'</arfolyam>';
        $xml .= '   <rendelesSzam>'.$order['salesorder_no'].'</rendelesSzam>';
        $xml .= '   <elolegszamla>'.$settings['eloleg_szamla'].'</elolegszamla>';
        $xml .= '   <vegszamla>'.$settings['vegszamla'].'</vegszamla>';
        $xml .= '   <dijbekero>'.$settings['dijbekero'].'</dijbekero>';
        $xml .= '   <szallitolevel>'.$settings['szallitolevel'].'</szallitolevel>';
        $xml .= '   <szamlaszamElotag>'.$settings['szamlazz_hu_elotag'].'</szamlaszamElotag>';
        $xml .= '   <fizetve>'.$settings['fizetve'].'</fizetve>';
        $xml .= '</fejlec>';

        $this->addFuttatasEredmeny('Számla létrehozás : fejléc OK.');

        if ($penznem == 'HUF') {
            $bank           = $settings['szamlazz_hu_bank'];
            $bankszamlaszam = $settings['szamlazz_hu_bankszamlaszam'];
        } elseif ($penznem == 'EUR') {
            $bank           = !empty($settings['szamlazz_hu_bank_eur']) ? $settings['szamlazz_hu_bank_eur'] : $settings['szamlazz_hu_bank'];
            $bankszamlaszam = !empty($settings['szamlazz_hu_bankszamlaszam_eur']) ? $settings['szamlazz_hu_bankszamlaszam_eur'] : $settings['szamlazz_hu_bankszamlaszam'];
        } else {
            $bank           = $settings['szamlazz_hu_bank'];
            $bankszamlaszam = $settings['szamlazz_hu_bankszamlaszam'];
        }

        $xml .= '<elado>';
        $xml .= '   <bank>'.$bank.'</bank>';
        $xml .= '   <bankszamlaszam>'.$bankszamlaszam.'</bankszamlaszam>';
        $xml .= '   <emailReplyto>'.$settings['szamlazz_hu_email_reply_to'].'</emailReplyto>';
        $xml .= '   <emailTargy>'.$settings['szamlazz_hu_email_targy'].'</emailTargy>';
        $xml .= '   <emailSzoveg>'.$settings['szamlazz_hu_email_szoveg'].'</emailSzoveg>';
        $xml .= '</elado>';


        if (!empty($order['account']['accountname'])) {
            $vevo_neve = $order['account']['accountname'];

        } elseif (!empty($order['contact']['firstname']) && !empty($order['contact']['lastname'])) {
            $vevo_neve = $order['contact']['lastname'].' '.$order['contact']['firstname'];

        } elseif (!empty($order['contact']['firstname']) ) {
            $vevo_neve = $order['contact']['firstname'];

        } else  {
            $vevo_neve = $order['contact']['lastname'];
        }


        $vevo_adoszam = !empty($order['account']['adoszam']) ? $order['account']['adoszam'] : '';

        $xml .= '<vevo>';
        $xml .= '   <nev>'      .$vevo_neve.'</nev>';
        $xml .= '   <irsz>'     .$order['payment']['bill_code'].'</irsz>';
        $xml .= '   <telepules>'.$order['payment']['bill_city'].'</telepules>';
        $xml .= '   <cim>'      .$order['payment']['bill_street'].'</cim>';
        $xml .= '   <email>'    .$order['contact']['email'].'</email>';
        $xml .= '   <sendEmail>true</sendEmail>';
        $xml .= '   <adoszam>' .$vevo_adoszam.'</adoszam>';
        $xml .= '</vevo>';



        $xml .= '<tetelek>';

        foreach($order['products'] as $product) {
            $megnevezes = $product['name'];
            $mennyiseg = $product['quantity'];
            $mennyisegiEgyseg    = "db";
            $nettoEgysegar      = (int)$product['listprice'];
            $nettoErtek     = $nettoEgysegar*$mennyiseg;

            $afaErtek = 0;
            $afakulcsok_ossz = 0;
            if ((int)$product['tax1'] > 0 ) {
                $afakulcs = (float)$product['tax1'];
                $afakulcsok_ossz += $afakulcs;
                $afaErtek += $nettoErtek*(1+$afakulcs/100)-$nettoErtek;

            }
            if ((int)$product['tax2'] > 0 ) {
                $afakulcs = (float)$product['tax2'];
                $afakulcsok_ossz += $afakulcs;
                $afaErtek += $nettoErtek*(1+$afakulcs/100)-$nettoErtek;

            }
            if ((int)$product['tax3'] > 0 ) {
                $afakulcs = (float)$product['tax3'];
                $afakulcsok_ossz += $afakulcs;
                $afaErtek += $nettoErtek*(1+$afakulcs/100)-$nettoErtek;

            }
            if ((int)$product['tax4'] > 0 ) {
                $afakulcs = (float)$product['tax4'];
                $afakulcsok_ossz += $afakulcs;
                $afaErtek += $nettoErtek*(1+$afakulcs/100)-$nettoErtek;

            }
            if ((int)$product['tax5'] > 0 ) {
                $afakulcs = (float)$product['tax5'];
                $afakulcsok_ossz += $afakulcs;
                $afaErtek += $nettoErtek*(1+$afakulcs/100)-$nettoErtek;

            }
            /*$afakulcs = 27;
            $afaErtek       = $nettoErtek*(1+$afakulcs/100)-$nettoErtek;*/

            $bruttoErtek    = $nettoErtek*(1+$afakulcsok_ossz/100);
            $comment        = $product['comment'];


            $xml .= '<tetel>';
            $xml .= '   <megnevezes>'.$megnevezes.'</megnevezes>';
            $xml .= '   <mennyiseg>'.$mennyiseg.'</mennyiseg>';
            $xml .= '   <mennyisegiEgyseg>'.$mennyisegiEgyseg.'</mennyisegiEgyseg>';
            $xml .= '   <nettoEgysegar>'.$nettoEgysegar.'</nettoEgysegar>';
            $xml .= '   <afakulcs>'.$afakulcsok_ossz.'</afakulcs>';
            $xml .= '   <nettoErtek>'.$nettoErtek.'</nettoErtek>';
            $xml .= '   <afaErtek>'.$afaErtek.'</afaErtek>';
            $xml .= '   <bruttoErtek>'.$bruttoErtek.'</bruttoErtek>';
            $xml .= '   <megjegyzes>'.$comment.'</megjegyzes>';
            $xml .= '</tetel>';
        }



        $xml .= '</tetelek>';

        $this->addFuttatasEredmeny('Számla létrehozás : tetelek OK.');

        $xml .= '</xmlszamla>';


        $output = fopen(DIR_ARUHAZ.'csv/szamlazz_'.$order['salesorderid'].'.xml', 'w');


        $siker = fputs($output, $xml);
        fclose($output);


        // cookie file teljes elérési útja a szerveren
        $cookie_file = str_replace('szamlazz_'.$order['salesorderid'].'.xml','szamlazz_cookie.txt',$output) ;
        // ebbe a fájlba menti a pdf-et, ha az xml-ben kértük
        $pdf_file = str_replace('szamlazz_'.$order['salesorderid'].'.xml','szamla'.$order['salesorderid'].'.pdf',$output) ;
        // ezt az xml fájlt küldi a számla agentnek
        $xmlfile = DIR_ARUHAZ.'csv/szamlazz_'.$order['salesorderid'].'.xml';
        // a számla agentet ezen az urlen lehet elérni
        $agent_url = 'https://www.szamlazz.hu/szamla/';
        // ha kérjük a számla pdf-et, akkor legyen true
        $szamlaletoltes = true;

        // ha még nincs --> létrehozzuk a cookie file-t --> léteznie kell, hogy a CURL írhasson bele
        if (!file_exists($cookie_file)) {
            $siker = file_put_contents($cookie_file, '');
        }

        $ch = curl_init($agent_url);


        // A curl hívás esetén tanúsítványhibát kaphatunk az SSL tanúsítvány valódiságától
        // függetlenül, ez az alábbi CURL paraméter állítással kiküszöbölhető,
        // ilyenkor nincs külön SSL ellenőrzés:
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // POST-ban küldjük az adatokat
        curl_setopt($ch, CURLOPT_POST, true);
        // Kérjük a HTTP headert a válaszba, fontos információk vannak benne
        curl_setopt($ch, CURLOPT_HEADER, true);
        // változóban tároljuk a válasz tartalmát, nem írjuk a kimenetbe
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


        $ver = phpversion();
        if (substr($ver,0,3) > "5.4") {
            $args['action-xmlagentxmlfile'] = new CurlFile($xmlfile, 'application/xml','szamlazz.hu');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, array('action-xmlagentxmlfile'=>'@' . $xmlfile));
        }

        // 30 másodpercig tartjuk fenn a kapcsolatot (ha valami bökkenő volna)
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        // Itt állítjuk be, hogy az érkező cookie a $cookie_file-ba kerüljön mentésre
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);

        // Ha van már cookie file-unk, és van is benne valami, elküldjük a Számlázz.hu-nak
        if (file_exists($cookie_file) && filesize($cookie_file) > 0) {
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
        }

        // elküldjük a kérést a Számlázz.hu felé, és eltároljuk a választ
        $agent_response = curl_exec($ch);


        // kiolvassuk a curl-ból volt-e hiba
        $http_error = curl_error($ch);

        // lekérjük a válasz HTTP_CODE-ját, ami ha 200, akkor a http kommunikáció rendben volt
        // ettől még egyáltalán nem biztos, hogy a számla elkészült
        $agent_http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);

        // a válasz egy byte kupac, ebből az első "header_size" darab byte lesz a header
        $header_size = curl_getinfo($ch,CURLINFO_HEADER_SIZE);

        // a header tárolása, ebben lesznek majd a számlaszám, bruttó nettó összegek, errorcode, stb.
        $agent_header = substr($agent_response, 0, $header_size);

        // a body tárolása, ez lesz a pdf, vagy szöveges üzenet
        $agent_body = substr( $agent_response, $header_size );

        curl_close($ch);

        // a header soronként tartalmazza az információkat, egy tömbbe teszük a külön sorokat
        $header_array = explode("\n", $agent_header);

        $volt_hiba = false;


        foreach ($header_array as $val) {
            if (substr($val, 0, strlen('szlahu')) === 'szlahu') {

                // megvizsgáljuk, hogy volt-e hiba
                if (substr($val, 0, strlen('szlahu_error:')) === 'szlahu_error:') {
                    // sajnos volt
                    $volt_hiba = true;
                    $agent_error = substr($val, strlen('szlahu_error:'));
                    $this->addFuttatasEredmeny('szlahu_error:'.urldecode($agent_error));

                }
                if (substr($val, 0, strlen('szlahu_error_code:')) === 'szlahu_error_code:') {
                    // sajnos volt
                    $volt_hiba = true;
                    $agent_error_code = substr($val, strlen('szlahu_error_code:'));
                    $this->addFuttatasEredmeny('szlahu_error_code:'.$agent_error_code);;

                }
                if (substr($val, 0, strlen('szlahu_szamlaszam:')) === 'szlahu_szamlaszam:') {
                    $szamlaszam = trim(substr($val, strlen('szlahu_szamlaszam:')));

                }
            }
        }

        if ($volt_hiba) {
            $this->addFuttatasEredmeny('Agent válasz: '.urldecode($agent_body));
            return false;
        }else{
            $this->addFuttatasEredmeny('Agent válasz: '.$szamla_tipusa_dokumentalashoz.'sikeresen továbbítva!');
            return $szamlaszam;
        }

    }

    public function sendCrmToSzamlazzKiegyenlitve($data) {

        $settings = $this->config->get("szamlazz_hu_module");

        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<xmlszamlakifiz xmlns="http://www.szamlazz.hu/xmlszamlakifiz" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.szamlazz.hu/xmlszamlakifiz xmlszamlakifiz.xsd ">';
        $xml .= '<beallitasok>';

        $xml .= '    <felhasznalo>'.$settings['szamlazz_hu_username'].'</felhasznalo>';
        $xml .= '    <jelszo>'.$settings['szamlazz_hu_password'].'</jelszo>';

        $xml .= '    <szamlaszam>'.$data['szamlaszam'].'</szamlaszam>';
        $xml .= '    <additiv>false</additiv>';
        $xml .= '    <aggregator></aggregator>';
        $xml .= '</beallitasok>';

        $xml .= '<kifizetes>';
        $xml .= '    <datum>'.date('Y-m-d').'</datum>';
        $xml .= '    <jogcim>átutalás</jogcim>';
        $xml .= '    <osszeg>'.(int)$data['total'].'</osszeg>';
        $xml .= '</kifizetes>';

        $xml .= '</xmlszamlakifiz>';


        $output = fopen(DIR_ARUHAZ.'csv/szamlazz_kiegyenlitve_'.$data['invoiceid'].'.xml', 'w');
        $siker = fputs($output, $xml);
        fclose($output);

        $xmlfile = DIR_ARUHAZ.'csv/szamlazz_kiegyenlitve_'.$data['invoiceid'].'.xml';



        // cookie file teljes elérési útja a szerveren
        $cookie_file = str_replace('szamlazz_kiegyenlitve_'.$data['invoiceid'].'.xml','szamlazz_kiegyenlitve_cookie.txt',$output) ;
        // ebbe a fájlba menti a pdf-et, ha az xml-ben kértük
        $pdf_file = str_replace('szamlazz_kiegyenlitve_'.$data['invoiceid'].'.xml','szamlazz_kiegyenlitve_'.$data['invoiceid'].'.pdf',$output) ;
        // ezt az xml fájlt küldi a számla agentnek
        //$xmlfile = DIR_ARUHAZ.'csv/szamlazz_ki_'.$data['invoiceid'].'.xml';



        // a számla agentet ezen az urlen lehet elérni


        $agent_url = 'https://www.szamlazz.hu/szamla/';
        $ch = curl_init($agent_url);


        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $ver = phpversion();
        if (substr($ver,0,3) > "5.4") {
            $args['action-szamla_agent_kifiz'] = new CurlFile($xmlfile, 'application/xml','szamlazz.hu');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, array('action-szamla_agent_kifiz'=>'@' . $xmlfile));
        }
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);





        // Itt állítjuk be, hogy az érkező cookie a $cookie_file-ba kerüljön mentésre
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);

        // Ha van már cookie file-unk, és van is benne valami, elküldjük a Számlázz.hu-nak
        if (file_exists($cookie_file) && filesize($cookie_file) > 0) {
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
        }

        $agent_response = curl_exec($ch);


        $agent_http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);

        // a válasz egy byte kupac, ebből az első "header_size" darab byte lesz a header
        $header_size = curl_getinfo($ch,CURLINFO_HEADER_SIZE);

        // a header tárolása, ebben lesznek majd a számlaszám, bruttó nettó összegek, errorcode, stb.
        $agent_header = substr($agent_response, 0, $header_size);

        // a body tárolása, ez lesz a pdf, vagy szöveges üzenet
        $agent_body = substr( $agent_response, $header_size );

        curl_close($ch);

        // a header soronként tartalmazza az információkat, egy tömbbe teszük a külön sorokat
        $header_array = explode("\n", $agent_header);

        $volt_hiba = false;

        $kintelevoseg = 0;
        foreach ($header_array as $val) {
            if (substr($val, 0, strlen('szlahu')) === 'szlahu') {

                // megvizsgáljuk, hogy volt-e hiba
                if (substr($val, 0, strlen('szlahu_error:')) === 'szlahu_error:') {
                    // sajnos volt
                    $volt_hiba = true;
                    $agent_error = substr($val, strlen('szlahu_error:'));
                    $this->addFuttatasEredmeny('szlahu_error:'.urldecode($agent_error));

                }
                if (substr($val, 0, strlen('szlahu_error_code:')) === 'szlahu_error_code:') {
                    // sajnos volt
                    $volt_hiba = true;
                    $agent_error_code = substr($val, strlen('szlahu_error_code:'));
                    $this->addFuttatasEredmeny('szlahu_error_code:'.$agent_error_code);;

                }
                if (substr($val, 0, strlen('szlahu_kintlevoseg:')) === 'szlahu_kintlevoseg:') {
                    $kintelevoseg = trim(substr($val, strlen('szlahu_kintlevoseg:')));

                }
            }
        }

        if ($volt_hiba) {
            $this->addFuttatasEredmeny('Agent válasz: '.urldecode($agent_body));
            return false;
        } else{
            $this->addFuttatasEredmeny('Sikeresen továbbítva!<br>Agent válasz - kintlevőség : '.$kintelevoseg.'<br>');
            return true;
        }


    }

    public function addFuttatasEredmeny($eredmeny) {
        if (!empty($_SESSION['cron_allapot_id'])) {
            $sql = "SELECT futtatas_eredmenyek FROM " . DB_PREFIX . "cron_allapot WHERE cron_allapot_id='" . $_SESSION['cron_allapot_id'] . "'";
            $query = $this->db->query($sql);

            if ($query->row) {
                $eredmeny = $query->row['futtatas_eredmenyek'] . '<br>' . $eredmeny;
            }
            $sql = "UPDATE " . DB_PREFIX . "cron_allapot SET futtatas_eredmenyek = '" . $this->db->escape($eredmeny) . "' WHERE cron_allapot_id='" . $_SESSION['cron_allapot_id'] . "'";
            $siker = $this->db->query($sql);
        }

    }

    public function getLineItemId() {
        $sql = "SELECT lineitem_id FROM vtiger_inventoryproductrel ORDER BY lineitem_id DESC LIMIT 0,1";
        $query = $this->db_crm->query($sql);

        return !empty($query->row['lineitem_id']) ? $query->row['lineitem_id']+1 : 1;
    }

    public function getInputElements($cron_id) {
        $vissza = array();

        $sql  = "SELECT input_elements FROM ".DB_PREFIX."cron WHERE cron_id='".$cron_id."'";
        $query = $this->db->query($sql);
        if ($query->num_rows > 0) {
            $vissza = unserialize($query->row['input_elements']);
        }

        return $vissza;

    }



}