<?php
class ModelCronEusolarVtigerCsomag extends Model {

	public function getChangeCsomag() {
        $vissza = array();

        $sql = "CREATE TABLE if not exists vtiger_pricebookproductrel_trigger (
            `pricebookid`	    int(19),
            `productid`	        int(19),
            `price`	            decimal(27,8),
            `esemeny`	        varchar(25)
            ) engine=MyISAM default charset=UTF8";
        $query = $this->db_crm->query($sql);

        $sql = "SELECT * FROM vtiger_pricebookproductrel_trigger";
        $query = $this->db_crm->query($sql);

        return $query->rows;

	}

    public function getProduct($vtiger_productid) {
        $sql = "SELECT p.*, pcf.* FROM vtiger_products p
                    LEFT JOIN vtiger_productcf pcf ON (pcf.productid=p.productid)
                    WHERE p.productid = '".$vtiger_productid."'";
        $query = $this->db_crm->query($sql);

        if (!$query->row) {
            $sql = "SELECT s.*, scf.* FROM vtiger_service s
                    LEFT JOIN vtiger_servicecf scf ON (scf.serviceid=s.serviceid)
                    WHERE s.serviceid = '".$vtiger_productid."'";
            $query = $this->db_crm->query($sql);
        }

        return $query->row;
    }

    public function addCsomagTetel($pricebookid,$productid,$price) {
        $this->load->model("cron/altalanos");

        $sql = "SELECT product_id, price FROM ".DB_PREFIX."product WHERE crm_product_id='".$pricebookid."'";
        $query = $this->db->query($sql);
        if ($query->num_rows > 0) {
            $product_id = $query->row['product_id'];
        } else {
            return false;
        }

        $sql = "SELECT product_id, price FROM ".DB_PREFIX."product WHERE crm_product_id='".$productid."'";
        $query = $this->db->query($sql);
        if ($query->num_rows > 0) {
            $package_id = $query->row['product_id'];
        } else {
            return false;
        }



        $sql = "SELECT product_id FROM ".DB_PREFIX."product_package WHERE package_id='".$package_id."' AND product_id='".$product_id."'";
        $query = $this->db->query($sql);
        if ($query->num_rows == 0) {
            $sql = "INSERT INTO ".DB_PREFIX."product_package SET
                        package_id  = '".$package_id."',
                        product_id  = '".$product_id."',
                        price       = '".$price."',
                        quantity    = 1 ";
            $siker = $this->db->query($sql);
        }
        $sql = "UPDATE ".DB_PREFIX."product_package set price='".$price."' WHERE package_id='".$package_id."'";
        $query = $this->db->query($sql);
        $this->model_cron_altalanos->addFuttatasEredmeny('Csomagban az árak aktualizálva. (product_id - ár):  ' .$package_id.' - '. $price);

    }

    public function getCsomagTetelek($pricebookid) {
        $sql = "SELECT pricebookid, productid, listprice AS price FROM vtiger_pricebookproductrel WHERE pricebookid='".$pricebookid."'";
        $query = $this->db_crm->query($sql);
        return $query->rows;


    }

    public function csomagarAktualizalasa($pricebookid) {
        $sql = "SELECT product_id, price FROM ".DB_PREFIX."product WHERE crm_product_id='".$pricebookid."'";
        $query = $this->db->query($sql);
        if ($query->row) {
            foreach($query->rows as $value) {
                $sql = "SELECT quantity, price FROM ".DB_PREFIX."product_package WHERE product_id='".$value['product_id']."'";
                $query_package = $this->db->query($sql);
                if ($query_package->rows) {
                    $ar = 0;
                    foreach($query_package->rows as $value_package) {
                        $ar += (int)$value_package['quantity']*(int)$value_package['price'];
                    }
                    echo '';
                    $sql = "UPDATE ".DB_PREFIX."product SET price='".$ar."' WHERE product_id='".$value['product_id']."'";
                    $siker = $this->db->query($sql);
                }
            }
        }
    }

    public function deleteCsomagTetel($pricebookid,$productid) {

        $sql = "SELECT product_id FROM ".DB_PREFIX."product WHERE crm_product_id='".$pricebookid."'";
        $query = $this->db->query($sql);
        if ($query->num_rows > 0) {
            $package_id = $query->row['product_id'];
        } else {
            return false;
        }

        $sql = "SELECT product_id FROM ".DB_PREFIX."product WHERE crm_product_id='".$productid."'";
        $query = $this->db->query($sql);
        if ($query->num_rows > 0) {
            $product_id = $query->row['product_id'];
        } else {
            return false;
        }



        $sql = "SELECT product_id FROM ".DB_PREFIX."product_package WHERE package_id='".$product_id."' AND product_id='".$package_id."'";
        $query = $this->db->query($sql);
        if ($query->num_rows > 0) {
            $sql = "DELETE FROM ".DB_PREFIX."product_package WHERE
                        package_id  = '".$product_id."' AND
                        product_id  = '".$package_id."'";
            $siker = $this->db->query($sql);
        }

    }

    public function deleteTrigger($pricebookid,$productid) {
        $sql = "DELETE FROM vtiger_pricebookproductrel_trigger WHERE pricebookid='".$pricebookid."' AND productid='".$productid."'";
        $siker = $this->db_crm->query($sql);

    }


}
?>