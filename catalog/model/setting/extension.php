<?php
class ModelSettingExtension extends Model {
    function getExtensions($type) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "extension WHERE `type` = '" . $this->db->escape($type) . "'");
        if (isset($_SESSION['no_modules'])) {
            $query_no_module = $_SESSION['no_modules'];
        } else {
            $query_no_module = $this->db->query("SELECT * FROM " . DB_PREFIX . "no_module");
            $_SESSION['no_modules'] = $query_no_module;
        }

        if ($query_no_module->num_rows > 0) {
            foreach ($query_no_module->rows as $result) {
               $no_module[] = $result['module_name'];
            }

            foreach($query->rows as $key=>$value){
                if (in_array($value['code'], $no_module)){
                    unset($query->rows[$key]);
                }
            }
        }

        return $query->rows;
    }

    function getPaymentsElhelyezkedes() {
        $files = glob(DIR_APPLICATION . 'controller/payment/payment_elhelyezkedes/*.php');
        $vissza = array();
        if ($files) {
            foreach ($files as $file) {
                $vissza[] = basename($file, '.php');
            }
        }

        return $vissza;
    }

    function getExtensionsCustomer($type) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_extension WHERE `customer_id` = '" . $type . "'");
        $vissza=array();
        $vissza['num_rows']=$query->num_rows;
        $vissza['rows']=$query->rows;
        return $vissza;
    }
    public function getNoModule() {
        $no_module = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "no_module");

        foreach ($query->rows as $result) {
            $no_module[] = $result['module_name'];
        }

        return $no_module;
    }
}
?>