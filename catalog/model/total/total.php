<?php
class ModelTotalTotal extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		$this->load->language('total/total');


        $total_voucher = 0;
        if (isset($this->session->data['vouchers']) && $this->session->data['vouchers']) {
            foreach ($this->session->data['vouchers'] as $voucher) {
                $total_voucher += $voucher['amount'];
            }
        }

		$total_data[] = array(
			'code'       => 'total',
			'title'      => $this->language->get('text_total'),
			'text'       => $this->currency->format(max(0, $total+$total_voucher)),
			'value'      => max(0, $total+$total_voucher),
			'sort_order' => $this->config->get('total_sort_order')
		);
	}
}
?>