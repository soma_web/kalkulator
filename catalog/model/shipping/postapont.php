<?php
class ModelShippingPostaPont extends Model {
	function getQuote($address, $fizetesek="") {
		$this->language->load('shipping/postapont');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('postapont_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if (!$this->config->get('postapont_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}
		
		$method_data = array();

		if ($status) {
            $fizetesi_mod = array();

            if ($fizetesek){
                foreach($fizetesek as $value) {
                    if ( $this->config->get("postapont_payment_".$value['code']) &&  $this->config->get("postapont_payment_".$value['code']) == 1){
                        $fizetesi_mod[] = array(
                            'fizetesi_mod' => $value['code']
                        );
                    }
                }
            }

			$quote_data = array();
			$free = false;

			/* Check if sub total is over the configured value */
			if ($this->cart->getSubTotal() >= $this->config->get('postapont_free_total')) {
				$free = true;
			}
			
			$content = file_get_contents(DIR_TEMPLATE."/default/template/shipping/postapont.tpl");
			$content = str_replace(
				array(
				  '[GOOGELMAPSAPIKEY]',
				  '[DISABLE_POSTAPONT]',
				  '[DISABLE_MOL]',
				  '[DISABLE_COOP]',
				  '[DISABLE_AUTOMATA]'
				),
				array(
					($this->config->get('postapont_google_maps_api_key')?'&key='.$this->config->get('postapont_google_maps_api_key'):''),
					($this->config->get('status_postapont')?'':"ppapi.setMarkers('10_posta', false);"),
					($this->config->get('mol_status')?'':"ppapi.setMarkers('20_molkut', false);"),
					($this->config->get('automata_status')?'':"ppapi.setMarkers('30_csomagautomata', false);"),
					($this->config->get('coop_status')?'':"ppapi.setMarkers('50_coop', false);")
				),
				$content
			);

            $quote_data = array();
            if ($this->config->get('postapont_tax_class_id') > 0) {
                $query_tax = $this->db->query("SELECT * FROM " . DB_PREFIX . "tax_class WHERE tax_class_id = '" . (int)$this->config->get('postapont_tax_class_id') . "'");
            }
            $ado_title = isset($query_tax->row['title']) ? $query_tax->row['title'] :"";

			if ($free) {
				$quote_data['postapont'] = array(
					'code'         => 'postapont.postapont',
					'title'        => $content,
					'cost'         => 0.00,
                    'fizetesi_mod'  => $fizetesi_mod,
					'tax_class_id' => 0,
                    'text'         => $this->currency->format(0.00),
                    'netto_text'   => $this->currency->format($this->config->get('postapont_cost')),
                    'ado_title'    => $ado_title,

                );

			} else {
				$quote_data['postapont'] = array(
					'code'         => 'postapont.postapont',
					'title'        => $content,
					'cost'         => $this->config->get('postapont_cost'),
                    'fizetesi_mod'  => $fizetesi_mod,
					'tax_class_id' => $this->config->get('postapont_tax_class_id'),
                    'text'         => $this->currency->format($this->tax->calculate($this->config->get('postapont_cost'), $this->config->get('postapont_tax_class_id'), $this->config->get('config_tax'))),
                    'netto_text'   => $this->currency->format($this->config->get('postapont_cost')),
                    'ado_title'    => $ado_title,

                );
			}			
			
			
			$method_data = array(
				'code'       => 'postapont',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
                'fizetesi_mod'  => $fizetesi_mod,
				'sort_order' => $this->config->get('postapont_sort_order'),
                'netto_text'   => $this->currency->format($this->config->get('postapont_cost')),
                'error'      => false
			);
		}

		return $method_data;
	}
}
?>