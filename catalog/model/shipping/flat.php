<?php
class ModelShippingFlat extends Model {
	function getQuote($address, $fizetesek="") {
		$this->load->language('shipping/flat');
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('flat_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
	
		if (!$this->config->get('flat_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();
	
		if ($status) {

            $fizetesi_mod = array();

            if ($fizetesek){
                foreach($fizetesek as $value) {
                    if ( $this->config->get("flat_payment_".$value['code']) &&  $this->config->get("flat_payment_".$value['code']) == 1){
                        $fizetesi_mod[] = array(
                            'fizetesi_mod' => $value['code']
                        );
                    }
                }
            }

			$quote_data = array();
            if ($this->config->get('flat_tax_class_id') > 0) {
                $query_tax = $this->db->query("SELECT * FROM " . DB_PREFIX . "tax_class WHERE tax_class_id = '" . (int)$this->config->get('flat_tax_class_id') . "'");
            }
            $ado_title = isset($query_tax->row['title']) ? $query_tax->row['title'] :"";

      		$quote_data['flat'] = array(
        		'code'         => 'flat.flat',
        		'title'        =>  $this->config->get('flat_text') != null ? $this->config->get('flat_text') :  $this->language->get('text_description'),
        		'cost'         => $this->config->get('flat_cost'),
        		'tax_class_id' => $this->config->get('flat_tax_class_id'),
                'fizetesi_mod' => $fizetesi_mod,
				'text'         => $this->currency->format($this->tax->calculate($this->config->get('flat_cost'), $this->config->get('flat_tax_class_id'), $this->config->get('config_tax'))),
                'netto_text'   => $this->currency->format($this->config->get('flat_cost')),
				'ado_title'    => $ado_title,
				'mail'         => $this->config->get('flat_mail')

			);

      		$method_data = array(
        		'code'          => 'flat',
        		'title'         => $this->config->get('flat_header') != null ? $this->config->get('flat_header') :  $this->language->get('text_description'),
        		'quote'         => $quote_data,
        		'fizetesi_mod'  => $fizetesi_mod,
				'sort_order'    => $this->config->get('flat_sort_order'),
        		'error'         => false
      		);
		}
	
		return $method_data;
	}
}
?>