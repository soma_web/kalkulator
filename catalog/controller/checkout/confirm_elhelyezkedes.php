<?php 
class ControllerCheckoutConfirmElhelyezkedes extends Controller {
	public function index() {
		$redirect = '';
		

		
		// Validate if payment address has been set.
		$this->load->model('account/address');
		
		if ($this->customer->isLogged() && isset($this->session->data['payment_address_id'])) {
			$payment_address = $this->model_account_address->getAddress($this->session->data['payment_address_id']);		
		} elseif (isset($this->session->data['guest'])) {
			$payment_address = $this->session->data['guest']['payment'];
		}	
				
		if (empty($payment_address)) {
			$redirect = $this->url->link('checkout/checkout_elhelyezkedes', '', 'SSL');
		}			
		
		// Validate if payment method has been set.	
		if (!isset($this->session->data['elhelyezkedes']['payment_method'])) {
			$redirect = $this->url->link('checkout/checkout_elhelyezkedes', '', 'SSL');
        }


        if (!$redirect) {

            $this->language->load('checkout/checkout_elhelyezkedes');




            $this->load->model('checkout/order');

            $this->data['totals'] = $this->model_checkout_order->getTotalsElhelyezkedes();

            $this->data['penznem'] =  " ". $this->currency->getFizetoeszkoz();

           // $this->data['total'] = 0;

            $this->load->model('tool/image');

            foreach ($this->cart_elhelyezkedes->getProducts() as $product) {

                $total =  $this->model_checkout_order->getProductTotalElhelyezkedes($product['key']);



                //$this->data['total'] += $product['total'];
                if ($product['image']) {
                    $image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
                } else {
                    $image = '';
                }

                $this->data['products'][] = array(
                    'key'           => $product['key'],
                    'thumb'         => $image,
                    'name'          => $product['name'],
                    'quantity'      => $product['quantity'],
                    'total'         => $total,
                    'kiemelesek'    => $product['kiemelesek'],
                    'href'          => $this->url->link('account/product/kiemeles', 'product_id=' . $product['product_id'])
                );

            }


            $this->data['column_name'] = $this->language->get('column_name');
            $this->data['total_title'] = $this->language->get('total_title');

            if ($this->config->get('megjelenit_form_admin_model') == 1) {
                $this->data['column_model'] = $this->language->get('column_model');
            } elseif ($this->config->get('megjelenit_form_admin_cikkszam') == 1) {
                $this->data['column_model'] = $this->language->get('column_cikkszam');
            } elseif ( $this->config->get('megjelenit_form_admin_cikkszam2') == 1) {
                $this->data['column_model'] = $this->language->get('column_cikkszam');
            } else {
                $this->data['column_model'] = "";
            }


            $this->data['column_quantity'] = $this->language->get('column_quantity');
            $this->data['column_price'] = $this->language->get('column_price');
            $this->data['column_total'] = $this->language->get('column_total');


            $this->data['payment'] = $this->getChild('payment/payment_elhelyezkedes/' . $this->session->data['elhelyezkedes']['payment_method']['code']);

        } else {
            $this->data['redirect'] = $redirect;
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/confirm_elhelyezkedes.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/checkout/confirm_elhelyezkedes.tpl';
        } else {
            $this->template = 'default/template/checkout/confirm_elhelyezkedes.tpl';
        }

        $this->response->setOutput($this->render());
    }
}
?>