<?php 
class ControllerCheckoutCartElhelyezkedes extends Controller {
	private $error = array();
	
	public function index() {
        $this->language->load('checkout/cart_elhelyezkedes');

  	}


    public function addMentes() {


        $this->load->model('account/product');
        $this->model_account_product->editProductKiemelesek($this->request->get['product_id'], $this->request->request);

    }

    public function addElhelyezkedes() {
        $this->language->load('checkout/cart_elhelyezkedes');

        $json = array();

        if (isset($this->request->request['product_id'])) {
            $product_id = $this->request->request['product_id'];
        } else {
            $json['error']['product_id'] = $this->language->get('ures_product_id');
        }

        if ($_REQUEST['elhelyezkedes_id'] == 0 && $_REQUEST['kiemelesek_id'] == 0) {
            $json['error']['ures_id'] = $this->language->get('text_error_ures_id');
        }

        if ($_REQUEST['idoszak'] == 0) {
            $json['error']['ures_idoszak'] = $this->language->get('text_error_ures_idoszak');
        }

        if ($_REQUEST['datum_kezdo'] == "0000-00-00" || empty($_REQUEST['datum_kezdo']) ) {
            $json['error']['ures_datum_kezdo'] = $this->language->get('ures_datum_kezdo');
        }

        if ($_REQUEST['datum_vege'] == "0000-00-00" || empty($_REQUEST['datum_vege']) ) {
            $json['error']['ures_datum_vege'] = $this->language->get('ures_datum_vege');
        }



        //$this->load->model('catalog/product');


        if (!$json) {
            $this->session->data['cart_elhelyezkedes'][$product_id][$this->request->request['sor']]['brutto'] = $this->request->request['brutto'];
            $this->session->data['cart_elhelyezkedes'][$product_id][$this->request->request['sor']]['elhelyezkedes_id'] = $this->request->request['elhelyezkedes_id'];
            $this->session->data['cart_elhelyezkedes'][$product_id][$this->request->request['sor']]['elhelyezkedes_neve'] = $this->request->request['elhelyezkedes_neve'];
            $this->session->data['cart_elhelyezkedes'][$product_id][$this->request->request['sor']]['elhelyezkedes_netto'] = $this->request->request['elhelyezkedes_netto'];
            $this->session->data['cart_elhelyezkedes'][$product_id][$this->request->request['sor']]['elhelyezkedes_brutto'] = $this->request->request['elhelyezkedes_brutto'];
            $this->session->data['cart_elhelyezkedes'][$product_id][$this->request->request['sor']]['elhelyezkedes_alcsoport_id'] = $this->request->request['elhelyezkedes_alcsoport_id'];
            $this->session->data['cart_elhelyezkedes'][$product_id][$this->request->request['sor']]['elhelyezkedes_alcsoport_neve'] = $this->request->request['elhelyezkedes_alcsoport_neve'];
            $this->session->data['cart_elhelyezkedes'][$product_id][$this->request->request['sor']]['elhelyezkedes_alcsoport_netto'] = $this->request->request['elhelyezkedes_alcsoport_netto'];
            $this->session->data['cart_elhelyezkedes'][$product_id][$this->request->request['sor']]['elhelyezkedes_alcsoport_brutto'] = $this->request->request['elhelyezkedes_alcsoport_brutto'];
            $this->session->data['cart_elhelyezkedes'][$product_id][$this->request->request['sor']]['kiemelesek_id'] = $this->request->request['kiemelesek_id'];
            $this->session->data['cart_elhelyezkedes'][$product_id][$this->request->request['sor']]['kiemelesek_neve'] = $this->request->request['kiemelesek_neve'];
            $this->session->data['cart_elhelyezkedes'][$product_id][$this->request->request['sor']]['kiemelesek_netto'] = $this->request->request['kiemelesek_netto'];
            $this->session->data['cart_elhelyezkedes'][$product_id][$this->request->request['sor']]['kiemelesek_brutto'] = $this->request->request['kiemelesek_brutto'];
            $this->session->data['cart_elhelyezkedes'][$product_id][$this->request->request['sor']]['idoszak'] = $this->request->request['idoszak'];
            $this->session->data['cart_elhelyezkedes'][$product_id][$this->request->request['sor']]['idoszak_mennyiseg'] = $this->request->request['idoszak_mennyiseg'];
        }
        $json['sor'] = $this->request->request['sor'];


        $this->response->setOutput(json_encode($json));
    }

    public function alcsoportVizsgal() {

        $json = array();

        $this->load->model('checkout/cart_elhelyezkedes');
        $kell_alcsoport = $this->model_checkout_cart_elhelyezkedes->kellAlcsoport();

        if ($kell_alcsoport) {
            $json['kell_alcsoport'] = true;
        }


        $json['sor'] = $this->request->request['sor'];


        $this->response->setOutput(json_encode($json));
    }
}
?>