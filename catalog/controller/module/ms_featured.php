<?php
class ControllerModuleMSFeatured extends Controller {
	protected function index($setting) {
		static $module = 0;
		
      	$this->data['heading_title'] = $this->config->get('ms_featured_heading_title_' . $this->config->get('config_language_id') );

        $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
        $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');
		$this->data['button_cart'] = $this->language->get('button_cart');
		$this->data['button_set_valos_ar'] = $this->language->get('button_set_valos_ar');
        $this->data['button_bovebben'] = $this->language->get('button_bovebben');
        $this->data['module_name'] = "ms_featured";
        $this->data['text_netto'] = $this->language->get('text_netto');
        $this->data['button_wishlist'] = $this->language->get('button_wishlist');
        $this->data['button_compare'] = $this->language->get('button_compare');
        $this->data['show_class'] = ($setting['initial'] == "up") ? "active" : "";
		
		$this->load->model('mobile_store/product'); 
		
		$this->load->model('tool/image');

		$this->data['products'] = array();

		$products = explode(',', $this->config->get('ms_featured_product'));		
		
		$fs_selected_products = $this->config->get('facebook_store_selected_products');	
		
		if ($fs_selected_products != 0 && $fs_selected_products != "") {
			$arr_fs_selected_products = explode("," , $fs_selected_products);
			$fs_filter_products = true;
		} else {
			$fs_filter_products = false;
		}


        $data1 = array(
            'sort'  => 'p.date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => 5
        );

        $results1 = $this->model_catalog_product->getProductsNew($data1);
		foreach ($products as $product_id) {
			if ($fs_filter_products){
				if ( in_array($product_id, $arr_fs_selected_products) ) {
					$product_info = $this->model_catalog_product->getProduct($product_id);
				} else {
					$product_info = null;
				}
			} else {
				$product_info = $this->model_catalog_product->getProduct($product_id);
			}	
			
			if ($product_info) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $setting['image_width'], $setting['image_height']);
				} else {
                    $image = $this->model_tool_image->resize('no_image.jpg', $setting['image_width'], $setting['image_height'], false);

                }

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}
						
				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}
				
				if ($this->config->get('config_review_status')) {
					$rating = $product_info['rating'];
				} else {
					$rating = false;
				}

                $uj=false;
                if (array_key_exists($product_info['product_id'],$results1)){
                    $uj=1;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
                } else {
                    $tax = false;
                }

                $discount_query = $this->model_catalog_product->getProductDiscounts($product_info['product_id']);
                $discounts = array();
                $legolcsobb_kulcs=0;
                if($product_info['price'])  {
                    foreach ($discount_query as $discount) {
                        $discounts[] = array(
                            'quantity' => $discount['quantity'],
                            'netto'    => $this->currency->format($discount['price']),
                            'brutto'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax'))),
                            'ara'    => $this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax'))
                        );
                    }
                    if (count($discounts) > 0){
                        $legolcsobb=$discounts[0]['ara'];
                        foreach ($discounts as $key => $value) {
                            if ($value['ara'] < $legolcsobb ){
                                $legolcsobb_kulcs=$key;
                                $legolcsobb=$value['ara'];
                            }
                        }
                    }
                }
                if (count($discounts) == 0) $discounts[$legolcsobb_kulcs]=false;

                if ($product_info['quantity'] > 0 && $product_info['quantity'] <= 4) {
                    $pardarab = true;
                } else {
                    $pardarab = false;
                }
                $descr= strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8'));
                $descr=str_replace("&nbsp;","",$descr);
                $descr=str_replace("\r","",$descr);
                $descr=str_replace("\t","",$descr);
                $descr=str_replace("\n","",$descr);
                $descr=trim($descr);
                $descr=mb_substr($descr, 0, 1000, "UTF-8");

                $this->data['leiras_karakter_lista'] = $this->config->get('megjelenit_description_lista_karakterek');
                $this->data['leiras_karakter_racs'] = $this->config->get('megjelenit_description_karakterek');

                $this->data['products'][] = array(
                    'product_id' => $product_info['product_id'],
                    'utalvany'    => $product_info['utalvany'],
                    'szazalek'    => $product_info['szazalek'],
                    'thumb'   	 => $image,
                    'name'    	 => $product_info['name'],
                    'description'=> $descr,
                    'tax'         => $tax,
                    'price'   	 => $price,
                    'date_ervenyes_ig' => $product_info['date_ervenyes_ig'],
                    'special' 	 => $special,
                    'price_netto' => $this->currency->format($product_info['price']),
                    'special_netto' => $this->currency->format($product_info['special']),
                    'rating'     => $rating,
                    'original_href'    	 => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
                    'href'    	 => $this->url->link('mobile_store/product', 'product_id=' . $product_info['product_id']),
                    'pardarab'	 => $pardarab,
                    'uj'         => $uj,
                    'imagedesabled' => $product_info['imagedesabled'],
                    'discounts' => $discounts[$legolcsobb_kulcs],
                    'csomagolasi_egyseg' => $product_info['csomagolasi_egyseg'],
                    'megyseg'    => $product_info['megyseg'],
                    'csomagolasi_mennyiseg' => $product_info['csomagolasi_mennyiseg'] > 1 ? $product_info['csomagolasi_mennyiseg'] : 0
                );

             /*   $this->data['products'][] = array(
					'product_id' => $product_info['product_id'],
					'thumb'   	 => $image,
					'name'    	 => $product_info['name'],
					'description'=> mb_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
					'price'   	 => $price,
					'special' 	 => $special,
					'rating'     => $rating,
					'original_href'    	 => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
				    'href'    	 => $this->url->link('mobile_store/product', 'product_id=' . $product_info['product_id'])
				);*/
			}
		}
		
		
		$this->data['module'] = $module++;

		$template_file = "ms_featured.tpl";
		

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/' . $template_file )) {
			$this->template = $this->config->get('config_template') . '/template/module/' . $template_file;
		} else {
			$this->template = 'default/template/module/' . $template_file;
		}

		$this->render();
	}
}
?>