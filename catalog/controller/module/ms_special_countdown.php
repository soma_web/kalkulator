<?php
class ControllerModuleMSSpecialCountDown extends Controller {
	protected function index($setting) {
		static $module = 0;
		
		$this->data['heading_title'] = $this->config->get('ms_special_countdown_heading_title_' . $this->config->get('config_language_id') );
		$this->data['heading_days'] = $this->config->get('ms_special_countdown_heading_days_' . $this->config->get('config_language_id') );
		$this->data['heading_hours'] = $this->config->get('ms_special_countdown_heading_hours_' . $this->config->get('config_language_id') );
        $this->data['button_wishlist'] = $this->language->get('button_wishlist');
        $this->data['button_compare'] = $this->language->get('button_compare');
        $this->data['text_netto'] = $this->language->get('text_netto');
		$this->load->model('mobile_store/product');
		$this->load->model('module/ms_special_countdown');
        $this->data['module_name'] = "ms_special";

        $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
        $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');
        $this->data['button_cart'] = $this->language->get('button_cart');
        $this->data['button_set_valos_ar'] = $this->language->get('button_set_valos_ar');

		$this->load->model('catalog/product');
		
		$this->load->model('tool/image');

		$this->data['products'] = array();
		
		$data = array(
			'sort'  => 'pd.name',
			'order' => 'ASC',
			'start' => 0,
			'limit' => $setting['limit']
		);

		$results = $this->model_mobile_store_product->getProductSpecials($data);
        $data1 = array(
            'sort'  => 'p.date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => $setting['limit']
        );
        $results1 = $this->model_catalog_product->getProductsNew($data1);
		foreach ($results as $result) {
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], $setting['image_width'], $setting['image_height']);
			} else {
				$image = false;
			}

			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$price = false;
			}
					
			if ((float)$result['special']) { 
				$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$special = false;
			}
			
			if ($this->config->get('config_review_status')) {
				$rating = $result['rating'];
			} else {
				$rating = false;
			}

            $uj=false;
            if (array_key_exists($result['product_id'],$results1)){
                $uj=1;
            }

            $discount_query = $this->model_catalog_product->getProductDiscounts($result['product_id']);
            $discounts = array();
            $legolcsobb_kulcs=0;
            if($result['price'])  {
                foreach ($discount_query as $discount) {
                    $discounts[] = array(
                        'quantity' => $discount['quantity'],
                        'netto'    => $this->currency->format($discount['price']),
                        'brutto'    => $this->currency->format($this->tax->calculate($discount['price'], $result['tax_class_id'], $this->config->get('config_tax'))),
                        'ara'    => $this->tax->calculate($discount['price'], $result['tax_class_id'], $this->config->get('config_tax'))
                    );
                }
                if (count($discounts) > 0){
                    $legolcsobb=$discounts[0]['ara'];
                    foreach ($discounts as $key => $value) {
                        if ($value['ara'] < $legolcsobb ){
                            $legolcsobb_kulcs=$key;
                            $legolcsobb=$value['ara'];
                        }
                    }
                }
            }
            if (count($discounts) == 0) $discounts[$legolcsobb_kulcs]=false;


            $description = mb_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..';
			
			$show_special_countdown = 0;
			$show_days              = 1;
			$secs					= 0;
			$mins                   = 0;
			$hours	                = 0;
			$days                   = 0;
			$weeks                  = 0;
 			
			$date_stop = $this->model_module_ms_special_countdown->getSpecialPriceEndData($result['product_id']);
			
			if ($date_stop){
				$target_split = preg_split('/\-/', $date_stop);		
				
				$now = time();
				$target = mktime(0,0,0, $target_split[1], $target_split[2], $target_split[0]);
				$diffSecs = $target - $now;
				
				if ( $diffSecs > 0 ){
					$show_special_countdown = 1;
					
					if ($diffSecs < 24 * 60 * 60 ){
						$show_days = 0;
					}
					
					$secs  = $diffSecs % 60;
					$mins  = floor($diffSecs/60)%60;
					$hours = floor($diffSecs/60/60)%24;
					$days  = floor($diffSecs/60/60/24);//%7;
					$weeks = floor($diffSecs/60/60/24/7);
				}
			}		
			
		/*	$this->data['products'][] = array(
				'show_special_countdown' => $show_special_countdown,
				'days'                   =>  $days,
				'hours'                  =>  $hours,
				'mins'                   =>  $mins,
				'secs'                   =>  $secs,
				'reviews'    => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
				'original_href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
				'href'    	 => $this->url->link('mobile_store/product', 'product_id=' . $result['product_id'])
			);*/

            if ($result['quantity'] > 0 && $result['quantity'] <= 4) {
                $pardarab = true;
            } else {
                $pardarab = false;
            }

            if ($this->config->get('config_tax')) {
                $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
            } else {
                $tax = false;
            }

            $descr= strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'));
            $descr=str_replace("&nbsp;","",$descr);
            $descr=str_replace("\r","",$descr);
            $descr=str_replace("\t","",$descr);
            $descr=str_replace("\n","",$descr);
            $descr=trim($descr);
            $descr=mb_substr($descr, 0, 1000, "UTF-8");

            $this->data['leiras_karakter_lista'] = $this->config->get('megjelenit_description_lista_karakterek');
            $this->data['leiras_karakter_racs'] = $this->config->get('megjelenit_description_karakterek');

            $this->data['products'][] = array(
                'show_special_countdown' => $show_special_countdown,
                'days'                   =>  $days,
                'hours'                  =>  $hours,
                'mins'                   =>  $mins,
                'date_ervenyes_ig' => $result['date_ervenyes_ig'],
                'secs'                   =>  $secs,
                'reviews'    => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
                'original_href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
                'href'    	 => $this->url->link('mobile_store/product', 'product_id=' . $result['product_id']),
                'product_id' => $result['product_id'],
                'szazalek'    => $result['szazalek'],
                'utalvany'    => $result['utalvany'],
                'thumb'   	 => $image,
                'name'    	 => $result['name'],
                'tax'         => $tax,
                'description'=> $descr,
                //'description'=> mb_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
                'pardarab'	 => $pardarab,
                'price'   	 => $price,
                'special' 	 => $special,
                'price_netto' => $this->currency->format($result['price']),
                'special_netto' => $this->currency->format($result['special']),
                'rating'     => $rating,
                'original_href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
                'href'    	 => $this->url->link('mobile_store/product', 'product_id=' . $result['product_id']),
                'uj'         => $uj,
                'discounts' => $discounts[$legolcsobb_kulcs],
                'imagedesabled' => $result['imagedesabled'],
                'csomagolasi_egyseg' => $result['csomagolasi_egyseg'],
                'megyseg'    => $result['megyseg'],
                'csomagolasi_mennyiseg' => $result['csomagolasi_mennyiseg'] > 1 ? $result['csomagolasi_mennyiseg'] : 0
            );
		
		}
		
		$this->data['show_class'] = ($setting['initial'] == "up") ? "active" : "";
			
		$this->data['theme_name'] = $this->config->get('config_template');

		$this->data['module'] = $module++;
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/ms_special_countdown.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/ms_special_countdown.tpl';
		} else {
			$this->template = 'default/template/module/ms_special_countdown.tpl';
		}

		$this->render();
	}
}
?>