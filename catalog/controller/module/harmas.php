<?php
class ControllerModuleHarmas extends Controller {
    protected function index($setting) {
        /* ------------------------  featured (Kiemelt termékek) ---------------------------*/
        $this->language->load('module/featured');


        $this->data['leiras_karakter_lista'] = $this->config->get('megjelenit_description_lista_karakterek');
        $this->data['leiras_karakter_racs'] = $this->config->get('megjelenit_description_karakterek');

        $this->data['text_eredeti_ar']  = $this->language->get('text_eredeti_ar');
        $this->data['heading_title_featured'] = $this->language->get('heading_title');
        $this->data['kaphato'] = $this->language->get('kaphato');

        $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
        $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');
        $this->data['button_cart'] = $this->language->get('button_cart');
        $this->data['button_set_valos_ar'] = $this->language->get('button_set_valos_ar');
        $this->data['button_bovebben'] = $this->language->get('button_bovebben');
        $this->data['text_tax'] = $this->language->get('text_tax');
        $this->data['text_netto'] = $this->language->get('text_netto');
        $this->data['module_name'] = "harmas_featured";

        if ($this->config->get('harmas_module_fejlec') == 1) {
            $this->data['heading_latszik'] = true;
        } else {
            $this->data['heading_latszik'] = false;
        }

        if (isset($setting['show']) ) {
            $this->data['show'] = $setting['show'];
        } else {
            $this->data['show'] = 0;
        }

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        $this->data['products_featured'] = array();

        $products_featured = explode(',', $this->config->get('harmas_product'));

        if (empty($setting['limit'])) {
            $setting['limit'] = 5;
        }

        $products_featured = array_slice($products_featured, 0, (int)$setting['limit']);
        $data1 = array(
            'sort'  => 'p.date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => $setting['limit']
        );

        $results1 = $this->model_catalog_product->getProductsNew($data1);
        $this->data['position'] = $setting['position'];
        foreach ($products_featured as $product_id) {

            $product_info = $this->model_catalog_product->getProduct($product_id);
            if ($product_info['status']) {
                $eredmeny = $this->product->productPreparation($product_info,$setting,$results1);
                if ($eredmeny['product_id']) {
                    $this->data['products_featured'][] = $this->product->productPreparation($product_info,$setting,$results1);
                }
            }

        }



/* ------------------------  bestseller (Legnépszerűbb termékek) ---------------------------*/



        $this->language->load('module/bestseller');

        $this->data['heading_title_bestseller'] = $this->language->get('heading_title');

        $this->data['kaphato'] = $this->language->get('kaphato');
        $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
        $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');
        $this->data['button_cart'] = $this->language->get('button_cart');
        $this->data['button_set_valos_ar'] = $this->language->get('button_set_valos_ar');
        $this->data['button_bovebben'] = $this->language->get('button_bovebben');
        $this->data['text_tax'] = $this->language->get('text_tax');
        $this->data['module_name'] = "harmas_bestseller";



        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        $this->data['products_bestseller'] = array();

        $results = $this->model_catalog_product->getBestSellerProducts($setting['limit']);


        $data1 = array(
            'sort'  => 'p.date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => $setting['limit']
        );

        $results1 = $this->model_catalog_product->getProductsNew($data1);
        $this->data['position'] = $setting['position'];

        foreach ($results as $result) {
            $this->data['products_bestseller'][] = $this->product->productPreparation($result,$setting,$results1);

        }


        /* ------------------------  latest (Legújabb termékek) ---------------------------*/

        $this->language->load('module/latest');

        $this->data['kaphato'] = $this->language->get('kaphato');
        $this->data['heading_title_latest'] = $this->language->get('heading_title');
        $this->data['kaphato'] = $this->language->get('kaphato');

        $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
        $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');
        $this->data['button_cart'] = $this->language->get('button_cart');
        $this->data['button_set_valos_ar'] = $this->language->get('button_set_valos_ar');
        $this->data['button_bovebben'] = $this->language->get('button_bovebben');
        $this->data['text_tax'] = $this->language->get('text_tax');
        $this->data['module_name'] = "harmas_latest";



        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        $this->data['products_latest'] = array();

        $data = array(
            'sort'  => 'p.date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => $setting['limit']
        );

        $results = $this->model_catalog_product->getProducts($data);
        $data1 = array(
            'sort'  => 'p.date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => $setting['limit']
        );

        $results1 = $this->model_catalog_product->getProductsNew($data1);
        $this->data['position'] = $setting['position'];

        foreach ($results as $result) {
           $this->data['products_latest'][] = $this->product->productPreparation($result,$setting,$results1);
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/harmas.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/harmas.tpl';
        } else {
            $this->template = 'default/template/module/harmas.tpl';
        }

        $this->render();
    }
}
?>