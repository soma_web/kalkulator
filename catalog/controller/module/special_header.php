<?php
class ControllerModuleSpecialHeader extends Controller {

    public function index() {

		$this->language->load('module/special');

        $setting=$this->config->get('special_header');

      	$this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_eredeti_ar']  = $this->language->get('text_eredeti_ar');
        $this->data['kaphato'] = $this->language->get('kaphato');
        $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
        $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');
		$this->data['button_cart'] = $this->language->get('button_cart');
		$this->data['button_set_valos_ar'] = $this->language->get('button_set_valos_ar');
		$this->data['button_bovebben'] = $this->language->get('button_bovebben');
        $this->data['text_tax'] = $this->language->get('text_tax');
        $this->data['text_netto'] = $this->language->get('text_netto');
        $this->data['module_name'] = "special_header";
        $this->data['button_wishlist'] = $this->language->get('button_wishlist');
        $this->data['button_compare'] = $this->language->get('button_compare');

        $this->data['text_nincs_raktaron'] = $this->language->get('text_nincs_raktaron');
        $this->data['text_cikkszam']            = $this->language->get('text_cikkszam');
        $this->data['text_raktaron'] = $this->language->get('text_raktaron');
        $this->data['text_compare'] = $this->language->get('text_compare');
        $this->data['text_wishlist'] = $this->language->get('text_wishlist');

        $this->data['text_akcio_title']         = $this->language->get('text_akcio_title');
        $this->data['text_pardarab_title']      = $this->language->get('text_pardarab_title');
        $this->data['text_uj_title']            = $this->language->get('text_uj_title');
        $this->data['show'] = 0;

        if ($setting['fejlec'] == 1) {
            $this->data['heading_latszik'] = true;
        } else {
            $this->data['heading_latszik'] = false;
        }

		$this->load->model('catalog/product');
		
		$this->load->model('tool/image');

		$this->data['products'] = array();
		
		$data = array(
			'sort'  => 'pd.name',
			'order' => 'ASC',
			'start' => 0,
			'limit' => $setting['limit'],
			'kiemelt' => isset($setting['kiemelt']) ? $setting['kiemelt'] : 0
		);
        $data1 = array(
            'sort'  => 'p.date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => $setting['limit']
        );


        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('module/special_header'),
            'separator' => $this->language->get('text_separator')
        );

		$results = $this->model_catalog_product->getProductSpecials($data);
        $results1 = $this->model_catalog_product->getProductsNew($data1);



        foreach ($results as $result) {
            $this->data['products'][] = $this->product->productPreparation($result,$setting,$results1);
        }


        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/special_header.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/special_header.tpl';
        } else {
            $this->template = 'default/template/module/special_header.tpl';
        }

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        $this->response->setOutput($this->render());

    }
}
?>