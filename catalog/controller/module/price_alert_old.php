<?php  
class ControllerModulePriceAlert extends Controller {
	protected function index($setting) {
		static $module = 0;
		
		$this->language->load('module/price_alert');
		$this->load->model('tool/image');
		
		$this->document->addStyle('catalog/view/theme/' .  $this->config->get('config_template') . '/stylesheet/price_alert.css');
		
		$this->data['heading_title'] = $this->config->get('price_alert_heading_title_' . $this->config->get('config_language_id') );
		
		$this->data['modal_title'] = $this->language->get('modal_title');
		$this->data['text_info'] = $this->language->get('text_info');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_desired_price'] = $this->language->get('entry_desired_price');
		$this->data['text_set_alert'] = $this->language->get('text_set_alert');
		
		if ($this->customer->isLogged()){
			$this->data['pa_fullname'] = $this->customer->getFirstName() . ' ' . $this->customer->getLastName();
			$this->data['pa_email'] = $this->customer->getEmail();
			$this->data['fieldset_class'] = 'hidden';
		} else {
			$this->data['pa_fullname'] = '';
			$this->data['pa_email'] = '';
			$this->data['fieldset_class'] = '';
		}
		
		$this->data['customer_logged'] = $this->customer->isLogged();
		$this->data['pa_currency_code'] = $this->session->data['currency'];
		$this->data['pa_product_id'] = $this->request->get['product_id'];
		$this->data['theme_name'] = $this->config->get('config_template');
		$this->data['image'] = $this->model_tool_image->resize($this->config->get('price_alert_image'), $this->config->get('price_alert_width'), $this->config->get('price_alert_height'));
		$this->data['display_type'] = $this->config->get('price_alert_display_type');
		
		$this->data['module'] = $module++;
				
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/price_alert.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/price_alert.tpl';
		} else {
			$this->template = 'default/template/module/price_alert.tpl';
		}
		
		$this->render();
	}
	
	public function save_alert() {
		$this->language->load('module/price_alert');
		
		$this->load->model('module/price_alert');
		
		$json = array();
		
		if (!is_numeric($this->request->post['pa_desired_price']) ) {
			$json['error'] = $this->language->get('error_desired_price');
		}	
		
		if (!preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['pa_email'])) {
      		$json['error'] = $this->language->get('error_email');
    	}
		
		if (utf8_strlen($this->request->post['pa_fullname']) < 3) {
			$json['error'] = $this->language->get('error_name');
		}
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && !isset($json['error'])) {
			$alert_price_id = $this->model_module_price_alert->existAlert($this->request->post);
			if (!$alert_price_id) {
				$this->model_module_price_alert->addAlert($this->request->post);
			} else {
				$this->model_module_price_alert->editAlert($alert_price_id, $this->request->post);
			}	
			
			$json['success'] = $this->language->get('text_success');
		}
		
		$this->response->setOutput(json_encode($json));
	}
}
?>