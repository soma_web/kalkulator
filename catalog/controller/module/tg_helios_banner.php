<?php  
class ControllerModuletgheliosbanner extends Controller {
	protected function index($setting) {
		static $module = 0;
		$this->language->load('module/tg_helios_banner'); 
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->load->model('design/banner');
		$this->load->model('tool/image');
		
		$this->document->addScript('catalog/view/javascript/jquery/jquery.cycle.js');
				
		$this->data['banners'] = array();
		
		$results = $this->model_design_banner->getBanner($setting['banner_id']);
		//ThemeGlobal.com - OpenCart Templates Club. Unlimited access to all of our themes for only $49
		$this->data['position'] = $setting['position'];
		$this->document->addScript('catalog/view/javascript/jquery/jquery.jcarousel.min33.js');
		
		if (file_exists('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/carousel33.css')) {
			$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/carousel33.css');
		} else {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/carousel33.css');
		}
		//ThemeGlobal.com - OpenCart Templates Club. Unlimited access to all of our themes for only $49
		  
		foreach ($results as $result) {
			if (file_exists(DIR_IMAGE . $result['image'])) {
				$this->data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'], false)
				);
			}
		}
		
		$this->data['module'] = $module++;
				
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/tg_helios_banner.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/tg_helios_banner.tpl';
		} else {
			$this->template = 'default/template/module/tg_helios_banner.tpl';
		}
		
		$this->render();
	}
}
?>