<?php
class ControllerPaymentcod1 extends Controller {
	protected function index() {
    	$this->data['button_confirm'] = $this->language->get('button_confirm');

        $this->data['continue'] = $_GET['route'] == "checkout/confirm_megrendelem" ? $this->url->link('checkout/success&megrendelem=1') : $this->url->link('checkout/success');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/cod1.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/cod1.tpl';
		} else {
			$this->template = 'default/template/payment/cod1.tpl';
		}	
		
		$this->render();
	}
	
	public function confirm() {
		$this->load->model('checkout/order');
		
		$this->model_checkout_order->confirm($this->session->data['order_id'], $this->config->get('cod1_order_status_id'));
	}
}
?>