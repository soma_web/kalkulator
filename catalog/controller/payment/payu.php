<?php
class ControllerPaymentPayU extends Controller {
    protected function index() {
        $this->language->load('payment/payu');

        $this->data['button_confirm'] = $this->language->get('button_confirm');
        $this->data['text_payuterms'] = sprintf($this->language->get('text_payuterms'),$this->config->get('config_owner'),$this->config->get('config_address'),$this->config->get('config_name'));

        $this->data['action'] = 'https://secure.payu.hu/order/lu.php';

        $this->load->model('checkout/order');

        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        $data = array();

        $data['merchant'] = $this->get_merchant();

        $data['order_ref'] = $order_info['order_id'];;               //belső referenciaszám, opencart által generált
        $data['order_date'] = date("Y-m-d H:i:s");              //ÉÉÉÉ-HH-NN ÓÓ:PP:MP
        $data['order_pname'] = array();       //termék neve
        //$data['order_pgroup'] = array();      //termékcsoport OPCIONÁLIS
        $data['order_pcode'] = array();       //egyedi termékkód
        $data['order_pinfo'] = array();       //egyéb termékinfo OPCIONÁLIS
        $data['order_price'] = array();       //nettó ár (ár áfa nélkül)
        $data['order_qty'] = array();         //termék mennyisége
        $data['order_vat'] = array();         //áfa százalék pl 25
        //$data['order_ver'] = array();         //termék verziója OPCIONÁLIS

        //$data['order_shipping'] = $this->currency->format($this->session->data['shipping_method']['cost'],'','',false);    //szállítási költség
        if(isset($this->session->data['shipping_method'])) {
            $data['order_shipping'] = $this->currency->format($this->tax->calculate($this->session->data['shipping_method']['cost'], $this->session->data['shipping_method']['tax_class_id'], $this->config->get('config_tax')),'','',false);
        }


        $currency = $this->currency->getCode();

             if($currency=="HUF") {$data['prices_currency'] = "HUF"; $secretkey=$this->config->get('payu_secret_key_eur');}
        else if($currency=="EUR") {$data['prices_currency'] = "EUR";$secretkey=$this->config->get('payu_secret_key_huf');}
        else                      {$data['prices_currency'] = "";$secretkey=$this->config->get('payu_secret_key_eur');}



        // Totals
        $this->load->model('setting/extension');

        $total_data = array();
        $total = 0;
        $taxes = $this->cart->getTaxes();

        $sort_order = array();

        $results = $this->model_setting_extension->getExtensions('total');

        foreach ($results as $key => $value) {
            $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
        }

        array_multisort($sort_order, SORT_ASC, $results);

        foreach ($results as $result) {
            if ($this->config->get($result['code'] . '_status')) {
                $this->load->model('total/' . $result['code']);

                $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);

            }

            $sort_order = array();

            foreach ($total_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $total_data);
        }

        $engedmeny = 0;
        foreach ($total_data as $value) {
            if ($value['code'] == 'reward' || $value['code'] == 'coupon' || $value['code'] == 'voucher') {
                $engedmeny +=$value['value'];
            }


        }
        $data['discount'] = abs($engedmeny);                //OPCIONÁLIS
        $data['destination_city'] = $order_info['payment_city'];        //OPCIONÁLIS
        $data['destination_state'] =  $order_info['payment_zone'];       //OPCIONÁLIS
        $data['destination_country'] = $order_info['payment_iso_code_2'];     //OPCIONÁLIS
        $data['pay_method'] = "CCVISAMC";              //CCVISAMC, CCAMEX, CCDINERS, CCJCB OPCIONÁLIS

        $data['order_hash'] = ""; //később lesz kitöltve, a sorrend miatt fontos hogy itt is legyen

        $data['testorder'] = $this->config->get('payu_testorder');         //teszt üzemmód OPCIONÁLIS
        $data['debug'] = $this->config->get('payu_debug');                 //hibakeresés mód OPCIONÁLIS
        $data['language'] = $this->config->get('payu_language');           //felület nyelve OPCIONÁLIS
        $data['order_timeout'] = $this->config->get('payu_order_timeout'); //rendelés timeout OPCIONÁLIS
        $data['timeout_url'] = HTTP_SERVER."index.php?route=payment/payu/timeout";     //rendelés timeout url OPCIONÁLIS

        $data['back_ref'] = HTTP_SERVER."index.php?route=payment/payu/success";                //lezáró oldal

        $data['automode'] = "1";
        $data['bill_fname'] = $order_info['payment_firstname'];
        $data['bill_lname'] = $order_info['payment_lastname'];
        $data['bill_company'] = $order_info['payment_company'];
        $data['bill_email'] = $order_info['email'];
        $data['bill_phone'] = $order_info['telephone'];
        $data['bill_fax'] = $order_info['fax'];
        $data['bill_address'] = $order_info['payment_address_1'];              
        $data['bill_zipcode'] = $order_info['payment_postcode'];	             
        $data['bill_city'] = $order_info['payment_city'];              
        $data['bill_countrycode'] = $order_info['payment_iso_code_2'];
        if ($order_info['payment_address_2']) {$data['bill_address2'] = $order_info['payment_address_2'];}
        $data['bill_state'] = $order_info['payment_zone'];


        if ($this->cart->hasShipping()) {
            $data['delivery_fname'] = $order_info['shipping_firstname'];
            $data['delivery_lname'] = $order_info['shipping_lastname'];
            $data['delivery_company'] = $order_info['shipping_company'];
            $data['delivery_phone'] = $order_info['telephone'];              
            $data['delivery_address'] = $order_info['shipping_address_1'];              
            $data['delivery_zipcode'] = $order_info['shipping_postcode'];	             
            $data['delivery_city'] = $order_info['shipping_city'];              
            $data['delivery_countrycode'] = $order_info['payment_iso_code_2'];
            if ($order_info['shipping_address_2']) {$data['delivery_address2'] = $order_info['shipping_address_2'];}              
            $data['delivery_state'] = $order_info['shipping_zone'];
        } else {
            $data['delivery_fname'] = $order_info['payment_firstname'];              
            $data['delivery_lname'] = $order_info['payment_lastname'];
            $data['delivery_company'] = $order_info['payment_company'];
            $data['delivery_phone'] = $order_info['telephone'];              
            $data['delivery_address'] = $order_info['payment_address_1'];              
            $data['delivery_zipcode'] = $order_info['payment_postcode'];	             
            $data['delivery_city'] = $order_info['payment_city'];              
            $data['delivery_countrycode'] = $order_info['payment_iso_code_2'];
            if ($order_info['payment_address_2']) {$data['delivery_address2'] = $order_info['payment_address_2'];}
            $data['delivery_state'] = $order_info['payment_zone'];
        }

        $products = $this->cart->getProducts();
        foreach ($products as $product) {

//            $data['order_pname'][] = iconv("utf-8", "ascii//TRANSLIT", $product['name']);       //termék neve
            $data['order_pname'][] = $product['name'];       //termék neve
           /* $afakulcs=$this->tax->getRates($product['price'],$product['tax_class_id']);
            foreach($afakulcs as $afa){
                $azafa=$afa['rate'];
            }*/

            //$data['order_pgroup'][] = "1";      //termékcsoport OPCIONÁLIS
            $data['order_pcode'][] = $product['product_id'];       //egyedi termékkód
            $data['order_pinfo'][] = "";       //egyéb termékinfo OPCIONÁLIS
            //$data['order_price'][] = $this->currency->format(max(0, $product['price']),'','',false);       //nettó ár (ár áfa nélkül)

            $data['order_price'][] = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')),'','',false);

            $data['order_qty'][] = $product['quantity'];          //termék mennyisége
            $data['order_vat'][] = "0";          //áfa százalék pl 25
            //$data['order_vat'][] = $azafa;          //áfa százalék pl 25
           // $data['order_ver'][] = "0";           //termék verzió
        }

        $datastring="";
        
        $datastring.=strlen($data['merchant']).$data['merchant'];
        $datastring.=strlen($data['order_ref']).$data['order_ref'];
        $datastring.=strlen($data['order_date']).$data['order_date'];

        foreach($data['order_pname'] as $order_pname){
            $datastring.=strlen($order_pname).$order_pname;
        }/*
        foreach($data['order_pgroup'] as $order_pgroup){
            $datastring.=strlen($order_pgroup).$order_pgroup;
        }*/
        foreach($data['order_pcode'] as $order_pcode){
            $datastring.=strlen($order_pcode).$order_pcode;
        }
        foreach($data['order_pinfo'] as $order_pinfo){
            $datastring.=strlen($order_pinfo).$order_pinfo;
        }
        foreach($data['order_price'] as $order_price){
            $datastring.=strlen($order_price).$order_price;
        }
        foreach($data['order_qty'] as $order_qty){
            $datastring.=strlen($order_qty).$order_qty;
        }
        foreach($data['order_vat'] as $order_vat){
            $datastring.=strlen($order_vat).$order_vat;
        }/*
        foreach($data['order_ver'] as $order_ver){
            $datastring.=strlen($order_ver).$order_ver;
        }*/

        if(isset($data['order_shipping'])) {
            $datastring.=strlen($data['order_shipping']).$data['order_shipping'];
        }
        $datastring.=strlen($data['prices_currency']).$data['prices_currency'];
        $datastring.=strlen($data['discount']).$data['discount'];
        $datastring.=strlen($data['destination_city']).$data['destination_city'];
        $datastring.=strlen($data['destination_state']).$data['destination_state'];
        $datastring.=strlen($data['destination_country']).$data['destination_country'];
        $datastring.=strlen($data['pay_method']).$data['pay_method'];

        $data['order_hash'] = hash_hmac("md5",$datastring,$this->get_secret());              //HMAC_MD5 hash
//        $data['order_hash'] = hash_hmac("md5",utf8_decode($datastring),$this->get_secret());              //HMAC_MD5 hash

        $this->data['data']  = $data;

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/payu.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/payment/payu.tpl';
        } else {
            $this->template = 'default/template/payment/payu.tpl';
        }

        $this->render();
    }

    private function to_success() {
        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
        if (isset($this->session->data['order_id'])) {
            $this->cart->clear();

            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);
            unset($this->session->data['guest']);
            unset($this->session->data['comment']);
            unset($this->session->data['order_id']);
            unset($this->session->data['coupon']);
            unset($this->session->data['reward']);
            unset($this->session->data['voucher']);
            unset($this->session->data['vouchers']);
        }

        $this->language->load('checkout/success');
        $this->language->load('payment/payu');

        $this->document->setTitle($this->language->get('text_transaction_success_title'));

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'href'      => $this->url->link('common/home'),
            'text'      => $this->language->get('text_home'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'href'      => $this->url->link('checkout/cart'),
            'text'      => $this->language->get('text_basket'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['breadcrumbs'][] = array(
            'href'      => $this->url->link('checkout/checkout', '', 'SSL'),
            'text'      => $this->language->get('text_checkout'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['breadcrumbs'][] = array(
            'href'      => $this->url->link('checkout/success'),
            'text'      => $this->language->get('text_transaction_success_title'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_transaction_title'] = $this->language->get('text_transaction_success_title');

        if ($this->customer->isLogged()) {
            $this->data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/account', '', 'SSL'), $this->url->link('account/order', '', 'SSL'), $this->url->link('information/contact', '', 'SSL'), $this->url->link('information/contact'));
        } else {
            $this->data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact'));
        }

        $this->data['button_continue'] = $this->language->get('button_continue');

        $this->data['continue'] = $this->url->link('common/home');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/payu_success.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/payment/payu_success.tpl';
        } else {
            $this->template = 'default/template/payment/payu_success.tpl';
        }

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        $this->data['amount']=$this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], true);

        if(isset($_GET['date'])) $this->data['date']=$_GET['date'];
        else $this->data['date']='';

        if(isset($_GET['payrefno'])) $this->data['payrefno']=$_GET['payrefno'];
        else $this->data['payrefno']='';


        $this->response->setOutput($this->render());
    }

    private function to_fail() {
        $this->language->load('checkout/success');
        $this->language->load('payment/payu');

        $this->document->setTitle($this->language->get('text_transaction_failure_title'));

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'href'      => $this->url->link('common/home'),
            'text'      => $this->language->get('text_home'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'href'      => $this->url->link('checkout/cart'),
            'text'      => $this->language->get('text_basket'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['breadcrumbs'][] = array(
            'href'      => $this->url->link('checkout/checkout', '', 'SSL'),
            'text'      => $this->language->get('text_checkout'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['breadcrumbs'][] = array(
            'href'      => $this->url->link('checkout/success'),
            'text'      => $this->language->get('text_transaction_failure_title'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_transaction_title'] = $this->language->get('text_transaction_failure_title');
        $this->data['text_transaction_description'] = $this->language->get('text_transaction_failure_description');

        $this->data['text_message'] = $this->language->get('text_transaction_failure_description');

        $this->data['button_continue'] = $this->language->get('button_continue');

        $this->data['continue'] = $this->url->link('common/home');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/payu_failure.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/payment/payu_failure.tpl';
        } else {
            $this->template = 'default/template/payment/payu_failure.tpl';
        }

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        if(isset($_GET['date'])) $this->data['date']=$_GET['date'];
        else $this->data['date']='';

        if(isset($_GET['payrefno'])) $this->data['payrefno']=$_GET['payrefno'];
        else $this->data['payrefno']='';


        $this->response->setOutput($this->render());
    }

    public function success() {
        //000 - Approved balances available | SIKER
        if (isset($this->session->data['order_id'])){

            $rt=$_GET['RT'];
            $rtx=explode(" - ", $rt);

            $this->load->model('checkout/order');

            $this->model_checkout_order->confirm($this->session->data['order_id'], $this->config->get('config_order_status_id'));

            $message = '';

            $message .= 'RC: ' . $_GET['RC'] . "\n";
            $message .= 'RT: ' . $_GET['RT'] . "\n";
            $message .= '3dsecure: ' . $_GET['3dsecure'] . "\n";
            $message .= 'date: ' . $_GET['date'] . "\n";
            $message .= 'payrefno: ' . $_GET['payrefno'] . "\n";
            $message .= 'ctrl: ' . $_GET['ctrl'] . "\n\n";


            $url=substr(html_entity_decode("http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]),0,-38);
            $url2=strlen($url).$url;
            $hash=hash_hmac("md5",$url2,$this->get_secret());

            if(($rtx[0]=="000" || $rtx[0]=="001") && $hash==$_GET['ctrl']) {
                $this->model_checkout_order->update($this->session->data['order_id'], $this->config->get('payu_order_status_id'), $message, false);
                $this->to_success();
            } else {
                $this->model_checkout_order->update($this->session->data['order_id'], $this->config->get('config_order_status_id'), $message, false);
                $this->to_fail();
            }
        } else {
            header("Location: ".HTTP_SERVER."index.php/");
        }
    }

    public function timeout() {
        $this->language->load('checkout/success');
        $this->language->load('payment/payu');

        $this->document->setTitle($this->language->get('text_transaction_timeout_title'));

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'href'      => $this->url->link('common/home'),
            'text'      => $this->language->get('text_home'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'href'      => $this->url->link('checkout/cart'),
            'text'      => $this->language->get('text_basket'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['breadcrumbs'][] = array(
            'href'      => $this->url->link('checkout/checkout', '', 'SSL'),
            'text'      => $this->language->get('text_checkout'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['breadcrumbs'][] = array(
            'href'      => $this->url->link('checkout/success'),
            'text'      => $this->language->get('text_transaction_timeout_title'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_transaction_title'] = $this->language->get('text_transaction_timeout_title');
        $this->data['text_transaction_description'] = $this->language->get('text_transaction_timeout_description');

        $this->data['text_message'] = $this->language->get('text_transaction_timeout_description');

        $this->data['button_continue'] = $this->language->get('button_continue');

        $this->data['continue'] = $this->url->link('common/home');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/payu_success.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/payment/payu_success.tpl';
        } else {
            $this->template = 'default/template/payment/payu_success.tpl';
        }

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        if(isset($_GET['date'])) $this->data['date']=$_GET['date'];
            else $this->data['date']='';

        if(isset($_GET['payrefno'])) $this->data['payrefno']=$_GET['payrefno'];
            else $this->data['payrefno']='';

        $this->response->setOutput($this->render());
    }

    public function ipn() {
        $this->load->model('checkout/order');

        $date=date("YmdHis");

        $hashstr="";
        $hashstr.=strlen($_POST['IPN_PID'][0]).$_POST['IPN_PID'][0];
        $hashstr.=strlen($_POST['IPN_PNAME'][0]).$_POST['IPN_PNAME'][0];
        $hashstr.=strlen($_POST['IPN_DATE']).$_POST['IPN_DATE'];
        $hashstr.=strlen($date).$date;

        $sid=$this->config->get('payu_order_status_id');
        $refnoext=$_POST['REFNOEXT'];

        $this->model_checkout_order->update((int)$refnoext, (int)$sid, 'IPN OK', false);

        $hash=hash_hmac("md5",utf8_decode($hashstr),$this->get_secret($_POST['CURRENCY']));
        echo "<EPAYMENT>".$date."|".$hash."</EPAYMENT>";
    }

    private function get_secret($currency = false) {
        if($currency==false) $currency = $this->currency->getCode();
        if($currency=="HUF") return html_entity_decode($this->config->get('payu_secret_key_huf'));
        if($currency=="EUR") return html_entity_decode($this->config->get('payu_secret_key_eur'));

        return html_entity_decode($this->config->get('payu_secret_key_eur'));
    }

    private function get_merchant() {
        $currency = $this->currency->getCode();

        if($currency=="HUF") return html_entity_decode($this->config->get('payu_merchant_huf'));
        if($currency=="EUR") return html_entity_decode($this->config->get('payu_merchant_eur'));

        return html_entity_decode($this->config->get('payu_merchant_eur'));
    }
}
?>
