<?php 
class ControllerAccountLogin extends Controller {
	private $error = array();
    private $token;
	
	public function index() {
		// Login override for admin users
        $this->data['megjelenit_altalanos'] = $this->config->get("megjelenit_altalanos");

        $alt = $this->config->get("megjelenit_altalanos");
        if(array_key_exists('emlekezz_ram_login', $alt)) {
            $this->data['remember_me'] = $alt['emlekezz_ram_login'];
        } else {
            $this->data['remember_me'] = 0;
        }

		if (!empty($this->request->get['token'])) {
			$this->customer->logout();
			
			$this->load->model('account/customer');
			
			$customer_info = $this->model_account_customer->getCustomerByToken($this->request->get['token']);
			
		 	if ($customer_info && $this->customer->login($customer_info['email'], '', true)) {
				$this->redirect($this->url->link('account/account', '', 'SSL')); 
			}
		}		
		
		if ($this->customer->isLogged()) {  
      		$this->redirect($this->url->link('account/account', '', 'SSL'));
    	}



    	$this->language->load('account/login');

    	$this->document->setTitle($this->language->get('heading_title'));
								
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			unset($this->session->data['guest']);
            $alt = $this->config->get("megjelenit_altalanos");
            if(array_key_exists('emlekezz_ram_login', $alt) && $alt['emlekezz_ram_login'] == '1') {
                setcookie('userid', $this->customer->getId(), time()+(365*24*60*60), '/', $_SERVER['SERVER_NAME'], false, true);
                setcookie('token', $this->token, time()+(365*24*60*60), '/', $_SERVER['SERVER_NAME'], false, true);
            }
			

			if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], HTTP_SERVER) !== false || strpos($this->request->post['redirect'], HTTPS_SERVER) !== false)) {
				$this->redirect(str_replace('&amp;', '&', $this->request->post['redirect']));
			} else {
				$this->redirect($this->url->link('account/account', '', 'SSL')); 
			}
    	}  
		
      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),       	
        	'separator' => false
      	);
  
      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
		
      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_login'),
			'href'      => $this->url->link('account/login', '', 'SSL'),      	
        	'separator' => $this->language->get('text_separator')
      	);
				
    	$this->data['heading_title'] = $this->language->get('heading_title');

    	$this->data['text_new_customer'] = $this->language->get('text_new_customer');
    	$this->data['text_facebook_account'] = $this->language->get('text_facebook_account');
    	$this->data['text_register'] = $this->language->get('text_register');
    	$this->data['text_register_account'] = $this->language->get('text_register_account');
		$this->data['text_returning_customer'] = $this->language->get('text_returning_customer');
		$this->data['text_i_am_returning_customer'] = $this->language->get('text_i_am_returning_customer');
    	$this->data['text_forgotten'] = $this->language->get('text_forgotten');

    	$this->data['entry_email'] = $this->language->get('entry_email');
    	$this->data['entry_password'] = $this->language->get('entry_password');

    	$this->data['button_continue'] = $this->language->get('button_continue');
		$this->data['button_login'] = $this->language->get('button_login');
        $this->data['entry_remember_me'] = $this->language->get('entry_remember_me');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['action'] = $this->url->link('account/login', '', 'SSL');

        if (isset($_REQUEST['feltolto']) && $_REQUEST['feltolto'] == 1) {
            $this->data['register'] = $this->url->link('account/register&feltolto=1', '', 'SSL');
        } else {
            $this->data['register'] = $this->url->link('account/register', '', 'SSL');
        }

		$this->data['forgotten'] = $this->url->link('account/forgotten', '', 'SSL');

		if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], HTTP_SERVER) !== false || strpos($this->request->post['redirect'], HTTPS_SERVER) !== false)) {
			$this->data['redirect'] = $this->request->post['redirect'];
		} elseif (isset($this->session->data['redirect'])) {
      		$this->data['redirect'] = $this->session->data['redirect'];
	  		
			unset($this->session->data['redirect']);		  	
    	} else {
			$this->data['redirect'] = '';
		}

		if (isset($this->session->data['success'])) {
    		$this->data['success'] = $this->session->data['success'];
    
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

       /* $cacheut=DIR_CACHE. 'cache.product*';
        if($i=strpos($cacheut, ':')==1){
            system("del $cacheut");
        }
        else{
            system("rm $cacheut");
        }*/


		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/login.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/account/login.tpl';
		} else {
			$this->template = 'default/template/account/login.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header',
			'module/fbjsconnect'
		);
						
		$this->response->setOutput($this->render());
  	}
  
  	private function validate() {
    	if (!$this->customer->login($this->request->post['email'], $this->request->post['password'])) {
      		$this->error['warning'] = $this->language->get('error_login');
    	}
        if(array_key_exists('remember_me', $this->request->request) && $this->request->request['remember_me'] == '1') {
            $this->token = $this->createToken();
        }
	
    	if (!$this->error) {
      		return true;
    	} else {
      		return false;
    	}  	
  	}

    private function createToken() {
        $customerID = $this->customer->getId();
        $token = uniqid('', true);
        $this->load->model('account/rememberme');
        $this->model_account_rememberme->addRememberedLogin($customerID, $token);

        return $token;
    }
}
?>