<?php
class ControllerAccountRegister extends Controller {
    private $error = array();

    public function index() {
        if ($this->customer->isLogged()) {
            $this->redirect($this->url->link('account/account', '', 'SSL'));
        }

        $this->language->load('account/register');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('account/customer');

        $email2_ell = true;
        if (isset($_REQUEST['email2_no']) && $_REQUEST['email2_no'] == 1) {
            $email2_ell = false;
        }



        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate($email2_ell)) {
            unset($this->session->data['guest']);

            $this->model_account_customer->addCustomer($this->request->post);

            $this->customer->login($this->request->post['email'], $this->request->post['password']);

            $this->redirect($this->url->link('account/success'));
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_account'),
            'href'      => $this->url->link('account/account', '', 'SSL'),
            'separator' => $this->language->get('text_separator')
        );




        if (isset($this->request->request['feltolto']) && $this->request->request['feltolto'] == 1){
            $this->data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login&feltolto=1', '', 'SSL'));
            $this->data['breadcrumbs'][] = array(
                'text'      => $this->language->get('text_register'),
                'href'      => $this->url->link('account/register&feltolto=1', '', 'SSL'),
                'separator' => $this->language->get('text_separator')
            );
        } else {
            $this->data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', 'SSL'));
            $this->data['breadcrumbs'][] = array(
                'text'      => $this->language->get('text_register'),
                'href'      => $this->url->link('account/register', '', 'SSL'),
                'separator' => $this->language->get('text_separator')
            );
        }


        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');
        $this->data['text_select'] = $this->language->get('text_select');



        $this->data['text_your_details'] = $this->language->get('text_your_details');
        $this->data['text_your_address'] = $this->language->get('text_your_address');
        $this->data['text_your_cegadatok'] = $this->language->get('text_your_cegadatok');
        $this->data['text_your_password'] = $this->language->get('text_your_password');
        $this->data['text_newsletter'] = $this->language->get('text_newsletter');

        $this->data['entry_adoszam'] = $this->language->get('entry_adoszam');
        $this->data['entry_firstname'] = $this->language->get('entry_firstname');
        $this->data['entry_lastname'] = $this->language->get('entry_lastname');
        $this->data['entry_email'] = $this->language->get('entry_email');
        $this->data['error_fb_email_error'] = $this->language->get('error_fb_email_error');
        $this->data['entry_email_again'] = $this->language->get('entry_email_again');
        $this->data['entry_telephone'] = $this->language->get('entry_telephone');
        $this->data['entry_fax'] = $this->language->get('entry_fax');
        $this->data['entry_company'] = $this->language->get('entry_company');
        $this->data['entry_adoszam'] = $this->language->get('entry_adoszam');
        $this->data['entry_address_1'] = $this->language->get('entry_address_1');
        $this->data['entry_address_2'] = $this->language->get('entry_address_2');
        $this->data['entry_postcode'] = $this->language->get('entry_postcode');
        $this->data['entry_city'] = $this->language->get('entry_city');
        $this->data['entry_country'] = $this->language->get('entry_country');
        $this->data['entry_zone'] = $this->language->get('entry_zone');
        $this->data['entry_newsletter'] = $this->language->get('entry_newsletter');
        $this->data['entry_password'] = $this->language->get('entry_password');
        $this->data['entry_confirm'] = $this->language->get('entry_confirm');
        $this->data['text_paypaltitle'] = $this->language->get('text_paypaltitle');
        $this->data['text_paypallable'] = $this->language->get('text_paypallable');
        $this->data['entry_nem'] = $this->language->get('entry_nem');
        $this->data['entry_eletkor'] = $this->language->get('entry_eletkor');
        $this->data['entry_iskolai_vegzettseg'] = $this->language->get('entry_iskolai_vegzettseg');
        $this->data['entry_weblap'] = $this->language->get('entry_weblap');

        $this->data['entry_vallalkozasi_forma'] = $this->language->get('entry_vallalkozasi_forma');
        $this->data['entry_szekhely'] = $this->language->get('entry_szekhely');
        $this->data['entry_ugyvezeto_neve'] = $this->language->get('entry_ugyvezeto_neve');
        $this->data['entry_ugyvezeto_telefonszama'] = $this->language->get('entry_ugyvezeto_telefonszama');

        if (isset($this->request->post['paypalemail'])) {
            $this->data['paypalemail'] = $this->request->post['paypalemail'];
        } else {
            $this->data['paypalemail'] = '';
        }

        $this->data['button_continue'] = $this->language->get('button_continue');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }


        if (isset($this->error['company'])) {
            $this->data['error_company'] = $this->error['company'];
        } else {
            $this->data['error_company'] = '';
        }
        if (isset($this->error['adoszam'])) {
            $this->data['error_adoszam'] = $this->error['adoszam'];
        } else {
            $this->data['error_adoszam'] = '';
        }





        if (isset($this->error['vallalkozasi_forma'])) {
            $this->data['error_vallalkozasi_forma'] = $this->error['vallalkozasi_forma'];
        } else {
            $this->data['error_vallalkozasi_forma'] = '';
        }
        if (isset($this->error['szekhely'])) {
            $this->data['error_szekhely'] = $this->error['szekhely'];
        } else {
            $this->data['error_szekhely'] = '';
        }
        if (isset($this->error['ugyvezeto_neve'])) {
            $this->data['error_ugyvezeto_neve'] = $this->error['ugyvezeto_neve'];
        } else {
            $this->data['error_ugyvezeto_neve'] = '';
        }
        if (isset($this->error['ugyvezeto_telefonszama'])) {
            $this->data['error_ugyvezeto_telefonszama'] = $this->error['ugyvezeto_telefonszama'];
        } else {
            $this->data['error_ugyvezeto_telefonszama'] = '';
        }





        if (isset($this->error['firstname'])) {
            $this->data['error_firstname'] = $this->error['firstname'];
        } else {
            $this->data['error_firstname'] = '';
        }

        if (isset($this->error['lastname'])) {
            $this->data['error_lastname'] = $this->error['lastname'];
        } else {
            $this->data['error_lastname'] = '';
        }

        if(isset($this->request->get['fb_email_error'])){
            $this->data['error_warning'] = $this->data['error_fb_email_error'];
        }

        if (isset($this->error['email'])) {
            $this->data['error_email'] = $this->error['email'];
        } else {
            $this->data['error_email'] = '';
        }

        if (isset($this->error['email_again'])) {
            $this->data['error_email_again'] = $this->error['email_again'];
        } else {
            $this->data['error_email_again'] = '';
        }

        if (isset($this->error['telephone'])) {
            $this->data['error_telephone'] = $this->error['telephone'];
        } else {
            $this->data['error_telephone'] = '';
        }


        if (isset($this->error['password'])) {
            $this->data['error_password'] = $this->error['password'];
        } else {
            $this->data['error_password'] = '';
        }

        if (isset($this->error['confirm'])) {
            $this->data['error_confirm'] = $this->error['confirm'];
        } else {
            $this->data['error_confirm'] = '';
        }

        if (isset($this->error['address_1'])) {
            $this->data['error_address_1'] = $this->error['address_1'];
        } else {
            $this->data['error_address_1'] = '';
        }

        if (isset($this->error['city'])) {
            $this->data['error_city'] = $this->error['city'];
        } else {
            $this->data['error_city'] = '';
        }

        if (isset($this->error['postcode'])) {
            $this->data['error_postcode'] = $this->error['postcode'];
        } else {
            $this->data['error_postcode'] = '';
        }

        if (isset($this->error['eletkor'])) {
            $this->data['error_eletkor'] = $this->error['eletkor'];
        } else {
            $this->data['error_eletkor'] = '';
        }



        if (isset($this->error['nem'])) {
            $this->data['error_nem'] = $this->error['nem'];
        } else {
            $this->data['error_nem'] = '';
        }

        if (isset($this->error['country'])) {
            $this->data['error_country'] = $this->error['country'];
        } else {
            $this->data['error_country'] = '';
        }

        if (isset($this->error['zone'])) {
            $this->data['error_zone'] = $this->error['zone'];
        } else {
            $this->data['error_zone'] = '';
        }

        if(isset($_REQUEST['fb_email_error'])){
            $this->error['error_fb_email_error'] = $this->language->get('error_fb_email_error');
        } else {
            $this->error['error_fb_email_error'] = '';
        }

        $this->data['action'] = $this->url->link('account/register', '', 'SSL');

        if (isset($this->request->post['firstname'])) {
            $this->data['firstname'] = $this->request->post['firstname'];
        } elseif (isset($this->request->request['fb_reg_firstname'])) {
            $this->data['firstname'] = $this->request->request['fb_reg_firstname'];
        }
        else {
            $this->data['firstname'] = '';
        }

        if (isset($this->request->post['lastname'])) {
            $this->data['lastname'] = $this->request->post['lastname'];
        } elseif (isset($this->request->request['fb_reg_lastname'])) {
            $this->data['lastname'] = $this->request->request['fb_reg_lastname'];
        } else {
            $this->data['lastname'] = '';
        }


        if (isset($this->request->post['weblap'])) {
            $this->data['weblap'] = $this->request->post['weblap'];
        } else {
            $this->data['weblap'] = '';
        }

        if (isset($this->request->post['email'])) {
            $this->data['email'] = $this->request->post['email'];
        } elseif (isset($this->request->request['fb_reg_email'])) {
            $this->data['email'] = $this->request->request['fb_reg_email'];
        } else {
            $this->data['email'] = '';
        }

        if (isset($this->request->post['email_again'])) {
            $this->data['email_again'] = $this->request->post['email_again'];
        } elseif (isset($this->request->request['fb_reg_email'])) {
            $this->data['email_again'] = $this->request->request['fb_reg_email'];
        } else {
            $this->data['email_again'] = '';
        }

        if (isset($this->request->post['telephone'])) {
            $this->data['telephone'] = $this->request->post['telephone'];
        } else {
            $this->data['telephone'] = '';
        }

        if (isset($this->request->post['fax'])) {
            $this->data['fax'] = $this->request->post['fax'];
        } else {
            $this->data['fax'] = '';
        }

        if (isset($this->request->post['adoszam'])) {
            $this->data['adoszam'] = $this->request->post['adoszam'];
        } else {
            $this->data['adoszam'] = '';
        }

        if (isset($this->request->post['company'])) {
            $this->data['company'] = $this->request->post['company'];
        } else {
            $this->data['company'] = '';
        }

        if (isset($this->request->post['company_cim'])) {
            $this->data['company_cim'] = $this->request->post['company_cim'];
        } else {
            $this->data['company_cim'] = '';
        }

        if (isset($this->request->post['adoszam'])) {
            $this->data['adoszam'] = $this->request->post['adoszam'];
        } else {
            $this->data['adoszam'] = '';
        }

        if (isset($this->request->post['address_1'])) {
            $this->data['address_1'] = $this->request->post['address_1'];
        } else {
            $this->data['address_1'] = '';
        }

        if (isset($this->request->post['address_2'])) {
            $this->data['address_2'] = $this->request->post['address_2'];
        } else {
            $this->data['address_2'] = '';
        }

        if (isset($this->request->post['postcode'])) {
            $this->data['postcode'] = $this->request->post['postcode'];
        } else {
            $this->data['postcode'] = '';
        }

        if (isset($this->request->post['city'])) {
            $this->data['city'] = $this->request->post['city'];
        } else {
            $this->data['city'] = '';
        }

        if (isset($this->request->post['nem'])) {
            $this->data['nem'] = $this->request->post['nem'];
        } else {
            $this->data['nem'] = '';
        }

        if (isset($this->request->post['eletkor'])) {
            $this->data['eletkor'] = $this->request->post['eletkor'];
        } else {
            $this->data['eletkor'] = '';
        }

        if (isset($this->request->post['iskolai_vegzettseg'])) {
            $this->data['iskolai_vegzettseg'] = $this->request->post['iskolai_vegzettseg'];
        } else {
            $this->data['iskolai_vegzettseg'] = '';
        }

        if (isset($this->request->post['vallalkozasi_forma'])) {
            $this->data['vallalkozasi_forma'] = $this->request->post['vallalkozasi_forma'];
        } else {
            $this->data['vallalkozasi_forma'] = '';
        }
        if (isset($this->request->post['szekhely'])) {
            $this->data['szekhely'] = $this->request->post['szekhely'];
        } else {
            $this->data['szekhely'] = '';
        }
        if (isset($this->request->post['ugyvezeto_neve'])) {
            $this->data['ugyvezeto_neve'] = $this->request->post['ugyvezeto_neve'];
        } else {
            $this->data['ugyvezeto_neve'] = '';
        }
        if (isset($this->request->post['ugyvezeto_telefonszama'])) {
            $this->data['ugyvezeto_telefonszama'] = $this->request->post['ugyvezeto_telefonszama'];
        } else {
            $this->data['ugyvezeto_telefonszama'] = '';
        }

        if (isset($this->request->post['country_id'])) {
            $this->data['country_id'] = $this->request->post['country_id'];
        } else {
            $this->data['country_id'] = $this->config->get('config_country_id');
        }

        if (isset($this->request->post['zone_id'])) {
            $this->data['zone_id'] = $this->request->post['zone_id'];
        } else {
            $this->data['zone_id'] = '';
        }

        if (isset($this->request->post['newsletter'])) {
            $this->data['newsletter'] = $this->request->post['newsletter'];
        } else {
            $this->data['newsletter'] = '';
        }

        if (isset($this->request->post['newslettercategories'])) {
            $this->data['newslettercategories'] = $this->request->post['newslettercategories'];
        } else {
            $this->data['newslettercategories'] = array();
        }

        $this->load->model('localisation/country');

        $this->data['countries'] = $this->model_localisation_country->getCountries();
        $this->data['eletkors'] = $this->model_localisation_country->getEletkor();

        $this->load->model('account/iskolaivegzettseg');

        $this->data['iskolai_vegzettsegek'] = $this->model_account_iskolaivegzettseg->getAll();

        $this->data['nems'][] = array(
            'ertek' => 1,
            'nev'   => $this->language->get('select_ferfi')
        );

        $this->data['nems'][] = array(
            'ertek' => 2,
            'nev'   => $this->language->get('select_no')
        );


        if (isset($this->request->post['password'])) {
            $this->data['password'] = $this->request->post['password'];
        } else {
            $this->data['password'] = '';
        }

        if (isset($this->request->post['confirm'])) {
            $this->data['confirm'] = $this->request->post['confirm'];
        } else {
            $this->data['confirm'] = '';
        }

        if (isset($this->request->post['newsletter'])) {
            $this->data['newsletter'] = $this->request->post['newsletter'];
        } else {
            $this->data['newsletter'] = '0';
        }

        if ($this->config->get('config_account_id')) {

            $elolvastam_informaciok = $this->config->get('megjelenit_elolvastam_informaciok');
            $elolvastam = $this->config->get('megjelenit_elolvastam');
            $elolvastam = html_entity_decode($elolvastam[$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8');
            $this->load->model('catalog/information');

            if ($elolvastam_informaciok) {
                $information_tomb = array();

                $elolvastam_informaciok =$this->config->rendezes($elolvastam_informaciok,"sort_order");
                foreach($elolvastam_informaciok as $elolvastam_informacio) {
                    $information_info = $this->model_catalog_information->getInformation($elolvastam_informacio['information_id']);
                    $information_tomb[] = $this->url->link('information/information/info', 'information_id='.$elolvastam_informacio['information_id'], 'SSL');
                    $information_tomb[] = $information_info['title'];
                    $information_tomb[] = $information_info['title'];

                }

                $this->data['text_agree'] = vsprintf($elolvastam, $information_tomb);
            }

        } else {
            $this->data['text_agree'] = '';
        }

        if (isset($this->request->post['agree'])) {
            $this->data['agree'] = $this->request->post['agree'];
        } else {
            $this->data['agree'] = false;
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/register.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/account/register.tpl';
        } else {
            $this->template = 'default/template/account/register.tpl';
        }
        $this->data['ceges'] = isset($_REQUEST['feltolto']) ? $_REQUEST['feltolto'] : 0;
        $this->data['regisztracio'] = 1;

        $this->load->model('catalog/category');
        $this->data['categories'] = $this->model_catalog_category->getCategories(0);


        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        $this->response->setOutput($this->render());
    }

    private function validate($email2_ell=false) {
        $ceges = ($this->request->post['feltolto'] == 1);

        if ( ($this->config->get('megjelenit_regisztracioblokk_szemelyes') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracioblokk_ceges_szemelyes') == 1 && $ceges) ) {


            if ($this->config->get('megjelenit_regisztracio_cegnev') == 1 && $this->config->get('megjelenit_regisztracio_cegnev_kotelezo') == 1) {
                if ((utf8_strlen($this->request->post['company']) < 3) || (utf8_strlen($this->request->post['company']) > 32)) {
                    $this->error['company'] = $this->language->get('error_company');
                }
            }

            if ($this->config->get('megjelenit_regisztracio_adoszam') == 1 && $this->config->get('megjelenit_regisztracio_adoszam_kotelezo') == 1) {
                if ((utf8_strlen($this->request->post['adoszam']) < 3) || (utf8_strlen($this->request->post['adoszam']) > 32)) {
                    $this->error['adoszam'] = $this->language->get('error_adoszam');
                }
            }

            if ($this->config->get('megjelenit_regisztracio_vallalkozasi_forma') == 1 && $this->config->get('megjelenit_regisztracio_vallalkozasi_forma_kotelezo') == 1) {
                if ((utf8_strlen($this->request->post['vallalkozasi_forma']) < 3) || (utf8_strlen($this->request->post['vallalkozasi_forma']) > 32)) {
                    $this->error['vallalkozasi_forma'] = $this->language->get('error_vallalkozasi_forma');
                }
            }
            if ($this->config->get('megjelenit_regisztracio_szekhely') == 1 && $this->config->get('megjelenit_regisztracio_szekhely_kotelezo') == 1) {
                if ((utf8_strlen($this->request->post['szekhely']) < 3) || (utf8_strlen($this->request->post['szekhely']) > 32)) {
                    $this->error['szekhely'] = $this->language->get('error_szekhely');
                }
            }
            if ($this->config->get('megjelenit_regisztracio_ugyvezeto_neve') == 1 && $this->config->get('megjelenit_regisztracio_ugyvezeto_neve_kotelezo') == 1) {
                if ((utf8_strlen($this->request->post['ugyvezeto_neve']) < 3) || (utf8_strlen($this->request->post['ugyvezeto_neve']) > 32)) {
                    $this->error['ugyvezeto_neve'] = $this->language->get('error_ugyvezeto_neve');
                }
            }
            if ($this->config->get('megjelenit_regisztracio_ugyvezeto_telefonszama') == 1 && $this->config->get('megjelenit_regisztracio_ugyvezeto_telefonszama_kotelezo') == 1) {
                if ((utf8_strlen($this->request->post['ugyvezeto_telefonszama']) < 3) || (utf8_strlen($this->request->post['ugyvezeto_telefonszama']) > 32)) {
                    $this->error['ugyvezeto_telefonszama'] = $this->language->get('error_ugyvezeto_telefonszama');
                }
            }













            if ( ($this->config->get('megjelenit_regisztracio_vezeteknev') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_vezeteknev') == 1 && $ceges) ) {
                if ((utf8_strlen($this->request->post['firstname']) < 1) || (utf8_strlen($this->request->post['firstname']) > 32)) {
                    $this->error['firstname'] = $this->language->get('error_firstname');
                }
            }

            if (($this->config->get('megjelenit_regisztracio_keresztnev') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_keresztnev') == 1 && $ceges)) {
                if ((utf8_strlen($this->request->post['lastname']) < 1) || (utf8_strlen($this->request->post['lastname']) > 32)) {
                    $this->error['lastname'] = $this->language->get('error_lastname');
                }
            }

            if (($this->config->get('megjelenit_regisztracio_email') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_email') == 1 && $ceges)) {
                if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
                    $this->error['email'] = $this->language->get('error_email');
                }
                if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
                    $this->error['warning'] = $this->language->get('error_exists');
                }
            }

            if (($this->config->get('megjelenit_regisztracio_email_megerosites') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_email_megerosites') == 1 && $ceges)) {
                if ($email2_ell && $this->request->post['email_again'] != $this->request->post['email'] ){
                    $this->error['email'] = $this->language->get('error_email');
                    $this->error['email_again'] = $this->language->get('error_email');
                }
            }

            if (($this->config->get('megjelenit_eletkor_regisztracio') == 1 && !$ceges) || ($this->config->get('megjelenit_eletkor_regisztracio_ceges') == 1 && $ceges) ) {
                if ($this->request->post['eletkor'] == '') {
                    $this->error['eletkor'] = $this->language->get('error_eletkor');
                }
            }

            if ( ($this->config->get('megjelenit_nem_regisztracio') == 1 && !$ceges) || ($this->config->get('megjelenit_nem_regisztracio_ceges') == 1 && $ceges) ) {
                if ($this->request->post['nem'] == '') {
                    $this->error['nem'] = $this->language->get('error_nem');
                }
            }

            if (($this->config->get('megjelenit_regisztracio_telefon') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_telefon') == 1 && $ceges)) {
                if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
                    $this->error['telephone'] = $this->language->get('error_telephone');
                }
            }
        }

        if ( ($this->config->get('megjelenit_regisztracioblokk_cim') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracioblokk_ceges_cim') == 1 && $ceges) ) {
            if ( ($this->config->get('megjelenit_regisztracio_utca') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_utca') == 1 && $ceges) ) {
                if ((utf8_strlen($this->request->post['address_1']) < 3) || (utf8_strlen($this->request->post['address_1']) > 128)) {
                    $this->error['address_1'] = $this->language->get('error_address_1');
                }
            }

            if ( ($this->config->get('megjelenit_regisztracio_varos') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_varos') == 1 && $ceges)) {
                if ((utf8_strlen($this->request->post['city']) < 2) || (utf8_strlen($this->request->post['city']) > 128)) {
                    $this->error['city'] = $this->language->get('error_city');
                }
            }

            if ( ($this->config->get('megjelenit_regisztracio_orszag') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_orszag') == 1 && $ceges) ) {
                $this->load->model('localisation/country');

                $country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);
                if ( ($this->config->get('megjelenit_regisztracio_iranyitoszam') == 1) && !$ceges || ($this->config->get('megjelenit_regisztracio_ceges_iranyitoszam') == 1 && $ceges) ) {
                    if ($country_info && $country_info['postcode_required'] && (utf8_strlen($this->request->post['postcode']) < 2) || (utf8_strlen($this->request->post['postcode']) > 10)) {
                        $this->error['postcode'] = $this->language->get('error_postcode');
                    }
                }
            } else {
                if ( ($this->config->get('megjelenit_regisztracio_iranyitoszam') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_iranyitoszam') == 1 && $ceges) ) {
                    if ((utf8_strlen($this->request->post['postcode']) < 2) || (utf8_strlen($this->request->post['postcode']) > 10)) {
                        $this->error['postcode'] = $this->language->get('error_postcode');
                    }
                }
            }

            if ( ($this->config->get('megjelenit_regisztracio_orszag') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_orszag') == 1 && $ceges) ) {
                if ($this->request->post['country_id'] == '') {
                    $this->error['country'] = $this->language->get('error_country');
                }
            }

            if ( ($this->config->get('megjelenit_regisztracio_megye') == 1 && !$ceges) || ($this->config->get('megjelenit_regisztracio_ceges_megye') == 1 && $ceges) ) {
                if ($this->request->post['zone_id'] == '') {
                    $this->error['zone'] = $this->language->get('error_zone');
                }
            }
        }

        if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
            $this->error['password'] = $this->language->get('error_password');
        }

        if ($this->request->post['confirm'] != $this->request->post['password']) {
            $this->error['confirm'] = $this->language->get('error_confirm');
        }

        if ($this->config->get('config_account_id')) {

            if (!isset($this->request->post['agree'])) {
                $this->load->model('catalog/information');
                $elolvastam_informaciok = $this->config->get('megjelenit_elolvastam_informaciok');
                foreach($elolvastam_informaciok as $elolvastam_informacio) {
                    break;
                }
                $information_info = $this->model_catalog_information->getInformation($elolvastam_informacio['information_id']);

                $this->error['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
            }
        }



        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    public function zone() {
        $output = '<option value="">' . $this->language->get('text_select') . '</option>';

        $this->load->model('localisation/zone');

        $results = $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']);

        foreach ($results as $result) {
            $output .= '<option value="' . $result['zone_id'] . '"';

            if (isset($this->request->get['zone_id']) && ($this->request->get['zone_id'] == $result['zone_id'])) {
                $output .= ' selected="selected"';
            }

            $output .= '>' . $result['name'] . '</option>';
        }

        if (!$results) {
            $output .= '<option value="0">' . $this->language->get('text_none') . '</option>';
        }

        $this->response->setOutput($output);
    }
}
?>