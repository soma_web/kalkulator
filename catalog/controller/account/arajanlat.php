<?php
class ControllerAccountArajanlat extends Controller {

    private $error = array();

    public function index() {
	
	    if (!$this->customer->isLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');
	  
	  		$this->redirect($this->url->link('account/login', '', 'SSL'));
    	} 
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('account/arajanlat');
		
		$this->getList();
  	}
  

  	public function toPdf() {
        //header('Status: 302');

       // header('Location: http://localhost/alaprendszer/system/pdfmaker/index.php');

        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/arajanlat', '', 'SSL');
            $this->redirect($this->url->link('account/login', '', 'SSL'));

        }
        $this->language->load('account/arajanlat');

        $this->load->model('account/arajanlat');

       /* if (isset($this->request->get['arajanlat_id'])) {

            $this->session->data['success'] = $this->language->get('text_success_pdf');

            $url = '';

            if (isset($this->request->get['filter_azonosito_arajanlat'])) {
                $url .= '&filter_azonosito_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_azonosito_arajanlat'], ENT_QUOTES, 'UTF-8'));
            }
            if (isset($this->request->get['filter_name_arajanlat'])) {
                $url .= '&filter_name_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_name_arajanlat'], ENT_QUOTES, 'UTF-8'));
            }
            if (isset($this->request->get['filter_customer_arajanlat'])) {
                $url .= '&filter_customer_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_customer_arajanlat'], ENT_QUOTES, 'UTF-8'));
            }
            if (isset($this->request->get['filter_date_added_arajanlat'])) {
                $url .= '&filter_date_added_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_added_arajanlat'], ENT_QUOTES, 'UTF-8'));
            }
            if (isset($this->request->get['filter_date_modified_arajanlat'])) {
                $url .= '&filter_date_modified_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_modified_arajanlat'], ENT_QUOTES, 'UTF-8'));
            }
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->redirect($this->url->link('account/arajanlat', $url, 'SSL'));
        }*/

        $this->getPdf();


     $this->model_account_arajanlat->toPdf($this->output);



    }

  	public function szerkeszt() {
        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/arajanlat', '', 'SSL');

            $this->redirect($this->url->link('account/login', '', 'SSL'));
        }
        $this->language->load('account/arajanlat');

        $this->load->model('account/arajanlat');

        if (isset($this->request->get['arajanlat_id']) && $this->validateSzerkeszt()) {
            $this->model_account_arajanlat->szerkesztArajanlat($this->request->get['arajanlat_id']);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_azonosito_arajanlat'])) {
                $url .= '&filter_azonosito_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_azonosito_arajanlat'], ENT_QUOTES, 'UTF-8'));
            }
            if (isset($this->request->get['filter_name_arajanlat'])) {
                $url .= '&filter_name_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_name_arajanlat'], ENT_QUOTES, 'UTF-8'));
            }
            if (isset($this->request->get['filter_customer_arajanlat'])) {
                $url .= '&filter_customer_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_customer_arajanlat'], ENT_QUOTES, 'UTF-8'));
            }
            if (isset($this->request->get['filter_date_added_arajanlat'])) {
                $url .= '&filter_date_added_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_added_arajanlat'], ENT_QUOTES, 'UTF-8'));
            }
            if (isset($this->request->get['filter_date_modified_arajanlat'])) {
                $url .= '&filter_date_modified_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_modified_arajanlat'], ENT_QUOTES, 'UTF-8'));
            }
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->redirect($this->url->link('account/arajanlat', $url, 'SSL'));
        }
        $this->getList();


    }

  	public function update() {
	    if (!$this->customer->isLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('account/arajanlat', '', 'SSL');

	  		$this->redirect($this->url->link('account/login', '', 'SSL'));
    	}
    	$this->language->load('account/arajanlat');

    	$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/arajanlat');

    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_account_arajanlat->editArajanlat($this->request->get['arajanlat_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success_update');

			$url = '';

			if (isset($this->request->get['filter_azonosito_arajanlat'])) {
				$url .= '&filter_azonosito_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_azonosito_arajanlat'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_name_arajanlat'])) {
				$url .= '&filter_name_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_name_arajanlat'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_customer_arajanlat'])) {
				$url .= '&filter_customer_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_customer_arajanlat'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_date_added_arajanlat'])) {
				$url .= '&filter_date_added_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_added_arajanlat'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_date_modified_arajanlat'])) {
				$url .= '&filter_date_modified_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_modified_arajanlat'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('account/arajanlat', $url, 'SSL'));
		}

    	$this->getForm();
  	}

  	public function delete() {
	if (!$this->customer->isLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

	  		$this->redirect($this->url->link('account/login', '', 'SSL'));
    	}
    	$this->language->load('account/arajanlat');

    	$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/arajanlat');

		if ( isset($this->request->post['selected']) && $this->validateDelete() ) {
			foreach ($this->request->post['selected'] as $arajanlat_id) {
				$this->model_account_arajanlat->deleteArajanlat($arajanlat_id);
	  		}

			$this->session->data['success'] = $this->language->get('text_success_delete');

			$url = '';

			if (isset($this->request->get['filter_azonosito_arajanlat'])) {
				$url .= '&filter_azonosito_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_azonosito_arajanlat'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_name_arajanlat'])) {
				$url .= '&filter_name_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_name_arajanlat'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_customer_arajanlat'])) {
				$url .= '&filter_customer_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_customer_arajanlat'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_date_added_arajanlat'])) {
				$url .= '&filter_date_added_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_added_arajanlat'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_date_modified_arajanlat'])) {
				$url .= '&filter_date_modified_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_modified_arajanlat'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('account/arajanlat', $url, 'SSL'));
		}

    	$this->getList();
  	}

  	public function copy() {
	if (!$this->customer->isLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

	  		$this->redirect($this->url->link('account/login', '', 'SSL'));
    	}
    	$this->language->load('account/arajanlat');

    	$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/arajanlat');

		if (isset($this->request->post['selected']) && $this->validateCopy()) {
			foreach ($this->request->post['selected'] as $arajanlat_id) {
				$this->model_account_arajanlat->copyArajanlat($arajanlat_id);
	  		}


			$this->session->data['success'] = $this->language->get('text_success_copy');

			$url = '';

			if (isset($this->request->get['filter_azonosito_arajanlat'])) {
				$url .= '&filter_azonosito_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_azonosito_arajanlat'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_name_arajanlat'])) {
				$url .= '&filter_name_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_name_arajanlat'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_customer_arajanlat'])) {
				$url .= '&filter_customer_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_customer_arajanlat'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_date_added_arajanlat'])) {
				$url .= '&filter_date_added_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_added_arajanlat'], ENT_QUOTES, 'UTF-8'));
			}
			if (isset($this->request->get['filter_date_modified_arajanlat'])) {
				$url .= '&filter_date_modified_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_modified_arajanlat'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('account/arajanlat', $url, 'SSL'));
		}

    	$this->getList();
  	}

  	protected function getList() {


		$filter_azonosito_arajanlat     = isset($this->request->get['filter_azonosito_arajanlat']) ? $this->request->get['filter_azonosito_arajanlat'] : null;
		$filter_name_arajanlat 	 	    = isset($this->request->get['filter_name_arajanlat']) ? $this->request->get['filter_name_arajanlat'] : null;
		$filter_customer_arajanlat 	    = isset($this->request->get['filter_customer_arajanlat']) ? $this->request->get['filter_customer_arajanlat'] : null;
		$filter_date_added_arajanlat 	= isset($this->request->get['filter_date_added_arajanlat']) ? $this->request->get['filter_date_added_arajanlat'] : null;
		$filter_date_modified_arajanlat = isset($this->request->get['filter_date_modified_arajanlat']) ? $this->request->get['filter_date_modified_arajanlat'] : null;
		$sort 				= isset($this->request->get['sort']) ? $this->request->get['sort'] : 'arajanlat_id';
		$order 				= isset($this->request->get['order']) ? $this->request->get['order'] : 'DESC';
		$page 				= isset($this->request->get['page']) ? $this->request->get['page'] : 1;


        $url = '';

        if (isset($this->request->get['filter_azonosito_arajanlat'])) {
            $url .= '&filter_azonosito_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_azonosito_arajanlat'], ENT_QUOTES, 'UTF-8'));
        }
		if (isset($this->request->get['filter_name_arajanlat'])) {
			$url .= '&filter_name_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_name_arajanlat'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_customer_arajanlat'])) {
			$url .= '&filter_customer_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_customer_arajanlat'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_date_added_arajanlat'])) {
			$url .= '&filter_date_added_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_added_arajanlat'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_date_modified_arajanlat'])) {
			$url .= '&filter_date_modified_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_modified_arajanlat'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}



		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home','SSL'),
      		'separator' => false
   		);
        $this->language->load('account/account');
        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_account'),
            'href'      => $this->url->link('account/account', 'SSL'),
            'separator' => $this->language->get('text_separator')
        );
        $this->language->load('account/arajanlat');
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('account/arajanlat', $url, 'SSL'),
      		'separator' => $this->language->get('text_separator')
   		);

        $this->document->setTitle($this->language->get('heading_title'));

		$this->data['mezok'] = $this->config->get("megjelenit_admin_product");
		$this->data['copy'] = $this->url->link('account/arajanlat/copy', $url, 'SSL');
		$this->data['delete'] = $this->url->link('account/arajanlat/delete', $url, 'SSL');

		$this->data['products'] = array();

		$data = array(
			'filter_azonosito_arajanlat'	    => $filter_azonosito_arajanlat,
			'filter_name_arajanlat'	            => $filter_name_arajanlat,
			'filter_customer_arajanlat'         => $filter_customer_arajanlat,
			'filter_date_added_arajanlat'       => $filter_date_added_arajanlat,
			'filter_date_modified_arajanlat'    => $filter_date_modified_arajanlat,
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           => $this->config->get('config_admin_limit')
		);

		$this->load->model('tool/image');
		$this->load->model('account/arajanlat');

		$arajanlat_total = $this->model_account_arajanlat->getArajanlats($data,'total');
		$results 		 = $this->model_account_arajanlat->getArajanlats($data);

		$arajanlat = array();
		if ($results) {
			foreach ($results as $result) {
				$action = array();

                 if (file_exists(DIR_TEMPLATE_IMAGE . $this->config->get('config_template') . '/image/download.png')) {
                     $image_icon = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/download.png';
                } else if (file_exists(DIR_TEMPLATE_IMAGE."default/image/download.png")) {
                     $image_icon = DIR_TEMPLATE_IMAGE."default/image/download.png";
                }  else if (file_exists(DIR_IMAGE_NYOMTAT."data/download.png")) {
                     $image_icon = DIR_IMAGE_NYOMTAT."data/download.png";
                } else {
                     $image_icon = "";
                 }


                $action[] = array(
                    'text'  => $this->language->get('button_pdf'),
                    'image' => $image_icon,
                    'href'  => $this->url->link('account/arajanlat/toPdf', '&arajanlat_id=' . $result['arajanlat_id'] . $url, 'SSL')
                );

                $action[] = array(
                    'text' => $this->language->get('button_szerkeszt'),
                    'href' => $this->url->link('account/arajanlat/szerkeszt', '&arajanlat_id=' . $result['arajanlat_id'] . $url, 'SSL')
                );

				$action[] = array(
					'text' => $this->language->get('button_edit'),
					'href' => $this->url->link('account/arajanlat/update', '&arajanlat_id=' . $result['arajanlat_id'] . $url, 'SSL')
				);

				$customer = $this->model_account_arajanlat->getCustomer($result['customer_id']);
				$arajanlat[] = array(
					'arajanlat_id'	=>	$result['arajanlat_id'],
					'name'			=>	$result['name'],
					'customer'		=>	$result['vevo_neve'],
					'customer_id'	=>	$result['customer_id'],
					'date_added'	=>	$result['date_added'],
					'date_modified'	=>	$result['date_modified'],
					'action'            => $action,
					'selected'   	=> isset($this->request->post['selected']) && in_array($result['arajanlat_id'], $this->request->post['selected'])

				);
			}
    	}
		$this->data['arajanlatok'] = $arajanlat;

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['text_delete'] = $this->language->get('text_delete');

		$this->data['column_azonosito'] = $this->language->get('column_azonosito');
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_customer'] = $this->language->get('column_customer');
		$this->data['column_price'] = $this->language->get('column_price');
		$this->data['column_quantity'] = $this->language->get('column_quantity');
		$this->data['column_date_added'] = $this->language->get('column_date_added');
		$this->data['column_date_modified'] = $this->language->get('column_date_modified');

		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_copy'] = $this->language->get('button_copy');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');


 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';
        if (isset($this->request->get['filter_azonosito_arajanlat'])) {
            $url .= '&filter_azonosito_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_azonosito_arajanlat'], ENT_QUOTES, 'UTF-8'));
        }
		if (isset($this->request->get['filter_name_arajanlat'])) {
			$url .= '&filter_name_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_name_arajanlat'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_customer_arajanlat'])) {
			$url .= '&filter_customer_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_customer_arajanlat'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_date_added_arajanlat'])) {
			$url .= '&filter_date_added_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_added_arajanlat'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_date_modified_arajanlat'])) {
			$url .= '&filter_date_modified_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_modified_arajanlat'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_azonosito'] 	 = $this->url->link('account/arajanlat', '&sort=arajanlat_id' . $url, 'SSL');
		$this->data['sort_name'] 	 = $this->url->link('account/arajanlat', '&sort=name' . $url, 'SSL');
		$this->data['sort_customer'] = $this->url->link('account/arajanlat', '&sort=customer' . $url, 'SSL');
		$this->data['sort_date_added'] = $this->url->link('account/arajanlat', '&sort=date_added' . $url, 'SSL');
		$this->data['sort_date_modified'] = $this->url->link('account/arajanlat', '&sort=date_modified' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['filter_azonosito_arajanlat'])) {
			$url .= '&filter_azonosito_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_azonosito_arajanlat'], ENT_QUOTES, 'UTF-8'));
		}
        if (isset($this->request->get['filter_name_arajanlat'])) {
			$url .= '&filter_name_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_name_arajanlat'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_customer_arajanlat'])) {
			$url .= '&filter_customer_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_customer_arajanlat'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_date_added_arajanlat'])) {
			$url .= '&filter_date_added_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_added_arajanlat'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_date_modified_arajanlat'])) {
			$url .= '&filter_date_modified_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_modified_arajanlat'], ENT_QUOTES, 'UTF-8'));
		}



		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $arajanlat_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('account/arajanlat', $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_azonosito_arajanlat']       = $filter_azonosito_arajanlat;
		$this->data['filter_name_arajanlat']            = $filter_name_arajanlat;
		$this->data['filter_customer_arajanlat']        = $filter_customer_arajanlat;
		$this->data['filter_date_added_arajanlat']      = $filter_date_added_arajanlat;
		$this->data['filter_date_modified_arajanlat']   = $filter_date_modified_arajanlat;

		$this->data['copy'] = $this->url->link('account/arajanlat/copy', $url, 'SSL');

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;


		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/arajanlat_list.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/account/arajanlat_list.tpl';
			} else {
				$this->template = 'default/template/account/arajanlat_list.tpl';
			}


		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
            'module/cart_elhelyezkedes',
			'common/header'
		);

		$this->response->setOutput($this->render());
  	}

    protected function getForm() {

        $this->language->load('account/arajanlat');

    	$this->data['heading_modosit'] = $this->language->get('heading_modosit');
    	$this->data['heading_azonosito'] = $this->language->get('heading_azonosito');
    	$this->data['heading_neve'] = $this->language->get('heading_neve');


        $this->data['column_azonosito'] = $this->language->get('column_azonosito');
        $this->data['column_name'] = $this->language->get('column_name');
        $this->data['column_customer'] = $this->language->get('column_customer');
        $this->data['column_price'] = $this->language->get('column_price');
        $this->data['column_quantity'] = $this->language->get('column_quantity');
        $this->data['column_date_added'] = $this->language->get('column_date_added');
        $this->data['column_date_modified'] = $this->language->get('column_date_modified');
        $this->data['column_customer_email'] = $this->language->get('column_customer_email');
        $this->data['column_arajanlat_keszito_adatai'] = $this->language->get('column_arajanlat_keszito_adatai');
        $this->data['column_arajanlat_kero_adatai'] = $this->language->get('column_arajanlat_kero_adatai');

    	$this->data['column_kep'] 			= $this->language->get('column_kep');
    	$this->data['column_nev'] 			= $this->language->get('column_nev');
    	$this->data['column_model'] 		= $this->language->get('column_model');
    	$this->data['column_mennyiseg'] 	= $this->language->get('column_mennyiseg');
    	$this->data['column_egysegar'] 		= $this->language->get('column_egysegar');
    	$this->data['column_ar_osszesen'] 	= $this->language->get('column_ar_osszesen');
    	$this->data['column_total_osszesen'] 	= $this->language->get('column_total_osszesen');

        $this->data['column_megyseg'] 	= $this->language->get('column_megyseg');
        $this->data['column_listaar'] 	= $this->language->get('column_listaar');
        $this->data['column_engedmeny_szazalek'] 	= $this->language->get('column_engedmeny_szazalek');
        $this->data['column_engedmeny_ertek'] 	= $this->language->get('column_engedmeny_ertek');
        $this->data['column_ertek'] 	= $this->language->get('column_ertek');

        $this->data['fejlec_szekhely'] 	        = $this->language->get('fejlec_szekhely');
        $this->data['fejlec_adoszam'] 	        = $this->language->get('fejlec_adoszam');
        $this->data['fejlec_bank'] 	            = $this->language->get('fejlec_bank');
        $this->data['fejlec_bank_szamla'] 	    = $this->language->get('fejlec_bank_szamla');
        $this->data['fejlec_swift_kod'] 	    = $this->language->get('fejlec_swift_kod');
        $this->data['fejlec_iban_kod'] 	        = $this->language->get('fejlec_iban_kod');
        $this->data['fejlec_ajanlat_szam'] 	    = $this->language->get('fejlec_ajanlat_szam');
        $this->data['fejlec_ajanlat_kelt'] 	    = $this->language->get('fejlec_ajanlat_kelt');
        $this->data['fejlec_szallitasi_hatarido']= $this->language->get('fejlec_szallitasi_hatarido');
        $this->data['fejlec_fizetesi_hatarido'] = $this->language->get('fejlec_fizetesi_hatarido');
        $this->data['fejlec_fizetesi_mod'] 	    = $this->language->get('fejlec_fizetesi_mod');
        $this->data['fejlec_kuldo'] 	        = $this->language->get('fejlec_kuldo');
        $this->data['fejlec_telefon'] 	        = $this->language->get('fejlec_telefon');
        $this->data['fejlec_email'] 	        = $this->language->get('fejlec_email');
        $this->data['fejlec_szallito'] 	        = $this->language->get('fejlec_szallito');
        $this->data['fejlec_vevo'] 	        = $this->language->get('fejlec_vevo');

    	$this->data['button_save'] = $this->language->get('button_save');
    	$this->data['button_cancel'] = $this->language->get('button_cancel');
    	$this->data['button_pdf'] = $this->language->get('button_pdf');


        $this->data['megjelenit_termekful'] = $this->config->get('megjelenit_admin_termekful');


        $this->load->model('tool/image');
        $this->data['no_image_product'] = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));

        if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}


		$url = '';
		if (isset($this->request->get['filter_name_arajanlat'])) {
			$url .= '&filter_name_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_name_arajanlat'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_customer_arajanlat'])) {
			$url .= '&filter_customer_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_customer_arajanlat'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_date_added_arajanlat'])) {
			$url .= '&filter_date_added_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_added_arajanlat'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_date_modified_arajanlat'])) {
			$url .= '&filter_date_modified_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_modified_arajanlat'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}



		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'SSL'),
			'separator' => false
   		);

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_account'),
            'href'      => $this->url->link('account/account', 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('account/arajanlat', $url, 'SSL'),
      		'separator' => $this->language->get('text_separator')
   		);

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_modosit'),
            'href'      => $this->url->link('account/arajanlat/update', '&arajanlat_id=' . $this->request->get['arajanlat_id'] . $url, 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

        $this->document->setTitle($this->language->get('heading_title'));

        $this->data['action'] = $this->url->link('account/arajanlat/update', '&arajanlat_id=' . $this->request->get['arajanlat_id'] . $url, 'SSL');
        $this->data['action_pdf'] = $this->url->link('account/arajanlat/toPdf', '&arajanlat_id=' . $this->request->get['arajanlat_id'] . $url, 'SSL');
		$this->data['arajanlat_id'] = $this->request->get['arajanlat_id'];



		$this->data['cancel'] = $this->url->link('account/arajanlat', $url, 'SSL');

		$arajanlat_info = array();
		if (isset($this->request->get['arajanlat_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      		$arajanlat_info = $this->model_account_arajanlat->getArajanlat($this->request->get['arajanlat_id']);
    	}


        $this->data['arajanlat_info'] = $arajanlat_info;
        $this->load->model('tool/image');

        if ($arajanlat_info) {
			if (isset($arajanlat_info['products'])) {
				foreach($arajanlat_info['products'] as $product_key=>$product) {
                    if ($product['image'] && file_exists(DIR_IMAGE . $product['image'])) {
                        $image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
                    } else {
                        $image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
                    }
                    $this->data['arajanlat_info']['products'][$product_key]['image'] = $image;
				}
			}
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/arajanlat_form.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/account/arajanlat_form.tpl';
			} else {
				$this->template = 'default/template/account/arajanlat_form.tpl';
			}

		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);

		$this->response->setOutput($this->render());
  	}

    protected function getPdf() {

        $this->language->load('account/arajanlat');

        $this->data['heading_modosit'] = $this->language->get('heading_modosit');
        $this->data['heading_azonosito'] = $this->language->get('heading_azonosito');
        $this->data['heading_neve'] = $this->language->get('heading_neve');
        $this->data['heading_arajanlat'] = $this->language->get('heading_arajanlat');


        $this->data['column_azonosito'] = $this->language->get('column_azonosito');
        $this->data['column_name'] = $this->language->get('column_name');
        $this->data['column_customer'] = $this->language->get('column_customer');
        $this->data['column_price'] = $this->language->get('column_price');
        $this->data['column_quantity'] = $this->language->get('column_quantity');
        $this->data['column_date_added'] = $this->language->get('column_date_added');
        $this->data['column_date_modified'] = $this->language->get('column_date_modified');
        $this->data['column_customer_email'] = $this->language->get('column_customer_email');
        $this->data['column_arajanlat_keszito_adatai'] = $this->language->get('column_arajanlat_keszito_adatai');
        $this->data['column_arajanlat_kero_adatai'] = $this->language->get('column_arajanlat_kero_adatai');

        $this->data['column_kep'] 			= $this->language->get('column_kep');
        $this->data['column_nev'] 			= $this->language->get('column_nev');
        $this->data['column_model'] 		= $this->language->get('column_model');
        $this->data['column_mennyiseg'] 	= $this->language->get('column_mennyiseg');
        $this->data['column_egysegar'] 		= $this->language->get('column_egysegar');
        $this->data['column_ar_osszesen'] 	= $this->language->get('column_ar_osszesen');
        $this->data['column_total_osszesen'] 	= $this->language->get('column_total_osszesen');

        $this->data['column_megyseg'] 	= $this->language->get('column_megyseg');
        $this->data['column_listaar'] 	= $this->language->get('column_listaar');
        $this->data['column_engedmeny_szazalek'] 	= $this->language->get('column_engedmeny_szazalek');
        $this->data['column_engedmeny_ertek'] 	= $this->language->get('column_engedmeny_ertek');
        $this->data['column_ertek'] 	= $this->language->get('column_ertek');

        $this->data['fejlec_szekhely'] 	        = $this->language->get('fejlec_szekhely');
        $this->data['fejlec_adoszam'] 	        = $this->language->get('fejlec_adoszam');
        $this->data['fejlec_bank'] 	            = $this->language->get('fejlec_bank');
        $this->data['fejlec_bank_szamla'] 	    = $this->language->get('fejlec_bank_szamla');
        $this->data['fejlec_swift_kod'] 	    = $this->language->get('fejlec_swift_kod');
        $this->data['fejlec_iban_kod'] 	        = $this->language->get('fejlec_iban_kod');
        $this->data['fejlec_ajanlat_szam'] 	    = $this->language->get('fejlec_ajanlat_szam');
        $this->data['fejlec_ajanlat_kelt'] 	    = $this->language->get('fejlec_ajanlat_kelt');
        $this->data['fejlec_szallitasi_hatarido']= $this->language->get('fejlec_szallitasi_hatarido');
        $this->data['fejlec_fizetesi_hatarido'] = $this->language->get('fejlec_fizetesi_hatarido');
        $this->data['fejlec_fizetesi_mod'] 	    = $this->language->get('fejlec_fizetesi_mod');
        $this->data['fejlec_kuldo'] 	        = $this->language->get('fejlec_kuldo');
        $this->data['fejlec_telefon'] 	        = $this->language->get('fejlec_telefon');
        $this->data['fejlec_email'] 	        = $this->language->get('fejlec_email');
        $this->data['fejlec_szallito'] 	        = $this->language->get('fejlec_szallito');
        $this->data['fejlec_vevo'] 	        = $this->language->get('fejlec_vevo');

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['button_pdf'] = $this->language->get('button_pdf');


        $this->data['megjelenit_termekful'] = $this->config->get('megjelenit_admin_termekful');


        $this->load->model('tool/image');
        $this->data['no_image_product'] = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }


        $url = '';
        if (isset($this->request->get['filter_name_arajanlat'])) {
            $url .= '&filter_name_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_name_arajanlat'], ENT_QUOTES, 'UTF-8'));
        }
        if (isset($this->request->get['filter_customer_arajanlat'])) {
            $url .= '&filter_customer_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_customer_arajanlat'], ENT_QUOTES, 'UTF-8'));
        }
        if (isset($this->request->get['filter_date_added_arajanlat'])) {
            $url .= '&filter_date_added_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_added_arajanlat'], ENT_QUOTES, 'UTF-8'));
        }
        if (isset($this->request->get['filter_date_modified_arajanlat'])) {
            $url .= '&filter_date_modified_arajanlat=' . urlencode(html_entity_decode($this->request->get['filter_date_modified_arajanlat'], ENT_QUOTES, 'UTF-8'));
        }
        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }



        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_account'),
            'href'      => $this->url->link('account/account', 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('account/arajanlat', $url, 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_modosit'),
            'href'      => $this->url->link('account/arajanlat/update', '&arajanlat_id=' . $this->request->get['arajanlat_id'] . $url, 'SSL'),
            'separator' => $this->language->get('text_separator')
        );


        $this->data['action'] = $this->url->link('account/arajanlat/update', '&arajanlat_id=' . $this->request->get['arajanlat_id'] . $url, 'SSL');
        $this->data['action_pdf'] = $this->url->link('account/arajanlat/toPdf', '&arajanlat_id=' . $this->request->get['arajanlat_id'] . $url, 'SSL');
        $this->data['arajanlat_id'] = $this->request->get['arajanlat_id'];



        $this->data['cancel'] = $this->url->link('account/arajanlat', $url, 'SSL');

        $arajanlat_info = array();
        if (isset($this->request->get['arajanlat_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $arajanlat_info = $this->model_account_arajanlat->getArajanlat($this->request->get['arajanlat_id']);
        }


        $this->data['arajanlat_info'] = $arajanlat_info;
        $this->load->model('tool/image');

        if ($arajanlat_info) {
            if (isset($arajanlat_info['products'])) {
                foreach($arajanlat_info['products'] as $product_key=>$product) {
                    if ($product['image'] && file_exists(DIR_IMAGE . $product['image'])) {
                        $image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
                    } else {
                        $image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
                    }
                    $this->data['arajanlat_info']['products'][$product_key]['image'] = $image;
                }
            }
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/arajanlat_pdf.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/account/arajanlat_pdf.tpl';
        } else {
            $this->template = 'default/template/account/arajanlat_pdf.tpl';
        }
       $this->response->setOutput($this->render());


    }

  	protected function validateSzerkeszt() {
        if (isset($this->session->data['arajanlat'])) {
            $this->error['warning'] = $this->language->get('error_uritsd_elobb');
        }


        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

  	protected function validateForm() {

    	if (!$this->error) {
			return true;
    	} else {
      		return false;
    	}
  	}

  	protected function validateDelete() {

        /*$this->load->library('user');

        $this->user = new User($this->registry);

    	if (!$this->user->hasPermission('modify', 'account/product')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}*/

		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
  	}

  	protected function validateCopy() {
        /*$this->load->library('user');

        $this->user = new User($this->registry);

    	if (!$this->user->hasPermission('modify', 'account/product')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}*/

		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
  	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name_arajanlat']) || isset($this->request->get['filter_customer_arajanlat']) ) {
			$this->load->model('account/arajanlat');

			if (isset($this->request->get['filter_name_arajanlat'])) {
				$filter_name_arajanlat = $this->request->get['filter_name_arajanlat'];
			} else {
				$filter_name_arajanlat = '';
			}

			if (isset($this->request->get['filter_customer_arajanlat'])) {
				$filter_customer_arajanlat = $this->request->get['filter_customer_arajanlat'];
			} else {
				$filter_customer_arajanlat = '';
			}

			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
			} else {
				$limit = 20;
			}

			$data = array(
				'filter_name_arajanlat'  => $filter_name_arajanlat,
				'filter_customer_arajanlat' => $filter_customer_arajanlat,
				'autocomplete' => true,
				'start'        => 0,
				'limit'        => $limit
			);

			$results = $this->model_account_arajanlat->getArajanlats($data);
			if ($results) {

				foreach ($results as $result) {
					$json[] = array(
						'arajanlat_id'	=>	$result['arajanlat_id'],
						'name'			=>	$result['name'],
						'customer'		=>	($result['firstname'].' '.$result['lastname']),
						'customer_id'	=>	$result['customer_id'],
						'selected'   	=> isset($this->request->post['selected']) && in_array($result['arajanlat_id'], $this->request->post['selected'])

					);

				}
			}
		}

		$this->response->setOutput(json_encode($json));
	}

    public function autocompleteCustomer () {
        $json = array();

        if (isset($this->request->get['customer_name']) && $this->request->get['customer_name']) {
            $this->load->model('account/customer');
            $this->language->load('module/arajanlat');

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 20;
            }

            $data = array(
                'filter_name'  => $this->request->get['customer_name'],
                'start'        => 0,
                'limit'        => $limit
            );

            $results = $this->model_account_customer->getCustomers($data);
            if ($results) {

                foreach ($results as $result) {

                    $sql = "SELECT a.*, c.name AS orszag FROM ".DB_PREFIX."address a
                                LEFT JOIN ".DB_PREFIX."country c ON (c.country_id=a.country_id)
                            WHERE address_id='".$result['address_id']."'";
                    $query = $this->db->query($sql);


                    $json[] = array(
                        'name'			=>	$result['name'],
                        'customer_id'	=>	$result['customer_id'],
                        'email'	        =>	$result['email'],
                        'vevo_varos'    => (isset($query->row['city']) ? $query->row['city'] : ''),
                        'vevo_utca'     => (isset($query->row['address_1']) ? $query->row['address_1'] : ''),
                        'vevo_orszag'   => (isset($query->row['orszag']) ? $query->row['orszag'] : ''),
                        'vevo_adoszam'  => $result['adoszam'],
                        'ajanlat_megszolitas' => sprintf($this->language->get('ajanlat_megszolitas'),$result['name'])


                    );

                }
            }
        }

        $this->response->setOutput(json_encode($json));
    }

}
?>