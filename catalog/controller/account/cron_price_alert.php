<?php   
class ControllerAccountCronPriceAlert extends Controller {
	public function index() {
	
		$this->language->load('mail/price_alert');
		
		$this->load->model('module/price_alert');	
		$this->load->model('catalog/product');	
		
		if (!isset($this->request->get['secret_code'])){
			echo "You forgot secret code";
			exit;
		}
		
		if ($this->request->get['secret_code'] != $this->config->get('price_alert_secret_code')){
			echo "Access Denied: Wrong secret code";
			exit;
		}
		
		$subscribers = $this->model_module_price_alert->getSubscribers();
		
		$subject = $this->language->get('text_subject'); 
		
		foreach($subscribers as $subscriber){
			$message = sprintf($this->language->get('text_hi'), $subscriber['name']) . "\n\n";
			$message .= $this->language->get('text_info') . "\n\n";
			
			$alerts = $this->model_module_price_alert->getSubscriberAlerts($subscriber['email']);
			$sendEmail = false;
			
			foreach($alerts as $alert){
				$product_info = $this->model_catalog_product->getProduct($alert['product_id']);
				
				if ($product_info){
					
					if ($this->currency->getCode() == $alert['currency_code']) {
						$desired_price = $alert['price'];
					} else {
						$desired_price = $this->currency->convert($alert['price'], $alert['currency_code'], $this->currency->getCode());
					}
					
					if ((float)$product_info['special']) {
						$price = $this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax'));
					} else {
						$price = $this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax'));
					}	
					
					
					if ((float)$price <= (float)$desired_price){
						$message .= sprintf($this->language->get('text_product_name'), $this->url->link('product/product', 'product_id=' . $product_info['product_id'], 'SSL'),  $product_info['name']);
						$message .= ' - ' . $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
						$message .= "\n";
						
						$this->model_module_price_alert->deleteAlert($alert['price_alert_id']);
						
						$sendEmail = true;
					}
				}
			}
			
			$message .= "\n\n" . $this->language->get('text_thanks') . "\n";
			$message .= $this->config->get('config_name');
			$message = nl2br($message);
			
			if ($sendEmail){
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');				
				$mail->setTo($subscriber['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($this->config->get('config_name'));
				$mail->setSubject($subject);
				$mail->setHtml($message);
				$mail->send();
				
				echo "Mail sent to: " . $subscriber['email'] . "<br />";
			}
		}
	} 	
}
?>