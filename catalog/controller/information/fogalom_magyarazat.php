<?php 
class ControllerInformationFogalomMagyarazat extends Controller {
	public function index() {  
    	$this->language->load('information/fogalom_magyarazat');
		
		$this->load->model('catalog/fogalom_magyarazat');
        $this->data['button_close'] = $this->language->get('button_close');


        if (isset($this->request->request['fogalom_id'])) {
			$fogalom_id = $this->request->request['fogalom_id'];
		} else {
            $fogalom_id = 0;
		}
		
		$fogalom_info = $this->model_catalog_fogalom_magyarazat->getFogalomMagyarazat($fogalom_id);
   		
		if ($fogalom_info) {


			$this->data['description'] = html_entity_decode($fogalom_info['description'], ENT_QUOTES, 'UTF-8');
			$this->data['name'] = $fogalom_info['name'];

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/fogalom_magyarazat.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/information/fogalom_magyarazat.tpl';
			} else {
				$this->template = 'default/template/information/fogalom_magyarazat.tpl';
			}
            $this->response->setOutput($this->render());


    	}
  	}

}
?>