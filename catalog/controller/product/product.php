<?php
class ControllerProductProduct extends Controller {
	private $error = array();

	public function index() {
        $this->log->setMicroTime();


        $this->language->load('module/package',$this->config->get('config_template'));
        $this->data['package_heading_title'] = $this->language->get('heading_title');

		$this->language->load('product/product');

        $this->data['text_stock_in'] = $this->language->get('text_stock_in');
		$this->data['breadcrumbs'] = array();
        $this->data['module_name'] = "product";


        $this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
			'separator' => false
		);
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');


        $this->data['elonezet'] = !empty($_GET['elonezet']) ? 'elonezet' : '';

		$this->load->model('catalog/category');

		if (isset($this->request->get['path'])) {
			$path = '';

			foreach (explode('_', $this->request->get['path']) as $path_id) {
				if (!$path) {
					$path = $path_id;
				} else {
					$path .= '_' . $path_id;
				}

                if (isset($_SESSION['categoryAll'])) {
                    $category_info = $this->config->in_array_category($_SESSION['categoryAll'],$path_id);
                } else {
                    $category_info = $this->model_catalog_category->getCategory($path_id);
                }
				if ($category_info) {
					$this->data['breadcrumbs'][] = array(
						'text'      => $category_info['name'],
						'href'      => $this->url->link('product/category', 'path=' . $path),
						'separator' => $this->language->get('text_separator')
					);
				}
			}
		}

		$this->load->model('catalog/manufacturer');

		if (isset($this->request->get['manufacturer_id'])) {
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_brand'),
				'href'      => $this->url->link('product/manufacturer'),
				'separator' => $this->language->get('text_separator')
			);

			$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);

			if ($manufacturer_info) {
				$this->data['breadcrumbs'][] = array(
					'text'	    => $manufacturer_info['name'],
					'href'	    => $this->url->link('product/manufacturer/product', 'manufacturer_id=' . $this->request->get['manufacturer_id']),
					'separator' => $this->language->get('text_separator')
				);
			}
		}



		if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_tag'])) {
			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_tag'])) {
				$url .= '&filter_tag=' . $this->request->get['filter_tag'];
			}

			if (isset($this->request->get['filter_description'])) {
				$url .= '&filter_description=' . $this->request->get['filter_description'];
			}

			if (isset($this->request->get['filter_category_id'])) {
				$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
			}

			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_search'),
				'href'      => $this->url->link('product/search', $url),
				'separator' => $this->language->get('text_separator')
			);
		}

		if (isset($this->request->get['product_id'])) {
			$product_id = $this->request->get['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		$product_info       = $this->model_catalog_product->getProduct($product_id);
		$package_info       = $this->model_catalog_product->getProductPackage($product_id);


		if ($product_info) {

            $data1 = array(
                'sort'  => 'p.date_added',
                'order' => 'DESC',
                'start' => 0,
                'limit' => 8
            );
            $results1 = $this->model_catalog_product->getProductsNew($data1);
            $uj=false;
            if (array_key_exists($product_info['product_id'],$results1)){
                $uj=1;
            }

            $this->data['uj'] = $uj;

            $product_info['arengedmeny_szazalek'] = 0;
            if (!empty($product_info['special'])) {
                $product_info['arengedmeny_szazalek'] = round((1-$product_info['special']/$product_info['price'])*100);
            }



            $this->data['product_info'] = $product_info;


            $this->data['qa_status'] = $this->config->get('qa_status');
            if ($this->config->get('qa_status')) {
                if (file_exists('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/qa.css')) {
                    $this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/qa.css');
                } else {
                    $this->document->addStyle('catalog/view/theme/default/stylesheet/qa.css');
                }

                $this->data['tab_qa'] = $this->language->get('tab_qa');

                $this->data['text_ask'] = $this->language->get('text_ask');

                $this->data['entry_question'] = $this->language->get('entry_question');
                $this->data['entry_email'] = $this->language->get('entry_email');
                $this->data['entry_phone'] = $this->language->get('entry_phone');
                $custom_field_names = $this->config->get('qa_form_custom_field_name');
                $this->data['entry_custom'] = $custom_field_names[$this->config->get('config_language_id')] . ':';

                $this->load->model('catalog/qa');

                $this->data['qa_display_questions'] = $this->config->get('qa_display_questions');
                $this->data['qa_form_display_name'] = $this->config->get('qa_form_display_name');
                $this->data['qa_form_require_name'] = $this->config->get('qa_form_require_name');
                $this->data['qa_form_display_email'] = $this->config->get('qa_form_display_email');
                $this->data['qa_form_require_email'] = $this->config->get('qa_form_require_email');
                $this->data['qa_form_display_phone'] = $this->config->get('qa_form_display_phone');
                $this->data['qa_form_require_phone'] = $this->config->get('qa_form_require_phone');
                $this->data['qa_form_display_custom_field'] = $this->config->get('qa_form_display_custom_field');
                $this->data['qa_form_require_custom_field'] = $this->config->get('qa_form_require_custom_field');
                $this->data['qa_form_display_captcha'] = $this->config->get('qa_form_display_captcha');
                $this->data['qa_form_require_captcha'] = $this->config->get('qa_form_require_captcha');
                $this->data['preload'] = (int)$this->config->get("qa_preload");

                $this->data['q_name'] = ($this->customer->isLogged()) ? $this->customer->getFirstName() . " " . $this->customer->getLastName() : "";
                $this->data['q_email'] = ($this->customer->isLogged()) ? $this->customer->getEmail() : "";

                if ($this->data['preload'] == 1) {
                    $this->data['qas'] =  $this->question($product_id, 1, false);
                } else if ($this->data['preload'] == 2) {
                    $this->data['qas'] =  $this->question($product_id, 1, false, 0);
                } else {
                    $this->data['qas'] =  '';
                }

                $this->data['qa_count'] = $this->model_catalog_qa->getTotalQuestionsByProductId($this->request->get['product_id']);
            }

            $url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_tag'])) {
				$url .= '&filter_tag=' . $this->request->get['filter_tag'];
			}

			if (isset($this->request->get['filter_description'])) {
				$url .= '&filter_description=' . $this->request->get['filter_description'];
			}

			if (isset($this->request->get['filter_category_id'])) {
				$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
			}

			$this->data['breadcrumbs'][] = array(
				'text'      => $product_info['name'],
				'href'      => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id']),
				'separator' => $this->language->get('text_separator')
			);

			$this->document->setTitle($product_info['name']);
			$this->document->setDescription($product_info['meta_description']);
			$this->document->setKeywords($product_info['meta_keyword']);
			$this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id']), 'canonical');

			$this->data['heading_title']    = $product_info['name'];

            $this->data['text_eredeti_ar']      = $this->language->get('text_eredeti_ar');
            $this->data['text_cikkszam']            = $this->language->get('text_cikkszam');
            $this->data['text_lizing']          = $this->language->get('text_lizing');
            $this->data['text_pcs'] = $this->language->get('text_pcs');
            $this->data['text_out_of_stock'] = $this->language->get('text_out_of_stock');
            $this->data['kaphato']              = $this->language->get('kaphato');
			$this->data['text_select']          = $this->language->get('text_select');
			$this->data['text_netto_ar']        = $this->language->get('text_netto_ar');
			$this->data['text_plus_afa']        = $this->language->get('text_plus_afa');
			$this->data['text_manufacturer']    = $this->language->get('text_manufacturer');
			$this->data['text_model']           = $this->language->get('text_model');
			$this->data['text_reward']          = $this->language->get('text_reward');
			$this->data['text_points']          = $this->language->get('text_points');
			$this->data['text_stock']           = $this->language->get('text_stock');
			$this->data['text_price']           = $this->language->get('text_price');
			$this->data['text_tax']             = $this->language->get('text_tax');
			$this->data['text_nincs_raktaron']  = $this->language->get('text_nincs_raktaron');
			$this->data['text_raktaron']        = $this->language->get('text_raktaron');
			$this->data['text_compare']         = $this->language->get('text_compare');
			$this->data['text_wishlist']        = $this->language->get('text_wishlist');
			$this->data['text_tax_brutto']      = $this->language->get('text_tax_brutto');
			$this->data['text_discount']        = $this->language->get('text_discount');
			$this->data['text_option']          = $this->language->get('text_option');
			$this->data['text_qty']             = $this->language->get('text_qty');
			$this->data['text_minimum']         = sprintf($this->language->get('text_minimum'), $product_info['minimum'],$product_info['megyseg']);
			$this->data['text_or']              = $this->language->get('text_or');
			$this->data['text_write']           = $this->language->get('text_write');
			$this->data['text_note']            = $this->language->get('text_note');
			$this->data['text_share']           = $this->language->get('text_share');
			$this->data['text_megyseg']         = $this->language->get('text_megyseg');
			$this->data['text_raktarinfo']      = $this->language->get('text_raktarinfo');
			$this->data['text_wait']            = $this->language->get('text_wait');
			$this->data['text_tags']            = $this->language->get('text_tags');
            $this->data['text_view']            = $this->language->get('text_view');
			$this->data['text_suly']            = $this->language->get('text_suly');
			$this->data['text_pardarab']        = $this->language->get('text_pardarab');
            $this->data['text_bejelentkezes']   = $this->language->get('text_bejelentkezes');
            $this->data['text_bejelentkezes_button'] = $this->language->get('text_bejelentkezes_button');
            $this->data['bejelentkezes']        = $this->url->link('account/login', '', 'SSL');
            $this->data['text_netto']           = $this->language->get('text_netto');
            $this->data['text_raktar']          = $this->language->get('text_raktar');
            $this->data['text_eredeti_ar']      = $this->language->get('text_eredeti_ar');
            $this->data['text_temkekdij']       = $this->language->get('text_temkekdij');
            $this->data['text_total']           = $this->language->get('text_total');


            $this->data['entry_name']       = $this->language->get('entry_name');
			$this->data['entry_review']     = $this->language->get('entry_review');
			$this->data['entry_rating']     = $this->language->get('entry_rating');
			$this->data['entry_good']       = $this->language->get('entry_good');
			$this->data['entry_bad']        = $this->language->get('entry_bad');
			$this->data['entry_captcha']    = $this->language->get('entry_captcha');

            $this->data['button_arajanlat_keszites'] = $this->language->get('button_arajanlat_keszites');
            $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
            $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');
            $this->data['button_set_valos_ar'] = $this->language->get('button_set_valos_ar');
            $this->data['tab_facebook_comments'] = $this->language->get('tab_facebook_comments');
            $this->data['tab_downloads']    = $this->language->get('tab_downloads');
			$this->data['button_cart']      = $this->language->get('button_cart');
			$this->data['button_set_valos_ar']      = $this->language->get('button_set_valos_ar');
			$this->data['button_bovebben']  = $this->language->get('button_bovebben');
			$this->data['button_wishlist']  = $this->language->get('button_wishlist');
			$this->data['button_compare']   = $this->language->get('button_compare');
			$this->data['button_upload']    = $this->language->get('button_upload');
			$this->data['button_continue']  = $this->language->get('button_continue');
			$this->data['button_download']  = $this->language->get('button_download');


            $this->data['informaciok1'] = $this->language->get('informaciok1');
            $this->data['informaciok2'] = $this->language->get('informaciok2');
            $this->data['informaciok3'] = $this->language->get('informaciok3');
            $this->data['informaciok4'] = $this->language->get('informaciok4');
            $this->data['informaciok5'] = $this->language->get('informaciok5');


			$this->load->model('catalog/review');


			$this->data['tab_description']      = $this->language->get('tab_description');
			$this->data['tab_meret']            = $this->language->get('tab_meret');
			$this->data['tab_attribute']        = $this->language->get('tab_attribute');
			$this->data['tab_review']           = sprintf($this->language->get('tab_review'), $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']));
			$this->data['tab_related']          = $this->language->get('tab_related');
			$this->data['tab_package']          = $this->language->get('tab_package');
			$this->data['tab_image']            = $this->language->get('tab_image');
			$this->data['tab_letoltesek']       = $this->language->get('tab_letoltesek');
			$this->data['tab_test_description'] = $this->language->get('tab_test_description');
			$this->data['tab_ajanlott']         = $this->language->get('tab_ajanlott');
			$this->data['tab_helyettesito']     = $this->language->get('tab_helyettesito');
			$this->data['tab_kereso']           = $this->language->get('tab_kereso');
			$this->data['tab_ajandek']          = $this->language->get('tab_ajandek');

			$this->data['product_id']           = $this->request->get['product_id'];
			$this->data['manufacturer']         = $product_info['manufacturer'];
			$this->data['manufacturers']        = $this->url->link('product/manufacturer/product', 'manufacturer_id=' . $product_info['manufacturer_id']);
			$this->data['model']                = $product_info['model'];
			$this->data['cikkszam']             = $product_info['cikkszam'];
			$this->data['cikkszam2']            = $product_info['cikkszam2'];
			$this->data['reward']               = $product_info['reward'];
			$this->data['points']               = $product_info['points'];
			$this->data['utalvany']             = $product_info['utalvany'];

            $this->data['text_akcio_title']     = $this->language->get('text_akcio_title');
            $this->data['text_pardarab_title']  = $this->language->get('text_pardarab_title');
            $this->data['text_uj_title']        = $this->language->get('text_uj_title');
            $this->data['text_ujdonsag_title']  = $this->language->get('text_ujdonsag_title');
            $this->data['text_kifuto_title']    = $this->language->get('text_kifuto_title');
            $this->data['text_facebook_comment']= $this->language->get('text_facebook_comment');
            $this->data['text_ajandek_title']   = $this->language->get('text_ajandek_title');
            $this->data['text_login']           = $this->language->get('text_login');

			$this->data['text_szallitas']       =  $this->language->get('text_szallitas');

            $this->data['informaciok']      = $this->model_catalog_product->getInformationDisplay($product_info);
            $this->data['termekadatok']     = $this->model_catalog_product->getInformationDisplays($product_info);
            $this->data['leiras_karakter_lista']    = $this->config->get('megjelenit_description_lista_karakterek');
            $this->data['leiras_karakter_racs']     = $this->config->get('megjelenit_description_karakterek');


            $this->data['megjelenit_product'] = $this->config->get('megjelenit_product');
            $this->data['megjelenit_termek']  = $this->config->get('termek_beallitas_termek');
            $this->data['megjelenit_kosar']   = $this->config->get('megjelenit_admin_kosar');


            if ($product_info['quantity'] <= 0) {
				$this->data['stock'] = $product_info['stock_status'];
			} elseif ($this->config->get('config_stock_display')) {
				$this->data['stock'] = $product_info['quantity'];
			} else {
				$this->data['stock'] = $this->language->get('text_instock');
			}

			$this->load->model('tool/image');

			if ($product_info['image'] && file_exists(DIR_IMAGE . $product_info['image'])) {
				$this->data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'),true,true);
			} else {
				$this->data['popup'] = '';
			}

			if ($product_info['image'] && file_exists(DIR_IMAGE . $product_info['image'])) {
				$this->data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
			} else {
				$this->data['thumb'] =  $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'),false);
			}

            $this->data['thumb_width'] = $this->config->get('config_image_thumb_width');
            $this->data['thumb_height'] = $this->config->get('config_image_thumb_height');

            $this->data['kapcsolodo_allitott'] = $this->config->get('megjelenit_product_kapcsolodo');

			$this->data['images'] = array();
			$this->data['vantab'] = false;

			$results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);

			foreach ($results as $result) {
				if ( file_exists(DIR_IMAGE . $result['image']) ) {
					$this->data['images'][] = array(
						'popup' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'), true, true),
						'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'))
					);
				} else {
					$this->data['images'][] = array(
						'popup' => $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'), true, true),
						'thumb' => $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'))
					);
				}
			}
            $negativ_ar = "";
            if ($this->config->get('megjelenit_negativ_ar') == "1" && $product_info['utalvany'] == 1) {
                $negativ_ar = "-";
            }

            $this->data['szazalek'] =$product_info['szazalek'];
            $this->data['kifuto']   =$product_info['kifuto'];

            if ($product_info['szazalek'] == 1 && $product_info['utalvany'] == 1) {
                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $this->data['price'] = $negativ_ar.number_format($this->tax->calculate($product_info['price'],$product_info['tax_class_id'],$this->config->get('config_tax')),0,".","." )."%";
                } else {
                    $this->data['price'] = false;
                }
            } else {
    			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
    				$this->data['price'] = $negativ_ar.$this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
    			} else {
    				$this->data['price'] = false;
    			}
            }

            $this->data['mutat_arat'] = ($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price') ? true : false;


            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $this->data['price_netto'] = $this->currency->format($product_info['price']);
                $this->data['price_brutto_value'] = $this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax'));
            } else {
                $this->data['price_netto'] = false;
                $this->data['price_brutto_value'] = false;
            }


            if ( $this->config->get('config_arhoz_magyarazat1') )  {
                $this->data['magyarazat1'] = $this->config->get('config_arhoz_magyarazat1');
            } else {
                $this->data['magyarazat1'] = false;
            }


            $kulcs = 'config_arhoz_magyarazat2'.$this->config->get('config_language_id');
            if ( $this->config->get($kulcs) )  {
                $this->data['magyarazat2'] = html_entity_decode($this->config->get($kulcs), ENT_QUOTES, 'UTF-8');
            } else {
                $this->data['magyarazat2'] = false;
            }



            if ((float)$product_info['special']) {
				$this->data['special_netto'] = $this->currency->format($product_info['special']);
			} else {
				$this->data['special_netto'] = false;
			}

            if ((float)$product_info['special']) {
                $this->data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                $this->data['special_brutto_value'] = $this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax'));
            } else {
                $this->data['special'] = false;
                $this->data['special_brutto_value'] = false;
            }

			if ($this->config->get('config_tax')) {
				$this->data['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
			} else {
				$this->data['tax'] = false;
			}

            if ($product_info['quantity'] > 0 && $product_info['quantity'] <= 4) {
                $this->data['pardarab'] = true;
            } else {
                $this->data['pardarab'] = false;
            }



			$discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);

			$this->data['discounts'] = array();
            $this->data['mennyisegi'] = false;

			foreach ($discounts as $discount) {
                $tax_names = array();
                foreach( $this->tax->getRates($discount['product_id'],$product_info['tax_class_id']) as $name_value) {
                    $tax_names[] = $name_value['name'];
                }
				$this->data['discounts'][] = array(
					'quantity' => $discount['quantity'],
					'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax'))),
					'price_netto' => $this->currency->format($discount['price']),
                    'tax_name' => $tax_names
				);
                if ($discount['quantity'] > 0) {
                    $this->data['mennyisegi'] = true;
                }
			}

            $this->data['price_value'] = $product_info['price'];
            $this->data['special_value'] = $product_info['special'];
            $this->data['tax_value'] = (float)$product_info['special'] ? $product_info['special'] : $product_info['price'];

            $var_currency = array();
            $var_currency['value'] = ($this->currency->getValue() ? $this->currency->getValue() : 1);
            $var_currency['symbol_left'] = $this->currency->getSymbolLeft();
            $var_currency['symbol_right'] = $this->currency->getSymbolRight();
            $var_currency['decimals'] = $this->currency->getDecimalPlace();
            $var_currency['decimal_point'] = $this->language->get('decimal_point');
            $var_currency['thousand_point'] = $this->language->get('thousand_point');
            $this->data['currency'] = $var_currency;

            $this->data['dicounts_unf'] = $discounts;

            $this->data['tax_class_id'] = $product_info['tax_class_id'];
            $this->data['tax_rates'] = $this->tax->getRates(0, $product_info['tax_class_id']);

            $this->data['options'] = array();

            foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) {
                if ($option['type'] == 'select' || $option['type'] == 'radio' || $option['type'] == 'checkbox'
                        || $option['type'] == 'image' || $option['type'] == 'checkbox_qty') {
                    $option_value_data = array();

                    foreach ($option['option_value'] as $option_value) {
                        if (!$option_value['subtract'] || ($option_value['quantity'] > -1)) {
                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                $price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                            } else {
                                $price = false;
                            }


                            $option_price = ($option['type'] == 'checkbox_qty' && $option_value['price_prefix'] != '-' )  ? $this->cart->optionKedvezmeny($option_value['price'],$product_id) : $option_value['price'];
                            if (empty($option_value['name'])) {
                                $option_value['name'] = $this->model_catalog_product->getProductOptionsName($option_value['option_value_id']);
                            }

                            $option_value_data[] = array(
                                'subtract'                => $option_value['subtract'],
                                'checked'                 => $option_value['checked'],
                                'quantity'                => $option_value['quantity'],
                                'price_value_tax'         => $this->tax->calculate($option_price, $product_info['tax_class_id'], $this->config->get('config_tax')),
                                'price_value'             => $option_price,
                                'price_display_brutto'    => $this->currency->format($this->tax->calculate($option_price, $product_info['tax_class_id'], $this->config->get('config_tax'))),
                                'price_display_netto'     => $this->currency->format($option_price),
                                'points_value'            => intval($option_value['points_prefix'].$option_value['points']),
                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id'         => $option_value['option_value_id'],
                                'name'                    => $option_value['name'],
                                'heading'                 => nl2br($option_value['heading']),
                                'image'                   => $this->model_tool_image->resize($option_value['image'], 70, 70),
                                'popup'                   => $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height')),
                                'price'                   => $price,
                                'price_prefix'            => $option_value['price_prefix']
                            );
                        }
                    }

                    $this->data['options'][] = array(
                        'product_option_id' => $option['product_option_id'],
                        'option_id'         => $option['option_id'],
                        'name'              => $option['name'],
                        'description'       => $option['description'],
                        'type'              => $option['type'],
                        'option_value'      => $option_value_data,
                        'required'          => $option['required'],
                    );
                } elseif ($option['type'] == 'text' || $option['type'] == 'textarea' || $option['type'] == 'file' || $option['type'] == 'date'
                                || $option['type'] == 'datetime' || $option['type'] == 'time') {
                    $this->data['options'][] = array(
                        'product_option_id' => $option['product_option_id'],
                        'option_id'         => $option['option_id'],
                        'name'              => $option['name'],
                        'description'       => $option['description'],
                        'type'              => $option['type'],
                        'option_value'      => $option['option_value'],
                        'required'          => $option['required']
                    );
                }
            }

            $this->data['options_meret'] = $this->model_catalog_product->getProductMeretek($this->data['options']);
            $this->data['cart_product_quantity'] = $this->cart->countProduct($product_id);

			if ((float)$product_info['minimum']) {
                if ((float)$product_info['minimum'] == (int)$product_info['minimum']) {
                    $this->data['minimum'] = (int)$product_info['minimum'];

                } else {
                    $this->data['minimum'] = $product_info['minimum'];
                }
			} else {
				$this->data['minimum'] = 1;
			}

			$this->data['video']                = html_entity_decode($product_info['video'], ENT_QUOTES, 'UTF-8');
            $this->data['video_url']            = (isset($product_info['videos']['video_url']) && !empty($product_info['videos']['video_url'])) ? html_entity_decode($product_info['videos']['video_url'], ENT_QUOTES, 'UTF-8') : '';
			$this->data['lizing']               = ($product_info['lizing']);

			$this->data['review_status']        = $this->config->get('config_review_status');
			$this->data['reviews']              = sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']);
			$this->data['rating']               = (int)$product_info['rating'];

			$this->data['description']          = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');
			$this->data['short_description']    = html_entity_decode($product_info['short_description'], ENT_QUOTES, 'UTF-8');
			$this->data['garancia_description'] = html_entity_decode($product_info['garancia_description'], ENT_QUOTES, 'UTF-8');
			$this->data['test_description']     = html_entity_decode($product_info['test_description'], ENT_QUOTES, 'UTF-8');

			$this->data['attribute_groups']     = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);


            $this->data['description']          = $this->product->fogalomBeepites($this->data['description']);
            $this->data['short_description']    = $this->product->fogalomBeepites($this->data['short_description']);
            $this->data['garancia_description'] = $this->product->fogalomBeepites($this->data['garancia_description']);
            $this->data['test_description']     = $this->product->fogalomBeepites($this->data['test_description']);



            $this->data['attribute_count']  = 0;

            foreach($this->data['attribute_groups'] as $value) {
                $this->data['attribute_count'] += count($value['attribute']);
            }


            if (!empty($product_info['darabaru_meret']))
                $this->data['meret'] = $product_info['darabaru_meret'];
            else
                $this->data['meret'] = false;

            $this->data['csomagolasi_egyseg'] = $product_info['csomagolasi_egyseg'];
            $this->data['megyseg'] = $product_info['megyseg'];
            $this->data['suly'] = $product_info['weight'];

            $this->data['length']       = $product_info['length'];
            $this->data['width']        = $product_info['width'];
            $this->data['height']       = $product_info['height'];
            $this->data['elorendeles']  = $product_info['elorendeles'];
            $this->data['megrendelem']  = $product_info['megrendelem'];
            $this->data['ar_tiltasa']   = $product_info['ar_tiltasa'];
            $this->data['stock_in_date']= $product_info['stock_in_date'];

            if ($product_info['csomagolasi_mennyiseg'] > 1) {
                $this->data['csomagolasi_mennyiseg'] = $product_info['csomagolasi_mennyiseg'];
            } else {
                $this->data['csomagolasi_mennyiseg'] = 0;
            }
            $this->load->model('warehouse/warehouse');
            $this->data['warehouses'] = $this->model_warehouse_warehouse->getWarehousesProduct($product_id);

            $this->data['megjelenit_admin_termekadatok'] = $this->config->get('megjelenit_admin_termekadatok');
            $this->data['megjelenit_stock_date'] =  isset($this->data['megjelenit_admin_termekadatok']['stock_date']) ? $this->data['megjelenit_admin_termekadatok']['stock_date'] : '';
            $today = date("Y-m-d");
            if(isset($this->data['megjelenit_stock_date']) && $this->data['megjelenit_stock_date'] == 1 && $this->data['stock_in_date'] > 0 && $this->data['stock_in_date'] > $today) {
                $sum = 0;
                foreach($this->data['warehouses'] as $warehouse) {
                    $sum += $warehouse['quantity'];
                }
                if ($sum < 1) {
                    $this->data['warehouses_out_of_stock'] = true;
                    $this->data['stock_in_date'] = date($this->language->get('date_format_short'), strtotime( $this->data['stock_in_date']));

                } else {
                    $this->data['warehouses_out_of_stock'] = false;
                }
            } else {
                $this->data['warehouses_out_of_stock'] = false;
            }

            if(isset($this->data['megjelenit_admin_termekadatok']['kiszerelesmertek']) && $this->data['megjelenit_admin_termekadatok']['kiszerelesmertek'] == 1 && isset($this->data['megjelenit_admin_termekadatok']['kiszereles']) && $this->data['megjelenit_admin_termekadatok']['kiszereles'] == 1) {
                if(!empty($product_info['packaging']) && $product_info['packaging'] > 0  && !empty($product_info['packaging_class'])) {

                    $this->data['text_unit_price'] = $this->language->get('text_unit_price');

                    if(!empty($this->data['special_brutto_value']) && $this->data['special_brutto_value'] > 0) {
                        $this->data['kiszereles_ar'] = $this->currency->format((int)($this->data['special_brutto_value']/$product_info['packaging']))."/".$product_info['packaging_class'];
                    } else if(!empty($this->data['price_brutto_value']) && $this->data['price_brutto_value'] > 0) {
                        $this->data['kiszereles_ar'] = $this->currency->format((int)($this->data['price_brutto_value']/$product_info['packaging']))."/".$product_info['packaging_class'];
                    } else if(!empty($this->data['special_value']) && $this->data['special_value'] > 0) {
                        $this->data['kiszereles_ar'] = $this->currency->format((int)($this->data['special_value']/$product_info['packaging']))."/".$product_info['packaging_class'];
                    } else if(!empty($this->data['price_value']) && $this->data['price_value'] > 0) {
                        $this->data['kiszereles_ar'] = $this->currency->format((int)($this->data['price_value']/$product_info['packaging']))."/".$product_info['packaging_class'];
                    } else {
                        $this->data['kiszereles_ar'] = false;
                    }
                }
            }

            $this->load->model('account/customer');

                      $this->data['leiras_karakter_lista'] = $this->config->get('megjelenit_description_lista_karakterek');
            $this->data['leiras_karakter_racs'] = $this->config->get('megjelenit_description_karakterek');

            if ($product_info['egyedi_szallitas'] == 1) {
                if ($product_info['egyedi_szallitasi_dij'] == 0) {
                    $this->data['szallitasi_koltseg'] = "ingyenes";
                } else {
                    $this->data['szallitasi_koltseg'] =   $this->currency->format($this->tax->calculate($product_info['egyedi_szallitasi_dij'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                }
            } else {
    			$this->data['szallitasi_koltseg'] = false;
            }


			$product_info['garancia_ertek'] = $product_info['garancia_egyseg'] == 2 ? $product_info['garancia_ertek']/12 : $product_info['garancia_ertek'];

            switch ($product_info['garancia_egyseg']) {
                case 0:
                    $this->data['text_garancia'] = "";
                    $this->data['garancia_kep'] = "";
                    break;
                case 1:
                    $this->data['text_garancia'] = $product_info['garancia_ertek']. " " . $this->language->get('entry_garancia_honap') . " " . $this->language->get('text_garancia');
                    $this->data['garancia_kep'] = $product_info['garancia_ertek']."_honap.png";
                    break;
                case 2:
                    $this->data['text_garancia'] = $product_info['garancia_ertek']. " " . $this->language->get('entry_garancia_ev') . " " . $this->language->get('text_garancia');
                    $this->data['garancia_kep'] = $product_info['garancia_ertek']."_ev.png";
                    break;
                default:
                    $this->data['text_garancia'] = "";
                    $this->data['garancia_kep'] = "";
                    break;
            }

            $data1 = array(
                'sort'  => 'p.date_added',
                'order' => 'DESC',
                'start' => 0,
                'limit' => 8
            );

            $results1 = $this->model_catalog_product->getProductsNew($data1);

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/ujtermek.png')) {
                $this->data['ujtermek'] = DIR_TEMPLATE. $this->config->get('config_template') . '/image/ujtermek.png';
            } else {
                $this->data['ujtermek'] = DIR_TEMPLATE.'default/image/ujtermek.png';
            }

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/pardarab.png')) {
                $this->data['pardarabkep'] = DIR_TEMPLATE. $this->config->get('config_template') . '/image/pardarab.png';
            } else {
                $this->data['pardarabkep'] = DIR_TEMPLATE.'default/image/pardarab.png';
            }
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/sale.png')) {
                $this->data['sale'] = DIR_TEMPLATE. $this->config->get('config_template') . '/image/sale.png';
            } else {
                $this->data['sale'] = DIR_TEMPLATE.'default/image/sale.png';
            }

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/kifuto.png')) {
                $this->data['kifutokep'] = DIR_TEMPLATE. $this->config->get('config_template') . '/image/kifuto.png';
            } else {
                $this->data['kifutokep'] = DIR_TEMPLATE.'default/image/kifuto.png';
            }

            $setting['image_width']     = $this->config->get('config_image_related_width');
            $setting['image_height']    = $this->config->get('config_image_related_height');

            /* Kapcsolódó termékek */

            $results = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);
            $this->data['products'] = array();

            foreach ($results as $result) {
                $this->data['products'][] = $this->product->productPreparation($result,$setting,$results1);
            }
            /* Kapcsolódó termékek ---- VÉGE ---- */


            /* Ajándék termékek */

            $results = $this->model_catalog_product->getProductAjandek($this->request->get['product_id']);
            $this->data['ajandekok'] = array();

            $this->data['product_info']['ajandek'] = $results ? 1 : 0;
            foreach ($results as $result) {
                $this->data['ajandekok'][] = $this->product->productPreparation($result,$setting,$results1);
            }
            /* Ajándék termékek ---- VÉGE ---- */




            /* Kapcsolódó tartozékok */
            $results = $this->model_catalog_product->getProductAccessory($this->request->get['product_id']);
            $this->data['products_accessory'] = array();

            foreach ($results as $result) {
                $this->data['products_accessory'][] = $this->product->productPreparation($result,$setting,$results1);

            }

            $results = $this->model_catalog_product->getProductGeneralAccessory($this->request->get['product_id']);
            $this->data['products_general_accessory'] = array();

            foreach ($results as $result) {
                $this->data['products_general_accessory'][] = $this->product->productPreparation($result,$setting,$results1);
            }
            /* Kapcsolódó tartozékok ---- VÉGE ---- */

            /* Helyettesítő termékek */

            $results = $this->model_catalog_product->getProductHelyettesito($this->request->get['product_id']);
            $this->data['products_helyettesito'] = array();

            foreach ($results as $result) {
                $this->data['products_helyettesito'][] = $this->product->productPreparation($result,$setting,$results1);
            }

            /* Helyettesítő termékek -- VÉGE -- */

            /* Letölthető PDF-ek */

            $product_pdfs = $this->model_catalog_product->getProductDownloadablePdfs($this->request->get['product_id']);

            if(isset($product_pdfs) && $product_pdfs) {
                $this->data['letoltesek'] = $product_pdfs;
            }

            /* Letölthető PDF-ek ---- VÉGE ---- */

            /* Csomag ajánlat*/
            $results = $this->model_catalog_product->getPackages($this->request->get['product_id']);
            if ($results) {
                $i = 0;
                foreach ($results as $result) {
                    $result = $this->model_catalog_product->getProduct($result['product_id']);

                    $this->data['packages'][$i]['package'] = $this->product->productPreparation($result,$setting,$results1);

                    $products = $this->model_catalog_product->getProductsPackage($result['product_id']);
                    $eredeti_ar = 0;
                    if ($products) {
                        foreach($products as $product) {
                            $product = $this->model_catalog_product->getProduct($product['package_id']);

                            $this->data['packages'][$i]['products'][] = $this->product->productPreparation($product,$setting,$results1);
                            if (isset($product['special']) && $product['special'] <  $product['price'] && $product['special'] > 0) {
                                $eredeti_ar += $product['special'];
                            } else {
                                $eredeti_ar += $product['price'];
                            }
                        }
                        $this->data['packages'][$i]['package']['eredeti_ar'] =   $this->currency->format($this->tax->calculate($eredeti_ar, $product['tax_class_id'], $this->config->get('config_tax')));
                        $i++;
                    }

                }
            }
            /* Csomag ajánlat ---- VÉGE ----*/




            /* Mit vettek még, akik ezt vették*/
            $setting          =  $this->config->get('also_bought_module');
            $setting          =  $setting[0];
            if (!empty($setting['status']) || $setting) {
                $data = array(
                    'dc_period'            => $this->config->get('also_bought_dc_period') * 60 * 60,
                    'filter_product_id'    => $this->request->get['product_id'],
                    'order_status_operand' => htmlspecialchars_decode($setting['order_status_operand']),
                    'order_status_id'      => $setting['order_status_id'],
                    'sort'  			   => $setting['sort'],
                    'order' 			   => $setting['sort_type'],
                    'start' 			   => 0,
                    'limit' 			   => $setting['limit']
                );

                $this->load->model('module/also_bought');

                $results = $this->model_module_also_bought->getAlsoBoughtProducts($data);
                $results = array_filter($results);

                $setting['image_width']     = $this->config->get('config_image_related_width');
                $setting['image_height']    = $this->config->get('config_image_related_height');

                $this->data['also_bought_title'] = $this->config->get('also_bought_heading_title_'. $this->config->get('config_language_id'));
                $this->data['also_boughts'] = array();
                if ($results) {
                    foreach ($results as $result) {
                        $this->data['also_boughts'][] = $this->product->productPreparation($result,$setting,$results1);
                    }
                } else {
                    $this->data['also_boughts'] = false;
                }
            } else {
                $this->data['also_boughts'] = false;
            }
            /* Mit vettek még, akik ezt vették ----   VÉGE ----*/

            $this->data['not_null'] = true;
            if ($this->data['price_value'] > 0 ) {
                $this->data['not_null'] = true;
            } else {
                $this->data['not_null'] = false;
            }


            $this->data['tags'] = array();

			$results = $this->model_catalog_product->getProductTags($this->request->get['product_id']);

			foreach ($results as $result) {
				$this->data['tags'][] = array(
					'tag'  => $result['tag'],
					'href' => $this->url->link('product/search', 'filter_tag=' . $result['tag'])
				);
			}


            $this->data['kosarba_gomb'] = true;

            $this->document->addScript('catalog/view/javascript/jquery/jquery.pricealert.js');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/price_alert.css')) {
                $this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/price_alert.css');
            } else {
                $this->document->addStyle('catalog/view/theme/default/stylesheet/price_alert.css');
            }

            $price_alert_languages_texts = $this->config->get('price_alert_popup');
            $price_alert_current_language_text = $price_alert_languages_texts[$this->config->get('config_language_id')];

            $this->data['text_price_alert_set_alert'] = $price_alert_current_language_text['set_alert'];
            $this->data['text_price_alert_short_explanation'] = html_entity_decode($price_alert_current_language_text['short_explanation'], ENT_QUOTES, 'UTF-8');
            $this->data['entry_price_alert_name'] = $price_alert_current_language_text['name'];
            $this->data['entry_price_alert_email'] = $price_alert_current_language_text['email'];
            $this->data['entry_price_alert_desired_price'] = $price_alert_current_language_text['desired_price'];

            $this->data['button_price_alert'] = $price_alert_current_language_text['button_set_alert'];

            $this->data['price_alert_status'] = $this->config->get('price_alert_status');

            if ($this->customer->isLogged()){
                $this->data['price_alert_name']  = $this->customer->getFirstName() . ' ' . $this->customer->getLastName();
                $this->data['price_alert_email'] = $this->customer->getEmail();
                $this->data['price_alert_customer_group_id'] = $this->customer->getCustomerGroupId();
            } else {
                $this->data['price_alert_name']  = '';
                $this->data['price_alert_email'] = '';
                $this->data['price_alert_customer_group_id'] = $this->config->get('config_customer_group_id');
            }

            if ((float)$product_info['special']) {
                $this->data['price_alert_price'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), '', '', false);
            } else {
                $this->data['price_alert_price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), '', '', false);
            }

            $this->data['price_alert_currency_code'] = $this->currency->getCode();

            $this->model_catalog_product->updateViewed($this->request->get['product_id']);
            $this->data['logged'] = $this->customer->isLogged();

            $this->model_catalog_product->setLastSeenProducts($product_id);

            if ($package_info) {
                foreach($package_info as $package) {
                    if ($package) {
                        $this->data['package'][$package['product_id']] = $this->product->productPreparation($package,$setting,$results1);
                    }
                }
            }









			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/product/product.tpl';
			} else {
				$this->template = 'default/template/product/product.tpl';
			}

			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header',
                'module/facebook_comments',
			);

            $this->log->GetMicroTime('Action - product/product');

            $this->response->setOutput($this->render());
		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_tag'])) {
				$url .= '&filter_tag=' . $this->request->get['filter_tag'];
			}

			if (isset($this->request->get['filter_description'])) {
				$url .= '&filter_description=' . $this->request->get['filter_description'];
			}

			if (isset($this->request->get['filter_category_id'])) {
				$url .= '&filter_category_id=' . $this->request->get['filter_category_id'];
			}

      		$this->data['breadcrumbs'][] = array(
        		'text'      => $this->language->get('text_error'),
				'href'      => $this->url->link('product/product', $url . '&product_id=' . $product_id),
        		'separator' => $this->language->get('text_separator')
      		);

      		$this->document->setTitle($this->language->get('text_error'));

      		$this->data['heading_title'] = $this->language->get('text_error');



      		$this->data['text_error'] = $this->language->get('text_error');

      		$this->data['button_continue'] = $this->language->get('button_continue');

      		$this->data['continue'] = $this->url->link('common/home');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
			} else {
				$this->template = 'default/template/error/not_found.tpl';
			}

			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header',
				'module/facebook_comments',
			);

            $this->log->GetMicroTime('Action - product/product');

			$this->response->setOutput($this->render());
    	}
  	}

	public function review() {
    	$this->language->load('product/product');

		$this->load->model('catalog/review');

		$this->data['text_no_reviews'] = $this->language->get('text_no_reviews');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$this->data['reviews'] = array();

		$review_total = $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']);

		$results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id'], ($page - 1) * 5, 5);

		foreach ($results as $result) {
        	$this->data['reviews'][] = array(
        		'author'     => $result['author'],
				'text'       => $result['text'],
				'rating'     => (int)$result['rating'],
        		'reviews'    => sprintf($this->language->get('text_reviews'), (int)$review_total),
        		'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
        	);
      	}

		$pagination = new Pagination();
		$pagination->total = $review_total;
		$pagination->page = $page;
		$pagination->limit = 5;
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('product/product/review', 'product_id=' . $this->request->get['product_id'] . '&page={page}');

		$this->data['pagination'] = $pagination->render();

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/review.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/product/review.tpl';
		} else {
			$this->template = 'default/template/product/review.tpl';
		}

		$this->response->setOutput($this->render());
	}

    public function question($product_id = null, $page = 1, $ajax = true, $per_page = null) {
        if (!(int)$this->config->get('qa_display_questions')) {
            return;
        }

        $this->language->load('product/product');

        $this->load->model('catalog/qa');

        $this->data['text_question'] = $this->language->get('text_question');
        $this->data['text_answer'] = $this->language->get('text_answer');
        $this->data['text_no_questions'] = $this->language->get('text_no_questions');
        $this->data['text_no_answer'] = $this->language->get('text_no_answer');

        $this->data['qa_display_question_author'] = $this->config->get('qa_display_question_author');
        $this->data['qa_display_question_date'] = $this->config->get('qa_display_question_date');
        $this->data['qa_display_answer_author'] = $this->config->get('qa_display_answer_author');
        $this->data['qa_display_answer_date'] = $this->config->get('qa_display_answer_date');

        if ($ajax) {
            if (isset($this->request->get['page'])) {
                $page = $this->request->get['page'];
            } else {
                $page = 1;
            }

            $product_id = $this->request->get['product_id'];
        }

        $this->data['qas'] = array();
        if (is_null($per_page)) {
            $per_page = (int)$this->config->get('qa_items_per_page');
        }

        $results = $this->model_catalog_qa->getQuestionsByProductId($product_id, ($page - 1) * $per_page, $per_page);

        foreach ($results as $result) {
            $this->data['qas'][] = array(
                'q_author'   => $result['question_author_name'],
                'a_author'   => $result['answer_author_name'],
                'question'   => html_entity_decode($result['question']),
                'answer'     => html_entity_decode($result['answer']),
                'date_asked' => date($this->language->get('date_format_short'), strtotime($result['date_asked'])),
                'date_answered' => date($this->language->get('date_format_short'), strtotime($result['date_answered']))
            );
        }

        $qa_total = $this->model_catalog_qa->getTotalQuestionsByProductId($product_id);

        $pagination = new Pagination();
        $pagination->total = $qa_total;
        $pagination->page = $page;
        $pagination->limit = ($per_page) ? $per_page : $qa_total;
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('product/product/question', 'product_id=' . $product_id . '&page={page}');

        $this->data['pagination'] = $pagination->render();

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/qa.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/product/qa.tpl';
        } else {
            $this->template = 'default/template/product/qa.tpl';
        }
        if ($ajax)
            $this->response->setOutput($this->render());
        else
            return $this->render();
    }

    public function ask() {
        $this->language->load('product/product');

        $this->load->helper('qa');
        $this->load->model('catalog/qa');

        $json = array();

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateQuestion($this->request->post)) {
            $this->model_catalog_qa->addQuestion($this->request->get['product_id'], $this->request->post);

            $json['success'] = $this->language->get('text_success_question');
        } else {
            $json['error'] = $this->error['message'];
        }

        $this->response->setOutput(json_enc($json));
    }

    private function validateQuestion($data) {
        if ((int)$this->config->get('qa_form_require_name') && (!isset($data['q_name']) || utf8_strlen($data['q_name']) < 1)) {
            $this->error['message'] = $this->language->get('error_q_author');
        }

        if ((int)$this->config->get('qa_form_require_email') && (!isset($data['q_email']) || !validate_email(utf8_decode($data['q_email'])))) {
            $this->error['message'] = $this->language->get('error_q_email');
        }

        if ((int)$this->config->get('qa_form_require_phone') && (!isset($data['q_phone']) || utf8_strlen($data['q_phone']) < 5)) {
            $this->error['message'] = $this->language->get('error_q_phone');
        }

        if ((int)$this->config->get('qa_form_require_custom_field') && (!isset($data['q_custom']) || utf8_strlen($data['q_custom']) < 1)) {
            $custom_field_names = $this->config->get('qa_form_custom_field_name');
            $this->error['message'] = sprintf($this->language->get('error_q_custom'), $custom_field_names[$this->config->get('config_language_id')]);
        }

        if (!isset($data['q_question']) || (utf8_strlen($data['q_question']) < 15) || (utf8_strlen($data['q_question']) > 1000)) {
            $this->error['message'] = $this->language->get('error_q_question');
        }

        if ((int)$this->config->get('qa_form_require_captcha') && (!isset($this->session->data['captcha']) || !isset($data['q_captcha']) || ($this->session->data['captcha'] != $data['q_captcha']))) {
            $this->error['message'] = $this->language->get('error_q_captcha');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }


    public function write() {
		$this->language->load('product/product');

		$this->load->model('catalog/review');

		$json = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
				$json['error'] = $this->language->get('error_name');
			}

			if ((utf8_strlen($this->request->post['text']) < 25) || (utf8_strlen($this->request->post['text']) > 1000)) {
				$json['error'] = $this->language->get('error_text');
			}

			if (!$this->request->post['rating']) {
				$json['error'] = $this->language->get('error_rating');
			}

			if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
				$json['error'] = $this->language->get('error_captcha');
			}

			if (!isset($json['error'])) {
				$this->model_catalog_review->addReview($this->request->get['product_id'], $this->request->post);

				$json['success'] = $this->language->get('text_success');
			}
		}

		$this->response->setOutput(json_encode($json));
	}

	public function captcha() {
		$this->load->library('captcha');

		$captcha = new Captcha();

		$this->session->data['captcha'] = $captcha->getCode();

		$captcha->showImage();
	}

	public function upload() {
		$this->language->load('product/product');

		$json = array();

		if (!empty($this->request->files['file']['name'])) {
			$filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

			if ((strlen($filename) < 3) || (strlen($filename) > 128)) {
        		$json['error'] = $this->language->get('error_filename');
	  		}

			$allowed = array();

			$filetypes = explode(',', $this->config->get('config_upload_allowed'));

			foreach ($filetypes as $filetype) {
				$allowed[] = trim($filetype);
			}

			if (!in_array(substr(strrchr($filename, '.'), 1), $allowed)) {
				$json['error'] = $this->language->get('error_filetype');
       		}

			if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
				$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
			}
		} else {
			$json['error'] = $this->language->get('error_upload');
		}

		if (!$json) {
			if (is_uploaded_file($this->request->files['file']['tmp_name']) && file_exists($this->request->files['file']['tmp_name'])) {
				$file = basename($filename) . '.' . md5(rand());

				// Hide the uploaded file name so people can not link to it directly.
				$json['file'] = $this->encryption->encrypt($file);

				move_uploaded_file($this->request->files['file']['tmp_name'], DIR_DOWNLOAD . $file);
			}

			$json['success'] = $this->language->get('text_upload');
		}

		$this->response->setOutput(json_encode($json));
	}

    public function downloadPdf() {


        $this->load->model('catalog/product');

        if (isset($this->request->get['pdf_download_id'])) {
            $pdf_id = $this->request->get['pdf_download_id'];
        } else {
            $pdf_id = 0;
        }

        $download_info = $this->model_catalog_product->getProductDownloadablePdf($pdf_id);

        if ($download_info) {
            $file = 'image/' . $download_info['path'];
            $mask = basename($download_info['path']);

            if (!headers_sent()) {
                if (file_exists($file)) {
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename="' . ($mask ? $mask : basename($file)) . '"');
                    header('Content-Transfer-Encoding: binary');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($file));

                    readfile($file, 'rb');

                    $this->model_account_download->updateRemaining($this->request->get['order_download_id']);

                    exit;
                } else {
                    exit('Error: Could not find file ' . $file . '!');
                }
            } else {
                exit('Error: Headers already sent out!');
            }
        } else {
            $this->redirect($this->url->link('product/product', '', 'SSL'));
        }
    }

}
?>