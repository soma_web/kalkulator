<?php 
class ControllerProductCategory extends Controller {  
	public function index() {
        $this->log->setMicroTime();

        $this->language->load('product/category');
		
		$this->load->model('catalog/category');
		
		$this->load->model('catalog/product');
		
		$this->load->model('tool/image');
        $this->data['module_name'] = "category";

        if (!empty($_GET['elonezet'])) {
            $categorys= $this->model_catalog_category->getCategories();
            $category_ids = array();
            foreach($categorys as $category) {
                $data = array(
                    'filter_category_id' => $category['category_id'],
                    'start'              => 0,
                    'limit'              => 1
                );
                $count = $this->model_catalog_product->getTotalProducts($data);
                if ($count > 0) {
                    $category_ids[] = $category['category_id'];
                }
            }
            $this->request->get['path'] = $category_ids[array_rand($category_ids)];
            $_GET['path'] = $this->request->get['path'];
            $this->data['nezet'] = !empty($_GET['listanezet']) ? 'list' : (!empty($_GET['racsnezet']) ? 'grid' : '');
        }

        $megjelenit_product = $this->config->get('megjelenit_product');


        if (isset($this->session->data['path'])) unset($this->session->data['path']);

        if (isset($this->session->data['filter']))              unset($this->session->data['filter']);
        if (isset($this->session->data['filter_varos']))        unset($this->session->data['filter_varos']);
        if (isset($this->session->data['filter_artol']))        unset($this->session->data['filter_artol']);
        if (isset($this->session->data['filter_arig']))         unset($this->session->data['filter_arig']);
        if (isset($this->session->data['filter_ar_tol_ig']))    unset($this->session->data['filter_ar_tol_ig']);
        if (isset($this->session->data['filter_arszazalek']))   unset($this->session->data['filter_arszazalek']);
        if (isset($this->session->data['filter_uzlet']))        unset($this->session->data['filter_uzlet']);
        if (isset($this->session->data['filter_tulajdonsagok'])) unset($this->session->data['filter_tulajdonsagok']);
        if (isset($this->session->data['filter_valasztek']))    unset($this->session->data['filter_valasztek']);
        if (isset($this->session->data['filter_gyarto']))       unset($this->session->data['filter_gyarto']);
        if (isset($this->session->data['filter_meret']))        unset($this->session->data['filter_meret']);
        if (isset($this->session->data['filter_szin']))         unset($this->session->data['filter_szin']);
        if (isset($this->session->data['filter_raktaron']))     unset($this->session->data['filter_raktaron']);



        $multiszuro = false;

        if (isset($this->request->get['multiszuro'])) {
            $multiszuro = true;
        }

        if (isset($this->request->get['filter'])) {
            $filter = $this->request->get['filter'];
        } else {
            $filter = '';
        }

        $config_sort_order = $this->config->get('config_sort_order');
        if(isset($config_sort_order) && !empty($config_sort_order)) {
            $config_sort_orders = explode('-', $config_sort_order);
        }

        if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
            if(isset($config_sort_order) && !empty($config_sort_order) && ($megjelenit_product['product_cikkszam_termeknevben'] == 0)) {
                $sort = $config_sort_orders[0];
            } elseif (isset($megjelenit_product['product_cikkszam_termeknevben']) && $megjelenit_product['product_cikkszam_termeknevben']) {
                $sort = 'p.model';
            } else {
                $sort = 'p.sort_order';
            }
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
            if(isset($config_sort_order) && !empty($config_sort_order) && !isset($megjelenit_product['product_cikkszam_termeknevben'])) {
                $order = $config_sort_orders[1];
            } else {
			    $order = 'ASC';
            }
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else { 
			$page = 1;
		}	
							
		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_catalog_limit');
		}
					
		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
       		'separator' => false
   		);	
			
		if (isset($this->request->get['path'])) {
			$path = '';
		
			$parts = explode('_', (string)$this->request->get['path']);
            foreach ($parts as $path_id) {
                if (!$path) {
                    $path = $path_id;
                } else {
                    $path .= '_' . $path_id;
                }

                if (isset($_SESSION['categoryAll'])) {
                    $category_info = $this->config->in_array_category($_SESSION['categoryAll'],$path_id);
                } else {
                    $category_info = $this->model_catalog_category->getCategory($path_id);
                }


                if ($category_info) {
                    if ($category_info['piacter'] == 1){
                        $this->data['breadcrumbs'][] = array(
                            'text'      => $category_info['name'],
                            'href'      => $this->url->link('product/category', 'path=' . $path)."&piacter=piacter",
                            'separator' => $this->language->get('text_separator')
                        );
                    } else {
                        $this->data['breadcrumbs'][] = array(
                            'text'      => $category_info['name'],
                            'href'      => $this->url->link('product/category', 'path=' . $path),
                            'separator' => $this->language->get('text_separator')
                        );
                    }
                }
            }

            $category_id = array_pop($parts);
		} else {
			$category_id = 0;
		}


        if (isset($_SESSION['categoryAll'])) {
            $category_info = $this->config->in_array_category($_SESSION['categoryAll'],$category_id);
        } else {
            $category_info = $this->model_catalog_category->getCategory($category_id);
        }

		if ($category_info) {
	  		$this->document->setTitle($category_info['name']);
			$this->document->setDescription($category_info['meta_description']);
			$this->document->setKeywords($category_info['meta_keyword']);
			
			$this->data['heading_title'] = $category_info['name'];

            $this->data['text_eredeti_ar']  = $this->language->get('text_eredeti_ar');
            $this->data['text_cikkszam']  = $this->language->get('text_cikkszam');
            $this->data['text_stock_in'] = $this->language->get('text_stock_in');
            $this->data['kaphato'] = $this->language->get('kaphato');
			$this->data['text_refine'] = $this->language->get('text_refine');
			$this->data['text_empty'] = $this->language->get('text_empty');			
			$this->data['text_quantity'] = $this->language->get('text_quantity');
			$this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$this->data['text_model'] = $this->language->get('text_model');
			$this->data['text_price'] = $this->language->get('text_price');
			$this->data['text_tax'] = $this->language->get('text_tax');
			$this->data['text_points'] = $this->language->get('text_points');
			$this->data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
			$this->data['text_display'] = $this->language->get('text_display');
			$this->data['text_list'] = $this->language->get('text_list');
			$this->data['text_grid'] = $this->language->get('text_grid');
			$this->data['text_sort'] = $this->language->get('text_sort');
			$this->data['text_limit'] = $this->language->get('text_limit');
			$this->data['text_netto'] = $this->language->get('text_netto');
			$this->data['text_megnevezes']  = $this->language->get('text_megnevezes');
			$this->data['text_azonosito']   = $this->language->get('text_azonosito');
			$this->data['text_suly']   = $this->language->get('text_suly');
			$this->data['text_brutto_ar']   = $this->language->get('text_brutto_ar');
			$this->data['text_netto_ar']    = $this->language->get('text_netto_ar');
			$this->data['text_raktar']    = $this->language->get('text_raktar');

            $this->data['text_nincs_raktaron'] = $this->language->get('text_nincs_raktaron');
            $this->data['text_raktaron'] = $this->language->get('text_raktaron');
            $this->data['text_compare'] = $this->language->get('text_compare');
            $this->data['text_wishlist'] = $this->language->get('text_wishlist');

            $this->data['text_akcio_title']   = $this->language->get('text_akcio_title');
            $this->data['text_mennyisegi_title']   = $this->language->get('text_mennyisegi_title');
            $this->data['text_pardarab_title']   = $this->language->get('text_pardarab_title');
            $this->data['text_kifuto_title']   = $this->language->get('text_kifuto_title');
            $this->data['text_uj_title']    = $this->language->get('text_uj_title');
            $this->data['text_ujdonsag_title']    = $this->language->get('text_ujdonsag_title');
            $this->data['text_ajandek_title']   = $this->language->get('text_ajandek_title');

            $this->data['button_arajanlat'] = $this->language->get('button_arajanlat');
            $this->data['button_megrendelem'] = $this->language->get('button_megrendelem');
            $this->data['button_arajanlat_keszites'] = $this->language->get('button_arajanlat_keszites');
			$this->data['button_cart'] = $this->language->get('button_cart');
			$this->data['button_set_valos_ar'] = $this->language->get('button_set_valos_ar');
			$this->data['button_bovebben'] = $this->language->get('button_bovebben');
			$this->data['button_wishlist'] = $this->language->get('button_wishlist');
			$this->data['button_compare'] = $this->language->get('button_compare');
			$this->data['button_continue'] = $this->language->get('button_continue');


            $this->data['megjelenit_lista'] = $this->config->get('megjelenit_lista');

            if ($this->config->get('megjelenit_category_description') == 1){
			    $this->data['description_show'] = true;
            } else {
                $this->data['description_show'] = false;
            }
            if ($this->config->get('megjelenit_category_header_image') == 1 || $this->config->get('megjelenit_category_header_image_above') == 1){
                $this->data['image_show'] = true;
                if ($this->config->get('megjelenit_category_header_image') == 1) {
                    $this->data['image_below_header'] = true;
                } else {
                    $this->data['image_below_header'] = false;
                }

                if ($this->config->get('megjelenit_category_header_image_above') == 1) {
                    $this->data['image_above_header'] = true;
                } else {
                    $this->data['image_above_header'] = false;
                }
            } else {
                $this->data['image_show'] = false;
            }

			if ($category_info['image']) {
				$this->data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'),false);
			} else {
				$this->data['thumb'] = '';
			}
									
			$this->data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
			$this->data['compare'] = $this->url->link('product/compare');
			
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}	
			
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }


            $this->data['categories'] = array();


            /*if (isset($_SESSION['categoryAll'])) {
                $results = $this->config->in_array_category($_SESSION['categoryAll'],$category_id);
                if (isset($results['children'])) {
                    $results = $results['children'];
                } else {
                    $results = array();
                }
            } else {
                $results = $this->model_catalog_category->getCategories($category_id);
            }*/
            $results = $this->model_catalog_category->getCategories($category_id);

            foreach ($results as $result) {

                $ha_piacter=$this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url);
                if ($result['piacter'] == 1){
                    $ha_piacter.="&piacter=piacter";
                }

                $this->data['categories'][] = array(
                    'name'  => $result['name'],
                    'href'  => $ha_piacter,
                    'thumb' => $result['image'] ? $this->model_tool_image->resize($result['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'),false) : $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'),false),
                    'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')
                );
            }

            $this->data['products'] = array();

            $data = array(
                'filter_category_id'    => $category_id,
                'sort'                  => $sort,
                'order'                 => $order,
                'start'                 => ($page - 1) * $limit,
                'limit'                 => (!empty($_GET['elonezet']) ? 3 : $limit),
                'ar_atszamol'           => (!empty($_SESSION['ar_atszamol']) ? $_SESSION['ar_atszamol'] : ''),
                'filter_tulajdonsagok'  => (!empty($this->request->request['filter_tulajdonsagok']) ? $this->request->request['filter_tulajdonsagok'] : '')
            );

            $product_total = $this->model_catalog_product->getTotalProducts($data);
            $results = $this->model_catalog_product->getProducts($data);


            $data1 = array(
                'sort'  => 'p.date_added',
                'order' => 'DESC',
                'start' => 0,
                'limit' => 10
            );

            $setting['image_width']     = $this->config->get('config_image_product_width');
            $setting['image_height']    = $this->config->get('config_image_product_height');
            $this->data['leiras_karakter_lista'] = $this->config->get('megjelenit_description_lista_karakterek');
            $this->data['leiras_karakter_racs'] = $this->config->get('megjelenit_description_karakterek');


            $results1 = $this->model_catalog_product->getProductsNew($data1);

			foreach ($results as $result) {
                $this->data['products'][] = $this->product->productPreparation($result,$setting,$results1);
			}


            $url = '';
	
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

            $href_url = $this->url->link('product/category', 'path=' . $this->request->get['path']);
							
			$this->data['sorts'] = array();
			
			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href'  => $href_url. '&sort=p.sort_order&order=ASC' . $url
			);
			
			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href'  => $href_url . '&sort=pd.name&order=ASC' . $url
			);

			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href'  => $href_url . '&sort=pd.name&order=DESC' . $url
			);

			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC',
				'href'  => $href_url . '&sort=p.price&order=ASC' . $url
			);

			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC',
				'href'  => $href_url . '&sort=p.price&order=DESC' . $url
			);
			
			if ($this->config->get('config_review_status')) {
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href'  => $href_url . '&sort=rating&order=DESC' . $url
				);
				
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href'  => $href_url . '&sort=rating&order=ASC' . $url
				);
			}
			
			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href'  => $href_url . '&sort=p.model&order=ASC' . $url
			);

			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_model_desc'),
				'value' => 'p.model-DESC',
				'href'  => $href_url . '&sort=p.model&order=DESC' . $url
			);
			
			$url = '';
	
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$this->data['limits'] = array();

            $this->data['limits'] = $this->product->limits('product/category','path=' . $this->request->get['path'],$url);

			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->text = $this->language->get('text_pagination');

            if (isset($_GET['piacter']) && $this->config->get("megjelenit_piacter") == 1){
                $pagination->url = $href_url . $url . '&limit='.$limit.'&page={page}&piacter=piacter';
            } else {
                $pagination->url = $href_url . $url . '&limit='.$limit.'&page={page}';
            }

			$this->data['pagination'] = $pagination->render();
		
			$this->data['sort'] = $sort;
			$this->data['order'] = $order;
			$this->data['limit'] = $limit;
		
			$this->data['continue'] = $this->url->link('common/home');


            $this->data['kategoria_lista'] = false;

            if ($this->config->get('multi_sub_category_module_lista') == 1 ) {
                $this->data['kategoria_lista'] = true;
            }
            if ($this->config->get('multi_category_module_lista') == 1 ) {
                $this->data['kategoria_lista'] = true;
            }
            if ($this->config->get('category_module_lista') == 1 ) {
                $this->data['kategoria_lista'] = true;
            }



            if (isset($_GET['piacter']) && $this->config->get("megjelenit_piacter") == 1){
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/piacter.tpl')) {
                    $this->template = $this->config->get('config_template') . '/template/product/piacter.tpl';
                } else {
                    $this->template = 'default/template/product/piacter.tpl';
                }
            } else {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
                    $this->template = $this->config->get('config_template') . '/template/product/category.tpl';
                } else {
                    $this->template = 'default/template/product/category.tpl';
                }
            }

			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);
            $this->log->GetMicroTime('Action - product/category');

            $this->response->setOutput($this->render());
    	} else {
			$url = '';
			
			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}
									
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
				
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
						
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
						
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_error'),
				'href'      => $this->url->link('product/category', $url),
				'separator' => $this->language->get('text_separator')
			);
				
			$this->document->setTitle($this->language->get('text_error'));

      		$this->data['heading_title'] = $this->language->get('text_error');

      		$this->data['text_error'] = $this->language->get('text_error');

      		$this->data['button_continue'] = $this->language->get('button_continue');

      		$this->data['continue'] = $this->url->link('common/home');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
			} else {
				$this->template = 'default/template/error/not_found.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);

            $this->log->GetMicroTime('Action - product/category');

            $this->response->setOutput($this->render());
		}
  	}
}
?>