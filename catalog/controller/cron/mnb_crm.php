<?php   
class ControllerCronMnbCrm extends Controller {

    public function index($cron_id='') {
        header('Content-Type: text/html; charset=utf-8');

        if (!count($this->db_crm)) {
            echo 'A CRM kapcsolat hiányzik. (config_crm.php)<br>';
            $this->log->write_crone('A CRM kapcsolat hiányzik. (config_crm.php)' );
            return false;
        }

        $cron_id= empty($cron_id) ? (!empty($_REQUEST['cron_id']) ? $_REQUEST['cron_id'] : '') : $cron_id;

        $this->load->model("cron/altalanos");
        $this->load->model('cron/mnb_crm');

        echo 'Elindítva<br>';
        $cron_allapot_id = $this->model_cron_altalanos->cronDocumentation($cron_id);
        echo '<br>cron allapot id:'.$cron_allapot_id;


        $data = array('USD','EUR','HRK');
        $arfolyam = $this->model_cron_mnb_crm->getMnb($data);
        if (!empty($arfolyam)) {
            foreach($arfolyam as $key=>$value) {
                echo '<br><br>'.$key.': '.$value;
                $this->model_cron_altalanos->addFuttatasEredmeny($key.': '. $value);
                if ($value) {
                    $this->model_cron_mnb_crm->addCurrency(array($key => $value));
                }
            }
        }
        $this->model_cron_altalanos->cronDocumentation($cron_id,$cron_allapot_id);
        echo '<br><br>History lekönyvelve';

    }
}

