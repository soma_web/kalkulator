<?php   
class ControllerCronCrmVevojeloltToKapcsolat extends Controller {

    public function index($cron_id='') {
        header('Content-Type: text/html; charset=utf-8');

        if (!count($this->db_crm)) {
            echo 'A CRM kapcsolat hiányzik. (config_crm.php)<br>';
            $this->log->write_crone('A CRM kapcsolat hiányzik. (config_crm.php)' );
            return false;
        }

        $cron_id= empty($cron_id) ? (!empty($_REQUEST['cron_id']) ? $_REQUEST['cron_id'] : '') : $cron_id;

        $this->load->model("cron/altalanos");
        $this->load->model('cron/crm_vevojelolt_to_kapcsolat');

        echo 'Elindítva<br>';
        $cron_allapot_id = $this->model_cron_altalanos->cronDocumentation($cron_id);
        echo '<br>cron allapot id:'.$cron_allapot_id;


        $leads = $this->model_cron_crm_vevojelolt_to_kapcsolat->getLeads();
        $convert_map = $this->model_cron_crm_vevojelolt_to_kapcsolat->getMap();

        echo '<br><br>Vevőjelöltek összesen: '.count($leads).' db';
        $this->model_cron_altalanos->addFuttatasEredmeny('Vevőjelöltek összesen: '.count($leads).' db');


        if ($leads) {

            foreach($leads as $key=>$value) {

                $siker = $this->model_cron_crm_vevojelolt_to_kapcsolat->addContact($value,$convert_map);
                if ($siker) {
                    $this->model_cron_crm_vevojelolt_to_kapcsolat->deleteTriggerLead($value);
                }

               echo '';
            }
        }


        $this->model_cron_altalanos->cronDocumentation($cron_id,$cron_allapot_id);
        echo '<br><br>History lekönyvelve';

    }
}

