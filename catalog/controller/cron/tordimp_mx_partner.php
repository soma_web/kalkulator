<?php   
class ControllerCronTordimpMxPartner extends Controller {

    public function index($cron_id='') {

        $cron_id= empty($cron_id) ? (!empty($_REQUEST['cron_id']) ? $_REQUEST['cron_id'] : '') : $cron_id;

        $this->load->model("cron/altalanos");
        $this->load->model('cron/tordimp_mx_partner');

        echo 'Inditás<br>';
        $cron_allapot_id = $this->model_cron_altalanos->cronDocumentation($cron_id);
        echo '<br>cron allapot id:'.$cron_allapot_id;


        $MxPartnerEngedmeny = $this->model_cron_tordimp_mx_partner->getChangeMxPartnerEngedmeny();

        echo '<br><br>MxPartnerEngedmeny sor: '.count($MxPartnerEngedmeny).' db';
        $MxPartnerEngedmeny_change = 0;

        if ($MxPartnerEngedmeny) {
            foreach($MxPartnerEngedmeny as $value) {
                $MxPartnerEngedmeny_change += $this->model_cron_tordimp_mx_partner->getWriteEngedmeny($value['RowId'],$value['trodimp_partner_id']);
            }
        }


        /* -------------- mx_partner fizetési mód -------------- */

        $MxPartnerTorzs = $this->model_cron_tordimp_mx_partner->getChangeMxPartnerTorzs();
        echo '<br><br>MxPartnerTorzs sor: '.count($MxPartnerTorzs).' db';

        $MxPartnerTorzs_change = 0;
        if ($MxPartnerTorzs) {
            foreach($MxPartnerTorzs as $value) {
                $MxPartnerTorzs_change += $this->model_cron_tordimp_mx_partner->getWriteFizetes($value['PartnerKod'],$value['trodimp_partner_id']);
            }
        }
        $this->log->write_crone('MX Partner engedmény össz.: ' .count($MxPartnerEngedmeny).'   módosítva: '. $MxPartnerEngedmeny_change );
        $this->log->write_crone('MX Partner fizetési mód össz.: ' .count($MxPartnerTorzs).'   módosítva: '.$MxPartnerTorzs_change );

        /* -------------- mx_partner fizetési mód vége -------------- */




        /* -------------- web_partner fizetési mód -------------- */

        $MxPartnerTorzs = $this->model_cron_tordimp_mx_partner->getChangeWebPartnerTorzs();
        echo '<br><br>MxPartnerTorzs sor: '.count($MxPartnerTorzs).' db';

        $MxPartnerTorzs_change = 0;
        if ($MxPartnerTorzs) {
            foreach($MxPartnerTorzs as $value) {
                $MxPartnerTorzs_change += $this->model_cron_tordimp_mx_partner->getWriteFizetesWeb($value['PartnerKod'],$value['trodimp_partner_id']);
            }
        }
        $this->log->write_crone('WEB Partner engedmény össz.: ' .count($MxPartnerEngedmeny).'   módosítva: '. $MxPartnerEngedmeny_change );
        $this->log->write_crone('WEB Partner fizetési mód össz.: ' .count($MxPartnerTorzs).'   módosítva: '.$MxPartnerTorzs_change );

        /* -------------- web_partner fizetési mód vége -------------- */


        $this->model_cron_altalanos->cronDocumentation($cron_id,$cron_allapot_id);
        echo '<br><br>History lekönyvelve';

    }
}
?>