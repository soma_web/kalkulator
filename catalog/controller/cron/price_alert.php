<?php   
class ControllerCronPriceAlert extends Controller {
	public function index() {
		$this->load->model('module/price_alert');	
		$this->load->model('catalog/product');	
		
		if (!isset($this->request->get['secret_code'])){
			echo "Elfelejtette megadni a titkos kódot";
			exit;
		}
		
		if ($this->request->get['secret_code'] != $this->config->get('price_alert_secret_code')){
			echo "Hozzáférés megtagadva: Rossz titkos kód";
			exit;
		}
		
		$alerts = $this->processAlerts($this->model_module_price_alert->getPriceAlerts());
		
		if ($alerts) {
			$this->sendPriceAlertEmail($alerts);
		} else {
			echo "Nem küldtünk ki semmit sem (nincs olyan termék, melynek ára <= vásárló által kívánt árnál). <br />";
		}		
	}

	private function sendPriceAlertEmail($alerts) {
		
		$mail = new Mail(); 
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');	
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
	
		$email_template_languages = $this->config->get('price_alert_customer_mail');
		
		if ($alerts) {
			foreach($alerts as $alert){
				if (isset($email_template_languages[$alert['language_id']]) && $this->isLanguageEnabled($alert['language_id'])) {
					$used_language_id = $alert['language_id'];
				} else {
					$used_language_id = $this->config->get('config_language_id');
				}
				
				$email_template = $email_template_languages[$used_language_id];
				$subject = html_entity_decode($email_template['subject'], ENT_QUOTES, 'UTF-8');
			
				$html = $this->getPriceAlertEmailHtml($alert);
				
				$mail->setSubject($subject);
				$mail->setTo($alert['email']);
				$mail->setHtml($html);
				$mail->send();
				
				echo "Árváltozás értesítő kiküldve: " . $alert['email'] . "<br />";

				$this->model_module_price_alert->addHistory($alert, $html);
				$this->model_module_price_alert->deleteAlerts($alert['alerts']);	
			}
		} 
	}
	
	private function getPriceAlertEmailHtml($alert){
		
		$this->load->model('module/price_alert');
		$this->load->model('catalog/product');
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}
		
		$template = new Template();
		
		$template->data['logo'] = $server . 'image/' . $this->config->get('config_logo');		
		$template->data['store_name'] = $this->config->get('config_name');	
		$template->data['store_url'] = $this->config->get('config_use_ssl') ? $this->config->get('config_ssl') : $this->config->get('config_url');
		
		$email_template_languages = $this->config->get('price_alert_customer_mail');
		
		if (isset($email_template_languages[$alert['language_id']]) && $this->isLanguageEnabled($alert['language_id'])) {
			$used_language_id = $alert['language_id'];
		} else {
			$used_language_id = $this->config->get('config_language_id');
		}
		
		$email_template = $email_template_languages[$used_language_id];
		
		$find = array(
			'{name}',
			'{products_list}',
			'{store_name}',
		);
		
		$replace = array(
			'name'          => $alert['name'],
			'products_list' => $this->getProductsHTML($alert['products']),
			'store_name'    => $this->config->get('config_name')	
		);
		
		$subject = html_entity_decode($email_template['subject'], ENT_QUOTES, 'UTF-8');	
		$message = str_replace($find, $replace, html_entity_decode($email_template['message'], ENT_QUOTES, 'UTF-8'));	
		
		if ($this->config->get('price_alert_use_html_email') && $this->isHTMLEmailExtensionInstalled()) {
			
			$this->load->model('tool/html_email');
			$html = $this->model_tool_html_email->getHTMLEmail($used_language_id, $subject, $message, 'html');
			
		} else {
				
				$template->data['title'] = $subject;
				$template->data['message'] = $message;
				
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/price_alert.tpl')) {
					$html = $template->fetch($this->config->get('config_template') . '/template/mail/price_alert.tpl');
				} else {
					$html = $template->fetch('default/template/mail/price_alert.tpl');
				}
		}
		
		return $html;
	}
	
	private function getProductsHTML($products) {
		
		$template = new Template();
		
		
		if ($this->config->get('html_email_main_table_border_color')) {
			$template->data['table_border_color'] = $this->config->get('html_email_main_table_border_color');				
		} else {
			$template->data['table_border_color'] = '#DDDDDD';				
		}		
		
		if ($this->config->get('html_email_main_table_body_bg')) {
			$template->data['table_body_bg'] = $this->config->get('html_email_main_table_body_bg');		
		} else {	
			$template->data['table_body_bg'] = '#FFFFFF';		
		}	
		
		if ($this->config->get('html_email_main_table_body_text_color')) {
			$template->data['table_body_text_color'] = $this->config->get('html_email_main_table_body_text_color');
		} else {
			$template->data['table_body_text_color'] = '#000000';
		}	
		
		$template->data['products'] = $products;
		
		$template_file = 'price_alert_list.tpl';
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/' . $template_file)) {
			$html = $template->fetch($this->config->get('config_template') . '/template/mail/' . $template_file);
		} else {
			$html = $template->fetch('default/template/mail/' . $template_file);
		}
		
		return $html;
	}

	private function processAlerts($alerts) {
		$this->load->model('tool/image');
		
		$filtered_alerts = array();
		
		$default_language_id = $this->config->get('config_language_id');
		
		if ($alerts) {
			foreach($alerts as $alert) {
				$this->config->set('config_language_id', $alert['language_id']);
				
				$product_info = $this->model_catalog_product->getProduct($alert['product_id']);
				
				if ($product_info) {
					if ((float)$product_info['special']) {
						$product_price = $this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax'));
					} else {
						$product_price = $this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax'));
					}
					
					$product_price = $this->currency->convert($product_price, $alert['currency_code'], $this->config->get('config_currency'));
					$desired_price = $this->currency->convert($alert['desired_price'], $alert['currency_code'], $this->config->get('config_currency'));
					
					if ($product_info['image']) {
						$image = str_replace(" ", "%20", $this->model_tool_image->resize($product_info['image'], 30, 30));
					} else {
						$image = str_replace(" ", "%20", $this->model_tool_image->resize('no_image.jpg', 30, 30));
					}
					
					if ($product_price <= $desired_price) {
						$key = trim(strtolower($alert['email']));
						
						if (!isset($filtered_alerts[$key])) {
							$filtered_alerts[$key] = array(
								'name'  	  => $alert['name'],
								'email' 	  => $alert['email'],
								'language_id' => $alert['language_id']
							);	
								
						} 
						
						$filtered_alerts[$key]['products'][] = array(
							'name'          => $product_info['name'],
							'price'         => $this->currency->format($product_price, $alert['currency_code']),
							'desired_price' => $this->currency->format($desired_price, $alert['currency_code']),
							'image'         => $image,
							'href'  	    => $this->url->link('product/product', 'product_id=' . $alert['product_id'])
						);	
						
						$filtered_alerts[$key]['alerts'][] = $alert['alert_id'];
					}
				}
			}
		}
		
		$this->config->set('config_language_id', $default_language_id);
		
		return $filtered_alerts;
	}	
	
	private function isHTMLEmailExtensionInstalled() {
		$installed = false;
		
		if ($this->config->get('html_email_default_word') && file_exists(DIR_APPLICATION . 'model/tool/html_email.php')) {
			$installed = true;	
		}
		
		return $installed;
	}	
	
	private function isLanguageEnabled($language_id) {
		$status = false;
		
		$this->load->model('localisation/language');
		$language_info = $this->model_localisation_language->getLanguage($language_id);
		
		if ($language_info) {
			$status = $language_info['status'];
		} 
		
		return $status;
	}
	
	public function getHistoryEmail() {
		if (!isset($this->request->get['secret_code'])){
			echo "Elfelejtette megadni a titkos kódot";
			exit;
		}
		
		if ($this->request->get['secret_code'] != $this->config->get('price_alert_secret_code')){
			echo "Hozzáférés megtagadva: Rossz titkos kód";
			exit;
		}
		
		$this->load->model('module/price_alert');

		if (isset($this->request->get['history_id'])) {
			$history_id = (int)$this->request->get['history_id'];
		} else {
			$history_id = 0;
		}      

		$history_info = $this->model_module_price_alert->getHistory($history_id);

		if ($history_info) {
			$output = html_entity_decode($history_info['email_message'], ENT_QUOTES, 'UTF-8');		

			$this->response->setOutput($output);
		}
	}
}
?>