<?php   
class ControllerCronArgep extends Controller {
	public function index() {
		$this->load->model('module/argep');

		$products = $this->model_module_argep->getProducts();
        $fejlec = array();
        $fejlec[0] = "Cikkszám";
        $fejlec[1] = "Kategória";
        $fejlec[2] = "Márka";
        $fejlec[3] = "Megnevezés";
        $fejlec[4] = "Termékleírás";
        $fejlec[5] = "BruttóÁr";
        $fejlec[6] = "SzállításiIdő";
        $fejlec[7] = "Fotólink";
        $fejlec[8] = "Terméklink";
        $filename = "argep.csv";

        $handle = fopen('csv/'.$filename, "w");
        fputcsv ( $handle, $fejlec, "|", '"');


        foreach($products as $product) {
            $short_descr = strip_tags(html_entity_decode($product['short_description'], ENT_QUOTES, 'UTF-8'));
            $short_descr = str_replace("&nbsp;","",$short_descr);
            $short_descr = str_replace("\r","",$short_descr);
            $short_descr = str_replace("\t","",$short_descr);
            $short_descr = str_replace("\n","",$short_descr);
            $short_descr = str_replace("|"," ",$short_descr);
            $short_descr = trim($short_descr);

            $price = $product['discount'] && $product['discount'] < $product['price'] ? $product['discount'] : $product['price'];
            $price = $product['special'] && $product['special'] < $price ? $product['special'] : $price;
            $price = $this->model_module_argep->tax($price, $product['tax_class_id'], $this->config->get('config_tax'));

            $szallitas      = $product['quantity'] > 0 ? "Azonnal" : "5-7 munkanap";
            $image_link     = $this->config->get('config_use_ssl') ? HTTPS_IMAGE.$product['image'] : HTTP_IMAGE.$product['image'];
            $product_link   = $this->config->get('config_use_ssl') ? HTTPS_SERVER : HTTP_SERVER;

            if ($this->config->get('config_seo_url')) {
                $product_link  .= $product['keyword']  ? $product['keyword'] : "index.php?route=product/product&product_id=".$product['product_id'];
            } else {
                $product_link  .= "index.php?route=product/product&product_id=".$product['product_id'];
            }

            $fields[0] =  $product['cikkszam'];
            $fields[1] =  $product['category'];
            $fields[2] =  $product['manufacturer'];
            $fields[3] =  $product['product_name'];
            $fields[4] =  $short_descr;
            $fields[5] =  $price;
            $fields[6] =  $szallitas;
            $fields[7] =  $image_link;
            $fields[8] =  $product_link;

            fputcsv ( $handle, $fields, "|", '"');
        }
        fclose($handle);

    }


}
?>