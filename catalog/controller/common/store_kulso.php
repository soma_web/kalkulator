<?php   
class ControllerCommonStoreKulso extends Controller {
	public function index($setting=array()) {
		$status = true;
		$this->load->model('setting/store');
		$results = $this->model_setting_store->getStores();
		if (!$results) {
			$status = false;
            $this->redirect($this->url->link('common/home','','SSL'));

        }

		
		if ($status) {
			$this->language->load('module/store_kulso');
			
			$this->data['heading_title'] = $this->language->get('heading_title');
			
			$this->data['text_store'] = $this->language->get('text_store');

            $this->data['text_enter'] = $this->language->get('text_enter');
			
			$this->data['store_id'] = $this->config->get('config_store_id');

            $this->data['store_text_status'] = $this->config->get('store_kulso_text');

			$this->load->model('tool/image');


			$store_images = $this->model_setting_store->storeImages($results);


			$image_width  = $this->config->get('store_kulso_image_width') ? $this->config->get('store_kulso_image_width') : $this->config->get('config_image_product_width') ;
			$image_height = $this->config->get('store_kulso_image_height') ? $this->config->get('store_kulso_image_height') : $this->config->get('config_image_product_height') ;

            if(isset($image_height) && $image_height) {
                $this->data['image_height'] = $image_height;
            }

            if(isset($image_width) && $image_width) {
                $this->data['image_width'] = $image_width;
            }

			$alap_image = $this->model_tool_image->resize('no_image.jpg', $image_width, $image_height);

			foreach($store_images as $images) {
				if ($images['store_id'] == 0) {
					if ($images['image'] && file_exists(DIR_IMAGE . $images['image'])) {
						$alap_image = $this->model_tool_image->resize($images['image'], $image_width, $image_height);
					}
					break;
				}
			}



			$this->data['stores'] = array();
			$url = $this->config->get('config_store_url') ? $this->config->get('config_store_url') : HTTP_SERVER . 'index.php?route=common/home&sessionba=valasztott';

			$this->data['stores'][] = array(
				'store_id' 	=> 0,
				'name'     	=> $this->config->get('base_config_language') == $_SESSION['language'] ? $this->config->get('config_store_name') : $this->config->get('config_store_name_'.$_SESSION['language']),
				'url'      	=> $url,
				'image'		=> $alap_image
			);
			


			foreach ($results as $result) {
				$alap_image = $this->model_tool_image->resize('no_image.jpg', $image_width, $image_height);

				foreach($store_images as $images) {
					if ($result['store_id'] == $images['store_id']) {
						$alap_image = $images['image'];
						if ($alap_image && file_exists(DIR_IMAGE . $alap_image)) {
							$alap_image = $this->model_tool_image->resize($alap_image, $image_width, $image_height);
						} else {
							$alap_image = $this->model_tool_image->resize('no_image.jpg', $image_width, $image_height);
						}
						break;
					}
				}

                $url = !empty($result['start_url']) ? $result['start_url'] : $result['url'].'index.php?route=common/home&sessionba=valasztott';

                $this->data['stores'][] = array(
					'store_id' => $result['store_id'],
					'name'     => $this->config->get('base_config_language') == $_SESSION['language']  ? $result['name'] : $result['config_name_'.$_SESSION['language']],
					'url'      => $url,
					'image'		=> $alap_image
				);
			}

            if ($this->config->get('store_egyeb_link') == '1' && $this->config->get('store_egyeb_link_link') ) {
                $this->data['egyeb_link'] = htmlspecialchars_decode($this->config->get('store_egyeb_link_link'));
                $this->data['egyeb_link_title'] = $this->config->get('base_config_language') == $_SESSION['language']  ? $this->config->get('store_egyeb_link_title') :  $this->config->get('store_egyeb_link_title_'.$_SESSION['language']);

                $link_image = htmlspecialchars_decode($this->config->get('store_egyeb_link_image'));

                if ($link_image && file_exists(DIR_IMAGE . $link_image)) {
                    $this->data['egyeb_link_image'] = $this->model_tool_image->resize($link_image, $image_width, $image_height);
                } else {
                    $this->data['egyeb_link_image'] = $this->model_tool_image->resize('no_image.jpg', $image_width, $image_height);

                }

            }
	
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/store_kulso.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/common/store_kulso.tpl';
			} else {
				$this->template = 'default/template/common/store_kulso.tpl';
			}

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header',
                'module/ecslideshow'
            );

            $this->response->setOutput($this->render());
        }
	}
}
?>