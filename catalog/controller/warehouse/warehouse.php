<?php 
class ControllerWarehouseWarehouse extends Controller {
	public function index() {

		$this->language->load('warehouse/warehouse');

		$this->document->setTitle($this->language->get('heading_title'));

      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('warehouse/warehouse', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
		
		if (isset($this->session->data['success'])) {
    		$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
        if (isset($this->session->data['error'])) {
    		$this->data['error'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$this->data['error'] = '';
		}
		
    	$this->data['heading_title'] = $this->language->get('heading_title');


        $this->load->model('warehouse/warehouse');
        $this->data['warehouses'] = $this->model_warehouse_warehouse->getWarehouses();


        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/warehouse/warehouse.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/warehouse/warehouse.tpl';
		} else {
			$this->template = 'default/template/warehouse/warehouse.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'		
		);
				
		$this->response->setOutput($this->render());
  	}
}
?>