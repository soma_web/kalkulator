<?php
?>
<style>
    .box-product > div input.button.arajanlat, .ajanlott_termekek input.button.arajanlat {
        padding-left: 20px;
        padding-right: 20px;
        margin: 0;
        width: 100%;
    }


    #arajanlatot_kerek_mail {
        position: absolute;
        z-index: 10000000;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, 0.3);
    }

    #arajanlatot_kerek_mail > div {
        background: #FFFFFF;
        border: 1px solid #104f9c;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        -webkit-box-shadow: 2px 2px 30px rgba(0, 0, 0, 0.75);
        -moz-box-shadow: 2px 2px 30px rgba(0, 0, 0, 0.75);
        box-shadow: 2px 2px 30px rgba(0, 0, 0, 0.75);
        margin: 50px auto 0 auto;
        max-width: 800px;
        padding: 20px;
        width: 100%;
        position: relative;
    }
    #arajanlatot_kerek_mail .close_arajanlat_icon{
        position: absolute;
        right: 2px;
        top: 2px;
        cursor: pointer;
        max-width: 14px;
    }

    #arajanlatot_kerek_mail textarea {
        height: 51px;
        width: 90%;
    }

    #arajanlatot_kerek_mail .buttons {
        width: 90%;
        border: none;

    }
    #arajanlatot_kerek_mail .email_adatok{
        display: inline-block;
        width: 50%;
    }

    #arajanlatot_kerek_mail .valasztott_termek{
        display: inline-block;
        vertical-align: top;
    }

    #arajanlatot_kerek_mail .cimke{
        font-weight: bold;
        width: 20%;
    }
    #arajanlatot_kerek_mail .ertek{
    }


    #arajanlatot_kerek_mail textarea {
        height: 51px;
        width: 90%;
    }


    #arajanlatot_kerek_mail .buttons {
        width: 90%;
        border: none;
    }

    #arajanlatot_kerek_mail .email_adatok{
        display: inline-block;
        width: 50%;
    }

    #arajanlatot_kerek_mail .valasztott_termek{
        display: inline-block;
        vertical-align: top;
    }

    #arajanlatot_kerek_mail .cimke{
        font-weight: bold;
        width: 20%;
    }

</style>

