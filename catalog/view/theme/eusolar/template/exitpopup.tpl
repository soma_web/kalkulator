<div id="popup_background">
    <div id="popup_box">
        <form name="Pop up" action="http://sw.marketingszoftverek.hu/newsletter/nl_subscribe.php" method="post" enctype="multipart/form-data" accept-charset="utf-8" target="_top">
            <div class="popup-child">
                <input name="mssys_formid" type="hidden" value="wp7l3HEQ5LIzW8F74uVZTX64oCe-UsY">
                <input type="hidden" name="character-encoding" id="mssys-character-encoding" value="utf-8">
                <input class="utmsource" type="hidden" value="" name="utm_source">
                <input class="utmsource" type="hidden" value="" name="utm_medium">
                <input class="utmsource" type="hidden" value="" name="url">
                <input class="utmsource" type="hidden" value="" name="utm_campaign">
                <input class="utmsource" type="hidden" value="" name="utm_term">
                <div class="columns">
                    <div class="popup_box comumn_one">
                        <span style="color: #4366ce;font-weight: bold;font-size: 23px;margin-bottom: 10px;">Ne menjen el üres kézzel! Most meglepjük egy hasznos ajándékkal!</span></br>
                        <span style="font-size: 17px;margin-bottom: 5px;">Adja meg e-mail címét, és cserébe <span style="font-weight: bold;">AJÁNDÉK KUPONT</span> adunk, amelyet ha online pénztárgép rendelés során felhasznál, egy 2100 forint értékű, 10 hőpapír tekercsből álló csomagot kap ingyenesen!</span>
                    </div>
                    <div class="popup_box center comumn_two">
                        <img src="http://onlinepenztargep-aruhaz.hu/catalog/view/theme/jutasoft/image/juta_ollo_kivag.jpg">
                    </div>
                </div>
                <div style="width: 80%;margin: 0 auto;margin-bottom: 20px;">
                    <input style="font-size: 20px;width: 100%;box-sizing: border-box;padding: 10px;border-radius: 5px;" type="text" name="email" value="" placeholder="E-mail">
                </div>
                <div class="next-div">Tovább, és kérem az ajándékot!!</div>
                <input type="submit">
            </div>
        </form>
        <div id="popup_close_button" onclick="closepopup();"><img src="http://ptplasztik.hu/wp-content/themes/newbusiness_v1.2/images/popup-close-btn.png"></div>
    </div>
</div>
<script>
/* A létrehozandó cookie neve, lehet több is. */
cookiename = "mmformsub58897";
/* A cookie hány nap múlva járjon le */
cookiedays = 365;
/* A cookie tartalma */
cookiecontent = "y";

Ajaxhivas = false;

/* Ellenőrzi, hogy léteznek e a cookie-k. Ha létezik már valamelyik cookie true-t ad vissza, különben false */
function cookies(cookiename){
    var is = false;
    for(var i = 0; i < cookiename.length; i++){
        if(readCookie(cookiename[i])){
            is = true;
        }
    }
    return is;
}

/* Követi a felhasználó egér mozgását, és ha a felső 10 pixelen áthalad, megjeleníti és elhelyezi a popup-ot. */
jQuery(document).mousemove(function(e) {
    var position = e.pageY - jQuery(window).scrollTop();
    if(position < 10){
        //debugger;
        if(jQuery.isArray(cookiename)){
            if(!cookies(cookiename)){
                jQuery("#popup_background").css("display","block");
                popuphelyez();
            }
        }else{
            if(!readCookie(cookiename)){
                jQuery("#popup_background").css("display","block");
                popuphelyez();
            }
        }
    }
});

/* Több oldalas popupnál lépteti az oldalakat a next-div class-sal ellátott divre kattintva. */
jQuery(document).ready(function(){
    var szam = 1;
    jQuery("#popup_background #popup_box .popup-child .next-div").bind("click",function(){
        if(!check_required_fields(jQuery(this).closest(".popup-child"))){
            szam++;
            if(jQuery("#popup_box .popup-child:nth-child("+szam+")").length != 0){
                jQuery("#popup_box .popup-child:nth-child("+(szam-1)+")").css("display","none");
                jQuery("#popup_box .popup-child:nth-child("+szam+")").css("display","block");
                popuphelyez();
            }
            else{
                jQuery("#popup_box > form").find("input[type='submit']").trigger("click");

            }
        }else{
            debugger;
            if(jQuery(".popup_error").length == 0){
                jQuery("#popup_box").append("<div class='popup_error' style='position: fixed;box-shadow: 0px 0px 5px #000;color:#000;background: rgb(255, 142, 142);padding: 20px 50px;font-weight: bold'>Kérem válasszon!</div>");
                popuphelyez();
            }
        }

    });
    label_click("#popup_background #popup_box .popup-child .columns > div > label","input[type='checkbox']");
});

function label_click(mire,mit){
    jQuery(mire).bind("click",function(){
        var ez = jQuery(this);
        if(jQuery(this).prev().attr("checked") == "checked"){
            jQuery(this).prev().removeAttr("checked");
        }else{
            jQuery(this).prev().prop("checked","checked");
            if(jQuery(".popup_error").length != 0){
                jQuery(".popup_error").remove();
            }
        }
    });
}
jQuery("#popup_box > form").submit(function(){
    if(Ajaxhivas){
        mailmaster_elkuld(jQuery("#popup_box > form"));
    }
    closepopup();
});
/* Átméretezéskor középre helyezi a popup-ot. */
jQuery(window).resize(function(){
    popuphelyez();
});
/* Görgetéskor középre helyezi a popup-ot. */
jQuery(window).scroll(function(){
    popuphelyez();
});

/* A popup-on kívülre kattintva bezárja azt és létrehozza a cookie-kat. */
jQuery("#popup_background").bind("click",function(){
    closepopup();
});

/* A popup divre kattintásnál letiltja a popup bezárását. */
jQuery("#popup_background #popup_box").bind("click",function (event) {
    event.stopPropagation();
});

/* Középre helyezi a popup-ot. */
function popuphelyez(){
    var windowheight = jQuery(window).height();
    var windowwidth = jQuery(window).width();
    var popupmagas = jQuery("#popup_box").outerHeight();
    var popupszeles = jQuery("#popup_box").outerWidth();
    jQuery("#popup_box").css("left",(windowwidth/2)-(popupszeles/2));
    jQuery("#popup_box").css("top",(windowheight/2)-(popupmagas/2));
    if(jQuery(".popup_error").length != 0){
        errorbox_helyez();
    }
}
function errorbox_helyez(){
    var windowheight = jQuery(window).height();
    var windowwidth = jQuery(window).width();
    var errorboxmagas = jQuery(".popup_error").outerHeight();
    var errorboxszeles = jQuery(".popup_error").outerWidth();
    jQuery(".popup_error").css("left",(windowwidth/2)-(errorboxszeles/2));
    jQuery(".popup_error").css("top",(windowheight/2)-(errorboxmagas/2));
}

/* Bezárja a popup-ot és létrehozza a cookiet(kat) */
function closepopup(){
    jQuery("#popup_background").css("display","none");
    if(jQuery.isArray(cookiename)){
        for(var i = 0; i < cookiename.length; i++){
            createCookie(cookiename[i], cookiecontent, cookiedays);
        }
    }else{
        createCookie(cookiename, cookiecontent, cookiedays);
    }
}

/* Létrehozza a cookie-t és beállítja. */
function createCookie(name, value, days) {
    var expires;

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
}

/* Ellenőrzni a cookie létezését. */
function readCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
}

/* Törli a cookie-t */
function eraseCookie(name) {
    createCookie(name, "", -1);
}

function check_required_fields(mainbox){
    var requiered_fields = mainbox.find(".requiered_field");
    var i;
    var hiba = false;
    var radio = new Array();
    var inputs = new Array();
    var checkbox = new Array();
    var k = 0;
    var n = 0;

    for(i = 0; i < requiered_fields.length; i++){
        if(requiered_fields.eq(i).is("input[type='radio']")){
            radio[k] = requiered_fields.eq(i);
            k++;
        }
        else if(requiered_fields.eq(i).is("input[type='checkbox']")){
            checkbox[k] = requiered_fields.eq(i);
            k++;
        }
    }
    if(radio.length != 0){
        if(radio_input(radio)){
            hiba = true;
        }
    }
    if(checkbox.length != 0){
        if(checkbox_input(checkbox)){
            hiba = true;
        }
    }
    return hiba;
}
function checkbox_input(checkboxes){
    var checked = false;
    var hiba = false;
    for(var i = 0; i < checkboxes.length; i++){
        if(checkboxes[i][0].checked){
            checked = true;
        }
    }
    if(!checked){
        hiba = true;
    }
    return hiba;
}
function mailmaster_elkuld(form){
    jQuery.ajax({
        type: "POST",
        url: "http://szabadalom-vedjegy.hu/wp-content/themes/wpex-pytheas/mailmaster_elkuld.php",
        data: jQuery(form).serialize(),
        success: function(){
            return true;
        }
    });
}
function inputs_ellenor(inputs){
    var hiba = false;
    for(var i = 0; i < inputs.length; i++){
        if(inputs[i][0].value == "" || inputs[i][0].value == undefined){
            hiba = true;
        }
    }
    return hiba;
}
function radio_input(radio){
    var name = new Array();
    var hiba = false;
    var radiobuttons;
    for(var i = 0; i < radio.length; i++){
        name[i] =  radio[i].attr("name");
    }
    var names = input_name(name);
    for(var n = 0; n < names.length; n++){
        if(!radio_ellenor(names[n])){
            hiba = true;
        }
    }
    return hiba;
}
function input_name(name){
    var k = 0;
    var nevek = new Array();
    for(var i = 0; i < name.length; i++){
        if(jQuery.inArray(name[i],nevek) < 0){
            nevek[k] = name[i];
            k++;
        }
    }
    return nevek;
}
function radio_ellenor(inputs){
    var radios = jQuery("input[name='"+inputs+"']");
    var vissza = false;
    for(var i = 0; i < radios.length; i++){
        if(radios[i].checked){
            vissza = true;
        }
    }
    return vissza;
}

function getURLParameters(url){
    var result = {};
    var searchIndex = url.indexOf("?");
    if (searchIndex == -1 ) return result;
    var sPageURL = url.substring(searchIndex +1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        result[sParameterName[0]] = sParameterName[1];
    }
    return result;
}
jQuery(document).ready(function(){
    jQuery("input[name='url']").val(window.location.href.split('?')[0]);
    var utm = getURLParameters(document.location.search);
    jQuery.each(utm, function(key, val) {
        for(var i = 0; i < jQuery(".utmsource").length;i++){
            if(jQuery(".utmsource").eq(i).attr("name") == key){
                jQuery(".utmsource").eq(i).val(val);
            }
        }
    });
});
</script>
<style>
#popup_background{
    background: rgba(0,0,0,0.8);
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0;
    font-family: 'Open Sans', sans-serif;
    left: 0;
    z-index: 100;
    display: none;
}
#popup_background #popup_box{
    position: fixed;
    border: 6px solid #104f9c;
    width: 865px;
    background: #f5f5f5;
    z-index: 150;
    padding: 10px 10px 10px;
    box-sizing: border-box;
    color: #777;
}
#popup_background #popup_box .popup-child .columns{
    font-size: 0;
    float: none;
    margin-top: 0px;
    margin-bottom: 0px;
    display: table;
    width: 100%;
    margin-right: 0;
    box-sizing: border-box;
}
#popup_background #popup_box .popup-child .columns .comumn_one{
    width: calc(55% - 4px);
    vertical-align: middle;
}
#popup_background #popup_box .popup-child .columns .comumn_two{
    margin-left: 20px;
    width: calc(45% - 20px);
    vertical-align: middle;
}
#popup_background #popup_box .popup-child .columns > div{
    font-size: 12px;
    display: inline-block;
    box-sizing: border-box;
    width: 50%;
    vertical-align: middle;
}
#popup_background #popup_box .popup-child .columns > div:last-child{
    margin-left: 20px;
}
#popup_background #popup_box .popup-child .columns > div > input{
    display: inline-table;
    vertical-align: middle;
}
#popup_background #popup_box .popup-child .columns .nowrap{
    white-space: nowrap;
}
#popup_background #popup_box .popup-child .columns > div > label{
    display: inline-table;
    vertical-align: middle;
    font-size: 17px;
    margin-left: 5px;
}
#popup_background #popup_box .popup-child:not(:first-child){
    display: none;
}
#popup_background #popup_box .popup-child .popup_box{
    font-size: 12px;
    display: inline-table;
    text-align: center;
    vertical-align: top;
    width: 100%;
}
#popup_background #popup_box .popup-child .popup_urlap{
    width: 100%;
    color: #fff;
    padding-bottom: 10px;
    font-size: 0;
    padding-top: 7px;
    margin-top: 7px;
}
#popup_background #popup_box .popup-child .popup_urlap > div{
    margin-top: 20px;
}
#popup_background #popup_box .popup-child .popup_urlap > .comumn_one{
    display: inline-table;
    vertical-align: middle;
    font-size: 12px;
    width: 60%;
}
#popup_background #popup_box .popup-child .popup_urlap > .comumn_two{
    display: inline-table;
    vertical-align: middle;
    font-size: 12px;
    width: 40%;
}
#popup_background #popup_box .popup-child .popup_urlap > .comumn_two > img{
    display: table;
    margin: 0 auto;
    vertical-align: middle;
}
#popup_background #popup_box .popup-child .popup_urlap > div:first-child{

}
#popup_background #popup_box .popup-child .popup_urlap > div:first-child > input{
    width: calc(100% - 24px);
    padding: 10px;
    border-radius: 5px;
    border: 2px solid #D3D3D3;
    margin-bottom: 10px;
    font-size: 18px;
}
#popup_background #popup_box .popup-child .popup_urlap > div{

}
.vevo-gyar{
    display: block;
    position: absolute;
    width: 100%;
    bottom: 10px;
    left: 10px;
}
#popup_background #popup_box .popup-child > .vevo-gyar > a > *{
    display: inline-table;
    vertical-align: bottom;
}
#popup_background #popup_box .popup-child > .vevo-gyar > a > div > img{
    vertical-align: bottom;
}
#popup_background #popup_box .popup-child > .vevo-gyar > a > span{
    color: #575757;
    text-shadow: none;
    margin-left: 10px;
    font-style: italic;
    font-size: 15px;
}

#popup_background #popup_box .popup-child .center{
    /*text-align: center;
    width: 40%;*/
}
#popup_background #popup_box .popup-child .betu_36 {
    font-size: 36px;
}
#popup_background #popup_box .popup-child .green {
    color: #59b200;
}
#popup_background #popup_box .popup-child .betu_30 {
    font-size: 30px;
}
#popup_background #popup_box .popup-child .betu_20 {
    font-size: 20px;
}
#popup_background #popup_box .popup-child .betu_18 {
    font-size: 18px;
}
#popup_background #popup_box .popup-child .betu_26 {
    font-size: 26px;
}
#popup_background #popup_box .popup-child .betu_34 {
    font-size: 34px;
}
#popup_background #popup_box .popup-child .center img{
    width: 80%;
}
#popup_background #popup_box .popup-child{
    font-size: 0;
    padding: 0px 15px 18px 15px;
    color: #777;
}
#popup_background #popup_box .popup-child .popup_box > h6{
    margin: 0;
    font-size: 22px;
    margin-bottom: 28px;
    line-height: 24px;
}
#popup_background #popup_box .popup-child .popup_box > h6 > strong{
    color: #383838;
    font-size: 28px;
}
#popup_background #popup_box .popup-child .popup_box > span{
    text-align: center;
    display: table;
    margin: 0 auto;
}
#popup_background #popup_box .popup-child .popup_box span > strong{
    font-size: 18px;
}
#popup_background #popup_box .popup-child .popup_box:last-child > span{
    font-style: italic;
}
#popup_background #popup_box .popup-child .popup_question{
    font-size: 16px;
    padding: 10px;
    border-radius: 15px;
    background: #F1F1F1;
    border: 2px solid #dedede;
}
#popup_background #popup_box .popup-child .popup_question > span:first-child{
    font-weight: bold;
    margin-bottom: 10px;
}
#popup_background #popup_box .popup-child .popup_question > label{

}
#popup_background #popup_box .popup-child .popup_question > label > span{

}
#popup_background #popup_box .popup-child .popup_question > label > input{

}
#popup_background #popup_box .popup-child .next-div{
    font-size: 20px;
    display: table;
    margin: 0 auto;
    padding: 20px 0px;
    width: 80%;
    text-align: center;
    border-radius: 5px;
    cursor: pointer;
    background: #c23030;
    background: -moz-linear-gradient(top, #c23030 0%, #ad0f0f 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#c23030), color-stop(100%,#ad0f0f));
    background: -webkit-linear-gradient(top, #c23030 0%,#ad0f0f 100%);
    background: -o-linear-gradient(top, #c23030 0%,#ad0f0f 100%);
    background: -ms-linear-gradient(top, #c23030 0%,#ad0f0f 100%);
    background: linear-gradient(to bottom, #c23030 0%,#ad0f0f 100%);
    color: #fff;
}
#popup_background #popup_box .popup-child .next-div:hover{
    opacity: 0.9;
}
.display_inline_table{
    display: inline-table;
}
#popup_background #popup_box #popup_close_button{
    position: absolute;
    top: -12px;
    cursor: pointer;
    display: table;
    right: -13px;
}
#popup_background #popup_box #popup_close_button > img{
    width: 25px;
}
.font_size_20{
    font-size: 20px;
}
.width_60_percent{
    width: 60% !important;
}
.width_70_percent{
    width: 70% !important;
}
.width_30_percent{
    width: 30% !important;
}
.margin_left_5{
    margin-left: 5px;
}
.submit_button_form{
    display: table !important;
    margin: 0 auto !important;
    border: 0 !important;
    padding: 2px 65px !important;
    font-family: inherit !important;
    font-size: 20px !important;
    font-weight: inherit !important;
    font-style: inherit !important;
    vertical-align: baseline !important;
    line-height: inherit !important;
    white-space: normal !important;
    outline: 0 !important;
    cursor: pointer !important;
    background: #BE2423 !important;
    color: #fff !important;
    text-shadow: 0px 0px 0px ! important;
    border-radius: 0 !important;
    margin-top: 9px !important;
    margin-bottom: 10px !important;
}
.submit_button_form:hover{
    background: #383838 !important;
    color: #fff !important;
}
#popup_background #popup_box input[type='submit']{
    display: none;
}
</style>
<script src="//cdn.optimizely.com/js/2813630499.js"></script>