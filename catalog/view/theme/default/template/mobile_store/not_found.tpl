<?php echo $header; ?>
<div class="container">
  <div id="content" class="round"><?php echo $content_top; ?>
    <h1><?php echo $heading_title; ?></h1>
    <div class="content"><?php echo $text_error; ?></div>
    <div class="buttons">
      <div class="right"><a href="<?php echo $continue; ?>" class="button"><span><?php echo $button_continue; ?></span></a></div>
    </div>
    <?php echo $content_bottom; ?></div>
</div>
<?php echo $footer; ?>