<h2><?php echo $text_instruction; ?></h2>
<p><?php echo $text_description; ?></p>
<p><?php echo $bank; ?></p>
<p><?php echo $text_payment; ?></p>

<div class="buttons">
  <div class="right">
    <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="button" />
  </div>
</div>
<script type="text/javascript"><!--
$('#button-confirm').bind('click', function() {
	$.ajax({ 
		type: 'GET',
		url: 'index.php?route=payment/bank_transfer/confirm',

        beforeSend: function() {
            $('#button-confirm').attr('disabled', true);
            $('#button-confirm').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
        },
        success: function() {
            $.ajax({
                url: 'index.php?route=checkout/guest_history',
                type: 'post',
                data: 'Megrendelem=Tovább',
                dataType: 'html',

                success: function(json) {
                    location = '<?php echo $continue; ?>';

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    debugger;

                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }		,
        complete: function() {
            $('#button-confirm').attr('disabled', false);
            $('.wait').remove();
        }
	});
});
//--></script> 
