<form name="frmForm" action="<?php echo $action; ?>" method="post">
    <input  type="hidden" name="BACK_REF" value="<?php echo $back_ref ?>" />
    <?php
        //foreach($data as $row){
        //print_r($data);

        while ($key = key($data)) {
            $ikey=strtoupper($key);
            if(is_array($data[$key]))
            {
                foreach($data[$key] as $ival){
                    echo "<input type=\"hidden\" name=\"".$ikey."[]\" value=\"".$ival."\" id=\"".$ikey."\" />";
                }
            } else {
                $ival=$data[$key];
                echo "<input type=\"hidden\" name=\"".$ikey."\" value=\"".$ival."\" id=\"".$ikey."\" />";
            }
            next($data);
        }
    ?>

    <div style="text-align: right;">

        <input type="checkbox" onclick="enablesubmit(this)"/><?php echo $text_payuterms ?>
    </div>
    <br />
    <div class="buttons">
        <div class="right">
            <input type="submit" value="<?php echo $button_confirm; ?>" id="submitbutton" class="button" disabled="disabled"/>
        </div>
    </div>
</form>

<script language=javascript>
    function enablesubmit(val){
        var sbmt = document.getElementById("submitbutton");
        if(val.checked == true) {
            sbmt.disabled = false;
        } else {
            sbmt.disabled = true;
        }
    }
</script>