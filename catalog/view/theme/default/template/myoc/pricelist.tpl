<?php echo $header; ?>
<?php if(isset($error_customer_group)) { ?>
<div class="attention"><?php echo $error_customer_group; ?></div>
<?php } ?>
    <div class="breadcrumb">
        <?php $elemszam=count($breadcrumbs);
        $i=0;
        foreach ($breadcrumbs as $breadcrumb) { $i++;
            if ($i==$elemszam) { ?>
                <? echo $breadcrumb['separator']; ?><a class="utolsobread" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? } else {
                echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? }
        } ?>
    </div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php  echo $content_top; ?>

    <h1><?php echo $heading_title; ?></h1>
    <?php if (!isset($error_customer_group)) { ?>
    <style type="text/css">
    .filter-control {
        padding: 5px 0;
        border-bottom: 1px solid #ddd;
        overflow: auto;
    }
    .fleft {
        float: left;
    }
    .fright {
        float: right;
    }
    .clear {
        clear: both;
    }
    .tright {
        text-align: right;
    }
    .tcenter {
        text-align: center;
    }
    .print-control {
        margin: 5px 0;
        overflow: auto;
    }
    .pricelist {
        overflow: auto;
    }
    .pricelist table {
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #ddd;
        margin-bottom: 10px;
    }
    .pricelist table th, .pricelist table td {
        border: 1px solid #ddd;
        border-width: 1px;
    }
    .pricelist table th {
        padding: 7px;
        background-color: #F7F7F7;
    }
    .pricelist table td {
        padding: 5px;
    }
    .pricelist table th.image {
        width: 100px;
        text-align: center;
    }
    .pricelist table th.name {
    }
    .pricelist table th.action {
        text-align: center;
        width: 100px;
    }
    .pricelist table th a {
        text-decoration: none;
    }
    .pricelist table th a.asc {
    	padding-right: 15px;
    	background: url('catalog/view/theme/default/image/asc.png') right center no-repeat;
    }
    .pricelist table th a.desc {
    	padding-right: 15px;
    	background: url('catalog/view/theme/default/image/desc.png') right center no-repeat;
    }
    .pricelist table td.nowrap {
        white-space: nowrap;
    }
    .pricelist table td.image img {
        padding: 3px;
        border: 1px solid #ddd;
    }
    .pricelist table td .price {
        font-weight: bold;
    }
    .pricelist table td .price-old {
        text-decoration: line-through;
    }
    .pricelist table td .price-new {
        color: #f00;
        font-weight: bold;
    }
    .pricelist table td .discount {
        margin-top: 10px;
        color: #666;
    }
    .pricelist table td .nostock {
        color: #c00;
    }
    </style>
    <div class="filter-control">
        <div class="fleft">
          <strong><?php echo $text_category; ?></strong> <select name="category_id" onchange="location = this.value;">
            <?php foreach($categories as $category){ ?>
            <option value="<?php echo $category['href']; ?>"<?php if($catid == $category['category_id']) { ?> selected="selected"<?php } ?>><?php echo $category['path']; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="fright">
          <strong><?php echo $text_limit; ?></strong> <select name="limit" onchange="location = this.value;">
            <?php foreach($limits as $limit_value) { ?>
            <option value="<?php echo $limit_value['href']; ?>"
                <?php if($limit == $limit_value['value']) { ?> selected="selected"<?php } ?>>
                <?php echo $limit_value['value']; ?>
            </option>
            <?php } ?>
          </select>
        </div>
    </div>
    <div class="print-control">
        <div class="fright">
            <a style="padding: 4px 20px" href="<?php echo $print; ?>" class="button" target="_blank"><span><?php echo $text_print; ?></span></a>
        </div>
    </div>
    <div class="pricelist">
      <table>
        <thead>
          <tr>
            <?php if($myocwpl_picture == 1) { ?>
            <th class="image"><?php echo $column_image; ?></th>
            <?php } ?>
            <th class="name"><?php if ($sort == 'pd.name') { ?>
                <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                <?php } ?>
            </th>
            <?php if($myocwpl_model == 1) { ?>
            <th><?php if ($sort == 'p.model') { ?>
                <a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_model; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_model; ?>"><?php echo $column_model; ?></a>
                <?php } ?>
            </th>
            <?php } ?>
            <?php if($myocwpl_rate == 1) { ?>
            <th><?php if ($sort == 'rating') { ?>
                <a href="<?php echo $sort_rating; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_rating; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_rating; ?>"><?php echo $column_rating; ?></a>
                <?php } ?>
            </th>
            <?php } ?>
            <?php if($myocwpl_buy == 1) { ?>
            <th class="action"><?php echo $column_action; ?></th>
            <?php } ?>
          </tr>
        </thead>
        <tbody>
          <?php if(!empty($products)) { ?>
          <?php foreach($products as $product_id => $product) { ?>
            <tr>
                <?php if($myocwpl_picture == 1) { ?>
                    <td class="tcenter image"><?php if($product['popup']) { ?><a href="<?php echo $product['popup']; ?>" class="fancybox" rel="fancybox"><?php } ?><img src="<?php echo $product['image']; ?>" alt="no_image" title="<?php echo $product['name']; ?>" /><?php if($product['popup']) { ?></a><?php } ?></td>
                <?php } ?>
                <td><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a><br /><?php echo $product['description']; ?></td>
                <?php if($myocwpl_model == 1) { ?>
                <td class="nowrap"><?php echo $product['model']; ?>
                    <?php if($product['sku']) { ?><br /><strong><?php echo $text_sku; ?></strong> <?php echo $product['sku']; } ?>
                    <?php if($product['upc']) { ?><br /><strong><?php echo $text_upc; ?></strong> <?php echo $product['upc']; } ?>
                </td>
                <?php } ?>
                <?php if($myocwpl_rate == 1) { ?>
                <td><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['rating']; ?> *" /></td>
                <?php } ?>
                <?php if($myocwpl_buy == 1) { ?>
                <td class="tcenter">
                    <a href="<?php echo $product['href']; ?>">
                        <input style="margin 0 auto" type="button" style="" value="<?php echo $button_bovebben; ?>" class="button nepszeru_button_cart" />
                    </a>
                </td>
                <?php } ?>
            </tr>
          <?php } ?>
          <?php } else { ?>
            <tr><td colspan="8" class="tcenter"><?php echo $text_empty; ?></td></tr>
          <?php } ?>
        </tbody>
      </table>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
    <div style="clear: both"></div>

    <script type="text/javascript"><!--

    $(".addtocart-button").click(function () {
        var product_id = $(this).attr("id").substring($(this).attr("id").indexOf("-")+1);
        var quantity = $("#qty-" + product_id).val();
        var csomag = $("#price_csomagolas_"+product_id).val();


        if (typeof(csomag) != "undefined" ){
            if (csomag > 1){
                quantity=quantity*csomag;
            }
        }

        add_to_cart(product_id, quantity);

        return false;
    });

    if(typeof $.fancybox == 'function') {
        $('.fancybox').fancybox({cyclic: true});
    }


    function add_to_cart(product_id, quantity) {
        quantity = typeof(quantity) != 'undefined' ? quantity : 1;

        var cart_path = 'index.php?route=checkout/cart/add'; //OC v1.5.2.x


        $.ajax({
            url: cart_path,
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + quantity,
            dataType: 'json',
            success: function(json) {
                $('.success, .warning, .attention, .information, .error').remove();

                if (json['error']) {
                    if (json['error']['warning']) {
                        $('#notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

                        $('.warning').fadeIn('slow');

                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                    } else if (json['redirect']) {
                        location = json['redirect'];
                    }
                }

                if (json['success']) {
                    $('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

                    $('.success').fadeIn('slow');

                    $('#cart-total, #cart_total').html(json['total']);

                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                    $("#slidecontent").load("index.php #slidecontent");
                }
            }
        });
    }

    function check_path(url) {
        var http = new XMLHttpRequest();
        http.open('HEAD', url, false);
        http.send();
        return http.status!=404;
    }
    //--></script>
    <?php } else { ?>
        <div class="content"><?php echo $text_empty; ?></div>
        <div class="buttons">
            <div class="right"><a href="<?php echo $continue; ?>" class="button"><span><?php echo $button_continue; ?></span></a></div>
        </div>
    <?php } ?>
    <?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>