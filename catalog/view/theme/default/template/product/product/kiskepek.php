<?php
$_['korhinta']          = array(
    'type'              => 'checkbox',
    'name'              => 'korhinta',
    'default'           => "1",
    'heading'           => 'Körhinta'
);

$_['korhinta_darab']    = array(
    'type'              => 'text',
    'name'              => 'korhinta_darab',
    'size'              => "2",
    'default'           => "3",
    'heading'           => "Mejelenít"
);

$_['korhinta_auto']          = array(
    'type'              => 'checkbox',
    'name'              => 'korhinta_auto',
    'default'           => "1",
    'heading'           => 'Auto'
);

$_['korhinta_infi']          = array(
    'type'              => 'checkbox',
    'name'              => 'korhinta_infi',
    'default'           => "1",
    'heading'           => 'Körbe-körbe'
);

$_['korhinta_speed']    = array(
    'type'              => 'text',
    'name'              => 'korhinta_speed',
    'size'              => "5",
    'default'           => "1500",
    'heading'           => "Sebesség"
);

?>