<?php if ( isset($package) ) { ?>

<div class="box-csomag">

        <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/plus.png')) {
            $plus = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/plus.png';
        } else {
            $plus = DIR_TEMPLATE_IMAGE.'default/image/plus.png';
        } ?>
    <fieldset>
        <legend class="btn">A csomag tartalma</legend>
            <div class="csomag">
                <?php $pluszjel = false;?>

                <ul  class="csomag-tartalma">
                    <?php  foreach ($package as $product) { ?>
                        <li>
                            <div class="plus">
                                <?php if ($pluszjel) { ?>
                                    <img src="<?php echo $plus?>">
                                <?php } ?>
                            </div>
                            <?php $pluszjel = true;?>

                            <div class="termek">
                                <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                                    $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
                                } else {
                                    $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
                                } ?>
                                <?php include($arkiir); ?>
                            </div>

                        </li>
                    <?php } ?>
                </ul>
            </div>
    </fieldset>
</div>

<?php } ?>
<?php if (isset($beallitas_termek_aloldal['korhinta']) && $beallitas_termek_aloldal['korhinta'] == 1) { ?>
    <script type="text/javascript" src="catalog/view/javascript/bxslider/jquery.bxslider.min.js"></script>
    <link rel="stylesheet" type="text/css" href="catalog/view/javascript/bxslider/jquery.bxslider.css" />
    <script>
        $(document).ready(function(){
            $('.csomag-tartalma').bxSlider({
             minSlides: <?php echo isset($beallitas_termek_aloldal['korhinta_darab']) ? $beallitas_termek_aloldal['korhinta_darab'] : 3?>,
             maxSlides: <?php echo isset($beallitas_termek_aloldal['korhinta_darab']) ? $beallitas_termek_aloldal['korhinta_darab'] : 3?>,
             slideWidth: <?php echo isset($beallitas_termek_aloldal['korhinta_width']) ? $beallitas_termek_aloldal['korhinta_width'] : 260?>,
             slideMargin: 10,
             //controls: true,
             infiniteLoop: false,
             responsive: true,
             hideControlOnEnd: true,
             adaptiveHeight: false

            });
         });
    </script>
<?php } ?>