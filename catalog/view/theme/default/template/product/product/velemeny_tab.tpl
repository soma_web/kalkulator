<div id="velemeny_tabs" class="velemeny_htabs">
    <?php if ($review_status) { ?>
        <span class="review-title"><?php echo $tab_review; ?></span>
            <div id="tab-review" class="tab-content">
                <div id="review"></div>
                <h2 id="review-title"><?php echo $text_write; ?></h2>
                <b><?php echo $entry_name; ?></b><br />
                <input type="text" name="name" value="" />
                <br />
                <br />
                <b><?php echo $entry_review; ?></b>
                <textarea name="text" cols="40" rows="8" style="width: 98%;"></textarea>
                <span style="font-size: 11px;"><?php echo $text_note; ?></span><br />
                <br />
                <b><?php echo $entry_rating; ?></b> <span><?php echo $entry_bad; ?></span>&nbsp;
                <input type="radio" name="rating" value="1" />
                &nbsp;
                <input type="radio" name="rating" value="2" />
                &nbsp;
                <input type="radio" name="rating" value="3" />
                &nbsp;
                <input type="radio" name="rating" value="4" />
                &nbsp;
                <input type="radio" name="rating" value="5" />
                &nbsp; <span><?php echo $entry_good; ?></span><br />
                <br />
                <b><?php echo $entry_captcha; ?></b><br />
                <input type="text" name="captcha" value="" />
                <br />
                <img src="index.php?route=product/product/captcha" alt="" id="captcha" /><br />
                <br />
                <div class="buttons">
                    <div class="right"><a id="button-review" class="button"><?php echo $button_continue; ?></a></div>
                </div>
            </div>
    <?php } ?>
</div>