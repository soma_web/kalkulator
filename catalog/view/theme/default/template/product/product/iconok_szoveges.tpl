<div class="product_icons">

    <?php if (isset($beallitas_termek_aloldal['uj']) && $beallitas_termek_aloldal['uj']) { ?>
        <?php if (isset($uj) && $uj == 1) { ?>
            <div class="uj_szoveges">
                <? $text_uj = $this->language->get('text_uj');
                if (isset($text_uj)) {
                    echo $text_uj;
                } ?>
            </div>
        <?php } ?>
    <?php } ?>

    <?php if (isset($beallitas_termek_aloldal['pardarab']) && $beallitas_termek_aloldal['pardarab']) { ?>
        <?php if ($product_info['quantity'] < 5) { ?>
            <div class="pardarab_szoveges">
                <? $text_pardarab = $this->language->get('text_pardarab');
                if (isset($text_pardarab)) {
                    echo $text_pardarab;
                } ?>
            </div>
        <?php } ?>
    <?php } ?>

    <?php if (isset($beallitas_termek_aloldal['szazalek']) && $beallitas_termek_aloldal['szazalek']) { ?>
        <?php if ($product_info['special']) { ?>
            <div class="akcio_szoveges">
                <? $text_akcios = $this->language->get('text_akcios');
                if (isset($text_akcios)) {
                    echo $text_akcios;
                } ?>
            </div>
        <?php } ?>
    <?php } ?>

    <?php if (isset($beallitas_termek_aloldal['mennyisegi']) && $beallitas_termek_aloldal['mennyisegi']) { ?>
        <?php if (isset($discounts) && count($discounts) > 0) { ?>
            <div class="mennyisegi_szoveges">
                <? $text_mennyisegi = $this->language->get('text_mennyisegi');
                if (isset($text_mennyisegi)) {
                    echo $text_mennyisegi;
                } ?>
            </div>
        <?php } ?>
    <?php } ?>

</div>