<?php if ($discounts) { ?>
    <div class="discount">
        <table>
            <?php if(isset($beallitas_termek_aloldal['netto_ar']) || isset($beallitas_termek_aloldal['netto_szoveg']) ||
                isset($beallitas_termek_aloldal['brutto_ar']) || isset($beallitas_termek_aloldal['brutto_szoveg'])
            || isset($beallitas_termek_aloldal['net_bru'])) { ?>

                <?php foreach ($discounts as $discount) { ?>
                    <tr>
                        <?php

                            $price_line = "";

                            if(isset($beallitas_termek_aloldal['brutto_szoveg']) || isset($beallitas_termek_aloldal['brutto_ar'])) {
                                $brutto = '<span class="price-brutto price-tax" style="padding-left: 10px;">';
                                if(isset($beallitas_termek_aloldal['brutto_szoveg']) && ($beallitas_termek_aloldal['brutto_szoveg']) == 1
                                && isset($beallitas_termek_aloldal['brutto_ar']) && ($beallitas_termek_aloldal['brutto_ar']) == 1) {
                                    $brutto .= $text_tax_brutto." ";
                                }
                                if(isset($beallitas_termek_aloldal['brutto_ar']) && ($beallitas_termek_aloldal['brutto_ar']) == 1) {
                                    $brutto .= '<span class="p-brutto">';
                                    $brutto .= $discount['price'];
                                    $brutto .= '</span>';
                                }
                                $brutto .= '</span>';
                            }

                            if(isset($beallitas_termek_aloldal['netto_szoveg']) || isset($beallitas_termek_aloldal['netto_ar'])) {
                                $netto = '<span class="price-netto price-tax" style="padding-left: 10px;">';
                                if(isset($beallitas_termek_aloldal['netto_szoveg']) && ($beallitas_termek_aloldal['netto_szoveg']) == 1
                                && isset($beallitas_termek_aloldal['netto_ar']) && ($beallitas_termek_aloldal['netto_ar']) == 1) {
                                    $netto .= $text_tax." ";
                                }
                                if(isset($beallitas_termek_aloldal['netto_ar']) && ($beallitas_termek_aloldal['netto_ar']) == 1
                                && isset($beallitas_termek_aloldal['netto_szoveg']) && ($beallitas_termek_aloldal['netto_szoveg'])) {
                                    $netto .= '<span class="p-netto">';
                                    $netto .= $discount['price_netto'];
                                    $netto .= '</span>';
                                } elseif (isset($beallitas_termek_aloldal['netto_ar']) && ($beallitas_termek_aloldal['netto_ar']) == 1
                                    && !(isset($beallitas_termek_aloldal['netto_szoveg']))) {
                                    $netto .= '<span class="p-netto">';
                                    $netto .= $discount['price_netto'];
                                    $netto .= " + ".$discount['tax_name'][0];
                                    $netto .= '</span>';
                                }
                                $netto .= '</span>';
                            }

                            if(isset($beallitas_termek_aloldal['net_bru']) && ($beallitas_termek_aloldal['net_bru']) == 1
                            && isset($beallitas_termek_aloldal['netto_ar']) && ($beallitas_termek_aloldal['netto_ar']) == 1
                            && isset($beallitas_termek_aloldal['brutto_ar']) && ($beallitas_termek_aloldal['brutto_ar']) == 1) {
                                $price_line = $netto.$brutto;
                            } else {
                                $price_line = $brutto;
                            }
                        ?>

                        <td><?php echo sprintf($text_discount, $discount['quantity'], $price_line); ?></td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <?php foreach ($discounts as $discount) { ?>
                    <tr>
                        <?php if ($this->config->get('megjelenit_product_brutto_ar') == 1) { ?>
                            <td>
                                <?php echo sprintf($text_discount, $discount['quantity'], $discount['price']); ?>
                            </td>
                            <?php if ($this->config->get('megjelenit_product_netto_ar') == 1 && $discount['price'] != $discount['price_netto']) { ?>
                                <td><span class="price-tax" style="padding-left: 10px;"><?php echo $text_tax;  echo " "; echo $discount['price_netto'] ?></span></td>
                            <?php } ?>

                        <?php } elseif ($this->config->get('megjelenit_product_netto_ar') == 1 && $discount['price'] != $discount['price_netto']) { ?>
                            <td>
                                <?php echo sprintf($text_discount, $discount['quantity'], $discount['price_netto']); ?>
                                <span class="puszafa">+ <?php echo $discount['tax_name'][0]; ?></span>
                            </td>
                        <?php } elseif ($this->config->get('megjelenit_product_netto_ar') == 1 && $discount['price'] == $discount['price_netto']) { ?>
                            <td>
                                <?php echo sprintf($text_discount, $discount['quantity'], $discount['price_netto']); ?>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            <?php } ?>
        </table>
    </div>
<?php } ?>