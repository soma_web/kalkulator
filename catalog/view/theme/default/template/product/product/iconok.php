<?php
$_['uj']          = array(
    'type'              => 'checkbox',
    'name'              => 'uj',
    'default'           => "1",
    'heading'           => 'Új'
);

$_['ujdonsag']          = array(
    'type'              => 'checkbox',
    'name'              => 'ujdonsag',
    'default'           => "1",
    'heading'           => 'Újdonság'
);

$_['pardarab']          = array(
    'type'              => 'checkbox',
    'name'              => 'pardarab',
    'default'           => "1",
    'heading'           => 'Pár darab'
);

$_['kifuto']          = array(
    'type'              => 'checkbox',
    'name'              => 'kifuto',
    'default'           => "1",
    'heading'           => 'Kifutó'
);

$_['szazalek']          = array(
    'type'              => 'checkbox',
    'name'              => 'szazalek',
    'default'           => "1",
    'heading'           => 'Százalék'
);
$_['szazalek_ertek']          = array(
    'type'              => 'checkbox',
    'name'              => 'szazalek_ertek',
    'default'           => "1",
    'heading'           => 'Százalék érték'
);

$_['mennyisegi']          = array(
    'type'              => 'checkbox',
    'name'              => 'mennyisegi',
    'default'           => "1",
    'heading'           => 'Mennyiségi'
);

$_['raktar']          = array(
    'type'              => 'checkbox',
    'name'              => 'raktar',
    'default'           => "1",
    'heading'           => 'Raktár'
);

$_['kamera']          = array(
    'type'              => 'checkbox',
    'name'              => 'kamera',
    'default'           => "1",
    'heading'           => 'Kamera'
);

$_['csapo']          = array(
    'type'              => 'checkbox',
    'name'              => 'csapo',
    'default'           => "1",
    'heading'           => 'Csapo'
);

$_['compare']          = array(
    'type'              => 'checkbox',
    'name'              => 'compare',
    'default'           => "1",
    'heading'           => 'Összehasonlítás'
);

$_['wishlist']          = array(
    'type'              => 'checkbox',
    'name'              => 'wishlist',
    'default'           => "1",
    'heading'           => 'Kedvencek'
);
$_['ajandek']          = array(
    'type'              => 'checkbox',
    'name'              => 'ajandek',
    'default'           => "1",
    'heading'           => 'Ajándék'
);

?>