<?php if (isset($termekadatok['megjelenit_product_mennyisegi_egyseg']) && !empty($termekadatok['megjelenit_product_mennyisegi_egyseg'])) { ?>
    <div class="termekadatok termekadatok-megyseg" >
        <span class="termekadatok-megyseg-szoveg"><?php echo $termekadatok['megjelenit_product_mennyisegi_egyseg']['text_name']; ?></span>
        <span class="termekadatok-megyseg-ertek"><?php echo $termekadatok['megjelenit_product_mennyisegi_egyseg']['value']; ?></span>
    </div>
<?php } ?>