<?php if ($text_garancia) { ?>

    <?php if ($garancia_description) { ?>
        <div class="garancia">
            <a class="various " href="#garancia_product">
                <span><?php echo $text_garancia ?></span>
            </a>
        </div>

        <div id="garancia_product" class=""
                 style="display: none; min-width: 320; min-height: 150px; background-color: white;">
            <?php echo $garancia_description?>
        </div>

    <?php } else { ?>
        <div class="garancia">
            <a style="cursor: default">
                <span><?php echo $text_garancia ?></span>
            </a>
        </div>
    <?php } ?>


<?php } ?>