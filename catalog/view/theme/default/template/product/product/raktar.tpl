
<?php if (isset($warehouses) && !empty($warehouses)) { ?>
    <div class="warehouse">
        <span><?php echo $text_raktar;?>
            <?php if ($warehouses_out_of_stock) { ?>
                <?php echo ' - '. $text_stock_in .' '. $stock_in_date; ?>
            <?php } ?>
        </span>
        <ul>
            <?php $hossz = 0; ?>
            <?php foreach($warehouses as $warehouse) { ?>
                <li class="<?php echo $warehouse['class']?>">
                     <?php echo $warehouse['postcode']?> <?php echo $warehouse['city']?>, <?php echo $warehouse['address']?> <span><?php echo $warehouse['viszontelado_quantity']; ?></span>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>

termekadatok-elerhetoseg-ertek