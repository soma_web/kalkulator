<?php if (isset($termekadatok['megjelenit_product_gyarto']) && !empty($termekadatok['megjelenit_product_gyarto'])) { ?>
    <div class="termekadatok termekadatok-gyarto" >
        <span class="termekadatok-gyarto-szoveg"><?php echo $termekadatok['megjelenit_product_gyarto']['text_name']; ?></span>
        <span class="termekadatok-gyarto-ertek"><?php echo $termekadatok['megjelenit_product_gyarto']['value']; ?></span>
    </div>
<?php } ?>