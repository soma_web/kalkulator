<?php
$vantab = false;
if (isset($products_accessory) && $products_accessory) {
    $vantab = true;
    $tab = "saccessory";

} elseif (isset($products_general_accessory) && $products_general_accessory ) {
    $vantab = true;
    $tab = "general_accessory";
}
?>

<?php if ($vantab) { ?>
    <div id="tabtartozek" class="htabs">
        <div class="inner-htabs">
            <?php if ($products_accessory) { ?>
                <?php if ($tab == "saccessory") { ?>
                    <a  style="position: relative; left: 8px; margin-right: 18px;"  href="#tab-ajanlott"><?php echo $tab_ajanlott; ?> (<?php echo count($products_accessory); ?>) </a>
                <?php } else {?>
                    <a  href="#tab-ajanlott"><?php echo $tab_ajanlott; ?> (<?php echo count($products_accessory); ?>) </a>
                <?php } ?>
            <?php } ?>

            <?php if ($products_general_accessory) { ?>
                <?php if ($tab == "general_accessory") { ?>
                    <a style="position: relative; left: 8px; margin-right: 18px;"  href="#tab-kereso"><?php echo $tab_kereso; ?> (<?php echo count($products_general_accessory); ?>)</a>
                <?php } else {?>
                    <a href="#tab-kereso"><?php echo $tab_kereso; ?> (<?php echo count($products_general_accessory); ?>)</a>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
<?php } ?>

<?php if ($products_accessory) { ?>
    <div id="tab-ajanlott" class="tab-content">
        <div class="box-product tab_kornyezet tab-ajanlott" style="width: 100%">
                <?php foreach ($products_accessory as $product) { ?>
                        <div class="termek_tabban">
                            <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                                $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
                            } else {
                                $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
                            } ?>
                            <?php include($arkiir); ?>
                        </div>
                <?php } ?>
        </div>
    </div>
<?php } ?>

<?php if ($products_general_accessory) { ?>
    <div id="tab-kereso" class="tab-content">

        <div class="box-product tab_kornyezet tab-kereso" style="width: 100%">
                <?php foreach ($products_general_accessory as $product) { ?>
                        <div class="termek_tabban">
                            <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                                $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
                            } else {
                                $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
                            } ?>
                            <?php include($arkiir); ?>
                        </div>

                <?php } ?>
        </div>


    </div>
<?php } ?>


<script type="text/javascript" src="catalog/view/javascript/bxslider/jquery.bxslider.min.js"></script>
    <link rel="stylesheet" type="text/css" href="catalog/view/javascript/bxslider/jquery.bxslider.css" />

<script>

    $('#tabtartozek a').tabs({
        bxSlidesOn: <?php echo isset($beallitas_termek_aloldal['korhinta_tart']) ? 1 : 0?>,
        minSlides: <?php echo isset($beallitas_termek_aloldal['korhinta_darab_tart']) ? $beallitas_termek_aloldal['korhinta_darab_tart'] : 3?>,
        maxSlides: <?php echo isset($beallitas_termek_aloldal['korhinta_darab_tart']) ? $beallitas_termek_aloldal['korhinta_darab_tart'] : 3?>,
        slideWidth: <?php echo isset($beallitas_termek_aloldal['korhinta_width_tart']) ? $beallitas_termek_aloldal['korhinta_width_tart'] : 260?>,
        slideMargin: <?php echo isset($beallitas_termek_aloldal['korhinta_margin_tart']) ? $beallitas_termek_aloldal['korhinta_margin_tart'] : 20?>,
        randomStart: <?php echo isset($beallitas_termek_aloldal['korhinta_random_tart']) ? "true" : "false" ?>,
        autoHidePager: true /*  Eltűnteti a nyilakat, ha minSlides értékénél kevesebb kép jelenik meg. */

    });

</script>

