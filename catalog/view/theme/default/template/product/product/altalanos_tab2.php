<?php
$_['description']          = array(
    'type'              => 'checkbox',
    'name'              => 'description',
    'default'           => "1",
    'heading'           => 'Leírás megjelenítés'
);

$_['meret']          = array(
    'type'              => 'checkbox',
    'name'              => 'meret',
    'default'           => "1",
    'heading'           => 'Méret megjelenítés'
);

$_['attribute_groups']          = array(
    'type'              => 'checkbox',
    'name'              => 'attribute_groups',
    'default'           => "1",
    'heading'           => 'Tulajdonságok megjelenítés'
);

$_['review_status']          = array(
    'type'              => 'checkbox',
    'name'              => 'review_status',
    'default'           => "1",
    'heading'           => 'Vélemények megjelenítés'
);

$_['test_description']          = array(
    'type'              => 'checkbox',
    'name'              => 'test_description',
    'default'           => "1",
    'heading'           => 'Tesztek megjelenítés'
);

$_['products']          = array(
    'type'              => 'checkbox',
    'name'              => 'products',
    'default'           => "0",
    'heading'           => 'Kapcsolódó temékek megjelenítés'
);

$_['ajandek']    = array(
    'type'              => 'checkbox',
    'name'              => 'ajandek',
    'default'           => "0",
    'heading'           => "Ajándék megjelenítése"
);

$_['images']          = array(
    'type'              => 'checkbox',
    'name'              => 'images',
    'default'           => "0",
    'heading'           => 'Képek megjelenítés'
);

$_['korhinta']          = array(
    'type'              => 'checkbox',
    'name'              => 'korhinta',
    'default'           => "1",
    'heading'           => 'Körhinta'
);

$_['korhinta_darab']    = array(
    'type'              => 'text',
    'name'              => 'korhinta_darab',
    'size'              => "2",
    'default'           => "3",
    'heading'           => "Mejelenít"
);
$_['korhinta_width']    = array(
    'type'              => 'text',
    'name'              => 'korhinta_width',
    'size'              => "2",
    'default'           => "260",
    'heading'           => "Termék szélesség"
);

$_['facebook_comments'] = array(
    'type'              => 'checkbox',
    'name'              => 'facebook_comments',
    'default'           => "1",
    'heading'           => 'Facebook Kommentek megjelenítése'
);

$_['downloads'] = array(
    'type'              => 'checkbox',
    'name'              => 'downloads',
    'default'           => "1",
    'heading'           => 'Letöltések megjelenítése'
);

$_['questions'] = array(
    'type'              => 'checkbox',
    'name'              => 'questions',
    'default'           => "1",
    'heading'           => 'Kérdések megjelenítése'
);

?>