
<!-- csomag ajánlatok-->
<?php if(isset($packages) && $packages) { ?>

    <div id="package_fejlec" class="box">

        <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/plus.png')) {
            $plus = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/plus.png';
        } else {
            $plus = DIR_TEMPLATE_IMAGE.'default/image/plus.png';
        } ?>


       <div class="box-csomag">
           <?php  foreach ($packages as $termekek) { ?>
               <div class="csomag">
                   <?php $product = $termekek['package']; ?>
                   <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                       <?php $pluszjel = false;?>
                   <ul  class="package-tartalma">
                       <?php  foreach ($termekek['products'] as $product) { ?>
                       <li>
                           <div class="plus">
                               <?php if ($pluszjel) { ?>
                                       <img src="<?php echo $plus?>">
                               <?php } ?>
                           </div>
                           <?php $pluszjel = true;?>

                            <div class="termek">
                                <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                                    $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
                                } else {
                                    $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
                                } ?>
                                <?php include($arkiir); ?>
                            </div>

                        </li>
                       <?php } ?>
                    </ul>
<!-- this is the package price labell -->
                   <div class="csomag-price">
                       <?php $product = $termekek['package']; ?>
                       <?php if($this->config->get('megjelenit_brutto_ar') == 1){ ?>
                           <?php if($this->config->get('megjelenit_brutto_netto') == 1){ ?>
                               <?php if ($product['price']) { ?>
                                   <div class="price <?php echo $szazalek_class?>" >
                                       <?php if (!$product['special']) { ?>
                                           <span class="price-new"><?php echo $product['price']; ?></span>
                                       <?php } else { ?>
                                           <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
                                       <?php } ?>
                                   </div>
                                   <?php if ($product['tax']) { ?>
                                       <?php if($this->config->get('megjelenit_netto_ar') == 1){ ?>
                                           <?php if (!$product['special']) { ?>
                                               <span class="price-tax"><?php echo "text_netto"; ?> <?php echo $product['price_netto']; ?></span>
                                           <?php } else { ?>
                                               <span class="price-tax"><?php echo "text_netto"; ?> <?php echo $product['special_netto']; ?></span>
                                           <?php } ?>
                                       <?php } ?>
                                   <?php } ?>
                               <?php } ?>
                           <?php } else {?>
                               <?php if ($product['price_netto']) { ?>
                                   <?php if($this->config->get('megjelenit_netto_ar') == 1){ ?>
                                       <div class="price  <?php echo $szazalek_class?>">
                                           <?php if (!$product['special_netto']) { ?>
                                               <span class="price-new"><?php echo $product['price_netto']; ?></span>
                                           <?php } else { ?>
                                               <span class="price-old"><?php echo $product['price_netto']; ?></span> <span class="price-new"><?php echo $product['special_netto']; ?></span>
                                           <?php } ?>
                                       </div>
                                       <?php if (!$product['special_netto']) { ?>
                                           <span class="price-tax"><?php echo "Brutto:"; ?> <?php echo $product['price']; ?></span>
                                       <?php } else { ?>
                                           <span class="price-tax"><?php echo "Brutto:"; ?> <?php echo $product['special']; ?></span>
                                       <?php } ?>
                                   <?php } else {?>

                                       <div class="price">
                                           <?php if (!$product['special']) { ?>
                                               <span class="price-new"><?php echo $product['price']; ?></span>
                                           <?php } else { ?>
                                               <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
                                           <?php } ?>
                                       </div>
                                   <?php } ?>
                               <?php } ?>
                            <?php } ?>


                       <?php } elseif ($this->config->get('megjelenit_netto_ar') == 1){ ?>
                           <?php if ($product['price_netto']) { ?>
                               <div class="price  <?php echo $szazalek_class?>">
                                   <?php if (!$product['special_netto']) { ?>
                                       <span class="text-netto"><?php echo $text_netto; ?></span>
                                       <?php if ($this->config->get('megjelenit_netto_ujsor') == 1){ ?>
                                           <br />
                                       <?php } ?>
                                       <span class="price-new"><?php echo $product['price_netto']; ?></span>
                                   <?php } else { ?>
                                       <span class="text-netto"><?php echo $text_netto; ?></span>
                                       <?php if ($this->config->get('megjelenit_netto_ujsor') == 1){ ?>
                                           <br />
                                       <?php } ?>
                                       <span class="price-old"><?php echo $product['price_netto']; ?></span> <span class="price-new"><?php echo $product['special_netto']; ?></span>
                                   <?php } ?>
                               </div>
                           <?php } ?>
                       <?php } ?>
                       <div class="price"><?php echo $text_eredeti_ar?> <?php echo $product['eredeti_ar']?></div>



                       <?if ($product['csomagolasi_mennyiseg'] > 1 && $this->config->get('megjelenit_csomagolas_admin') == 1 ){ ?>
                           <div class="cart">

                               <input type="hidden" name="quantity2" />
                               <input id="category_list_csomagolas_<?php echo $product['product_id']; ?>" type="hidden" value="<? echo $product['csomagolasi_mennyiseg']?>" />
                               <?php if($this->config->get('megjelenit_csomagolasi_mennyiseg') == 1){ ?>
                               <div class="csomagolasi_mennyiseg_racs" >
                                   <? }?>

                                   <?php if($this->config->get('megjelenit_mennyiseg') == 1 || true){ ?>
                                       <input class="mennyit_vasarol" id="category-grid-quantity_<?php echo $product['product_id']; ?>" type="text" value="1" />
                                       <?php if($this->config->get('megjelenit_csomagolasi_mennyiseg') == 1){ ?>
                                           <span class="csomagolasi_mennyiseg_span" style="margin-left: 4px;"><?echo 'x '.$product['csomagolasi_mennyiseg'].' '. $product['megyseg'].'/'.$product['csomagolasi_egyseg']?></span>
                                       <?php }?>

                                   <?php } else {?>
                                       <input  id="category-grid-quantity_<?php echo $product['product_id']; ?>" type="hidden" value="1" />
                                       <?php if($this->config->get('megjelenit_csomagolasi_mennyiseg') == 1){ ?>
                                           <span class="csomagolasi_mennyiseg_span"><?echo $product['csomagolasi_mennyiseg'].' '. $product['megyseg'].'/'.$product['csomagolasi_egyseg']?></span>
                                       <?php }?>
                                   <?php }?>

                                   <?php if($this->config->get('megjelenit_csomagolasi_mennyiseg') == 1){ ?>
                               </div>
                           <? }?>
                               <span class="atmenet-hatter">
                            <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>',$('#category-grid-quantity_<?php echo $product['product_id']; ?>').val()*<? echo $product['csomagolasi_mennyiseg']?>,'<?php echo $this->config->get('config_template')?>','<?php echo $module_name.$product['product_id']?>')" class="button" />
                            </span>
                           </div>

                       <? } else { ?>
                           <div class="cart">
                               <?php $product['csomagolasi_mennyiseg'] = 1; ?>
                               <?php if($this->config->get('megjelenit_mennyiseg') == 1 || true){ ?>
                                   <input class="mennyit_vasarol" id="category-grid-quantity_<?php echo $product['product_id']; ?>" type="text" value="1" style="margin-right: 1px;" />
                               <?php } else { ?>
                                   <input id="category-grid-quantity_<?php echo $product['product_id']; ?>" type="hidden" value="1" style="margin-right: 1px;" />
                               <?php }?>
                               <span class="atmenet-hatter">
                                    <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>',$('#category-grid-quantity_<?php echo $product['product_id']; ?>').val()*<? echo $product['csomagolasi_mennyiseg']?>,'<?php echo $this->config->get('config_template')?>','<?php echo $module_name.$product['product_id']?>')" class="button" />
                               </span>
                           </div>

                       <? }?>
                   </div>
               </div>
           <?php } ?>
       </div>

    </div>

    <?php if (isset($beallitas_termek_aloldal['korhinta']) && $beallitas_termek_aloldal['korhinta'] == 1) { ?>
        <script>
        $('.package-tartalma').bxSlider({
            minSlides: <?php echo isset($beallitas_termek_aloldal['korhinta_darab']) ? $beallitas_termek_aloldal['korhinta_darab'] : 3?>,
            maxSlides: <?php echo isset($beallitas_termek_aloldal['korhinta_darab']) ? $beallitas_termek_aloldal['korhinta_darab'] : 3?>,
            slideWidth: <?php echo isset($beallitas_termek_aloldal['korhinta_width']) ? $beallitas_termek_aloldal['korhinta_width'] : 260?>,
            slideMargin: 10,
            //controls: true,
            infiniteLoop: false,
            responsive: true,
            hideControlOnEnd: true,
            adaptiveHeight: false
        });
        </script>
    <?php } ?>
<?php } ?>


