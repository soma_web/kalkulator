<?php if(isset($video) && $video) { ?>
    <div class="video"><?php echo $video; ?></div>
<?php } ?>

<?php if(isset($video_url) && $video_url) { ?>
    <div class="video"><iframe width="100%" height="400px" src="<?php echo $video_url; ?>" frameborder="0" allowfullscreen></iframe></div>
<?php } ?>
