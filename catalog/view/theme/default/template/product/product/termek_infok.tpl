<table>
<?php foreach($informaciok as $value) {?>
    <tr>
        <?php if (count($value['value']) == 1 && !$value['group_filter']) {?>
            <td><span><?php echo $value['text_name']; ?></span></td>
            <?php if (gettype($value['value']) == "array") { ?>
                <?php if (count($value['value']) > 0) { ?>
                    <?php foreach ($value['value'] as $tomb){ ?>
                        <td> <?php echo $tomb; ?></td>
                    <?php } ?>
                <?php } ?>
            <?php } else { ?>
                <td> <?php echo $value['value']; ?></td>
            <?php } ?>
        <?php } elseif (!$value['group_filter']) {?>
            <td style="display: table-row-group;"><span><?php echo $value['text_name']; ?></span></td>

            <td>
                <table>
                    <?php foreach($value['value'] as $megnevezes) { ?>
                        <tr>
                            <td> <?php echo $megnevezes; ?></td>
                        </tr>
                    <?php } ?>
                </table>
            </td>




        <?php } elseif ($value['group_filter']) {?>
            <td style="display: table-row-group;"><?php echo $value['text_name']; ?></td>
            <td>
                <table class="form">
                    <?php foreach($value['value'] as $csoport) { ?>
                        <tr>
                            <td style="display: table-row-group;"><?php echo $csoport['group']; ?><td>


                                <table class="form">
                                    <?php foreach($csoport['filter'] as $filter) { ?>
                                        <tr>
                                            <td> <?php echo $filter['filter_name']; ?></td>
                                        </tr>
                                    <?php } ?>
                                </table>

                        </tr>
                    <?php } ?>
                </table>
            </td>

        <?php }?>




    </tr>
<?php }?>
</table>