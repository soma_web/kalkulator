<?php $megjelenit_termek_csoportok = $this->config->get('termek_beallitas_termek_csoportok'); ?>
<?php if ($megjelenit_termek_csoportok)  { ?>

    <?php
    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/stylesheet_product.php')) {
        $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/stylesheet/stylesheet_product.php';
        include($arkiir);

    } elseif (file_exists(DIR_TEMPLATE . 'default/stylesheet/stylesheet_product.php')) {
        $arkiir = DIR_TEMPLATE.'default/stylesheet/stylesheet_product.php';
        include($arkiir);

    } ?>

    <?php   $elso       = true;
    $elozo      = 0;
    $divszamol_product  = 1;
    $div_nyit_product   = 0;
    $div_zar_product    = 0;
    ?>
    <div class="product-info">
    <?php foreach ($megjelenit_termek_csoportok as $key=>$value) { ?>
        <?php
        if ($value['default']['status'] == 0) {
            continue;
        }

        if ($elso) {$elso = false;}
        else {
            $elozo = $divszamol_product;
        }
        $divszamol_product = count(explode('_',$key));

        if($divszamol_product <= $elozo) {
            for($i=$divszamol_product; $elozo >=$i; $i++) {
                echo "</div>";
                $div_zar_product ++;

            }
        }
        $elotag = ($divszamol_product%2 == 0) ? "product_oszlop_" : "product_sor_";

        $style_inlines = isset($value['default']['inline']) && $value['default']['inline'] ? htmlspecialchars_decode($value['default']['inline']) : '';
        if ($style_inlines) {
            $style_inlines = str_replace(' ','',$style_inlines);
            $style_inline = explode(';',$style_inlines);

            foreach($style_inline as $key_inline=>$inline) {
                $pos = strpos("$inline","$");
                if ($pos !== false) {
                    $pos1 = strpos($inline,"this",$pos);
                    if ($pos1 == $pos+1) {
                        $pos2 = strpos($inline,"-",$pos1);
                        if ($pos2 == $pos1+4) {
                            $pos3 = strpos($inline,">",$pos2);
                            if ($pos3 == $pos2+1) {
                                $pos4 = strpos($inline,"config",$pos3);
                                if ($pos4 == $pos3+1) {
                                    $pos5 = strpos($inline,"-",$pos4);
                                    if ($pos5 == $pos4+6) {
                                        $pos6 = strpos($inline,">",$pos5);
                                        if ($pos6 == $pos5+1) {
                                            $pos7 = strpos($inline,"get",$pos6);
                                            if ($pos7 == $pos6+1) {
                                                $parameter_pos_tol = strpos($inline,"'",$pos7);
                                                if ($parameter_pos_tol == $pos7+4) {
                                                    $parameter_pos_tol ++;
                                                    $parameter_pos_ig = strpos($inline,"'",$parameter_pos_tol);
                                                    $parameter_pos_ig;
                                                    $parameter = substr($inline,$parameter_pos_tol,($parameter_pos_ig-$parameter_pos_tol));

                                                } else {
                                                    $parameter_pos_tol = strpos($inline,'"',$pos7);
                                                    if ($parameter_pos_tol == $pos7+4) {
                                                        $parameter_pos_tol ++;
                                                        $parameter_pos_ig = strpos($inline,"'",$parameter_pos_tol);
                                                        $parameter = substr($inline,$parameter_pos_tol,($parameter_pos_ig-$parameter_pos_tol));
                                                    }
                                                }

                                                if (isset($parameter) && $parameter) {
                                                    $eredmeny=$this->config->get($parameter);
                                                    $style_inline[$key_inline] = substr($inline,0,$pos);
                                                    $style_inline[$key_inline] .= $eredmeny;
                                                    $style_inline[$key_inline] .= substr($inline,$parameter_pos_ig+2);
                                                }

                                            }

                                        }

                                    }

                                }

                            }

                        }
                    }
                }
            }
            $style_inlines = implode(';',$style_inline);
        }
        ?>
    <div class="<?php echo $elotag.$key. ' ' . (isset($value['default']['class']) ? $value['default']['class'] : '')?> "  style="<?php echo $style_inlines?>">
            <?php $div_nyit_product++?>
            <?php if ($value['default']['status'] == 1) { ?>
                <?php if(isset($value['templates']) && !empty($value['templates'])) { ?>
                    <?php foreach ($value['templates'] as $keytemplate=>$beallitas_termek_aloldal) { ?>
                        <?php if (isset($beallitas_termek_aloldal['status']) && $beallitas_termek_aloldal['status'] == 1) {?>
                            <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product/'. $keytemplate .'.tpl')) {
                                $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/product/product/'.$keytemplate . '.tpl';
                            } else {
                                $arkiir = DIR_TEMPLATE.'default/template/product/product/'.$keytemplate.'.tpl';
                            } ?>
                            <?php include($arkiir); ?>
                        <?php } ?>

                    <?php } ?>
                <?php } ?>
            <?php } ?>
    <?php }


    if ($div_nyit_product - $div_zar_product > 0) {
        $div_nyitva_product = $div_nyit_product - $div_zar_product;
        for ($i=0; $div_nyitva_product > $i; $i++) {
            echo "</div>";
        }
    }
    ?>
    </div>


    <?php if (!empty($elonezet) ) { ?>
        <script>
            var listposition = $(".product-info").offset().top;
            $('html body').animate({ scrollTop: listposition-10 }, 'fast');
        </script>
    <?php } ?>
<?php } ?>