<?php if (isset($header))  echo $header; ?>
<?php $modularis = $this->config->get('lista_beallitas_lista');?>
<?php $modularis = $modularis['modularis']?>
<?php $megjelenit_lista_csoportok = $this->config->get('lista_beallitas_lista_csoportok'); ?>

<div class="breadcrumb">
        <?php $elemszam=count($breadcrumbs);
        $i=0;
        foreach ($breadcrumbs as $breadcrumb) { $i++;
            if ($i==$elemszam) { ?>
                <? echo $breadcrumb['separator']; ?><a class="utolsobread" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? } else {
                echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? }
        } ?>
    </div>
<?php echo $column_left; ?><?php echo $column_right; ?>
    <div id="content"><?php echo $content_top; ?>
        <?php if ($thumb && $image_show && $image_above_header) { ?>
            <div class="image image-above-header"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></div>
        <?php } ?>

        <?php if (isset($heading_title) && $description_show) { ?>
            <h1 class="heading-title" >
                <?php  echo $heading_title; ?>
            </h1>
        <?php } ?>

        <?php if ($thumb || $description) { ?>
            <div class="category-info">
                <?php if ($thumb && $image_show && $image_below_header) { ?>
                    <div class="image image-below-header"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></div>
                <?php } ?>
                <?php if ($description) { ?>
                    <?php echo $description; ?>
                <?php } ?>
            </div>
        <?php } ?>

    <?php if (isset($kategoria_lista) && $kategoria_lista) {?>
        <div class="box-product-categories">
            <?php foreach($categories as $category) { ?>
                <div>
                    <?php if ($category['name']) { ?>
                    <div class="name" style="min-height: 40px;"><a href="<? echo $category['href']?>"><?php echo $category['name']; ?></a></div>
                    <?php } ?>
                    <?php if ($category['thumb']) { ?>
                        <a href="<? echo $category['href']?>" class="image"><img style="vertical-align: middle" src="<?php echo $category['thumb']; ?>" alt="<?php echo $heading_title; ?>" /></a>
                    <?php } ?>
                    <?php if ($category['description'] && $description_show) { ?>
                        <a href="<? echo $category['href']?>" ><?php echo $category['description']; ?></a>
                    <?php } ?>
                    <a href="<?php echo $category['href']; ?>">
                       <input  style="margin 0 auto" type="button" value="<?php echo $button_bovebben; ?>" class="button nepszeru_button_cart" />
                    </a>
                </div>
            <?php } ?>
        </div>
    <?php } ?>

        <?php //refine search kiszedve ?>

        <?php if ($products) { ?>
            <?php if ($this->config->get('megjelenit_felul_lapszamozas') == 1) {?>

                <div class="product-filter">
                    <div class="display"><b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display('grid');"><?php echo $text_grid; ?></a></div>
                    <div class="limit"><b><?php echo $text_limit; ?></b>
                        <select onchange="location = this.value;">
                            <?php foreach ($limits as $limits) { ?>
                                <?php if ($limits['value'] == $limit) { ?>
                                    <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="sort"><b><?php echo $text_sort; ?></b>
                        <select onchange="location = this.value;">
                            <?php foreach ($sorts as $sorts) { ?>
                                <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                    <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="pagination"><?php echo $pagination; ?></div>

                <div class="product-compare"><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></div>
            <?php }?>



            <?php $bal = true; ?>
            <div class="box-product">
                <?php  foreach ($products as $product) { ?>

                    <?php if ($product['utalvany'] != 0) { ?>
                        <?php $class=" utalvany";?>
                    <?php } else {?>
                        <?php $class="";?>
                    <?php } ?>
                    <div class="termek_tabban <?php echo $class; ?>">

                        <?php if (isset($product['kiemelt_tpl']) && $product['kiemelt_tpl']) { ?>
                            <?php include($product['kiemelt_tpl']); ?>
                        <?php } else {?>

                            <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                                $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
                            } else {
                                $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
                            } ?>
                            <?php include($arkiir); ?>
                        <?php  } ?>
                    </div>

                <?php  } ?>
            </div>

            <div class="product-list">
                <?php foreach ($products as $product) { ?>
                    <div class="lista-elem">
                        <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/listanezet.tpl')) {
                                $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/listanezet.tpl';
                            } else {
                                $arkiir = DIR_TEMPLATE.'default/template/module/listanezet.tpl';
                            } ?>
                            <?php include($arkiir); ?>
                    </div>
                <?php } ?>

            </div>




            <div class="pagination"><?php echo $pagination; ?></div>
        <?php } ?>
        <?php if (!$categories && !$products) { ?>
            <div class="content"><?php echo $text_empty; ?></div>
            <div class="buttons">
                <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
            </div>
        <?php } ?>
        <?php echo $content_bottom; ?>
    </div>

<?php if (isset($footer)) echo $footer; ?>
