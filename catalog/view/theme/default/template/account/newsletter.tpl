<?php echo $header; ?>
    <div class="breadcrumb">
        <?php $elemszam=count($breadcrumbs);
        $i=0;
        foreach ($breadcrumbs as $breadcrumb) { $i++;
            if ($i==$elemszam) { ?>
                <? echo $breadcrumb['separator']; ?><a class="utolsobread" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? } else {
                echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? }
        } ?>
    </div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <h1><?php echo $heading_title; ?></h1>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
      <div class="content">
          <table class="form">
              <tr class="newslettertr">
                  <td><?php echo $entry_newsletter; ?>
                      <?php if($newsletter == '1') { ?>
                          <input id="newsletter" type="checkbox" class="css-checkbox" name="newsletter" checked="checked" value="1" />
                      <?php } else { ?>
                          <input id="newsletter" type="checkbox" class="css-checkbox" name="newsletter" value="1" />
                      <?php } ?>
                  </td>
                  <?php if ( ($this->config->get('megjelenit_regisztracio_hirlevel_kategoriak') == 1 && $ceges == 0) || ($this->config->get('megjelenit_regisztracio_ceges_hirlevel_kategoriak') == 1 && $ceges == 1) ) {?>
                      <?php if($newsletter == '1') { ?>
                          <td id="newslettercategories" class="newslettertd">
                      <?php } else { ?>
                          <td id="newslettercategories" class="newslettertd" style="display: none">
                      <?php } ?>
                      <div>
                          <?php foreach($categories as $category) { ?>
                              <?php if(in_array($category['category_id'], $newslettercategories)) { ?>
                                  <input class="css-checkbox" type="checkbox" checked="checked" name="newslettercategories[]" value="<?php echo $category['category_id']; ?>" /><?php echo $category['name']; ?><br />
                              <?php } else { ?>
                                  <input class="css-checkbox" type="checkbox" name="newslettercategories[]" value="<?php echo $category['category_id']; ?>" /><?php echo $category['name']; ?><br />
                              <?php } ?>
                          <?php } ?>
                      </div>
                      </td>
                  <?php } ?>
              </tr>
          </table>
      </div>
    <div class="buttons">
      <div class="left"><a href="<?php echo $back; ?>" class="button"><?php echo $button_back; ?></a></div>
      <div class="right"><input type="submit" value="<?php echo $button_continue; ?>" class="button" /></div>
    </div>
  </form>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>

<script type="text/javascript">
    $('#newsletter').on('change', function() {
        if (this.checked) {
            $('#newslettercategories').show();
        } else {
            $('#newslettercategories').hide();
        }
    });
</script>