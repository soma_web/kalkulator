<?php echo $header; ?>

<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
</div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>

    <h1><img src="catalog/view/theme/default/image/product.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div style="text-align:right; margin-bottom:10px;"><a href="<?php echo $back; ?>" class="button"><?php echo $button_back; ?></a></div>
    <div class="wishlist-info">

<?php if($new == 1) { ?>
    <div id="errorincode" class="success"><?php echo $text_biztonsagi_kod_elkuldve; ?></div>
<?php } ?>

<div id="security_code">
    <div><?php echo $text_biztonsagi_kod; ?></div>
    <div>
        <input type="text" name="security_code" />
        <button type="button" onclick="checkSecurityCode('<?php echo $productid ?>');"><?php echo $button_sending ?></button>
    </div>
    <div>
        <button type="button" onclick="requestNewSecurityCode('<?php echo $productid ?>');"><?php echo $text_biztonsagi_kod_igenyles ?></button>
    </div>
</div>

<script type="text/javascript">
    function requestNewSecurityCode(productid) {
        $('#errorincode').remove();
        $.ajax({
            type: "POST",
            url: 'index.php?route=account/product/requestsecuritycode',
            data: 'productid='+productid,
            dataType: 'json',
            success: function(json) {
                debugger;
                $('#errorincode').remove();
                var text_success = "<?php echo $text_biztonsagi_kod_elkuldve; ?>";
                var text_fail = "<?php echo $text_biztonsagi_kod_nincs_elkuldve; ?>";
                if(json instanceof Object && json['success'] == '1') {
                    var newdiv = '<div id="errorincode" class="success">'+text_success+'</div>';
                    $('#security_code').before(newdiv);
                } else {
                    var newdiv = '<div id="errorincode" class="warning">'+text_fail+'</div>';
                    $('#security_code').before(newdiv);
                }
            },
            beforeSend: function() {
                var text_success = "<?php echo $text_biztonsagi_kod_kuldese; ?>";
                var newdiv = '<div id="errorincode" class="success">'+text_success+'</div>';
                $('#security_code').before(newdiv);
            },
            error: function(e) {
                debugger;
            }
        });
    }

    function checkSecurityCode(productid) {
        var securitycode = $('[name=security_code]').val();
        $('#errorincode').remove();
        $.ajax({
            type: "POST",
            url: 'index.php?route=account/product/checksecuritycode',
            data:   'productid='+productid+
                '&securitycode='+securitycode,
            dataType: 'json',
            success: function(json) {
                debugger;
                var text_success = "<?php echo $text_sikeres_biztonsagi_kod; ?>";
                var text_fail = "<?php echo $text_sikertelen_biztonsagi_kod; ?>";
                if(json instanceof Object && json['success'] == '1') {
                    var newdiv = '<div id="errorincode" class="success">'+text_success+'</div>';
                    $('#security_code').before(newdiv);
                } else {
                    var newdiv = '<div id="errorincode" class="warning">'+text_fail+'</div>';
                    $('#security_code').before(newdiv);
                }
            },
            error: function(e) {
                debugger;
            }
        });
    }
</script>

<?php echo $footer; ?>