<?php echo $header; ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?>
    <img src="catalog/view/theme/default/image/close.png" alt="" class="close" />
</div>
<?php } ?>

<?php if ($error) { ?>
    <div class="warning"><?php echo $error; ?>
        <img src="catalog/view/theme/default/image/close.png" alt="" class="close" />
    </div>
<?php } ?>

<div class="breadcrumb">
    <?php $elemszam=count($breadcrumbs);
    $i=0;
    foreach ($breadcrumbs as $breadcrumb) { $i++;
        if ($i==$elemszam) { ?>
            <? echo $breadcrumb['separator']; ?><a class="utolsobread" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <? } else {
            echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <? }
    } ?>
</div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>

  <h1><?php echo $heading_title; ?></h1>
  <?php if ( $this->config->get('megjelenit_modul_frontend') == 1 && isset($feltolto) && $feltolto == 1) { ?>
      <h2><?php echo $text_myproduct; ?></h2>
      <div class="content">
          <ul>
              <?php if ($this->config->get('megjelenit_frontend_sajat_termek') ) { ?>
                <li><a href="<?php echo $myproduct; ?>"><?php echo $text_myproduct; ?></a></li>
              <?php } ?>

              <?php if ($this->config->get('megjelenit_frontend_termek_felvitel') ) { ?>
                  <li><a href="<?php echo $newproduct; ?>"><?php echo $text_newproduct; ?></a></li>
              <?php } ?>

              <?php if ($this->config->get('megjelenit_frontend_jelentesek') ) { ?>

                  <li><a  style="cursor: default; color: rgba(0, 0, 0, 0.45);"><?php echo $text_jelentesek; ?></a>
                      <ul>
                          <?php if ($this->config->get('megjelenit_frontend_ertesitesi_jelentes') ) { ?>
                              <li><a href="<?php echo $salereport; ?>"><?php echo $text_salereport; ?></a></li>
                          <?php } ?>

                          <?php if ($this->config->get('megjelenit_frontend_letöltések') ) { ?>
                              <li><a href="<?php echo $product_report_rendelesek; ?>"><?php echo $text_productreport_rendelesek; ?></a></li>
                          <?php } ?>

                          <?php if ($this->config->get('megjelenit_frontend_megnezett') ) { ?>
                              <li><a href="<?php echo $product_report_megnezett; ?>"><?php echo $text_productreport_megnezett; ?></a></li>
                          <?php } ?>

                          <?php if ($this->config->get('megjelenit_frontend_letoltott') ) { ?>
                              <li><a href="<?php echo $product_report_megvasarolt; ?>"><?php echo $text_productreport_megvasarolt; ?></a></li>
                          <?php } ?>

                          <?php if ($this->config->get('megjelenit_frontend_hatteradatok') ) { ?>
                              <li><a href="<?php echo $report_kategoria_hatteradatok; ?>"><?php echo $text_kategoria_hatteradatok; ?></a></li>
                          <?php } ?>

                      </ul>
                  </li>
              <?php } ?>

              <?php if ($this->config->get('megjelenit_frontend_utalvany') ) { ?>
                  <li><a href="<?php echo $utalvanyellenorzes; ?>"><?php echo $text_utalvanyellenorzes; ?></a></li>
              <?php } ?>

              <?php if ($this->config->get('megjelenit_frontend_arajanlat') ) { ?>
                  <li><a href="<?php echo $arajanlat; ?>"><?php echo $text_arajanlat; ?></a></li>
              <?php } ?>

          </ul>
      </div>
  <?php } ?>

  <h2><?php echo $text_my_account; ?></h2>
  <div class="content">
    <ul>
      <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
      <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
        <?php if( (isset($feltolto) && $feltolto == 1 && $ceges_cimmodositas_a_fiok_szerkeztesben != 1)
            || (isset($feltolto) && $feltolto == 0 && $cimmodositas_a_fiok_szerkeztesben != 1) || !isset($feltolto) && $cimmodositas_a_fiok_szerkeztesben != 1) { ?>
      <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
        <?php } ?>
      <!--<li><a href="<?php echo $edit; ?>"><?php echo $text_edit_paypal; ?></a></li>-->
      <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
    </ul>
  </div>



  <h2><?php echo $text_my_orders; ?></h2>
  <div class="content">
    <ul>
      <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
      <?php if ($this->config->get("megjelenit_vonalkod_kuldes") == 1) { ?>

            <li><a  style="cursor: default; color: rgba(0, 0, 0, 0.45);"><?php echo $text_download; ?></a>
            <ul>
              <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
              <li><a href="<?php echo $utalvanyok; ?>"><?php echo $text_download_utalvany; ?></a></li>
            </ul>
          </li>
        <?php } else { ?>
          <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
      <?php } ?>

      <?php if ($reward) { ?>
      <li><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></li>
      <?php } ?>
      <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
      <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
    </ul>
  </div>

  <h2><?php echo $text_my_newsletter; ?></h2>
  <div class="content">
    <ul>
      <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
    </ul>
  </div>
  <?php echo $content_bottom; ?>
  </div>
<?php echo $footer; ?> 