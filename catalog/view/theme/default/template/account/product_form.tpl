<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>

    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>

  <h1><img src="catalog/view/theme/default/image/product.png" alt="" /> <?php echo $heading_title; ?></h1>
  <div style="text-align:right; margin-bottom:10px;"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>&nbsp;
      <a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a>
  </div>
  <div class="wishlist-info">



  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">

      <?php $class_name = "frontend_nev";
      $class_description = "frontend_leiras";
      $language['language_id']= $this->config->get('config_language_id')?>

      <?php if ($mezok['elolnezet'] == 1) {?>
          <div class="elolnezet">
              <div class="box-product">
                  <?php
                  $module_name = "frontend";
                  $product['product_id'] = 0;
                  $product['thumb'] = $thumb;
                  $product['name'] = isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['name'] : '';
                  $product['description'] = isset($product_description[$language['language_id']]['description']) ? html_entity_decode($product_description[$language['language_id']]['description']) : "";
                  $product['date_ervenyes_ig'] = isset($date_ervenyes_ig) ? $date_ervenyes_ig : date('Y-m-d');
                  $product['price'] = $price ." Ft";
                  $product['special'] = "";
                  $product['utalvany'] = 1;
                  $product['price_netto'] = "";
                  $product['special_netto'] = "";
                  $product['tax'] = "";
                  $product['rating'] = "";
                  $product['reviews'] = "";
                  $product['href'] = "";
                  $product['uj'] = "";
                  $product['szazalek'] = $szazalek;
                  $product['pardarab'] = "";
                  $product['quantity'] = "";
                  $product['model'] = "";
                  $product['discounts'] = "";
                  $product['csomagolasi_egyseg'] = "";
                  $product['megyseg'] = "";
                  $product['csomagolasi_mennyiseg'] = "";
                  $product['special'] = "";
                  $product['href'] = "";
                  $product['imagedesabled'] = isset($imagedesabled) ? $imagedesabled : 0;
                  $elonezet = 1;
                  ?>

                  <?php if ($product['utalvany'] != 0) { ?>
                      <?php $class="szazalek";?>
                  <?php } else {?>
                      <?php $class="";?>
                  <?php } ?>
                  <div class="balra <?php echo " ".$class; ?>">
                      <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                          $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
                      } else {
                          $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
                      } ?>
                      <?php  include($arkiir); ?>
                  </div>

              </div>
          </div>
      <?php } ?>

      <?php if($nyelv) { ?>
          <div id="tab-general">

          <div id="languages" class="htabs">
              <?php foreach ($languages as $language) { ?>
                  <a href="#language<?php echo $language['language_id']; ?>">
                      <img style="width: 20px" src="image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?>
                  </a>
              <?php } ?>
          </div>

          <?php foreach ($languages as $language) { ?>
              <div id="language<?php echo $language['language_id']; ?>">
                  <?php
                    if ($language['language_id'] != $this->config->get('config_language_id')) {
                        $class_name='';
                    } ?>

                  <table class="form">
                      <tr>
                          <td><span class="required">*</span> <?php echo $mezok['megnevezes_neve']; ?></td>
                          <td><input class="<?php echo $class_name?>" type="text" name="product_description[<?php echo $language['language_id']; ?>][name]" size="100" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['name'] : ''; ?>" />
                              <?php if (isset($error_name[$language['language_id']])) { ?>
                                  <span class="error"><?php echo $error_name[$language['language_id']]; ?></span>
                              <?php } ?></td>
                      </tr>

                      <tr>
                          <td><?php echo $mezok['keyword_neve']; ?></td>
                          <td><textarea name="product_description[<?php echo $language['language_id']; ?>][meta_keyword]" cols="40" rows="5"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_keyword'] : ''; ?></textarea></td>
                      </tr>
                      <tr>
                          <td><?php echo $mezok['meta_description_neve']; ?></td>
                          <td><textarea name="product_description[<?php echo $language['language_id']; ?>][meta_description]" cols="40" rows="5"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_description'] : ''; ?></textarea></td>
                      </tr>
                      <tr>
                          <td><?php echo $mezok['description_neve']; ?></td>
                          <td><textarea class="<?php echo $class_description?>"  name="product_description[<?php echo $language['language_id']; ?>][description]" id="description<?php echo $language['language_id']; ?>"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['description'] : ''; ?></textarea></td>
                      </tr>
                      <tr>
                          <td><?php echo $mezok['tag_neve']; ?></td>
                          <td><input type="text" name="product_tag[<?php echo $language['language_id']; ?>]" value="<?php echo isset($product_tag[$language['language_id']]) ? $product_tag[$language['language_id']] : ''; ?>" size="80" /></td>
                      </tr>
                  </table>
              </div>
          <?php } ?>
      <?php } else {
          $language['language_id']= $this->config->get('config_language_id'); ?>

          <div id="language<?php echo $language['language_id']; ?>">
              <table class="form">
                  <?php if ($mezok['megnevezes'] == 1) {?>
                      <tr>
                          <td><span class="required">*</span> <?php echo $mezok['megnevezes_neve']; ?></td>
                          <td><input class="<?php echo $class_name?>" type="text" name="product_description[<?php echo $language['language_id']; ?>][name]" size="100" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['name'] : ''; ?>" />
                              <?php if (isset($error_name[$language['language_id']])) { ?>
                                  <span class="error"><?php echo $error_name[$language['language_id']]; ?></span>
                              <?php } ?>
                          </td>
                      </tr>
                  <?php } ?>

                  <?php if ($mezok['keyword'] == 1) {?>
                      <tr>
                          <td><?php echo $mezok['keyword_neve']; ?></td>
                          <td><textarea name="product_description[<?php echo $language['language_id']; ?>][meta_keyword]" cols="40" rows="5"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_keyword'] : ''; ?></textarea></td>
                      </tr>
                  <?php } ?>

                  <?php if ($mezok['meta_description'] == 1) {?>
                      <tr>
                          <td><?php echo $mezok['meta_description_neve']; ?></td>
                          <td><textarea name="product_description[<?php echo $language['language_id']; ?>][meta_description]" cols="40" rows="5"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_description'] : ''; ?></textarea></td>
                      </tr>
                  <?php } ?>

                  <?php if ($mezok['description'] == 1) {?>
                      <tr>
                          <td>
                              <?php if (array_key_exists('description_ellenorzes', $mezok) && $mezok['description_ellenorzes'] == 1 ) {?>
                                  <span class="required">*</span>
                              <?php } ?>
                              <?php echo $mezok['description_neve']; ?>
                          </td>
                          <td>
                              <textarea class="<?php echo $class_description?>"  name="product_description[<?php echo $language['language_id']; ?>][description]" id="description<?php echo $language['language_id']; ?>"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['description'] : ''; ?></textarea>
                              <?php if ($error_description) { ?>
                                  <span class="error"><?php echo $error_description; ?></span>
                              <?php } ?></td>
                          </td>
                      </tr>
                  <?php } ?>

                  <?php if ($mezok['tag'] == 1) {?>
                      <tr>
                          <td><?php echo $mezok['tag_neve']; ?></td>
                          <td><input type="text" name="product_tag[<?php echo $language['language_id']; ?>]" value="<?php echo isset($product_tag[$language['language_id']]) ? $product_tag[$language['language_id']] : ''; ?>" size="80" /></td>
                      </tr>
                  <?php } ?>

              </table>
          </div>
      <?php } ?>


      <table class="form">
          <?php if ($mezok['image'] == 1) {?>
              <tr>
                  <td><?php echo $mezok['image_neve']; ?></td>
                  <td>
                      <div class="image" style="text-align:left;">

                          <!--<a href="<?php echo $popup; ?>"  class="colorbox" rel="colorbox" id="kep_nezegeto">-->
                          <a href="<?php echo $popup; ?>"  class="fancy" rel="fancy-pics" data-fancybox-group="gallery-1" id="kep_nezegeto">
                              <img style="width: 100px" src="<?php echo $thumb; ?>" alt="" id="thumb" /><br />
                          </a>
                          <input type="hidden" name="image" value="<?php echo $image; ?>" id="image" />
                          <a onclick="image_upload('image', 'thumb');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="kepcsere('image', 'thumb')"><?php echo $text_clear; ?></a>
                      </div>
                  </td>


                  <?php if($megjelenit_termekadat['kep_megjelenites_kapcsolas']) { ?>
                       <td style="vertical-align: middle"><?php echo $entry_kep_tiltas; ?></td>
                       <td style="vertical-align: middle">
                            <?php if ($imagedesabled) { ?>
                                <input type="radio" id="image_engedve" name="imagedesabled" value="1" checked="checked" />
                                <?php echo $text_yes_kep; ?><br>
                                <input type="radio" id="image_tiltva" name="imagedesabled" value="0" />
                                <?php echo $text_no_kep; ?>
                            <?php } else { ?>
                                <input type="radio" id="image_engedve"  name="imagedesabled" value="1" />
                                <?php echo $text_yes_kep; ?><br>
                                <input type="radio" id="image_tiltva"  name="imagedesabled" value="0" checked="checked" />
                                <?php echo $text_no_kep; ?>
                            <?php } ?>
                       </td>
                  <?php } ?>
              </tr>
          <?php } ?>

          <?php if ($mezok['letoltheto'] == 1) {?>
              <tr>
                  <td>
                      <?php if ($mezok['letoltheto_ellenorzes'] == 1 ) {?>
                          <span class="required">*</span>
                      <?php } ?>
                      <?php echo $mezok['letoltheto_neve']; ?>
                  </td>
                  <td>
                      <div class="image" style="text-align:left;" id="kupon_nezegeto">

                          <a href="<?php echo $letoltheto_popup; ?>"  class="fancy" rel="fancy-pics" data-fancybox-group="gallery-1">
                          <!--<a href="<?php echo $letoltheto_popup; ?>"  class="colorbox" rel="colorbox" >-->
                              <img style="width: 100px" src="<?php echo $letoltheto_thumb; ?>" alt="" id="letoltheto_thumb" /><br />
                              <input type="hidden" name="letoltheto" value="<?php echo $letoltheto; ?>" id="letoltheto" />
                          </a>
                          <a onclick="image_upload('letoltheto', 'letoltheto_thumb');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="kepcsere('letoltheto', 'letoltheto_thumb')"><?php echo $text_clear; ?></a>
                      </div>
                      <?php if ($error_letoltheto) { ?>
                          <span class="error"><?php echo $error_letoltheto; ?></span>
                      <?php } ?>
                  </td>
                  <td style="vertical-align: middle; text-align: right;">
                      <a class="button" onclick="kuponKeszit()"><?php echo $text_kupon_keszit; ?></a>
                  </td>

              </tr>
          <?php } ?>

          <?php if ($mezok['model'] == 1) {?>
              <tr>
                <td> <!--<span class="required">*</span>--> <?php echo $mezok['model_neve']; ?></td>
                <td><input type="text" name="model" value="<?php echo $model; ?>" />
                  <?php if ($error_model) { ?>
                  <span class="error"><?php echo $error_model; ?></span>
                  <?php } ?></td>
              </tr>
          <?php } ?>



          <?php if ($mezok['location'] == 1) {?>
              <tr>
                <td><?php echo $mezok['location_neve']; ?></td>
                <td><input type="text" name="location" value="<?php echo $location; ?>" /></td>
              </tr>
          <?php } ?>

          <?php if ($mezok['price'] == 1) {?>
              <tr>
                <td>
                    <?php if ($mezok['price_ellenorzes'] == 1 ) {?>
                        <span class="required">*</span>
                    <?php } ?>
                    <?php echo $mezok['price_neve']; ?>
                </td>

                <td><input class="frontend_ar" type="text" name="price" value="<?php echo $price; ?>" />
                    <?php if ($mezok['price_ellenorzes'] == 1 ) {?>
                        <?php if (isset($error_price)) { ?>
                            <span class="error"><?php echo $error_price; ?></span>
                        <?php } ?>
                    <?php } ?>

                </td>

                    <?php if ($mezok['szazalek'] == 1 ) {?>
                        <td><?php echo $entry_szazalek; ?></td>
                        <td>
                            <?php if ($szazalek == 1) { ?>
                                <input type="radio" name="szazalek" value="1" checked="checked"  class="frontend_szazalek"/>
                                <?php echo $text_yes; ?>
                                <input type="radio" name="szazalek" value="0" class="frontend_forint"/>
                                <?php echo $text_no; ?>
                            <?php } else { ?>
                                <input type="radio" name="szazalek" value="1" class="frontend_szazalek"/>
                                <?php echo $text_yes; ?>
                                <input type="radio" name="szazalek" value="0" checked="checked" class="frontend_forint"/>
                                <?php echo $text_no; ?>
                            <?php } ?>
                        </td>
                    <?php } ?>


              </tr>
          <?php } ?>

          <?php if ($mezok['eredeti_ar'] == 1) {?>
              <tr>
                  <td><?php echo $mezok['eredeti_ar_neve']; ?></td>
                  <td><input class="frontend_ar" type="text" name="eredeti_ar" value="<?php echo $eredeti_ar; ?>" /></td>
              </tr>
          <?php } ?>


          <?php if ($mezok['date_ervenyes_ig'] == 1) {?>
              <tr>
                  <td>
                      <?php if ($mezok['date_ervenyes_ig_ellenorzes'] == 1 ) {?>
                          <span class="required">*</span>
                      <?php } ?>
                      <?php echo $mezok['date_ervenyes_ig_neve']; ?>
                  </td>
                  <td>
                      <input type="text" id="date_ervenyes_ig" name="date_ervenyes_ig" value="<?php echo $date_ervenyes_ig; ?>" size="12" class="date frondend_datum_ig" readonly="readonly"/>
                      <?php if ($mezok['date_ervenyes_ig_ellenorzes'] == 1 ) {?>
                          <?php if (isset($error_date_ervenyes_ig)) { ?>
                              <span class="error"><?php echo $error_date_ervenyes_ig; ?></span>
                          <?php } ?>
                      <?php } ?>
                  </td>

                  <?php if($config_lejarat_nelkuli) { ?>
                  <td><?php echo $config_lejarat_nelkuli_neve; ?>:</td>
                  <td><?php if (empty($date_ervenyes_ig) ) { ?>
                          <input type="radio" name="meddig_kaphato" value="1" checked="checked"  class="meddig_kaphato_mindig"/>
                          <?php echo $text_yes; ?>
                          <input type="radio" name="meddig_kaphato" value="0" class="meddig_kaphato_lejar"/>
                          <?php echo $text_no; ?>
                      <?php } else { ?>
                          <input type="radio" name="meddig_kaphato" value="1" class="meddig_kaphato_mindig"/>
                          <?php echo $text_yes; ?>
                          <input type="radio" name="meddig_kaphato" value="0" checked="checked" class="meddig_kaphato_lejar"/>
                          <?php echo $text_no; ?>
                      <?php } ?>
                  </td>
                  <?php } else { ?>
                      <td><?php if ($config_lejarat_nelkuli_default) { ?>
                              <input type="hidden" name="meddig_kaphato" value="1" />
                          <?php } else { ?>
                              <input type="hidden" name="meddig_kaphato" value="0" />
                          <?php } ?>
                      </td>
                  <?php } ?>


              </tr>
          <?php } ?>

          <?php if ($mezok['date_available'] == 1) {?>
              <tr>
                  <td><?php echo $mezok['date_available_neve']; ?></td>
                  <td><input type="text" name="date_available" value="<?php echo $date_available; ?>" size="12" class="date" /></td>
              </tr>
          <?php } ?>


          <?php if ($mezok['quantity'] == 1) {?>
              <tr>
                  <td><?php echo $mezok['quantity_neve']; ?></td>
                  <td><input type="text" name="quantity" value="<?php echo $quantity; ?>" size="2" /></td>
              </tr>
          <?php } ?>

          <?php if ($mezok['subtract'] == 1) {?>
              <tr>
                  <td><?php echo $mezok['subtract_neve']; ?></td>
                  <td><select name="subtract">
                          <?php if ($subtract) { ?>
                              <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                              <option value="0"><?php echo $text_no; ?></option>
                          <?php } else { ?>
                              <option value="1"><?php echo $text_yes; ?></option>
                              <option value="0" selected="selected"><?php echo $text_no; ?></option>
                          <?php } ?>
                      </select></td>
              </tr>
          <?php } ?>

          <?php if ($mezok['minimum'] == 1) {?>
              <tr>
                  <td><?php echo $mezok['minimum_neve']; ?></td>
                  <td><input type="text" name="minimum" value="<?php echo $minimum; ?>" size="2" /></td>
              </tr>
          <?php } ?>



          <?php if ($mezok['stock_status_id'] == 1) {?>
              <tr>
                <td><?php echo $mezok['stock_status_id_neve']; ?></td>
                <td><select name="stock_status_id">
                    <?php foreach ($stock_statuses as $stock_status) { ?>
                    <?php if ($stock_status['stock_status_id'] == $stock_status_id) { ?>
                    <option value="<?php echo $stock_status['stock_status_id']; ?>" selected="selected"><?php echo $stock_status['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $stock_status['stock_status_id']; ?>"><?php echo $stock_status['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select></td>
              </tr>
          <?php } ?>

          <?php if ($mezok['shipping'] == 1) {?>
              <tr>
                <td><?php echo $mezok['shipping_neve']; ?></td>
                <td><?php if ($shipping) { ?>
                  <input type="radio" name="shipping" value="1" checked="checked" />
                  <?php echo $text_yes; ?>
                  <input type="radio" name="shipping" value="0" />
                  <?php echo $text_no; ?>
                  <?php } else { ?>
                  <input type="radio" name="shipping" value="1" />
                  <?php echo $text_yes; ?>
                  <input type="radio" name="shipping" value="0" checked="checked" />
                  <?php echo $text_no; ?>
                  <?php } ?></td>
              </tr>
          <?php } ?>

          <?php if ($mezok['keyword'] == 1) {?>
              <tr>
                <td><?php echo $mezok['keyword_neve']; ?></td>
                <td><input type="text" name="keyword" value="<?php echo $keyword; ?>" /></td>
              </tr>
          <?php } ?>



          <?php if ($mezok['length'] == 1 || $mezok['width'] == 1 || $mezok['height'] == 1 ) {?>
              <tr>
                <td><?php echo $mezok['length_neve']; ?></td>
                <td>
                    <?php if ($mezok['length'] == 1) { ?>
                        <input type="text" name="length" value="<?php echo $length; ?>" size="4" />
                    <?php } ?>
                    <?php if ($mezok['width'] == 1) { ?>
                        <input type="text" name="width" value="<?php echo $width; ?>" size="4" />
                    <?php } ?>
                    <?php if ($mezok['height'] == 1) { ?>
                        <input type="text" name="height" value="<?php echo $height; ?>" size="4" />
                    <?php } ?>
                </td>
              </tr>
          <?php } ?>

          <?php if ($mezok['sku'] == 1) {?>
              <tr>
                  <td><?php echo $mezok['sku_neve']; ?></td>
                  <td><input type="text" name="sku" value="<?php echo $sku; ?>" /></td>
              </tr>
          <?php } ?>

          <?php if ($mezok['upc'] == 1) {?>
              <tr>
                  <td><?php echo $mezok['upc_neve']; ?></td>
                  <td><input type="text" name="upc" value="<?php echo $upc; ?>" /></td>
              </tr>
          <?php } ?>

          <?php if (isset($mezok['biztonsagi_kod']) && $mezok['biztonsagi_kod'] == 1) { ?>
              <?php if(isset($product_info) && array_key_exists('correctsecuritycode', $product_info) && $product_info['correctsecuritycode'] != 1) { ?>
                  <tr id="security_code">
                      <td>
                          <?php if($mezok['biztonsagi_kod_ellenorzes'] == 1) { ?>
                            <span class="required">*</span>
                          <?php } ?>
                          <?php echo $text_biztonsagi_kod; ?></td>
                      <td>
                          <input type="text" name="security_code" />
                          <button type="button" onclick="checkSecurityCode('<?php echo $productid ?>');"><?php echo $button_sending ?></button>
                      </td>
                      <td>
                          <button type="button" onclick="requestNewSecurityCode('<?php echo $productid ?>');"><?php echo $text_biztonsagi_kod_igenyles ?></button>
                      </td>
                  </tr>
              <?php } ?>
          <?php } ?>

          <?php if ($mezok['length_class_id'] == 1) {?>
              <tr>
                <td><?php echo $mezok['length_class_id_neve']; ?></td>
                <td><select name="length_class_id">
                    <?php foreach ($length_classes as $length_class) { ?>
                    <?php if ($length_class['length_class_id'] == $length_class_id) { ?>
                    <option value="<?php echo $length_class['length_class_id']; ?>" selected="selected"><?php echo $length_class['title']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $length_class['length_class_id']; ?>"><?php echo $length_class['title']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select></td>
              </tr>
          <?php } ?>

          <?php if ($mezok['weight'] == 1) {?>
              <tr>
                <td><?php echo $mezok['weight_neve']; ?></td>
                <td><input type="text" name="weight" value="<?php echo $weight; ?>" /></td>
              </tr>
              <?php } ?>
              <?php if ($mezok['location'] == 1) {?>
              <tr>
                <td><?php echo $entry_weight_class; ?></td>
                <td><select name="weight_class_id">
                    <?php foreach ($weight_classes as $weight_class) { ?>
                    <?php if ($weight_class['weight_class_id'] == $weight_class_id) { ?>
                    <option value="<?php echo $weight_class['weight_class_id']; ?>" selected="selected"><?php echo $weight_class['title']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $weight_class['weight_class_id']; ?>"><?php echo $weight_class['title']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select></td>
              </tr>
          <?php } ?>

          <?php if ($mezok['manufacturer_id'] == 1) {?>
              <tr>
                <td><?php echo $mezok['manufacturer_id_neve']; ?></td>
                <td><input type="text" name="manufacturer" value="<?php echo $manufacturer ?>" />
                  <input type="hidden" name="manufacturer_id" value="<?php echo $manufacturer_id; ?>" /></td>
              </tr>
          <?php } ?>

          <?php if ($mezok['category'] == 1) {?>
              <tr><td><br></td></tr>

              <tr>
                  <td>
                      <?php if (array_key_exists('category_ellenorzes', $mezok) && $mezok['category_ellenorzes'] == 1 ) {?>
                          <span class="required">*</span>
                      <?php } ?>
                      <?php echo $mezok['category_neve']; ?>
                  </td>
                  <td colspan=3><?php
                      $pro_selcate[]='';
                      foreach ($product_categories as $product_category) {
                          $pro_selcate[] = $product_category['category_id'];
                      }
                      ?>
                      <div class="scrollbox">
                          <?php $class = 'odd'; ?>
                          <?php foreach ($procategorys as $category) { ?>
                              <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                              <div class="<?php echo $class; ?>">
                                  <?php if (in_array($category['category_id'], $pro_selcate)) { ?>
                                      <input type="checkbox" name="product_category[]" value="<?php echo $category['category_id']; ?>" checked="checked" onclick="addAttribute();"/>
                                      <?php echo $category['name']; ?>
                                  <?php } else { ?>
                                      <input type="checkbox" name="product_category[]" value="<?php echo $category['category_id']; ?>" onclick="addAttribute();"/>
                                      <?php echo $category['name']; ?>
                                  <?php } ?>
                              </div>
                          <?php } ?>
                      </div>
                      <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a>
                      <?php if (array_key_exists('category_ellenorzes', $mezok) && $mezok['category_ellenorzes'] == 1 ) { ?>
                          <?php if (isset($error_category)) { ?>
                              <span class="error"><?php echo $error_category; ?></span>
                          <?php } ?>
                      <?php } ?>
                  </td>

                  <?php if (in_array(0, $product_store)) { ?>
                      <input type="hidden" name="product_store[]" id="product_store[]" value="0" />
                  <?php } ?>
                  </td>
              </tr>
              <tr><td><br></td></tr>

          <?php } ?>

          <?php if ($mezok['filter_csoport'] == 1) {?>
              <?php if ($mezok['filter_csoport_neve']) { ?>
                  <tr>
                      <td>
                          <?php if ($mezok['filter_csoport_ellenorzes'] == 1 ) {?>
                              <span class="required">*</span>
                          <?php } ?>
                          <?php echo $entry_filter; ?>
                      </td>

                      <td  colspan=3><div class="scrollbox">
                              <?php $class = 'odd'; ?>
                              <?php foreach ($szuro_csoport as $szuro) { ?>
                                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                  <div class="<?php echo $class; ?>">
                                      <?php if (in_array($szuro, $product_filters_group)) { ?>
                                          <input type="checkbox" name="product_filter[]" value="<?php echo $szuro['filter_id']; ?>" checked="checked" />
                                          <?php echo $szuro['name']; ?>
                                      <?php } else { ?>
                                          <input type="checkbox" name="product_filter[]" value="<?php echo $szuro['filter_id']; ?>" />
                                          <?php echo $szuro['name']; ?>
                                      <?php } ?>
                                  </div>
                              <?php } ?>
                          </div>
                          <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a>

                          <?php if ($mezok['filter_csoport_ellenorzes'] == 1 ) { ?>
                              <?php if (isset($error_product_filter)) { ?>
                                  <span class="error"><?php echo $error_product_filter; ?></span>
                              <?php } ?>
                          <?php } ?>

                        </td>

                  </tr>
                  <tr><td><br></td></tr>
              <?php } ?>
          <?php } ?>

          <?php if ($mezok['filter'] == 1) {?>
              <tr>
                  <td><?php echo $mezok['filter_neve']; ?></td>
                  <td  colspan=3><div class="scrollbox">
                          <?php $class = 'odd'; ?>
                          <?php foreach ($szurok as $szuro) { ?>
                              <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                              <div class="<?php echo $class; ?>">
                                  <?php if (in_array($szuro, $product_filters)) { ?>
                                      <input type="checkbox" name="product_filter[]" value="<?php echo $szuro['filter_id']; ?>" checked="checked" />
                                      <?php echo $szuro['name']; ?>
                                  <?php } else { ?>
                                      <input type="checkbox" name="product_filter[]" value="<?php echo $szuro['filter_id']; ?>" />
                                      <?php echo $szuro['name']; ?>
                                  <?php } ?>
                              </div>
                          <?php } ?>
                      </div>
                      <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a></td>
              </tr>
              <tr><td><br></td></tr>
          <?php } ?>
      </table>

      <?php if ($mezok['images'] == 1) {?>
		  <table id="images" class="list">
            <thead>
              <tr>
                <td class="left nyolcvanketto">
                    <?php echo $mezok['images_neve']; ?>
                </td>
                <td class="right tiz"><?php echo $entry_sort_order; ?></td>
                <td></td>
              </tr>
            </thead>
            <?php $image_row = 0; ?>
            <?php foreach ($product_images as $product_image) { ?>
            <tbody id="image-row<?php echo $image_row; ?>">
              <tr>
                  <td class="left nyolcvanketto">
                    <div class="image"><img src="<?php echo $product_image['thumb']; ?>" alt="" id="thumb<?php echo $image_row; ?>" />
                    <input type="hidden" name="product_image[<?php echo $image_row; ?>][image]" value="<?php echo $product_image['image']; ?>" id="image<?php echo $image_row; ?>" />
                    <br />
                    <a onclick="image_upload('image<?php echo $image_row; ?>', 'thumb<?php echo $image_row; ?>');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb<?php echo $image_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#image<?php echo $image_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
                <td class="right tiz vertikalis"><input type="text" name="product_image[<?php echo $image_row; ?>][sort_order]" value="<?php echo $product_image['sort_order']; ?>" size="2" /></td>
                <td class="left vertikalis"><a onclick="$('#image-row<?php echo $image_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
              </tr>
            </tbody>
            <?php $image_row++; ?>
            <?php } ?>
            <tfoot style="display: table; width: 100%;">
              <tr>
                <td colspan="2"></td>
                <td class="right"><a onclick="addImage();" class="button"><?php echo $button_add_image; ?></a></td>
              </tr>
            </tfoot>
          </table>
      <?php } ?>


      <?php if ($mezok['elolnezet'] == 1) { ?>
          <div class="elolnezet">
              <div class="box-product">

                  <?php $product['price'] = $price ." Ft";?>

                  <?php if ($product['utalvany'] != 0) { ?>
                      <?php $class="szazalek";?>
                  <?php } else {?>
                      <?php $class="";?>
                  <?php } ?>
                  <div class="balra <?php echo " ".$class; ?>">
                      <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                          $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
                      } else {
                          $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
                      } ?>
                      <?php  include($arkiir); ?>
                  </div>

              </div>
          </div>
      <?php } ?>


          </div>
    </form>
  </div>
  <div style="text-align:right; margin-top:10px;"><a onclick="$('#form').submit();;" class="button"><?php echo $button_save; ?></a>&nbsp;<a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
  <?php echo $content_bottom; ?></div>

<script type="text/javascript" src="catalog/view/javascript/ckeditor/ckeditor.js"></script>

<script type="text/javascript"><!--

<?php foreach ($languages as $language) { ?>
    CKEDITOR.replace('description<?php echo $language['language_id']; ?>', {
        filebrowserBrowseUrl: 'index.php?route=account/filemanager',
        filebrowserImageBrowseUrl: 'index.php?route=account/filemanager',
        filebrowserFlashBrowseUrl: 'index.php?route=account/filemanager',
        filebrowserUploadUrl: 'index.php?route=account/filemanager',
        filebrowserImageUploadUrl: 'index.php?route=account/filemanager',
        filebrowserFlashUploadUrl: 'index.php?route=account/filemanager'
    });
<?php } ?>

//--></script>
<script type="text/javascript"><!--

$.widget('custom.catcomplete', $.ui.autocomplete, {
	_renderMenu: function(ul, items) {
		var self = this, currentCategory = '';
		$.each(items, function(index, item) {
			if (item.category != currentCategory) {
				ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
				currentCategory = item.category;
			}
			self._renderItem(ul, item);
		});
	}
});
// Manufacturer

$('input[name=\'manufacturer\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=account/manufacturer/autocomplete&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.manufacturer_id
					}
				}));
			}
		});
	},
	select: function(event, ui) {
		$('input[name=\'manufacturer\']').attr('value', ui.item.label);
		$('input[name=\'manufacturer_id\']').attr('value', ui.item.value);
		return false;
	},
	focus: function(event, ui) {
      return false;
   }

});

// Category

$('input[name=\'category\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=account/category/autocomplete&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.category_id
					}
				}));
			}
		});
	},
	select: function(event, ui) {
		$('#product-category' + ui.item.value).remove();
		$('#product-category').append('<div id="product-category' + ui.item.value + '">' + ui.item.label + '<img src="catalog/view/theme/default/image/delete.png" alt="" /><input type="hidden" name="product_category[]" value="' + ui.item.value + '" /></div>');
		$('#product-category div:odd').attr('class', 'odd');
		$('#product-category div:even').attr('class', 'even');
		return false;
	},
	focus: function(event, ui) {
      return false;

   }
});

$('#product-category div img').on('click', function() {
	$(this).parent().remove();
	$('#product-category div:odd').attr('class', 'odd');
	$('#product-category div:even').attr('class', 'even');
});

//--></script>
<script type="text/javascript"><!--

function image_upload(field, thumb) {
	$('#dialog').remove();
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=account/filemanager&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {

				$.ajax({
					url: 'index.php?route=account/filemanager/image&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(text) {
                        var kep_letiltva = $("input[name='imagedesabled']").filter("[checked=checked]").val();
                        $('#temp').remove();
						$('#' + thumb).replaceWith('<img style="width: 100px" src="' + text + '" alt="" id="' + thumb + '" />');
                        $('.elolnezet .image').css("display","block");
                        $('.elolnezet .image').html('<img src="' + $('#thumb').attr('src') + '" class="fektetett_tpl_kep"  style="display:block"/>');

                        if (kep_letiltva == 1) {
                            $('.elolnezet .image img').css("display","none");
                            $('.elolnezet').css('height','<?php echo $this->config->get('config_image_product_height');?>px');

                            $('.elolnezet .jobboldal').addClass("no_kep_nelkul");
                            $('.elolnezet .szoveg .description').addClass("max_width");

                        } else {
                            if ($('.elolnezet .jobboldal').hasClass("no_kep_nelkul") ) {
                                $('.elolnezet .jobboldal').removeClass("no_kep_nelkul");
                            }
                            if ($('.elolnezet .jobboldal').hasClass("max_width") ) {
                                $('.elolnezet .szoveg .description').removeClass("max_width");
                            }
                            $(".elolnezet .review").css("display","none");
                            $(".elolnezet .review").fadeIn(200);
                        }

                    }

				});
			}
		},
		bgiframe: false,
		width: 800,
		height: 400,
		resizable: false,
		modal: false
	});

};

//--></script>
<script type="text/javascript"><!--
var image_row = <?php echo $image_row; ?>;

    function addImage() {
        html  = '<tbody id="image-row' + image_row + '">';
        html += '  <tr>';
        html += '    <td class="left nyolcvanketto"><div class="image"><img src="<?php echo $no_image; ?>" alt="" id="thumb' + image_row + '" /><input type="hidden" name="product_image[' + image_row + '][image]" value="" id="image' + image_row + '" /><br /><a onclick="image_upload(\'image' + image_row + '\', \'thumb' + image_row + '\');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$(\'#thumb' + image_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image' + image_row + '\').attr(\'value\', \'\');"><?php echo $text_clear; ?></a></div></td>';
        html += '    <td class="right tiz vertikalis"><input type="text" name="product_image[' + image_row + '][sort_order]" value="" size="2" /></td>';
        html += '    <td class="left vertikalis"><a onclick="$(\'#image-row' + image_row  + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
        html += '  </tr>';
        html += '</tbody>';

        $('#images tfoot').before(html);

        image_row++;
    }
    //--></script>
<script>

    function kuponKeszit() {
        var nev = "product_description[<?php echo $this->config->get('config_language_id'); ?>][name]";
        var szoveg = CKEDITOR.instances.description2.getData();

        var kep_letiltva = $("input[name='imagedesabled']").filter("[checked=checked]").val();

        var a = jQuery('[name="product_filter[]"]').filter('[checked=checked]');

        var a = jQuery('[name="product_filter[]"]').filter('[checked=checked]');
        var kep =  $('#thumb').attr("src");
        var filter = new Array();
        for (i=0; i< a.length; i++) {
            filter[i] = a[i].value;
        }
        $.ajax({
            type: "POST",
            url: 'index.php?route=account/kuponfile',
            data: 'name=' + encodeURIComponent($('input[name="'+nev+'"]').val())
                + '&szoveg='            + encodeURIComponent(szoveg)
                + '&image='             + kep
                + '&price='             + encodeURIComponent($('input[name="price"]').val())
                + '&szazalek='          + encodeURIComponent($('input[name="szazalek"]').val())
                + '&date_ervenyes_ig='  + encodeURIComponent($('input[name="date_ervenyes_ig"]').val())
                + '&upc='               + encodeURIComponent($('input[name="upc"]').val())
                + '&kep_letiltva='      + kep_letiltva
                + '&filters='           + filter,
            dataType: 'json',
            success: function(json) {


                html =  '<a href="'+json['utvonal_cache'] + '" class="fancy" rel="fancy-pics" data-fancybox-group="gallery-1">';
                //html =  '<a href="'+json['utvonal_cache'] + '" class="colorbox" rel="colorbox" >';
                html += '<img style="width: 100px" src="' + json['utvonal_mentes'] + '" alt="" id="letoltheto_thumb" /><br/>';
                html += '<input type="hidden" name="letoltheto" value="' + json['utvonal_mentes2'] + '" id="letoltheto" />';
                html += '</a>';
                html += '<a onclick="image_upload(\'letoltheto\', \'letoltheto_thumb\');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="kepcsere(\'letoltheto\', \'letoltheto_thumb\')"><?php echo $text_clear; ?></a>';

                $("#kupon_nezegeto").html(html);
                /*$('.colorbox').colorbox({
                    overlayClose: true,
                    opacity: 0.5
                });*/
            },
            error: function(e){
            }

        });
    }
    </script>



<script type="text/javascript">

    function kepcsere(image, thumb) {

        $('#'+thumb).attr('src', '<?php echo $no_image; ?>');
        $('#'+image).attr('value', '');

        if (image == "image") {
            <?php if ($megjelenit_altalanos['no_kep_tiltas'] != 1) { ?>
            $('.fektetett_tpl_kep').before('<img src="' + $('#thumb').attr('src') + '" id="temp" class="temp_elonezet" style="position: absolute; z-index: 10000; " />');
            $('#temp').attr('src', '<?php echo $no_image_product; ?>');
            $('.temp_elonezet').attr('src', '<?php echo $no_image_product; ?>');
        <?php } else { ?>
            $('.elolnezet .image').html('<img src="" class="fektetett_tpl_kep"  />');
            $('.elolnezet .image').css("display","block");
            $('.elolnezet , .elolnezet ').css('height','<?php echo $this->config->get('config_image_product_height');?>px');
            $('.elolnezet .jobboldal').addClass("no_kep_nelkul");

            //$('.elolnezet .name, .elolnezet .szoveg, .elolnezet .review ').css("padding-left","10px");
            //$(' .description,  .description').css("max-width","380px");
            $('.elolnezet .description').css("max-width","380px");

            $(".elolnezet .review").css("display","none");
            $(".elolnezet .review").fadeIn(200);
        <?php } ?>

        }
    }

    /*$('.colorbox').colorbox({
        overlayClose: true,
        opacity: 0.5
    });*/

</script>

    <script type="text/javascript"><!--
        $('#tabs a').tabs();
        $('#languages a').tabs();
        $('#vtab-option a').tabs();

        CKEDITOR.instances.description2.on("key",function() {
            var ck_szoveg = CKEDITOR.instances.description2.getData()
            $(".elolnezet .box-product  .szoveg .description").html(ck_szoveg);
        });

        CKEDITOR.instances.description2.on("blur",function() {
            var ck_szoveg = CKEDITOR.instances.description2.getData()
            $(".elolnezet .box-product  .szoveg .description").html(ck_szoveg);
        });

        $(".frontend_nev").keyup(function(){
            $(".box-product  .name").html($(".frontend_nev").val());
        });


        $(".frontend_nev").focusout(function(){
            $(".box-product  .name").html($(".frontend_nev").val());
        });

        $(".frontend_ar").keyup(function(){
            <?php if($this->config->get('megjelenit_negativ_ar') == 1){ ?>
            if ( $(".frontend_forint").prop("checked") ) {
                $(".box-product  .price-new, .box-product  .price-new-szazalek").html("-"+$(".frontend_ar").val()+" Ft");
                var meret = 80/($(".frontend_ar").val().length+3);
                $(".box-product  .price-new, .box-product  .price-new-szazalek").css("font-size",meret+"pt");

            } else {
                $(".box-product  .price-new, .box-product  .price-new-szazalek").html("-"+$(".frontend_ar").val()+" %");
                $(".box-product  .price-new, .box-product  .price-new-szazalek").css("font-size","18pt");

            }
            <?php } else { ?>
            $(".box-product  .price-new, .box-product  .price-new-szazalek").html($(".frontend_ar").val()+" Ft");
            <?php } ?>
        });

        $(".frondend_datum_ig").keyup(function(){
            $(".box-product .date_ervenyes_ig span").html($(".frondend_datum_ig").val());
        });


        $(".frontend_forint").bind("click",function(){
            <?php if($this->config->get('megjelenit_negativ_ar') == 1){ ?>
            $(".box-product  .price-new-szazalek").html("-"+$(".frontend_ar").val()+" Ft");
            <?php } else { ?>
            $(".box-product  .price-new-szazalek").html($(".frontend_ar").val()+" Ft");
            <?php } ?>
        });

        $(".frontend_szazalek").bind("click",function(){
            <?php if($this->config->get('megjelenit_negativ_ar') == 1){ ?>
            $(".box-product  .price-new-szazalek").html("-"+$(".frontend_ar").val()+"%");
            <?php } else { ?>
            $(".box-product  .price-new-szazalek").html($(".frontend_ar").val()+"%");
            <?php } ?>
        });

        $("input[name='imagedesabled']").bind("click",function(){

            $("input[name='imagedesabled']").attr("checked",false);
            $(this).attr("checked",true);

            var kepneve = $('#thumb').attr('src');
            var no_kep_pos = kepneve.indexOf("no_image");
            var imagedisabled =  this.value;


            if (imagedisabled == 1) {     //1. megjelenés letiltva
                $('.elolnezet .image img').css("display","none");
                $('.elolnezet , .elolnezet ').css('height','<?php echo $this->config->get('config_image_product_height');?>px');

                $('.elolnezet .jobboldal').addClass("no_kep_nelkul");
                $('.elolnezet .szoveg .description').addClass("max_width");

            } else if (imagedisabled == 0 && no_kep_pos == -1 ) {      //2. megjelenés engedélyezve - van kép
                $('.elolnezet .image img').css("display","block");

                $('.elolnezet .jobboldal').removeClass("no_kep_nelkul");
                $('.elolnezet .szoveg .description').removeClass("max_width");

            } else if (imagedisabled == 0 && no_kep_pos > -1 ) {      //2. megjelenés engedélyezve - no-image
                <?php if ($megjelenit_altalanos['no_kep_tiltas'] != 1) { ?>
                    $('.elolnezet .image img').css("display","block");
                    $('.elolnezet .jobboldal').removeClass("no_kep_nelkul");
                    $('.elolnezet .szoveg .description').removeClass("max_width");

                <?php } else {?>
                    $('.elolnezet .image img').css("display","none");
                    $('.elolnezet , .elolnezet ').css('height','<?php echo $this->config->get('config_image_product_height');?>px');
                    $('.elolnezet .jobboldal').addClass("no_kep_nelkul");
                    $('.elolnezet .szoveg .description').addClass("max_width");
                <?php } ?>
            }

            $(".elolnezet .review").css("display","none");
            $(".elolnezet .review").fadeIn(200);



                //1. megjelenés letiltva
            //2. megjelenés engedélyezve
            //  - van kép
            //  - no-image
            //      - no-image engedélyezve
            //      - no-image letiltva


        });

        $(".meddig_kaphato_lejar").bind("click",function(){
            $("#date_ervenyes_ig").focus();
        });

        $(".meddig_kaphato_mindig").bind("click",function(){
            $("#date_ervenyes_ig").attr("value","");
            $(".elolnezet .box-product .date_ervenyes_ig span").html("");
            $(".elolnezet .box-product .date_ervenyes_ig").slideUp("slow");

        });


        //--></script>


<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--

    $('.date').datepicker({
        dateFormat: 'yy-mm-dd',
        onSelect: function(text,valami){
            $(".elolnezet .box-product .date_ervenyes_ig").slideDown("slow");

            $(".box-product .date_ervenyes_ig span").html(text);
            $('.meddig_kaphato_lejar').filter('[value="0"]').attr('checked', true);

        }
    });

    $('.datetime').datetimepicker({
        dateFormat: 'yy-mm-dd',
        timeFormat: 'h:m'
    });
    $('.time').timepicker({timeFormat: 'h:m'});
    //--></script>

<script type="text/javascript">
    function requestNewSecurityCode(productid) {
        $.ajax({
            type: "POST",
            url: 'index.php?route=account/product/requestsecuritycode',
            data: 'productid='+productid,
            dataType: 'json',
            success: function(json) {
                var text_success = "<?php echo $text_biztonsagi_kod_elkuldve; ?>";
                var text_fail = "<?php echo $text_biztonsagi_kod_nincs_elkuldve; ?>";
                if(json instanceof Object && json['success'] == '1') {
                    var newdiv = '<tr id="errorincode"><td></td><td colspan=2><div class="success">'+text_success+'</div></td></tr>';
                    $('#security_code').before(newdiv);
                } else {
                    var newdiv = '<tr id="errorincode"><td></td><td colspan=2><div class="warning">'+text_fail+'</div></td></tr>';
                    $('#security_code').before(newdiv);
                }
            },
            error: function(e) {
            }
        });
    }

    function checkSecurityCode(productid) {
        var securitycode = $('[name=security_code]').val();
        $('#errorincode').remove();
        $.ajax({
            type: "POST",
            url: 'index.php?route=account/product/checksecuritycode',
            data:   'productid='+productid+
                    '&securitycode='+securitycode,
            dataType: 'json',
            success: function(json) {
                var text_success = "<?php echo $text_sikeres_biztonsagi_kod; ?>";
                var text_fail = "<?php echo $text_sikertelen_biztonsagi_kod; ?>";
                if(json instanceof Object && json['success'] == '1') {
                    var newdiv = '<tr id="errorincode"><td></td><td colspan=2><div class="success">'+text_success+'</div></td></tr>';
                    $('#security_code').before(newdiv);
                } else {
                    var newdiv = '<tr id="errorincode"><td></td><td colspan=2><div class="warning">'+text_fail+'</div></td></tr>';
                    $('#security_code').before(newdiv);
                }
            },
            error: function(e) {
            }
        });
    }

</script>

<?php echo $footer; ?>