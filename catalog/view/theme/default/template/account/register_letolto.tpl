<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
    <div class="breadcrumb">
        <?php $elemszam=count($breadcrumbs);
        $i=0;
        foreach ($breadcrumbs as $breadcrumb) { $i++;
            if ($i==$elemszam) { ?>
                <? echo $breadcrumb['separator']; ?><a class="utolsobread" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? } else {
                echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <? }
        } ?>
    </div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>

  <h1><?php echo $heading_title; ?></h1>
  <p><?php echo $text_account_already; ?></p>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

    <h2><?php echo $text_your_details; ?></h2>
    <div class="content">
      <table class="form">
        <tr>
          <td><span class="required">*</span> <?php echo $entry_firstname; ?></td>
          <td><input type="text" name="firstname" value="<?php echo $firstname; ?>" />
            <?php if ($error_firstname) { ?>
            <span class="error"><?php echo $error_firstname; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_lastname; ?></td>
          <td><input type="text" name="lastname" value="<?php echo $lastname; ?>" />
            <?php if ($error_lastname) { ?>
            <span class="error"><?php echo $error_lastname; ?></span>
            <?php } ?>
          </td>
        </tr>

        <tr>
          <td><span class="required">*</span> <?php echo $entry_email; ?></td>
          <td><input type="text" name="email" value="<?php echo $email; ?>" />
            <?php if ($error_email) { ?>
            <span class="error"><?php echo $error_email; ?></span>
            <?php } ?>
          </td>
        </tr>


        <tr>
          <td><span class="required">*</span> <?php echo $entry_telephone; ?></td>
          <td><input type="text" name="telephone" value="<?php echo $telephone; ?>" />
            <?php if ($error_telephone) { ?>
            <span class="error"><?php echo $error_telephone; ?></span>
            <?php } ?>
          </td>
        </tr>
        <input type="hidden" name="fax" value="<?php echo $fax; ?>" />

          <?php if ($this->config->get('megjelenit_eletkor_regisztracio') == 1 ) {?>
              <tr>
                  <td><span class="required">*</span> <?php echo $entry_eletkor; ?></td>
                  <td><select name="eletkor" >
                          <option value=""><?php echo $text_select; ?></option>
                          <?php foreach ($eletkors as $eletkora) { ?>
                              <?php if ($eletkora['megnevezes'] == $eletkor) { ?>
                                  <option value="<?php echo $eletkora['megnevezes']; ?>" selected="selected"><?php echo $eletkora['megnevezes']; ?></option>
                              <?php } else { ?>
                                  <option value="<?php echo $eletkora['megnevezes']; ?>"><?php echo $eletkora['megnevezes']; ?></option>
                              <?php } ?>
                          <?php } ?>
                      </select>
                      <?php if ($error_eletkor) { ?>
                          <span class="error"><?php echo $error_eletkor; ?></span>
                      <?php } ?>
                  </td>
              </tr>
          <?php } ?>

          <?php if ($this->config->get('megjelenit_nem_regisztracio') == 1 ) {?>
              <tr>
                  <td><span class="required">*</span> <?php echo $entry_nem; ?></td>
                  <td><select name="nem" >
                          <option value=""><?php echo $text_select; ?></option>
                          <?php foreach ($nems as $value) { ?>
                              <?php if ($value['ertek'] == $nem) { ?>
                                  <option value="<?php echo $value['ertek']; ?>" selected="selected"><?php echo $value['nev']; ?></option>
                              <?php } else { ?>
                                  <option value="<?php echo $value['ertek']; ?>"><?php echo $value['nev']; ?></option>
                              <?php } ?>
                          <?php } ?>
                      </select>
                      <?php if ($error_nem) { ?>
                          <span class="error"><?php echo $error_nem; ?></span>
                      <?php } ?>
                  </td>
              </tr>
          <?php } ?>


      </table>
    </div>
    <h2><?php echo $text_your_address; ?> </h2>
    <div class="content">
      <table class="form">
        <input type="hidden" name="company" value="<?php echo $company; ?>" />
        <input type="hidden" name="adoszam" value="<?php echo $adoszam; ?>" />
        <?php if($this->config->get('megjelenit_regisztracio_utca') !== "0" ) { ?>
          <tr>
          <td><span class="required">*</span> <?php echo $entry_address_1; ?></td>
          <td><input type="text" name="address_1" value="<?php echo $address_1; ?>" />
            <?php if ($error_address_1) { ?>
            <span class="error"><?php echo $error_address_1; ?></span>
            <?php } ?></td>
        </tr>

        <input type="hidden" name="address_2" value="<?php echo $address_2; ?>" />
        <?php } ?>
        <?php if($this->config->get('megjelenit_regisztracio_varos') !== "0") { ?>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_city; ?></td>
          <td><input type="text" name="city" value="<?php echo $city; ?>" />
            <?php if ($error_city) { ?>
            <span class="error"><?php echo $error_city; ?></span>
            <?php } ?></td>
        </tr>
        <?php } ?>
        <?php if($this->config->get('megjelenit_regisztracio_iranyitoszam') !== "0") { ?>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_postcode; ?></td>
          <td><input type="text" name="postcode" value="<?php echo $postcode; ?>" />
            <?php if ($error_postcode) { ?>
            <span class="error"><?php echo $error_postcode; ?></span>
            <?php } ?></td>
        </tr>
        <?php } ?>
        <?php if($this->config->get('megjelenit_regisztracio_orszag') !== "0") { ?>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_country; ?></td>
          <td><select name="country_id" onchange="$('select[name=\'zone_id\']').load('index.php?route=account/register/zone&country_id=' + this.value + '&zone_id=<?php echo $zone_id; ?>');">
              <option value=""><?php echo $text_select; ?></option>
              <?php foreach ($countries as $country) { ?>
              <?php if ($country['country_id'] == $country_id) { ?>
              <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
            <?php if ($error_country) { ?>
            <span class="error"><?php echo $error_country; ?></span>
            <?php } ?></td>
        </tr>
        <?php } ?>
        <?php if($this->config->get('megjelenit_regisztracio_megye') !== "0") { ?>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_zone; ?></td>
          <td><select name="zone_id">
            </select>
            <?php if ($error_zone) { ?>
            <span class="error"><?php echo $error_zone; ?></span>
            <?php } ?></td>
        </tr>
        <?php } ?>
      </table>
    </div>
    <h2><?php echo $text_your_password; ?></h2>
    <div class="content">
      <table class="form">
        <tr>
          <td><span class="required">*</span> <?php echo $entry_password; ?></td>
          <td><input type="password" name="password" value="<?php echo $password; ?>" />
            <?php if ($error_password) { ?>
            <span class="error"><?php echo $error_password; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_confirm; ?></td>
          <td><input type="password" name="confirm" value="<?php echo $confirm; ?>" />
            <?php if ($error_confirm) { ?>
            <span class="error"><?php echo $error_confirm; ?></span>
            <?php } ?></td>
        </tr>
      </table>
    </div>

    <input type="hidden" name="paypalemail" value="<?php echo $paypalemail; ?>" />
    <input type="hidden" name="email2_no"   value="1" />
    <input type="hidden" name="feltolto"   value="0" />

    <h2><?php echo $text_newsletter; ?></h2>
    <div class="content">
      <table class="form">
        <tr>
          <td><?php echo $entry_newsletter; ?></td>
          <td><?php if ($newsletter == 1) { ?>
            <input type="radio" name="newsletter" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="newsletter" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="newsletter" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="newsletter" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
      </table>
    </div>
    <?php if ($text_agree) { ?>
    <div class="buttons">
      <div class="right"><?php echo $text_agree; ?>
        <?php if ($agree) { ?>
        <input type="checkbox"  class="agree" name="agree" value="1" checked="checked" />
        <?php } else { ?>
        <input type="checkbox"  class="agree" name="agree" value="1" />
        <?php } ?>
        <input type="submit" value="<?php echo $button_continue; ?>" class="button" />
      </div>
    </div>
    <?php } else { ?>
    <div class="buttons">
      <div class="right">
        <input type="submit" value="<?php echo $button_continue; ?>" class="button" />
      </div>
    </div>
    <?php } ?>
  </form>
  <?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
$('select[name=\'zone_id\']').load('index.php?route=account/register/zone&country_id=<?php echo $country_id; ?>&zone_id=<?php echo $zone_id; ?>');
//--></script> 
<script type="text/javascript"><!--
/*$('.colorbox').colorbox({
	width: 560,
	height: 560
});*/
//--></script> 
<?php echo $footer; ?>