<?php $modularis = $this->config->get('racs_beallitas_racs');  ?>
<?php $modularis = $modularis['modularis']  ?>

<?php if (!$modularis) {  ?>
    <?php if ($this->config->get('megjelenit_modularis') != 1) {  ?>

        <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/sale.png')) {
            $sale = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/sale.png';
        } else {
            $sale = DIR_TEMPLATE_IMAGE.'default/image/sale.png';
        }  ?>

        <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/ujtermek.png')) {
            $ujtermek = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/ujtermek.png';
        } else {
            $ujtermek = DIR_TEMPLATE_IMAGE.'default/image/ujtermek.png';
        }  ?>

        <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/pardarab.png')) {
            $pardarab = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/pardarab.png';
        } else {
            $pardarab = DIR_TEMPLATE_IMAGE.'default/image/pardarab.png';
        }  ?>


        <?php if ( isset($product["szazalek"]) && $product["szazalek"]== 1 ) {
            $szazalek_class = "szazalek_price";
        } else {
            $szazalek_class = "";
        }  ?>
        <?php $megjelenit_altalanos = $this->config->get('megjelenit_altalanos');  ?>
        <?php $megjelenit_admin_termekadatok = $this->config->get('megjelenit_admin_termekadatok');  ?>

        <?php if ( $this->config->get('megjelenit_allitott') == 1 || (isset($kapcsolodo_allitott) && $kapcsolodo_allitott == 1 ) ){  ?>
            <?php if($this->config->get('megjelenit_kep_szoveg') == 1){  ?>

                <?php
                $fektetve="";
                if($this->config->get('megjelenit_ikon_hely') == 0) {
                    $fektetve="ikon_vizszintes";

                } elseif($this->config->get('megjelenit_ikon_hely') == 2) {
                    $fektetve="ikon_vizszintes";

                }  else if($this->config->get('megjelenit_ikon_hely') == 3) {
                    $fektetve="ikon_kep_mellett";
                }
                 ?>

                <div class="ikonok <?php echo $fektetve;  ?>">
                    <?php if ($product['special']) {  ?>
                        <?php if($this->config->get('megjelenit_szazalek') == 1){  ?>
                            <img class="icon_meret" title="<? if (isset($text_akcio_title)) { echo $text_akcio_title; }  ?>"  src="<? echo $sale ?>" >
                        <?php }  ?>
                    <?}  ?>
                    <?php if ($product['pardarab']) {  ?>
                        <?php if($this->config->get('megjelenit_pardarab') == 1){  ?>
                            <img class="icon_meret" title="<? if (isset($text_pardarab_title)) { echo $text_pardarab_title; }  ?>" src="<? echo $pardarab ?>" >
                        <?}  ?>
                    <?}  ?>
                    <?php if ($product['uj']) {  ?>
                        <?php if($this->config->get('megjelenit_uj') == 1){  ?>
                            <img class="icon_meret" title="<? if (isset($text_uj_title)) { echo $text_uj_title; }  ?>"  src="<? echo $ujtermek ?>" >
                        <?}  ?>
                    <?}  ?>
                </div>

                <?php if ($product['thumb']) {  ?>
                    <div class="image">
                        <?php if(!empty($product['href'])) {  ?>
                            <a href="<?php echo $product['href'];  ?>">
                        <?php }  ?>
                            <img id="<?php echo $module_name.$product['product_id']   ?>" src="<?php echo $product['thumb'];  ?>" alt="<?php echo $product['name'];  ?>" />
                        <?php if(!empty($product['href'])) {  ?>
                            </a>
                        <?php }  ?>
                    </div>
                <?php }  ?>
                <div class="name"><a href="<?php echo $product['href'];  ?>"><?php echo $product['name'];  ?></a></div>
                <?php if($this->config->get('megjelenit_description') == 1){  ?>
                    <div class="description">
                        <?php echo mb_substr($product['description'], 0, $leiras_karakter_racs, "UTF-8");
                        if(!empty($product['description']) && strlen($product['description']) > $leiras_karakter_racs ){
                            echo "...";
                        }  ?>
                    </div>
                <?php }  ?>
            <?php } else {  ?>
                <?php
                $fektetve="";
                if($this->config->get('megjelenit_ikon_hely') == 0) {
                    $fektetve="ikon_vizszintes_szoveg_folott";

                } elseif($this->config->get('megjelenit_ikon_hely') == 2) {
                    $fektetve="ikon_vizszintes";

                }  else if($this->config->get('megjelenit_ikon_hely') == 3) {
                    $fektetve="ikon_kep_mellett";
                }
                 ?>

                <?php if($this->config->get('megjelenit_ikon_hely') == 0 || $this->config->get('megjelenit_ikon_hely') == 1) {  ?>
                    <div class="ikonok <?php echo $fektetve ?>">
                        <?php if ($product['special']) {  ?>
                            <?php if($this->config->get('megjelenit_szazalek') == 1){  ?>
                                <img class="icon_meret" title="<? if (isset($text_akcio_title)) { echo $text_akcio_title; }  ?>" src="<? echo $sale;  ?>" >
                            <?php }  ?>
                        <?}  ?>

                        <?php if ($product['pardarab']) {  ?>
                            <?php if($this->config->get('megjelenit_pardarab') == 1){  ?>
                                <img class="icon_meret" title="<? if (isset($text_pardarab_title)) { echo $text_pardarab_title; }  ?>" src="<? echo $pardarab  ?>" >
                            <?}  ?>
                        <?}  ?>

                        <?php if ($product['uj']) {  ?>
                            <?php if($this->config->get('megjelenit_uj') == 1){  ?>
                                <img class="icon_meret" title="<? if (isset($text_uj_title)) { echo $text_uj_title; }  ?>" src="<? echo $ujtermek  ?>" >
                            <?}  ?>
                        <?}  ?>
                    </div>
                <?php }  ?>

                <div class="name">
                    <a style="max-width: 125px;" href="<?php echo $product['href'];  ?>"><?php echo $product['name'];  ?></a>

                </div>
                <?php if($this->config->get('megjelenit_ikon_hely') == 2 || $this->config->get('megjelenit_ikon_hely') == 3) {  ?>
                    <div class="ikonok <?php echo $fektetve  ?>" >
                        <?php if ($product['special']) {  ?>
                            <?php if($this->config->get('megjelenit_szazalek') == 1){  ?>
                                <img class="icon_meret" title="<? if (isset($text_akcio_title)) { echo $text_akcio_title; }  ?>" src="<? echo $sale ?>" >
                            <?php }  ?>
                        <?}  ?>
                        <?php if ($product['pardarab']) {  ?>
                            <?php if($this->config->get('megjelenit_pardarab') == 1){  ?>
                                <img class="icon_meret" title="<? if (isset($text_pardarab_title)) { echo $text_pardarab_title; }  ?>" src="<? echo $pardarab ?>" >
                            <?}  ?>
                        <?}  ?>
                        <?php if ($product['uj']) {  ?>
                            <?php if($this->config->get('megjelenit_uj') == 1){  ?>
                                <img class="icon_meret" title="<? if (isset($text_uj_title)) { echo $text_uj_title; }  ?>" src="<? echo $ujtermek  ?>" >
                            <?}  ?>
                        <?}  ?>
                    </div>
                <?php }  ?>
                <div class="image">
                    <?php if(!empty($product['href'])) {  ?>
                        <a href="<?php echo $product['href'];  ?>">
                    <?php }  ?>
                        <img id="<?php echo $module_name.$product['product_id']   ?>" src="<?php echo $product['thumb'];  ?>" alt="<?php echo $product['name'];  ?>" />
                    <?php if(!empty($product['href'])) {  ?>
                        </a>
                    <?php }  ?>
                </div>
                <?php if($this->config->get('megjelenit_description') == 1){  ?>
                    <div class="description"><?php echo mb_substr($product['description'], 0, $this->config->get('megjelenit_description_karakterek'), "UTF-8") ?>
                        <?php if(!empty($product['description'])){ echo "...";}  ?>
                    </div>
                <?php }  ?>
            <?php }  ?>



            <div style="padding-bottom: 15px;">
                <?php if($this->config->get('megjelenit_brutto_ar') == 1){  ?>
                    <?php if($this->config->get('megjelenit_brutto_netto') == 1){  ?>
                        <?php if ($product['price']) {  ?>
                            <div class="price <?php echo $szazalek_class  ?>" >
                                <?php if (!$product['special']) {  ?>
                                    <span class="price-new"><?php echo $product['price'];  ?></span>
                                <?php } else {  ?>
                                    <span class="price-old"><?php echo $product['price'];  ?></span> <span class="price-new"><?php echo $product['special'];  ?></span>
                                <?php }  ?>
                            </div>
                            <?php if ($product['tax']) {  ?>
                                <?php if($this->config->get('megjelenit_netto_ar') == 1){  ?>
                                    <?php if (!$product['special']) {  ?>
                                        <span class="price-tax"><?php echo "text_netto";  ?> <?php echo $product['price_netto'];  ?></span>
                                    <?php } else {  ?>
                                        <span class="price-tax"><?php echo "text_netto";  ?> <?php echo $product['special_netto'];  ?></span>
                                    <?php }  ?>
                                <?php }  ?>
                            <?php }  ?>
                        <?php }  ?>
                    <?php } else {  ?>
                        <?php if ($product['price_netto']) {  ?>
                            <?php if($this->config->get('megjelenit_netto_ar') == 1){  ?>
                                <div class="price  <?php echo $szazalek_class  ?>"">
                                    <?php if (!$product['special_netto']) {  ?>
                                        <span class="price-new"><?php echo $product['price_netto'];  ?></span>
                                    <?php } else {  ?>
                                        <span class="price-old"><?php echo $product['price_netto'];  ?></span> <span class="price-new"><?php echo $product['special_netto'];  ?></span>
                                    <?php }  ?>
                                </div>
                                <?php if (!$product['special_netto']) {  ?>
                                    <span class="price-tax"><?php echo "Brutto:";  ?> <?php echo $product['price'];  ?></span>
                                <?php } else {  ?>
                                    <span class="price-tax"><?php echo "Brutto:";  ?> <?php echo $product['special'];  ?></span>
                                <?php }  ?>
                            <?php } else {  ?>

                                <?php if ($product['utalvany'] == 1) {  ?>
                                    <?php $class="utalvany"; ?>
                                <?php } else {  ?>
                                    <?php $class="";  ?>
                                <?php }  ?>


                                <div class="price <?php echo $class ?>  <?php echo $szazalek_class ?>">
                                    <?php if (!$product['special']) {  ?>
                                        <span class="price-new"><?php echo $product['price'];  ?></span>
                                    <?php } else {  ?>
                                        <span class="price-old"><?php echo $product['price'];  ?></span> <span class="price-new"><?php echo $product['special'];  ?></span>
                                    <?php }  ?>
                                </div>

                            <?php }  ?>
                        <?php }  ?>
                    <?php }  ?>









































                <?php } elseif ($this->config->get('megjelenit_netto_ar') == 1){  ?>
                    <?php if ($product['price_netto']) {  ?>
                        <div class="price  <?php echo $szazalek_class  ?>">
                            <?php if (!$product['special_netto']) {  ?>
                                <span class="text-netto"><?php echo $text_netto;  ?></span>
                                    <?php if ($this->config->get('megjelenit_netto_ujsor') == 1){  ?>
                                        <br />
                                    <?php }  ?>
                                <span class="price-new"><?php echo $product['price_netto'];  ?></span>
                            <?php } else {  ?>
                                <span class="text-netto"><?php echo $text_netto;  ?></span>
                                    <?php if ($this->config->get('megjelenit_netto_ujsor') == 1){  ?>
                                        <br />
                                    <?php }  ?>
                                <span class="price-old"><?php echo $product['price_netto'];  ?></span> <span class="price-new"><?php echo $product['special_netto'];  ?></span>
                            <?php }  ?>
                        </div>
                    <?php }  ?>
                <?php }  ?>
            </div>

            <?php if($this->config->get('megjelenit_kosarba') == 1 || (isset($kosarba_gomb) && $kosarba_gomb) ){  ?>
                <?if ($product['csomagolasi_mennyiseg'] > 1 && $this->config->get('megjelenit_csomagolas_admin') == 1 ){  ?>
                    <div class="cart">

                        <input type="hidden" name="quantity2" />
                        <input id="category_list_csomagolas_<?php echo $product['product_id'];  ?>" type="hidden" value="<? echo $product['csomagolasi_mennyiseg']  ?>" />
                        <?php if($this->config->get('megjelenit_csomagolasi_mennyiseg') == 1){  ?>
                            <div class="csomagolasi_mennyiseg_racs" >
                        <? }  ?>

                            <?php if($this->config->get('megjelenit_mennyiseg') == 1){  ?>
                                <input class="mennyit_vasarol" id="category-grid-quantity_<?php echo $product['product_id'];  ?>" type="text" value="1" />
                                <?php if($this->config->get('megjelenit_csomagolasi_mennyiseg') == 1){  ?>
                                    <span class="csomagolasi_mennyiseg_span" style="margin-left: 4px;"><?echo 'x '.$product['csomagolasi_mennyiseg'].' '. $product['megyseg'].'/'.$product['csomagolasi_egyseg']  ?></span>
                                <?php }  ?>

                            <?php } else {  ?>
                                <input  id="category-grid-quantity_<?php echo $product['product_id'];  ?>" type="hidden" value="1" />
                                <?php if($this->config->get('megjelenit_csomagolasi_mennyiseg') == 1){  ?>
                                    <span class="csomagolasi_mennyiseg_span"><?echo $product['csomagolasi_mennyiseg'].' '. $product['megyseg'].'/'.$product['csomagolasi_egyseg']  ?></span>
                                <?php }  ?>
                            <?php }  ?>

                        <?php if($this->config->get('megjelenit_csomagolasi_mennyiseg') == 1){  ?>
                            </div>
                        <? }  ?>
                        <span class="atmenet-hatter">
                        <input type="button" value="<?php echo $button_cart;  ?>" onclick="addToCart('<?php echo $product['product_id'];  ?>',$('#category-grid-quantity_<?php echo $product['product_id'];  ?>').val()*<? echo $product['csomagolasi_mennyiseg'] ?>,'<?php echo $this->config->get('config_template') ?>','<?php echo $module_name.$product['product_id'] ?>')" class="button" />
                        </span>
                    </div>

                <? } else {  ?>
                    <div class="cart">
                        <?php $product['csomagolasi_mennyiseg'] = 1;  ?>
                        <?php if($this->config->get('megjelenit_mennyiseg') == 1){  ?>
                            <input class="mennyit_vasarol" id="category-grid-quantity_<?php echo $product['product_id'];  ?>" type="text" value="1" style="margin-right: 1px;" />
                        <?php } else {  ?>
                            <input id="category-grid-quantity_<?php echo $product['product_id'];  ?>" type="hidden" value="1" style="margin-right: 1px;" />
                        <?php }  ?>
                    <span class="atmenet-hatter">
                        <input type="button" value="<?php echo $button_cart;  ?>" onclick="addToCart('<?php echo $product['product_id'];  ?>',$('#category-grid-quantity_<?php echo $product['product_id'];  ?>').val()*<? echo $product['csomagolasi_mennyiseg'] ?>,'<?php echo $this->config->get('config_template') ?>','<?php echo $module_name.$product['product_id'] ?>')" class="button" />
                    <span>
                    </div>

                <? }  ?>

            <?php } else {  ?>
                <div class="cart">
                    <a href="<?php echo $product['href'];  ?>">
                        <input class="kosar button" type="button" style="" value="<?php echo $button_bovebben;  ?>" />
                    </a>
                </div>

            <?php }  ?>
            <?php if($this->config->get('megjelenit_kivansaglista') == 1){  ?>
                <div class="wishlist">
                    <a onclick="addToWishList('<?php echo $product['product_id'];  ?>');"><?php echo $button_wishlist;  ?></a>
                </div>
            <?php }  ?>
            <?php if($this->config->get('megjelenit_osszehasonlitas') == 1){  ?>
                <div class="compare">
                    <a onclick="addToCompare('<?php echo $product['product_id'];  ?>');"><?php echo $button_compare;  ?></a>
                </div>
            <?php }  ?>
        <?php }




        else { /* fektetett template */  ?>


            <?php $osztaly = '' ?>
            <?php $max_width = '' ?>

            <?php if ($product['imagedesabled'] == 1) {
                    $kep_tiltas = 'display:none';
                    $osztaly = "no_kep_nelkul";
                    $max_width = "max_width";
            } else {
                $kep_tiltas = 'display:block';
            }

            $kep_name = basename($product['thumb']);
            if (strpos($kep_name,"no_image") !== false ) {
                if ($megjelenit_altalanos['no_kep_tiltas'] == 1) {
                    $kep_tiltas = 'display:none';
                    $osztaly = "no_kep_nelkul";
                    $max_width = "max_width";
                }
            }  ?>




            <?php if ($product['thumb']) {  ?>

                <div class="image">
                    <?php if(!empty($product['href'])) {  ?>
                    <a href="<?php echo $product['href'];  ?>">
                        <?php }  ?>
                        <img class="fektetett_tpl_kep" style="<?php echo $kep_tiltas;  ?>" id="<?php echo $module_name.$product['product_id']   ?>" src="<?php echo $product['thumb'];  ?>" alt="<?php echo $product['name'];  ?>" />
                        <?php if(!empty($product['href'])) {  ?>
                    </a>
                <?php }  ?>
                </div>




                <!--<?php $kep_name = basename($product['thumb']);  ?>
                <?php if (strpos($kep_name,"no_image") !== false ) {  ?>

                    <?php if ($megjelenit_altalanos['no_kep_tiltas'] != 1) {  ?>
                        <div class="image">
                            <?php if(!empty($product['href'])) {  ?>
                                <a href="<?php echo $product['href'];  ?>">
                            <?php }  ?>
                                <img class="fektetett_tpl_kep" id="<?php echo $module_name.$product['product_id']   ?>" src="<?php echo $product['thumb'];  ?>" alt="<?php echo $product['name'];  ?>" />
                            <?php if(!empty($product['href'])) {  ?>
                                </a>
                            <?php }  ?>
                        </div>

                    <?php } else {  ?>
                        <?php $osztaly = "no_kep_nelkul" ?>
                        <?php $max_width = "max_width" ?>
                    <?php }  ?>

                <?php } else {  ?>
                    <div class="image">
                        <?php if(!empty($product['href'])) {  ?>
                        <a href="<?php echo $product['href'];  ?>">
                            <?php }  ?>
                            <img class="fektetett_tpl_kep" id="<?php echo $module_name.$product['product_id']   ?>" src="<?php echo $product['thumb'];  ?>" alt="<?php echo $product['name'];  ?>" />
                            <?php if(!empty($product['href'])) {  ?>
                        </a>
                    <?php }  ?>
                    </div>
                <?php }  ?>-->

            <?php }  ?>



            <div class="jobboldal <?php echo $osztaly ?>">
                <?php if($this->config->get('megjelenit_brutto_ar') == 1){  ?>
                    <?php if ($product['price']) {  ?>
                        <?php if($product['utalvany'] == 1 ) {  ?>
                            <div class="price-szazalek">
                                <span class="price-new-szazalek"><?php echo $product['price']  ?></span>
                            </div>

                        <?php } else {  ?>
                            <div class="price">
                                 <span class="price-new"><?php echo $product['eredeti_ar'];  ?></span>
                            </div>
                        <?php }  ?>
                    <?php }  ?>
                <?php }  ?>

                <div class="name">
                    <?php if(!empty($product['href'])) {  ?>
                        <a href="<?php echo $product['href'];  ?>">
                    <?php }  ?>

                    <?php echo $product['name']; ?>

                    <?php if(!empty($product['href'])) {  ?>
                        </a>
                    <?php }  ?>

                </div>

                <?php if(isset($kaphato) && !empty($kaphato) && ($product['date_ervenyes_ig'] >= date('Y-m-d') || (isset($elonezet) && $elonezet == 1) ) )  {  ?>
                    <div class="szoveg date_ervenyes_ig">
                        <?php if(!empty($product['href'])) {  ?>
                            <a href="<?php echo $product['href'];  ?>">
                        <?php }  ?>
                            <?php echo $kaphato ?>: <span><?php echo $product['date_ervenyes_ig'];  ?></span>-ig
                        <?php if(!empty($product['href'])) {  ?>
                            </a>
                        <?php }  ?>
                    </div>
                <?php }  ?>

                <?php if($product['utalvany'] != 1 ) {  ?>
                    <div class="szoveg">
                        <?php if(!empty($product['href'])) {  ?>
                            <a href="<?php echo $product['href'];  ?>">
                        <?php }  ?>
                        <b><?php echo $text_eredeti_ar ?>: <span><?php echo $product['price']  ?></span></b>
                        <?php if(!empty($product['href'])) {  ?>
                            </a>
                        <?php }  ?>
                    </div>
                <?php }  ?>

                <div class="szoveg" style="height: <?php echo ($this->config->get('config_image_product_height')-112 );  ?>px;overflow: hidden">
                    <?php if($this->config->get('megjelenit_description_fk') == 1){  ?>
                        <?php if(!empty($product['href'])) {  ?>
                            <a href="<?php echo $product['href'];  ?>">
                        <?php }  ?>

                            <div class="description <?php echo $max_width;  ?>">
                                <?php echo mb_substr($product['description'], 0, $this->config->get('megjelenit_description_karakterek_fk'), "UTF-8");
                                if(!empty($product['description']) && strlen($product['description']) > $this->config->get('megjelenit_description_karakterek_fk') ){
                                    echo "...";
                                }  ?>
                            </div>
                        <?php if(!empty($product['href'])) {  ?>
                            </a>
                        <?php }  ?>
                    <?php }  ?>
                </div>

                <div class="review">
                   <!-- <input  id="category-grid-quantity_<?php echo $product['product_id'];  ?>" type="hidden" value="1" />-->
                    <span class="share">
                        <?php if($this->config->get('megjelenit_email_fk') == 1){  ?>
                            <span class="addthis_default_style">
                                <a class="addthis_button_email"> </a>
                            </span>
                        <?php }  ?>
                        <?php if($this->config->get('megjelenit_facebook_fk') == 1){  ?>
                            <?
                            $fboldal = str_replace("http://","",HTTP_SERVER);
                            $fboldal = str_replace("www.","",$fboldal);
                            $fblink = "https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2F".$fboldal."%2Findex.php%3Froute%3Dproduct%2Fproduct%26product_id%3D";
                            $fbproduct_id = $product['product_id'];
                             ?>
                            <a href="<?php echo $fblink.$fbproduct_id;  ?>" target="_blank"><div class="fb_kep"></div></a>
                        <?php }  ?>
                        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53675cea59952f34"></script>
                    </span>
                </div>

                <?php $product['csomagolasi_mennyiseg'] = 1;  ?>
                <div class="kosargomb">

                    <?php if($this->config->get('megjelenit_kosarba_fk') == 1){  ?>



                        <?if ($product['csomagolasi_mennyiseg'] > 1 && $this->config->get('megjelenit_csomagolas_admin') == 1 ){  ?>
                            <div class="cart">

                                <input type="hidden" name="quantity2" />
                                <input id="category_list_csomagolas_<?php echo $product['product_id'];  ?>" type="hidden" value="<? echo $product['csomagolasi_mennyiseg'] ?>" />
                                <?php if($this->config->get('megjelenit_csomagolasi_mennyiseg_fk') == 1){  ?>
                                <div class="csomagolasi_mennyiseg_racs" >
                                    <? }  ?>

                                    <?php if($this->config->get('megjelenit_mennyiseg_fk') == 1){  ?>
                                        <input class="mennyit_vasarol" id="category-grid-quantity_<?php echo $product['product_id'];  ?>" type="text" value="1" />
                                        <?php if($this->config->get('megjelenit_csomagolasi_mennyiseg_fk') == 1){  ?>
                                            <span class="csomagolasi_mennyiseg_span" style="margin-left: 4px;"><?echo 'x '.$product['csomagolasi_mennyiseg'].' '. $product['megyseg'].'/'.$product['csomagolasi_egyseg'] ?></span>
                                        <?php }  ?>

                                    <?php } else {  ?>
                                        <input  id="category-grid-quantity_<?php echo $product['product_id'];  ?>" type="hidden" value="1" />
                                        <?php if($this->config->get('megjelenit_csomagolasi_mennyiseg_fk') == 1){  ?>
                                            <span class="csomagolasi_mennyiseg_span"><?echo $product['csomagolasi_mennyiseg'].' '. $product['megyseg'].'/'.$product['csomagolasi_egyseg'] ?></span>
                                        <?php }  ?>
                                    <?php }  ?>

                                    <?php if($this->config->get('megjelenit_csomagolasi_mennyiseg_fk') == 1){  ?>
                                </div>
                            <? }  ?>
                                <span class="atmenet-hatter">
                                <input type="button" value="<?php echo $button_cart;  ?>" onclick="addToCart('<?php echo $product['product_id'];  ?>',$('#category-grid-quantity_<?php echo $product['product_id'];  ?>').val()*<? echo $product['csomagolasi_mennyiseg'] ?>,'<?php echo $this->config->get('config_template') ?>','<?php echo $module_name.$product['product_id'] ?>')" class="button" />
                                </span>
                            </div>

                        <? } else {  ?>
                            <?php if($this->config->get('megjelenit_megnezemgomb_fk') == 1) {  ?>
                                <span class="cart">
                            <?php } else {  ?>
                            <div class="cart">
                            <?php }  ?>
                                <?php $product['csomagolasi_mennyiseg'] = 1;  ?>
                                <?php if($this->config->get('megjelenit_mennyiseg_fk') == 1){  ?>
                                    <input class="mennyit_vasarol" id="d-quantity_<?php echo $product['product_id'];  ?>" type="text" value="1" style="margin-right: 1px;" />
                                <?php } else {  ?>
                                    <input id="d-quantity_<?php echo $product['product_id'];  ?>" type="hidden" value="1" style="margin-right: 1px;" />
                                <?php }  ?>
                                <span class="atmenet-hatter">
                                 <?php if($this->config->get('megjelenit_minikosar_fk') == 1) {  ?>
                                    <input type="button" value="" onclick="addToCart('<?php echo $product['product_id'];  ?>',$('#d-quantity_<?php echo $product['product_id'];  ?>').val()*<? echo $product['csomagolasi_mennyiseg'] ?>,'<?php echo $this->config->get('config_template') ?>','<?php echo $module_name.$product['product_id'] ?>')" class="minicart" />
                                <?php } else {  ?>
                                     <input type="button" value="<?php echo $button_cart;  ?>" onclick="addToCart('<?php echo $product['product_id'];  ?>',$('#d-quantity_<?php echo $product['product_id'];  ?>').val()*<? echo $product['csomagolasi_mennyiseg'] ?>,'<?php echo $this->config->get('config_template') ?>','<?php echo $module_name.$product['product_id'] ?>')" class="button" />
                                <?php }  ?>
                                </span>
                            <?php if($this->config->get('megjelenit_megnezemgomb_fk') == 1) { ?>
                            </span>
                            <?php } else {  ?>
                            </div>
                            <?php }  ?>
                            <?php if($this->config->get('megjelenit_megnezemgomb_fk') == 1) {  ?>
                                <?php if(!empty($product['href'])) {  ?>
                                    <a href="<?php echo $product['href'];  ?>">
                                <?php }  ?>
                                <input class="bovebben" type="button" value="<?php echo $button_bovebben;  ?>" class="button" />
                                <?php if(!empty($product['href'])) {  ?>
                                    </a>
                                <?php }  ?>
                            <?php }  ?>

                        <? }  ?>


                    <?php } else {  ?>
                        <?php if($this->config->get('megjelenit_megnezemgomb_fk') == 1) {  ?>
                            <?php if(!empty($product['href'])) {  ?>
                                <a href="<?php echo $product['href'];  ?>">
                            <?php }  ?>
                                <input class="bovebben" type="button" value="<?php echo $button_bovebben;  ?>" class="button" />
                            <?php if(!empty($product['href'])) {  ?>
                                </a>
                            <?php }  ?>
                        <?php }  ?>
                    <?php }  ?>

                </div>
            </div>

            <script>
                $('.box-product > div').css('height','<?php echo $this->config->get('config_image_product_height'); ?>px');
            </script>
        <?php }  ?>
    <?php } else {  ?>

        <?php if ( $this->config->get('megjelenit_allitott') == 1 || (isset($kapcsolodo_allitott) && $kapcsolodo_allitott == 1 ) ){  ?>
            <?php $termek_dobozok = $this->config->get('megjelenit_termek_doboz');  ?>
            <?php $allitott = true ?>

        <?php } else {  ?>
            <?php $allitott = false ?>
            <?php $termek_dobozok = $this->config->get('megjelenit_termek_fektetett');  ?>
        <?php }  ?>

        <?php if ($termek_dobozok) {  ?>
            <?php $oszlop = 0; ?>
            <?php $sor = 0; ?>
            <?php $nyitott = false; ?>

            <?php foreach($termek_dobozok as $key=>$beallitas) { ?>
                <?php if ($beallitas['status'] == 1 ) { ?>

                    <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/termek_doboz/'. $key .'.tpl')) {
                        $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/termek_doboz/'.$key . '.tpl';
                    } else {
                        $arkiir = DIR_TEMPLATE.'default/template/module/termek_doboz/'.$key.'.tpl';
                    }  ?>

                    <?php if ($allitott) {  ?>
                        <?php if ($sor != $beallitas['sort_order_sor']) {  ?>
                            <?php $sor = $beallitas['sort_order_sor'] ?>
                            <?php if ($nyitott) { ?>
                                <?php $nyitott = false; ?>
                                </div>
                            <?php }  ?>

                            <div class="sor-<?php echo $beallitas['sort_order_sor'] ?>">
                            <?php $nyitott = true; ?>
                        <?php }  ?>

                    <?php } else { ?>
                        <?php if ($oszlop != $beallitas['sort_order_oszlop']) {  ?>
                            <?php $oszlop = $beallitas['sort_order_oszlop'] ?>
                            <?php if ($nyitott) { ?>
                                <?php $nyitott = false; ?>
                                </div>
                            <?php }  ?>

                            <div class="oszlop-<?php echo $beallitas['sort_order_oszlop'] ?>">
                            <?php $nyitott = true; ?>
                        <?php }  ?>
                    <?php }  ?>

                    <?php include($arkiir);  ?>

                <?php }  ?>

            <?php }  ?>

            <?php if ($nyitott) {  ?>
                </div>
            <?php }  ?>
        <?php }  ?>


    <?php }  ?>
<?php } else {  ?>

    <?php $megjelenit_racs_csoportok = $this->config->get('racs_beallitas_racs_csoportok');  ?>
    <?php if ($megjelenit_racs_csoportok)  {  ?>

        <?php
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/stylesheet_racs.php')) {
            $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/stylesheet/stylesheet_racs.php';
            include_once($arkiir);

        } elseif (file_exists(DIR_TEMPLATE . 'default/stylesheet/stylesheet_racs.php')) {
            $arkiir = DIR_TEMPLATE.'default/stylesheet/stylesheet_racs.php';
            include_once($arkiir);

        }  ?>

        <?php   $elso       = true;
        $elozo      = 0;
        $divszamol  = 1;
        $div_nyit_racs   = 0;
        $div_zar_racs   = 0;
         ?>

        <?php foreach ($megjelenit_racs_csoportok as $key=>$value) {  ?>
            <?php
            if ($elso) {$elso = false;}
            else {
                $elozo = $divszamol;
            }
            $divszamol = count(explode('_',$key));

            if($divszamol <= $elozo) {
                for($i=$divszamol; $elozo >=$i; $i++) {
                    echo "</div>";
                    $div_zar_racs ++;

                }
            }
            $elotag = ($divszamol%2 == 0) ? "racs_oszlop_" : "racs_sor_";

            $style_inlines = isset($value['default']['inline']) && $value['default']['inline'] ? htmlspecialchars_decode($value['default']['inline']) : '';
            if ($style_inlines) {
                $style_inlines = str_replace(' ','',$style_inlines);
                $style_inline = explode(';',$style_inlines);

                foreach($style_inline as $key_inline=>$inline) {
                    $pos = strpos("$inline","$");
                    if ($pos !== false) {
                        $pos1 = strpos($inline,"this",$pos);
                        if ($pos1 == $pos+1) {
                            $pos2 = strpos($inline,"-",$pos1);
                            if ($pos2 == $pos1+4) {
                                $pos3 = strpos($inline,">",$pos2);
                                if ($pos3 == $pos2+1) {
                                    $pos4 = strpos($inline,"config",$pos3);
                                    if ($pos4 == $pos3+1) {
                                        $pos5 = strpos($inline,"-",$pos4);
                                        if ($pos5 == $pos4+6) {
                                            $pos6 = strpos($inline,">",$pos5);
                                            if ($pos6 == $pos5+1) {
                                                $pos7 = strpos($inline,"get",$pos6);
                                                if ($pos7 == $pos6+1) {
                                                    $parameter_pos_tol = strpos($inline,"'",$pos7);
                                                    if ($parameter_pos_tol == $pos7+4) {
                                                        $parameter_pos_tol ++;
                                                        $parameter_pos_ig = strpos($inline,"'",$parameter_pos_tol);
                                                        $parameter_pos_ig;
                                                        $parameter = substr($inline,$parameter_pos_tol,($parameter_pos_ig-$parameter_pos_tol));

                                                    } else {
                                                        $parameter_pos_tol = strpos($inline,'"',$pos7);
                                                        if ($parameter_pos_tol == $pos7+4) {
                                                            $parameter_pos_tol ++;
                                                            $parameter_pos_ig = strpos($inline,"'",$parameter_pos_tol);
                                                            $parameter = substr($inline,$parameter_pos_tol,($parameter_pos_ig-$parameter_pos_tol));
                                                        }
                                                    }

                                                    if (isset($parameter) && $parameter) {
                                                        $eredmeny=$this->config->get($parameter);
                                                        $style_inline[$key_inline] = substr($inline,0,$pos);
                                                        $style_inline[$key_inline] .= $eredmeny;
                                                        $style_inline[$key_inline] .= substr($inline,$parameter_pos_ig+2);
                                                    }

                                                }

                                            }

                                        }

                                    }

                                }

                            }
                        }
                    }
                }
                $style_inlines = implode(';',$style_inline);
            }
             ?>
            <div class="<?php echo $elotag.$key. ' ' . $value['default']['class'] ?> "  style="<?php echo $style_inlines ?>">
            <?php $div_nyit_racs++; ?>
            <?php if ($value['default']['status'] == 1) {  ?>
                <?php if(isset($value['templates']) && !empty($value['templates'])) {  ?>
                    <?php foreach ($value['templates'] as $keytemplate=>$beallitas) {  ?>
                        <?php if (isset($beallitas['status']) && $beallitas['status'] == 1) { ?>
                            <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/termek_doboz/'. $keytemplate .'.tpl')) {
                                $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/termek_doboz/'.$keytemplate . '.tpl';
                            } else {
                                $arkiir = DIR_TEMPLATE.'default/template/module/termek_doboz/'.$keytemplate.'.tpl';
                            }  ?>
                            <?php include($arkiir);  ?>
                        <?php }  ?>

                    <?php }  ?>
                <?php }  ?>
            <?php }  ?>
        <?php }


        if ($div_nyit_racs - $div_zar_racs > 0) {
            $div_nyitva_racs = $div_nyit_racs - $div_zar_racs;
            for ($i=0; $div_nyitva_racs > $i; $i++) {
                echo "</div>";
            }
        }
         ?>
    <?php }  ?>
<?php }  ?>

<script>
    view = $.cookie('display');

</script>