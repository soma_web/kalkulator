<?php if ($show == 1) {?>
    <div id="mcs5_container_<?php echo $id_name?>">
    <div class="customScrollBox">
    <div class="horWrapper">
    <div class="container">
    <div class="content">
<?php } elseif ($show == 2) {?>
    <div id="carousel">
    <ul class="jcarousel22-skin-opencart box-product">
<?php } ?>

<?php if ($show != 2) { ?>
    <div class="box-product">
<?php } ?>

<?php $bal = true; ?>
<?php  foreach ($$prs_name as $product) { ?>
    <?php if ($show == 2) { ?>
        <li style="position: relative">
        <div class=" box-product">
    <? } ?>
    <?php
    if ($bal) {
        $merre = "balra";
        $bal = false;
    } else {
        $merre = "jobbra";
        $bal = true;
    }?>

    <?php if ($product['utalvany'] != 0) { ?>
        <?php $class=" szazalek";?>
    <?php } else {?>
        <?php $class="";?>
    <?php } ?>
    <div class="<?php echo $merre; echo $class; ?>">
        <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
            $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
        } else {
            $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
        } ?>
        <?php include($arkiir); ?>
    </div>
    <?php if ($show == 2) { ?>
        </div>
        </li>
    <?php } ?>
<?php } ?>


<?php if ($show != 2) { ?>
    </div>
<?php } ?>

<?php if ($show == 1) {?>
    </div>
    </div>
    </div>

    <div class="dragger_container">
        <div class="dragger"></div>
    </div>

    </div>
    </div>
<?php } elseif ($show == 2) {?>
    </ul>
    </div>
<?php } ?>


<?php if ($show == 1) {?>
    <noscript>
        <style type="text/css">
            #mcs5_container_<?php echo $id_name?> .customScrollBox{overflow:auto;}
            #mcs5_container_<?php echo $id_name?> .dragger_container{display:none;}
        </style>
    </noscript>

    <script>
        mCustomScrollbars_<?php echo $id_name?>();

        /*$(window).load(function() {
        });*/

        function mCustomScrollbars_<?php echo $id_name?>(){
            $("#mcs5_container_<?php echo $id_name?>").mCustomScrollbar("horizontal",500,"easeOutCirc",1,"fixed","yes","no",20);
        }

        /* function to fix the -10000 pixel limit of jquery.animate */
        $.fx.prototype.cur = function(){
            if ( this.elem[this.prop] != null && (!this.elem.style || this.elem.style[this.prop] == null) ) {
                return this.elem[ this.prop ];
            }
            var r = parseFloat( jQuery.css( this.elem, this.prop ) );
            return typeof r == 'undefined' ? 0 : r;
        }

        /* function to load new content dynamically */
        function LoadNewContent(id,file){
            $("#"+id+" .customScrollBox .content").load(file,function(){
                mCustomScrollbars();
            });
        }
    </script>
<?php } elseif ($show == 2) {?>
    <script type="text/javascript">
        $('#carousel ul').jcarousel22({
            vertical: false,
            visible: 4,
            scroll: 3
        });
    </script>
<?php } ?>
