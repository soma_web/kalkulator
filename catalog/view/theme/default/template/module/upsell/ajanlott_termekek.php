<?php
$_['korhinta']          = array(
    'type'              => 'checkbox',
    'name'              => 'korhinta',
    'default'           => "1",
    'heading'           => 'Körhinta'
);

$_['korhinta_darab']    = array(
    'type'              => 'text',
    'name'              => 'korhinta_darab',
    'size'              => "2",
    'default'           => "3",
    'heading'           => "Mejelenít"
);

$_['korhinta_width']    = array(
    'type'              => 'text',
    'name'              => 'korhinta_width',
    'size'              => "2",
    'default'           => "260",
    'heading'           => "Termék szélesség"
);

$_['korhinta_infinity'] = array(
    'type'              => 'checkbox',
    'name'              => 'korhinta_infinity',
    'default'           => "1",
    'heading'           => 'Végtelen'
);

$_['korhinta_speed']    = array(
    'type'              => 'text',
    'name'              => 'korhinta_speed',
    'size'              => "4",
    'default'           => "1500",
    'heading'           => "Sebesség"
);

$_['korhinta_width']    = array(
    'type'              => 'text',
    'name'              => 'korhinta_width',
    'size'              => "2",
    'default'           => "260",
    'heading'           => "Termék szélesség"
);
?>