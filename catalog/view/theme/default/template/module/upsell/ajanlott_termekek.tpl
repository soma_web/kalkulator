<div class="ajanlott_termekek" >
    <?php  foreach ($ajanlott_termekek as $product) { ?>
        <div >
            <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
            } else {
                $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
            } ?>
            <?php include($arkiir); ?>
        </div>
    <?php } ?>
</div>

<?php if (isset($beallitas_upsell['korhinta']) && $beallitas_upsell['korhinta'] == 1
            && (isset($beallitas_upsell['korhinta_darab']) && (($beallitas_upsell['korhinta_darab']) <= count($ajanlott_termekek)))) { ?>

    <script>
        $('.ajanlott_termekek').bxSlider({
            minSlides: <?php echo isset($beallitas_upsell['korhinta_darab']) ? $beallitas_upsell['korhinta_darab'] : 3?>,
            maxSlides: <?php echo isset($beallitas_upsell['korhinta_darab']) ? $beallitas_upsell['korhinta_darab'] : 3?>,
            slideWidth: <?php echo isset($beallitas_upsell['korhinta_width']) ? $beallitas_upsell['korhinta_width'] : 260?>,
            slideMargin: 10,
            auto: true,
            pause: <?php echo isset($beallitas_upsell['korhinta_speed']) ? $beallitas_upsell['korhinta_speed'] : 1500?>,
            autoHover: true,
            infiniteLoop: <?php echo isset($beallitas_upsell['korhinta_infinity']) ? "true" : "false" ?>,
            responsive: true,
            hideControlOnEnd: true,
            adaptiveHeight: false
        });
    </script>

<?php } ?>