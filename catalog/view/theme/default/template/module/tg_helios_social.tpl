<div class="box-right-social">

		<div class="box-right-social-content">
				<div class="box-right-social-product">
					
					<?php if ($this->config->get('facebook_show') == "1") { ?>
					<div>
						<div class="image"><a href="<?php echo $this->config->get('facebook_icon');?>"  target="_blank"> <img src="catalog/view/theme/helios/image/social_icons/<?php echo ($this->config->get('tg_helios_cp_default_color')); ?>/facebook.png" title="Facebook"/> </a></div>
						<div class="name"><a href="<?php echo $this->config->get('facebook_icon');?>"  target="_blank"><?php echo $text_facebook; ?></a></div>
					</div>
					<?php } ?>
					
					<?php if ($this->config->get('twitter_show') == "1") { ?>
					<div>
						<div class="image"><a href="<?php echo $this->config->get('twitter_icon');?>"  target="_blank"> <img src="catalog/view/theme/helios/image/social_icons/<?php echo ($this->config->get('tg_helios_cp_default_color')); ?>/twitter.png" title="Twitter"/> </a></div>
						<div class="name"><a href="<?php echo $this->config->get('twitter_icon');?>"  target="_blank"><?php echo $text_twitter; ?></a></div>
					</div>
					<?php } ?>
					
					<?php if ($this->config->get('vimeo_show') == "1") { ?>
					<div>
						<div class="image"><a href="<?php echo $this->config->get('vimeo_icon');?>"  target="_blank"> <img src="catalog/view/theme/helios/image/social_icons/<?php echo ($this->config->get('tg_helios_cp_default_color')); ?>/vimeo.png" title="Vimeo"/> </a></div>
						<div class="name"><a href="<?php echo $this->config->get('vimeo_icon');?>"  target="_blank"><?php echo $text_vimeo; ?></a></div>
					</div>
					<?php } ?>
					
					<?php if ($this->config->get('yahoomessenger_show') == "1") { ?>
					<div>
						<div class="image"><a href="ymsgr:sendIM?<?php echo $this->config->get('yahoomessenger_icon');?>"> <img src="catalog/view/theme/helios/image/social_icons/<?php echo ($this->config->get('tg_helios_cp_default_color')); ?>/yahoomessenger.png" title="yahoomessenger"/> </a></div>
						<div class="name"><a href="ymsgr:sendIM?<?php echo $this->config->get('yahoomessenger_icon');?>"> <?php echo $text_yahoomessenger; ?></a></div>
					</div>
					<?php } ?>
					
					<?php if ($this->config->get('yahoomessenger_show') == "1") { ?>
					<div>
						<div class="image"><a href="skype:<?php echo $this->config->get('skype_icon');?>?chat" target="_blank"> <img src="catalog/view/theme/helios/image/social_icons/<?php echo ($this->config->get('tg_helios_cp_default_color')); ?>/skype.png" title="Skype" /> </a> </a></div>
						<div class="name"><a href="skype:<?php echo $this->config->get('skype_icon');?>?chat" target="_blank"> <?php echo $text_skype; ?></a></div>
					</div>
					<?php } ?>
					
					<?php if ($this->config->get('flickr_show') == "1") { ?>
					<div>
						<div class="image"><a href="<?php echo $this->config->get('flickr_icon');?>"  target="_blank"> <img src="catalog/view/theme/helios/image/social_icons/<?php echo ($this->config->get('tg_helios_cp_default_color')); ?>/flickr.png" title="flickr"/> </a></div>
						<div class="name"><a href="<?php echo $this->config->get('flickr_icon');?>"  target="_blank"><?php echo $text_flickr; ?></a></div>
					</div>
					<?php } ?>
					
					

				</div>
		</div>


		
					
</div>					


