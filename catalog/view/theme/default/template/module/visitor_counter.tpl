<div class="box">
    <?php if ($heading_latszik) { ?>
        <div class="box-heading"><span><?php echo $heading_title; ?></span></div>
    <?php } ?>
  <div class="box-content" style="text-align: center;">
      <img class="visitor-frontimage" src="/image/count_front.png">
  <div class="box-visitors">
  	  <div><?php echo $visitor_counter_num; ?><br /></div>
  	  <table width="90%" align="center" class="visitor_counter_table">
  	     <tr>
  	       <td valign="middle" align="left"><img src="image/counter/todayv.png"></td>
  	       <td valign="middle" align="left">Today Visitors</td>
  	       <td valign="middle" align="right"><?php echo $today_visitor; ?></td>
  	     </tr>
  	     <tr>
  	       <td valign="middle" align="left"><img src="image/counter/totalv.png"></td>
  	       <td valign="middle" align="left">Total Visitors</td>
  	       <td valign="middle" align="right"><?php echo $total_visitor; ?></td>
  	     </tr>
  	     <tr>
  	       <td valign="middle" align="left"><img src="image/counter/todayh.png"></td>
  	       <td valign="middle" align="left">Today Hits</td>
  	       <td valign="middle" align="right"><?php echo $today_hits; ?></td>
  	     </tr>
  	     <tr>
  	       <td valign="middle" align="left"><img src="image/counter/totalh.png"></td>
  	       <td valign="middle" align="left">Total Hits</td>
  	       <td valign="middle" align="right"><?php echo $total_hits; ?></td>
  	     </tr>
  	     <tr>
  	       <td valign="middle" align="left"><img src="image/counter/online.png"></td>
  	       <td valign="middle" align="left">Online Visitors</td>
  	       <td valign="middle" align="right"><?php echo $online_visitor; ?></td>
  	     </tr>
  	  </table>
  </div>
  
  </div>
</div>

