<?php



    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/stylesheet_arajanlat.php')) {
        $stylesheet = DIR_TEMPLATE. $this->config->get('config_template') . '/stylesheet/stylesheet_arajanlat.php';
    } else {
        $stylesheet = DIR_TEMPLATE.'default/stylesheet/stylesheet_arajanlat.php';
    }

    require_once($stylesheet);
?>

<div id="arajanlatok">
    <div class="ajanlat_form">
        <img class="close_arajanlat_icon" title="<?php echo $text_close; ?>" alt="<?php echo $text_close;?>"  src="<?php echo $close?>" >

        <div style="display: table; width: 100%; margin-bottom: 10px;">
            <h1 style="display: table-cell"><?php echo $heading_title; ?></h1>
            <div style="display: table-cell; text-align: right">
                <span style="color: red; font-size: 17px"><?php echo $text_ajanlat_neve?></span>
                <input onchange="ajanlatKeresMegtekintes()" type="text" name="arajanlat_neve"
                       style="width: 30%;min-width: 110px;height: 27px; font-size: 15px; background-color: #f2f2f2; color: #222222"
                       value="<?php echo $ajanlat_neve?>">
            </div>
        </div>

            <table class="arajanlat-table">
                <thead>
                <tr>
                    <th class="image"><?php echo $column_image; ?></th>
                    <th class="name"><?php echo $column_name; ?></th>
                    <?php if ( $megjelenit_kosar['model_adat'] == 1) { ?>
                        <th class="model"><?php echo $column_model; ?></th>
                    <?php } ?>
                    <th class="quantity"><?php echo $column_quantity; ?></th>
                    <th class="price"><?php echo $column_price; ?></th>
                    <th class="total"><?php echo $column_total; ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($products as $product) { ?>
                    <tr>
                        <td data-th="<?php echo $column_image; ?>"  class="image"><?php if ($product['thumb']) { ?>
                                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
                            <?php } ?></td>
                        <td data-th="<?php echo $column_name; ?>" class="name">
                            <a href="<?php echo $product['href']; ?>" class="arajanlat-termek-neve" style="">
                                <?php if ($megjelenit_product['product_model_termeknevben']) { ?>
                                    <?php if (isset($product['model']) && $product['model']) { ?>
                                        <span class="model_cart"><?php echo $product['model'];?></span> <?php echo " - ";?>
                                    <?php }?>
                                <?php }?>

                                <?php if ($megjelenit_product['product_cikkszam_termeknevben']) { ?>
                                    <?php if (isset($product['cikkszam']) && $product['cikkszam']) { ?>
                                        <span class="model_cart"><?php echo $product['cikkszam'];?></span> <?php echo " - ";?>
                                    <?php }?>
                                <?php }?>

                                <?php if ($megjelenit_product['product_gyarto_termeknevben']) { ?>
                                    <?php if (isset($product['manufacturer']) && $product['manufacturer']) { ?>
                                        <span class="manufacturer_cart"><?php echo $product['manufacturer'];?></span> <?php echo " - ";?>
                                    <?php }?>
                                <?php }?>
                                <?php echo $product['name']; ?>
                            </a>


                            <div>
                                <?php foreach ($product['option'] as $option) { ?>
                                    - <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                        <?php if(!$option['required']) { ?>
                                            <a class="termek-torol option" onclick="ajanlatOptionTorol('<?php echo $product['key'];?>','<?php echo $option['product_option_id']?>','<?php echo $option['product_option_value_id'];?>')">
                                                <img class="torles_icon option" src="catalog/view/theme/default/image/remove.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" />
                                            </a>
                                        <?php } else { ?>
                                    <a class="termek-torol option" ></a>
                                <?php } ?>
                                    <br />
                                <?php } ?>
                            </div>

                        <?php if ( $megjelenit_kosar['model_adat'] == 1) { ?>
                            <td data-th="<?php echo $column_model; ?>"  class="model"><?php echo $product['model']; ?></td>
                        <?php } ?>

                        <td data-th="<?php echo $column_quantity; ?>"  class="quantity">

                            <input onchange="ajanlatMennyiseg('<?php echo $product['key'];?>',this)" readonly type="text" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" size="4" />

                            <a class="termek-torol" onclick="ajanlatTermekTorol('<?php echo $product['key'];?>')">
                                <img class="torles_icon" src="catalog/view/theme/default/image/remove.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" />
                            </a>
                        </td>
                        <td data-th="<?php echo $column_price; ?>"  class="price"><?php echo $product['price']; ?></td>
                        <td data-th="<?php echo $column_total; ?>"  class="price"><?php echo $product['total']; ?></td>
                    </tr>
                <?php } ?>

                </tbody>
            </table>



        <div class="buttons" style="text-align: right">
            <span class="right"><a onclick="ajanlatTorol()" class="no-button"><?php echo $button_torol_metese; ?></a></span>
            <span class="right"><a onclick="ajanlatkeroBezar()" class="no-button"><?php echo $button_tovabb; ?></a></span>
            <span class="right"><a onclick="ajanlatotKerek()" class="button"><?php echo $button_arajanlat_metese; ?></a></span>
        </div>

    </div>
</div>

<script>

    function ajanlatotKerek() {

        var datatombatad= $('#arajanlatok input[type=\'text\']');

        $.ajax({
            url: 'index.php?route=module/arajanlat/ajanlatotKerek',
            type: 'post',
            data : datatombatad,
            dataType: 'json',
            success: function(json) {
                debugger;
                ajanlatkeroBezar();
                $('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/'+json['template']+'/image/close.png" alt="" class="close" /></div>');
                notaficationBezar();
                $('.success').slideDown('slow');

                $('html, body').animate({ scrollTop: 0 }, '1200');
                $('#header_ajanlatkeres').hide();

            },
            error: function(html){
                debugger;

            },
            complete: function (html) {
                debugger;

            }
        });
    }

    function ajanlatMennyiseg(product_key,para) {

        mennyiseg = para.value;
        $.ajax({
            url: 'index.php?route=module/arajanlat/mennyiseg',
            type: 'post',
            data : 'product_key='+product_key+'&mennyiseg='+mennyiseg,
            dataType: 'html',
            success: function(html) {
                if ($("#arajanlatok").length > 0) {
                    $("#arajanlatok").remove();
                    $("#container").before(html);

                    $("#arajanlatok").css("height",$('body').height());
                    $('.ajanlat_form').css('top',$(window).scrollTop());


                } else {
                    $("#container").before(html);
                    $("#arajanlatok").css("height",$('body').height());
                    $('.ajanlat_form').css('top',$(window).scrollTop());

                }


            },
            error: function(html){
            },
            complete: function (html) {
            }
        });


    }

    function ajanlatTermekTorol(product_key) {
        $.ajax({
            url: 'index.php?route=module/arajanlat/delete',
            type: 'post',
            data : 'product_key='+product_key,
            dataType: 'html',
            success: function(html) {
                debugger;
                if (html.length != 0) {
                    if ($("#arajanlatok").length > 0) {
                        $("#arajanlatok").remove();
                        $("#container").before(html);

                        $("#arajanlatok").css("height", $('body').height());
                        $('.ajanlat_form').css('top', $(window).scrollTop());

                    } else {
                        $("#container").before(html);
                        $("#arajanlatok").css("height", $('body').height());

                    }
                } else {
                    $('#header_ajanlatkeres').hide();
                    $("#arajanlatok").slideUp('slow',function(){
                        $("#arajanlatok").remove();
                    });

                }



            },
            error: function(html){
            },

            complete: function (html) {

            }
        });
    }

    function ajanlatOptionTorol(product_key,product_option_id,szin_meret_szukseges) {
        $.ajax({
            url: 'index.php?route=module/arajanlat/deleteOption',
            type: 'post',
            data : 'product_key='+product_key+'&option_id='+product_option_id+'&szin_meret_szukseges='+szin_meret_szukseges,
            dataType: 'html',
            success: function(html) {
                debugger;
                if (html.length != 0) {
                    if ($("#arajanlatok").length > 0) {
                        $("#arajanlatok").remove();
                        $("#container").before(html);

                        $("#arajanlatok").css("height", $('body').height());
                        $('.ajanlat_form').css('top', $(window).scrollTop());

                    } else {
                        $("#container").before(html);
                        $("#arajanlatok").css("height", $('body').height());

                    }
                } else {
                    $('#header_ajanlatkeres').hide();
                    $("#arajanlatok").slideUp('slow',function(){
                        $("#arajanlatok").remove();
                    });

                }



            },
            error: function(html){
                debugger;

            },

            complete: function (html) {
                debugger;

            }
        });
    }

    function ajanlatTorol() {
        $.ajax({
            url: 'index.php?route=module/arajanlat/deleteAll',
            type: 'post',
            dataType: 'html',
            success: function(html) {
                $('#header_ajanlatkeres').hide();
                $("#arajanlatok").slideUp('slow',function(){
                    $("#arajanlatok").remove();
                });

            },
            error: function(html){
            },

            complete: function (html) {

            }
        });
    }

    $('.close_arajanlat_icon').bind("click",function(){
        $("#arajanlatok").slideUp('slow',function(){
            $("#arajanlatok").remove();
            $('.success, .warning, .attention, .information, .error').remove();
            $('#lepel').remove();

        });
    });

    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            ajanlatkeroBezar();
        }
    });

    $(document).bind("click",function(e) {
        if (e.target.id == 'arajanlatok') {
            ajanlatkeroBezar();
        }

    });

    function ajanlatkeroBezar() {
        $('.success, .warning, .attention, .information, .error').remove();

        $("#arajanlatok").hide('slow',function(){
            $("#arajanlatok").remove();
        });
    }
</script>

