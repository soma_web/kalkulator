<?php if ($display_type){ ?>
<div class="banner">
	<a id="show-price-alert">
    <?php if(isset($image)) { ?>
        <img src="<?php echo $image; ?>" class="banner" alt="<?php echo $heading_title; ?>"></a>
    <?php } ?>
</div>	
<?php } else { ?>
<div class="box special_reminder">
    <div class="box-content ">
	    <a id="show-price-alert">
            <?php if(isset($image)) { ?>
                <img src="<?php echo $image; ?>">
            <?php } ?>
            <div class="box-heading"><?php echo $heading_title; ?></div>
        </a>
    </div>
</div>
<?php } ?>

<div id="price-alert" class="modal-box wrapper"><div class="inside grey">
	<span class="modal-close left" id="close-price-alert"></span>
    <?php if ( file_exists("catalog/view/theme/<?php echo $theme_name; ?>/image/modal_box/price_barcode.png") ) {?>
	    <img src="catalog/view/theme/<?php echo $theme_name; ?>/image/modal_box/price_barcode.png" class="right" />
    <?php } else {?>
        <img src="catalog/view/theme/default/image/modal_box/price_barcode.png" class="right" />
    <?php }?>
	<h3 class="small"><?php echo $modal_title; ?></h3>
	<p><?php echo $text_info; ?></p>
	<div class="hr">&nbsp;</div>
	<form>
		<div id="info-message"></div>
		<fieldset class="<?php echo $fieldset_class; ?>">
			<label><span class="required">*</span><?php echo $entry_name; ?></label><br/>
			<input type="text" class="text" name="pa_fullname" value="<?php echo $pa_fullname; ?>" />
		</fieldset>
		<fieldset class="<?php echo $fieldset_class; ?>">
			<label><span class="required">*</span><?php echo $entry_email; ?></label><br/>
			<input type="text" class="text" name="pa_email" value="<?php echo $pa_email; ?>" />
		</fieldset>
		<fieldset>
			<label><span class="required">*</span><?php echo $entry_desired_price; ?></label><br/>
			<input type="text" class="text" name="pa_desired_price" value="" />
		</fieldset>
		<input type="hidden" name="pa_product_id" value="<?php echo $pa_product_id; ?>" />
		
		<fieldset>
			<span class="button white save" id="set-price-alert"><span><?php echo $text_set_alert; ?></span></span>
		</fieldset>
	</form>
</div></div>

<script type="text/javascript">
$('#show-price-alert').bind('click', function(){
	$('#price-alert').slideDown();
});

$('#close-price-alert').bind('click', function(){
	$('#price-alert').slideUp();
});

$('#price-alert input[name=\'pa_desired_price\']').bind('keyup', function(){
	$(this).val( $(this).val().replace(/,/g, ".") );
});


$('#set-price-alert').bind('click', function(){
	$.ajax({
		type: 'POST',
		url: 'index.php?route=module/price_alert/save_alert',
		data: $('#price-alert input[type=\'text\'], #price-alert input[type=\'hidden\'] '),
		dataType: 'json',
		success: function(data){
            debugger;
			$('#info-message').html('');
			
			if (data.error) {
				$('#info-message').html('<div class="warning">' + data.error + '</div>');
			}
			
			if (data.success) {
				$('#info-message').html('<div class="success">' + data.success + '</div>');
				
				<?php if (!$customer_logged) { ?>	
					$('#price-alert input[name=\'pa_fullname\']').val('');
					$('#price-alert input[name=\'pa_email\']').val('');
				<?php } ?>
				$('#price-alert input[name=\'pa_desired_price\']').val('');
				setTimeout(function() {
					$('#info-message').html('');
					$("#price-alert").hide('blind', {}, 500)
				}, 3000);
			}
		}
		
	});
});
</script>