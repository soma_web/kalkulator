<?php if (isset($product['rating'])) {?>
    <?php $rating_pic = $product['rating']; ?>
    <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/stars-'.$rating_pic.'.png')) {
        $rating = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/stars-'.$rating_pic.'.png';
    } else {
        $rating = DIR_TEMPLATE_IMAGE.'default/image/stars-'.$rating_pic.'.png';
    } ?>
    <img src="<? echo $rating ?>" />
<?php } ?>
