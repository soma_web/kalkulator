<?php
$_['uj']          = array(
    'type'              => 'checkbox',
    'name'              => 'uj',
    'default'           => "1",
    'heading'           => 'Új'
);

$_['pardarab']          = array(
    'type'              => 'checkbox',
    'name'              => 'pardarab',
    'default'           => "1",
    'heading'           => 'Pár darab'
);

$_['szazalek']          = array(
    'type'              => 'checkbox',
    'name'              => 'szazalek',
    'default'           => "1",
    'heading'           => 'Százalék'
);

$_['mennyisegi']          = array(
    'type'              => 'checkbox',
    'name'              => 'mennyisegi',
    'default'           => "1",
    'heading'           => 'Mennyiségi'
);

?>