<?php if(!(isset($beallitas['null_off']) && $beallitas['null_off'] == 1)) { ?>
    <?php $product['not_null'] = true; ?>
<?php } ?>
<?php if($product['not_null'] && empty($product['ar_tiltasa'])) { ?>

    <?php if (!empty($product['price_netto'])) { ?>
        <div class="price_netto <?php echo !$product['mutat_arat'] ? 'price_nemkell' : ''?>" >
            <?php if (isset($beallitas['netto']) && ($product['price_netto'] || $product['special_netto']) ) { ?>
                <?php $netto = $this->language->get('text_netto'); ?>
                <span><?php echo $netto?>: </span>
            <?php } ?>

            <?php if (!$product['special']) { ?>
                <span class="price-tax"><?php echo $product['price_netto']; ?></span>
            <?php } else { ?>
                <?php if (empty($beallitas['eredeti_ar_tiltas'])) { ?>
                    <span class="price-old"><?php echo $product['price_netto']; ?></span>
                <?php } ?>
                <span class="price-tax"><?php echo $product['special_netto']; ?></span>
            <?php } ?>

        </div>
    <?php } ?>
<?php } ?>
