<?php if(!isset($beallitas['cikkszam'])) { ?>
    <?php if(isset($product['model']) && $product['model']) { ?>
        <div class="name modell">
            <?php if (isset($beallitas['model_kotojel'])) { ?>
                <a href="<?php echo $product['href']; ?>"><span class="model-color"><?php echo $product['model']."&nbsp;-&nbsp;"; ?></span></a>
            <?php } else if (isset($beallitas['cikkszam_felirat'])) { ?>
                <p><?php echo $text_cikkszam; ?></p><a href="<?php echo $product['href']; ?>"><span class="model-color"><?php echo $product['model']; ?></span></a>
            <?php } else { ?>
                <a href="<?php echo $product['href']; ?>"><span class="model-color"><?php echo $product['model']; ?></span></a>
            <?php } ?>
        </div>
    <?php } ?>
<?php } else { ?>

    <?php if(isset($product['cikkszam']) && $product['cikkszam']) { ?>
        <div class="name">
            <?php if (isset($beallitas['model_kotojel'])) { ?>
                <a href="<?php echo $product['href']; ?>"><span class="model-color"><?php echo $product['cikkszam']."&nbsp;-&nbsp;"; ?></span></a>
            <?php } else if (isset($beallitas['cikkszam_felirat'])) { ?>
                 <p><?php echo $text_cikkszam; ?></p><a href="<?php echo $product['href']; ?>"><span class="model-color"><?php echo $product['cikkszam']; ?></span></a>
            <?php } else { ?>
                <a href="<?php echo $product['href']; ?>"><span class="model-color"><?php echo $product['cikkszam']; ?></span></a>
            <?php } ?>
        </div>
    <?php } ?>
<?php } ?>