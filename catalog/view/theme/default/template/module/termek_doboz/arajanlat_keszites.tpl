<?php if (isset($this->session->data['feltolto']) && $this->session->data['feltolto'] == 1) { ?>
    <div class="cart" style="margin-right: 10px; vertical-align: bottom;">
        <?php $csomagolasi_mennyiseg = $product['csomagolasi_mennyiseg'] > 1 ? $product['csomagolasi_mennyiseg'] : 1; ?>
        <input type="button" class="arajanlat_keszites" value="<?php echo $button_arajanlat_keszites; ?>"
               onclick="arajanlatKeszites('<?php echo $product['product_id']; ?>',
                   $('#quantity_<?php echo $product['product_id']; ?>').val()*<? echo $csomagolasi_mennyiseg?>,
                   '<?php echo $this->config->get('config_template')?>',
                   '<?php echo $module_name.$product['product_id']?>',
                   false)" />
    </div>
<?php }  ?>