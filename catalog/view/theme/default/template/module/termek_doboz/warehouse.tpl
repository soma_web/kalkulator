<?php if ($product['warehouse']) { ?>

    <div class="warehouse-button">
        <a class="various " href="#warehouse_list<?php echo $product['product_id']?>">
            <?php if (isset($text_raktar)) { ?>
                <?php echo $text_raktar; ?>
            <?php } ?>
        </a>
    </div>


    <div id="warehouse_list<?php echo $product['product_id']?>" class="warehouse " style="display: none; width: 560px; height: 210px; background-color: white;">
        <span><?php echo $text_raktar;?>
            <?php if ($product['warehouses_out_of_stock']) { ?>
                <?php echo ' - '. $text_stock_in .' '. $product['stock_in_date']; ?>
            <?php } ?>
        </span>
        <ul>
            <?php foreach($product['warehouse'] as $warehouse) { ?>
                <li class="<?php echo $warehouse['class']?>">
                    <?php echo $warehouse['postcode']?> <?php echo $warehouse['city']?>, <?php echo $warehouse['address']?><span><?php echo $warehouse['viszontelado_quantity']; ?></span>
                </li>
            <?php } ?>
        </ul>
    </div>



    <?php } ?>