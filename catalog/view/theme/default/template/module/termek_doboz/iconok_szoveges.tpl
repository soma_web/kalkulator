<div class="product_icons">

    <?php if (isset($beallitas['uj']) && $beallitas['uj']) { ?>
        <?php if ($product['uj']) { ?>
            <div class="uj_szoveges">
                <? $text_uj = $this->language->get('text_uj');
                if (isset($text_uj)) {
                    echo $text_uj;
                } ?>
            </div>
        <?php } ?>
    <?php } ?>

    <?php if (isset($beallitas['pardarab']) && $beallitas['pardarab']) { ?>
        <?php if ($product['pardarab']) { ?>
            <div class="pardarab_szoveges">
                <? $text_pardarab = $this->language->get('text_pardarab');
                if (isset($text_pardarab) && $text_pardarab) {
                    echo $text_pardarab;
                } ?>
            </div>
        <?php } ?>
    <?php } ?>

    <?php if (isset($beallitas['szazalek']) && $beallitas['szazalek']) { ?>
        <?php if ($product['special']) { ?>
            <div class="akcio_szoveges">
                <? $text_akcios = $this->language->get('text_akcios');
                if (isset($text_akcios)) {
                    echo $text_akcios;
                } ?>
            </div>
        <?php } ?>
    <?php } ?>

    <?php if (isset($beallitas['mennyisegi']) && $beallitas['mennyisegi']) { ?>
        <?php if ($product['discounts']) { ?>
            <div class="mennyisegi_szoveges">
                <? $text_mennyisegi = $this->language->get('text_mennyisegi');
                if (isset($text_mennyisegi)) {
                    echo $text_mennyisegi;
                } ?>
            </div>
        <?php } ?>
    <?php } ?>

</div>