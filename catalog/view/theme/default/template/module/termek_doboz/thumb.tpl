<?php if ($product['thumb']) { ?>
    <?php $lista = strpos($module_name, "lista"); ?>
    <?php if(isset($beallitas['hover_div']) && $beallitas['hover_div'] == 1) { ?>
        <script>
            function myFunction<?php echo $product['product_id']; ?>(e) {
                parentOffset = $("#view-image-id<?php echo $product['product_id']; ?>").offset();

                $("#view-image-box-id<?php echo $product['product_id']; ?>").show();

                moveX = e.pageY - parentOffset.top + 60;
                moveY = e.pageX - parentOffset.left + 30;

                pageWidth = $('body').outerWidth();
                if(e.pageX+730 > pageWidth) {
                   moveY -= ((e.pageX+730)-pageWidth);
                }

                $("#view-image-box-id<?php echo $product['product_id']; ?>").css({
                    top:  (moveX) + "px",
                    left: (moveY) + "px"
                });
            }

            function clearCoor<?php echo $product['product_id']; ?>() {
                $("#view-image-box-id<?php echo $product['product_id']; ?>").hide();
            }
        </script>

        <div class="view-image-box" id="view-image-box-id<?php echo $product['product_id']  ?>" >
            <div class="view-image-box-inner">
                <div class="view-image-box-left">
                    <div class="view-image-box-thumb">
                        <img src="<?php echo $product['thumb_product']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
                    </div>
                </div>
                <div class="view-image-box-right">
                    <div class="view-image-box-name">
                        <?php echo $product['name']; ?>
                    </div>
                    <div class="view-image-box-description">
                        <?php echo $product['description']; ?>
                    </div>
                    <div class="view-image-box-price">
                        <?php echo $product['price']; ?>
                    </div>
                </div>
            </div>
        </div>

        <style>
            .view-image-box {
                display:none;
                width: 700px;
                background-color: #fff;
                position: absolute;
                z-index: 1000;
                padding-top: 10px;
                padding-bottom: 10px;
                border: 1px solid #ddd;
                color: #686868;
                box-shadow: 3px 3px 10px #666;
                -moz-box-shadow: 3px 3px 5px #666;
            }

            .view-image-box-inner {
                display: table;
                width: 100%;
            }

            .view-image-box-left, view-image-box-right {
                display: table-cell;
                vertical-align: top;
            }

            .view-image-box-left {
                width: 50%;
            }

            .view-image-box-right {
                text-align: right;
                padding: 10px;
            }

            .view-image-box-thumb {
                padding-left: 10px;
            }

            .view-image-box-description {
                width: 100%;
                text-align: justify;
                line-height: 20px;

                display: block; /* Fallback for non-webkit */
                display: -webkit-box;
                max-width: 350px;
                height: 12*20*10; /* Fallback for non-webkit */
                margin: 0 auto;
                font-size: 12px;
                -webkit-line-clamp: 10;
                -webkit-box-orient: vertical;
                overflow: hidden;
                text-overflow: ellipsis;

            }

            .view-image-box-name {
                text-align: center;
                padding: 10px;
                font-weight: bold;
                font-size: 14px;
            }

            .view-image-box-price {
                margin-top: 20px;
                text-align: center;
                padding: 10px;
                font-weight: bold;
                font-size: 16px;
                color: #C20000;
            }

        </style>
    <?php } ?>
    <div class="image">
        <?php if(!empty($product['href'])) { ?>
        <a class="view-image"
        <?php if(isset($beallitas['hover_div']) && $beallitas['hover_div'] == 1) { ?>
           onmousemove="myFunction<?php echo $product['product_id']; ?>(event)" onmouseout="clearCoor<?php echo $product['product_id']; ?>()"
        <?php } ?>
           id="view-image-id<?php echo $product['product_id']; ?>" style="display: block" href="<?php echo $product['href']; ?>">
            <?php } ?>
            <?php if ($lista !== false) { ?>
            <img id="<?php echo $module_name.$product['product_id']  ?>" src="<?php echo $product['thumb']; ?>" style="max-width: <?php echo $product['thumb_list_width']; ?>px" alt="<?php echo $product['name']; ?>" />
        <?php } else { ?>
            <img id="<?php echo $module_name.$product['product_id']  ?>" src="<?php echo $product['thumb_racs']; ?>" style="max-width: <?php echo $product['thumb_grid_width']; ?>px" alt="<?php echo $product['name']; ?>" />
        <?php } ?>
            <?php if(!empty($product['href'])) { ?>
        </a>

    <?php }?>
    </div>
<?php } ?>