<?php if ( isset($szinek) && $szinek && count($szinek) > 1 ){ ?>

    <style>
        .szinlatszik {
            display: none;
        }
    </style>

    <div class="box">
        <div class="box-heading reszek harmadik-filter"><?php echo $heading_title_szin; ?><img style="display: none"  src="catalog/view/theme/default/image/responsive/pos_r_opened.png"></div>
            <div class="box-content">

                <?php if ( isset($beallitasok['szin']['szinek']) && $beallitasok['szin']['szinek'] == 0  ) { ?>
                    <?php foreach ($szinek as $szin) { ?>
                        <?php $szinek_db = str_replace(' ','',$szin['szinkod']);?>
                        <?php $szinek_db = str_replace('#','',$szinek_db);?>
                        <?php $szinek_db = explode('/',$szinek_db); ?>

                        <?php if (!empty($szin['name'])) { ?>
                            <?php $check = ''?>
                            <?php if (in_array($szin['option_szin_id'], $filter_szin)) { ?>
                                <?php $check = 'checked="checked"'?>
                            <?php } ?>

                            <?php $box_filter_class = ''; ?>



                            <div class="box-filter box-filter-szin <?php echo $box_filter_class;?>" title="<?php echo $szin['name']?>">
                                <input type="checkbox" value="<?php echo $szin['option_szin_id']; ?>" id="szin<?php echo $szin['option_szin_id']; ?>" name="check" <?php echo $check?>/>
                                <label style=""  class="" for="szin<?php echo $szin['option_szin_id']; ?>"><?php echo $szin['name']?></label>
                            </div>
                        <?php } ?>
                    <?php } ?>


                <?php } elseif ( isset($beallitasok['szin']['szinek']) && ($beallitasok['szin']['szinek'] == 1 ||  $beallitasok['szin']['szinek'] == 2 || $beallitasok['szin']['szinek'] == 3) ) { ?>

                    <?php foreach ($szin_groups as $szin_group) { ?>
                        <?php $szinek_db = str_replace(' ','',$szin_group['szinkod']);?>
                        <?php $szinek_db = str_replace('#','',$szinek_db);?>
                        <?php $szinek_db = explode('/',$szinek_db); ?>

                        <?php if (!empty($szin_group['name'])) { ?>
                            <?php $check = ''?>
                            <?php if (in_array($szin_group['option_szin_group_id'], $filter_szin)) { ?>
                                <?php $check = 'checked="checked"'?>
                            <?php } ?>

                            <?php $class = ''; ?>
                            <?php if ($beallitasok['szin']['szinek'] == 3) { ?>
                                <?php $class = 'szincsoport_egyedul';?>
                            <?php } ?>
                            <div style="" class="szincsoportok <?php echo $class;?>">
                                <div class="box-filter box-filter-szin szincsoport" title="<?php echo $szin_group['name']?>"
                                     style="cursor: pointer;"
                                     onclick="csoportosKijeloles('szin_csoport<?php echo $szin_group['option_szin_group_id']?>',this)">
                                        <span class="szincsoport_neve"> <?php echo $szin_group['name']?></span>
                                </div>
                                <br>
                                <?php foreach($szinek as $szin) { ?>
                                    <?php $szinek_db = str_replace(' ','',$szin['szinkod']);?>
                                    <?php $szinek_db = str_replace('#','',$szinek_db);?>
                                    <?php $szinek_db = explode('/',$szinek_db); ?>

                                    <?php $check = ''?>
                                    <?php if (in_array($szin['option_szin_id'], $filter_szin)) { ?>
                                        <?php $check = 'checked="checked"'?>
                                    <?php } ?>

                                    <?php if ( isset($szin['groups']) && $szin['groups'] ) { ?>
                                        <?php $szinlatszik = ''; ?>

                                        <?php if($beallitasok['szin']['szinek'] == 3 ) { ?>
                                            <?php $szinlatszik = 'szinlatszik'; ?>
                                        <?php } ?>
                                        <?php foreach($szin['groups'] as $group) { ?>
                                            <?php if($group['option_szin_group_id'] == $szin_group['option_szin_group_id']) {?>
                                                <div class="box-filter box-filter-szin <?php echo $szinlatszik;?>" title="<?php echo $szin['name']?>">

                                                    <input type="checkbox"  class="szin_csoport<?php echo $szin_group['option_szin_group_id']?> csoportban_<?php echo $szin['option_szin_id']?> szin_input"
                                                            szin="csoportban_<?php echo $szin['option_szin_id']?>"
                                                            value="<?php echo $szin['option_szin_id']; ?>"
                                                            id="szin_<?php echo $szin_group['option_szin_group_id'].'_'. $szin['option_szin_id']; ?>" name="check" <?php echo $check?>
                                                            onclick="szinekClick('csoportban_<?php echo $szin['option_szin_id']?>',this)"/>

                                                    <label style=""  class="" for="szin_<?php echo $szin_group['option_szin_group_id'].'_'.$szin['option_szin_id']; ?>"><?php echo $szin['name']?></label>

                                                </div>
                                            <?php } ?>
                                        <?php } ?>

                                    <?php } ?>
                                <?php } ?>
                            </div>

                        <?php } ?>
                    <?php } ?>
                <?php } ?>

            </div>

    </div>

    <script>


        $(document).ready(function(){
            $('.szincsoportok').each(function(para,para1){

                valasztva = true;
                if ($(this).children().find('input').length > 0) {
                    $(this).children().find('input').each(function(){
                        if ( $(this).prop('checked') == false) {
                            valasztva = false;
                        }
                    });
                } else {
                    valasztva = false;
                }
                if (valasztva) {
                    $(para1).find('.szincsoport').addClass('aktiv_csoport');
                }
            });
        });

        function csoportosKijeloles(para,aktualis){
            var minden_kijelolve = true;
            $('.'+para).each(function(){
                if ( $(this).attr('checked') == 'checked' ) {
                } else {
                    minden_kijelolve = false;
                }
            });
            if (minden_kijelolve) {
                $('.'+para).removeAttr('checked');
                $(aktualis).removeClass('aktiv_csoport');

            } else {
                $('.'+para).attr('checked',"checked");
                $(aktualis).addClass('aktiv_csoport');
            }

            $('.'+para).each(function(){

                var szinkod=this.attributes.szin.value;
                if ($('.aktiv_csoport').parent().find('input[szin='+szinkod+']').length < 1) {
                    szinekClick(szinkod,this);
                }
            });
            szuroIndit('szin_csoport');

        }

        function szinekClick(para,aktualis) {
            if ($(aktualis).prop('checked')) {
                $('.'+para).attr('checked',"checked");
            } else {
                $('.'+para).removeAttr('checked');
            }

        }

    </script>

<?php }?>