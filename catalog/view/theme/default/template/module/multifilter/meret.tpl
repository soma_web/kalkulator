
<?php if ( isset($meretek) && $meretek && count($meretek) > 1 ){ ?>

    <style>
        .box-filter-meret {
            width: 33px;
            height: 28px;
            background: #f5f5f5;


            -webkit-box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.1);
            -moz-box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.1);
            box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.1);
            position: relative;
            display: inline-block;
            margin-bottom: 5px;
            margin: 0 4px 10px 0;
            text-align: center;
        }

        .box-filter-meret label {
            cursor: pointer;
            position: absolute;
            width: 100%;
            height: 100%;
            left: 0px;
            top: 0px;
            font-size: 12px;
        }

        .box-filter-meret label span {
            position: relative;
            top: 3px;
            display: block;

        }


        .box-filter-meret input[type=checkbox]:checked + .css-label {
            color: #fff;
            background: red;
        }

        .box-filter-meret input {
            opacity: 0;
        }

    </style>


    <div class="box">
        <div class="box-heading reszek harmadik-filter"><?php echo $heading_title_meret; ?><img style="display: none"  src="catalog/view/theme/default/image/responsive/pos_r_opened.png"></div>
        <?php if ($beallitasok['meret']['teljes'] != 1) { ?>
            <div class="box-content">
                    <?php foreach ($meretek as $meret) { ?>
                        <?php if (!empty($meret['name'])) { ?>

                            <?php $l = explode(' ',$meret['name']) ?>
                            <?php $style = count($l) > 1 ? "line-height: 9px; font-size: 90%;" : ""?>

                            <?php if (in_array($meret['option_id'], $filter_meret)) { ?>
                                <div class="box-filter box-filter-meret">
                                    <input class="css-checkbox" type="checkbox" value="<?php echo $meret['option_id']; ?>" id="meret<?php echo $meret['option_id']; ?>"  checked="checked"/>
                                    <label class="css-label mac-style" for="meret<?php echo $meret['option_id']; ?>"><span style="<?php echo $style?>"><?php echo $meret['name']; ?></span></label>
                                </div>
                            <?php }  else {?>
                                <div class="box-filter box-filter-meret">
                                    <input class="css-checkbox" type="checkbox" value="<?php echo $meret['option_id']; ?>" id="meret<?php echo $meret['option_id']; ?>" />
                                    <label class="css-label mac-style" for="meret<?php echo $meret['option_id']; ?>"><span style="<?php echo $style?>"><?php echo $meret['name']; ?></span></label>
                                </div>
                            <?php } ?>

                        <?php } ?>
                    <?php } ?>
            </div>
        <?php } else {?>
            <div class="box-content">
                <ul class="box-filter box-filter-meret">
                    <?php foreach ($meretek_teljes as $meret_teljes) { ?>
                        <?php if (in_array($meret_teljes, $meretek)) { ?>
                            <?php if (in_array($meret_teljes['option_id'], $filter_meret)) { ?>
                                <li><input class="css-checkbox" type="checkbox" value="<?php echo $meret_teljes['option_id']; ?>" id="meret<?php echo $meret_teljes['option_id']; ?>"  checked="checked"/>
                                    <label class="css-label mac-style" for="meret<?php echo $meret_teljes['option_id']; ?>"><?php echo $meret_teljes['name']; ?></label>
                                </li>
                            <?php } else {?>
                                <li><input class="css-checkbox" type="checkbox" value="<?php echo $meret_teljes['option_id']; ?>" id="meret<?php echo $meret_teljes['option_id']; ?>" />
                                    <label class="css-label mac-style" for="meret<?php echo $meret_teljes['option_id']; ?>"><?php echo $meret_teljes['name']; ?></label>
                                </li>
                            <?php } ?>
                        <?php } else {?>
                            <li><input class="css-checkbox" type="checkbox" value="<?php echo $meret_teljes['option_id']; ?>" id="meret<?php echo $meret_teljes['option_id']; ?>" disabled="disabled"/>
                                <label class="css-label mac-style halvany" for="meret<?php echo $meret_teljes['option_id']; ?>" ><?php echo $meret_teljes['name']; ?></label>
                            </li>
                        <?php } ?>

                    <?php } ?>
                </ul>
            </div>
        <?php } ?>
    </div>
<?php }?>