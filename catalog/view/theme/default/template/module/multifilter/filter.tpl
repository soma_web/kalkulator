<?php if( (isset($filter_selects) && $filter_selects) || $beallitasok['szuro']['teljes'] == 1){ ?>
        <div class="box">
            <div class="box-heading reszek harmadik-filter"><?php echo $heading_title_filter; ?><img  style="display: none"  src="catalog/view/theme/default/image/responsive/pos_r_opened.png"></div>
            <?php if ($beallitasok['szuro']['teljes'] != 1) { ?>
                <div id="mf-bevaltasipont" class="box-content">
                    <ul class="box-filter box-filter-uzlet">
                        <?php foreach ($filter_selects as $filter_select) { ?>
                            <?php if (!empty($filter_select['name'])) { ?>
                                <?php if (in_array($filter_select['filter_select_id'], $filter_uzletek)) { ?>
                                    <li><input class="css-checkbox" type="checkbox" value="<?php echo $filter_select['filter_select_id']; ?>" id="filter_select<?php echo $filter_select['filter_select_id']; ?>"  checked="checked"/>
                                        <label class="css-label mac-style" for="filter_select<?php echo $filter_select['filter_select_id']; ?>"><?php echo $filter_select['name']; ?></label>
                                    </li>
                                <?php }  else {?>
                                    <li><input class="css-checkbox" type="checkbox" value="<?php echo $filter_select['filter_select_id']; ?>" id="filter_select<?php echo $filter_select['filter_select_id']; ?>" />
                                        <label class="css-label mac-style" for="filter_select<?php echo $filter_select['filter_select_id']; ?>"><?php echo $filter_select['name']; ?></label>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
        <?php } else {?>
                <div id="mf-bevaltasipont" class="box-content">
                    <ul class="box-filter box-filter-uzlet">
                        <?php foreach ($filter_selects_teljes as $filter_select_teljes) { ?>
                            <?php if (in_array($filter_select_teljes, $filter_selects)) { ?>
                                <?php if (in_array($filter_select_teljes['filter_select_id'], $filter_uzletek)) { ?>
                                    <li><input class="css-checkbox" type="checkbox" value="<?php echo $filter_select_teljes['filter_select_id']; ?>" id="filter_select<?php echo $filter_select_teljes['filter_select_id']; ?>"  checked="checked"/>
                                        <label class="css-label mac-style" for="filter_select<?php echo $filter_select_teljes['filter_select_id']; ?>"><?php echo $filter_select_teljes['name']; ?></label>
                                    </li>
                                <?php } else {?>
                                    <li><input class="css-checkbox" type="checkbox" value="<?php echo $filter_select_teljes['filter_select_id']; ?>" id="filter_select<?php echo $filter_select_teljes['filter_select_id']; ?>" />
                                        <label class="css-label mac-style" for="filter_select<?php echo $filter_select_teljes['filter_select_id']; ?>"><?php echo $filter_select_teljes['name']; ?></label>
                                    </li>
                                <?php } ?>
                            <?php } else {?>
                                <li><input class="css-checkbox" type="checkbox" value="<?php echo $filter_select_teljes['filter_select_id']; ?>" id="filter_select<?php echo $filter_select_teljes['filter_select_id']; ?>" disabled="disabled"/>
                                    <label class="css-label mac-style halvany" for="filter_select<?php echo $filter_select_teljes['filter_select_id']; ?>" ><?php echo $filter_select_teljes['name']; ?></label>
                                </li>
                            <?php } ?>

                        <?php } ?>
                    </ul>
                </div>
        <?php } ?>
        </div>
    <?php }?>