<?php if ( (isset($filter_ar_tol_ig) && $filter_ar_tol_ig[0] < $filter_ar_tol_ig[1]) || !isset($filter_ar_tol_ig) ) { ?>
    <div class="box tol-ig" style="min-height: 60px; max-width: 100%; font-size: 0;">

        <div class="box-heading reszek masodik-filter"><?php echo $heading_title_arszuro; ?>
            <img style="display: none"  src="catalog/view/theme/default/image/responsive/pos_r_opened.png">
        </div>
        <div class="price_tol_ig">
            <span style="font-size: 12px; display: inline-block; width: 50%;" id="arvalaszt_tol"
                    value= "<?php echo ( isset($filter_ar_tol_ig[2]) && !empty($filter_ar_tol_ig[2]) ) ? $filter_ar_tol_ig[2] :
                        (isset($filter_ar_tol_ig[0]) ? $filter_ar_tol_ig[0] : $beallitasok['ar_szures_tol_ig']['tol']) ?>" >

                <span style="color: #fc0808;">
                    <?php echo ( isset($filter_ar_tol_ig[2]) && !empty($filter_ar_tol_ig[2]) ) ?
                        $this->currency->format($filter_ar_tol_ig[2]) :
                        $this->currency->format((isset($filter_ar_tol_ig[0]) ? $filter_ar_tol_ig[0] : $beallitasok['ar_szures_tol_ig']['tol'])) ?>
                </span>
            </span>

             <span style="font-size: 12px;  display: inline-block; width: 50%; text-align: right;"  id="arvalaszt_ig"
                        value= "<?php echo ( isset($filter_ar_tol_ig[3]) && !empty($filter_ar_tol_ig[3]) ) ? $filter_ar_tol_ig[3] :
                            (isset($filter_ar_tol_ig[1]) ? $filter_ar_tol_ig[1] : $beallitasok['ar_szures_tol_ig']['ig']) ?>" >

                  <span style="color: #fc0808;">
                    <?php echo ( isset($filter_ar_tol_ig[3]) && !empty($filter_ar_tol_ig[3]) ) ?
                          $this->currency->format($filter_ar_tol_ig[3]) :
                          $this->currency->format((isset($filter_ar_tol_ig[1]) ? $filter_ar_tol_ig[1] : $beallitasok['ar_szures_tol_ig']['ig'])) ?>
                  </span>
            </span>


            <div id="mf-artol" class="box-content box-filter_artol box-filter" style="font-size: 12px; width: 94%; margin: auto; margin-top: 10px;">


                <div id="slider-range" class="slider"></div>


                <div id="ar_tol_ig_also_felso_hatar" style="margin-top: 10px; width: 107%; position: relative; left: -7px;">
                    <span value= "<?php echo ( isset($filter_ar_tol_ig[0]) && !empty($filter_ar_tol_ig[0]) ) ? $filter_ar_tol_ig[0] : $beallitasok['ar_szures_tol_ig']['tol']?>"
                          style="float: left">
                        <?php echo $this->currency->format(isset($filter_ar_tol_ig[0]) ? $filter_ar_tol_ig[0] : $beallitasok['ar_szures_tol_ig']['tol'])?>
                    </span>
                    <span value= "<?php echo (isset($filter_ar_tol_ig[1]) && !empty($filter_ar_tol_ig[1]) ) ? $filter_ar_tol_ig[1] : $beallitasok['ar_szures_tol_ig']['ig']?>"
                          style="float: right">
                        <?php echo $this->currency->format(isset($filter_ar_tol_ig[1]) ? $filter_ar_tol_ig[1] : $beallitasok['ar_szures_tol_ig']['ig'])?>
                    </span>
                </div>

            </div>
        </div>

    </div>
    <style>
        .ui-widget-header {
            background: #df190a;
            background: -webkit-linear-gradient(top, #df190a, #ddd);
            background: -moz-linear-gradient(top, #df190a, #ddd);
            background: linear-gradient(top, #df190a, #ddd);
            -webkit-box-shadow: inset 0 2px 4px rgba(0,0,0,0.1);
            -moz-box-shadow: inset 0 2px 4px rgba(0,0,0,0.1);
            box-shadow: inset 0 2px 4px rgba(0,0,0,0.1);
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
            border: 1px solid #aaa;
            height: 4px;
            background: linear-gradient(#4366ce, #104f9c);
            background: #fff;

        }
    </style>

<?php } ?>



