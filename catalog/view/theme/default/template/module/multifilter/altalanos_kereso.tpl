<div class="box">
    <div id="mf-search" class="box-content search">
        <div id="search">
            <?php if ($filter_name) { ?>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" />
            <?php } else { ?>
                <input type="text" name="filter_name" value="<?php echo $text_search; ?>" onclick="this.value = '';" onkeydown="this.style.color = '#000000';" />
            <?php } ?>
            <div class="button-search"></div>
            <input type="hidden" name="keres_autocomplate" value="<?php echo $tihs->config->get('megjelenit_keres_autocomplete')?>">

        </div>
    </div>
</div>