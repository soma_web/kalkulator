<?php if(isset($categories_checkbox) && $categories_checkbox){ ?>
    <div class="box">
        <div class="box-heading reszek elso-filter"><?php echo $heading_title_category; ?><img style="display: none;" src="catalog/view/theme/default/image/responsive/pos_r_opened.png"></div>

        <div id="mf-category-checkbox" class="box-content">
            <ul class="box-filter box-filter-category">
                <?php foreach ($categories_checkbox as $category) { ?>
                    <?php if (in_array($category['category_id'], $filter_category)) { ?>
                        <li><input class="css-checkbox" type="checkbox" value="<?php echo $category['category_id']; ?>" id="category<?php echo $category['category_id']; ?>" checked="checked" />
                        <label class="css-label mac-style" for="category<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></label>

                    <?php } else { ?>
                        <li><input class="css-checkbox" type="checkbox" value="<?php echo $category['category_id']; ?>" id="category<?php echo $category['category_id']; ?>" />
                        <label class="css-label mac-style" for="category<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></label>
                    <?php } ?>

                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
<?php } ?>


