<div id="cart_elhelyezkedes">
  <div class="heading">
      <a><img style="position:relative; top: 0px;" src="image/data/cart.png"></a>
      <a><?php echo $heading_title; ?></a>
      <a><?php echo $text_items; ?></a>
      <a><span></span></a>

  </div>


  <div class="content" style="padding-bottom: 20px; padding-right: 10px;">




      <?php if ($products ) { ?>


          <div class="mini-cart-info">
              <table>
                  <?php foreach ($products as $product) { ?>
                      <tr>
                          <td class="image"><?php if ($product['thumb']) { ?>
                                  <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
                              <?php } ?></td>
                          <td class="name"><b><a style="text-decoration: underline" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></b>

                          <?php if (!empty($product['quantity']) && false) { ?>
                              <td class="quantity">x&nbsp;<?php echo $product['quantity']; ?></td>
                          <?php } else {?>
                              <td class="quantity">&nbsp;</td>
                          <?php } ?>
                          <td class="total"><b><?php echo number_format($product['total'],0,",",".").$penznem; ?></b></td>
                          <td class="remove"><img src="catalog/view/theme/default/image/remove-small.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>"
                                                  onclick="$('#cart_elhelyezkedes').load('index.php?route=module/cart_elhelyezkedes&remove=<?php echo $product['key']; ?> #cart_elhelyezkedes > *'); location = location " />

                          </td>
                      </tr>


                      <?php if ($product['kiemelesek']) { ?>
                          <tr>
                              <td colspan= 3>
                                  <table class="form no-border" style="margin: 0 0 0 20px">
                                      <?php foreach($product['kiemelesek'] as $kiemelesek_key=>$kiemelesek ) { ?>

                                      <tr>
                                          <td colspan="2"><?php echo $kiemelesek['idoszak']." ".$kiemelesek['idoszak_mennyiseg']?></td>
                                          <td>
                                             <table class="form no-border" style="margin-bottom: 0">
                                                     <?php if (isset($kiemelesek['elhelyezkedes_id']) && $kiemelesek['elhelyezkedes_id'] > 0) { ?>
                                                         <tr>
                                                             <td><?php echo $kiemelesek['elhelyezkedes_neve']?></td>
                                                             <td class="td_right"><?php echo number_format($kiemelesek['elhelyezkedes_netto'],0,",",".").$penznem?></td>
                                                             <td class="td_right"><?php echo number_format($kiemelesek['elhelyezkedes_brutto'],0,",",".").$penznem?></td>
                                                         </tr>
                                                     <?php } ?>

                                                     <?php if (isset($kiemelesek['elhelyezkedes_alcsoport_id']) && $kiemelesek['elhelyezkedes_alcsoport_id'] > 0) { ?>
                                                         <tr>
                                                             <td><?php echo $kiemelesek['elhelyezkedes_alcsoport_neve']?></td>
                                                             <td class="td_right"><?php echo number_format($kiemelesek['elhelyezkedes_alcsoport_netto'],0,",",".").$penznem?></td>
                                                             <td class="td_right"><?php echo number_format($kiemelesek['elhelyezkedes_alcsoport_brutto'],0,",",".").$penznem?></td>
                                                         </tr>
                                                     <?php } ?>

                                                     <?php if (isset($kiemelesek['kiemelesek_id']) && $kiemelesek['kiemelesek_id'] > 0) { ?>
                                                         <tr>
                                                             <td><?php echo $kiemelesek['kiemelesek_neve']?></td>
                                                             <td class="td_right"><?php echo number_format($kiemelesek['kiemelesek_netto'],0,",",".").$penznem?></td>
                                                             <td class="td_right"><?php echo number_format($kiemelesek['kiemelesek_brutto'],0,",",".").$penznem?></td>
                                                         </tr>
                                                     <?php } ?>
                                             </table>
                                          </td>
                                          <td class="remove"><img src="catalog/view/theme/default/image/remove-small.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>"
                                                                  onclick="$('#cart_elhelyezkedes').load('index.php?route=module/cart_elhelyezkedes&remove=<?php echo $product['key'].",".$kiemelesek_key; ?> #cart_elhelyezkedes > *'); location = location "/>

                                      </tr>
                                      <?php } ?>


                                  </table>
                              </td>
                          </tr>

                      <?php } ?>

                  <?php } ?>
                  <tr>
                      <td colspan=3><b><?php echo $total_title?></b></td>
                      <td class="td_right" ><b><?php echo number_format($totals,0,",",".").$penznem?></b></td>
                  </tr>

              </table>
          </div>



         <div class="checkout"><a class="button" href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a></div>
      <?php } else { ?>
          <div class="empty"><?php echo $text_empty; ?></div>
      <?php } ?>





  </div>
</div>

<script>
    $('#cart_elhelyezkedes > .heading a').on('click', function() {
        $('#cart_elhelyezkedes').addClass('active');

        $('#cart_elhelyezkedes').load('index.php?route=module/cart_elhelyezkedes #cart_elhelyezkedes > *');

        $('#cart_elhelyezkedes').on('mouseleave', function() {
            $(this).removeClass('active');
        });
    });




</script>