<?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/multifilter/category_php_fv.tpl')) {
    $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/multifilter/category_php_fv.tpl';
} else {
    $arkiir = DIR_TEMPLATE.'default/template/module/multifilter/category_php_fv.tpl';
} ?>
<?php require_once($arkiir); ?>



<div class="multicategory" style="">

    <?php if ($heading_latszik){ ?>
        <div class="box-heading"><?php echo $heading_title; ?></div>
    <?php }?>

    <?php if ($beallitasok) { ?>
        <?php foreach($beallitasok as $beallitas) {?>
            <?php if ($beallitas['value'] == 1 ) {?>

                <?php if ($beallitas['tpl_name']) { ?>
                    <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/multifilter/'.$beallitas['tpl_name'])) {
                        $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/multifilter/'.$beallitas['tpl_name'];
                    } elseif( file_exists(DIR_TEMPLATE . 'default/template/module/multifilter/'.$beallitas['tpl_name'])) {
                        $arkiir = DIR_TEMPLATE.'default/template/module/multifilter/'.$beallitas['tpl_name'];
                    } ?>
                    <?php if (!empty($arkiir)) include($arkiir); ?>
                <?php } ?>
            <?php }?>
        <?php }?>
    <?php } ?>


</div>



<script>

    var szuro = '';

    $('#search input[name=\'filter_name\']').bind('keydown', function(e) {
        if (e.keyCode == 13) {
            url = $('base').attr('href') + 'index.php?route=product/search';

            var filter_name = $('input[name=\'filter_name\']').attr('value');

            if (filter_name) {
                url += '&filter_name=' + encodeURIComponent(filter_name);
            }

            location = url;
        }
    });


    <?php if (!empty($scroll_position)) { ?>
        $(window).scrollTop("<?php echo $scroll_position ?>");
    <?php } ?>
</script>

<script>
    $("[data-slider]").each(function () {
        var input = $(this);
        $("<span>")
            .addClass("output")
            .insertAfter($(this));
    })
        .bind("slider:ready slider:changed", function (event, data) {
            if ( data['el'].attr('id') == "filter_arszazalek-slider" ) {
                $(this)
                    .nextAll(".output:first")
                    .html("<?php echo $entry_filter_select_artol ?>"+data.value.toFixed(0) +"%");
            } else {
                $(this)
                    .nextAll(".output:first")
                    .html("<?php echo $entry_filter_select_artol ?>"+data.value.toFixed(0) +"Ft");
            }
        });

    $( "#slider-range" ).slider({
        range: true,

        min: <?php echo  isset($filter_ar_tol_ig[0]) ? $filter_ar_tol_ig[0] : $beallitasok['ar_szures_tol_ig']['tol']?>,
        max: <?php echo  isset($filter_ar_tol_ig[1]) ? $filter_ar_tol_ig[1] : $beallitasok['ar_szures_tol_ig']['ig']?>,

        values: [ <?php echo isset($filter_ar_tol_ig[2]) ? $filter_ar_tol_ig[2] : $beallitasok['ar_szures_tol_ig']['tol']?>,
            <?php echo isset($filter_ar_tol_ig[3]) ? $filter_ar_tol_ig[3] : $beallitasok['ar_szures_tol_ig']['ig']?> ],

        slide: function( event, ui ) {
            $( "#arvalaszt_tol" ).html(number_format(ui.values[0])+" Ft" );
            $( "#arvalaszt_tol" ).val(ui.values[0] );
            $( "#arvalaszt_tol" ).attr("value",ui.values[0] );

            $( "#arvalaszt_ig" ).html(number_format(ui.values[1])+ " Ft" );
            $( "#arvalaszt_ig" ).val(ui.values[1] );
            $( "#arvalaszt_ig" ).attr("value",ui.values[1] );
        },
        change: function(event, ui) {
            $('#slider-range').trigger('click');
        }
    });



    $('.box-filter-category input').bind("click",function() {
        if (this.checked) {
            szuro = (this.value);
        } else {
            szuro = "";

        }
    });

    $('.box-filter-sub-category').bind("click",function() {
        szuro = "<?php echo isset($_REQUEST['path']) ? $_REQUEST['path']: "";?>";
    });

    $('.box-filter input[type=\'checkbox\'], .box-content input[type=\'button\'], .box-filter_artol, .box-filter_arig').bind('click', function() {
        szuroIndit(this.id)
    });

    function szuroIndit(para_id) {
        filter = [];
        filter_categorys = [];
        filter_varos = [];
        filter_uzlet = [];
        filter_artol = 0;
        filter_arig = 0;
        filter_arszazalek = 0;

        filter_tulajdonsagok = [];
        filter_tulajdonsagok_szukit = [];
        filter_tulajdonsagok_bovit = [];
        filter_valasztek = [];
        filter_gyarto = [];
        filter_meret = [];
        filter_szin = [];
        filter_szin_group = [];
        filter_raktaron = [];
        filter_ar_tol_ig = [];
        csuszka_min = '';
        csuszka_max = '';
        esemeny_kivalto = para_id;

        if (typeof (csuszka_tol_ig) == "undefined") csuszka_tol_ig = [];


        <?php if ($beallitasok['kategoria_sub']['value'] == 1) { ?>
        szuro = "<?php echo isset($this->request->request['path'] ) ? $this->request->request['path'] : "";?>";
        <?php } ?>

        <?php if ($beallitasok['kategoria_normal']['value'] == 1) { ?>
        szuro = "<?php echo isset($this->request->request['path'] ) ? $this->request->request['path'] : "";?>";

        <?php } ?>

        if (esemeny_kivalto != 'szurok_torlese') {
            $('.box-filter-category input[type=\'checkbox\']:checked').each(function(element) {
                filter.push(this.value);
            });

            $('.box-filter-varos input[type=\'checkbox\']:checked').each(function(element) {
                filter_varos.push(this.value);
            });

            $('.box-filter-uzlet input[type=\'checkbox\']:checked').each(function(element) {
                filter_uzlet.push(this.value);
            });


            $('.box-filter-tulajdonsagok.szukit input[type=\'checkbox\']:checked').each(function(element) {
                filter_tulajdonsagok_szukit.push(this.value);
            });
            $('.box-filter-tulajdonsagok.bovit input[type=\'checkbox\']:checked').each(function(element) {
                filter_tulajdonsagok_bovit.push(this.value);
            });

            $('.box-filter-tulajdonsagok input[type=\'checkbox\']:checked').each(function(element) {
                filter_tulajdonsagok.push(this.value);
            });
            $('.box-filter-valasztek input[type=\'checkbox\']:checked').each(function(element) {
                filter_valasztek.push(this.value);
            });

            $('.box-filter-gyarto input[type=\'checkbox\']:checked').each(function(element) {
                filter_gyarto.push(this.value);
            });

            $('.box-filter-meret input[type=\'checkbox\']:checked').each(function(element) {
                filter_meret.push(this.value);
            });

            $('.box-filter-szin input[type=\'checkbox\']:checked').each(function(element) {
                filter_szin.push(this.value);
            });

            $('.szincsoportok_valasztva').each(function(element) {
                if ($(this).val() == "1") {
                    filter_szin_group.push($(this).attr("szincsoport_id"));
                }
            });

            $('.box-filter-raktar input[type=\'checkbox\']:checked').each(function(element) {
                filter_raktaron.push(this.value);
            });

            $('#filter_artol').each(function(element) {
                filter_artol = this.value;
            });

            $('#filter_arig').each(function(element) {
                filter_arig = this.value;
            });

            $('#filter_arszazalek').each(function(element) {
                filter_arszazalek = this.value;
            });

            if (esemeny_kivalto == 'mf-artol') {
                if ($("#arvalaszt_tol").attr("value")) {
                    filter_ar_tol_ig.push($("#arvalaszt_tol").attr("value"));
                } else {
                    filter_ar_tol_ig.push("<?php echo $beallitasok['ar_szures_tol_ig']['tol']?>");
                }
                if ($("#arvalaszt_ig").attr("value")) {
                    filter_ar_tol_ig.push($("#arvalaszt_ig").attr("value"));
                } else {
                    filter_ar_tol_ig.push("<?php echo $beallitasok['ar_szures_tol_ig']['ig']?>");
                }
            }


            if (para_id.substr(0,10) == "attr-tolig") {
                csuszka_tol_ig = [];

                if ($('#'+para_id).find('.csuszka_tol').attr('value')*1 > $('#'+para_id).find('.csuszka_tol').attr('minimum')*1 ||
                    $('#'+para_id).find('.csuszka_ig').attr('value')*1 < $('#'+para_id).find('.csuszka_ig').attr('maximum')*1 ) {

                    csuszka_tol_ig.push('tol_'+$('#'+para_id).find('.csuszka_tol').attr('attribute_id')+'_'+$('#'+para_id).find('.csuszka_tol').attr('value'));
                    csuszka_tol_ig.push('ig_'+$('#'+para_id).find('.csuszka_ig').attr('attribute_id')+'_'+$('#'+para_id).find('.csuszka_ig').attr('value'));
                    csuszka_min = $('#'+para_id).find('.csuszka_tol').attr('minimum');
                    csuszka_max = $('#'+para_id).find('.csuszka_ig').attr('maximum');
                }
            }
        }

        var sort = $(".sort select").prop("selectedIndex");
        var limit = $(".limit select").find('option:selected').html();
        var order = "";

        <?php if ($this->config->get('config_review_status')) { ?>
        if (sort == 0) {
            sort    = "p.sort_order"
            order   = "ASC";
        } else if (sort == 1) {
            sort = "pd.name";
            order   = "ASC";
        } else if (sort == 2) {
            sort = "pd.name";
            order   = "DESC";
        } else if (sort == 3) {
            sort = "p.price";
            order   = "ASC";
        } else if (sort == 4) {
            sort = "p.price";
            order   = "DESC";
        } else if (sort == 5) {
            sort = "rating";
            order   = "DESC";
        } else if (sort == 6) {
            sort = "rating";
            order   = "ASC";
        } else if (sort == 7) {
            sort = "p.model";
            order   = "ASC";
        } else if (sort == 8) {
            sort = "p.model";
            order   = "DESC";
        }

        <?php } else { ?>
        if (sort == 0) {
            sort = "p.sort_order"
            order   = "ASC";
        } else if (sort == 1) {
            sort = "pd.name";
            order   = "ASC";
        } else if (sort == 2) {
            sort = "pd.name";
            order   = "DESC";
        } else if (sort == 3) {
            sort = "p.price";
            order   = "ASC";
        } else if (sort == 4) {
            sort = "p.price";
            order   = "DESC";
        } else if (sort == 5) {
            sort = "p.model";
            order   = "ASC";
        } else if (sort == 6) {
            sort = "p.model";
            order   = "DESC";
        }
        <?php } ?>

        scroll_position = $(window).scrollTop();

        <?php if ($ajaxos_kuldes) { ?>
        if (filter.length > 0 ||
            filter_varos.length > 0  ||
            filter_tulajdonsagok.length > 0  ||
            filter_tulajdonsagok_szukit.length > 0  ||
            filter_tulajdonsagok_bovit.length > 0  ||
            filter_valasztek.length > 0  ||
            filter_gyarto.length > 0  ||
            filter_meret.length > 0  ||
            filter_szin.length > 0  ||
            filter_raktaron.length > 0  ||
            filter_uzlet.length > 0  ||
            filter_arig.length > 0  ||
            filter_artol.length > 0  ||
            filter_arszazalek.length > 0  ||
            filter_ar_tol_ig.length > 0  ||
            csuszka_tol_ig.length > 0  ||
            szuro.length > 0  ||
            csuszka_min.length > 0  ||
            csuszka_max.length > 0  ) {

            $.ajax({
                url: 'index.php?route=product/multifilter',
                type: 'post',
                dataType: 'html',
                data: 'filter=' + filter.join(',') +
                '&filter_varos=' + filter_varos.join(',')  +
                '&filter_tulajdonsagok=' + filter_tulajdonsagok.join(',')  +
                '&filter_tulajdonsagok_szukit=' + filter_tulajdonsagok_szukit.join(',')  +
                '&filter_tulajdonsagok_bovit=' + filter_tulajdonsagok_bovit.join(',')  +
                '&filter_valasztek=' + filter_valasztek.join(',')  +
                '&filter_gyarto=' + filter_gyarto.join(',')  +
                '&filter_meret=' + filter_meret.join(',')  +
                '&filter_szin=' + filter_szin.join(',')  +
                '&filter_raktaron=' + filter_raktaron.join(',')  +
                '&filter_uzlet=' + filter_uzlet.join(',') +
                '&filter_arig='+ filter_arig  +
                '&filter_artol='+ filter_artol  +
                '&filter_arszazalek='+ filter_arszazalek +
                '&filter_ar_tol_ig='+ filter_ar_tol_ig +
                '&filter_tulajdonsag_csuszka='+ csuszka_tol_ig +
                '&path='+szuro +
                '&sort='+ sort +
                '&order='+ order +
                '&limit='+ limit +
                '&esemeny_kivalto='+ esemeny_kivalto +
                '&tulajdonsag_csuszka_min='+ csuszka_min +
                '&tulajdonsag_csuszka_max='+ csuszka_max +
                '&scroll_position='+ scroll_position +
                '&filter_szin_group='+ filter_szin_group +
                '&multiszuro=1&ajax_hivas=1',

                beforeSend: function() {
                    $(".szuro_valaszt").remove();

                    $('.kozepe').after('<div class="szuro_valaszt" style="position: fixed; top: 0px; left: 0px; width: 100%; height: 100%; z-index: 99999999999; background: #dddddd; opacity: 0.2; display: none"></div>');
                    $('.szuro_valaszt').slideDown('fast');
                },

                complete: function(html) {
                    $('.szuro_valaszt').slideUp('fast',function(){
                        $(".szuro_valaszt").remove();

                    });
                },

                success: function(html) {
                    html = '<div id="notification"></div>' + html;
                    $('.kozepe').html(html);

                    if (typeof(addthis) != "undefined") {
                        var addthis_config = {"data_track_addressbar":true};

                        addthis.toolbox('.addthis_default_style');
                    }

                    $("#menu a").removeClass("active");

                    $("[data-slider]").each(function () {
                        var el = $(this.parentElement).find('.slider');
                        if ( el.attr('id') == "filter_arszazalek-slider" ) {
                            $(this)
                                .nextAll(".output:first")
                                .html("<?php echo $entry_filter_select_artol ?>"+(this.value*1).toFixed(0) +"%");
                        } else {
                            $(this)
                                .nextAll(".output:first")
                                .html("<?php echo $entry_filter_select_artol ?>"+(this.value*1).toFixed(0) +"Ft");
                        }
                    });
                    mulitifilterScriptInitialize();

                },
                error: function(e) {
                }
            });
            if ( $("#menu-kategoriak").length > 0 ) {
                $.ajax({
                    url: 'index.php?route=module/icon_doboz',
                    type: 'post',
                    dataType: 'html',
                    data: 'filter=' + filter.join(',') +
                    '&filter_varos=' + filter_varos.join(',')  +
                    '&filter_tulajdonsagok=' + filter_tulajdonsagok.join(',')  +
                    '&filter_tulajdonsagok_szukit=' + filter_tulajdonsagok_szukit.join(',')  +
                    '&filter_tulajdonsagok_bovit=' + filter_tulajdonsagok_bovit.join(',')  +
                    '&filter_valasztek=' + filter_valasztek.join(',')  +
                    '&filter_gyarto=' + filter_gyarto.join(',')  +
                    '&filter_meret=' + filter_meret.join(',')  +
                    '&filter_szin=' + filter_szin.join(',')  +
                    '&filter_raktaron=' + filter_raktaron.join(',')  +
                    '&filter_uzlet=' + filter_uzlet.join(',') +
                    '&filter_arig='+ filter_arig  +
                    '&filter_artol='+ filter_artol  +
                    '&filter_arszazalek='+ filter_arszazalek +
                    '&filter_ar_tol_ig='+ filter_ar_tol_ig +
                    '&filter_tulajdonsag_csuszka='+ csuszka_tol_ig +
                    '&path='+szuro +
                    '&sort='+ sort +
                    '&order='+ order +
                    '&limit='+ limit +
                    '&esemeny_kivalto='+ esemeny_kivalto +
                    '&tulajdonsag_csuszka_min='+ csuszka_min +
                    '&tulajdonsag_csuszka_max='+ csuszka_max +
                    '&scroll_position='+ scroll_position +
                    '&filter_szin_group='+ filter_szin_group +
                    '&multiszuro=1&ajax_hivas=1',

                    success: function(html) {
                        $("#menu-kategoriak").html(html);
                    },
                    error: function(e) {
                    }
                });
            }
            $('a[attr_id]').removeClass('active');
            if (filter_tulajdonsagok) {
                $(filter_tulajdonsagok).each(function(e,val){
                    $('a[attr_id='+val+']').addClass('active');

                });
            }

        } else {
            location = '<?php echo $action; ?>&sort='+ sort + '&order='+ order + '&limit='+ limit
        }


        <?php } else { ?>
        location = '<?php echo $action; ?>&filter=' + filter.join(',') +
        '&filter_varos=' + filter_varos.join(',')  +
        '&filter_tulajdonsagok=' + filter_tulajdonsagok.join(',')  +
        '&filter_tulajdonsagok_szukit=' + filter_tulajdonsagok_szukit.join(',')  +
        '&filter_tulajdonsagok_bovit=' + filter_tulajdonsagok_bovit.join(',')  +
        '&filter_valasztek=' + filter_valasztek.join(',')  +
        '&filter_gyarto=' + filter_gyarto.join(',')  +
        '&filter_meret=' + filter_meret.join(',')  +
        '&filter_szin=' + filter_szin.join(',')  +
        '&filter_raktaron=' + filter_raktaron.join(',')  +
        '&filter_uzlet=' + filter_uzlet.join(',') +
        '&filter_arig='+ filter_arig  +
        '&filter_artol='+ filter_artol  +
        '&filter_arszazalek='+ filter_arszazalek +
        '&filter_ar_tol_ig='+ filter_ar_tol_ig +
        '&filter_tulajdonsag_csuszka='+ csuszka_tol_ig +
        '&path='+szuro +
        '&sort='+ sort +
        '&order='+ order +
        '&limit='+ limit +
        '&esemeny_kivalto='+ esemeny_kivalto +
        '&tulajdonsag_csuszka_min='+ csuszka_min +
        '&tulajdonsag_csuszka_max='+ csuszka_max +
        '&scroll_position='+ scroll_position +
        '&filter_szin_group='+ filter_szin_group +
        '&multiszuro=1&ajax_hivas=0';
        <?php } ?>

    }

</script>
