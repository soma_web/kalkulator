
<!-- Legnépszerűbb termékek-->

<? if($products) { ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/jquery.mCustomScrollbar.css" />
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.mCustomScrollbar.js"></script>

<?php $template=$this->config->get("config_template");?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $template?>/stylesheet/carousel22.css" />
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.jcarousel.min22.js"></script>


<div id="bestseller_fejlec" class="box">
    <?php if ($heading_latszik) { ?>
        <div class="box-heading"><span><?php echo $heading_title; ?></span></div>
    <?php } ?>

   <?php if ($show == 1) {?>
       <div id="mcs5_container_nepszeru">
       <div class="customScrollBox">
       <div class="horWrapper">
       <div class="container">
       <div class="content">
   <?php } elseif ($show == 2) {?>
       <div id="carousel">
       <ul class="jcarousel22-skin-opencart">
   <?php } ?>

   <?php if ($show != 2) { ?>
       <div class="box-product">
   <?php } ?>

   <?php $bal = true; ?>
   <?php  foreach ($products as $product) { ?>
       <?php if ($show == 2) { ?>
           <li style="position: relative">
           <div class=" box-product">
       <? } ?>

       <?php if ($product['utalvany'] != 0) { ?>
           <?php $class="szazalek";?>
       <?php } else {?>
           <?php $class="";?>
       <?php } ?>


       <div class="termek_tabban <?php echo $class; ?>">
           <? if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
               $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
           } else {
               $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
           } ?>
           <?php include($arkiir); ?>
       </div>
       <?php if ($show == 2) { ?>
           </div>
           </li>
       <?php } ?>
   <?php } ?>

   <?php if ($show != 2) { ?>
       </div>
   <?php } ?>
   <?php if ($show == 1) {?>
       </div>
       </div>
       </div>
           
       <div class="dragger_container">
           <div class="dragger"></div>
       </div>

       </div>
       </div>
   <?php } elseif ($show == 2) {?>
       </ul>
       </div>
   <?php } ?>
</div>


<?php if ($show == 1) {?>
    <noscript>
        <style type="text/css">
            #mcs5_container_nepszeru .customScrollBox{overflow:auto;}
            #mcs5_container_nepszeru .dragger_container{display:none;}
        </style>
    </noscript>

    <script>
        $(window).load(function() {
            mCustomScrollbars_nepszeru();
        });

        function mCustomScrollbars_nepszeru(){
            $("#mcs5_container_nepszeru").mCustomScrollbar("horizontal",500,"easeOutCirc",1,"fixed","yes","no",20);
        }

        /* function to fix the -10000 pixel limit of jquery.animate */
        $.fx.prototype.cur = function(){
            if ( this.elem[this.prop] != null && (!this.elem.style || this.elem.style[this.prop] == null) ) {
                return this.elem[ this.prop ];
            }
            var r = parseFloat( jQuery.css( this.elem, this.prop ) );
            return typeof r == 'undefined' ? 0 : r;
        }

        /* function to load new content dynamically */
        function LoadNewContent(id,file){
            $("#"+id+" .customScrollBox .content").load(file,function(){
                mCustomScrollbars();
            });
        }
    </script>
<?php } elseif ($show == 2) {?>
    <script type="text/javascript">
        $('#carousel ul').jcarousel22({
            vertical: false,
            visible: 4,
            scroll: 3
        });
    </script>
<?php } ?>
<?php } ?>


