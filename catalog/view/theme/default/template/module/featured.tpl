<!-- Kiemelt termékek-->
<?php if ($products) { ?>
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/jquery.mCustomScrollbar.css" />
    <script type="text/javascript" src="catalog/view/javascript/jquery/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery/jquery.mCustomScrollbar.js"></script>

    <?php $template=$this->config->get("config_template");?>
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $template?>/stylesheet/carousel22.css" />
    <script type="text/javascript" src="catalog/view/javascript/jquery/jquery.jcarousel.min22.js"></script>

    <div id="featured_fejlec" class="box">
        <?php if ($heading_latszik) { ?>
            <div class="box-heading">
                <span><?php echo $heading_title; ?></span>
                <?php if ($osszes_gomb && $show != "1" && $featured_osszes_nmb != 0 &&  (count($products) > $featured_osszes_nmb)) { ?>

                    <div class="featured_all"><span><?php echo $osszes_gomb_title; ?></span></div>
                <?php } ?>
            </div>
        <?php } ?>

        <div class="outer-box-product">
            <div class="box-product <?php if(isset($show) && $show == "1") { echo "bxslider_featured"; } ?>" >
                <?php  foreach ($products as $product) { ?>
                    <?php if ($product['utalvany'] != 0) { ?>
                        <?php $class=" utalvany";?>
                    <?php } else { ?>
                        <?php $class="";?>
                    <?php } ?>

                        <div class="termek_tabban <?php echo $class; ?>">
                            <?php if (isset($product['kiemelt_tpl']) && $product['kiemelt_tpl']) { ?>
                                <?php include($product['kiemelt_tpl']); ?>
                            <?php } else { ?>
                                <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/arkiiratas.tpl')) {
                                    $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/module/arkiiratas.tpl';
                                } else {
                                    $arkiir = DIR_TEMPLATE.'default/template/module/arkiiratas.tpl';
                                } ?>
                                <?php include($arkiir); ?>
                            <?php } ?>
                        </div>

                <?php } ?>
            </div>
        </div>
    </div>
    <?php if(isset($show) && $show == "1") { ?>
    <script type="text/javascript" src="catalog/view/javascript/bxslider/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/bxslider/jquery.bxslider.min.js"></script>
    <link rel="stylesheet" type="text/css" href="catalog/view/javascript/bxslider/jquery.bxslider.css" />
    <script type="text/javascript">
        $(document).ready(function(){
            $('.bxslider_featured').bxSlider({
                minSlides: <?php echo isset($megjelenit) ? $megjelenit : 3?>,
                maxSlides: <?php echo isset($megjelenit) ? $megjelenit : 3?>,
                slideWidth: 260,
                slideMargin: 10,
                autoHidePager: true, /*  Eltűnteti a nyilakat, ha minSlides értékénél kevesebb kép jelenik meg. */
                auto: <?php echo isset($auto) ? "true" : "false" ?>,
                pause: <?php echo isset($speed) ? $speed : 1500?>,
                autoHover: true,
                infiniteLoop: <?php echo isset($infinity) ? "true" : "false" ?>,
                responsive: true,
                hideControlOnEnd: true,
                adaptiveHeight: false
            });
        });
    </script>
    <?php } ?>


    <?php if(isset($osszes_gomb) && $osszes_gomb && $featured_osszes_nmb != 0 && $show != "1") { ?>
        <script>
            if($(window).width() > 1165) {
                prod_box_nmb = <?php echo $featured_osszes_nmb; ?>;
            } else if ($(window).width() < 1165 && $(window).width() > 890) {
                prod_box_nmb = <?php echo $featured_osszes_nmb-1; ?>;
            } else if ($(window).width() < 890) {
                prod_box_nmb = <?php echo $featured_osszes_nmb-2; ?>;
            }

            $("#featured_fejlec .box-product > *").css("display", "none");
            $("#featured_fejlec .box-product > div:nth-child(-n+"+prod_box_nmb+")").css("display","inline-block");

            (function($,sr){
                var debounce = function (func, threshold, execAsap) {
                    var timeout;

                    return function debounced () {
                        var obj = this, args = arguments;
                        function delayed () {
                            if (!execAsap)
                                func.apply(obj, args);
                            timeout = null;
                        };

                        if (timeout)
                            clearTimeout(timeout);
                        else if (execAsap)
                            func.apply(obj, args);
                        // milyen gyorsan töltsön be a függvény
                        timeout = setTimeout(delayed, threshold || 500);
                    };
                }
                // smartresize
                jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

            })(jQuery,'smartresize');

            $(window).smartresize(function(){
                if($(window).width() > 1165) {
                    prod_box_nmb = <?php echo $featured_osszes_nmb; ?>;
                } else if ($(window).width() < 1165 && $(window).width() > 890) {
                    prod_box_nmb = <?php echo $featured_osszes_nmb-1; ?>;
                } else if ($(window).width() < 890) {
                    prod_box_nmb = <?php echo $featured_osszes_nmb-2; ?>;
                }

                $("#featured_fejlec .box-product > *").css("display", "none");
                $("#featured_fejlec .box-product > div:nth-child(-n+"+prod_box_nmb+")").css("display","inline-block");

            });

            $(".featured_all").bind("click",function(){
                if($(window).width() > 1200) {
                    prod_box_nmb = <?php echo $featured_osszes_nmb; ?>;
                } else if ($(window).width() < 1200 && $(window).width() > 890) {
                    prod_box_nmb = <?php echo $featured_osszes_nmb-1; ?>;
                } else if ($(window).width() < 890) {
                    prod_box_nmb = <?php echo $featured_osszes_nmb-2; ?>;
                }
                if($("#featured_fejlec .box-product > div:nth-child(n+"+(prod_box_nmb+1)+")").css("display") == "none") {
                $("#featured_fejlec .box-product > *").css("display", "inline-block");
                } else {
                    $("#featured_fejlec .box-product > *").css("display", "none");
                    $("#featured_fejlec .box-product > div:nth-child(-n+"+prod_box_nmb+")").css("display","inline-block");
                }
            });
        </script>
    <?php } ?>
<?php } ?>