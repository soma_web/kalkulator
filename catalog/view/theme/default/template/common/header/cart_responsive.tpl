    <div class="slidecontent" >
        <div id="cart_responsive">
            <div class="heading">
                <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/image/cart.png')) {
                    $icon = DIR_TEMPLATE_IMAGE. $this->config->get('config_template') . '/image/cart.png';
                } else if (file_exists(DIR_TEMPLATE_IMAGE.'default/image/cart.png')) {
                    $icon = DIR_TEMPLATE_IMAGE.'default/image/cart.png';
                } else if (file_exists('image/cart.png')) {
                    $icon = 'image/cart.png';
                } ?>
                <a class="kep_icon"><img style="position:relative; top: 0px;" src="<?php echo (isset($icon) ? $icon : ''); ?>"></a>
                <span><a class="responsive_darab"><?php echo $text_items_responsive; ?></a></span>

            </div>
            <div class="content_responsive" style="padding-bottom: 20px; padding-right: 10px;">
            </div>

        </div>

    </div>