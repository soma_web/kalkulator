<div id="termekek">
    <?php if(!(isset($template['fokategoria']) && $template['fokategoria'])) { ?>
        <a href="<?php echo $product; ?>"><?php echo $text_termekek; ?></a>
    <?php } ?>
<?php if ($categories) { ?>
    <div class="termek_kategoria">
        <ul>
            <?php foreach ($categories as $category) { ?>
                <?php if (!empty($category['top'])) { ?>
                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                        <?php if (isset($category['children']) && $category['children']) { ?>
                        <?php if(isset($template['responsive']) && $template['responsive']) { ?>
                            <img class="alkategoria_fokategoriaja_resp" alt="" style="display: inline;  margin-left: 10px" src="catalog/view/theme/default/image/desc.png">
                        <?php } ?>
                            <div>
                                <?php for ($i = 0; $i < count($category['children']);) { ?>
                                    <ul>
                                        <?php $j = $i + ceil(count($category['children']) / ( $category['column'] > 0 ? $category['column'] : 1) ); ?>
                                        <?php for (; $i < $j; $i++) { ?>
                                            <?php if (isset($category['children'][$i])) { ?>
                                                <li><a href="<?php echo $category['children'][$i]['href']; ?>"> - <?php echo $category['children'][$i]['name']; ?></a></li>
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </li>
                <?php } ?>
            <?php } ?>
        </ul>
    </div>
<?php } ?>
</div>

<?php if(isset($template['responsive']) && $template['responsive']) { ?>
    <script>
        $(".alkategoria_fokategoriaja_resp").bind("click",function(){
            if ($(this).next().css("display") == "none") {
                $(this).next().slideDown("slow");
                $(this).attr("src","catalog/view/theme/default/image/asc.png");
            } else {
                $(this).next().slideUp("slow");
                $(this).attr("src","catalog/view/theme/default/image/desc.png");

            }
        });

    </script>
<?php } ?>
