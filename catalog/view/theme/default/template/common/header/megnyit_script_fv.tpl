<script>
    function megnyit(para) {


        /*Szétvágom a paramétert, hogy csak az utolsó classt használjam -
        * így a régi kódot használó webáruháznál nem lesz gond */

        var classes = para.split('.');
        var lastClass = classes[classes.length-1];

        para = para.substr(0,1) == '.' ? para.substr(1) : para;
        /* Csak az icon_menu classjában megadott elemet nyitja meg */
        if ( $('.'+para).css('display') == 'none' ){

            var szulo_class = $('.'+lastClass).parent().parent().attr('class');
            var szulo = $('.'+szulo_class);
            closeAllMenus(szulo);


            $('.'+para).css("display","none");
            $('#cart_responsive').removeClass('active');
            $('.'+para).css("display","block");
        } else{
            $('.'+para).css("display","none");
        }

    }

    function menusorokatZar() {
        $('.sor_4 .oszlop_4_1 .sor_4_0_0').css("display","none");
        $('.sor_4 .oszlop_4_1 .sor_4_1_0').css("display","none");
        $('.sor_4 .oszlop_4_2 .sor_4_2_0').css("display","none");
        $('.sor_4 .oszlop_4_3 .sor_4_3_0').css("display","none");
    }

    /** Minden lenyíló menüt becsuk **/

    function closeAllMenus(parentDiv) {

        var hossz = parentDiv.children('div').length;
        for(var i = 0; i < hossz; i++) {
            parentDiv.children('div').eq(i).children('div:eq(1)').css("display", "none");
        }
    }
</script>


