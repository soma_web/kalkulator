<?php

    /*
     *  Alkategóriákat kiolvasó rekurzív függvény
     */

    function children2($para, $cat_detail, $img_icon, $cols){
        if (isset($para['children']) && $para['children']) { ?>
            <ul class="mega-menu-sub-ul">
                <?php ($cols != 0) ? $columns = intval(count($para['children']) / $cols) : $columns=0; ?>
                <?php $extra_col = count($para['children']) - ($cols  * $columns); ?>
                <?php $current_col = 1; ?>
                <?php $temp_col = $columns; ?>
                <?php $i = 1; $j = 1; ?>
                <?php foreach ($para['children'] as $child) { ?>
                    <li class="mega-menu-subcategory">
                        <?php if (isset($child['image_icon']) && $child['image_icon'] && isset($img_icon) && $img_icon) { ?>
                            <div class="mega-menu-cat-icon">
                                <a href="<?php echo $child['href']; ?>" >
                                    <img src="<?php echo $child['image_icon']?>">
                                </a>
                            </div>
                        <?php } ?>
                        <div class="category">
                            <div class="category-name">
                                <a href="<?php echo $child['href']; ?>" >
                                    <?php echo $child['name']?>
                                    <?php if (isset($child['description']) && $child['description'] && isset($cat_detail) && $cat_detail) { ?>
                                        <div class="cat-detail">
                                            <?php echo html_entity_decode($child['description'], ENT_QUOTES, 'UTF-8'); ?>
                                        </div>
                                    <?php } ?>
                                </a>
                            </div>
                        </div>
                        <?php children2($child, $cat_detail, $img_icon, 0); ?>
                    </li>
                <?php if ($cols > count($para['children']) && $j!= count($para['children'])) { ?>
                    </ul>
                    <ul class="mega-menu-sub-ul" >
                    <?php $j++; ?>
                <?php } else { ?>
                    <?php if (isset($cols) && $cols && $cols != 0 && isset($columns) && $columns != 0) {
                        if ($current_col <= $extra_col ) {
                            $columns = $temp_col+1;
                        } else {
                            $columns = $temp_col;
                        }
                        if ($i - $columns == 0) {
                            $i = 0;
                            $current_col++; ?>
                            </ul>
                            <ul class="mega-menu-sub-ul">
                        <?php
                        }
                        $i++;
                    } ?>
                <?php }
            } ?>
            </ul>
            <?
            return true;
        }
    }
?>

<div class="mega-menu">
    <div id="mega-menu-inside">
        <?php /*
               * Ez a kapcsoló felel azért, hogy megjelenjen-e egy átfogó cím (csak egy div szöveggel), ami alá tartozik az összes kategória (ul->li->ul->li...),
               * vagy nélküle az összes kategória felsorolásával kezdődik a megjelenítés, ul-ekbe.
               */
        ?>
        <?php if(!(isset($template['fokategoria']) && $template['fokategoria'])) { ?>
            <div class="mega-menu-title">
                <?php if((isset($template['main_link']) && $template['main_link'])) { ?>
                    <a href="<?php echo $product; ?>"><?php echo $text_termekek; ?></a>
                <?php } else { ?>
                    <?php echo $text_termekek; ?>
                <?php } ?>
            </div>
        <?php } ?>
        <?php /*
               * A főkategóriákhoz tartozó képek "bal" oldalon való megjelenítésére szolgáló kapcsoló.
               * A lényege, hogy még a kategóriák elé bekerül egy div, amibe az összes főkategória képe bekerül divenként.
               *
               * A kategória kép és kategória ikon méretét adminban lehet állítani a Sokszintű kategória modulban.
               *
               * Azért adtam meg a képhez kiszámolt magasságot a mega-menu-image container-hez, mert absolute pozícióban
               * van a div és szeretném a relatív pozicíóban lévő felmenőjéhez képest vertikálisan középre és jobb oldalra igazítani.
               * Ezt nem lehet megoldani anélkül, hogy megadnánk a div magasságát.
               */
        ?>
        <?php if ((isset($template['img_left']) && $template['img_left']) && !(isset($template['img_right']) && $template['img_right'])) { ?>
            <div class="mega-menu-image-container" style="height:<?php echo $this->config->get('multi_category_full_image_height'); ?>px;">
                <?php foreach ($categories as $category) { ?>
                    <div class="mega-menu-image left">
                        <?php if (isset($category['image']) && $category['image']) { ?>
                            <div id="cat_img_<?php echo $category['category_id']; ?>" class="category_image" >
                                <a href="<?php echo $category['href']; ?>" >
                                    <img src="<?php echo $category['image']?>">
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
        <div class="mega-menu-list" >
            <div class="mega-menu-list-in" >

                    <?php ($template['columns'] != 0) ? $columns = intval(count($categories) / $template['columns']) : $columns=0 ?>
                    <?php $extra_col = count($categories) - ($template['columns']  * $columns); ?>
                    <?php $current_col = 1; ?>
                    <?php $temp_col = $columns; ?>
                    <?php $i = 1; $j = 1; ?>
                <ul class="mega-menu-ul" <?php if ((isset($template['img_right']) && $template['img_right']) || (isset($template['img_left']) && $template['img_left'])) { ?>
                    style="min-height: <?php echo $this->config->get('multi_category_full_image_height'); ?>px;"
                <?php } ?> >
                    <?php foreach ($categories as $category) { ?>
                        <li  class="mega-menu-category i_<?php echo $i; ?>">
                            <?php if (isset($category['image_icon']) && $category['image_icon'] && isset($template['img_icon']) && $template['img_icon']) { ?>
                                <div class="mega-menu-cat-icon">
                                    <a href="<?php echo $category['href']; ?>" >
                                        <img src="<?php echo $category['image_icon']?>">
                                    </a>
                                </div>
                            <?php } ?>
                            <?php if (isset($category['image']) && $category['image'] && ((isset($template['img_left']) && $template['img_left']) && (isset($template['img_right']) && $template['img_right']))) { ?>
                                <div class="category_image">
                                    <a href="<?php echo $category['href']; ?>" >
                                        <img src="<?php echo $category['image']?>">
                                    </a>
                                </div>
                            <?php } ?>
                            <?php if (isset($category_id) && $category['category_id'] == $category_id) { ?>
                                <div class="category">
                                    <div id="cat_id_<?php echo $category['category_id']; ?>" class="category-name">
                                        <a href="<?php echo $category['href']; ?>" class="active">
                                            <?php echo $category['name']; ?>
                                            <?php if (isset($category['description']) && $category['description'] && isset($template['cat_detail']) && $template['cat_detail']) { ?>
                                                <div class="cat-detail">
                                                    <?php echo html_entity_decode($category['description'], ENT_QUOTES, 'UTF-8'); ?>
                                                </div>
                                            <?php } ?>
                                        </a>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="category">
                                    <div id="cat_id_<?php echo $category['category_id']; ?>" class="category-name">
                                        <a href="<?php echo $category['href']; ?>" >
                                            <?php echo $category['name']?>
                                            <?php if (isset($category['description']) && $category['description'] && isset($template['cat_detail']) && $template['cat_detail']) { ?>
                                                <div class="cat-detail">
                                                    <?php echo html_entity_decode($category['description'], ENT_QUOTES, 'UTF-8'); ?>
                                                </div>
                                            <?php } ?>
                                        </a>
                                    </div>
                                </div>
                            <?php }
                                if(!(isset($template['sub_icon']) && $template['sub_icon'])) {
                                    $template['img_icon'] = "";
                                }
                                if(!(isset($template['sub_detail']) && $template['sub_detail'])) {
                                    $template['cat_detail'] = "";
                                }
                                children2($category, $template['cat_detail'], $template['img_icon'], $template['sub_columns']);
                            ?>
                        </li>
                        <?php if ($template['columns'] > count($categories) && $j!= count($categories) ) { ?>
                            </ul>
                            <ul class="mega-menu-ul" style="display:inline-block; vertical-align: top;">
                        <?php } else { ?>
                            <?php if (isset($template['columns']) && $template['columns'] && $template['columns'] != 0 && isset($columns) && $columns != 0) {
                                if ($current_col <= $extra_col ) {
                                    $columns = $temp_col+1;
                                } else {
                                    $columns = $temp_col;
                                }

                                            if ($i - $columns == 0) {
                                    $i = 0;
                                $current_col++;
                                ?>
                            </ul>
                            <ul class="mega-menu-ul" style="display:inline-block; vertical-align: top;">
                                <?php
                                }
                                $i++;
                            } ?>
                    <?php } } ?>
                </ul>

            <?php /*
           * A főkategóriákhoz tartozó képek "bal" oldalon való megjelenítésére szolgáló kapcsoló.
           * A lényege, hogy még a kategóriák elé bekerül egy div, amibe az összes főkategória képe bekerül divenként.
           */
            ?>
            <?php if ((isset($template['img_right']) && $template['img_right']) && !(isset($template['img_left']) && $template['img_left'])) { ?>
                <div class="mega-menu-image-container" style="height:<?php echo $this->config->get('multi_category_full_image_height'); ?>px;">
                    <?php foreach ($categories as $category) { ?>
                            <div class="mega-menu-image right">
                                <?php if (isset($category['image']) && $category['image']) { ?>
                                    <div id="cat_img_<?php echo $category['category_id']; ?>" class="category_image" >
                                        <a href="<?php echo $category['href']; ?>" >
                                            <img src="<?php echo $category['image']?>">
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                    <?php } ?>
                </div>
            <?php } ?>
            </div>
        </div>
    </div>
</div>

<?php /*
       * A főkategóriára hoverezve megjelenik a hozzátartozó kép.
       * Alapból az összes főkategóriához tartozó képet betölti (!csak főkategóriához).
       * Egy főkategóriára hoverezve display: block-ot kap a kép, megjelenik, majd ha elhagytuk a főkategória div-et eltűnik.
       */
?>
<?php if(isset($template['cat_hover']) && $template['cat_hover']) { ?>
    <script type="text/javascript">
        $('.mega-menu-category').mouseenter(function(){
          var id        = $(this).children().eq(0).children().eq(0).attr('id');
          var id_array  = id.split('_');
          var id_nmb    = id_array[2];
          $('#cat_img_'+id_nmb).css('display','block');

          <?php if(isset($template['cat_arrow']) && $template['cat_arrow']) { ?>
          var hasChildMain  =  $(this).find('ul');
          if(hasChildMain.length > 0) {
              $(this).find('a').first().addClass('has_child');
          }
          <?php } ?>
        });

        $('.mega-menu-category').mouseleave(function(){
            var id = $(this).children().eq(0).children().eq(0).attr('id');
            var id_array = id.split('_');
            var id_nmb = id_array[2];

            $('#cat_img_'+id_nmb).css('display','none');
            <?php if(isset($template['cat_arrow']) && $template['cat_arrow']) { ?>
            $(this).find('a').first().removeClass('has_child');
            <?php } ?>
        });

        <?php if(isset($template['cat_arrow']) && $template['cat_arrow']) { ?>
        $('.mega-menu-subcategory').mouseenter(function(){
            var hasChild = $(this).find('ul');
            if(hasChild.length > 0) {
                $(this).find('a').first().addClass('has_child');
            }
        });

        $('.mega-menu-subcategory').mouseleave(function(){
            $(this).find('a').first().removeClass('has_child');
        });
        <?php } ?>

    </script>
<?php } ?>

<?php
    /*
     *
     * Optiwebnél a kategóriák és alkategóriák egységesen egymás mellett vannak.
     * Ahhoz, hogy a menü mérete igazodjon a kategóriák, alkategóriák méretéhez,
     * figyelnünk kell a legmagasabb kategóriát, alkategóriát s ahhoz igazítjuk
     * a menü méretét.
     *
     */
?>

<?php if(isset($template['cat_measure']) && $template['cat_measure']) { ?>
    <script type="text/javascript">
        $('.mega-menu li').on('mouseover', function() {
            var $menuItem       = $(this),
                $submenuWrapper = $('> ul', $menuItem),
                subHeight       = $submenuWrapper.height(),
                menuHeight      = $('.mega-menu-ul').height();

            if($submenuWrapper != "" ) {
                if( subHeight > menuHeight ) {
                    $submenuWrapper.css("height", subHeight+"px");
                    $('.mega-menu-ul').css("height", subHeight+"px");
                } else {
                    $submenuWrapper.css("height", "100%");
                }
            }
        });

        $('.mega-menu li').on('mouseout', function() {
            $('.mega-menu ul').removeAttr('style');
            var imageHeight = 0;
            var menuHeight  = $('.mega-menu-ul').height();

            <?php if ((isset($template['img_right']) && $template['img_right']) || (isset($template['img_left']) && $template['img_left'])) { ?>
            imageHeight = <?php echo $this->config->get('multi_category_full_image_height'); ?>;
            <?php } ?>

            imageHeight > menuHeight ? $('.mega-menu-ul').css("height", imageHeight+"px") : "";
        });
    </script>
<?php } ?>
