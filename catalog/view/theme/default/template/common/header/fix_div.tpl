<?php if((isset($template['upper_div'])) && (isset($template['fix_div'])) && $template['upper_div'] && $template['fix_div']) { ?>
<script type="text/javascript">

    function sticky_relocate() {
        var window_top = $(window).scrollTop();
        var div_top         = $('.<?php echo $template['upper_div']; ?>').offset().top+$('.<?php echo $template['upper_div']; ?>').height();
        var fix_div         = $('.<?php echo $template['fix_div']; ?>').height();
        var fix_div_width   = $('.<?php echo $template['fix_div']; ?>').width();
        var window_width    = $(window).width();
        var cart            = $('.<?php echo $template['fix_div']; ?> > div:last-child').offset();

        if (window_top > div_top) {
            if(fix_div_width < window_width) {
                var padding = (window_width-fix_div_width)/2;
                $('.<?php echo $template['fix_div']; ?>').css('padding-left',padding);
                $('.<?php echo $template['fix_div']; ?>').css('padding-right',padding);
            }
            $('.<?php echo $template['fix_div']; ?>').addClass('fix-div');
            $('.<?php echo $template['upper_div']; ?>').css('margin-bottom',fix_div);
        } else {
            $('.<?php echo $template['fix_div']; ?>').removeClass('fix-div');
            $('.<?php echo $template['upper_div']; ?>').removeAttr('style');
        }
    }

    $(function () {
        $(window).scroll(sticky_relocate);
        sticky_relocate();
    });

</script>
<?php } ?>