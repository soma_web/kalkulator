<div id="menu" style="">
    <ul>
        <li><a href="<?php echo $home; ?>"><?php echo $text_home; ?></a></li>
        <li id="termekek"><a href="<?php echo $product; ?>"><?php echo $text_termekek; ?></a>
            <?php if ($categories) { ?>
                <div class="termek_kategoria">
                    <ul>
                        <?php foreach ($categories as $category) { ?>
                            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                                <?php if ($category['children']) { ?>
                                    <div>
                                        <?php for ($i = 0; $i < count($category['children']);) { ?>
                                            <ul>
                                                <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
                                                <?php for (; $i < $j; $i++) { ?>
                                                    <?php if (isset($category['children'][$i])) { ?>
                                                        <li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>
                                                    <?php } ?>
                                                <?php } ?>
                                            </ul>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>

        </li>
        <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
        <li><a href="<?php echo $pricelist; ?>"><?php echo $text_arlista; ?></a></li>
        <li><a href="<?php echo $kapcsolat; ?>"><?php echo $text_kapcsolat; ?></a></li>
    </ul>
</div>