<?php if ($logo) { ?>
    <div id="logo" class="header-content"><a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a></div>
    <?php if (!empty($template['motto']) ) { ?>
        <div id="motto"><?php echo $text_motto; ?></div>
    <?php } ?>
<?php } ?>