<?php
if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/stylesheet_header.php') && false) {
    $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/stylesheet/stylesheet_header.php';
    include($arkiir);

} elseif (file_exists(DIR_TEMPLATE . 'default/stylesheet/stylesheet_header.php')) {
    $arkiir = DIR_TEMPLATE.'default/stylesheet/stylesheet_header.php';
    include($arkiir);

} ?>

<?php   $elso       = true;
$elozo      = 0;
$divszamol  = 1;
$div_nyit   = 0;
$div_zar    = 0;
?>


<?php foreach ($megjelenit_header_csoportok as $key=>$value) { ?>
<?php
if ($elso) {$elso = false;}
else {
    $elozo = $divszamol;
}
$divszamol = count(explode('_',$key));

if($divszamol <= $elozo) {
    for($i=$divszamol; $elozo >=$i; $i++) {
        echo "</div>";
        $div_zar ++;

    }
}
$elotag = ($divszamol%2 == 0) ? "oszlop_" : "sor_";

$style_inlines = isset($value['default']['inline']) && $value['default']['inline'] ? htmlspecialchars_decode($value['default']['inline']) : '';
if ($style_inlines) {
    $style_inlines = str_replace(' ','',$style_inlines);
    $style_inline = explode(';',$style_inlines);

    foreach($style_inline as $key_inline=>$inline) {
        $pos = strpos("$inline","$");
        if ($pos !== false) {
            $pos1 = strpos($inline,"this",$pos);
            if ($pos1 == $pos+1) {
                $pos2 = strpos($inline,"-",$pos1);
                if ($pos2 == $pos1+4) {
                    $pos3 = strpos($inline,">",$pos2);
                    if ($pos3 == $pos2+1) {
                        $pos4 = strpos($inline,"config",$pos3);
                        if ($pos4 == $pos3+1) {
                            $pos5 = strpos($inline,"-",$pos4);
                            if ($pos5 == $pos4+6) {
                                $pos6 = strpos($inline,">",$pos5);
                                if ($pos6 == $pos5+1) {
                                    $pos7 = strpos($inline,"get",$pos6);
                                    if ($pos7 == $pos6+1) {
                                        $parameter_pos_tol = strpos($inline,"'",$pos7);
                                        if ($parameter_pos_tol == $pos7+4) {
                                            $parameter_pos_tol ++;
                                            $parameter_pos_ig = strpos($inline,"'",$parameter_pos_tol);
                                            $parameter_pos_ig;
                                            $parameter = substr($inline,$parameter_pos_tol,($parameter_pos_ig-$parameter_pos_tol));

                                        } else {
                                            $parameter_pos_tol = strpos($inline,'"',$pos7);
                                            if ($parameter_pos_tol == $pos7+4) {
                                                $parameter_pos_tol ++;
                                                $parameter_pos_ig = strpos($inline,"'",$parameter_pos_tol);
                                                $parameter = substr($inline,$parameter_pos_tol,($parameter_pos_ig-$parameter_pos_tol));
                                            }
                                        }

                                        if (isset($parameter) && $parameter) {
                                            $eredmeny=$this->config->get($parameter);
                                            $style_inline[$key_inline] = substr($inline,0,$pos);
                                            $style_inline[$key_inline] .= $eredmeny;
                                            $style_inline[$key_inline] .= substr($inline,$parameter_pos_ig+2);
                                        }

                                    }

                                }

                            }

                        }

                    }

                }
            }
        }
    }
    $style_inlines = implode(';',$style_inline);
}
?>
<div class="<?php echo $elotag.$key. ' ' . $value['default']['class']?>" style="<?php echo $style_inlines?>">
    <?php $div_nyit++?>
    <?php if ($value['default']['status'] == 1) { ?>
        <?php if(isset($value['templates']) && !empty($value['templates'])) { ?>
            <?php foreach ($value['templates'] as $keytemplate=>$template) { ?>
                <?php if (isset($template['status']) && $template['status'] == 1) {?>
                    <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/header/'. $keytemplate .'.tpl')) {
                        $arkiir = DIR_TEMPLATE. $this->config->get('config_template') . '/template/common/header/'.$keytemplate . '.tpl';
                    } else {
                        $arkiir = DIR_TEMPLATE.'default/template/common/header/'.$keytemplate.'.tpl';
                    } ?>
                    <?php include($arkiir); ?>
                <?php } ?>

            <?php } ?>
        <?php } ?>
    <?php } ?>
<?php }


    $div_nyitva = $div_nyit - $div_zar;
    if ($div_nyit - $div_zar > 0) {
        $div_nyitva = $div_nyit - $div_zar;
        for ($i=0; $div_nyitva > $i; $i++) {
            echo "</div>";
        }
    }

    ?>

