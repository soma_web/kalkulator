<?php echo $header; ?>
<?php if ($teljes_szelesseg) {?>
    <?php echo $content_top; ?>
    <div style="clear: both"></div>
    <?php echo $column_left; ?><?php echo $column_right; ?>
    <div id="content">
        <h1 style="display: none;"><?php echo $heading_title; ?></h1>
        <?php echo $content_bottom; ?>
    </div>
    <div style="clear: both"></div>
<?php } else {?>
    <?php echo $column_left; ?><?php echo $column_right; ?>
    <div id="content"><?php echo $content_top; ?>
        <h1 style="display: none;"><?php echo $heading_title; ?></h1>
        <?php echo $content_bottom; ?>
    </div>
<?php }?>

<?php echo $footer; ?>