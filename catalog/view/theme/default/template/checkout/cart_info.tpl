    <div class="cart-info">
    <?php $megjelenit_product = $this->config->get('megjelenit_product'); ?>


        <?php if ( $megjelenit_kosar['netto_adat'] == 0) { ?>


            <table class="cart-info-table">
                <thead>
                <tr>
                    <th class="image"><?php echo $column_image; ?></th>
                    <th class="name"><?php echo $column_name; ?></th>
                    <?php if ( $megjelenit_kosar['model_adat'] == 1) { ?>
                        <th class="model"><?php echo $column_model; ?></th>
                    <?php } ?>
                    <th class="quantity"><?php echo $column_quantity; ?></th>
                    <th class="price"><?php echo $column_price; ?></th>
                    <th class="total"><?php echo $column_total; ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($products as $product) { ?>
                    <tr>
                        <td data-th="<?php echo $column_image; ?>"  class="image"><?php if ($product['thumb']) { ?>
                                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
                            <?php } ?></td>
                        <td data-th="<?php echo $column_name; ?>" class="name">
                            <a href="<?php echo $product['href']; ?>">
                                <?php if ($megjelenit_product['product_model_termeknevben']) { ?>
                                    <?php if (isset($product['model']) && $product['model']) { ?>
                                        <span class="model_cart"><?php echo $product['model'];?></span> <?php echo " - ";?>
                                    <?php }?>
                                <?php }?>

                                <?php if ($megjelenit_product['product_cikkszam_termeknevben']) { ?>
                                    <?php if (isset($product['cikkszam']) && $product['cikkszam']) { ?>
                                        <span class="model_cart"><?php echo $product['cikkszam'];?></span> <?php echo " - ";?>
                                    <?php }?>
                                <?php }?>

                                <?php if ($megjelenit_product['product_gyarto_termeknevben']) { ?>
                                    <?php if (isset($product['manufacturer']) && $product['manufacturer']) { ?>
                                        <span class="manufacturer_cart"><?php echo $product['manufacturer'];?></span> <?php echo " - ";?>
                                    <?php }?>
                                <?php }?>
                                <?php echo $product['name']; ?>
                            </a>
                            <?php if (!$product['stock']) { ?>
                                <span class="stock">***</span>
                            <?php } ?>

                            <div>
                                <?php foreach ($product['option'] as $option) { ?>
                                    - <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small><br />
                                <?php } ?>
                            </div>
                            <?php if ($product['reward']) { ?>
                                <small><?php echo $product['reward']; ?></small>
                            <?php } ?></td>
                        <?php if ( $megjelenit_kosar['model_adat'] == 1) { ?>
                            <td data-th="<?php echo $column_model; ?>"  class="model"><?php echo $product['model']; ?></td>
                        <?php } ?>

                        <td data-th="<?php echo $column_quantity; ?>"  class="quantity">
                            <?php if (!$product['minimum_warrning']) { ?>
                                <input style="border: 1px solid red" type="text" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" />
                            <?php } else { ?>
                                <input type="text" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" />
                            <?php } ?>
                            &nbsp;
                            <input type="image" src="catalog/view/theme/default/image/update.png" alt="<?php echo $button_update; ?>" title="<?php echo $button_update; ?>" />
                            &nbsp;<a href="<?php echo $product['remove']; ?>"><img src="catalog/view/theme/default/image/remove.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" /></a></td>
                        <td data-th="<?php echo $column_price; ?>"  class="price"><?php echo $product['price']; ?></td>
                        <td data-th="<?php echo $column_total; ?>" class="total"><?php echo $product['total']; ?></td>
                    </tr>
                <?php } ?>
                <?php foreach ($vouchers as $vouchers) { ?>
                    <tr>
                        <td class="image"></td>
                        <td class="name"><?php echo $vouchers['description']; ?></td>
                        <?php if ( $megjelenit_kosar['model_adat'] == 1) { ?>
                            <td class="model"><?php echo $column_model; ?></td>
                        <?php } ?>
                        <td class="quantity"><input type="text" name="" value="1" size="1" disabled="disabled" />
                            &nbsp;<a href="<?php echo $vouchers['remove']; ?>"><img src="catalog/view/theme/default/image/remove.png" alt="<?php echo $text_remove; ?>" title="<?php echo $button_remove; ?>" /></a></td>
                        <td class="price"><?php echo $vouchers['amount']; ?></td>
                        <td class="total"><?php echo $vouchers['amount']; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>


        <?php } else { ?>


            <table>
                <thead>
                <tr>
                    <td class="image"><?php echo $column_image; ?></td>
                    <td class="name"><?php echo $column_name; ?></td>
                    <?php if ( $megjelenit_kosar['model_adat'] == 1) { ?>
                        <td class="model"><?php echo $column_model; ?></td>
                    <?php } ?>
                    <td class="price"><?php echo $column_netto; ?></td>
                    <td class="price"><?php echo $column_price_netto_ar; ?></td>
                    <td class="quantity" style="text-align: right"><?php echo $column_quantity; ?></td>
                    <td class="netto"><?php echo $column_netto_total_price; ?></td>
                    <td class="total"><?php echo $column_total_netto_ar; ?></td>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($products as $product) { ?>
                    <tr>
                        <td data-th="<?php echo $column_image; ?>" class="image"><?php if ($product['thumb']) { ?>
                                <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
                            <?php } ?></td>
                        <td data-th="<?php echo $column_name; ?>" class="name">
                            <a href="<?php echo $product['href']; ?>">
                                <?php if ($megjelenit_product['product_model_termeknevben']) { ?>
                                    <?php if (isset($product['model']) && $product['model']) { ?>
                                        <span class="model_cart"><?php echo $product['model'];?></span> <?php echo " - ";?>
                                    <?php }?>
                                <?php }?>

                                <?php if ($megjelenit_product['product_cikkszam_termeknevben']) { ?>
                                    <?php if (isset($product['cikkszam']) && $product['cikkszam']) { ?>
                                        <span class="model_cart"><?php echo $product['cikkszam'];?></span> <?php echo " - ";?>
                                    <?php }?>
                                <?php }?>

                                <?php if ($megjelenit_product['product_gyarto_termeknevben']) { ?>
                                    <?php if (isset($product['manufacturer']) && $product['manufacturer']) { ?>
                                        <span class="manufacturer_cart"><?php echo $product['manufacturer'];?></span> <?php echo " - ";?>
                                    <?php }?>
                                <?php }?>
                                <?php echo $product['name']; ?>
                            </a>
                            <?php if (!$product['stock']) { ?>
                                <span class="stock">***</span>
                            <?php } ?>
                            <div>
                                <?php foreach ($product['option'] as $option) { ?>
                                    - <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small><br />
                                <?php } ?>
                            </div>
                            <?php if ($product['reward']) { ?>
                                <small><?php echo $product['reward']; ?></small>
                            <?php } ?>
                        </td>
                        <?php if ( $megjelenit_kosar['model_adat'] == 1) { ?>
                            <td class="model"><?php echo $product['model']; ?></td>
                        <?php } ?>
                        <td class="price"><?php echo $product['netto_price']; ?></td>
                        <td class="price"><?php echo $product['price']; ?></td>
                        <td class="quantity"  style="text-align: right">

                            <?php if (!$product['minimum_warrning']) { ?>
                                <input style="border: 1px solid red" type="text" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" />
                            <?php } else { ?>
                                <input type="text" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" />
                            <?php } ?>

                            &nbsp;
                            <input type="image" src="catalog/view/theme/default/image/update.png" alt="<?php echo $button_update; ?>" title="<?php echo $button_update; ?>" />
                            &nbsp;<a href="<?php echo $product['remove']; ?>"><img src="catalog/view/theme/default/image/remove.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" /></a></td>
                        <td class="total"><?php echo $product['netto_total_price']; ?></td>
                        <td class="total"><?php echo $product['total']; ?></td>
                    </tr>
                <?php } ?>
                <?php foreach ($vouchers as $vouchers) { ?>
                    <tr>
                        <td class="image"></td>
                        <td class="name"><?php echo $vouchers['description']; ?></td>
                        <?php if ( $megjelenit_kosar['model_adat'] == 1) { ?>
                            <td class="model"><?php echo $column_model; ?></td>
                        <?php } ?>
                        <td class="price"><?php echo $vouchers['amount']; ?></td>
                        <td class="price"><?php echo $vouchers['amount']; ?></td>
                        <td class="quantity"><input type="text" name="" value="1" size="1" disabled="disabled" />
                            &nbsp;<a href="<?php echo $vouchers['remove']; ?>"><img src="catalog/view/theme/default/image/remove.png" alt="<?php echo $text_remove; ?>" title="<?php echo $button_remove; ?>" /></a></td>
                        <td class="total"><?php echo $vouchers['amount']; ?></td>
                        <td class="total"><?php echo $vouchers['amount']; ?></td>
                    </tr>
                <?php } ?>


                <tr>
                    <td  colspan="<?php if ( $megjelenit_kosar['model_adat'] == 1) {echo 6; } else {echo 5;}?>"><?php echo $termek_ar_osszes; ?></td>

                    <td class="price" style="font-weight: bold;" >
                        <?php echo $products_netto; ?>
                    </td>
                    <td class="price" style="font-weight: bold;" >
                        <?php echo $products_brutto; ?>
                    </td>

                </tr>
                </tbody>

            </table>
            <style>
                .cart-info tfoot td{
                    text-align: left;
                }
                .cart-info tfoot .total {
                    text-align: right;
                }
                .cart-info thead .netto {
                    text-align: right;
                }
                .cart-info thead .quantity{
                    text-align: center;
                }
            </style>
        <?php }?>

    </div>