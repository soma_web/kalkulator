<?php
?>
<style>
    .box-product > div input.button.arajanlat, .ajanlott_termekek input.button.arajanlat {
        padding-left: 20px;
        padding-right: 20px;
        margin: 0;
        width: 100%;
    }


    #arajanlatok {
        position: absolute;
        z-index: 10000000;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, 0.3);
    }

    #arajanlatok > div {
        background: #FFFFFF;
        /*border: 1px solid #104f9c;*/
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        -webkit-box-shadow: 2px 2px 30px rgba(0, 0, 0, 0.75);
        -moz-box-shadow: 2px 2px 30px rgba(0, 0, 0, 0.75);
        box-shadow: 2px 2px 30px rgba(0, 0, 0, 0.75);
        margin: 50px auto 0 auto;
        max-width: 800px;
        padding: 20px;
        width: 100%;
        position: relative;
    }
    #arajanlatok .close_arajanlat_icon{
        position: absolute;
        right: 2px;
        top: 2px;
        cursor: pointer;
        max-width: 14px;
    }

    #arajanlatok textarea {
        height: 51px;
        width: 90%;
    }

    #arajanlatok .buttons {
        border: none;

    }
    #arajanlatok .email_adatok{
        display: inline-block;
        width: 50%;
    }

    #arajanlatok .valasztott_termek{
        display: inline-block;
        vertical-align: top;
    }

    #arajanlatok .cimke{
        font-weight: bold;
        width: 20%;
    }
    #arajanlatok .ertek{
    }


    #arajanlatok textarea {
        height: 51px;
        width: 90%;
    }

    #arajanlatok .email_adatok{
        display: inline-block;
        width: 50%;
    }

    #arajanlatok .valasztott_termek{
        display: inline-block;
        vertical-align: top;
    }

    #arajanlatok .cimke{
        font-weight: bold;
        width: 20%;
    }
    .arajanlat-table {
        width: 100%;
    }

    .arajanlat-table thead {
        background: #eee;
    }

    .arajanlat-table thead th {
        padding: 6px 8px 6px 0;
    }

    .arajanlat-table td, .arajanlat-table th {
        text-align: right;
    }

    .torles_icon {
        position: relative;
        margin-left: 4px;
        top: 4px;
        cursor: pointer;
        width: 13px;
    }

    .torles_icon:hover {
        width: 14px;
    }

    .termek-torol {
        width: 15px;
        display: inline-table;
        overflow: hidden;
        height: 15px;
    }

    .torles_icon.option {
        width: 10px;
    }

    .torles_icon.option:hover {
        width: 11px;
    }

    .termek-torol.option {
        width: 17px;
        height: 12px;
    }




    #arajanlatok td.price {
        text-decoration: line-through;
    }

    .arajanlat-termek-neve {
        text-decoration: underline;
    }

    .arajanlat-termek-neve:hover {
        color: #B40038;
    }


</style>

