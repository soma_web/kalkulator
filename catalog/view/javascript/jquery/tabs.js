$.fn.tabs = function(a) {
	var selector = this;

    var defaults = {
        bxSlidesOn: false,
        minSlides: 5,
        maxSlides: 5,
        slideWidth: 260,
        slideMargin: 10,
        infiniteLoop: false,
        responsive: true,
        hideControlOnEnd: true,
        adaptiveHeight: false
    };

    var tabs = new Array();
    tabs.settings = $.extend({}, defaults, a);

    if (typeof(raadva) == "undefined") {
         raadva = new Array();
    }

    this.each(function() {
		var obj = $(this);
		
		$(obj.attr('href')).hide();
		
		$(obj).click(function() {
			$(selector).removeClass('selected');
			
			$(selector).each(function(i, element) {
				$($(element).attr('href')).hide();
			});
			
			$(this).addClass('selected');
			
			$($(this).attr('href')).slideDown(100);

            if (tabs.settings.bxSlidesOn) {
                if (raadva.indexOf($(this).attr('href').substr(1)) == -1) {
                    $("."+$(this).attr('href').substr(1)).bxSlider({
                        minSlides:           tabs.settings.minSlides,
                        maxSlides:           tabs.settings.maxSlides,
                        slideWidth:          tabs.settings.slideWidth,
                        slideMargin:         tabs.settings.slideMargin,
                        infiniteLoop:        tabs.settings.infiniteLoop,
                        responsive:          tabs.settings.responsive,
                        hideControlOnEnd:    tabs.settings.hideControlOnEnd,
                        adaptiveHeight:      tabs.settings.adaptiveHeight
                    });
                    raadva[raadva.length] = $(this).attr('href').substr(1);
                }
            }
			return false;
		});
	});

	$(this).show();
	
	$(this).first().click();
};
