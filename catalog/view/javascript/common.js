
function mulitifilterScriptInitialize(keres_autocomplate) {


    $('.button-search').bind('click', function() {
        url = $('base').attr('href') + 'index.php?route=product/search';
        var filter_name = $('input[name=\'filter_name\']').val();

        if (filter_name) {
            url += '&filter_name=' + encodeURIComponent(filter_name) + '&normal_kereso=1';
        }
        location = url;
    });

    $('#header input[name=\'filter_name\']').bind('keydown', function(e) {
        if (e.keyCode == 13) {
            debugger;

            url = $('base').attr('href') + 'index.php?route=product/search';

            var filter_name = this.value;

            if (filter_name) {
                url += '&filter_name=' + encodeURIComponent(filter_name) + '&normal_kereso=1';
            }

            location = url;
        }
    });

    $('.search-button').bind('click', function(){
        location =  $('base').attr('href') + 'index.php?route=product/search';
    });

    if (keres_autocomplate == 1) {
        $('input[name=\'filter_name\']').autocomplete({
            'source': function(request, response) {
                $.ajax({
                    url: 'index.php?route=product/search/autocomplete&filter_name=' +  encodeURIComponent(request)+'&normal_kereso=1',
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                image: item['image'],
                                value: item['product_id'],
                                label: "<span class='product_name'>"+item.name.substr(0,40)+"</span>" + ($('input[name=\'megjelenit_cikkszam\']').val() > 0 && item.cikkszam ? "<span class='model'>" + item.cikkszam +"</span>" : "") + ($('input[name=\'megjelenit_model\']').val() > 0 && item.model ? "<span class='model'>"+item.model +"</span>" : "")  + ($('input[name=\'megjelenit_gyarto\']').val() > 0 && item.manufacturer ? "<span class='model'>" + item.manufacturer+"</span>" : "")

                            }
                        }));
                    }
                });
            },
            'select': function(item) {
                url = $('base').attr('href') + 'index.php?route=product/product&product_id='+item.value;

                location = url;
                return false;
            }
        });

        $('input[name=\'filter_model\']').autocomplete({
            'source': function(request, response) {
                $.ajax({
                    url: 'index.php?route=product/search/autocomplete&filter_model=' +  encodeURIComponent(request)+'&normal_kereso=1',
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                image: item['image'],
                                label: item['model'],
                                value: item['product_id']
                            }
                        }));
                    }
                });
            },
            'select': function(item) {
                url = $('base').attr('href') + 'index.php?route=product/product&product_id='+item.value;

                location = url;
                return false;
            }
        });

        $('input[name=\'filter_cikkszam\']').autocomplete({
            'source': function(request, response) {
                $.ajax({
                    url: 'index.php?route=product/search/autocomplete&filter_cikkszam=' +  encodeURIComponent(request)+'&normal_kereso=1',
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                image: item['image'],
                                label: item['cikkszam'],
                                value: item['product_id']
                            }
                        }));
                    }
                });
            },
            'select': function(item) {
                url = $('base').attr('href') + 'index.php?route=product/product&product_id='+item.value;

                location = url;
                return false;
            }
        });




        /*$('input[name=\'filter_name\']').autocomplete({
            delay: 0,
            source: function(request, response) {
                $.ajax({
                    url: 'index.php?route=product/search/autocomplete&filter_name=' +  encodeURIComponent(request.term)+'&normal_kereso=1',
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                label: ($('input[name=\'megjelenit_cikkszam\']').attr('value') > 0 && item.cikkszam ? item.cikkszam +"-" : "") + ($('input[name=\'megjelenit_model\']').attr('value') > 0 && item.model ? item.model +"-" : "")  + ($('input[name=\'megjelenit_gyarto\']').attr('value') > 0 && item.manufacturer ? item.manufacturer+"-" : "") +item.name.substr(0,40),
                                product_id: item.product_id,
                                value: item.name,
                                image: item.image
                            }
                        }));
                    },
                    error: function(e) {
                    }
                });
            },
            select: function(event, ui) {


                url = $('base').attr('href') + 'index.php?route=product/product&product_id='+ui.item.product_id;

                location = url;
                return false;
            },
            open: function(){
                $(this).autocomplete('widget').css('z-index', 100000);
                $(this).autocomplete('widget').css('max-width', "30%");
                return false;
            }
        });

        $('input[name=\'filter_model\']').autocomplete({
            delay: 0,
            source: function(request, response) {
                $.ajax({
                    url: 'index.php?route=product/search/autocomplete&filter_model=' +  encodeURIComponent(request.term),
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                label: item.model,
                                //value: item.product_id
                                value: item.model
                            }
                        }));
                    }
                });
            },
            select: function(event, ui) {
                $('input[name=\'filter_model\']').val(ui.item.label);
                url = $('base').attr('href') + 'index.php?route=product/search';

                var filter_model = $('input[name=\'filter_model\']').attr('value');

                if (filter_model) {
                    url += '&filter_model=' + encodeURIComponent(filter_model);
                }
                location = url;
                return true;
            },
            open: function(){
                $(this).autocomplete('widget').css('z-index', 100000);
                $(this).autocomplete('widget').css('max-width', "300px");
                return false;
            }
        });

        $('input[name=\'filter_cikkszam\']').autocomplete({
            delay: 0,
            source: function(request, response) {
                $.ajax({
                    url: 'index.php?route=product/search/autocomplete&filter_cikkszam=' +  encodeURIComponent(request.term),
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                label: item.cikkszam,
                                //value: item.product_id
                                value: item.cikkszam
                            }
                        }));
                    },
                    error: function(e) {
                    }
                });
            },
            select: function(event, ui) {
                $('input[name=\'filter_cikkszam\']').val(ui.item.label);
                url = $('base').attr('href') + 'index.php?route=product/search';

                var filter_cikkszam = $('input[name=\'filter_cikkszam\']').attr('value');

                if (filter_cikkszam) {
                    url += '&filter_cikkszam=' + encodeURIComponent(filter_cikkszam);
                }
                location = url;
                return true;
            },
            open: function(){
                $(this).autocomplete('widget').css('z-index', 100000);
                $(this).autocomplete('widget').css('max-width', "300px");
                return false;
            }
        });*/
    }



    var search_visibility = 0;
// Animation for the search button
    $("#show_search").bind("click", function(){
        if (search_visibility == 0) {
            $("#search_bar").fadeIn();
            $('#show_search').addClass('s_search_button2');
            search_visibility = 1;
        } else {
            $("#search_bar").fadeOut();
            $('#show_search').removeClass('s_search_button2');
            search_visibility = 0;
        }
    });



    $('#search_button').bind('click', function() {
        moduleSearch();
    });

    $('#filter_keyword').keydown(function(e) {
        if (e.keyCode == 13) {
            moduleSearch();
        }
    });


    /* Ajax Cart */
    $(document).delegate('#cart > .heading','click', function() {
        $('#cart').addClass('active');

        $('#cart').load('index.php?route=module/cart #cart > *');

        $('#cart').on('mouseleave', function() {
            $(this).removeClass('active');
        });
    });

    $(document).delegate('#cart_responsive > .heading','click', function() {

        if ($('#cart_responsive.active').length > 0) {
            $('#cart_responsive').removeClass('active');
        } else {
            $('#cart_responsive').addClass('active');
        }

        $('#cart_responsive .content_responsive').load('index.php?route=module/cart #cart .content > *');

        menusorokatZar();

    });

    /* Mega Menu */
    $('#menu ul > li > a + div').each(function(index, element) {
        // IE6 & IE7 Fixes
        if ($.browser.msie && ($.browser.version == 7 || $.browser.version == 6)) {
            var category = $(element).find('a');
            var columns = $(element).find('ul').length;

            $(element).css('width', (columns * 143) + 'px');
            $(element).find('ul').css('float', 'left');
        }

        var menu = $('#menu').offset();
        var dropdown = $(this).parent().offset();

        i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

        if (i > 0) {
            $(this).css('margin-left', '-' + (i + 0) + 'px');
        }
    });



    $('.success img, .warning img, .attention img, .information img').on('click', function() {
        $(this).parent().slideUp('slow', function() {
            $(this).remove();
        });
    });
}


jQuery(pozicio=function($) {
    xOffset = 0;
    yOffset = 0;
    $(document).mousemove(function (e) {
         xOffset = e.pageX;
         yOffset = e.pageY;
    });
});

$(document).ready(function() {
    notafication_kiiras = "";
    cart_active = "";
    var keres_autocomplate  = $('input[name=\'keres_autocomplate\']').attr('value');
    mulitifilterScriptInitialize(keres_autocomplate);
});

/* Search */
function moduleSearch() {
    var filter_name = $('#filter_keyword').val();

    if (filter_name) {
        url = 'index.php?route=product/search&filter_name=' + encodeURIComponent(filter_name);
        location = url;
    }
}

function addToCart(product_id, quantity, template, kep_id,termek_aloldal, in_product_id) {


    debugger;

    if (typeof(template) == "undefined" ){
        template="default";
    }

    if (typeof(termek_aloldal) == "undefined" ){
        termek_aloldal=false;
    }
    quantity = typeof(quantity) != 'undefined' ? quantity : 1;

    if (termek_aloldal) {
       if ($('.product-info input[name=\'quantity2\']')) {
           $('.product-info input[name=\'quantity2\']').attr("value",quantity);
       }

       var datatombatad= $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea');
    } else {
       var datatombatad='product_id=' + product_id + '&quantity=' + quantity;
    }

    in_product_id = '';
    if ($('input[name="kapcsolodo_csoport"]').length > 0) {
       in_product_id = $('input[name="kapcsolodo_csoport"]').val();
    }

    $.ajax({
        url: 'index.php?route=checkout/cart/add&termek_aloldal='+termek_aloldal+'&display=block&in_product_id='+in_product_id,
		type: 'post',
        data : datatombatad,
		dataType: 'json',
		success: function(json) {

            $('.success, .warning, .attention, .information, .error').remove();

            if (json['error']) {

                if (json['error']['warning']) {
                    $('#notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                    $('.warning').slideDown('slow');
                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                }

                if (json['error']['marvan']) {
                    $('#notification').html('<div class="warning" style="display: none;">' + json['error']['marvan'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

                    $("#slidecontent").load("index.php #cart", function( response, status, xhr ) {
                        clearTimeout(cart_active);
                        cart_active = setTimeout(" $('#cart').removeClass('active')" ,7000);

                        $('#cart').addClass('active');
                        $('#cart').on('mouseleave', function() {
                            $(this).removeClass('active');
                        });

                        $('#cart').on('mouseenter', function() {
                            clearTimeout(cart_active);
                        });

                    });
                    $('.warning').slideDown('slow');

                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                }

                if (json['error']['option']) {
                    $('#notification').html('<div class="warning" style="display: none;">' + json['error']['option'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                    $('.warning').slideDown('slow');
                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                }

                if (json['redirect']) location = json['redirect'];
                notaficationBezar();


            }

			else if (json['success']) {
                if (typeof(kep_id) != "undefined"){

                    var cart = $('#cart').offset();
                    var image_helye=$("#"+kep_id).offset();
                    if (image_helye != null){
                        $('#temp').remove();
                        $('#'+kep_id).before('<img src="' + $('#'+kep_id).attr('src') + '" id="temp" style="position: absolute; z-index: 10000; width: auto" />');

                        params = {
                            top : (image_helye.top-cart.top)*-1+"px",
                            left : (image_helye.left-cart.left)*-1+"px",
                            opacity : 0.0,
                            width : $('#cart').width(),
                            height : $('#cart').height()
                        };
                        $('#temp').animate(params, 1200, false, function () {
                            $('#temp').remove();
                        });
                    }
                }

                if (template == "helios")
                    $('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/'+template+'/image/remove-notification.png" alt="" class="close" /></div>');
                else
                    $('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

				$('#cart-total').html(json['total']);

                notaficationBezar();




                /*$("#slidecontent").load("index.php #cart");
                clearTimeout(cart_active);
                cart_active = setTimeout(" $('#cart').removeClass('active')" ,3000);

                $('#cart').addClass('active');
                $('#cart').on('mouseleave', function() {
                    $(this).removeClass('active');
                });

                $('#cart').on('mouseenter', function() {
                    clearTimeout(cart_active);
                });*/

                $("#slidecontent").load("index.php #cart", function( response, status, xhr ) {
                     clearTimeout(cart_active);
                     cart_active = setTimeout(" $('#cart').removeClass('active')" ,3000);

                    $('#cart').addClass('active');
                    $('#cart').on('mouseleave', function() {
                        $(this).removeClass('active');
                    });

                    $('#cart').on('mouseenter', function() {
                        clearTimeout(cart_active);
                    });

                });
                $('.success').slideDown('slow');

                $('html, body').animate({ scrollTop: 0 }, '1200');


                /*$( "#success" ).load( "/not-here.php", function( response, status, xhr ) {
                    if ( status == "error" ) {
                        var msg = "Sorry but there was an error: ";
                        $( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
                    }
                });*/


                if ($(".cart-info").length > 0) {
                    $(".cart-info").load($('base').attr('href') + 'index.php?route=checkout/cart&no_header=1');
                } else {
                    if (window.location.search.substring(1) == "route=checkout/cart") {
                        location = 'index.php?route=checkout/cart';
                    }
                }
                reloadMiniCartContent();
                $("#pay-button").slideDown("slow");

                $("#cart_responsive .responsive_darab").remove();
                $("#cart_responsive span").load('index.php .slidecontent .responsive_darab_cart');

                if (kep_id.substr(0,6) != "upsell") {
                    $.ajax({
                        url: 'index.php?route=module/upsell',
                        type: 'post',
                        data : 'product_id='+product_id,
                        dataType: 'html',
                        success: function(html) {
                            if ($("#upsell").length > 0) {
                                $("#upsell").remove();
                            }
                            $("#container").before(html);

                        },
                        error: function(html){
                        }
                    });
                } else {
                    $("#upsell").slideUp("slow");
                }

                if (typeof(kalkOsszesen) == 'function') {
                    kalkOsszesen(-1);
                }


            }
		},
        error: function(json){
        },

        complete: function (json) {

        }
	});
}

function megrendelem(product_id, quantity, template, kep_id,termek_aloldal) {

    if (typeof(template) == "undefined" ){
        template="default";
    }

    if (typeof(termek_aloldal) == "undefined" ){
        termek_aloldal=false;
    }
    quantity = typeof(quantity) != 'undefined' ? quantity : 1;

    if (termek_aloldal) {
        if ($('.product-info input[name=\'quantity2\']')) {
            $('.product-info input[name=\'quantity2\']').attr("value",quantity);
        }

        var datatombatad= $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea');
    } else {
        var datatombatad='product_id=' + product_id + '&quantity=' + quantity;
    }

    $.ajax({
        url: 'index.php?route=checkout/cart/addMegrendelem&termek_aloldal='+termek_aloldal+'&display=block',
        type: 'post',
        data : datatombatad,
        dataType: 'json',
        success: function(json) {

            $('.success, .warning, .attention, .information, .error').remove();

            if (json['error']) {

                if (json['error']['warning']) {
                    $('#notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                    $('.warning').slideDown('slow');
                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                }

                if (json['error']['option']) {
                    $('#notification').html('<div class="warning" style="display: none;">' + json['error']['option'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                    $('.warning').slideDown('slow');
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }

                if (json['redirect']) location = json['redirect'];


            }

            else if (json['success']) {
                if (json['redirect']) location = json['redirect'];




            }
        },
        error: function(json){
            debugger;

        },

        complete: function (json) {

        }
    });
}



function addToWishList(product_id) {
	$.ajax({
		url: 'index.php?route=account/wishlist/add',
		type: 'post',
		data: 'product_id=' + product_id,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information').remove();
						
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
				$('.success').slideDown('slow');
				
				$('#wishlist-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow');
                notaficationBezar();
            }

            if (json['error']) {
				$('#notification').html('<div class="warning" style="display: none;">' + json['error'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
				$('.warning').slideDown('slow');

				$('#wishlist-total').html(json['total']);

				$('html, body').animate({ scrollTop: 0 }, 'slow');
                notaficationBezar();
            }
		}
	});
}



function addToCompare(product_id) {

	$.ajax({
		url: 'index.php?route=product/compare/add',
		type: 'post',
		data: 'product_id=' + product_id,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information').remove();
						
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                $('.success').slideDown('slow');

				$('#compare-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow');
                notaficationBezar();
            }
		},
        error: function(json) {
            debugger;
        }
	});
}

function reloadMiniCartContent(product_id) {
    var sidecart= $("#sidecart");
    $(".box-right-sidecart-content", sidecart).add("#sidecart").load('index.php?route=module/sidecart #sidecart > *');



    //$(".box-right-sidecart-content", sidecart).add("#cart .content").load('index.php?route=module/cart .content > *');
}

function removeFromCart(product_id) {
    $.ajax({
        url: 'index.php?route=module/cart',
        type: 'get',
        data: 'remove=' + product_id,
        dataType: 'html',
        success: function(html) {

            $('.success, .warning, .attention, .information').remove();

            $('#cart').replaceWith(html);
            $(".box-right-sidecart-content").add("#sidecart").load('index.php?route=module/sidecart #sidecart > *');
            if (html.indexOf("empty") < 0) {
                $('.cart-info').load($('base').attr('href') + 'index.php?route=checkout/cart&no_header=1');
            } else {
                $('#content').load($('base').attr('href') + 'index.php?route=checkout/cart&no_header=1');
            }

            $('#notification').html('<div class="success" style="display: none;">' +  $('input[name=\'text_success\']').attr('value') + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
            $('.success').slideDown('slow');


            $('#cart_responsive .content_responsive').load('index.php?route=module/cart #cart .content > *');
            $("#cart_responsive .responsive_darab").remove();
            $("#cart_responsive span").load('index.php .slidecontent .responsive_darab_cart');
            notaficationBezar();
            $('#cart').addClass('active');

            $('#cart').on('mouseleave', function() {
                $(this).removeClass('active');
            });
            if (typeof(kosarba) == 'function') {
                kosarba();
            }
        },
        error: function(e) {

        }
    });
}

function JelentesNyomtat() {

    var column_left_display     = $("#column-left").css('display');
    var header_display          = $("#header").css('display');
    var footer_display          = $("#footer").css('display');
    var pageshare_display       = $("#pageshare").css('display');
    var breadcrumb_display      = $(".breadcrumb").css('display');
    var powered_display         = $("#powered").css('display');
    var second_footer_display   = $("#second_footer").css('display');
    var margin_left_content     = $("#content").css('margin-left');
    var margin_content          = $("#content").css('margin');
    var width_content           = $("#content").css('width');
    var margin                  = $("#column-left + #column-right + #content, #column-left + #content").css("margin-left");
    var pegination              = $(".pagination").css("display");
    var nyomtat                 = $(".nyomtat").css("display");

    $("#column-left").css('display','none');
    $("#header").css('display','none');
    $("#footer").css('display','none');
    $("#pageshare").css('display','none');
    $(".breadcrumb").css('display','none');
    $("#powered").css('display','none');
    $("#second_footer").css('display','none');
    $(".pagination").css("display","none");
    $("#content").css('margin','0 auto');
    $("#content").css('width',"800px");
    $(".nyomtat").css("display","none");

    window.print();

    $("#column-left").css('display',column_left_display);
    $("#header").css('display',header_display);
    $("#footer").css('display',footer_display);
    $("#pageshare").css('display',pageshare_display);
    $(".breadcrumb").css('display',breadcrumb_display);
    $("#powered").css('display',powered_display);
    $("#second_footer").css('display',second_footer_display);
    $("#content").css('margin-left',margin_left_content);
    $("#content").css('margin',margin_content);
    $("#content").css('width',width_content);
    $(".pagination").css("display",pegination);
    $(".nyomtat").css("display",nyomtat);

    $("#column-left + #column-right + #content, #column-left + #content").css("margin-left",margin);
}

Number.prototype.format = function(n, x, s, c) {
    if(n == undefined) {
        n = 0;
    }
    if(s == undefined) {
        s = '.';
    }
    if(c == undefined) {
        c = ',';
    }
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

function number_format(number, n, x, s, c) {
    return number.format(n, x, s, c);
}

function responsiveToggle(element, display) {
    var status = null;
    if(element instanceof Object) {
        var elementsArray = element.toArray();
        for(var n=0;n < elementsArray.length;n++) {
            var e = elementsArray[n];
            if(e.style.display == display) {
                e.removeAttribute('style');
                status = 0;
            } else {
                e.style.display = display;
                status = 1;
            }
        }
    }
    return status;
}


function mindentbecsuk(elem,display) {

    if($(window).width() <= 1250){
    //if($("smallest_responsive_icons_main").css("display") != "none"){
        var becsukando = [".multicategory","#responsive_kiemeltmenusor","#responsive_informations_small","#cart .content","#responsive_informations"] ;
        for(var i = 0; i < becsukando.length;i++){
            if(becsukando[i] != elem){
                $(becsukando[i]).removeAttr( 'style' );
            }
            else{
                if($(elem).css("display") != display){
                    $(elem).css("display",display);
                    if($(elem) != "#cart .content"){
                        $("#cart").removeClass("active");
                    }

                }
                else{
                    $(elem).removeAttr( 'style' );

                }

            }

        }
    }
}

function notaficationBezar() {

    $('#notification > div').mouseenter(function(){
        clearTimeout(notafication_kiiras);
    });

    $('#notification > div').mouseleave(function(){
        clearTimeout(notafication_kiiras);
        notafication_kiiras = setTimeout(" $('#notification > div').slideUp(500, function(){$('#notification > div').remove();})",1000);
    });

    if (typeof(notafication_kiiras) == 'number') {
        clearTimeout(notafication_kiiras);
    }
    notafication_kiiras = setTimeout(" $('#notification > div').slideUp(500, function(){$('#notification > div').remove();})",7000);

}




function cookiefigyelmeztetBezar(element){
    var now = new Date();
    var expireDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 7);
    document.cookie=element+'_accept_cookies=true;';
    $('#cookie_figyelmeztetes').remove();
}

function arajanlatotKerek(product_id,name,enquiry,email,telefon) {

    var get_url = 'product_id=' + product_id;

    if (typeof(name) != 'undefined') {
        get_url += '&name='+name;
    }
    if (typeof(enquiry) != 'undefined') {
        get_url += '&enquiry='+enquiry;
    }
    if (typeof(email) != 'undefined') {
        get_url += '&email='+email;
    }
    if (typeof(telefon) != 'undefined') {
        get_url += '&telefon='+telefon;
    }

    $("#lepel").remove();

    $.ajax({
        url: 'index.php?route=information/arajanlatot_kerek',
        type: 'GET',
        data: get_url,
        dataType: 'html',
        success: function(html) {
            if ($("#arajanlatot_kerek_mail").length > 0) {
                $("#arajanlatot_kerek_mail").remove();
                $("#container").before(html);

                $("#arajanlatot_kerek_mail").css("height",$('body').height());
                $('.ajanlat_form').css('top',$(window).scrollTop());


            } else {
                $("#container").before(html);
                $("#arajanlatot_kerek_mail").css("height",$('body').height());
                $('.ajanlat_form').css('top',$(window).scrollTop());

            }

        },
        error: function(e) {
        }
    });
}

function fogalomMagyarazat(fogalom_id,para) {

    hely = $(para).parent().offset();
    $('.fogalom_magyarazo').remove();

    $.ajax({
        url: 'index.php?route=information/fogalom_magyarazat',
        type: 'POST',
        data: 'fogalom_id=' + fogalom_id,
        dataType: 'html',
        success: function(html) {
            var html_elo = '<div style="top: '+yOffset+'px; left: '+xOffset+'px; display: none;" class="fogalom_magyarazo" >';
                html_elo += html;
                html_elo += '</div>';
            $("body").append(html_elo);

            var ossz_height = $(window).height()+$(window).scrollTop();
            var ossz_width = $(window).width()+$(window).scrollLeft();

            if ( (ossz_height-yOffset) < $(".fogalom_magyarazo").height()) {
                yOffset -= $(".fogalom_magyarazo").height();
                $(".fogalom_magyarazo").css("top",yOffset+'px');
            }

            if ( (ossz_width-xOffset) < $(".fogalom_magyarazo").width()) {
                xOffset -= $(".fogalom_magyarazo").width();
                $(".fogalom_magyarazo").css("left",xOffset+'px');
            }
            $('.fogalom_magyarazo').slideDown("slow");

        },
        error: function(e) {
        }
    });


}

function fogalomClosed() {

    $('.fogalom_magyarazo').slideUp("slow",function() {
        $('.fogalom_magyarazo').remove();
    });
}

function locationPost(url, postData)
{
    var postFormStr = "<form method='POST' action='" + url + "'>\n";

    for (var key in postData)
    {
        if (postData.hasOwnProperty(key))
        {
            postFormStr += "<input type='hidden' name='" + key + "' value='" + postData[key] + "'></input>";
        }
    }

    postFormStr += "</form>";

    var formElement = $(postFormStr);

    $('body').append(formElement);
    debugger;
    $(formElement).submit();
}


function arajanlatKeszites(product_id, quantity, template, kep_id,termek_aloldal) {

    if (typeof(template) == "undefined" ){
        template="default";
    }

    if (typeof(termek_aloldal) == "undefined" ){
        termek_aloldal=false;
    }
    quantity = typeof(quantity) != 'undefined' ? quantity : 1;

    if (termek_aloldal) {
        var datatombatad= $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea');
    } else {
        var datatombatad='product_id=' + product_id + '&quantity=' + quantity;
    }

    $.ajax({
        url: 'index.php?route=module/arajanlat/add&termek_aloldal='+termek_aloldal+'&display=block',
        type: 'post',
        data : datatombatad,
        dataType: 'html',
        success: function(html) {
            $('html, body').animate({ scrollTop: 0 }, 'slow',function(){
                if (html.substr(0,5) == 'error') {
                    if (html.substr(5,9) == '_redirect') {

                        $('.success, .warning, .attention, .information, .error').remove();
                        var tombom = html.split('-');
                        location=tombom[1]+'&warning='+tombom[2];
                    } else {
                        $('.success, .warning, .attention, .information, .error').remove();
                        $('#notification').html('<div class="warning" style="display: none;">' + html.substr(5) + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                        notaficationBezar();
                        $('.warning').slideDown('slow');
                    }
                } else {
                    if ($("#arajanlatok").length > 0) {
                        $("#arajanlatok").remove();
                        $("#container").before(html);

                        $("#arajanlatok").css("height", $('body').height());
                        $('.ajanlat_form').css('top', $(window).scrollTop());

                    } else {
                        $("#container").before(html);
                        $("#arajanlatok").css("height", $('body').height());
                        $('.ajanlat_form').css('top', $(window).scrollTop());

                    }
                    $('#header_ajanlatkeres').slideDown("slow");
                }

            });
        },
        error: function(html){
            debugger;
        },

        complete: function (html) {

        }
    });
}


function ajanlatKeresMegtekintes(product_id, quantity, template, kep_id,termek_aloldal) {

    if (typeof(template) == "undefined" ){
        template="default";
    }

    if (typeof(termek_aloldal) == "undefined" ){
        termek_aloldal=false;
    }
    quantity = typeof(quantity) != 'undefined' ? quantity : 1;

        var datatombatad= $('#arajanlatok input[type=\'text\']');


    $.ajax({
        url: 'index.php?route=module/arajanlat&termek_aloldal='+termek_aloldal+'&display=block',
        type: 'post',
        data : datatombatad,
        dataType: 'html',
        success: function(html) {
            if (html.substr(0,5) == 'error') {
                $('.success, .warning, .attention, .information, .error').remove();
                $('#notification').html('<div class="warning" style="display: none;">' + html.substr(5) + '<img src="catalog/view/theme/'+template+'/image/close.png" alt="" class="close" /></div>');
                notaficationBezar();
                $('.warning').slideDown('slow');
            } else {
                if ($("#arajanlatok").length > 0) {
                    $("#arajanlatok").remove();
                    $("#container").before(html);

                    $("#arajanlatok").css("height", $('body').height());
                    $('.ajanlat_form').css('top', $(window).scrollTop());


                } else {
                    $("#container").before(html);
                    $("#arajanlatok").css("height", $('body').height());
                    $('.ajanlat_form').css('top', $(window).scrollTop());

                }
            }



        },
        error: function(html){
            debugger;
        },

        complete: function (html) {

        }
    });
}

(function($) {
    $.fn.autocomplete = function(option) {
        return this.each(function() {
            this.timer = null;
            this.items = new Array();

            $.extend(this, option);

            $(this).attr('autocomplete', 'off');

            // Focus
            $(this).on('focus', function() {
                this.request();
            });

            // Blur
            $(this).on('blur', function() {
                setTimeout(function(object) {
                    object.hide();
                }, 200, this);
            });

            // Keydown
            $(this).on('keydown', function(event) {
                switch(event.keyCode) {
                    case 27: // escape
                        this.hide();
                        break;
                    default:
                        this.request();
                        break;
                }
            });

            // Click
            this.click = function(event) {
                debugger;
                event.preventDefault();

                value = $(event.target).parent().attr('data-value');

                if (value && this.items[value]) {
                    this.select(this.items[value]);
                } else {
                    value = $(event.target).parent().parent().attr('data-value');
                    if (value && this.items[value]) {
                        this.select(this.items[value]);
                    }
                }
            }

            // Show
            this.show = function() {
                var pos = $(this).position();

                $(this).siblings('ul.dropdown-menu').css({
                    top: pos.top + $(this).outerHeight(),
                    left: pos.left
                });

                $(this).siblings('ul.dropdown-menu').show();
            }

            // Hide
            this.hide = function() {
                $(this).siblings('ul.dropdown-menu').hide();
            }

            // Request
            this.request = function() {
                clearTimeout(this.timer);

                this.timer = setTimeout(function(object) {
                    object.source($(object).val(), $.proxy(object.response, object));
                }, 200, this);
            }

            // Response
            this.response = function(json) {
                html = '';
                if (json.length) {
                    for (i = 0; i < json.length; i++) {
                        this.items[json[i]['value']] = json[i];
                    }

                    for (i = 0; i < json.length; i++) {
                        if (!json[i]['category']) {
                            html += '<li data-value="' + json[i]['value'] + '">';
                                html += '<a class="kep" href="#"><img src="' + json[i]['image'] + '"></a>';
                                html += '<a class="szoveg" href="#">' + json[i]['label'] + '</a>';
                            html += '</li>';
                        }
                    }

                    // Get all the ones with a categories
                    var category = new Array();

                    for (i = 0; i < json.length; i++) {
                        if (json[i]['category']) {
                            if (!category[json[i]['category']]) {
                                category[json[i]['category']] = new Array();
                                category[json[i]['category']]['name'] = json[i]['category'];
                                category[json[i]['category']]['item'] = new Array();
                            }

                            category[json[i]['category']]['item'].push(json[i]);
                        }
                    }

                    for (i in category) {
                        html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

                        for (j = 0; j < category[i]['item'].length; j++) {
                            html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                        }
                    }
                }

                if (html) {
                    this.show();
                } else {
                    this.hide();
                }

                $(this).siblings('ul.dropdown-menu').html(html);
            }

            $(this).after('<ul class="dropdown-menu"></ul>');
            $(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));

        });
    }
})(window.jQuery);
